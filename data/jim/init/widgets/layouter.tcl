proc window: {dsc} {
  set _layouter_water_mark_ {}
  set window [dict create]
  set widgets [dict create]
  set widget [dict create]
  set widgetlist {}
  set zxw {}
  #
  set window(scheme) ""
  set window(id) ""
  set window(title) ""
  set window(x) 0
  set window(y) 0
  set window(width) 64
  set window(height) 64
  set window(margin-x) 2
  set window(margin-y) 2
  #
  namespace eval ::zx-window $dsc
  #
  set zxw [winsys window $window(scheme) $window(id) $window(x) $window(y) $window(width) $window(height) $window(title)]
  # now create widgets
  set wpost {}  ;# postfix widgets
  #
  foreach dsc $widgetlist {
    #cputs "widget: [lindex $dsc 0]"
    if {[lindex $dsc 0] eq "<margins>"} {
      #cputs "new margin: <[lindex $dsc 1]>"
      set xy [::zx-window::i::pos-expr {*}[lindex $dsc 1]]
      set window(margin-x) $xy(x)
      set window(margin-y) $xy(y)
      #set xy [lindex $dsc 1]
      #set window(margin-x) [uplevel 1 expr [lindex $xy 0]]
      #set window(margin-y) [uplevel 1 expr [lindex $xy 1]]
      #cputs "margins: <$window(margin-x):$window(margin-y)>"
    } else {
      # widget also set
      set widget [dict create]
      set zxg [namespace eval ::zx-window $dsc]
      set widget(zxg) $zxg
      lappend wpost $widget
    }
  }
  # do postfixes
  foreach ww $wpost {
    ::zx-window::common::post-setup $zxw $ww(zxg) ww
  }
  # do oninit
  foreach ww $wpost {
    if {$ww(oninit) ne ""} {
      set lmb [::zx-window::common::buildlambda $ww(oninit)]
      uplevel 1 eval $lmb $ww(zxg)
    }
  }
  #
  return $zxw
}


################################################################################
# internal functions
proc ::zx-window::i::find-lvl {} {
  set lvl 1
  while 1 {
    if {[uplevel $lvl exists -var _layouter_water_mark_]} {
      #cputs "level: $lvl"
      return $($lvl-1)
    }
    incr lvl
  }
}


proc ::zx-window::i::expr {args} {
  set lvl [::zx-window::i::find-lvl]
  return [uplevel $($lvl+1) expr {*}$args]
}


proc ::zx-window::i::process-pos-expr {zxw w e} {
  set newlst {}
  #
  foreach i $e {
    #cputs ":<$i>"
    if {[llength $i] == 2 && [lindex $i 0] in {after under x y width height}} {
      set gg [$zxw findbyid [lindex $i 1]]
      if {$gg eq ""} {
        if {[lindex $i 1] eq "window" && [lindex $i 0] in {width height}} {
          set gg $zxw
        } else {
          error "widget not found: <[lindex $i 1]>"
        }
      }
      #
      switch [lindex $i 0] {
        after { lappend newlst $([$gg getx]+[$gg getw]+$w(margin-x)); continue }
        under { lappend newlst $([$gg gety]+[$gg geth]+$w(margin-y)); continue }
        x { lappend newlst [$gg getx]; continue }
        y { lappend newlst [$gg gety]; continue }
        width { lappend newlst [$gg getw]; continue }
        height { lappend newlst [$gg geth]; continue }
      }
    }
    #
    if {[llength $i] > 1} {
      lappend newlst [list [process-pos-expr zxw w $i]]
      continue
    }
    #
    lappend newlst $i
  }
  return $newlst
}


proc ::zx-window::i::pos-expr {args} {
  set lvl [::zx-window::i::find-lvl]
  # preprocess
  upvar $lvl window w
  upvar $lvl zxw zxw
  #
  if {[llength $args] == 2 && [lindex $args 0] in {after under}} {
    set gg [$zxw findbyid [lindex $args 1]]
    if {$gg eq ""} { error "widget not found: <[lindex $args 1]>" }
    #
    switch [lindex $args 0] {
      after {
        return [dict create \
          x $([$gg getx]+[$gg getw]+$w(margin-x)) \
          y [$gg gety]]
      }
      under {
        return [dict create \
          x [$gg getx] \
          y $([$gg gety]+[$gg geth]+$w(margin-y))]
      }
    }
  } elseif {[llength $args] == 2} {
    set x [::zx-window::i::process-pos-expr $zxw $w [lindex $args 0]]
    set y [::zx-window::i::process-pos-expr $zxw $w [lindex $args 1]]
    return [dict create x [uplevel $($lvl+1) expr {*}$x]  y [uplevel $($lvl+1) expr {*}$y]]
  }
  error "invalid coordinate expression: $args ([llength $args])"
}


proc ::zx-window::i::coord-expr {args} {
  set lvl [::zx-window::i::find-lvl]
  # preprocess
  upvar $lvl window w
  upvar $lvl zxw zxw
  #
  if {[llength $args] == 1} {
    set r [::zx-window::i::process-pos-expr $zxw $w $args]
    return [uplevel $($lvl+1) expr {*}$r]
  }
  error "invalid coordinate expression: $args"
}


proc ::zx-window::i::setvar {name value} {
  set lvl [::zx-window::i::find-lvl]
  #
  upvar $lvl $name tt
  set tt $value
  return value
}


proc ::zx-window::i::set-dict {dict key value} {
  ::zx-window::i::setvar "$dict\($key)" $value
}


proc ::zx-window::i::set-window {key value} {
  ::zx-window::i::set-dict window $key $value
}


proc ::zx-window::i::set-widget {key value} {
  ::zx-window::i::set-dict widget $key $value
}


################################################################################
# window properties
proc ::zx-window::scheme: {name} { ::zx-window::i::set-window scheme $name }
proc ::zx-window::id: {name} { ::zx-window::i::set-window id $name }
proc ::zx-window::title: {tit} { ::zx-window::i::set-window title $tit }
proc ::zx-window::position: {crd} {
  set xy [::zx-window::i::pos-expr {*}$crd]
  ::zx-window::i::set-window x $xy(x)
  ::zx-window::i::set-window y $xy(y)
}
proc ::zx-window::size: {crd} {
  set wh [::zx-window::i::pos-expr {*}$crd]
  ::zx-window::i::set-window width $wh(x)
  ::zx-window::i::set-window height $wh(y)
}
#proc ::zx-window::margins: {crd} {
#  set xy [::zx-window::i::pos-expr {*}$crd]
#  ::zx-window::i::set-window margin-x $xy(x)
#  ::zx-window::i::set-window margin-y $xy(y)
#}


################################################################################
# widget creators
proc ::zx-window::creators::new {type dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widgetlist l
  lappend l [list $type $dsc]
  return 1
}


proc ::zx-window::margins: {dsc} { ::zx-window::creators::new <margins> $dsc }

proc ::zx-window::frame: {dsc} { ::zx-window::creators::new frame $dsc }
proc ::zx-window::static: {dsc} { ::zx-window::creators::new static $dsc }
proc ::zx-window::button: {dsc} { ::zx-window::creators::new button $dsc }
proc ::zx-window::checkbox: {dsc} { ::zx-window::creators::new checkbox $dsc }
proc ::zx-window::radio: {dsc} { ::zx-window::creators::new radio $dsc }
proc ::zx-window::lineedit: {dsc} { ::zx-window::creators::new lineedit $dsc }
proc ::zx-window::scrollbar: {dsc} { ::zx-window::creators::new scrollbar $dsc }
proc ::zx-window::listbox: {dsc} { ::zx-window::creators::new listbox $dsc }
#proc ::zx-window::textedit: {dsc} { ::zx-window::creators::new textedit $dsc }


################################################################################
# helpers
proc ::zx-window::common::position {crd} {
  set xy [::zx-window::i::pos-expr {*}$crd]
  ::zx-window::i::set-widget x $xy(x)
  ::zx-window::i::set-widget y $xy(y)
}


proc ::zx-window::common::size: {crd} {
  set wh [::zx-window::i::pos-expr {*}$crd]
  ::zx-window::i::set-widget width $wh(x)
  ::zx-window::i::set-widget height $wh(y)
}


proc ::zx-window::common::coord: {field crd} {
  ::zx-window::i::set-widget $field [::zx-window::i::coord-expr $crd]
}


proc ::zx-window::common::framebox {wlst} {
  set lvl [::zx-window::i::find-lvl]
  #upvar $lvl window w
  upvar $lvl zxw zxw
  #
  set first 1
  set minx 0
  set maxx 0
  set miny 0
  set maxy 0
  foreach n $wlst {
    set gg [$zxw findbyid $n]
    if {$gg eq ""} { error "widget not found: <$n>" }
    set x [$gg getx]
    set y [$gg gety]
    set ex $($x + [$gg getw])
    set ey $($y + [$gg geth])
    if {$first} {
      set first 0
      set minx $x
      set miny $y
      set maxx $ex
      set maxy $ey
    } else {
      if {$x < $minx} { set minx $x }
      if {$y < $miny} { set miny $y }
      if {$ex > $maxx} { set maxx $ex }
      if {$ey > $maxy} { set maxy $ey }
    }
  }
  # fix for frame
  set minx $($minx - 3)
  set miny $($miny - 9)
  set maxx $($maxx + 3)
  set maxy $($maxy + 8 + 5)
  #
  ::zx-window::i::set-widget x $minx
  ::zx-window::i::set-widget y $miny
  ::zx-window::i::set-widget width $($maxx - $minx)
  ::zx-window::i::set-widget height $($maxy - $miny)
  return 1
}


proc ::zx-window::common::init {&g} {
  set g(id) ""
  set g(title) ""
  set g(width) -1
  set g(height) -1
  set g(active) 0
  set g(linked-to) ""
  set g(onclick) ""
  set g(oninit) ""
}


proc ::zx-window::common::post-setup {zxw zxg &g} {
  if {$g(active)} { $zxg activate }
  #if {$g(linked-from) ne ""} {
  #  set gg [$zxw findbyid $g(linked-from)]
  #  if {$gg eq ""} { error "widget not found: <$g(linked-from)>" }
  #  $gg setlinked $zxg
  #}
  if {$g(linked-to) ne ""} {
    set gg [$zxw findbyid $g(linked-to)]
    if {$gg eq ""} { error "widget not found: <$g(linked-to)>" }
    $zxg setlinked $gg
  }
}


proc ::zx-window::common::buildlambda {code} {
  if {$code ne ""} {
    # create an anonymous procedure
    set lmb [namespace eval :: "lambda {me} [list $code]"]
  } else {
    set lmb {}
  }
  return $lmb
}


################################################################################
# frame definition
proc ::zx-window::frame {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  set g(id) ""
  set g(title) ""
  #
  namespace eval ::zx-window::frame $dsc
  #
  set zxg [$zxw frame $g(id) $g(x) $g(y) $g(width) $g(height) $g(title)]
  #
  return $zxg
}


proc ::zx-window::frame::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::frame::caption: {tit} { ::zx-window::i::set-widget title $tit }
proc ::zx-window::frame::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::frame::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::frame::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::frame::size: {crd} { ::zx-window::common::size: $crd }
proc ::zx-window::frame::width: {c} { ::zx-window::common::coord: width $c }
proc ::zx-window::frame::height: {c} { ::zx-window::common::coord: height $c }
proc ::zx-window::frame::framebox: {args} { ::zx-window::common::framebox $args }
proc ::zx-window::frame::oninit: {name} { ::zx-window::i::set-widget oninit $name }


################################################################################
# static definition
proc ::zx-window::static {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  namespace eval ::zx-window::static $dsc
  #
  set zxg [$zxw static $g(id) $g(x) $g(y) $g(width) $g(height) $g(title)]
  return $zxg
}


proc ::zx-window::static::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::static::caption: {tit} { ::zx-window::i::set-widget title $tit }
proc ::zx-window::static::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::static::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::static::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::static::size: {crd} { ::zx-window::common::size: $crd }
proc ::zx-window::static::width: {c} { ::zx-window::common::coord: width $c }
proc ::zx-window::static::height: {c} { ::zx-window::common::coord: height $c }
proc ::zx-window::static::linked-to: {id} { ::zx-window::i::set-widget linked-to $id }
proc ::zx-window::static::oninit: {name} { ::zx-window::i::set-widget oninit $name }


################################################################################
# button definition
proc ::zx-window::button {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  set g(role) {}
  #
  namespace eval ::zx-window::button $dsc
  #
  set lmb [::zx-window::common::buildlambda $g(onclick)]
  set zxg [$zxw button $g(id) $g(x) $g(y) $g(width) $g(height) $g(title) $lmb]
  if {$g(role) ne ""} { $zxg set$g(role) }
  #::zx-window::common::post-setup $zxw $zxg g
  return $zxg
}


proc ::zx-window::button::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::button::caption: {tit} { ::zx-window::i::set-widget title $tit }
proc ::zx-window::button::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::button::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::button::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::button::size: {crd} { ::zx-window::common::size: $crd }
proc ::zx-window::button::width: {c} { ::zx-window::common::coord: width $c }
proc ::zx-window::button::height: {c} { ::zx-window::common::coord: height $c }
proc ::zx-window::button::onclick: {lmb} { ::zx-window::i::set-widget onclick $lmb }
proc ::zx-window::button::active {} { ::zx-window::i::set-widget active 1 }
proc ::zx-window::button::oninit: {name} { ::zx-window::i::set-widget oninit $name }
#
proc ::zx-window::button::default {} { ::zx-window::i::set-widget role default }
proc ::zx-window::button::cancel {} { ::zx-window::i::set-widget role cancel }


################################################################################
# checkbox definition
proc ::zx-window::checkbox {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  set g(state) 0
  #
  namespace eval ::zx-window::checkbox $dsc
  #
  set lmb [::zx-window::common::buildlambda $g(onclick)]
  set zxg [$zxw checkbox $g(id) $g(x) $g(y) $g(width) $g(height) $g(title) $lmb]
  $zxg setstate $g(state)
  #::zx-window::common::post-setup $zxw $zxg g
  return $zxg
}


proc ::zx-window::checkbox::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::checkbox::caption: {tit} { ::zx-window::i::set-widget title $tit }
proc ::zx-window::checkbox::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::checkbox::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::checkbox::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::checkbox::size: {crd} { ::zx-window::common::size: $crd }
proc ::zx-window::checkbox::width: {c} { ::zx-window::common::coord: width $c }
proc ::zx-window::checkbox::height: {c} { ::zx-window::common::coord: height $c }
proc ::zx-window::checkbox::onclick: {lmb} { ::zx-window::i::set-widget onclick $lmb }
proc ::zx-window::checkbox::active {} { ::zx-window::i::set-widget active 1 }
proc ::zx-window::checkbox::oninit: {name} { ::zx-window::i::set-widget oninit $name }
#
proc ::zx-window::checkbox::state: {st} { ::zx-window::i::set-widget state $st }
proc ::zx-window::checkbox::checked {} { ::zx-window::i::set-widget state 1 }
proc ::zx-window::checkbox::question {} { ::zx-window::i::set-widget state 2 }


################################################################################
# radio definition
proc ::zx-window::radio {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  set g(group) ""
  set g(radioactive) 0
  #
  namespace eval ::zx-window::radio $dsc
  #
  set lmb [::zx-window::common::buildlambda $g(onclick)]
  set zxg [$zxw radio $g(group) $g(id) $g(x) $g(y) $g(width) $g(height) $g(title) $lmb]
  if {$g(radioactive)} { $zxg setradioactive }
  #::zx-window::common::post-setup $zxw $zxg g
  return $zxg
}


proc ::zx-window::radio::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::radio::caption: {tit} { ::zx-window::i::set-widget title $tit }
proc ::zx-window::radio::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::radio::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::radio::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::radio::size: {crd} { ::zx-window::common::size: $crd }
proc ::zx-window::radio::width: {c} { ::zx-window::common::coord: width $c }
proc ::zx-window::radio::height: {c} { ::zx-window::common::coord: height $c }
proc ::zx-window::radio::onclick: {lmb} { ::zx-window::i::set-widget onclick $lmb }
proc ::zx-window::radio::active {} { ::zx-window::i::set-widget active 1 }
proc ::zx-window::radio::oninit: {name} { ::zx-window::i::set-widget oninit $name }
#
proc ::zx-window::radio::group: {name} { ::zx-window::i::set-widget group $name }
proc ::zx-window::radio::radioactive {} { ::zx-window::i::set-widget radioactive 1 }


################################################################################
# lineedit definition
proc ::zx-window::lineedit {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  set g(text) ""
  #
  namespace eval ::zx-window::lineedit $dsc
  #
  set lmb [::zx-window::common::buildlambda $g(onclick)]
  set zxg [$zxw lineedit $g(id) $g(x) $g(y) $g(width) $g(height) $g(text) $lmb]
  #::zx-window::common::post-setup $zxw $zxg g
  return $zxg
}

proc ::zx-window::lineedit::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::lineedit::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::lineedit::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::lineedit::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::lineedit::size: {crd} { ::zx-window::common::size: $crd }
proc ::zx-window::lineedit::width: {c} { ::zx-window::common::coord: width $c }
proc ::zx-window::lineedit::height: {c} { ::zx-window::common::coord: height $c }
proc ::zx-window::lineedit::onclick: {lmb} { ::zx-window::i::set-widget onclick $lmb }
proc ::zx-window::lineedit::active {} { ::zx-window::i::set-widget active 1 }
proc ::zx-window::lineedit::oninit: {name} { ::zx-window::i::set-widget oninit $name }
#
proc ::zx-window::lineedit::text: {txt} { ::zx-window::i::set-widget text $txt }


################################################################################
# scrollbar definition
proc ::zx-window::scrollbar {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  set g(text) ""
  set g(dir) vert
  set g(size) -1
  set g(min) 0
  set g(max) 100
  set g(pos) 0
  set g(page-size) 32
  #
  namespace eval ::zx-window::scrollbar $dsc
  #
  set lmb [::zx-window::common::buildlambda $g(onclick)]
  set zxg [$zxw scrollbar $g(dir) $g(id) $g(x) $g(y) $g(size) $lmb]
  $zxg setminmax $g(min) $g(max)
  $zxg setpos $g(pos)
  $zxg setpagesize $g(page-size)
  #::zx-window::common::post-setup $zxw $zxg g
  return $zxg
}

proc ::zx-window::scrollbar::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::scrollbar::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::scrollbar::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::scrollbar::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::scrollbar::size: {sz} { ::zx-window::common::coord: size $sz }
proc ::zx-window::scrollbar::width: {c} { ::zx-window::common::coord: size $c }
proc ::zx-window::scrollbar::height: {c} { ::zx-window::common::coord: size $c }
proc ::zx-window::scrollbar::onclick: {lmb} { ::zx-window::i::set-widget onclick $lmb }
proc ::zx-window::scrollbar::linked-to: {id} { ::zx-window::i::set-widget linked-to $id }
proc ::zx-window::scrollbar::active {} { ::zx-window::i::set-widget active 1 }
proc ::zx-window::scrollbar::oninit: {name} { ::zx-window::i::set-widget oninit $name }
#
proc ::zx-window::scrollbar::vert {} { ::zx-window::i::set-widget dir vert }
proc ::zx-window::scrollbar::horiz {} { ::zx-window::i::set-widget dir horiz }
proc ::zx-window::scrollbar::min: {v} { ::zx-window::i::set-widget min $v }
proc ::zx-window::scrollbar::max: {v} { ::zx-window::i::set-widget max $v }
proc ::zx-window::scrollbar::pos: {v} { ::zx-window::i::set-widget pos $v }
proc ::zx-window::scrollbar::page-size: {v} { ::zx-window::i::set-widget page-size $v }


################################################################################
# listbox definition
proc ::zx-window::listbox {dsc} {
  set lvl [::zx-window::i::find-lvl]
  upvar $lvl widget g
  upvar $lvl zxw zxw
  #
  ::zx-window::common::init g
  set g(text) ""
  set g(vbar) 0
  set g(hbar) 0
  set g(multi) 0
  set g(items) {}
  #
  namespace eval ::zx-window::listbox $dsc
  #
  set lmb [::zx-window::common::buildlambda $g(onclick)]
  set zxg [$zxw listbox $g(id) $g(x) $g(y) $g(width) $g(height) $lmb]
  foreach i $g(items) { $zxg additem $i }
  $zxg setvbar $g(vbar)
  $zxg sethbar $g(hbar)
  $zxg setmulti $g(multi)
  #::zx-window::common::post-setup $zxw $zxg g
  return $zxg
}

proc ::zx-window::listbox::id: {name} { ::zx-window::i::set-widget id $name }
proc ::zx-window::listbox::position: {crd} { ::zx-window::common::position $crd }
proc ::zx-window::listbox::x: {c} { ::zx-window::common::coord: x $c }
proc ::zx-window::listbox::y: {c} { ::zx-window::common::coord: y $c }
proc ::zx-window::listbox::size: {crd} { ::zx-window::common::size: $crd }
proc ::zx-window::listbox::width: {c} { ::zx-window::common::coord: width $c }
proc ::zx-window::listbox::height: {c} { ::zx-window::common::coord: height $c }
proc ::zx-window::listbox::onclick: {lmb} { ::zx-window::i::set-widget onclick $lmb }
proc ::zx-window::listbox::active {} { ::zx-window::i::set-widget active 1 }
proc ::zx-window::listbox::oninit: {name} { ::zx-window::i::set-widget oninit $name }
#
proc ::zx-window::listbox::vbar {} { ::zx-window::i::set-widget vbar 1 }
proc ::zx-window::listbox::hbar {} { ::zx-window::i::set-widget hbar 1 }
proc ::zx-window::listbox::multi {} { ::zx-window::i::set-widget multi 1 }
proc ::zx-window::listbox::items: {v} { ::zx-window::i::set-widget items $v }
