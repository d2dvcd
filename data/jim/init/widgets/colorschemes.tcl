# blue
# caption inactive
setschemecolor "/blue/caption/stripes" 2
setschemecolor "/blue/caption/ink" 0
setschemecolor "/blue/caption/paper" 7
# caption active
setschemecolor "/blue/caption/active/stripes" 1
setschemecolor "/blue/caption/active/ink" 0
setschemecolor "/blue/caption/active/paper" 15
# caption disabled
setschemecolor "/blue/caption/disabled/stripes" 2
setschemecolor "/blue/caption/disabled/ink" 6
setschemecolor "/blue/caption/disabled/paper" 7
# window
setschemecolor "/blue/window/moving/alpha" 190
# window inactive
setschemecolor "/blue/window/alpha" 140
setschemecolor "/blue/window/ink" 7
setschemecolor "/blue/window/paper" 1
setschemecolor "/blue/window/hotkey_ink" 4
setschemecolor "/blue/window/hotkey_paper" 1
# window active
setschemecolor "/blue/window/active/alpha" 255
setschemecolor "/blue/window/active/ink" 15
setschemecolor "/blue/window/active/paper" 9
setschemecolor "/blue/window/active/hotkey_ink" 12
setschemecolor "/blue/window/active/hotkey_paper" 9
# window disabled
setschemecolor "/blue/window/disabled/ink" 6
setschemecolor "/blue/window/disabled/paper" 1
setschemecolor "/blue/window/disabled/hotkey_ink" 6
setschemecolor "/blue/window/disabled/hotkey_paper" 1
#
# frame
# static
# button inactive
setschemecolor "/blue/button/ink" 0
setschemecolor "/blue/button/paper" 7
setschemecolor "/blue/button/hotkey_ink" 2
setschemecolor "/blue/button/hotkey_paper" 7
# button active
setschemecolor "/blue/button/active/ink" 0
setschemecolor "/blue/button/active/paper" 15
setschemecolor "/blue/button/active/hotkey_ink" 10
setschemecolor "/blue/button/active/hotkey_paper" 15
# button disabled
setschemecolor "/blue/button/disabled/ink" 0
setschemecolor "/blue/button/disabled/paper" 6
setschemecolor "/blue/button/disabled/hotkey_ink" 0
setschemecolor "/blue/button/disabled/hotkey_paper" 6
# checkbox
# radio
# lineedit
setschemecolor "/blue/lineedit/ink" 7
setschemecolor "/blue/lineedit/paper" 0
setschemecolor "/blue/lineedit/active/ink" 15
setschemecolor "/blue/lineedit/disabled/ink" 6
setschemecolor "/blue/lineedit/arrow_ink" 6
setschemecolor "/blue/lineedit/active/arrow_ink" 14
setschemecolor "/blue/lineedit/disabled/arrow_ink" 6
# scrollbar
setschemecolor "/blue/scrollbar/ink" 0
setschemecolor "/blue/scrollbar/paper" 5
setschemecolor "/blue/scrollbar/knob_ink" 7
setschemecolor "/blue/scrollbar/active/knob_ink" 15
setschemecolor "/blue/scrollbar/disabled/ink" 6
setschemecolor "/blue/scrollbar/disabled/paper" 5
setschemecolor "/blue/scrollbar/disabled/knob_ink" 6
# listbox
setschemecolor "/blue/listbox/ink" 7
setschemecolor "/blue/listbox/paper" 0
setschemecolor "/blue/listbox/cursor_ink" 0
setschemecolor "/blue/listbox/cursor_paper" 7
setschemecolor "/blue/listbox/mark_ink" 0
setschemecolor "/blue/listbox/mark_paper" 5
setschemecolor "/blue/listbox/curmark_ink" 1
setschemecolor "/blue/listbox/curmark_paper" 7
setschemecolor "/blue/listbox/active/ink" 15
setschemecolor "/blue/listbox/active/cursor_ink" 0
setschemecolor "/blue/listbox/active/cursor_paper" 15
setschemecolor "/blue/listbox/active/mark_ink" 0
setschemecolor "/blue/listbox/active/mark_paper" 13
setschemecolor "/blue/listbox/active/curmark_ink" 9
setschemecolor "/blue/listbox/active/curmark_paper" 15


# red
# caption inactive
setschemecolor "/red/caption/stripes" 0
setschemecolor "/red/caption/ink" 0
setschemecolor "/red/caption/paper" 4
# caption active
setschemecolor "/red/caption/active/stripes" 0
setschemecolor "/red/caption/active/ink" 0
setschemecolor "/red/caption/active/paper" 4
# caption disabled
setschemecolor "/red/caption/disabled/stripes" 0
setschemecolor "/red/caption/disabled/ink" 9
setschemecolor "/red/caption/disabled/paper" 95
# window
setschemecolor "/red/window/moving/alpha" 190
# window inactive
setschemecolor "/red/window/alpha" 140
setschemecolor "/red/window/ink" 86
setschemecolor "/red/window/paper" 179
setschemecolor "/red/window/hotkey_ink" 116
setschemecolor "/red/window/hotkey_paper" 2
# window active
setschemecolor "/red/window/active/alpha" 255
setschemecolor "/red/window/active/ink" 4
setschemecolor "/red/window/active/paper" 176
setschemecolor "/red/window/active/hotkey_ink" 112
setschemecolor "/red/window/active/hotkey_paper" 176
# window disabled
setschemecolor "/red/window/disabled/ink" 9
setschemecolor "/red/window/disabled/paper" 179
setschemecolor "/red/window/disabled/hotkey_ink" 9
setschemecolor "/red/window/disabled/hotkey_paper" 179
#
# frame
# static
# button inactive
setschemecolor "/red/button/ink" 0
setschemecolor "/red/button/paper" 86
setschemecolor "/red/button/hotkey_ink" 180
setschemecolor "/red/button/hotkey_paper" 86
# button active
setschemecolor "/red/button/active/ink" 0
setschemecolor "/red/button/active/paper" 4
setschemecolor "/red/button/active/hotkey_ink" 176
setschemecolor "/red/button/active/hotkey_paper" 4
# button disabled
setschemecolor "/red/button/disabled/ink" 0
setschemecolor "/red/button/disabled/paper" 6
setschemecolor "/red/button/disabled/hotkey_ink" 0
setschemecolor "/red/button/disabled/hotkey_paper" 6
# checkbox
# radio
# lineedit
setschemecolor "/red/lineedit/ink" 86
setschemecolor "/red/lineedit/paper" 0
setschemecolor "/red/lineedit/active/ink" 4
setschemecolor "/red/lineedit/disabled/ink" 8
setschemecolor "/red/lineedit/arrow_ink" 162
setschemecolor "/red/lineedit/active/arrow_ink" 160
setschemecolor "/red/lineedit/disabled/arrow_ink" 164
# scrollbar
setschemecolor "/red/scrollbar/ink" 0
setschemecolor "/red/scrollbar/paper" 92
setschemecolor "/red/scrollbar/knob_ink" 86
setschemecolor "/red/scrollbar/active/knob_ink" 4
#TODO
setschemecolor "/red/scrollbar/disabled/ink" 6
setschemecolor "/red/scrollbar/disabled/paper" 5
setschemecolor "/red/scrollbar/disabled/knob_ink" 6
# listbox
#setschemecolor "/red/listbox/ink" 7
setschemecolor "/red/listbox/paper" 0
setschemecolor "/red/listbox/cursor_ink" 0
setschemecolor "/red/listbox/cursor_paper" 86
setschemecolor "/red/listbox/mark_ink" 196
setschemecolor "/red/listbox/mark_paper" 3
setschemecolor "/red/listbox/curmark_ink" 200
setschemecolor "/red/listbox/curmark_paper" 86
#setschemecolor "/red/listbox/active/ink" 15
setschemecolor "/red/listbox/active/cursor_ink" 0
setschemecolor "/red/listbox/active/cursor_paper" 4
setschemecolor "/red/listbox/active/mark_ink" 194
setschemecolor "/red/listbox/active/mark_paper" 3
setschemecolor "/red/listbox/active/curmark_ink" 197
setschemecolor "/red/listbox/active/curmark_paper" 4


# green
# caption inactive
setschemecolor "/green/caption/stripes" 2
setschemecolor "/green/caption/ink" 0
setschemecolor "/green/caption/paper" 7
# caption active
setschemecolor "/green/caption/active/stripes" 1
setschemecolor "/green/caption/active/ink" 0
setschemecolor "/green/caption/active/paper" 15
# caption disabled
setschemecolor "/green/caption/disabled/stripes" 2
setschemecolor "/green/caption/disabled/ink" 6
setschemecolor "/green/caption/disabled/paper" 7
# window
setschemecolor "/green/window/moving/alpha" 190
# window inactive
setschemecolor "/green/window/alpha" 140
setschemecolor "/green/window/ink" 14
setschemecolor "/green/window/paper" 4
setschemecolor "/green/window/hotkey_ink" 12
setschemecolor "/green/window/hotkey_paper" 4
# window active
setschemecolor "/green/window/active/alpha" 255
setschemecolor "/green/window/active/ink" 15
setschemecolor "/green/window/active/paper" 4
setschemecolor "/green/window/active/hotkey_ink" 15
setschemecolor "/green/window/active/hotkey_paper" 4
# window disabled
setschemecolor "/green/window/disabled/ink" 1
setschemecolor "/green/window/disabled/paper" 4
setschemecolor "/green/window/disabled/hotkey_ink" 1
setschemecolor "/green/window/disabled/hotkey_paper" 4
#
# frame
# static
# button inactive
setschemecolor "/green/button/ink" 0
setschemecolor "/green/button/paper" 7
setschemecolor "/green/button/hotkey_ink" 2
setschemecolor "/green/button/hotkey_paper" 7
# button active
setschemecolor "/green/button/active/ink" 0
setschemecolor "/green/button/active/paper" 15
setschemecolor "/green/button/active/hotkey_ink" 12
setschemecolor "/green/button/active/hotkey_paper" 15
# button disabled
setschemecolor "/green/button/disabled/ink" 0
setschemecolor "/green/button/disabled/paper" 6
setschemecolor "/green/button/disabled/hotkey_ink" 1
setschemecolor "/green/button/disabled/hotkey_paper" 6
# checkbox
# radio
# lineedit
setschemecolor "/green/lineedit/ink" 7
setschemecolor "/green/lineedit/paper" 0
setschemecolor "/green/lineedit/active/ink" 15
setschemecolor "/green/lineedit/disabled/ink" 6
setschemecolor "/green/lineedit/arrow_ink" 6
setschemecolor "/green/lineedit/active/arrow_ink" 14
setschemecolor "/green/lineedit/disabled/arrow_ink" 6
# scrollbar
setschemecolor "/green/scrollbar/ink" 0
setschemecolor "/green/scrollbar/paper" 5
setschemecolor "/green/scrollbar/knob_ink" 7
setschemecolor "/green/scrollbar/active/knob_ink" 15
setschemecolor "/green/scrollbar/disabled/ink" 6
setschemecolor "/green/scrollbar/disabled/paper" 5
setschemecolor "/green/scrollbar/disabled/knob_ink" 6
# listbox
setschemecolor "/green/listbox/ink" 7
setschemecolor "/green/listbox/paper" 0
setschemecolor "/green/listbox/cursor_ink" 0
setschemecolor "/green/listbox/cursor_paper" 7
setschemecolor "/green/listbox/mark_ink" 0
setschemecolor "/green/listbox/mark_paper" 5
setschemecolor "/green/listbox/curmark_ink" 1
setschemecolor "/green/listbox/curmark_paper" 7
setschemecolor "/green/listbox/active/ink" 15
setschemecolor "/green/listbox/active/cursor_ink" 0
setschemecolor "/green/listbox/active/cursor_paper" 15
setschemecolor "/green/listbox/active/mark_ink" 0
setschemecolor "/green/listbox/active/mark_paper" 13
setschemecolor "/green/listbox/active/curmark_ink" 9
setschemecolor "/green/listbox/active/curmark_paper" 15
