################################################################################
proc _zxemut_init {} {
  rename _zxemut_init {}

  # Add to the standard auto_path
  lappend p "$::bindir/data/jim/lib"
  lappend p {*}$::auto_path
  set ::auto_path $p
  #cputs "$::auto_path"
}

_zxemut_init
