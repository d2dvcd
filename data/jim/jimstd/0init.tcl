# there is some bug (?) in jim package system (or in my code) which leads to memory corruption; i'm sad
if {$::jim_internal} {
  load "jimstd/stdlib.tcl"
  load "jimstd/nshelper.tcl"
  load "jimstd/oo.tcl"
  load "jimstd/tclcompat.tcl"
  #load "jimstd/binary.tcl"  removed due to bug either in Jim or in D2D
  #load "jimstd/glob.tcl"
  #load "jimstd/tree.tcl"
} else {
}
