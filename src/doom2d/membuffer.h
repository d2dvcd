/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#ifndef D2D_MEMBUFFER_H
#define D2D_MEMBUFFER_H

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef struct MemBuffer MemBuffer;


extern MemBuffer *membuf_new_write (void); // for writing
extern void membuf_free (MemBuffer *buf);
extern void membuf_clear (MemBuffer *buf);
extern void membuf_set_error (MemBuffer *buf, int err);

extern void membuf_write2read (MemBuffer *buf); // will not change 'freedata'; resets reading pos
extern int membuf_write2readex (MemBuffer *buf, int sz);

extern int membuf_set_min_size (MemBuffer *buf, int sz); // only for writebufs

// returs # of bytes written; can't be lesser than datalen! <0: error
// so it returns either datalen or <0
extern int membuf_write (MemBuffer *buf, const void *data, int datalen);

extern MemBuffer *membuf_new_read (const void *data, int datalen);
extern MemBuffer *membuf_new_read_copy (const void *data, int datalen); // copies data
extern MemBuffer *membuf_new_read_free (const void *data, int datalen);
extern MemBuffer *membuf_new_read_from_file (FILE *fl, int size);

// returs # of bytes read; can be lesser than datalen! <0: error
// warning: if membuf_read() reads less than datalen, it will
// set error flag on buffer!
extern int membuf_read (MemBuffer *buf, void *data, int datalen);

// returns only datalen on -1
extern int membuf_read_full (MemBuffer *buf, void *data, int datalen);

extern int membuf_read_skip (MemBuffer *buf, int datalen);
extern void membuf_read_reset (MemBuffer *buf);

extern int membuf_used (const MemBuffer *buf);
extern int membuf_size (const MemBuffer *buf);
extern void *membuf_data (const MemBuffer *buf); //WARNING! 'const' for membuf_new_read!
extern const void *membuf_data_const (const MemBuffer *buf); //WARNING! 'const' for membuf_new_read!
extern int membuf_error (const MemBuffer *buf);

extern int membuf_read_bool (MemBuffer *mbuf); // only 0 or 1
extern uint8_t membuf_read_ui8 (MemBuffer *mbuf);
extern uint16_t membuf_read_ui16 (MemBuffer *mbuf);
extern uint32_t membuf_read_ui32 (MemBuffer *mbuf);
extern int8_t membuf_read_i8 (MemBuffer *mbuf);
extern int16_t membuf_read_i16 (MemBuffer *mbuf);
extern int32_t membuf_read_i32 (MemBuffer *mbuf);

extern int membuf_write_bool (MemBuffer *mbuf, int v);
extern int membuf_write_ui8 (MemBuffer *mbuf, uint8_t v);
extern int membuf_write_ui16 (MemBuffer *mbuf, uint16_t v);
extern int membuf_write_ui32 (MemBuffer *mbuf, uint32_t v);
extern int membuf_write_i8 (MemBuffer *mbuf, int8_t v);
extern int membuf_write_i16 (MemBuffer *mbuf, int16_t v);
extern int membuf_write_i32 (MemBuffer *mbuf, int32_t v);


#ifdef __cplusplus
}
#endif
#endif
