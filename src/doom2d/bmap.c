/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "bmap.h"

#include "common.h"
#include "error.h"
#include "map.h"
#include "view.h"


static uint8_t fld_need_remap = 1;
static uint8_t *bmap = NULL;
static int bmwidth = 0, bmheight = 0;


GCC_DESTRUCTOR_USER {
  if (bmap != NULL) free(bmap);
}


void BM_set_size (void) {
  bmwidth = (FLDW/4)+1;
  bmheight = (FLDH/4)+1;
  if ((bmap = realloc(bmap, bmwidth*bmheight)) == NULL) ERR_fatal("BM_set_size: out of memory");
  fld_need_remap = 1;
}


void BM_mark (const obj_t *o, uint8_t f) {
  int xs, ys, xe, ye;
  //
  if ((xs = (o->x-o->r)>>5) < 0) xs = 0;
  if ((xe = (o->x+o->r)>>5) >= bmwidth) xe = bmwidth-1;
  if ((ys = (o->y-o->h)>>5) < 0) ys = 0;
  if ((ye = (o->y>>5)) >= bmheight) ye = bmheight-1;
  //
  for (int y = ys; y <= ye; ++y) {
    if (y >= 0 && y < bmheight) {
      for (int x = xs; x <= xe; ++x) {
        if (x >= 0 && x < bmwidth) bmap[y*bmwidth+x] |= f;
      }
    }
  }
}


void BM_clear (uint8_t f) {
  f = ~f;
  for (int y = 0; y < bmheight; ++y) {
    for (int x = 0; x < bmwidth; ++x) {
      bmap[y*bmwidth+x] &= f;
    }
  }
}


int BM_get (int x, int y) {
  if ((x >>= 5) < 0 || x >= bmwidth || (y >>= 5) < 0 || y >= bmheight) return 0;
  return bmap[y*bmwidth+x];
}


//TODO: optimize!
void BM_remapfld (uint8_t clearbits) {
  const uint8_t *fld = &FIELD_MAP(0, 0);
  //
  BM_clear(clearbits|BM_WALL);
  for (int y = 0; y < FLDH; ++y) {
    for (int x = 0; x < FLDW; ++x, ++fld) {
      if (y/4 < bmheight && x/4 < bmwidth) {
        switch (*fld) {
          case TILE_WALL:
          case TILE_DOORC:
            bmap[(y/4)*bmwidth+(x/4)] |= BM_WALL;
            break;
        }
      }
    }
  }
  fld_need_remap = 0;
}


ccBool BM_need_remap (void) {
  return (fld_need_remap ? CC_TRUE : CC_FALSE);
}


void BM_queue_remap (void) {
  fld_need_remap = 1;
}


void BM_clear_maybe_remap (uint8_t f) {
  if (fld_need_remap) {
    BM_remapfld(f);
  } else {
    BM_clear(f);
  }
}
