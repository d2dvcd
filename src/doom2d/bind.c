/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "bind.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"

#include "common.h"
#include "player.h"


static const char *actnames[] = {
  "up",
  "down",
  "left",
  "right",
  "fire",
  "jump",
  "prev",
  "next",
  "use"
};


void bind_reset_player (int plnum) {
  if (plnum >= 0 && plnum <= 1) {
    for (int kid = 0; kid < KB_MAX; ++kid) {
      for (int alt = 0; alt <= 1; ++alt) {
        pl[plnum].kb[kid][alt] = 0;
      }
    }
  }
}


void bind_reset_all (void) {
  for (int plnum = 0; plnum <= 1; ++plnum) bind_reset_player(plnum);
}


void bind_save_all (FILE *fo) {
  for (int plnum = 0; plnum <= 1; ++plnum) {
    for (int alt = 0; alt <= 1; ++alt) {
      if (alt) fprintf(fo, "# player %d, alt\n", plnum+1); else fprintf(fo, "# player %d\n", plnum+1);
      for (int kid = 0; kid < KB_MAX; ++kid) {
        if (pl[plnum].kb[kid][alt] > 0) {
          const char *s = SDL_GetKeyName(pl[plnum].kb[kid][alt]);
          //
          if (s != NULL) {
            fprintf(fo, "bind %d%s %s ", plnum+1, (alt ? " alt" : ""), actnames[kid]);
            fputqstr(fo, s);
            fputc('\n', fo);
          }
        }
      }
    }
  }
}


static int *bind_find (int plnum, int alt, const char *name) {
  if (name != NULL && name[0] && plnum >= 0 && plnum <= 1) {
    alt = !!alt;
    for (int kid = 0; kid < KB_MAX; ++kid) {
      if (strcasecmp(actnames[kid], name) == 0) {
        return &(pl[plnum].kb[kid][alt]);
      }
    }
  }
  return NULL;
}


// bind plnum [alt] action keyname
// bind plnum reset
// bind reset
CONCMD(bind,
  "set or reset bindings:\n"
  "bind plnum [alt] action keyname\n"
  "bind plnum reset\n"
  "bind reset"
  "")
{
  int plnum, alt = 0, ano = 2;
  int *b;
  //
  CMDCON_HELP();
  //
  if (argc < 2 || argc > 5) {
invargs:
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if (argc == 2) {
    if (strcasecmp(argv[1], "reset") == 0) {
      bind_reset_all();
      return;
    }
    goto invargs;
  }
  //
  if ((plnum = cmdcon_parseint(argv[1], -1, 1, 2, NULL)) < 0) {
    conlogf("%s: invalid player number: '%s'!\n", argv[0], argv[1]);
    return;
  }
  --plnum; // 0 or 1
  //
  if (argc == 3) {
    if (strcasecmp(argv[2], "reset") == 0) {
      bind_reset_player(plnum);
      return;
    }
    goto invargs;
  }
  //
  if (strcasecmp(argv[2], "alt") == 0) {
    if (argc != 5) goto invargs;
    alt = 1;
    ++ano;
  }
  //
  if ((b = bind_find(plnum, alt, argv[ano++])) == NULL) {
    conlogf("%s: invalid action: '%s'!\n", argv[0], argv[ano-1]);
    return;
  }
  //
  for (char *p = argv[ano]; *p; ++p) if (*p == '_') *p = ' ';
  for (int f = 1; f < SDLK_LAST; ++f) {
    const char *s = SDL_GetKeyName(f);
    //
    if (s != NULL && strcasecmp(s, argv[ano]) == 0) {
      // key found
      *b = f;
      //conlogf("bind: plnum=%d; alt=%d; action=%s; key=%s; keycode=%u\n", plnum, alt, argv[ano-1], argv[ano], f);
      return;
    }
  }
  //
  conlogf("%s: invalid key name: '%s'!\n", argv[0], argv[ano]);
}
