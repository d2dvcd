/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "keyb.h"

#include "SDL.h"

#include "error.h"
#include "vga.h"


GameKeyCB k_key_proc = NULL;
// ������ ������: 0 - ��������, ����� - ������
Uint8 *keys = NULL;


void K_init (void) {
  keys = SDL_GetKeyState(NULL);
}


void K_done (void) {
}


// ���������� ������� ��������� ������
void K_setkeyproc (GameKeyCB k) {
  k_key_proc = k;
}


int K_is_any_key (int key) {
  static const int nokeys[] = {
    SDLK_NUMLOCK,
    SDLK_CAPSLOCK,
    SDLK_SCROLLOCK,
    SDLK_RSHIFT,
    SDLK_LSHIFT,
    SDLK_RCTRL,
    SDLK_LCTRL,
    SDLK_RALT,
    SDLK_LALT,
    SDLK_RMETA,
    SDLK_LMETA,
    SDLK_LSUPER,
    SDLK_RSUPER,
  };
  for (size_t f = 0; f < sizeof(nokeys)/sizeof(nokeys[0]); ++f) if (key == nokeys[f]) return 0;
  return (key > 0);
}
