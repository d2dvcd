/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "gamemode.h"

#include "common.h"
#include "netgame.h"


ccBool g_cheats_enabled = CC_FALSE;

ccBool gm_climb_stairs = CC_FALSE;
CONVAR_PROT_BOOL(gm_climb_stairs, gm_climb_stairs, CMDCON_FLAGS_PERSISTENT|CMDCON_FLAGS_SERVER, N_var_protector, "autoclimb stairs")

ccBool gm_new_cansee = CC_FALSE;
CONVAR_PROT_BOOL(gm_new_cansee, gm_new_cansee, CMDCON_FLAGS_PERSISTENT|CMDCON_FLAGS_SERVER, N_var_protector, "use new tracer in 'cansee'?")


ccBool g_paused = CC_FALSE;
CONVAR_PROT_BOOL_CB(g_paused, g_paused, CMDCON_FLAGS_NET_RO, N_var_protector, "is game paused?") {
  if (n_host != NULL) g_paused = 0;
}


ccBool g_no_monsters = CC_TRUE;
