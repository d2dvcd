/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include "membuffer.h"

#include <stdlib.h>
#include <string.h>


////////////////////////////////////////////////////////////////////////////////
struct MemBuffer {
  uint8_t *data;
  int used;
  int size;
  int freedata;
  int isreading;
  int error;
};


void membuf_write2read (MemBuffer *buf) {
  if (buf != NULL && !buf->isreading) {
    buf->size = buf->used;
    buf->used = 0;
    buf->isreading = 1;
  }
}


int membuf_write2readex (MemBuffer *buf, int sz) {
  if (buf != NULL && !buf->isreading && sz < buf->size) {
    buf->size = sz;
    buf->used = 0;
    buf->isreading = 1;
  }
  return -1;
}


int membuf_set_min_size (MemBuffer *buf, int sz) {
  if (buf != NULL && !buf->isreading) {
    if (sz > buf->size) {
      int newsz = (sz|0x7fff)+0x8001;
      uint8_t *nd = realloc(buf->data, newsz);
      //
      if (nd == NULL) return -1;
      memset(nd+buf->size, 0, newsz-buf->size);
      buf->data = nd;
      buf->size = newsz;
    }
    //
    return 0;
  }
  //
  return -1;
}


//FIXME: check for 'out of memory'
MemBuffer *membuf_new_write (void) {
  MemBuffer *buf = malloc(sizeof(*buf));
  //
  if (buf != NULL) {
    memset(buf, 0, sizeof(*buf));
    buf->size = 32768;
    if ((buf->data = calloc(buf->size, 1)) == NULL) {
      free(buf);
      buf = NULL;
    }
    buf->freedata = 1;
    buf->isreading = 0;
    buf->error = 0;
  }
  //
  return buf;
}


MemBuffer *membuf_new_read (const void *data, int datalen) {
  if (datalen <= 0 || data != NULL) {
    MemBuffer *buf = malloc(sizeof(*buf));
    //
    if (datalen < 0) datalen = 0;
    if (buf != NULL) {
      memset(buf, 0, sizeof(*buf));
      buf->size = datalen;
      buf->data = (void *)data;
      buf->freedata = 0;
      buf->isreading = 1;
      buf->error = 0;
    }
    //
    return buf;
  }
  //
  return NULL;
}


MemBuffer *membuf_new_read_free (const void *data, int datalen) {
  MemBuffer *buf = membuf_new_read(data, datalen);
  //
  if (buf != NULL) buf->freedata = 1;
  return buf;
}


MemBuffer *membuf_new_read_copy (const void *data, int datalen) {
  if (datalen <= 0 || data != NULL) {
    MemBuffer *buf = malloc(sizeof(*buf));
    //
    if (datalen < 0) datalen = 0;
    if (buf != NULL) {
      memset(buf, 0, sizeof(*buf));
      buf->size = datalen;
      if ((buf->data = malloc(buf->size+1)) == NULL) {
        free(buf);
        buf = NULL;
      } else {
        if (datalen > 0) memcpy(buf->data, data, datalen);
      }
      buf->freedata = 1;
      buf->isreading = 1;
      buf->error = 0;
    }
    //
    return buf;
  }
  //
  return NULL;
}


MemBuffer *membuf_new_read_from_file (FILE *fl, int size) {
  if (fl == NULL && size > 0) return NULL;
  if (size >= 0) {
    MemBuffer *buf = membuf_new_write();
    //
    if (buf != NULL) {
      if (size > 0) {
        if (membuf_set_min_size(buf, size) == 0) {
          if (fread(buf->data, size, 1, fl) == 1) {
            buf->isreading = 1;
            buf->size = size;
            //
            return buf;
          }
        }
      }
      membuf_free(buf);
    }
  }
  //
  return NULL;
}


void membuf_free (MemBuffer *buf) {
  if (buf != NULL) {
    if (buf->data != NULL && buf->freedata) free(buf->data);
    memset(buf, 0, sizeof(*buf));
    free(buf);
  }
}


void membuf_clear (MemBuffer *buf) {
  if (buf != NULL) {
    buf->used = 0;
    if (buf->isreading) buf->size = 0;
    buf->error = 0;
  }
}


void membuf_read_reset (MemBuffer *buf) {
  if (buf != NULL) {
    buf->used = 0;
    buf->error = 0;
  }
}


int membuf_write (MemBuffer *buf, const void *data, int datalen) {
  if (buf != NULL && !buf->isreading && !buf->error) {
    if (datalen > 0) {
      if (membuf_set_min_size(buf, buf->used+datalen) < 0) { buf->error = -1; return -1; }
      memmove(buf->data+buf->used, data, datalen);
      buf->used += datalen;
    }
    return (datalen >= 0 ? datalen : 0);
  }
  return -1;
}


int membuf_read (MemBuffer *buf, void *data, int datalen) {
  if (buf != NULL && buf->isreading && !buf->error) {
    if (datalen > 0) {
      if (buf->used+datalen > buf->size) {
        buf->error = -1;
        datalen = buf->size-buf->used;
      }
      if (datalen > 0) {
        memmove(data, buf->data+buf->used, datalen);
        buf->used += datalen;
      }
    }
    return (datalen >= 0 ? datalen : 0);
  }
  return -1;
}


int membuf_read_full (MemBuffer *buf, void *data, int datalen) {
  int res = membuf_read(buf, data, datalen);
  //
  if (datalen >= 0 && res >= 0 && res != datalen) res = -1;
  if (buf != NULL && res < 0 && !buf->error) buf->error = -1;
  return res;
}


int membuf_read_skip (MemBuffer *buf, int datalen) {
  if (buf != NULL && !buf->error && buf->isreading) {
    if (datalen > 0) {
      if (buf->used+datalen > buf->size) {
        buf->used = buf->size;
        buf->error = -1;
        return -1;
      }
      buf->used += datalen;
      return 0;
    }
  }
  return -1;
}


int membuf_used (const MemBuffer *buf) {
  return (buf != NULL ? buf->used : 0);
}


int membuf_size (const MemBuffer *buf) {
  return (buf != NULL ? buf->size : 0);
}


void *membuf_data (const MemBuffer *buf) {
  return (buf != NULL ? buf->data : NULL);
}


const void *membuf_data_const (const MemBuffer *buf) {
  return (buf != NULL ? buf->data : NULL);
}


int membuf_error (const MemBuffer *buf) {
  return (buf != NULL ? buf->error : -1);
}


void membuf_set_error (MemBuffer *buf, int err) {
  if (buf != NULL) buf->error = err;
}


static int mb_write_int (MemBuffer *mbuf, int64_t i, int bytes) {
  for (int f = 0; f < bytes; ++f) {
    uint8_t b = (i>>(f*8))&0xff;
    //
    if (membuf_write(mbuf, &b, 1) < 0) return -1;
  }
  return 0;
}


static int64_t mb_read_int (MemBuffer *mbuf, int bytes, int signext) {
  int64_t i = 0;
  //
  for (int f = 0; f < bytes; ++f) {
    uint8_t b;
    //
    if (membuf_read(mbuf, &b, 1) != 1) {
      mbuf->error = -1;
      return 0; // fixme
    }
    i |= ((int64_t)b)<<(f*8);
  }
  //
  if (signext && (i&(1<<(bytes*8-1))) != 0) {
    // sign extend
    for (int f = bytes; f < sizeof(i); ++f) i |= 0xff<<(f*8);
  }
  //
  return i;
}


int membuf_read_bool (MemBuffer *mbuf) { return (mb_read_int(mbuf, 1, 0) != 0); }
uint8_t membuf_read_ui8 (MemBuffer *mbuf) { return mb_read_int(mbuf, 1, 0); }
uint16_t membuf_read_ui16 (MemBuffer *mbuf) { return mb_read_int(mbuf, 2, 0); }
uint32_t membuf_read_ui32 (MemBuffer *mbuf) { return mb_read_int(mbuf, 4, 0); }
int8_t membuf_read_i8 (MemBuffer *mbuf) { return mb_read_int(mbuf, 1, 1); }
int16_t membuf_read_i16 (MemBuffer *mbuf) { return mb_read_int(mbuf, 2, 1); }
int32_t membuf_read_i32 (MemBuffer *mbuf) { return mb_read_int(mbuf, 4, 1); }

int membuf_write_bool (MemBuffer *mbuf, int v) { return mb_write_int(mbuf, !!v, 1); }
int membuf_write_ui8 (MemBuffer *mbuf, uint8_t v) { return mb_write_int(mbuf, v, 1); }
int membuf_write_ui16 (MemBuffer *mbuf, uint16_t v) { return mb_write_int(mbuf, v, 2); }
int membuf_write_ui32 (MemBuffer *mbuf, uint32_t v) { return mb_write_int(mbuf, v, 4); }
int membuf_write_i8 (MemBuffer *mbuf, int8_t v) { return mb_write_int(mbuf, v, 1); }
int membuf_write_i16 (MemBuffer *mbuf, int16_t v) { return mb_write_int(mbuf, v, 2); }
int membuf_write_i32 (MemBuffer *mbuf, int32_t v) { return mb_write_int(mbuf, v, 4); }
