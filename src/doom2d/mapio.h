/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_MAPIO_H
#define D2D_MAPIO_H

#include <setjmp.h>
#include <stdio.h>

#include "common.h"
#include "files.h"
#include "map.h"
#include "membuffer.h"


typedef int (*F_maploadCB) (MemBuffer *mbuf, const map_block_t *blk); // <0 for error; mbuf and blk is NULL: postprocessing phase
typedef int (*F_mapsaveCB) (MemBuffer *mbuf, map_block_t *blk); // <0 for error; can set 'st'; no need to set 'sz'

extern void F_registerMapLoad (uint16_t type, F_maploadCB lcb, F_mapsaveCB scb);


extern int F_loadmap (const char *name); // !0 (<0): error


#endif
