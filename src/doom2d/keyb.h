/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_MYKEYB_H
#define D2D_MYKEYB_H

#include "SDL.h"


// ��� ������� ��������� ������
typedef void (*GameKeyCB) (SDL_KeyboardEvent *key, int pressed);

// ������ ������: 0 - ��������, ����� - ������
extern Uint8 *keys;
extern GameKeyCB k_key_proc;


extern void K_init (void);
extern void K_done (void);

// ���������� ������� ��������� ������
extern void K_setkeyproc (GameKeyCB k);

extern int K_is_any_key (int key);


#endif
