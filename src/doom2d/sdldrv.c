/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "sdldrv.h"

#include "vga.h"


////////////////////////////////////////////////////////////////////////////////
//FrameCB frameCB = NULL;
KeyCB keyCB = NULL;
MouseCB mouseCB = NULL;
MouseButtonCB mouseButtonCB = NULL;
WindowActivationCB windowActivationCB = NULL;


////////////////////////////////////////////////////////////////////////////////
int vidWindowActivated = 1;


////////////////////////////////////////////////////////////////////////////////
/*
void forcedShowFrame (void) {
  if (frameSfc != NULL) {
    frameSfcRaped = 1;
    unlockSurface(&frameSfcLock);
    screenFlip();
    lockSurface(&frameSfcLock, screen);
  }
}


void buildFrame (void) {
  frameSfcRaped = 0;
  if (frameCB != NULL) {
    lockSurface(&frameSfcLock, screen);
    frameSfc = screen;
    frameCB();
    frameSfc = NULL;
    unlockSurface(&frameSfcLock);
  }
  if (!frameSfcRaped) screenFlip();
}
*/


#if 0
static Uint32 cbFrameTimer (Uint32 interval, void *param) {
  SDL_Event evt;
  //
  evt.type = SDL_USEREVENT;
  /*
  //evt.user.type = SDL_USEREVENT;
  evt.user.code = 0;
  evt.user.data1 = NULL;
  evt.user.data2 = NULL;
  */
  SDL_PushEvent(&evt);
  //fprintf(stderr, "!\n");
  return interval;
}
#endif


////////////////////////////////////////////////////////////////////////////////
void postQuitMessage (void) {
  SDL_Event evt;
  //
  evt.type = SDL_QUIT;
  SDL_PushEvent(&evt);
}


////////////////////////////////////////////////////////////////////////////////
int msButtons = 0, msLastX = 0, msLastY = 0, kbCtrl = 0, kbAlt = 0, kbShift = 0, kbSuper = 0;
int kbLCtrl = 0, kbRCtrl = 0, kbLAlt = 0, kbRAlt = 0, kbLShift = 0, kbRShift = 0, kbLSuper = 0, kbRSuper = 0;
int msAutoTimeMSFirst = 500;
int msAutoTimeMSRepeat = 100;
int msAutoTresholdX = 2;
int msAutoTresholdY = 2;


typedef struct {
  int64_t time;
  int x, y;
  int repeat;
} MSPressInfo;

static MSPressInfo msbi[5]; // left,right,middle


typedef struct {
  SDLKey key;
  int *var;
} KBCtrlKeyInfo;


static const KBCtrlKeyInfo kbCtrlKeys[] = {
  {.key=SDLK_LCTRL, .var=&kbLCtrl},
  {.key=SDLK_RCTRL, .var=&kbRCtrl},
  {.key=SDLK_LALT, .var=&kbLAlt},
  {.key=SDLK_RALT, .var=&kbRAlt},
  {.key=SDLK_LSHIFT, .var=&kbLShift},
  {.key=SDLK_RSHIFT, .var=&kbRShift},
  {.key=SDLK_LSUPER, .var=&kbLSuper},
  {.key=SDLK_RSUPER, .var=&kbRSuper},
};


__attribute__((constructor(666))) static void initEventSystem (void) {
  memset(msbi, 0, sizeof(msbi));
  //for (size_t f = 0; f < sizeof(kbCtrlKeys)/sizeof(kbCtrlKeys[0]); ++f) *(kbCtrlKeys[f].var) = 0;
  //kbCtrl = kbAlt = kbShift = kbSuper = 0;
}


static void fixKbCtrl (SDLKey key, int isdown) {
  for (size_t f = 0; f < sizeof(kbCtrlKeys)/sizeof(kbCtrlKeys[0]); ++f) {
    if (key == kbCtrlKeys[f].key) *(kbCtrlKeys[f].var) = isdown;
  }
  kbCtrl = kbLCtrl||kbRCtrl;
  kbAlt = kbLAlt||kbRAlt;
  kbShift = kbLShift||kbRShift;
  kbSuper = kbLSuper||kbRSuper;
}


static void kbDepressCtrls (void) {
  for (unsigned int f = 0; f < sizeof(kbCtrlKeys)/sizeof(KBCtrlKeyInfo); ++f) {
    if (*(kbCtrlKeys[f].var)) {
      fixKbCtrl(kbCtrlKeys[f].key, 0);
      if (keyCB) {
        SDL_Event event;
        //
        event.type = SDL_KEYUP;
        event.key.state = SDL_RELEASED;
        event.key.keysym.scancode = 0;
        event.key.keysym.unicode = 0;
        event.key.keysym.sym = kbCtrlKeys[f].key;
        event.key.keysym.mod =
          (kbLCtrl ? KMOD_LCTRL : 0) |
          (kbRCtrl ? KMOD_RCTRL : 0) |
          (kbLAlt ? KMOD_LALT : 0) |
          (kbRAlt ? KMOD_RALT : 0) |
          (kbLShift ? KMOD_LSHIFT : 0) |
          (kbRShift ? KMOD_RSHIFT : 0) |
          0;
        keyCB(&event.key);
      }
    }
  }
}


static void doAutoMouse (void) {
  if (mouseButtonCB && msAutoTimeMSFirst > 0) {
    int64_t t = SDL_GetTicks();//timerGetMS();
    //
    for (int b = 0; b < 3; ++b) {
      int n = (1<<b);
      //
      if (msbi[n].time > 0) {
        if (!msbi[n].repeat && (abs(msLastX-msbi[n].x) > msAutoTresholdX || abs(msLastY-msbi[n].y) > msAutoTresholdY)) {
          // mouse moved too far, stop autorepeating
          msbi[n].time = 0;
        } else if ((msbi[n].repeat && t-msbi[n].time >= msAutoTimeMSRepeat) ||
                   (!msbi[n].repeat && t-msbi[n].time >= msAutoTimeMSFirst)) {
          // generate repeat
          msbi[n].time = t;
          msbi[n].repeat = 1;
          mouseButtonCB(msLastX, msLastY, (n|MS_BUTTON_AUTO), msButtons);
        }
      }
    }
  }
}


// -1: quit; 1: new frame
SDLDrvEventRes processEvents (int dowait) {
  SDL_Event event;
  //
  while ((dowait ? SDL_WaitEvent(&event) : SDL_PollEvent(&event))) {
    switch (event.type) {
      case SDL_QUIT: // the user want to quit
        return SDLDRV_ER_QUIT;
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        fixKbCtrl(event.key.keysym.sym, (event.type == SDL_KEYDOWN));
        if (keyCB) keyCB(&event.key);
        break;
      case SDL_MOUSEMOTION: {
        //int buttons;
        int x = event.motion.x, y = event.motion.y, rx = event.motion.xrel, ry = event.motion.yrel;
        //
        if (screenReal != NULL && screenReal->w > screen->w) { x /= 2; y /= 2; }
        msLastX = x;
        msLastY = y;
        //SDLMB2VMB(event.motion.state);
        //msButtons = buttons;
        if (mouseCB) mouseCB(x, y, rx, ry, msButtons);
        break; }
      case SDL_MOUSEBUTTONUP:
      case SDL_MOUSEBUTTONDOWN: {
        int x = event.button.x, y = event.button.y, btn = 0;
        //
        if (screenReal != NULL && screenReal->w > screen->w) { x /= 2; y /= 2; }
        msLastX = x;
        msLastY = y;
        switch (event.button.button) {
          case SDL_BUTTON_LEFT: btn = MS_BUTTON_LEFT; break;
          case SDL_BUTTON_RIGHT: btn = MS_BUTTON_RIGHT; break;
          case SDL_BUTTON_MIDDLE: btn = MS_BUTTON_MIDDLE; break;
          case SDL_BUTTON_WHEELUP: btn = MS_BUTTON_WHEELUP; break;
          case SDL_BUTTON_WHEELDOWN: btn = MS_BUTTON_WHEELDOWN; break;
          default: ;
        }
        //fprintf(stderr, "DOWN:%d; btn:%d; bt:%02x\n", (event.button.state == SDL_PRESSED), btn, msButtons);
        if (btn != 0) {
          if (event.button.state == SDL_PRESSED) {
            msButtons |= btn;
            if (btn <= MS_BUTTON_MIDDLE) {
              msbi[btn].time = SDL_GetTicks();//timerGetMS();
              msbi[btn].x = x;
              msbi[btn].y = y;
              msbi[btn].repeat = 0;
            }
          } else {
            msButtons &= ~btn;
            btn |= MS_BUTTON_DEPRESSED;
            if (btn <= MS_BUTTON_MIDDLE) msbi[btn].time = 0;
          }
          if (mouseButtonCB) mouseButtonCB(x, y, btn, msButtons);
        }
        break; }
      case SDL_ACTIVEEVENT:
        if (event.active.gain) {
          vidWindowActivated = 1;
          if (windowActivationCB != NULL) windowActivationCB(1);
        } else {
          vidWindowActivated = 0;
          SDL_WM_GrabInput(SDL_GRAB_OFF);
          // just in case
          kbDepressCtrls();
          if (msButtons != 0) {
            if (mouseButtonCB != NULL) {
              for (unsigned int mask = 0x01; mask < MS_BUTTON_DEPRESSED; mask <<= 1) {
                if (msButtons&mask) {
                  msButtons &= ~mask;
                  mouseButtonCB(msLastX, msLastY, mask|MS_BUTTON_DEPRESSED, msButtons);
                }
              }
            }
            msButtons = 0;
          }
          if (windowActivationCB != NULL) windowActivationCB(0);
        }
        break;
      case SDL_USEREVENT:
        //while (SDL_PollEvent(NULL)) processEvents();
        doAutoMouse();
        return SDLDRV_ER_USER_EVENT;
      default: ;
    }
  }
  //
  doAutoMouse();
  return SDLDRV_ER_OK;
}


////////////////////////////////////////////////////////////////////////////////
/*
void mainLoop (void) {
  SDL_TimerID frameTimer = SDL_AddTimer(20, cbFrameTimer, NULL);
  int eres = 1;
  //
  while (eres >= 0) {
    if (eres > 0) buildFrame();
    eres = processEvents(1);
  }
  //
  SDL_RemoveTimer(frameTimer);
}
*/
