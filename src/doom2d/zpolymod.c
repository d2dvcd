typedef struct PolyPoint {
  int dy; // vertical line direction; used for 'winding' rule
  int next; // -1: none
  int x;
} PolyPoint;


static int ppool_size = 0;
static int ppool_pos = 0;
static PolyPoint *ppool = NULL;
static int pscans[4096]; // index of the first point of each scan; -1: none
static int firstX, firstY;
static int lastX, lastY;
static int vertex_count; // total number of added polygon vertex (not rasterized!)


void polymod_init (void) {
  for (int f = 0; f < SCRH; ++f) pscans[f] = -1;
  if (ppool != NULL) free(ppool);
  ppool = NULL;
  ppool_size = 0;
  ppool_pos = 0;
  vertex_count = 0;
}


void polymod_deinit (void) {
  if (ppool != NULL) free(ppool);
  ppool = NULL;
  ppool_size = 0;
  ppool_pos = 0;
  vertex_count = 0;
}


static inline int ppoint_alloc (int x, int next, int dy) {
  if (ppool_pos+1 > ppool_size) {
    int newsz = ppool_size+8192;
    PolyPoint *nn = realloc(ppool, sizeof(PolyPoint)*newsz);
    //
    if (nn == NULL) ERR_fatal("out of memory in polymod");
    ppool = nn;
    ppool_size = newsz;
  }
  ppool[ppool_pos].x = x;
  ppool[ppool_pos].next = next;
  ppool[ppool_pos].dy = dy;
  return ppool_pos++;
}


static inline void ppoint_insert (int x, int y, int dy) {
  // we don't need offscreen points
  if (y >= w_viewofs_vert && y < w_viewofs_vert+w_view_hgt) {
    int n;
    //
    if ((n = pscans[y]) != -1) {
      int p;
      //
      // there will be no huge number of points, so do linear lookup to keep points x-sorted
      for (p = -1; n != -1 && x > ppool[n].x; p = n, n = ppool[n].next) ;
      //
      if (p == -1) {
        // this is new first point
        pscans[y] = ppoint_alloc(x, n, dy);
      } else {
        // insert new point after p; n is the next point
        ppool[p].next = ppoint_alloc(x, n, dy);
      }
    } else {
      // first
      pscans[y] = ppoint_alloc(x, -1, dy);
    }
  }
}


// vertical rasterization
static void line_dda (int x0, int y0, int x1, int y1) {
  if_expect (y0 == y1, 0) return; // horizontal line
  //if ((y0 < 0 && y1 < 0) || (y0 >= SCRH && y1 >= SCRH)) return; // out-of-screen lines aren't interesting
  //
  if_expect (x0 == x1, 0) {
    // straight vertical line
    if (y0 < y1) {
      for (; y0 < y1; ++y0) ppoint_insert(x0, y0, 1);
    } else {
      for (--y0; y0 >= y1; --y0) ppoint_insert(x0, y0, -1);
    }
  } else {
    // draw sloped
    float x = x0;
    //
    if (y0 < y1) {
      float dx = (float)(x1-x0)/(float)(y1-y0);
      //
      for (; y0 < y1; ++y0) { ppoint_insert(x, y0, 1); x += dx; }
    } else {
      float dx = (float)(x1-x0)/(float)(y0-y1);
      //
      for (--y0; y0 >= y1; --y0) { ppoint_insert(x, y0, -1); x += dx; }
    }
  }
}


static void polymod_start (void) {
  for (int f = 0; f < SCRH; ++f) pscans[f] = -1;
  ppool_pos = 0;
  vertex_count = 0;
}


void polymod_add_vertex (int x, int y) {
  if_expect (vertex_count > 0, 1) {
    if (x == lastX && y == lastY) return;
    line_dda(lastX, lastY, x, y);
  } else {
    firstX = x;
    firstY = y;
  }
  lastX = x;
  lastY = y;
  ++vertex_count;
}


static void polymod_end (void) {
  if_expect (vertex_count >= 3, 1) {
    line_dda(lastX, lastY, firstX, firstY);
  } else {
    for (int f = 0; f < SCRH; ++f) pscans[f] = -1;
  }
}


/*
static void line_draw (int x0, int y0, int x1, int y1) {
  int dx =  abs(x1-x0), sx = (x0 < x1 ? 1 : -1);
  int dy = -abs(y1-y0), sy = (y0 < y1 ? 1 : -1);
  int err = dx+dy, e2; // error value e_xy
  //
  for (;;) {
    //if (x0 == x1 && y0 == y1) break; // break first, so last point is not drawn
    mixPixelL(x0, y0);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } // e_xy+e_x > 0
    if (e2 <= dx) { err += dx; y0 += sy; } // e_xy+e_y < 0
  }
}
*/


#define POLYMOD_DO_PIXEL()  do { \
  int dst = sqrttbl[(x-lcx)*(x-lcx)+y0]; \
  if (dst >= z_light_mindist) { \
    int a = _alpha-(dst*_alpha/lrad); \
    if_expect (a > 0 && *d != LMAP_FULLLIT, 1) { \
      if (a > 255) a = 255; \
      if_expect (*d == LMAP_NORMAL, 1) { \
        lb_add_rgb(x, y, _bgr, a); \
      }/* else if (*d == LMAP_BACK) { \
        if (_bgr != 0) putPixelMixU(x, y, _r, _g, _b, dst); \
      }*/ \
    } \
  } \
} while (0)


static void polymod_fill (void) {
  if (vertex_count < 3) return;
  //
  /*
  if (vertex_count == 1) { mixPixelL(firstX, firstY); return; }
  if (vertex_count == 2) { line_draw(firstX, firstY, lastX, lastY); return; }
  */
  //
  for (int y = w_viewofs_vert; y < w_viewofs_vert+w_view_hgt; ++y) {
    int p = pscans[y], y0 = (y-lcy)*(y-lcy);
    //
    //if (y >= SCRH) break;
    if (p >= 0) {
      int wind = ppool[p].dy, x = ppool[p].x;
      //
      for (p = ppool[p].next; p != -1; p = ppool[p].next) {
        int ex = ppool[p].x;
        //
        if (wind != 0) {
          int ee = (ex < w_view_wdt-1 ? ex : w_view_wdt-2);
          //const Uint8 *vs;
          const Uint8 *d;
          //
          if_expect (x < 0, 0) x = 0;
          d = scrlightmap+SCRW*y+x;
          //vs = vscrbuf+SCRW*y+x;
          while (x++ <= ee) {
            //++vs;
            ++d;
            POLYMOD_DO_PIXEL();
          }
          x = ex;
        } else {
          x = ex;
          if (ppool[p].dy < 0 && x >= 0 && x < w_view_wdt) {
            //const Uint8 *vs = vscrbuf+SCRW*y+x;
            const Uint8 *d = scrlightmap+SCRW*y+x;
            //
            POLYMOD_DO_PIXEL();
          }
        }
        //
        wind += ppool[p].dy;
      }
    }
  }
}
