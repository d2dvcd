/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "files.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>

#include "error.h"
#include "common.h"
#include "cmdcon.h"
#include "items.h"
#include "map.h"
#include "misc.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "view.h"


////////////////////////////////////////////////////////////////////////////////
jmp_buf flJmpBuf;


void flError (int errCode) {
  longjmp(flJmpBuf, errCode);
}


void flReadBuf (FILE *fl, void *buf, size_t buflen) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (buflen > 0) {
    if (buf == NULL) flError(FL_OTHER_ERROR);
    if (fread(buf, buflen, 1, fl) != 1) flError(FL_READ_ERROR);
  }
}


void flWriteBuf (FILE *fl, const void *buf, size_t buflen) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (buflen > 0) {
    if (buf == NULL) flError(FL_OTHER_ERROR);
    if (fwrite(buf, buflen, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


ccBool flReadBool (FILE *fl) {
  uint8_t res;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fread(&res, 1, 1, fl) != 1) flError(FL_READ_ERROR);
  return (res ? CC_TRUE : CC_FALSE);
}


uint8_t flReadUI8 (FILE *fl) {
  uint8_t res;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fread(&res, 1, 1, fl) != 1) flError(FL_READ_ERROR);
  return res;
}


uint16_t flReadUI16 (FILE *fl) {
  uint16_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((uint16_t)b)<<(f*8));
  }
  return res;
}


uint32_t flReadUI32 (FILE *fl) {
  uint32_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((uint32_t)b)<<(f*8));
  }
  return res;
}


int8_t flReadI8 (FILE *fl) {
  int8_t res;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fread(&res, 1, 1, fl) != 1) flError(FL_READ_ERROR);
  return res;
}


int16_t flReadI16 (FILE *fl) {
  int16_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((int16_t)b)<<(f*8));
  }
  return res;
}


int32_t flReadI32 (FILE *fl) {
  int32_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((int32_t)b)<<(f*8));
  }
  return res;
}


void flWriteBool (FILE *fl, ccBool v) {
  uint8_t b;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  b = (v ? 255 : 0);
  if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
}


void flWriteUI8 (FILE *fl, uint8_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fwrite(&v, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
}


void flWriteUI16 (FILE *fl, uint16_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


void flWriteUI32 (FILE *fl, uint32_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


void flWriteI8 (FILE *fl, int8_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fwrite(&v, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
}


void flWriteI16 (FILE *fl, int16_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


void flWriteI32 (FILE *fl, int32_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  uint32_t ofs;
  uint32_t size;
  char n[9];
} wad_t;


////////////////////////////////////////////////////////////////////////////////
#define MAX_WADS  (20)
#define MAX_WAD   (4096)


typedef struct GCC_PACKED {
  uint32_t ofs;
  uint32_t size;
  char n[9]; // asciiz
  int fidx; // file index in wadh[]
} mwad_t;


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  uint8_t n, i, v, d;
} dmv;


static char *wads[MAX_WADS];
static int wad_num;

static mwad_t wad[MAX_WAD];

//int d_start, d_end;
static int m_start, m_end;
static int s_start, s_end;

static FILE *wadh[MAX_WADS];


////////////////////////////////////////////////////////////////////////////////
void F_startup (void) {
  conlogf("F_startup: initializing file system");
  wad_num = 0;
  for (size_t f = 0; f < sizeof(wads)/sizeof(wads[0]); ++f) wads[f] = NULL;
}


//FIXME: buffer overflow
//FIXME: '../../../', etc
static char *findWAD (const char *fname) {
  static const char *pathlist[] = {
    "/usr/share/doom2d-vcd"
    "/usr/local/share/doom2d-vcd"
  };
  char *res = NULL;
  //
  char *tryfile (const char *path, const char *path1) {
    const char *ext = file_get_ext(fname);
    char *fn;
    //
    if (path1 == NULL) path1 = "";
    if (ext == NULL || (strcasecmp(ext, ".wad") != 0 && strcasecmp(ext, ".lmp") != 0)) ext = ".wad"; else ext = "";
    fn = Z_sprintf("%s%s/%s%s", path, path1, fname, ext);
    //
    //dlogf("trying [%s]", fn);
    if (fexists(fn)) return fn;
    for (char *p = fn+strlen(path)+strlen(path1); *p; ++p) *p = toupper(*p);
    //dlogf("trying [%s]", fn);
    if (fexists(fn)) return fn;
    free(fn);
    return NULL;
  }
  //
  if (fname == NULL || !fname[0]) return NULL;
  if (fname[0] == '/' && strrchr(fname, '.') != NULL &&
      (strcasecmp(strrchr(fname, '.'), ".wad") == 0 || strcasecmp(strrchr(fname, '.'), ".lmp") == 0) &&
      fexists(fname)) {
    return strdup(fname);
  }
  // try '$BINDIR/data/<fname>'
  if ((res = tryfile(get_bin_dir(), "/data")) != NULL) return res;
  if ((res = tryfile(get_bin_dir(), "")) != NULL) return res;
  //
  for (size_t f = 0; f < sizeof(pathlist)/sizeof(pathlist[0]); ++f) {
    if ((res = tryfile(pathlist[f], NULL)) != NULL) return res;
  }
  //
#ifndef _WIN32
  return tryfile(getenv("HOME"), "/.doom2d-vcd");
#else
  return NULL;
#endif
}


void F_addwad (const char *fn, int required) {
  for (int i = 0; i < MAX_WADS; ++i) {
    if (wads[i] == NULL) {
      char *diskname = findWAD(fn);
      //
      if (diskname == NULL) break;
      wads[i] = diskname;
      return;
    }
  }
  //
  if (required) ERR_failinit("Can't find WAD: '%s'", fn);
}


static long myfilelength (FILE *fl) {
  long size, pos = ftell(fl);
  //
  fseek (fl, 0, SEEK_END);
  size = ftell(fl);
  fseek (fl, pos, SEEK_SET);
  //
  return size;
}


static void flReadWadT (FILE *fl, wad_t *w) {
  memset(w->n, 0, 9);
  w->ofs = flReadUI32(fl);
  w->size = flReadUI32(fl);
  flReadBuf(fl, w->n, 8);
}


// build wad directory
void F_initwads (void) {
  int k, p;
  FILE *fl;
  char s[4];
  uint32_t n, ofs, j;
  wad_t w;
  //
  conlogf("F_initwads: initializing WAD files");
  for (int f = 0; f < MAX_WAD; ++f) wad[f].n[0] = 0;
  //
  conlogf(" adding %s", wads[0]);
  if ((wadh[0] = fl = fopen(wads[0], "rb")) == NULL) {
    ERR_failinit("Can't open WAD: '%s'", wads[0]);//sys_errlist[errno]);
  }
  *s = 0;
  flReadBuf(fl, s, 4);
  if (strncmp(s, "IWAD", 4) != 0 && strncmp(s, "PWAD", 4) != 0) {
    ERR_failinit("Invalid WAD signature in '%s'", wads[0]);
  }
  n = flReadUI32(fl);
  ofs = flReadUI32(fl);
  if (ofs < 12) ERR_failinit("Invalid WAD: '%s'", wads[0]);
  fseek(fl, ofs, SEEK_SET);
  for (j = 0, p = 0; j < n; ++j) {
    flReadWadT(fl, &w);
    if (p >= MAX_WAD) ERR_failinit("Too many entries in WAD: '%s'", wads[0]);
    memcpy(wad[p].n, w.n, 9);
    wad[p].ofs = w.ofs;
    wad[p].size = w.size;
    wad[p].fidx = 0;
    ++p;
  }
  //fclose(fl);
  for (int f = 1; f < MAX_WADS; ++f) {
    if (wads[f] != NULL) {
      const char *ext;
      //
      conlogf(" adding %s", wads[f]);
      if ((wadh[f] = fl = fopen(wads[f], "rb")) == NULL) {
        ERR_failinit("Can't open WAD: '%s'", wads[f]);//sys_errlist[errno]);
      }
      ext = file_get_ext(wads[f]);
      if (ext != NULL && strcasecmp(ext, ".lmp") == 0) {
        const char *name;
        int namelen;
        char name8[9];
        //
        name = file_get_name_only(wads[f], &namelen);
        if (name == NULL || namelen == 0 || namelen > 8) ERR_failinit("Invalid lump name: '%s'", wads[f]);
        //
        memset(name8, 0, sizeof(name8));
        memcpy(name8, name, namelen);
        //
        for (k = 0; k < MAX_WAD; ++k) {
          if (strncasecmp(wad[k].n, name8, 8) == 0) {
            wad[k].ofs = 0L;
            wad[k].size = myfilelength(fl);
            wad[k].fidx = f;
            break;
          }
        }
        if (k >= MAX_WAD) {
          if (p >= MAX_WAD) ERR_failinit("Too many entries in WAD: '%s'", wads[f]);
          memset(wad[p].n, 0, 8);
          strncpy(wad[p].n, name8, 8);
          wad[p].ofs = 0L;
          wad[p].size = myfilelength(fl);
          wad[p].fidx = f;
          ++p;
        }
        continue;
      }
      *s = 0;
      flReadBuf(fl, s, 4);
      if (strncmp(s, "IWAD", 4) != 0 && strncmp(s, "PWAD", 4) != 0) {
        ERR_failinit("Invalid WAD signature in '%s'", wads[f]);
      }
      n = flReadI32(fl);
      ofs = flReadI32(fl);
      if (n < 0 || ofs < 12) ERR_failinit("Invalid WAD: '%s'", wads[f]);
      fseek(fl, ofs, SEEK_SET);
      for (j = 0; j < n; ++j) {
        flReadWadT(fl, &w);
        for (k = 0; k < MAX_WAD; ++k) {
          if (strncasecmp(wad[k].n, w.n, 8) == 0) {
            wad[k].ofs = w.ofs;
            wad[k].size = w.size;
            wad[k].fidx = f;
            break;
          }
        }
        if (k >= MAX_WAD) {
          if (p >= MAX_WAD) ERR_failinit("Too many entries in WAD: '%s'", wads[f]);
          memcpy(wad[p].n, w.n, 9);
          wad[p].ofs = w.ofs;
          wad[p].size = w.size;
          wad[p].fidx = f;
          ++p;
        }
      }
    }
  }
  wad_num = p;
}


// allocate resources
// (called from M_startup)
void F_allocres (void) {
  /*
  d_start = F_getresid("D_START");
  d_end = F_getresid("D_END");
  */
  m_start = F_getresid("M_START");
  m_end = F_getresid("M_END");
  if (m_start > m_end) ERR_fatal("invalid WAD file(s): M_START/M_END");
  s_start = F_getresid("S_START");
  s_end = F_getresid("S_END");
  if (s_start > s_end) ERR_fatal("invalid WAD file(s): S_START/S_END");
}


// load resource
void F_loadres (int r, void *p, uint32_t ofs, uint32_t size) {
  long oo;
  FILE *fh;
  //
  oo = ftell(fh = wadh[wad[r].fidx]);
  if (fseek(fh, wad[r].ofs+ofs, SEEK_SET) != 0) ERR_fatal("File seeking error");
  if ((uint32_t)fread(p, 1, size, fh) != size) ERR_fatal("Resource loading error: %.8s", wad[r].n);
  fseek(fh, oo, SEEK_SET);
}


MemBuffer *F_loadres2buf (int id) {
  if (id >= 0 && id < wad_num) {
    long oo;
    FILE *fh;
    MemBuffer *buf;
    //
    oo = ftell(fh = wadh[wad[id].fidx]);
    if (fseek(fh, wad[id].ofs, SEEK_SET) != 0) return NULL;
    buf = membuf_new_read_from_file(fh, wad[id].size);
    fseek(fh, oo, SEEK_SET);
    //
    return buf;
  }
  //
  return NULL;
}


// load resource
void *F_loadres1 (int idx, int *size) {
  int sz = wad[idx].size;
  void *res = malloc(sz+1);
  //
  //dlogf("F_loadres1: idx=%d", idx);
  F_loadres(idx, res, 0, sz);
  if (size != NULL) *size = sz;
  //dlogf("F_loadres1: idx=%d (%d); OK", idx, (int)ftell(fo));
  return res;
}


// get resource id
int F_getresid (const char *n) {
  for (int f = wad_num-1; f >= 0; --f) if (strncasecmp(wad[f].n, n, 8) == 0) return f;
  ERR_fatal("F_getresid: resource not found: %.8s", n);
  return -1;
}


// get resource id
int F_findres (const char *n) {
  for (int f = wad_num-1; f >= 0; --f) if (strncasecmp(wad[f].n, n, 8) == 0) return f;
  return -1;
}


void F_getresname (char *n, int r) {
  memcpy(n, wad[r].n, 8);
}


// get sprite id
int F_getsprid (const char n[4], int s, int d) {
  s += 'A';
  d += '0';
  for (int f = s_start+1; f < s_end; ++f) {
    if (strncasecmp(wad[f].n, n, 4) == 0 && (wad[f].n[4] == s || wad[f].n[6] == s)) {
      uint8_t a = 0, b = 0;
      //
      if (wad[f].n[4] == s) a = wad[f].n[5];
      if (wad[f].n[6] == s) b = wad[f].n[7];
      if (a == '0') return f;
      if (b == '0') return (f|0x8000);
      if (a == d) return f;
      if (b == d) return (f|0x8000);
    }
  }
  ERR_fatal("F_getsprid: image %.4s%c%c not found", n, (uint8_t)s, (uint8_t)d);
  return -1;
}


int F_getreslen (int r) {
  return wad[r].size;
}


void F_nextmus (char *s) {
  int f = F_findres(s);
  //
  if (f <= m_start || f >= m_end) f = m_start;
  for (++f; ; ++f) {
    if (f >= m_end) f = m_start+1;
    if (strcasecmp(wad[f].n, "MENU") == 0 ||
        strcasecmp(wad[f].n, "INTERMUS") == 0 ||
        strcasecmp(wad[f].n, "\x8a\x8e\x8d\x85\x96\x0") == 0) {
      continue;
    }
    if (strncasecmp(wad[f].n, "DMI", 3) != 0) break;
  }
  memcpy(s, wad[f].n, 8);
}


void F_randmus (char *s) {
  int n = myrand1(10);
  //
  for (int f = 0; f < n; f++) F_nextmus(s);
}


////////////////////////////////////////////////////////////////////////////////
static ccBool m_active = CC_FALSE;

static void *resp[MAX_WAD];
static int resl[MAX_WAD];


void M_startup (void) {
  if (!m_active) {
    conlogf("M_startup: initializing memory areas");
    memset(resp, 0, sizeof(resp));
    memset(resl, 0, sizeof(resl));
    m_active = CC_TRUE;
  }
}


void M_shutdown (void) {
  if (m_active) {
    m_active = CC_FALSE;
  }
}


// no need to treat DS_xxx as 'specials'
static void allocres (int h) {
  int *p;
  //
  if (!(p = malloc(wad[h].size+4))) ERR_fatal("M_lock: out of memory");
  *p = h;
  ++p;
  resp[h] = p;
  F_loadres(h, p, 0, wad[h].size);
}


void *M_lock (int h) {
  if (h == -1 || h == 0xFFFF) return NULL;
  h &= ~0x8000;
  if (h >= MAX_WAD) ERR_fatal("M_lock: invalid resource index");
  if (!resl[h] && !resp[h]) allocres(h);
  ++resl[h];
  return resp[h];
}


void M_unlock (void *p) {
  if (p != NULL) {
    int *pp = ((int *)p)-1;
    int h = *pp;
    //
    if (h >= MAX_WAD) ERR_fatal("M_unlock: invalid resource index");
    if (!resl[h]) return;
    if (--resl[h] == 0) {
      if (resp[h] != pp+1) ERR_fatal("M_unlock: invalid resource pointer");
      free(pp);
      resp[h] = NULL;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
GCC_DESTRUCTOR_SYSTEM {
  for (int f = 0; f < MAX_WAD; ++f) {
    if (resp[f] != NULL) {
      int *pp = ((int *)resp[f])-1;
      //
      free(pp);
    }
  }
}
