/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_GAMEMODE_H
#define D2D_GAMEMODE_H

#include "common.h"


extern ccBool gm_climb_stairs; // true: autoclimb stairs (non-standard mechanics)
extern ccBool gm_new_cansee; // use new tracer in 'cansee'?
extern ccBool g_paused;
extern ccBool g_cheats_enabled;

extern ccBool g_no_monsters;


#endif
