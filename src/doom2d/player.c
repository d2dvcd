/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "player.h"

#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "cmdcon.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "gamemode.h"
#include "items.h"
#include "jimapi.h"
#include "keyb.h"
#include "menu.h"
#include "misc.h"
#include "monster.h"
#include "netgame.h"
#include "smoke.h"
#include "switch.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"


#ifndef xoffsetof
# define xoffsetof(type, member)  __builtin_offsetof(type, member)
#endif


#define PL_RAD  (8)
#define PL_HT   (26)

#define PL_SWUP   (4)
#define PL_FLYUP  (4)

#define PL_AIR       (360)
#define PL_AQUA_AIR  (1091)


player_t pl[2];
ccBool p_immortal = CC_FALSE, p_fly = CC_FALSE, p_ghost = CC_FALSE;
int PL_JUMP = 10, PL_RUN = 8;
int wp_it[11] = {0,I_CSAW,0,I_SGUN,I_SGUN2,I_MGUN,I_LAUN,I_PLAS,I_BFG,I_GUN2,0};


typedef void fire_f (int, int, int, int, int);

static int aitime;
static const snd_t *aisnd[3];
static const snd_t *pdsnd[5];

static vgaimg spr[27*2];
static const snd_t *snd[11];
static char sprd[27*2];
static vgaimg wpn[11][6];
static const uint8_t goanim[] = "BDACDA";
static const uint8_t dieanim[] = "HHHHIIIIJJJJKKKKLLLLMMMM";
static const uint8_t slopanim[] = "OOPPQQRRSSTTUUVVWW";


////////////////////////////////////////////////////////////////////////////////
static inline int isKeyDown (int k0, int k1) {
  if (k0 >= 0 && keys[k0]) return 1;
  if (k1 >= 0 && keys[k1]) return 1;
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
#define DEMO_SIGN     "D2D:VCDD"
#define DEMO_VERSION  (0)


typedef struct GCC_PACKED {
  char sign[8];
  uint8_t version;
  uint8_t plcount; // 1 or 2
  uint8_t map;
  uint8_t deathmatch;
  uint8_t immortal;
  int PL_JUMP;
  int PL_RUN;
  int aitime;
  BJRandCtx prng;
  int framecount;
} DemoHeader;


typedef struct GCC_PACKED {
  uint8_t keys[2][KB_MAX]; // keys for both players
} DemoFrame;


////////////////////////////////////////////////////////////////////////////////
DEMO_MODE demo_mode = DEMO_NONE;
void *demo_buf = NULL;
int demo_buf_pos = 0;
int demo_buf_size = 0;
BJRandCtx demo_initial_prng;
BJRandCtx demo_saved_prng;
static DemoFrame cur_demo_frame;
static DemoHeader demo_header;
static char *demo_out_file_name = NULL;


////////////////////////////////////////////////////////////////////////////////
// pack and store frame
static void demo_pack_cur_frame (void) {
  uint8_t *buf = ((uint8_t *)demo_buf)+demo_buf_pos;
  //
  if ((demo_buf_pos += 4) >= demo_buf_size) {
    ERR_fatal("out of memory for demo recording");
  }
  //
  for (int plnum = 0; plnum <= 1; ++plnum) {
    uint16_t flg = 0;
    //
    for (int kid = 0; kid < KB_MAX; ++kid) {
      if (cur_demo_frame.keys[plnum][kid]) flg |= (1<<kid);
    }
    *buf++ = (flg&0xff);
    *buf++ = ((flg>>8)&0xff);
  }
  //
  ++(demo_header.framecount);
  //memset(&cur_demo_frame, 0, sizeof(cur_demo_frame));
}


// 0: no more frames
static void demo_unpack_cur_frame (void) {
  const uint8_t *buf = ((const uint8_t *)demo_buf)+demo_buf_pos;
  //
  if (demo_buf_pos >= demo_buf_size) return;
  //
  //memset(&cur_demo_frame, 0, sizeof(cur_demo_frame));
  for (int plnum = 0; plnum <= 1; ++plnum) {
    uint16_t flg = (buf[0]|(buf[1]<<8));
    //
    buf += 2;
    for (int kid = 0; kid < KB_MAX; ++kid) {
      cur_demo_frame.keys[plnum][kid] = (flg&(1<<kid) ? 1 : 0);
    }
  }
  //
  demo_buf_pos += 4;
}


void player_unpack_keys (int plnum, uint16_t keys) {
  //memset(&cur_demo_frame, 0, sizeof(cur_demo_frame));
  for (int kid = 0; kid < KB_MAX; ++kid) {
    cur_demo_frame.keys[plnum][kid] = (keys&(1<<kid) ? 1 : 0);
  }
}


uint16_t player_build_keys (int plnum) {
  uint16_t res = 0;
  //
  for (int kid = 0; kid < KB_MAX; ++kid) {
    for (int alt = 0; alt <= 1; ++alt) {
      int k = pl[plnum].kb[kid][alt];
      //
      if (k > 0 && keys[k]) {
        res |= (1<<kid);
        break;
      }
    }
  }
  //
  return res;
}


////////////////////////////////////////////////////////////////////////////////
static int demo_init_record (void) {
  demo_buf_size = (60*60*19*10)*4;
  demo_buf = realloc(demo_buf, demo_buf_size);
  demo_buf_pos = 0;
  demo_mode = DEMO_RECORD;
  //
  memcpy(demo_header.sign, DEMO_SIGN, 8);
  demo_header.version = DEMO_VERSION;
  demo_header.plcount = g_plrcount;
  demo_header.map = g_map;
  demo_header.deathmatch = (g_dm ? 1 : 0);
  demo_header.immortal = p_immortal;
  demo_header.PL_JUMP = PL_JUMP;
  demo_header.PL_RUN = PL_RUN;
  demo_header.aitime = aitime;
  //demo_initial_prng = demo_header.prng = g_prng;
  demo_header.framecount = 0;
  //
  conlogf("restoring 'vanilla' game mode");
  gm_climb_stairs = CC_FALSE;
  gm_new_cansee = CC_FALSE;
  //
  return 1;
}


int demo_init_play_fl (FILE *fl) {
  demo_mode = DEMO_NONE;
  //
  if (fl != NULL) {
    if (fread(&demo_header, sizeof(demo_header), 1, fl) != 1) goto error;
    if (memcmp(demo_header.sign, DEMO_SIGN, 8) != 0) goto error;
    if (demo_header.version != DEMO_VERSION) goto error;
    if (demo_header.framecount < 1 || demo_header.framecount > 60*60*19*10) goto error;
    //
    demo_buf_size = demo_header.framecount*4;
    demo_buf = realloc(demo_buf, demo_buf_size);
    demo_buf_pos = 0;
    //
    if (fread(demo_buf, demo_buf_size, 1, fl) != 1) goto error;
    //
    demo_saved_prng = g_prng;
    demo_initial_prng = g_prng = demo_header.prng;
    //
    g_plrcount = demo_header.plcount;
    if (g_plrcount < 1 || g_plrcount > 2) goto error;
    g_map = demo_header.map;
    g_dm = demo_header.deathmatch;
    //
    M_init_players();
    G_start();
    GM_set(NULL);
    //
    p_immortal = demo_header.immortal;
    PL_JUMP = demo_header.PL_JUMP;
    PL_RUN = demo_header.PL_RUN;
    aitime = demo_header.aitime;
    //
    conlogf("restoring 'vanilla' game mode");
    gm_climb_stairs = CC_FALSE;
    gm_new_cansee = CC_FALSE;
    //
    demo_mode = DEMO_PLAY;
    //
    return 0;
  }
error:
  demo_mode = DEMO_NONE;
  //
  return -1;
}


int demo_init_play (const char *fname) {
  FILE *fl = fopen(fname, "rb");
  int res = -1;
  //
  if (fl != NULL) {
    res = demo_init_play_fl(fl);
    fclose(fl);
  }
  //
  return res;
}


void demo_done (void) {
  switch (demo_mode) {
    case DEMO_RECORD:
      demo_mode = DEMO_NONE;
      //
      if (demo_out_file_name != NULL) {
        FILE *fo = fopen(demo_out_file_name, "wb");
        //
        if (fo != NULL) {
          fwrite(&demo_header, sizeof(demo_header), 1, fo);
          fwrite(demo_buf, demo_buf_pos, 1, fo);
          fclose(fo);
        }
        //
        free(demo_out_file_name);
        demo_out_file_name = NULL;
      }
      break;
    case DEMO_PLAY:
      demo_mode = DEMO_NONE;
      g_prng = demo_saved_prng;
      break;
    default: ;
  }
}


void demo_begin_frame (void) {
  switch (demo_mode) {
    case DEMO_RECORD:
      // save keys
      //memset(&cur_demo_frame, 0, sizeof(cur_demo_frame));
      for (int plnum = 0; plnum <= 1; ++plnum) {
        for (int kid = 0; kid < KB_MAX; ++kid) {
          cur_demo_frame.keys[plnum][kid] = 0;
          for (int alt = 0; alt <= 1; ++alt) {
            int k = pl->kb[kid][alt];
            //
            if (k > 0 && keys[k]) {
              cur_demo_frame.keys[plnum][kid] = 1;
              break;
            }
          }
        }
      }
      break;
    case DEMO_PLAY:
      demo_unpack_cur_frame();
      break;
    default: ;
  }
}


int demo_end_frame (void) {
  switch (demo_mode) {
    case DEMO_RECORD:
      demo_pack_cur_frame();
      return 1;
    case DEMO_PLAY:
      if (demo_buf_pos >= demo_buf_size) {
        demo_mode = DEMO_NONE;
        return 0;
      }
      return 1;
    default: ;
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
CONCMD(demo_record, "record demo\ndemo_record map filename") {
  int n;
  char s[8];
  //
  CMDCON_HELP();
  //CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc != 3) {
    conlogf("%s: invalid number of arguments\n", argv[0]);
    return;
  }
  //
  n = cmdcon_parseint(argv[1], -1, 0, 99, NULL);
  if (n < 0) {
    conlogf("%s: invalid number: '%s'\n", argv[0], argv[1]);
    return;
  }
  //
  sprintf(s, "MAP%02d", n);
  if (F_findres(s) < 0) {
    conlogf("%s: can't warp to MAP%02d: no such map!\n", argv[0], n);
    return;
  }
  //
  if (demo_out_file_name != NULL) free(demo_out_file_name);
  demo_out_file_name = strdup(argv[2]);
  //
  g_plrcount = 1;
  g_dm = 0;
  g_map = n;
  //
  demo_initial_prng = demo_header.prng = g_prng;
  //
  M_init_players();
  G_start();
  GM_set(NULL);
  //
  demo_init_record();
}


CONCMD(demo_play, "play demo") {
  CMDCON_HELP();
  //CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc != 2) {
    conlogf("%s: invalid number of arguments\n", argv[0]);
    return;
  }
  //
  demo_init_play(argv[1]);
}


CONCMD(demo_stop, "stop recording demo") {
  CMDCON_CHECK_NONET();
  demo_done();
}


////////////////////////////////////////////////////////////////////////////////
static inline int ISKEYX (const player_t *pl, int kid) {
  if (demo_mode == DEMO_PLAY || n_host != NULL) return cur_demo_frame.keys[(-pl->id)-1][kid];
  return isKeyDown(pl->kb[kid][0], pl->kb[kid][1]);
}


#define ISKEY(_kname)  ISKEYX(p, _kname)


////////////////////////////////////////////////////////////////////////////////
static inline int nonz (int a) { return (a ? a : 1);}


static inline int firediry (const player_t *p) {
  if (p->f&PLF_UP) return -42;
  if (p->f&PLF_DOWN) return 19;
  return 0;
}


static void fire (player_t *p) {
  static fire_f *ff[11] = {
    WP_pistol, WP_pistol, WP_pistol, WP_shotgun, WP_dshotgun,
    WP_mgun, WP_rocket, WP_plasma, WP_bfgshot, WP_shotgun, WP_pistol
  };
  static const int ft[11] = {5, 2, 6, 18, 36, 2, 12, 2, 0, 2, 1};
  //
  if (p->cwpn) return;
  if (p->wpn == 8) {
    if (!p->fire) {
      if (ISKEY(KB_FIRE) && p->cell >= 40) {
        Z_sound(snd[5], 128);
        p->fire = 21;
        p->cell -= 40;
        p->drawst |= PL_DRAWWPN;
        return;
      } else {
        return;
      }
    }
    if (p->fire == 1) p->cwpn = 12; else return;
  } else if (p->wpn == 1) {
    if (!p->csnd) {
      if (!ISKEY(KB_FIRE)) {
        Z_sound(snd[7], 128);
        p->csnd = 13;
        return;
      }
    }
    if (ISKEY(KB_FIRE) && !p->fire) {
      p->fire = 2;
      WP_chainsaw(p->o.x+((p->d)?4:-4), p->o.y, (g_dm)?9:3, p->id);
      if (!p->csnd) {
        Z_sound(snd[8], 128);
        p->csnd = 29;
      }
    }
    return;
  } else if (p->fire) {
    return;
  }
  //
  if (ISKEY(KB_FIRE) || p->wpn == 8) {
    switch (p->wpn) {
      case 2:
      case 5:
        if (!p->ammo) return;
        --p->ammo;
        p->drawst |= PL_DRAWWPN;
        break;
      case 3:
      case 9:
        if (!p->shel) return;
        --p->shel;
        p->drawst |= PL_DRAWWPN;
        break;
      case 4:
        if (p->shel < 2) return;
        p->shel -= 2;
        p->drawst |= PL_DRAWWPN;
        break;
      case 6:
        if (!p->rock) return;
        --p->rock;
        p->drawst |= PL_DRAWWPN;
        break;
      case 7:
        if (!p->cell) return;
        --p->cell;
        p->drawst |= PL_DRAWWPN;
        break;
      case 10:
        if (!p->fuel) return;
        --p->fuel;
        p->drawst |= PL_DRAWWPN;
        break;
    }
    if (p->wpn == 10) {
      WP_flamethrower(p->o.x, p->o.y-15, p->o.x+((p->d)?30:-30), p->o.y-15+firediry(p), p->o.xv+p->o.vx, p->o.yv+p->o.vy, p->id);
    } else if (p->wpn >= 1) {
      (ff[p->wpn&0x7f])(p->o.x, p->o.y-15, p->o.x+((p->d)?30:-30), p->o.y-15+firediry(p), p->id);
    } else {
      WP_punch(p->o.x+((p->d)?4:-4), p->o.y, 3, p->id);
    }
    p->fire = ft[p->wpn&0x7f];
    if (p->wpn >= 2) p->f |= PLF_FIRE;
  }
}


static void chgwpn (player_t *p) {
  if (p->cwpn) return;
  if (p->fire && p->wpn != 1) return;
  if (ISKEY(KB_WPNPREV)) {
    int newwpn = p->wpn;
    //
    for (int f = 0; f < 12; ++f) {
      if (--newwpn < 0) newwpn = 10;
      if (p->wpns&(1<<newwpn)) { p->wpn = newwpn; break; }
    }
    p->cwpn = 3;
  } else if (ISKEY(KB_WPNNEXT)) {
    int newwpn = p->wpn;
    //
    for (int f = 0; f < 12; ++f) {
      if (++newwpn > 10) newwpn = 0;
      if (p->wpns&(1<<newwpn)) { p->wpn = newwpn; break; }
    }
    p->cwpn = 3;
  }
  if (p->cwpn) {
    p->drawst |= PL_DRAWWPN;
    p->fire = 0;
    if (p->wpn == 1) Z_sound(snd[6], 128);
  }
}


static void jump (player_t *p, int st) {
  if (Z_canbreathe(p->o.x, p->o.y, p->o.r, p->o.h)) {
    if (p->air < PL_AIR) {
      p->air = PL_AIR;
      p->drawst |= PL_DRAWAIR;
    }
  } else {
    if (--p->air < -9) {
      p->air = 0;
      PL_hit(p, 10, -3, HIT_WATER);
    } else if ((p->air&31) == 0) {
      FX_bubble(p->o.x, p->o.y-20, 0, 0, 5);
    }
    p->drawst |= PL_DRAWAIR;
  }
  //
  if (!p_ghost && ISKEY(KB_JUMP)) {
    if (p_fly) {
      p->o.yv = -PL_FLYUP;
    } else {
      if (Z_canstand(OBJT_PLAYER, p->o.x, p->o.y, p->o.r)) p->o.yv = -PL_JUMP;
      else if (st&Z_INWATER) p->o.yv = -PL_SWUP;
    }
  }
}


static void godown (player_t *p) {
  /*
  if (ISKEY(KB_DOWN) && ISKEY(KB_UP)) {
    //fprintf(stderr, "trying to go down\n");
    if (Z_cangodown(OBJT_PLAYER, p->o.x, p->o.y, p->o.r)) {
      //fprintf(stderr, "going down\n");
      p->o.y += 2;
    }
  }
  */
}


int PL_isdead (const player_t *p) {
  switch (p->st) {
    case PLST_DEAD:
    case PLST_MESS:
    case PLST_PLOUT:
      return 1;
  }
  return 0;
}


void PL_init (void) {
  //k8:p_immortal = 0;
  p_ghost = 0; //k8
  p_fly = 0;
  PL_JUMP = 10;
  PL_RUN = 8;
  aitime = 0;
}


void PL_alloc (void) {
  static const char nm[][6] = {
    "OOF",
    "PLPAIN",
    "PLDETH",
    "SLOP",
    "PDIEHI",
    "BFG",
    "SAWUP",
    "SAWIDL",
    "SAWFUL",
    "SAWHIT",
    "PLFALL"
  };
  static char s[6];
  int i, j;
  //
  for (i = 0; i < 27; ++i) {
    Z_getspr(&spr[i*2], "PLAY", i, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "PLAY", i, 2, sprd+i*2+1);
  }
  memcpy(s, "PWPx", 4);
  for (i = 1; i < 11; ++i) {
    s[3] = (i < 10 ? '0' : ('A'-10))+i;
    for (j = 0; j < 6; ++j) Z_getspr(&wpn[i][j], s, j, 1, NULL);
  }
  for (i = 0; i < 11; ++i) snd[i] = Z_getsnd(nm[i]);
  memcpy(s, "AIx", 4);
  for (i = 0; i < 3; ++i) {
    s[2] = i+'1';
    aisnd[i] = Z_getsnd(s);
  }
  memcpy(s, "PLDTHx", 6);
  for (i = 0; i < 5; ++i) {
    s[5] = i+'1';
    pdsnd[i] = Z_getsnd(s);
  }
  //
  for (int f = 0; f <= 1; ++f) pl[f].o.type = OBJT_PLAYER;
}


void PL_restore (player_t *p) {
  p->o.type = OBJT_PLAYER;
  p->o.xv = p->o.yv = p->o.vx = p->o.vy = 0;
  p->o.r = PL_RAD;
  p->o.h = PL_HT;
  p->pain = 0;
  p->invl = p->suit = 0;
  switch (p->st) {
    case PLST_DEAD:
    case PLST_MESS:
    case PLST_PLOUT:
    case PLST_DIE:
    case PLST_SLOP:
    case PLST_FALL:
      p->life = 100;
      p->armor = 0;
      p->air = PL_AIR;
      p->wpns = 5;
      p->wpn = 2;
      p->ammo = 50;
      p->fuel = p->shel = p->rock = p->cell = 0;
      p->amul = 1;
      break;
  }
  p->st = PLST_STAND;
  p->fire = p->cwpn = p->csnd = 0;
  p->f = 0;
  p->drawst = 0xFF;
  p->looky = 0;
  p->keys = (g_dm ? 0x70 : 0);
}


void PL_reset (void) {
  pl[0].st = pl[1].st = PLST_DEAD;
  pl[0].frag = pl[1].frag = 0;
}


void PL_spawn (player_t *p, int x, int y, char d) {
  PL_restore(p);
  p->o.x = x;
  p->o.y = y;
  p->d = d;
  p->kills = p->secrets = 0;
}


int PL_hit (player_t *p, int d, int o, int t) {
  if (!d) return 0;
  //
  switch (p->st) {
    case PLST_DIE:
    case PLST_SLOP:
    case PLST_DEAD:
    case PLST_MESS:
    case PLST_PLOUT:
    case PLST_FALL:
      return 0;
  }
  //
  if (t == HIT_TRAP) {
    if (!p_immortal) {
      p->armor = 0;
      p->life = -100;
    }
  } else if (t != HIT_ROCKET && t != HIT_ELECTRO) {
    if (p->id == -1) {
      if (o == -1) return 0;
    } else if (o == -2) {
      return 0;
    }
  }
  //
  if (t != HIT_WATER && t != HIT_ELECTRO) DOT_blood(p->o.x, p->o.y-15, hit_xv, hit_yv, d*2);
  else if (t == HIT_WATER) FX_bubble(p->o.x, p->o.y-20, 0, 0, d/2);
  //
  if (p_immortal || p->invl) return 1;
  //
  p->hit += d;
  p->hito = o;
  return 1;
}


void PL_damage (player_t *p) {
  int i;
  //
  if (!p->hit && p->life > 0) return;
  switch (p->st) {
    case PLST_DIE:
    case PLST_SLOP:
    case PLST_DEAD:
    case PLST_MESS:
    case PLST_PLOUT:
    case PLST_FALL:
      return;
  }
  i = p->hit*p->life/nonz(p->armor*3/4+p->life);
  p->pain += p->hit;
  p->drawst |= PL_DRAWLIFE|PL_DRAWARMOR;
  if ((p->armor -= p->hit-i) < 0) {
    p->life += p->armor;
    p->armor = 0;
  }
  if ((p->life -= i) <= 0) {
    if (p->life>-30) {
      p->st = PLST_DIE;
      p->s = 0;
      Z_sound(pdsnd[myrand(5)], 128);
    } else {
      p->st = PLST_SLOP;
      p->s = 0;
      Z_sound(snd[3], 128);
    }
    if (p->amul > 1) IT_spawn(p->o.x, p->o.y, I_BPACK);
    if (!g_dm) {
      if (p->keys&16) IT_spawn(p->o.x, p->o.y, I_KEYR);
      if (p->keys&32) IT_spawn(p->o.x, p->o.y, I_KEYG);
      if (p->keys&64) IT_spawn(p->o.x, p->o.y, I_KEYB);
    }
    for (i = 1, p->wpns >>= 1; i < 11; ++i, p->wpns >>= 1) {
      if (i != 2 && (p->wpns&1)) IT_spawn(p->o.x, p->o.y, wp_it[i]);
    }
    p->wpns = 5;
    p->wpn = 2;
    p->f |= PLF_PNSND;
    p->drawst |= PL_DRAWWPN;
    //
    if (g_dm && g_plrcount > 1) {
      if (p->id == -1) {
        if (p->hito == -2) {
          ++pl[1].kills;
          ++pl[1].frag;
        } else if (p->hito == -1) {
          --pl[0].frag;
        }
      } else {
        if (p->hito == -1) {
          ++pl[0].kills;
          ++pl[0].frag;
        } else if (p->hito == -2) {
          --pl[1].frag;
        }
      }
      for (int f = 0; f < g_plrcount; ++f) pl[f].drawst |= PL_DRAWFRAG;
    }
    //
    p->life = 0;
    return;
  }
  return;
}


void PL_cry (player_t *p) {
  Z_sound(snd[p->pain > 20 ? 1 : 0], 128);
  p->f |= PLF_PNSND;
}


int PL_give (player_t *p, int t) {
  int i;
  //
  switch (p->st) {
    case PLST_DIE:
    case PLST_SLOP:
    case PLST_DEAD:
    case PLST_MESS:
    case PLST_PLOUT:
      return 0;
  }
  switch (t) {
    case I_STIM:
    case I_MEDI:
      if (p->life >= 100) return 0;
      if ((p->life += (t == I_MEDI ? 25 : 10)) > 100) p->life = 100;
      p->drawst |= PL_DRAWLIFE;
      return 1;
    case I_CLIP:
      if (p->ammo >= 200*p->amul) return 0;
      if ((p->ammo += 10) > 200*p->amul) p->ammo = 200*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_AMMO:
      if (p->ammo >= 200*p->amul) return 0;
      if ((p->ammo += 50) > 200*p->amul) p->ammo = 200*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_SHEL:
      if (p->shel >= 50*p->amul) return 0;
      if ((p->shel += 4) > 50*p->amul) p->shel = 50*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_SBOX:
      if (p->shel >= 50*p->amul) return 0;
      if ((p->shel += 25) > 50*p->amul) p->shel = 50*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_ROCKET:
      if (p->rock >= 50*p->amul) return 0;
      if ((++p->rock) > 50*p->amul) p->rock = 50*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_RBOX:
      if (p->rock >= 50*p->amul) return 0;
      if ((p->rock += 5) > 50*p->amul) p->rock = 50*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_CELL:
      if (p->cell >= 300*p->amul) return 0;
      if ((p->cell += 40) > 300*p->amul) p->cell = 300*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_CELP:
      if (p->cell >= 300*p->amul) return 0;
      if ((p->cell += 100) > 300*p->amul) p->cell = 300*p->amul;
      p->drawst |= PL_DRAWWPN;
      return 1;
    case I_BPACK:
      if (p->amul == 1) { p->amul = 2; i = 1; } else i = 0;
      i |= PL_give(p, I_CLIP);
      i |= PL_give(p, I_SHEL);
      i |= PL_give(p, I_ROCKET);
      i |= PL_give(p, I_CELL);
      return i;
    case I_CSAW:
      if (!(p->wpns&2)) {
        p->wpns |= 2;
        p->drawst |= PL_DRAWWPN;
        return 1;
      }
      return 0;
    case I_GUN2:
      i = PL_give(p, I_SHEL);
      if (!(p->wpns&512)) { p->wpns |= 512; i = 1; }
      p->drawst |= PL_DRAWWPN;
      return i;
    case I_SGUN:
      i = PL_give(p, I_SHEL);
      if (!(p->wpns&8)) { p->wpns |= 8; i = 1; }
      p->drawst |= PL_DRAWWPN;
      return i;
    case I_SGUN2:
      i = PL_give(p, I_SHEL);
      if (!(p->wpns&16)) { p->wpns |= 16; i = 1; }
      p->drawst |= PL_DRAWWPN;
      return i;
    case I_MGUN:
      i = PL_give(p, I_AMMO);
      if (!(p->wpns&32)) { p->wpns |= 32; i = 1; }
      p->drawst |= PL_DRAWWPN;
      return i;
    case I_LAUN:
      i = PL_give(p, I_ROCKET);
      i |= PL_give(p, I_ROCKET);
      if (!(p->wpns&64)) { p->wpns |= 64; i = 1; }
      p->drawst |= PL_DRAWWPN;
      return i;
    case I_PLAS:
      i = PL_give(p, I_CELL);
      if (!(p->wpns&128)) { p->wpns |= 128; i = 1; }
      p->drawst |= PL_DRAWWPN;
      return i;
    case I_BFG:
      i = PL_give(p, I_CELL);
      if (!(p->wpns&256)) { p->wpns |= 256; i = 1; }
      p->drawst |= PL_DRAWWPN;
      return i;
    case I_ARM1:
      if (p->armor >= 100) return 0;
      p->armor = 100;
      p->drawst |= PL_DRAWARMOR;
      return 1;
    case I_ARM2:
      if (p->armor >= 200) return 0;
      p->armor = 200;
      p->drawst |= PL_DRAWARMOR;
      return 1;
    case I_MEGA:
      i = 0;
      if (p->life < 200) { p->life = 200; p->drawst |= PL_DRAWLIFE; i = 1;}
      if (p->armor < 200) { p->armor = 200; p->drawst |= PL_DRAWARMOR; i = 1;}
      return i;
    case I_SUPER:
      if (p->life < 200) {
        p->life = K8MIN(p->life+100, 200);
        p->drawst |= PL_DRAWLIFE;
        return 1;
      }
      return 0;
    case I_INVL:
      p->invl = PL_POWERUP_TIME;
      return 1;
    case I_SUIT:
      p->suit = PL_POWERUP_TIME;
      return 1;
    case I_AQUA:
      if (p->air >= PL_AQUA_AIR) return 0;
      p->air = PL_AQUA_AIR;
      p->drawst |= PL_DRAWAIR;
      return 1;
    case I_KEYR:
      if (p->keys&16) return 0;
      p->keys |= 16;
      p->drawst |= PL_DRAWKEYS;
      return 1;
    case I_KEYG:
      if (p->keys&32) return 0;
      p->keys |= 32;
      p->drawst |= PL_DRAWKEYS;
      return 1;
    case I_KEYB:
      if (p->keys&64) return 0;
      p->keys |= 64;
      p->drawst |= PL_DRAWKEYS;
      return 1;
    default:
      return 0;
  }
}


static const int itemids[] = {
  I_CLIP,
  I_SHEL,
  I_ROCKET,
  I_CELL,
  I_AMMO,
  I_SBOX,
  I_RBOX,
  I_CELP,
  I_STIM,
  I_MEDI,
  I_BPACK,
  I_CSAW,
  I_SGUN,
  I_SGUN2,
  I_MGUN,
  I_LAUN,
  I_PLAS,
  I_BFG,
  I_ARM1,
  I_ARM2,
  I_MEGA,
  I_INVL,
  I_AQUA,
  I_KEYR,
  I_KEYG,
  I_KEYB,
  I_SUIT,
  I_SUPER,
  I_GUN2,
  0
};


static const char *itemnames[] = {
  "ammo_clip",
  "ammo_shell",
  "ammo_rocket",
  "ammo_cell",
  "ammo",
  "sbox",
  "rbox",
  "celp",
  "stim",
  "medi",
  "backpack",
  "chainsaw",
  "shotgun",
  "double_shoutgun",
  "machinegun",
  "rocket_launcher",
  "plasmagun",
  "bfg",
  "green_armor",
  "blue_armor",
  "megahealth",
  "invulnerability",
  "aqualung",
  "red_key",
  "green_key",
  "blue_key",
  "envirosuit",
  "superhealth",
  "supergun",
  NULL
};


//TODO: give to 2nd player
CONCMD(give, "gime something") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  //
  if (argc < 2) {
    conlogf("%s: give what?\n", argv[0]);
    for (int f = 0; itemnames[f] != NULL; ++f) conlogf(" %s\n", itemnames[f]);
    return;
  }
  //
  for (int f = 1; f < argc; ++f) {
    const char *s = argv[f];
    int idx;
    //
    for (idx = 0; itemnames[idx] != NULL; ++idx) if (strcasecmp(itemnames[idx], s) == 0) break;
    if (itemnames[idx] != NULL) {
      PL_give(&pl[0], itemids[idx]);
      conlogf("given: '%s'\n", s);
    } else {
      conlogf("unknown item: '%s'\n", s);
    }
  }
}


void PL_act (player_t *p) {
  int st;
  //
  if (ISKEY(KB_DOWN) && p_ghost) p->o.yv = 4;
  if (ISKEY(KB_UP) && p_ghost) p->o.yv = -4;
  //
  if (--aitime < 0) aitime = 0;
  //
  SW_press(p->o.x, p->o.y, p->o.r, p->o.h, 4|p->keys, p->id);
  //
  if (!p->suit && (g_time&15) == 0) PL_hit(p, Z_getacid(p->o.x, p->o.y, p->o.r, p->o.h), -3, HIT_SOME);
  //
  if (p->st != PLST_FALL && p->st != PLST_PLOUT) {
    if (((st = Z_moveobj(&p->o))&Z_FALLOUT) && p->o.y >= FLDH*CELH+50) {
      switch (p->st) {
        case PLST_DEAD:
        case PLST_MESS:
        case PLST_DIE:
        case PLST_SLOP:
          p->s = 5;
          break;
        default:
          p->s = Z_sound(snd[10], 128);
          if (g_dm) --p->frag;
      }
      p->st = PLST_FALL;
    }
  } else {
    st = 0;
  }
  //
  if (p_ghost) { p->o.yv = p->o.vy = 0; }
  //
  if (st&Z_HITWATER) Z_splash(&p->o, PL_RAD+PL_HT);
  if ((p->f&PLF_FIRE) && p->fire != 2) p->f &= ~PLF_FIRE;
  //
  if (ISKEY(KB_UP)) {
    p->f |= PLF_UP;
    p->looky -= 5;
  } else {
    p->f &= ~PLF_UP;
    if (ISKEY(KB_DOWN)) {
      p->f |= PLF_DOWN;
      p->looky += 5;
    } else {
      p->f &= ~PLF_DOWN;
      p->looky = Z_dec(p->looky, 5);
    }
  }
  //
  if (ISKEY(KB_USE)) SW_press(p->o.x, p->o.y, p->o.r, p->o.h, 1|p->keys, p->id);
  //
  if (p->fire) --p->fire;
  if (p->cwpn) --p->cwpn;
  if (p->csnd) --p->csnd;
  if (p->invl) --p->invl;
  if (p->suit) --p->suit;
  //
  switch (p->st) {
    case PLST_DIE:
      p->o.h = 7;
      if (!dieanim[++p->s]) { p->st = PLST_DEAD; MN_killedp(); }
      p->o.xv = Z_dec(p->o.xv, 1);
      break;
    case PLST_SLOP:
      p->o.h = 6;
      if (!slopanim[++p->s]) { p->st = PLST_MESS; MN_killedp(); }
      p->o.xv = Z_dec(p->o.xv, 1);
      break;
    case PLST_GO:
      chgwpn(p);
      fire(p);
      jump(p, st);
      godown(p);
      if (p_fly) SMK_gas(p->o.x, p->o.y-2, 2, 3, p->o.xv+p->o.vx, p->o.yv+p->o.vy, 128);
      if ((p->s += abs(p->o.xv)/2) >= 24) p->s %= 24;
      if (!ISKEY(KB_LEFT) && !ISKEY(KB_RIGHT)) {
        if (p->o.xv) p->o.xv = Z_dec(p->o.xv, 1); else p->st = PLST_STAND;
        break;
      }
      if (p->o.xv < PL_RUN && ISKEY(KB_RIGHT)) {
        p->o.xv += PL_RUN>>3;
        p->d = 1;
      } else if (PL_RUN > 8) {
        SMK_gas(p->o.x, p->o.y-2, 2, 3, p->o.xv+p->o.vx, p->o.yv+p->o.vy, 32);
      }
      if (p->o.xv>-PL_RUN && ISKEY(KB_LEFT)) {
        p->o.xv -= PL_RUN>>3;
        p->d = 0;
      } else if (PL_RUN > 8) {
        SMK_gas(p->o.x, p->o.y-2, 2, 3, p->o.xv+p->o.vx, p->o.yv+p->o.vy, 32);
      }
      break;
    case PLST_STAND:
      chgwpn(p);
      fire(p);
      jump(p, st);
      godown(p);
      if (p_fly) {
        SMK_gas(p->o.x, p->o.y-2, 2, 3, p->o.xv+p->o.vx, p->o.yv+p->o.vy, 128);
      }
      if (ISKEY(KB_LEFT)) {
        p->st = PLST_GO;
        p->s = 0;
        p->d = 0;
      } else if (ISKEY(KB_RIGHT)) {
        p->st = PLST_GO;
        p->s = 0;
        p->d = 1;
      }
      break;
    case PLST_DEAD:
    case PLST_MESS:
    case PLST_PLOUT:
      p->o.xv = Z_dec(p->o.xv, 1);
      for (int f = 0; f < KB_MAX; ++f) {
        if (ISKEYX(p, f)) {
          if (p->st != PLST_PLOUT) MN_spawn_deadpl(&p->o, p->color, (p->st == PLST_MESS ? 1 : 0));
          PL_restore(p);
          if (g_dm) { G_respawn_player(p); break; }
          if (g_plrcount == 1) {
            if (--p->lives == 0) {
              G_start();
              break;
            } else {
              p->o.x = dm_pos[0].x;
              p->o.y = dm_pos[0].y;
              p->d = dm_pos[0].d;
            }
            p->drawst |= PL_DRAWLIVES;
          }
          //
          if (p->id == -1) {
            p->o.x = dm_pos[0].x;
            p->o.y = dm_pos[0].y;
            p->d = dm_pos[0].d;
          } else {
            p->o.x = dm_pos[1].x;
            p->o.y = dm_pos[1].y;
            p->d = dm_pos[1].d;
          }
        }
      }
      break;
    case PLST_FALL:
      if (--p->s <= 0) p->st = PLST_PLOUT;
      break;
  }
}


static inline int standspr (player_t *p) {
  if (p->f&PLF_UP) return 'X';
  if (p->f&PLF_DOWN) return 'Z';
  return 'E';
}


static inline int wpnspr (player_t *p) {
  if (p->f&PLF_UP) return 'C';
  if (p->f&PLF_DOWN) return 'E';
  return 'A';
}


void PL_draw (player_t *p) {
  int s, w, wx, wy;
  static const int wytab[] = {-1, -2, -1, 0};
  //
  s = 'A';
  w = 0;
  wx = wy = 0;
  switch (p->st) {
    case PLST_STAND:
      if (p->f&PLF_FIRE) {
        s = standspr(p)+1;
        w = wpnspr(p)+1;
      } else if (p->pain) {
        s = 'G';
        w = 'A';
        wx = (p->d ? 2 : -2);
        wy = 1;
      } else {
        s = standspr(p);
        w = wpnspr(p);
      }
      break;
    case PLST_DEAD:
      s = 'N';
      break;
    case PLST_MESS:
      s = 'W';
      break;
    case PLST_GO:
      if (p->pain) {
        s = 'G';
        w = 'A';
        wx = (p->d ? 2 : -2);
        wy = 1;
      } else {
        s = goanim[p->s/8];
        w = (p->f&PLF_FIRE ? 'B' : 'A');
        wx = (p->d ? 2 : -2);
        wy = 1+wytab[s-'A'];
      }
      break;
    case PLST_DIE:
      s = dieanim[p->s];
      break;
    case PLST_SLOP:
      s = slopanim[p->s];
      break;
    case PLST_PLOUT:
      s = 0;
      break;
  }
  if (p->wpn == 0) w = 0;
  if (w) Z_drawspr(p->o.x+wx, p->o.y+wy, &wpn[p->wpn&0x7f][w-'A'], p->d);
  if (s) Z_drawmanspr(p->o.x, p->o.y, &spr[(s-'A')*2+p->d], sprd[(s-'A')*2+p->d], p->color);
}


vgaimg *PL_getspr (int s, int d) {
  return &spr[(s-'A')*2+d];
}


static void chk_bfg (player_t *p, int x, int y) {
  if (aitime == 0) {
    int dx, dy;
    //
    switch (p->st) {
      case PLST_DIE:
      case PLST_SLOP:
      case PLST_FALL:
      case PLST_DEAD:
      case PLST_MESS:
      case PLST_PLOUT:
        return;
    }
    dx = p->o.x-x;
    dy = p->o.y-p->o.h/2-y;
    if (dx*dx+dy*dy <= 1600) aitime = Z_sound(aisnd[myrand(3)], 128)*4;
  }
}


void bfg_fly (int x, int y, int o) {
  //if (!g_dm) return;
  if (o != -1) chk_bfg(&pl[0], x, y);
  if (g_plrcount == 2 && o != -2) chk_bfg(&pl[1], x, y);
  if (o == -1 || o == -2) MN_warning(x-50, y-50, x+50, y+50);
}


void PL_drawst (player_t *p) {
  int i;
  //
  V_setrect(w_view_wdt, 120, w_viewofs_vert, w_view_hgt);
  Z_clrst();
  //
  if ((p->drawst&PL_DRAWAIR) && p->air < PL_AIR) Z_drawstair(p->air);
  if (p->drawst&PL_DRAWLIFE) Z_drawstprcnt(0, p->life);
  if (p->drawst&PL_DRAWARMOR) Z_drawstprcnt(1, p->armor);
  if (p->drawst&PL_DRAWWPN) {
    switch (p->wpn) {
      case 2: case 5: i = p->ammo; break;
      case 3: case 4: case 9: i = p->shel; break;
      case 6: i = p->rock; break;
      case 10: i = p->fuel; break;
      case 7: case 8: i = p->cell; break;
      default: i = p->shel; break;
    }
    Z_drawstwpn(p->wpn, i);
  }
  if (p->drawst&PL_DRAWFRAG) Z_drawstnum(p->frag);
  if (p->drawst&PL_DRAWKEYS) Z_drawstkeys(p->keys);
  if (g_plrcount == 1 && (p->drawst&PL_DRAWLIVES)) Z_drawstlives(p->lives);
}


#define PLAYER_T_IO_SIZE  xoffsetof(player_t, endofsavemark)

static int PL_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, PLAYER_T_IO_SIZE); // structure size
    for (int f = 0; f < 2; ++f) membuf_write(mbuf, &pl[f], PLAYER_T_IO_SIZE);
  }
  //
  return 0;
}


static int PL_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'players' version!\n");
      return -1;
    }
    if (membuf_read_ui16(mbuf) !=  PLAYER_T_IO_SIZE) {
      conlogf("LOADGAME ERROR: invalid 'players' size!\n");
      return -1;
    }
    for (int f = 0; f < 2; ++f) membuf_read_full(mbuf, &pl[f], PLAYER_T_IO_SIZE);
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_initvga(&spr[f]);
  for (size_t f = 0; f < sizeof(wpn)/sizeof(wpn[0]); ++f) {
    V_initvga(&wpn[f][0]);
    V_initvga(&wpn[f][1]);
    V_initvga(&wpn[f][2]);
    V_initvga(&wpn[f][3]);
    V_initvga(&wpn[f][4]);
    V_initvga(&wpn[f][5]);
  }
  //
  F_registerSaveLoad("PLAYERS", PL_savegame, PL_loadgame, SAV_NORMAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_freevga(&spr[f]);
  for (size_t f = 0; f < sizeof(wpn)/sizeof(wpn[0]); ++f) {
    V_freevga(&wpn[f][0]);
    V_freevga(&wpn[f][1]);
    V_freevga(&wpn[f][2]);
    V_freevga(&wpn[f][3]);
    V_freevga(&wpn[f][4]);
    V_freevga(&wpn[f][5]);
  }
}
