/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "config.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "bind.h"
#include "common.h"
#include "gamemode.h"
#include "error.h"
#include "files.h"
#include "game.h"
#include "netgame.h"
#include "sound.h"
#include "vga.h"


#define CFG_NAME  "doom2dvcd.rc"


int goobers = 0;
int startup_in_process = 1;


ccBool cli_as_server = CC_FALSE;
ccBool cli_as_client = CC_FALSE;
ccBool cli_mode_dm = CC_TRUE;
ccBool cli_mode_coop = CC_FALSE;


enum {
  CT_NONE,
  CT_BYTE,
  CT_WORD,
  CT_DWORD,
  CT_STRING,
  CT_SW_ON,
  CT_SW_OFF,
  CT_SW_ON_RAD,
  CT_FILES,
  CT_NOTHING
};


typedef struct {
  const char *name;
  uint8_t type;
  union {
    void *pvoid;
    uint8_t *pbyte;
    uint16_t *pword;
    uint32_t *pdword;
    char **pchar;
    ccBool *pbool;
  };
} cfg_t;


static const cfg_t cfg[] = {
  {.name="file", .pvoid=NULL, .type=CT_FILES},
  {.name="cheat", .pvoid=&g_cheats_enabled, .type=CT_SW_ON},
  {.name="vga", .pvoid=&shot_vga, .type=CT_SW_ON},
  {.name="sndvol", .pvoid=&snd_vol, .type=CT_DWORD},
  {.name="musvol", .pvoid=&mus_vol, .type=CT_DWORD},
  {.name="fullscreen", .pvoid=&fullscreen, .type=CT_SW_ON},
  {.name="window", .pvoid=&fullscreen, .type=CT_SW_OFF},
  {.name="mon", .pvoid=&g_no_monsters, .type=CT_SW_OFF},
  {.name="gamma", .pvoid=&gammaa, .type=CT_DWORD},
  {.name="warp", .pvoid=&cli_warp, .type=CT_BYTE},
  {.name="width", .pvoid=&SCRW, .type=CT_DWORD},
  {.name="height", .pvoid=&SCRH, .type=CT_DWORD},
  {.name="nomusic", .pvoid=&musdisabledc, .type=CT_SW_OFF},
  {.name="nosound", .pvoid=&snddisabledc, .type=CT_SW_OFF},
  //
  {.name="host", .pvoid=&n_host_addr, .type=CT_STRING},
  {.name="port", .pvoid=&n_host_port, .type=CT_WORD},
  //
  {.name=NULL, .pvoid=NULL, .type=CT_NOTHING},
  {.name="server", .pvoid=&cli_as_server, .type=CT_SW_ON_RAD},
  {.name="client", .pvoid=&cli_as_client, .type=CT_SW_ON_RAD},
  //
  {.name=NULL, .pvoid=NULL, .type=CT_NOTHING},
  {.name="dm", .pvoid=&cli_mode_dm, .type=CT_SW_ON_RAD},
  {.name="coop", .pvoid=&cli_mode_coop, .type=CT_SW_ON_RAD},
  //
  //{"config", cfg_file, CT_STRING},
  {.name="goobers", .pvoid=NULL, .type=CT_NOTHING},
  {.name="nettest", .pvoid=NULL, .type=CT_NOTHING},
};


static int64_t uintArg (const char *str) {
  while (*str && isspace(*str)) ++str;
  //
  if (isdigit(*str)) {
    uint64_t v = 0;
    //
    while (*str && isdigit(*str)) {
      if ((v = v*10+str[0]-'0') > 0x7fffffffULL) break;
      ++str;
    }
    //
    while (*str && isspace(*str)) ++str;
    //
    if (!str[0]) return (int)v;
  }
  //
  return -1;
}


void CFG_args (int argc, char *argv[]) {
  int f = 1;
  //
  conlogf("CFG_args: parsing command line arguments");
  //
  while (f < argc) {
    const char *s = argv[f++];
    int optfound = 0;
    //
    if (*s == '/') ++s;
    else if (*s == '-' && *(++s) == '-') ++s; // hehe, clever trick!
    if (!s[0]) continue;
    //
    for (size_t cidx = 0; cidx < sizeof(cfg)/sizeof(cfg[0]); ++cidx) {
      if (cfg[cidx].name != NULL && strcmp(s, cfg[cidx].name) == 0) {
        int64_t n;
        //
        optfound = 1;
        switch (cfg[cidx].type) {
          case CT_BYTE:
            if (f >= argc) goto missingvalue;
            if ((n = uintArg(argv[f++])) >= 0 && n <= 255) {
              *(cfg[cidx].pbyte) = n;
            } else {
              goto badvalue;
            }
            break;
          case CT_WORD:
            if (f >= argc) goto missingvalue;
            if ((n = uintArg(argv[f++])) >= 0 && n <= 0xffffu) {
              *(cfg[cidx].pword) = n;
            } else {
              goto badvalue;
            }
            break;
          case CT_DWORD:
            if (f >= argc) goto missingvalue;
            if ((n = uintArg(argv[f++])) >= 0 && n <= 0xffffffffu) {
              *(cfg[cidx].pdword) = n;
            } else {
              goto badvalue;
            }
            break;
          case CT_STRING:
            if (f >= argc) goto missingvalue;
            if ((*cfg[cidx].pchar) != NULL) free(*cfg[cidx].pchar);
            (*cfg[cidx].pchar) = strdup(argv[f++]);
            break;
          case CT_SW_ON:
            *(cfg[cidx].pbool) = CC_TRUE;
            break;
          case CT_SW_OFF:
            *(cfg[cidx].pbool) = CC_FALSE;
            break;
          case CT_SW_ON_RAD:
            *(cfg[cidx].pbool) = CC_TRUE;
            //
            for (size_t f = cidx; f > 0; --f) {
              if (cfg[f-1].type != CT_SW_ON_RAD) break;
              *(cfg[f-1].pbool) = CC_FALSE;
            }
            for (++cidx; cidx < sizeof(cfg)/sizeof(cfg[0]); ++cidx) {
              if (cfg[cidx].type != CT_SW_ON_RAD) break;
              *(cfg[cidx].pbool) = CC_FALSE;
            }
            //
            break;
          case CT_FILES:
            while (f < argc && argv[f][0] != '-') {
              F_addwad(argv[f], 1);
              ++f;
            }
            break;
          case CT_NOTHING:
            break;
          default:
            ERR_failinit("invalid type in cfg");
missingvalue:
            ERR_failinit("missing value for option '%s'", s);
badvalue:
            ERR_failinit("bad value for option '%s'", s);
        }
        break;
      }
    }
    if (!optfound) ERR_failinit("invalid option: '%s'", s);
  }
}


static const char *findConfigFile (void) {
  const char *bindir = get_bin_dir();
  const char *home = get_home_dir();
  static char res[4096];
  //
  if (bindir != NULL && bindir[0]) {
    sprintf(res, "%s/%s", bindir, CFG_NAME);
    if (access(res, R_OK) == 0) return res;
  }
  //
  if (home != NULL && home[0]) {
    sprintf(res, "%s/%s/%s", home, SAVE_DIR, CFG_NAME);
    if (access(res, R_OK) == 0) return res;
  }
  //
  return NULL;
}


static void load_config (FILE *fl) {
  char s[1024];
  //
  while (fgets(s, sizeof(s), fl) != NULL) cmdcon_exec(s);
}


void CFG_load (void) {
  const char *cfgfilename = findConfigFile();
  const char *home = get_home_dir();
  static char fn[4096];
  struct stat stc, stcc;
  FILE *fl;
  //
  sprintf(fn, "%s/%s/config.cfg", home, SAVE_DIR);
  //
  if (cfgfilename == NULL) {
loadsavedcfg:
    if (access(fn, R_OK) == 0) {
      if ((fl = fopen(fn, "r")) != NULL) {
        conlogf("CFG_load: loading config file: '%s'", fn);
        load_config(fl);
        fclose(fl);
        goto autoexec;
      }
    }
    conlogf("config file not found, using defaults");
    goto autoexec;
  }
  //
  if (stat(cfgfilename, &stc) == 0) {
    if (stat(fn, &stcc) == 0) {
      // have both configs
      if (stcc.st_mtime > stc.st_mtime) goto loadsavedcfg; // saved is better
    }
  } else {
    // no config, load saved
    goto loadsavedcfg;
  }
  //
  conlogf("CFG_load: loading config file: '%s'", cfgfilename);
  if ((fl = fopen(cfgfilename, "r")) == NULL) goto loadsavedcfg;
  load_config(fl);
  fclose(fl);
autoexec:
  sprintf(fn, "%s/autoexec.cfg", get_bin_dir());
  if (access(fn, R_OK) == 0 && (fl = fopen(fn, "r")) != NULL) {
    conlogf("CFG_load: loading config file: '%s'", fn);
    load_config(fl);
    fclose(fl);
    return;
  }
  sprintf(fn, "%s/%s/autoexec.cfg", home, SAVE_DIR);
  if (access(fn, R_OK) == 0 && (fl = fopen(fn, "r")) != NULL) {
    conlogf("CFG_load: loading config file: '%s'", fn);
    load_config(fl);
    fclose(fl);
    return;
  }
}


CONCMD(show_key_names, "show SDL key names") {
  CMDCON_HELP();
  for (int f = 1; f < SDLK_LAST; ++f) {
    const char *s = SDL_GetKeyName(f);
    //
    if (s != NULL && strcasecmp(s, "unknown key") != 0) conlogf("%s", s);
  }
}


void CFG_save (void) {
  static char fn[4096];
  const char *home = get_home_dir();
  FILE *fo;
  //
  sprintf(fn, "%s/%s", home, SAVE_DIR);
  xmkdir(fn);
  //
  sprintf(fn, "%s/%s/config.cfg", home, SAVE_DIR);
  if ((fo = fopen(fn, "w")) != NULL) {
    fprintf(fo, "# autogenerated file; do not edit!\n");
    cmdcon_enum(cmdcon_lambda(int, (CmdConCmd *cmd) {
      if ((cmd->flags&CMDCON_FLAGS_PERSISTENT) && cmd->type != CMDCON_CMD) {
        switch (cmd->type) {
          case CMDCON_BOOL: fprintf(fo, "%s %s\n", cmd->name, (*(cmd->vbool) ? "true" : "false")); break;
          case CMDCON_BYTE: fprintf(fo, "%s %u\n", cmd->name, *(cmd->vu8)); break;
          case CMDCON_WORD: fprintf(fo, "%s %u\n", cmd->name, *(cmd->vu16)); break;
          case CMDCON_DWORD: fprintf(fo, "%s %u\n", cmd->name, *(cmd->vu32)); break;
          case CMDCON_INT: fprintf(fo, "%s %d\n", cmd->name, *(cmd->vint)); break;
          case CMDCON_STRING: fprintf(fo, "%s ", cmd->name); fputqstr(fo, *(cmd->vstr)); fputc('\n', fo); break;
          default: ;
        }
      }
      return 0;
    }));
    fprintf(fo, "screen_mode %d %d\n", SCRWorig, SCRHorig);
    fprintf(fo, "music %s\n", (musdisabledc ? "false" : "true"));
    fprintf(fo, "sound %s\n", (snddisabledc ? "false" : "true"));
    // write bindings
    fprintf(fo, "# key bindings\n");
    fprintf(fo, "bind reset\n");
    bind_save_all(fo);
    //
    fclose(fo);
  }
}
