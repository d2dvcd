/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "smoke.h"

#include <stdlib.h>

#include "common.h"
#include "cmdcon.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "jimapi.h"
#include "misc.h"
#include "sound.h"
#include "vga.h"
#include "view.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_PARTFX  (500)

#define MAXSR  (20)
#define SMSN   (10)
#define FLSN   (8)


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  int x, y, xv, yv;
  uint8_t t, s;
  short o;
} smoke_t;


////////////////////////////////////////////////////////////////////////////////
static smoke_t sm[MAX_PARTFX];
static int sr_r, sxr[MAXSR], syr[MAXSR];
static int lsm;

static vgaimg spr[SMSN], fspr[FLSN];
static const snd_t *burnsnd;
static int burntm = 0;


const uint8_t flametab[16] = {
  0xBC, 0xBA, 0xB8, 0xB6, 0xB4, 0xB2, 0xB0, 0xD5, 0xD6, 0xD7, 0xA1, 0xA0, 0xE3, 0xE2, 0xE1, 0xE0
};


////////////////////////////////////////////////////////////////////////////////
void SMK_init (void) {
  for (int i = 0; i < MAX_PARTFX; ++i) sm[i].t = 0;
  lsm = 0;
  burntm = 0;
}


void SMK_alloc (void) {
  burnsnd = Z_getsnd("BURN");
  for (int i = 0; i < SMSN; ++i) Z_getspr(&spr[i], "SMOK", i, 0, NULL);
  for (int i = 0; i < FLSN; ++i) Z_getspr(&fspr[i], "FLAM", i, 0, NULL);
  for (int i = 0; i < MAXSR; ++i) {
    sxr[i] = myrand1(256*2+1)-256;
    syr[i] = myrand1(256*2+1)-256;
  }
  sr_r = 0;
}


static inline void inclast (void) {
  if (++lsm >= MAX_PARTFX) lsm = 0;
}


void SMK_act (void) {
  int ox, oy;
  obj_t o;
  //
  o.type = OBJT_SMOKE;
  if (burntm) --burntm;
  for (int i = 0;i < MAX_PARTFX;++i) {
    if (sm[i].t) {
      if (sm[i].s) {
        ox = sm[i].x;
        oy = sm[i].y;
        sm[i].xv = Z_dec(sm[i].xv, 20);
        sm[i].yv = Z_dec(sm[i].yv, 20);
        sm[i].x += sm[i].xv/2;
        sm[i].y += sm[i].yv/2;
        if (!Z_canfit(OBJT_SMOKE, sm[i].x>>8, (sm[i].y>>8)+3, 3, 7)) {
          sm[i].x = ox;
          sm[i].y = oy;
        } else if (Z_inwater(sm[i].x>>8, (sm[i].y>>8)+3, 3, 7)) {
          sm[i].x = ox;
          sm[i].y = oy;
        }
        ox = sm[i].x;
        oy = sm[i].y;
        sm[i].x += sm[i].xv/2;
        sm[i].y += sm[i].yv/2;
        if (!Z_canfit(OBJT_SMOKE, sm[i].x>>8, (sm[i].y>>8)+3, 3, 7)) {
          sm[i].x = ox;
          sm[i].y = oy;
        } else if (Z_inwater(sm[i].x>>8, (sm[i].y>>8)+3, 3, 7)) {
          sm[i].x = ox;
          sm[i].y = oy;
        }
        if (sm[i].o != -3) {
          o.x = sm[i].x>>8;
          o.y = sm[i].y>>8;
          o.xv = sm[i].xv>>10;
          o.yv = sm[i].yv>>10;
          o.vx = o.vy = 0;
          if (!(g_time&3)) Z_hit(&o, 1, sm[i].o, HIT_FLAME);
        }
      } else {
        ox = sm[i].x;
        oy = sm[i].y;
        sm[i].xv = Z_dec(sm[i].xv, 20);
        sm[i].yv = Z_dec(sm[i].yv, 20);
        sm[i].x += sm[i].xv;
        sm[i].y += sm[i].yv;
        if (!Z_canfit(OBJT_SMOKE, sm[i].x>>8, (sm[i].y>>8)+3, 3, 7)) {
          sm[i].x = ox;
          sm[i].y = oy;
        } else if (Z_inwater(sm[i].x>>8, (sm[i].y>>8)+3, 3, 7)) {
          sm[i].x = ox;
          sm[i].y = oy;
        }
      }
      --sm[i].t;
    }
  }
}


void SMK_draw (void) {
  for (int i = 0;i < MAX_PARTFX; ++i) {
    if (sm[i].t) {
      int s;
      //
      switch (sm[i].s) {
        case 0:
          if ((s = sm[i].t) >= (SMSN-1)*3) s = 0; else s = SMSN-1-s/3;
          V_sprf(Z_MAP2SCR_X(sm[i].x>>8), Z_MAP2SCR_Y(sm[i].y>>8), &spr[s], &smoke_sprf);
          break;
        case 1:
          if ((s = sm[i].t) >= (FLSN-1)) s = 0; else s = FLSN-1-s;
          V_sprf(Z_MAP2SCR_X(sm[i].x>>8), Z_MAP2SCR_Y(sm[i].y>>8), &fspr[s], &flame_sprf);
          break;
      }
    }
  }
}


void SMK_add (int x, int y, int xv, int yv, uint8_t t, uint8_t s, short o) {
  int i;
  //
  if (!Z_canfit(OBJT_SMOKE, x>>8, (y>>8)+3, 3, 7)) return;
  if (Z_inwater(x>>8, (y>>8)+3, 3, 7)) { FX_bubble(x>>8, y>>8, xv, yv, 1); return; }
  i = lsm;
  sm[i].x = x;
  sm[i].y = y;
  sm[i].xv = xv;
  sm[i].yv = yv;
  sm[i].t = t;
  sm[i].s = s;
  sm[i].o = o;
  inclast();
}


void SMK_gas (int x0, int y0, int xr, int yr, int xv, int yv, int k) {
  int sxv, syv;
  //
  xv = -xv;
  yv = -yv;
  sxv = xv*k;
  syv = yv*k;
  k = K8MAX(abs(xv), abs(yv));
  if (!k) return;
  for (int i = 0;i <= k;i += 3) {
    int x = ((xv*i/k+x0)<<8)+sxr[sr_r]*xr;
    int y = ((yv*i/k+y0)<<8)+syr[sr_r]*yr;
    //
    if (++sr_r >= MAXSR) sr_r = 0;
    SMK_add(x, y, sxv, syv, SMSN*3, 0, -3);
  }
}


void SMK_flame (int x0, int y0, int ox, int oy, int xr, int yr, int xv, int yv, int k, int o) {
  int sxv, syv;
  //
  sxv = xv*k;
  syv = yv*k;
  xv = xv-(ox<<8);
  yv = yv-(oy<<8);
  k = K8MAX(abs(xv), abs(yv));
  if (!k) return;
  if (!burntm) burntm = Z_sound(burnsnd, 128);
  for (int i = 0; i <= k; i += 200) {
    int x = xv*i/k+(x0<<8)+sxr[sr_r]*xr;
    int y = yv*i/k+(y0<<8)+syr[sr_r]*yr;
    //
    if (++sr_r >= MAXSR) sr_r = 0;
    SMK_add(x, y, sxv, syv, FLSN, 1, o);
  }
}


static int SMK_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt = 0;
    //
    for (int f = 0; f < MAX_PARTFX; ++f) if (sm[f].t) ++cnt;
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(sm[0])); // structure size
    membuf_write_ui32(mbuf, cnt);
    for (int f = 0; f < MAX_PARTFX; ++f) if (sm[f].t) membuf_write(mbuf, &sm[f], sizeof(sm[0]));
  }
  return 0;
}


static int SMK_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    SMK_init();
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'smoke' version!\n");
      goto error;
    }
    if (membuf_read_ui16(mbuf) != sizeof(sm[0])) {
      conlogf("LOADGAME ERROR: invalid 'smoke' size!\n");
      goto error;
    }
    cnt = membuf_read_ui32(mbuf);
    if (cnt > MAX_PARTFX) {
      conlogf("LOADGAME ERROR: too many 'smoke'!\n");
      cnt = MAX_PARTFX;
    }
    if (membuf_read_full(mbuf, sm, cnt*sizeof(sm[0])) < 0) {
      conlogf("LOADGAME ERROR: can't read 'smoke'!\n");
      goto error;
    }
  }
  //
  if (waserror) SMK_init();
  return 0; // we are optional anyway
error:
  // we are optional anyway
  SMK_init();
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_initvga(&spr[f]);
  for (size_t f = 0; f < sizeof(fspr)/sizeof(fspr[0]); ++f) V_initvga(&fspr[f]);
  //
  F_registerSaveLoad("SMOKEFX", SMK_savegame, SMK_loadgame, SAV_OPTIONAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_freevga(&spr[f]);
  for (size_t f = 0; f < sizeof(fspr)/sizeof(fspr[0]); ++f) V_freevga(&fspr[f]);
}
