/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "fx.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "membuffer.h"
#include "map.h"
#include "misc.h"
#include "saveload.h"
#include "view.h"
#include "vga.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_FX  (300)


////////////////////////////////////////////////////////////////////////////////
enum {
  NONE,
  TFOG,
  IFOG,
  BUBL
};


typedef struct GCC_PACKED {
  int x, y, xv, yv;
  char t, s;
} fx_t;


////////////////////////////////////////////////////////////////////////////////
static vgaimg spr[15];
static const snd_t *bsnd[2];
static char sprd[15];
static fx_t fx[MAX_FX];
static char bubsn;
static int maxfx;


////////////////////////////////////////////////////////////////////////////////
void FX_alloc (void) {
  int f;
  //
  for (f = 0; f < 10; ++f) Z_getspr(&spr[f], "TFOG", f, 0, sprd+f);
  for (; f < 15; ++f) Z_getspr(&spr[f], "IFOG", f-10, 0, sprd+f);
  bsnd[0] = Z_getsnd("BUBL1");
  bsnd[1] = Z_getsnd("BUBL2");
}


void FX_init (void) {
  for (int f = 0; f < MAX_FX; ++f) fx[f].t = 0;
  bubsn = 0;
  maxfx = 0;
}


static void FX_compact (void) {
  int src, dest = 0;
  //
  while (dest < MAX_FX && fx[dest].t) ++dest; // find place for this dot
  if (dest >= MAX_FX) {
    // the thing that should not be
    maxfx = MAX_FX;
    return;
  }
  //
  src = dest;
  while (src < maxfx) {
    if (!fx[src].t) { ++src; continue; } // this dot is not used, go on
    // copy used dot
    fx[dest++] = fx[src++];
  }
  maxfx = dest;
}


void FX_act (void) {
  int needcomp = 0;
  uint8_t b;
  //
  bubsn = 0;
  for (int f = 0; f < maxfx; ++f) {
    switch (fx[f].t) {
      case TFOG:
        if (++fx[f].s >= 20) { fx[f].t = 0; needcomp = 1; }
        break;
      case IFOG:
        if (++fx[f].s >= 10) { fx[f].t = 0; needcomp = 1; }
        break;
      case BUBL:
        fx[f].yv -= 5;
        fx[f].xv = Z_dec(fx[f].xv, 20);
        fx[f].x += fx[f].xv;
        fx[f].y += fx[f].yv;
        if ((b = FIELD_MAP(fx[f].x>>11, fx[f].y>>11)) <= TILE_STEP || b > 7) { fx[f].t = 0;  needcomp = 1; }
        break;
    }
  }
  if (needcomp) FX_compact();
}


void FX_draw (void) {
  for (int f = 0; f < maxfx; ++f) {
    int s = -1;
    //
    switch (fx[f].t) {
      case TFOG:
        s = fx[f].s/2;
        break;
      case IFOG:
        s = fx[f].s/2+10;
        break;
      case BUBL:
        V_putpixel(Z_MAP2SCR_X(fx[f].x>>8), Z_MAP2SCR_Y(fx[f].y>>8), 0xC0+fx[f].s);
        continue;
    }
    if (s >= 0) Z_drawspr(fx[f].x, fx[f].y, &spr[s], sprd[s]);
  }
}


static int findfree (void) {
  for (int f = 0; f < maxfx; ++f) if (!fx[f].t) return f;
  if (maxfx < MAX_FX) return maxfx++;
  for (int f = 0; f < MAX_FX; ++f) if (fx[f].t == IFOG) return f;
  return 0;
}


void FX_tfog (int x, int y) {
  int f = findfree();
  //
  fx[f].t = TFOG;
  fx[f].s = 0;
  fx[f].x = x;
  fx[f].y = y;
}


void FX_ifog (int x, int y) {
  int f = findfree();
  //
  fx[f].t = IFOG;
  fx[f].s = 0;
  fx[f].x = x;
  fx[f].y = y;
}


void FX_bubble (int x, int y, int xv, int yv, int n) {
  if (!bubsn) {
    Z_sound(bsnd[myrandbit1()], 128);
    bubsn = 1;
  }
  //
  for (; n > 0; --n) {
    int f = findfree();
    //
    fx[f].t = BUBL;
    fx[f].s = myrand1(4);
    fx[f].x = (x<<8)+myrand1(513)-256;
    fx[f].y = (y<<8)+myrand1(513)-256;
    fx[f].xv = xv;
    fx[f].yv = yv-myrand1(256)-768;
  }
}


static int FX_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(fx_t)); // structure size
    membuf_write_ui32(mbuf, maxfx);
    membuf_write(mbuf, fx, maxfx*sizeof(fx[0]));
  }
  return 0;
}


static int FX_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    FX_init();
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'fx' version!\n");
      goto error;
    }
    if (membuf_read_ui16(mbuf) != sizeof(fx_t)) {
      conlogf("LOADGAME ERROR: invalid 'fx' size!\n");
      goto error;
    }
    maxfx = membuf_read_ui32(mbuf);
    if (maxfx > MAX_FX) {
      conlogf("LOADGAME ERROR: too many fx!\n");
      maxfx = MAX_FX;
    }
    if (membuf_read_full(mbuf, fx, maxfx*sizeof(fx[0])) < 0) {
      conlogf("LOADGAME ERROR: can't read fx!\n");
      goto error;
    }
  }
  //
  if (waserror) FX_init();
  return 0; // we are optional anyway
error:
  // we are optional anyway
  FX_init();
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_initvga(&spr[f]);
  //
  F_registerSaveLoad("PARTICLESFX", FX_savegame, FX_loadgame, SAV_OPTIONAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_freevga(&spr[f]);
}
