/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "saveload.h"

#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "common.h"
#include "cmdcon.h"
#include "config.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "gamemode.h"
#include "items.h"
#include "jimapi.h"
#include "keyb.h"
#include "map.h"
#include "membuffer.h"
#include "menu.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "smoke.h"
#include "sound.h"
#include "switch.h"
#include "things.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"


////////////////////////////////////////////////////////////////////////////////
#define SAVE_SIGN  "D2DVCDS0"


////////////////////////////////////////////////////////////////////////////////
char savname[10][42];
uint8_t savok[10];


////////////////////////////////////////////////////////////////////////////////
static const char *getsavfpname (int n, int ro) {
  static char fname[4096];
  const char *home = get_home_dir();
  //
  if (home == NULL) home = "/tmp";
  if (n < 0) n = 0;
  else if (n > 9) n = 9;
  //
  if (!ro) {
    sprintf(fname, "%s/%s", home, SAVE_DIR);
    xmkdir(fname);
  }
  //
  sprintf(fname, "%s/%s/savgame%d.dat", home, SAVE_DIR, n);
  //
  return fname;
}


////////////////////////////////////////////////////////////////////////////////
#define MAX_SL_CALLBACKS  (64)


typedef struct {
  char name[17]; // 16 chars and '0'
  F_saveloadCB scb;
  F_saveloadCB lcb;
  SaveGameChunkType ctype;
} SaveLoadCB;


static SaveLoadCB slCallbacks[MAX_SL_CALLBACKS];
static int slCBCount = 0;


////////////////////////////////////////////////////////////////////////////////
static int PRNG_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) membuf_write(mbuf, &g_prng, sizeof(g_prng));
  return 0;
}


static int PRNG_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) membuf_read_full(mbuf, &g_prng, sizeof(g_prng));
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
void F_registerSaveLoad (const char *chunkName, F_saveloadCB scb, F_saveloadCB lcb, SaveGameChunkType ctype) {
  if (chunkName != NULL && chunkName[0]) {
    int cblen = strlen(chunkName);
    //
    if (cblen > 16) ERR_fatal("save/load callback name too long: '%s'!", chunkName);
    if (slCBCount >= MAX_SL_CALLBACKS) ERR_fatal("too many save/load callbacks!");
    memset(slCallbacks[slCBCount].name, 0, sizeof(slCallbacks[slCBCount].name));
    strncpy(slCallbacks[slCBCount].name, chunkName, cblen);
    slCallbacks[slCBCount].scb = scb;
    slCallbacks[slCBCount].lcb = lcb;
    slCallbacks[slCBCount].ctype = ctype;
    ++slCBCount;
    //conlogf("S/L callback: '%s'\n", chunkName);
  } else {
    ERR_fatal("trying to registed unnamed save/load callback!");
  }
}


GCC_CONSTRUCTOR_USER {
  F_registerSaveLoad("PRNG", PRNG_savegame, PRNG_loadgame, SAV_OPTIONAL); // optional
}


////////////////////////////////////////////////////////////////////////////////
void F_getsavnames (void) {
  for (int f = 0; f < 10; ++f) {
    const char *fname = getsavfpname(f, 1);
    FILE *fi;
    //
    memset(savname[f], 0, 24);
    savok[f] = 0;
    //
    if ((fi = fopen(fname, "rb")) != NULL) {
      WAD2Header whdr;
      WAD2DirEntry nfo;
      //
      if (fread(&whdr, sizeof(whdr), 1, fi) != 1 ||
          memcmp(whdr.sign, WAD2SIGN, WAD2SIGNSIZE) != 0 ||
          whdr.dircount == 0 || whdr.dirofs < sizeof(whdr) ||
          fseek(fi, whdr.dirofs, SEEK_SET) != 0) {
        //conlogf("not WAD2 file: '%s'!\n", fname);
        fclose(fi);
        continue;
      }
      //
      memset(&nfo, 0, sizeof(nfo));
      for (uint32_t didx = 0; didx < whdr.dircount; ++didx) {
        WAD2DirEntry de;
        //
        if (fread(&de, WAD2DESIZE, 1, fi) != 1) { nfo.name[0] = 1; break; }
        if (strcasecmp(de.name, "SG_INFO") == 0) nfo = de;
      }
      //TODO: stricter checking
      if (nfo.name[0] && nfo.type == 'S' && nfo.comp == 0) {
        if (fseek(fi, nfo.offset+1, SEEK_SET) == 0) {
          if (fread(savname[f], 24, 1, fi) == 1) savok[f] = 1;
        }
      }
      //
      fclose(fi);
    }
    //
    if (!savok[f]) memset(savname[f], 0, 24);
  }
}


int Fl_savegame (const char *fname, const char *gamename) {
  FILE *fo;
  //
  if (fname != NULL && fname[0] && gamename != NULL && (fo = fopen(fname, "wb")) != NULL) {
    MemBuffer *chunks[MAX_SL_CALLBACKS], *nfo;
    int waserror = 0;
    //
    for (int f = 0; f < MAX_SL_CALLBACKS; ++f) chunks[f] = NULL;
    //
    // create 'info' section
    {
      MemBuffer *buf = membuf_new_write();
      char gname[256];
      //
      if (buf == NULL) {
badnfo: fclose(fo);
        unlink(fname);
        return -1;
      }
      // chunk version
      membuf_write_ui8(buf, 0);
      // game name
      memset(gname, 0, sizeof(gname));
      strncpy(gname, gamename, sizeof(gname)-1);
      if (membuf_write(buf, gname, sizeof(gname)) < 0) goto badnfo;
      // TODO:game time, screenshot?
      nfo = buf;
    }
    //
    for (int f = 0; f < slCBCount; ++f) {
      if (slCallbacks[f].scb != NULL) {
        MemBuffer *buf = membuf_new_write();
        //
        if (buf == NULL) { waserror = 1; break; }
        if (slCallbacks[f].scb(buf, 0) != 0 || membuf_error(buf) != 0) {
          membuf_free(buf);
          waserror = 1;
          conlogf("error saving chunk '%s'\n", slCallbacks[f].name);
          break;
        }
        if (slCallbacks[f].ctype == SAV_OPTIONAL && membuf_used(buf) == 0) membuf_free(buf); else chunks[f] = buf;
      }
    }
    // call 'finalizers'
    for (int f = 0; f < slCBCount; ++f) if (slCallbacks[f].scb != NULL) slCallbacks[f].scb(NULL, waserror);
    //
    chunks[slCBCount] = nfo;
    //
    if (waserror == 0) {
      WAD2Header whdr;
      WAD2DirEntry de[MAX_SL_CALLBACKS+1];
      int depos = 0;
      //
      waserror = 1;
      //
      memset(de, 0, sizeof(de));
      memcpy(whdr.sign, WAD2SIGN, WAD2SIGNSIZE);
      whdr.dircount = 0;
      whdr.dirofs = sizeof(whdr);
      for (int f = 0; f <= slCBCount; ++f) if (chunks[f] != NULL) { whdr.dirofs += membuf_used(chunks[f]); ++whdr.dircount; }
      //
      if (fwrite(&whdr, sizeof(whdr), 1, fo) != 1) goto quit;
      //
      for (int f = 0; f <= slCBCount; ++f) {
        if (chunks[f] != NULL) {
          de[depos].offset = ftell(fo);
          de[depos].dsize = de[depos].size = membuf_used(chunks[f]);
          de[depos].type = 'S';
          de[depos].comp = 0;
          de[depos].unused = 0;
          de[depos].zero = 0;
          strncpy(de[depos].name, (f < slCBCount ? slCallbacks[f].name : "SG_INFO"), 16);
          ++depos;
          //
          if (fwrite(membuf_data_const(chunks[f]), membuf_used(chunks[f]), 1, fo) != 1) {
            conlogf("error saving chunk '%s' to disk\n", de[depos-1].name);
            goto quit;
          }
        }
      }
      //
      for (int f = 0; f < depos; ++f) if (fwrite(&de[f], WAD2DESIZE, 1, fo) != 1) goto quit;
      //
      waserror = 0;
    }
quit:
    fclose(fo);
    if (waserror) unlink(fname);
    for (int f = slCBCount; f >= 0; --f) membuf_free(chunks[f]);
    //
    if (waserror) {
      conlogf("can't saved game to '%s'\n", fname);
    } else {
      conlogf("game saved to '%s'\n", fname);
    }
    //
    return (waserror ? -1 : 0);
  }
  //
  return -1;
}


int Fl_loadgame (const char *fname) {
  FILE *fi;
  //
  if (fname != NULL && fname[0] && (fi = fopen(fname, "rb")) != NULL) {
    WAD2Header whdr;
    char loaded[MAX_SL_CALLBACKS];
    int waserror = 0;
    //
    if (fread(&whdr, sizeof(whdr), 1, fi) != 1 ||
        memcmp(whdr.sign, WAD2SIGN, WAD2SIGNSIZE) != 0 ||
        whdr.dircount == 0 || whdr.dirofs < sizeof(whdr) ||
        fseek(fi, whdr.dirofs, SEEK_SET) != 0) {
      conlogf("not WAD2 file: '%s'!\n", fname);
      fclose(fi);
      return -1;
    }
    //
    memset(loaded, 0, sizeof(loaded));
    for (uint32_t didx = 0; didx < whdr.dircount && !waserror; ++didx) {
      WAD2DirEntry de;
      int found = 0;
      //
      if (fread(&de, WAD2DESIZE, 1, fi) != 1) { waserror = 1; break; }
      de.zero = 0;
      //
      for (int f = 0; f < slCBCount; ++f) {
        if (strcmp(de.name, slCallbacks[f].name) == 0) {
          found = 1;
          if (loaded[f]) {
            conlogf("duplicate chunk '%s' in savegame\n", de.name);
            // but load it anyway
          } else {
            //conlogf("loading chunk '%s' from savegame\n", de.name);
            loaded[f] = 1;
          }
          //
          if (slCallbacks[f].lcb != NULL) {
            MemBuffer *buf;
            void *data;
            //
            if (de.size >= 1024*1024*32) {
              conlogf("chunk '%s' too big\n", de.name);
              waserror = 1;
              break;
            }
            if ((data = malloc(de.size+1)) == NULL) {
              conlogf("chunk '%s' out of memory\n", de.name);
              waserror = 1;
              break;
            }
            //
            if (de.size > 0) {
              long opos = ftell(fi);
              //
              fseek(fi, de.offset, SEEK_SET);
              if (fread(data, de.size, 1, fi) != 1) {
                free(data);
                conlogf("error reading chunk '%s' from disk!\n", de.name);
                waserror = 1;
                break;
              }
              fseek(fi, opos, SEEK_SET);
            }
            //
            if ((buf = membuf_new_read_free(data, de.size)) == NULL) {
              free(data);
              conlogf("chunk '%s' out of memory\n", de.name);
              waserror = 1;
              break;
            }
            //
            if (slCallbacks[f].lcb(buf, 0) != 0 || membuf_error(buf) != 0) {
              conlogf("error reading chunk '%s'!\n", de.name);
              waserror = 1;
            }
            //
            membuf_free(buf);
          }
          break;
        }
      }
      //
      if (!found && strcmp(de.name, "SG_INFO") != 0) conlogf("unknown chunk: '%s'!\n", de.name);
    }
    fclose(fi);
    //
    for (int f = 0; f < slCBCount; ++f) if (slCallbacks[f].lcb != NULL) slCallbacks[f].lcb(NULL, waserror);
    if (!waserror) {
      for (int f = 0; f < slCBCount; ++f) {
        if (slCallbacks[f].ctype == SAV_NORMAL && !loaded[f]) {
          conlogf("missing chunk: '%s'!\n", slCallbacks[f].name);
          waserror = 1;
        }
      }
    }
    //
    return (waserror ? -1 : 0);
  }
  //
  return -1;
}


int F_savegame (int n, const char *gamename) {
  return Fl_savegame(getsavfpname(n, 0), gamename);
}


int F_loadgame (int n) {
  return Fl_loadgame(getsavfpname(n, 1));
}


static char *buildSaveFileName (const char *fname) {
  if (fname != NULL && fname[0]) {
    int len = strlen(fname);
    //
    if (fname[len-1] != '/') {
      char *res = calloc(len+32, sizeof(char)), *e;
      //
      strcpy(res, fname);
      e = strrchr(res, '.');
      if (e == NULL || strcasecmp(e, ".dsg") != 0) strcat(res, ".dsg");
      //
      return res;
    }
  }
  return NULL;
}


CONCMD(save, "save game\nsave filename [gamename]") {
  char *fname;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc < 2 || argc > 3) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  if ((fname = buildSaveFileName(argv[1])) != NULL) {
    conlogf("saving game to '%s'...\n", fname);
    Fl_savegame(fname, (argc > 2 ? argv[2] : "untitled"));
    free(fname);
  }
}


CONCMD(save_game, "save game to the given slot\nsave slot [gamename]") {
  int slot;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc < 2 || argc > 3) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  if ((slot = cmdcon_parseint(argv[1], -1, 0, 9, NULL)) < 0) {
    conlogf("%s: invalid slot number: '%s'!\n", argv[0], argv[1]);
    return;
  }
  conlogf("saving game to slot #%d...\n", slot);
  F_savegame(slot, (argc > 2 ? argv[2] : "untitled"));
}


CONCMD(load, "load game\nload filename") {
  char *fname;
  //
  CMDCON_HELP();
  CMDCON_CHECK_NONET();
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  if ((fname = buildSaveFileName(argv[1])) != NULL) {
    conlogf("load game from '%s'...\n", fname);
    if (access(fname, R_OK) == 0) {
      if (load_game_fn(fname) == 0) conlogf("game loaded");
    } else {
      conlogf("FILE NOT FOUND!\n");
    }
    free(fname);
  }
}


CONCMD(load_game, "load game from the given slot\nload slot [gamename]") {
  int slot;
  //
  CMDCON_HELP();
  CMDCON_CHECK_NONET();
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  if ((slot = cmdcon_parseint(argv[1], -1, 0, 9, NULL)) < 0) {
    conlogf("%s: invalid slot number: '%s'!\n", argv[0], argv[1]);
    return;
  }
  conlogf("loading game from slot #%d...\n", slot);
  if (load_game(slot) == 0) conlogf("game loaded");
}
