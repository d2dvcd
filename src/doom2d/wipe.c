/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "wipe.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "cmdcon.h"
#include "error.h"
#include "fx.h"
#include "menu.h"
#include "misc.h"
#include "netgame.h"
#include "vga.h"


////////////////////////////////////////////////////////////////////////////////
// 0: none
// >=1: doom melt
static int r_wipefx = 1;
CONVAR_INT(r_wipefx, r_wipefx, CMDCON_FLAGS_PERSISTENT, "screen melt fx (0: disable)");


////////////////////////////////////////////////////////////////////////////////
static int *meltColsY = NULL;
static int meltSize = 0;


#define VID_HQ_X  (1)

static void melt_init (void) {
  int ww = VID_HQ_X*2;
  //
  if (meltSize < SCRW) {
    meltColsY = realloc(meltColsY, SCRW*sizeof(int));
    meltSize = SCRW;
  }
  //
  meltColsY[0] = -myrand1(48)*VID_HQ_X;
  for (int x = 1; x < ww; ++x) meltColsY[x] = meltColsY[x-1];
  //
  for (int x = ww; x < SCRW; x += ww) {
    int r = myrand1(4)*VID_HQ_X*4;
    //
    r = meltColsY[x-1]+(myrand1(2) ? r : -r);
    if (r < -48*VID_HQ_X) r = -48*VID_HQ_X; else if (r > 0) r = 0;
    for (int f = x; f < x+ww && f < SCRW; ++f) meltColsY[f] = r;
  }
}


// copy column from oldscreen
static void copyColumn (int x, int y) {
  if (y < 0) y = 0;
  if (x >= 0 && x < SCRW && y < SCRH) {
    Uint32 *src = (Uint32 *)((Uint8 *)oldscreen->pixels+x*4);
    Uint32 *dst = (Uint32 *)((Uint8 *)screen->pixels+y*screen->pitch+x*4);
    //
    for (; y < SCRH; ++y) {
      *dst = *src;
      src = (Uint32 *)((Uint8 *)src+oldscreen->pitch);
      dst = (Uint32 *)((Uint8 *)dst+screen->pitch);
    }
  }
}


// return CC_TRUE if melt is NOT complete
static ccBool melt_step (void) {
  ccBool res = CC_FALSE;
  //
  for (int x = 0; x < SCRW; ++x) {
    if ((meltColsY[x] += 12*VID_HQ_X) < SCRH) {
      // still sliding
      res = CC_TRUE;
    }
    copyColumn(x, meltColsY[x]);
  }
  //
  return res;
}


static void melt_deinit (void) {
  if (meltColsY != NULL) free(meltColsY);
  meltColsY = NULL;
  meltSize = 0;
}


////////////////////////////////////////////////////////////////////////////////
#define SINP   (256)
#define SINPM  (SINP-1)
#define W      (SINP/100)


////////////////////////////////////////////////////////////////////////////////
static int stdsin[SINP] = {
 0, 1608, 3215, 4821, 6423, 8022, 9616, 11204,
 12785, 14359, 15923, 17479, 19024, 20557, 22078, 23586,
 25079, 26557, 28020, 29465, 30893, 32302, 33692, 35061,
 36409, 37736, 39039, 40319, 41575, 42806, 44011, 45189,
 46340, 47464, 48558, 49624, 50660, 51665, 52639, 53581,
 54491, 55368, 56212, 57022, 57797, 58538, 59243, 59913,
 60547, 61144, 61705, 62228, 62714, 63162, 63571, 63943,
 64276, 64571, 64826, 65043, 65220, 65358, 65457, 65516,
 65535, 65516, 65457, 65358, 65220, 65043, 64826, 64571,
 64276, 63943, 63571, 63162, 62714, 62228, 61705, 61144,
 60547, 59913, 59243, 58538, 57797, 57022, 56212, 55368,
 54491, 53581, 52639, 51665, 50660, 49624, 48558, 47464,
 46340, 45189, 44011, 42806, 41575, 40319, 39039, 37736,
 36409, 35061, 33692, 32302, 30893, 29465, 28020, 26557,
 25079, 23586, 22078, 20557, 19024, 17479, 15923, 14359,
 12785, 11204, 9616, 8022, 6423, 4821, 3215, 1608,
 0, -1608, -3215, -4821, -6423, -8022, -9616, -11204,
 -12785, -14359, -15923, -17479, -19024, -20557, -22078, -23586,
 -25079, -26557, -28020, -29465, -30893, -32302, -33692, -35061,
 -36409, -37736, -39039, -40319, -41575, -42806, -44011, -45189,
 -46340, -47464, -48558, -49624, -50660, -51665, -52639, -53581,
 -54491, -55368, -56212, -57022, -57797, -58538, -59243, -59913,
 -60547, -61144, -61705, -62228, -62714, -63162, -63571, -63943,
 -64276, -64571, -64826, -65043, -65220, -65358, -65457, -65516,
 -65535, -65516, -65457, -65358, -65220, -65043, -64826, -64571,
 -64276, -63943, -63571, -63162, -62714, -62228, -61705, -61144,
 -60547, -59913, -59243, -58538, -57797, -57022, -56212, -55368,
 -54491, -53581, -52639, -51665, -50660, -49624, -48558, -47464,
 -46340, -45189, -44011, -42806, -41575, -40319, -39039, -37736,
 -36409, -35061, -33692, -32302, -30893, -29465, -28020, -26557,
 -25079, -23586, -22078, -20557, -19024, -17479, -15923, -14359,
 -12785, -11204, -9616, -8022, -6423, -4821, -3215, -1608
};

static int sintab[SINP];
static unsigned char dmap[32*32];


static inline int isin (int a) {
  return sintab[a&SINPM];
}


static void setamp (int a) {
  for (int f = 0; f < SINP; ++f) sintab[f] = stdsin[f]*a;
}


////////////////////////////////////////////////////////////////////////////////
static void FX_trans1 (int t, const uint8_t *scr00, int scr00pitch, const uint8_t *scr11, int scr11pitch) {
  unsigned char k;
  //
  setamp(t);
  k = t;
  t *= W;
  for (int y = 0; y < SCRH; ++y) {
    for (int x = 0; x < SCRW; ++x) {
      if (dmap[(y&31)*32+(x&31)] >= k) {
        int u = x+((isin(y+t))>>16);
        int v = y+((isin(u+t))>>16);
        //
        if (u >= 0 && u < SCRW && v >= 0 && v < SCRH) {
          uint32_t c = *((uint32_t *)(((uint8_t *)scr00)+v*scr00pitch+u*4));
          //
          putPixel32(x, y, c);
        } else {
          putPixel32(x, y, 0);
        }
      } else {
        uint32_t c = *((uint32_t *)(((uint8_t *)scr11)+y*scr11pitch+x*4));
        //
        putPixel32(x, y, c);
      }
    }
  }
}


static void init_fx1sin (void) {
  int rr = 32*32;
  //
  memset(dmap, 0, 32*32);
  for (int f = 1; f < 64; ++f) {
    for (int l = 32*32/64; l; --l, --rr) {
      int r = myrand1(rr), j;
      //
      for (j = 0; r; --r, ++j) {
        for(; dmap[j]; ++j) ;
      }
      for (; dmap[j]; ++j);
      dmap[j] = f;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static int cur_sin_step;
static void *scr00, *scr11;
static int scr00p, scr11p;


static void sin_init (void) {
  cur_sin_step = 0;
  //
  scr00 = malloc(oldscreen->pitch*oldscreen->h);
  scr00p = oldscreen->pitch;
  memcpy(scr00, oldscreen->pixels, oldscreen->pitch*oldscreen->h);
  //
  scr11 = malloc(screen->pitch*screen->h);
  scr11p = screen->pitch;
  memcpy(scr11, screen->pixels, screen->pitch*screen->h);
  //
  init_fx1sin();
}


// return CC_TRUE if melt is NOT complete
static ccBool sin_step (void) {
  if (cur_sin_step <= 32) {
    if (cur_sin_step == 0) {
      // here we have new screen rendered
      memcpy(scr11, screen->pixels, screen->pitch*screen->h);
    }
    FX_trans1(cur_sin_step*2, scr00, scr00p, scr11, scr11p);
    ++cur_sin_step;
    return CC_TRUE;
  }
  return CC_FALSE;
}


static void sin_deinit (void) {
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  void (*init) (void);
  ccBool (*step) (void);
  void (*done) (void);
} FX;


static const FX fxlist[] = {
  {melt_init, melt_step, melt_deinit},
  {sin_init, sin_step, sin_deinit},
};

static const FX *curfx;


// return CC_TRUE if we should do the wipe
ccBool wipe_init (void) {
  if (r_wipefx > 0 && n_host == NULL) {
    curfx = &fxlist[r_wipefx > sizeof(fxlist)/sizeof(fxlist[0]) ? 0 : r_wipefx-1];
    memcpy(oldscreen->pixels, screen->pixels, oldscreen->pitch*oldscreen->h);
    curfx->init();
    return CC_TRUE;
  }
  //
  return CC_FALSE;
}


void wipe_deinit (void) {
  curfx->done();
}


// return CC_TRUE if melt is NOT complete
ccBool wipe_step (void) {
  return curfx->step();
}
