/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "items.h"

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "cmdcon.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "jimapi.h"
#include "keyb.h"
#include "map.h"
#include "mapio.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "sdldrv.h"
#include "sound.h"
#include "things.h"
#include "vga.h"
#include "view.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_ITEMS  (3000)


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  obj_t o;
  int t;
  int s;
} item_t;


////////////////////////////////////////////////////////////////////////////////
static const snd_t *snd[4];
static vgaimg spr[58];
static char sprd[58];
static int tsndtm, rsndtm;
static item_t it[MAX_ITEMS];
static int max_used_items;

int itm_rtime = 1092;


////////////////////////////////////////////////////////////////////////////////
void IT_alloc (void) {
  static const char nm[][6] = {"ITEMUP", "WPNUP", "GETPOW", "ITMBK"};
  static const char snm[][4] = {
    "CLIP", "SHEL", "ROCK", "CELL", "AMMO", "SBOX", "BROK", "CELP",
    "STIM", "MEDI", "BPAK",
    "CSAW", "SHOT", "SGN2", "MGUN", "LAUN", "PLAS", "BFUG"};
  static const char n4[][4] = {"SOUL", "SMRT", "SMGT", "SMBT"};
  static const char n3[][4] = {"GOR1", "FCAN"};
  int i, j, n;
  //
  conlogf("IT_alloc: allocating items data");
  for (i = 0; i < 18; ++i) Z_getspr(&spr[i], snm[i], 0, 0, sprd+i);
  for (; i < 20; ++i) {
    Z_getspr(&spr[i], "ARM1", i-18, 0, sprd+i);
    Z_getspr(&spr[i+2], "ARM2", i-18, 0, sprd+i);
  }
  i += 2;
  for (; i < 26; ++i) Z_getspr(&spr[i],"MEGA", i-22, 0, sprd+i);
  for (; i < 30; ++i) Z_getspr(&spr[i],"PINV", i-26, 0, sprd+i);
  Z_getspr(&spr[30], "AQUA", 0, 0, sprd+30);
  Z_getspr(&spr[31], "KEYR", 0, 0, sprd+31);
  Z_getspr(&spr[32], "KEYG", 0, 0, sprd+32);
  Z_getspr(&spr[33], "KEYB", 0, 0, sprd+33);
  Z_getspr(&spr[34], "SUIT", 0, 0, sprd+34);
  for (n = 35, j = 0; j < 4; ++j) {
    for (i = 0; i < 4; ++i, ++n) Z_getspr(&spr[n], n4[j], i, 0, sprd+n);
  }
  for (j = 0; j < 2; ++j) {
    for (i = 0; i < 3; ++i, ++n) Z_getspr(&spr[n], n3[j], i, 0, sprd+n);
  }
  Z_getspr(&spr[57], "GUN2", 0, 0, sprd+57);
  for (i = 0; i < 4; ++i) snd[i] = Z_getsnd(nm[i]);
  for (i = 0; i < MAX_ITEMS; ++i) {
    it[i].o.type = OBJT_ITEM;
    it[i].o.r = 10;
    it[i].o.h = 8;
  }
}


void IT_init (void) {
  for (int i = 0; i < MAX_ITEMS; ++i) {
    it[i].o.type = OBJT_ITEM;
    it[i].t = I_NONE;
    it[i].o.xv = it[i].o.yv = it[i].o.vx = it[i].o.vy = 0;
  }
  tsndtm = rsndtm = 0;
  max_used_items = 0;
}


static void takesnd (int t) {
  if (!tsndtm) {
    t &= 0x7FFF;
    if (t <= I_CELP || (t >= I_BPACK && t <= I_BFG) || t == I_GUN2) { tsndtm = Z_sound(snd[1], 128); return; }
    if (t == I_MEGA || t == I_INVL || t == I_SUPER) { tsndtm = Z_sound(snd[2], 192); return; }
    tsndtm = Z_sound(snd[0], 256);
  }
}


void IT_act (void) {
  if (tsndtm) --tsndtm;
  if (rsndtm) --rsndtm;
  //
  for (int i = 0; i < max_used_items; ++i) {
    if (it[i].t) {
      if (it[i].s < 0) {
        if (++it[i].s == -8) {
          FX_ifog(it[i].o.x, it[i].o.y);
          if (!rsndtm) rsndtm = Z_sound(snd[3], 128);
        }
      } else {
        switch (it[i].t&0x7fff) {
          case I_ARM1: case I_ARM2:
            if (++it[i].s >= 18) it[i].s = 0;
            break;
          case I_MEGA: case I_INVL:
          case I_SUPER: case I_RTORCH: case I_GTORCH: case I_BTORCH:
            if (++it[i].s >= 8) it[i].s = 0;
            break;
          case I_GOR1: case I_FCAN:
            if (++it[i].s >= 6) it[i].s = 0;
            break;
        }
        if (it[i].t&0x8000) {
          int j;
          //
          if ((j = Z_moveobj(&it[i].o))&Z_FALLOUT) { it[i].t = 0; continue;}
          else if (j&Z_HITWATER) Z_splash(&it[i].o, it[i].o.r+it[i].o.h);
        }
        if (Z_overlap(&it[i].o, &pl[0].o)) {
          if (PL_give(&pl[0], it[i].t&0x7FFF)) {
            takesnd(it[i].t);
            if (g_plrcount != 1 && (it[i].t&0x7FFF) >= I_KEYR && (it[i].t&0x7FFF) <= I_KEYB) continue;
            if (!(it[i].s = -itm_rtime) || (it[i].t&0x8000)) it[i].t = 0;
            continue;
          }
        }
        if (g_plrcount == 2) {
          if (Z_overlap(&it[i].o, &pl[1].o)) {
            if (PL_give(&pl[1], it[i].t&0x7FFF)) {
              takesnd(it[i].t);
              if ((it[i].t&0x7FFF) >= I_KEYR && (it[i].t&0x7FFF) <= I_KEYB) continue;
              if (!(it[i].s = -itm_rtime) || (it[i].t&0x8000)) it[i].t = 0;
              continue;
            }
          }
        }
      }
    }
  }
}


static void IT_lighting (int i) {
  static const int plp[8] = {0,1,1,2,3,3,2,1};
  //
  if (it[i].t && it[i].s >= 0) {
    switch (it[i].t&0x7FFF) {
      case I_ARM1:
        Z_add_light(it[i].o.x, it[i].o.y-8, 32, 0, 255, 0, 210);
        break;
      case I_ARM2:
        Z_add_light(it[i].o.x, it[i].o.y-8, 32, 0, 0, 255, 210);
        break;
      case I_MEGA:
        Z_add_light(it[i].o.x, it[i].o.y-8, 48, 255, 255, 0, 210);
        break;
      case I_INVL:
        Z_add_light(it[i].o.x, it[i].o.y-8, 48, 0, 255, 0, 210);
        break;
      case I_SUPER:
        Z_add_light(it[i].o.x, it[i].o.y-8, 48, 0, 0, 255, 210);
        break;
      case I_RTORCH:
        Z_add_light(it[i].o.x, it[i].o.y-26, 64/*+4*plp[it[i].s]*/, 255, 0, 0, 210+2*plp[it[i].s]);
        break;
      case I_GTORCH:
        Z_add_light(it[i].o.x, it[i].o.y-26, 64/*+4*plp[it[i].s]*/, 0, 255, 0, 210+2*plp[it[i].s]);
        break;
      case I_BTORCH:
        Z_add_light(it[i].o.x, it[i].o.y-26, 64/*+4*plp[it[i].s]*/, 0, 0, 255, 210+2*plp[it[i].s]);
        break;
      case I_KEYR:
        Z_add_light(it[i].o.x, it[i].o.y-5, 24, 255, 0, 0, 210);
        break;
      case I_KEYG:
        Z_add_light(it[i].o.x, it[i].o.y-5, 24, 0, 255, 0, 210);
        break;
      case I_KEYB:
        Z_add_light(it[i].o.x, it[i].o.y-5, 24, 0, 0, 255, 210);
        break;
      case I_FCAN:
        Z_add_light(it[i].o.x-3, it[i].o.y-18, 48, 255, 127, 0, 210);
        break;
      case I_SUIT:
        Z_add_light(it[i].o.x, it[i].o.y-14, 42, 0, 255, 0, 210);
        break;
    }
  }
}


void IT_draw (void) {
  for (int i = 0; i < max_used_items; ++i) {
    int s = -1, lit = 0;
    //
    if (it[i].t && it[i].s >= 0) {
      switch (it[i].t&0x7FFF) {
        case I_ARM1: s = it[i].s/9+18; break;
        case I_ARM2: s = it[i].s/9+20; break;
        case I_MEGA: s = it[i].s/2+22; break;
        case I_INVL: s = it[i].s/2+26; break;
        case I_SUPER:
        case I_RTORCH:
        case I_GTORCH:
        case I_BTORCH:
          s = it[i].s/2+((it[i].t&0x7fff)-I_SUPER)*4+35;
          lit = 1;
          break;
        case I_GOR1:
        case I_FCAN:
          s = it[i].s/2+((it[i].t&0x7fff)-I_GOR1)*3+51;
          break;
        case I_AQUA: s = 30; break;
        case I_SUIT: s = 34; break;
        case I_KEYR:
        case I_KEYG:
        case I_KEYB:
          s = (it[i].t&0x7FFF)-I_KEYR+31;
          lit = 1;
          break;
        case I_GUN2: s = 57; break;
        default: s = (it[i].t&0x7FFF)-1; break;
      }
    }
    //
    if (s >= 0) {
      IT_lighting(i);
      (lit ? Z_drawspr_lit : Z_drawspr)(it[i].o.x, it[i].o.y, &spr[s], sprd[s]);
    }
  }
}


int IT_spawn (int x, int y, int t) {
  for (int i = 0; i < MAX_ITEMS; ++i) {
    if (!it[i].t) {
      it[i].t = t|0x8000;
      it[i].s = 0;
      it[i].o.type = OBJT_ITEM;
      it[i].o.x = x;
      it[i].o.y = y;
      it[i].o.xv = it[i].o.yv = it[i].o.vx = it[i].o.vy = 0;
      it[i].o.r = 10;
      it[i].o.h = 8;
      if (i+1 > max_used_items) max_used_items = i+1;
      return i;
    }
  }
  return -1;
}


static const char *item_names[] = {
  NULL,
  "CLIP",
  "SHEL",
  "ROCKET",
  "CELL",
  "AMMO",
  "SBOX",
  "RBOX",
  "CELP",
  "STIM",
  "MEDI",
  "BPACK",
  "CSAW",
  "SGUN",
  "SGUN2",
  "MGUN",
  "LAUN",
  "PLAS",
  "BFG",
  "ARM1",
  "ARM2",
  "MEGA",
  "INVL",
  "AQUA",
  "KEYR",
  "KEYG",
  "KEYB",
  "SUIT",
  "SUPER",
  "RTORCH",
  "GTORCH",
  "BTORCH",
  "GOR1",
  "FCAN",
  "GUN2",
};


CONCMD(spawn_item, "spawn new item") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc < 2) {
    conlogf("spawn what item?\n");
    return;
  }
  //
  if (!r_mouse_cursor) {
    conlogf("not in edit mode!\n");
    return;
  }
  //
  for (int f = 1; f < argc; ++f) {
    const char *s = argv[f];
    //
    for (size_t i = 0; i < sizeof(item_names)/sizeof(item_names[0]); ++i) {
      if (item_names[i] == NULL) continue;
      if (strcasecmp(item_names[i], s) == 0 ||
          (s[0] == 'h' && s[1] == '_' && strcasecmp(item_names[i], s+2) == 0)) {
        int id = IT_spawn(Z_SCR2MAP_X(msLastX), Z_SCR2MAP_Y(msLastY), i);
        //
        if (id >= 0 && s[0] == 'h' && s[1] == '_') it[id].t &= ~0x8000;
        break;
      }
    }
  }
}


CONCMD(spawn_item_at, "spawn new item at coords") {
  int error, x, y;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc != 3) {
    conlogf("spawn what item?\n");
    return;
  }
  //
  if ((x = cmdcon_parseint(argv[1], -1, 0, FLDW*8, &error)) < 0 || error) {
    conlogf("%s: invalid x: '%s'!\n", argv[0], argv[1]);
    return;
  }
  if ((y = cmdcon_parseint(argv[2], -1, 0, FLDH*8, &error)) < 0 || error) {
    conlogf("%s: invalid y: '%s'!\n", argv[0], argv[2]);
    return;
  }
  //
  const char *s = argv[3];
  //
  for (size_t i = 0; i < sizeof(item_names)/sizeof(item_names[0]); ++i) {
    if (item_names[i] == NULL) continue;
    if (strcasecmp(item_names[i], s) == 0 ||
        (s[0] == 'h' && s[1] == '_' && strcasecmp(item_names[i], s+2) == 0)) {
      int id = IT_spawn(x, y, i);
      //
      if (id >= 0 && s[0] == 'h' && s[1] == '_') it[id].t &= ~0x8000;
      break;
    }
  }
}


static inline int is_in_circle (int x, int y, int cx, int cy, int rad) {
  if (rad > 0) return ((cx-x)*(cx-x)+(cy-y)*(cy-y) <= rad*rad);
  return 0;
}


int IT_edit_act (void) {
  int x = Z_SCR2MAP_X(msLastX);
  int y = Z_SCR2MAP_Y(msLastY);
  //
  if (keys[SDLK_i]) {
    // find item under cursor
    for (int i = 0; i < max_used_items; ++i) {
      if (it[i].t&0x7fff) {
        if (is_in_circle(x, y, it[i].o.x, it[i].o.y, it[i].o.r)) {
          conlogf("id=%d; type=%d\n", i, it[i].t&0x7fff);
        }
      }
    }
    return 1;
  }
  //
  return 0;
}


void IT_drop_ammo (int t, int n, int x, int y) {
  static const int an[8] = {10, 4, 1, 40, 50, 25, 5, 100};
  //
  for (;;) {
    for (int a = an[t-I_CLIP]; n >= a; n -= a) IT_spawn(x+myrand(3*2+1)-3, y-myrand(7), t);
    if (t >= I_AMMO) t -= 4; else break;
  }
}


static int IT_mapload (MemBuffer *mbuf, const map_block_t *blk) {
  int i;
  //
  if (mbuf == NULL) {
    // no processing
    for (i = MAX_ITEMS; i > 0; --i) if (it[i].t) break;
    max_used_items = i;
    return 0;
  }
  //
  int m, j;
  map_thing_t t;
  int sz = blk->sz;
  //
  IT_init();
  MN_init();
  for (i = 0; sz > 0; ++i, sz -= 8) {
    if (membuf_read_full(mbuf, &t, sizeof(t)) < 0) return -1;
    it[i].o.type = OBJT_ITEM;
    it[i].o.x = t.x;
    it[i].o.y = t.y;
    it[i].t = t.t;
    it[i].s = t.f;
    if (!it[i].t) break;
    if ((it[i].s&THF_DM) && !g_dm) it[i].t = 0;
  }
  m = i;
  for (i = 0, j = -1; i < m; ++i) {
    if (it[i].t == TH_PLR1) { j = i; it[i].t = 0; }
  }
  if (!g_dm) {
    if (j == -1) {
      conlogf("FATAL: item 'player_1' is not found");
      return -1;
    }
    dm_pos[0].x = it[j].o.x;
    dm_pos[0].y = it[j].o.y;
    dm_pos[0].d = it[j].s&THF_DIR;
  }
  for (i = 0, j = -1; i < m; ++i) {
    if (it[i].t == TH_PLR2) { j = i; it[i].t = 0; }
  }
  if (!g_dm && g_plrcount == 2) {
    if (j == -1) {
      conlogf("FATAL: item 'player_2' is not found");
      return -1;
    }
    dm_pos[1].x = it[j].o.x;
    dm_pos[1].y = it[j].o.y;
    dm_pos[1].d = it[j].s&THF_DIR;
  }
  for (i = 0, j = 0; i < m; ++i) {
    if (it[i].t == TH_DMSTART) {
      if (g_dm) {
        dm_pos[j].x = it[i].o.x;
        dm_pos[j].y = it[i].o.y;
        dm_pos[j].d = it[i].s&THF_DIR;
      }
      it[i].t = 0;
      ++j;
    }
  }
  if (g_dm && j < 2) {
    conlogf("FATAL: less than two DM spawn points");
    return -1;
  }
  if (g_dm) {
    dm_pnum = j;
    dm_pl1p = myrand(dm_pnum);
    do {
      dm_pl2p = myrand(dm_pnum);
    } while (dm_pl2p == dm_pl1p);
  } else {
    dm_pl1p = 0;
    dm_pl2p = 1;
    dm_pnum = 2;
  }
  PL_spawn(&pl[0], dm_pos[dm_pl1p].x, dm_pos[dm_pl1p].y, dm_pos[dm_pl1p].d);
  if (g_plrcount == 2) PL_spawn(&pl[1], dm_pos[dm_pl2p].x, dm_pos[dm_pl2p].y, dm_pos[dm_pl2p].d);
  for (i = 0; i < m; ++i) {
    if (it[i].t >= TH_CLIP && it[i].t < TH_DEMON) {
      it[i].s = 0;
      it[i].t = it[i].t-TH_CLIP+I_CLIP;
      if (it[i].t >= I_KEYR && it[i].t <= I_KEYB) it[i].t |= 0x8000;
    } else if (it[i].t >= TH_DEMON) {
      MN_spawn(it[i].o.x, it[i].o.y, it[i].s&THF_DIR, it[i].t-TH_DEMON+MN_DEMON);
      it[i].t = 0;
    }
  }
  //
  return 0;
}


static int IT_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    for (cnt = max_used_items; cnt > 0; --cnt) if (it[cnt-1].t) break;
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(it[0])); // structure size
    membuf_write_ui32(mbuf, itm_rtime);
    membuf_write_ui16(mbuf, cnt);
    membuf_write(mbuf, it, cnt*sizeof(it[0]));
  }
  //
  return 0;
}


static int IT_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    IT_init();
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'items' version!\n");
      return -1;
    }
    if (membuf_read_ui16(mbuf) != sizeof(it[0])) {
      conlogf("LOADGAME ERROR: invalid 'items' size!\n");
      return -1;
    }
    itm_rtime = membuf_read_ui32(mbuf);
    cnt = membuf_read_ui16(mbuf);
    if (cnt > MAX_ITEMS) {
      conlogf("LOADGAME ERROR: too many items (%u)!\n", cnt);
      return -1;
    }
    if (membuf_read_full(mbuf, it, cnt*sizeof(it[0])) < 0) {
      conlogf("LOADGAME ERROR: can't read items!\n");
      return -1;
    }
    max_used_items = cnt;
  }
  //
  if (waserror) IT_init();
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_initvga(&spr[f]);
  //
  F_registerMapLoad(MB_THING, IT_mapload, NULL);
  F_registerSaveLoad("THINGS", IT_savegame, IT_loadgame, SAV_NORMAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_freevga(&spr[f]);
}
