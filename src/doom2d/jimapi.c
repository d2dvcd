/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include "jimapi.h"

#include <string.h>
#include <malloc.h>

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "items.h"
#include "map.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "sdldrv.h"
#include "smoke.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"
#include "widgets.h"


////////////////////////////////////////////////////////////////////////////////
JIMAPI_Command *jimapi_command_list = NULL;


////////////////////////////////////////////////////////////////////////////////
__attribute((format(printf,2,3))) void jim_SetResStrf (Jim_Interp *interp, const char *fmt, ...) {
  va_list va;
  char *str;
  //
  va_start(va, fmt);
  str = Z_vasprintf(fmt, va);
  va_end(va);
  //
  Jim_SetResultString(interp, str, -1);
  free(str);
}


////////////////////////////////////////////////////////////////////////////////
void jimInit (void) {
  if ((jim = Jim_CreateInterp()) == NULL) {
    fprintf(stderr, "FATAL: can't init jim-tcl!\n");
    exit(1);
  }
  Jim_RegisterCoreCommands(jim);
  Jim_InitStaticExtensions(jim);
  //
  Jim_SetGlobalVariableStr(jim, "bindir", Jim_NewStringObj(jim, get_bin_dir(), -1));
  Jim_SetGlobalVariableStr(jim, "homedir", Jim_NewStringObj(jim, get_home_dir(), -1));
  //
#ifdef JIM_INTERNAL
  Jim_SetGlobalVariableStr(jim, "jim_internal", Jim_NewIntObj(jim, 1));
#else
  Jim_SetGlobalVariableStr(jim, "jim_internal", Jim_NewIntObj(jim, 0));
#endif
  //
  for (const JIMAPI_Command *cmd = jimapi_command_list; cmd != NULL; cmd = cmd->next) {
    //fprintf(stderr, "JIM: registering '%s'\n", cmd->name);
    Jim_CreateCommand(jim, cmd->name, cmd->func, NULL, NULL);
  }
  //
#ifdef JIM_INTERNAL
  jimEvalFile("jimstd/0init.tcl", 0);
#endif
}


void jimDeinit (void) {
  Jim_FreeInterp(jim);
}


////////////////////////////////////////////////////////////////////////////////
//FIXME: buffer overflow
//FIXME: '../../../', etc
static char *findJim (const char *fname) {
  static const char *pathlist[] = {
    "/usr/share/doom2d-vcd"
    "/usr/local/share/doom2d-vcd"
  };
  char *res = NULL;
  //
  char *tryfile (const char *path, const char *path1) {
    char *fn;
    //
    if (path1 == NULL) path1 = "";
    fn = Z_sprintf("%s%s/%s", path, path1, fname);
    //
    if (fexists(fn)) return fn;
    for (char *p = fn+strlen(path)+strlen(path1); *p; ++p) *p = toupper(*p);
    if (fexists(fn)) return fn;
    free(fn);
    return NULL;
  }
  //
  if (fname == NULL || !fname[0]) return NULL;
  if (fname[0] == '/' && strrchr(fname, '.') != NULL && fexists(fname)) {
    return strdup(fname);
  }
  // try '$BINDIR/<fname>'
  if ((res = tryfile(get_bin_dir(), "")) != NULL) return res;
  // try '$BINDIR/data/jim/<fname>'
  if ((res = tryfile(get_bin_dir(), "/data/jim")) != NULL) return res;
  // try '$BINDIR/jim/<fname>'
  if ((res = tryfile(get_bin_dir(), "/jim")) != NULL) return res;
  //
  for (size_t f = 0; f < sizeof(pathlist)/sizeof(pathlist[0]); ++f) {
    if ((res = tryfile(pathlist[f], NULL)) != NULL) return res;
  }
  //
#ifndef _WIN32
  return tryfile(getenv("HOME"), "/.doom2d-vcd/jim");
#else
  return NULL;
#endif
}


int jimEvalFile (const char *fname, int okifabsent) {
  char *fn = findJim(fname);
  int res;
  //
  if (fn == NULL) {
    if (!okifabsent) conlogf("JIM ERROR: can't find file: '%s'!\n", fname);
    return -1;
  }
  //
  conlogf("JIM: loading: '%s'\n", fn);
  if (Jim_EvalFile(jim, fn) == JIM_OK) {
    res = 0;
  } else {
    res = -1;
    Jim_MakeErrorMessage(jim);
    conlogf("JIM ERROR: %s\n", Jim_GetString(Jim_GetResult(jim), NULL));
  }
  //
  free(fn);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
// cputs
JIMAPI_FN(cputs) {
  if (argc != 2 && argc != 3) {
    Jim_WrongNumArgs(interp, 1, argv, "?-nonewline? string");
    return JIM_ERR;
  }
  //
  if (argc == 3) {
    if (!Jim_CompareStringImmediate(interp, argv[1], "-nonewline")) {
      jim_SetResStrf(interp, "%s: the second argument must be '-nonewline'", Jim_String(argv[0]));
      return JIM_ERR;
    }
    conlogf("%s", Jim_String(argv[2]));
  } else {
    conlogf("%s\n", Jim_String(argv[1]));
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// errputs
JIMAPI_FN(errputs) {
  if (argc != 2 && argc != 3) {
    Jim_WrongNumArgs(interp, 1, argv, "?-nonewline? string");
    return JIM_ERR;
  }
  //
  if (argc == 3) {
    if (!Jim_CompareStringImmediate(interp, argv[1], "-nonewline")) {
      jim_SetResStrf(interp, "%s: the second argument must be '-nonewline'", Jim_String(argv[0]));
      return JIM_ERR;
    }
    conlogf("%s", Jim_String(argv[2]));
    //fprintf(stderr, "%s", Jim_String(argv[2]));
  } else {
    conlogf("%s\n", Jim_String(argv[1]));
    //fprintf(stderr, "%s\n", Jim_String(argv[1]));
  }
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// load name
JIMAPI_FN(load) {
  if (argc != 2) { Jim_WrongNumArgs(interp, 1, argv, "name"); return JIM_ERR; }
  //
  jimEvalFile(Jim_String(argv[1]), 0);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// quit
JIMAPI_FN(quit) {
  if (argc != 1) { Jim_WrongNumArgs(interp, 1, argv, "no args expected"); return JIM_ERR; }
  postQuitMessage();
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


CONCMD(jim_exec, "execute Jim script from file") {
  CMDCON_HELP();
  //
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  jimEvalFile(argv[1], 0);
}


CONCMD(jim_eval, "execute Jim code directly") {
  CMDCON_HELP();
  //
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if (Jim_Eval(jim, argv[1]) == JIM_OK) {
    conlogf("%s\n", Jim_GetString(Jim_GetResult(jim), NULL));
  } else {
    Jim_MakeErrorMessage(jim);
    conlogf("JIM ERROR: %s\n", Jim_GetString(Jim_GetResult(jim), NULL));
  }
}
