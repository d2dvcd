/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include <string.h>
#include <malloc.h>

#include "widgets_jimapi.h"

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "items.h"
#include "jimapi.h"
#include "map.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "sdldrv.h"
#include "smoke.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"
#include "widgets.h"


////////////////////////////////////////////////////////////////////////////////
// setschemecolor path clrnum
JIMAPI_FN(setschemecolor) {
  const char *path;
  char newpath[1028], *npp = newpath, *clrname = NULL;
  long val;
  //
  if (argc != 3) {
    Jim_WrongNumArgs(interp, argc, argv, "setschemecolor path clr");
    return JIM_ERR;
  }
  //
  path = Jim_String(argv[1]);
  if (strlen(path) > 1024) {
    jim_SetResStrf(interp, "setschemecolor: path too long");
    return JIM_ERR;
  }
  //
  if (Jim_GetLong(interp, argv[2], &val) != JIM_OK) {
    jim_SetResStrf(interp, "%s: color index expected", Jim_String(argv[0]));
    return JIM_ERR;
  }
  if (val < 0 || val > 255) {
    jim_SetResStrf(interp, "%s: invalid color index: %d", Jim_String(argv[0]), (int)val);
    return JIM_ERR;
  }
  // norm path
  // scheme name
  while (*path == '/') ++path;
  while (*path && *path != '/') *npp++ = *path++;
  while (*path == '/') ++path;
  if (!path[0]) {
    jim_SetResStrf(interp, "%s: invalid path: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }
  //
  *npp++ = 0;
  clrname = npp;
  *npp++ = '/';
  while (*path) {
    if (*path == '/') {
      while (*path == '/') ++path;
      *npp++ = '/';
    } else {
      *npp++ = *path++;
    }
  }
  while (npp > clrname && npp[-1] == '/') --npp;
  if (npp == clrname) {
    jim_SetResStrf(interp, "%s: invalid path: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }
  *npp = 0;
  //
  if (zxcFindColorScheme(newpath) == NULL) zxcNewColorScheme(newpath);
  zxcSetColor(newpath, clrname, val);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
static void *processWinsysCommands (void *ud, const char *pfx, Jim_DelCmdProc dp, void *(*fn) (const char *name, Jim_Cmd *cmd, void *ud)) {
  Jim_HashTableIterator *it;
  Jim_HashEntry *he;
  int len = strlen(pfx);
  //
  it = Jim_GetHashTableIterator(&jim->commands);
  while ((he = Jim_NextHashEntry(it)) != NULL) {
    if (strncmp((const char *)he->key, pfx, len) == 0) {
      // it's our command?
      Jim_Cmd *cmd = (Jim_Cmd *)he->u.val;
      //
      if (!cmd->isproc && cmd->u.native.delProc == dp) {
        // this SHOULD be our command
        void *res = fn((const char *)he->key, cmd, ud);
        //
        if (res != NULL) {
          Jim_FreeHashTableIterator(it);
          return res;
        }
      }
    }
  }
  Jim_FreeHashTableIterator(it);
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
static void cleanupWindowCommands (ZXWindow *w);
static void cleanupWidgetCommands (ZXWidget *g);


////////////////////////////////////////////////////////////////////////////////
// 'window' object
//
typedef struct {
  ZXWinPaintCB opaintCB;
  ZXWinKeyCB okeyCB;
  ZXWinMouseCB omouseCB;
  ZXWinActivationCB oactivationCB;
  ZXWinDestroyCB odestroyCB;
} JimWinData;


////////////////////////////////////////////////////////////////////////////////
static void jwinPaint (ZXWindow *w, VExSurface *sfc) {
  ((JimWinData *)w->udata)->opaintCB(w, sfc);
}


static int jwinKey (ZXWindow *w, SDL_KeyboardEvent *key) {
  return ((JimWinData *)w->udata)->okeyCB(w, key);
}


static int jwinMouse (ZXWindow *w, int x, int y, int btn, int buttons) {
  return ((JimWinData *)w->udata)->omouseCB(w, x, y, btn, buttons);
}


static void jwinActivation (ZXWindow *w, int activated) {
  ((JimWinData *)w->udata)->oactivationCB(w, activated);
}


static void jwinDestroy (ZXWindow *w) {
  JimWinData *wd = (JimWinData *)w->udata;
  //
  //fprintf(stderr, "jwinDestroy\n");
  cleanupWindowCommands(w);
  wd->odestroyCB(w);
  free(wd);
}


////////////////////////////////////////////////////////////////////////////////
static void jimWinDelProc (Jim_Interp *interp, void *udata) {
  //if (udata != NULL) free(udata);
  //fprintf(stderr, "windelproc\n");
}


////////////////////////////////////////////////////////////////////////////////
// 'widget' object
//
typedef struct {
  Jim_Obj *action;
  ZXWPaintCB opaintCB;
  ZXWKeyCB okeyCB;
  ZXWMouseCB omouseCB;
  ZXWActivationCB oactivationCB;
  ZXWDestroyCB odestroyCB;
  //
  zxwClickCB oclickCB;
} JimWidgetData;


////////////////////////////////////////////////////////////////////////////////
static void jwPaint (ZXWidget *g, VExSurface *sfc) {
  ((JimWidgetData *)(zxwUData(g)))->opaintCB(g, sfc);
}


static int jwKey (ZXWidget *g, SDL_KeyboardEvent *key) {
  return ((JimWidgetData *)(zxwUData(g)))->okeyCB(g, key);
}


static int jwMouse (ZXWidget *g, int x, int y, int btn, int buttons) {
  return ((JimWidgetData *)(zxwUData(g)))->omouseCB(g, x, y, btn, buttons);
}


static void jwActivation (ZXWidget *g, int activated) {
  ((JimWidgetData *)(zxwUData(g)))->oactivationCB(g, activated);
}


static void jwDestroy (ZXWidget *g) {
  JimWidgetData *wd = (JimWidgetData *)(zxwUData(g));
  //
  cleanupWidgetCommands(g);
  if (wd->action != NULL) Jim_DecrRefCount(jim, wd->action);
  wd->action = NULL;
  wd->odestroyCB(g);
  free(wd);
}


////////////////////////////////////////////////////////////////////////////////
static void jimWidgetDelProc (Jim_Interp *interp, void *udata) {
  //if (udata != NULL) free(udata);
}


////////////////////////////////////////////////////////////////////////////////
static void cleanupWindowCommands (ZXWindow *w) {
  char buf[128];
  //
  processWinsysCommands(w, "<zxwindow:", jimWinDelProc, cmdcon_lambda(void *, (const char *name, Jim_Cmd *cmd, void *ud) {
    if (cmd->u.native.privData == ud) {
      //fprintf(stderr, "cleanup: [%s]\n", name);
      cmd->u.native.privData = NULL;
    }
    return NULL;
  }));
  //
  for (;;) {
    int done = 1;
    //
    processWinsysCommands(NULL, "<zxwindow:", jimWinDelProc, cmdcon_lambda(void *, (const char *name, Jim_Cmd *cmd, void *ud) {
      if (cmd->u.native.privData == NULL && strlen(name) < 128) {
        strcpy(buf, name);
        done = 0;
        return cmd;
      }
      return NULL;
    }));
    if (done) break;
    //fprintf(stderr, "empty command: [%s]\n", buf);
    Jim_DeleteCommand(jim, buf);
  }
  //
  for (;;) {
    int done = 1;
    //
    processWinsysCommands(NULL, "<zxwidget:", jimWidgetDelProc, cmdcon_lambda(void *, (const char *name, Jim_Cmd *cmd, void *ud) {
      if (cmd->u.native.privData == NULL && strlen(name) < 128) {
        strcpy(buf, name);
        done = 0;
        return cmd;
      }
      return NULL;
    }));
    if (done) break;
    //fprintf(stderr, "empty command: [%s]\n", buf);
    Jim_DeleteCommand(jim, buf);
  }
}


static void cleanupWidgetCommands (ZXWidget *g) {
  processWinsysCommands(g, "<zxwidget:", jimWidgetDelProc, cmdcon_lambda(void *, (const char *name, Jim_Cmd *cmd, void *ud) {
    if (cmd->u.native.privData == ud) {
      //fprintf(stderr, "cleanup: [%s]\n", name);
      cmd->u.native.privData = NULL;
    }
    return NULL;
  }));
}


////////////////////////////////////////////////////////////////////////////////
static ZXWidget *commandWidgetFind (const char *cmdname) {
  return processWinsysCommands(NULL, "<zxwidget:", jimWidgetDelProc, cmdcon_lambda(void *, (const char *name, Jim_Cmd *cmd, void *ud) {
    if (strcmp(name, cmdname) == 0) return cmd->u.native.privData;
    return NULL;
  }));
}


static const char *createWinCmd (ZXWindow *w, Jim_CmdProc wc) {
  static char namebuf[128];
  const char *nm;
  //
  nm = processWinsysCommands(w, "<zxwindow:", jimWinDelProc, cmdcon_lambda(void *, (const char *name, Jim_Cmd *cmd, void *ud) {
    if (cmd->u.native.privData == ud) return (void *)name;
    return NULL;
  }));
  //
  if (nm != NULL) {
    strcpy(namebuf, nm);
  } else {
    sprintf(namebuf, "<zxwindow:%ld>", Jim_GetId(jim));
    Jim_CreateCommand(jim, namebuf, wc, w, jimWinDelProc);
  }
  //
  return namebuf;
}


static const char *createWidgetCmd (ZXWidget *g, Jim_CmdProc wc) {
  static char namebuf[128];
  const char *nm;
  //
  nm = processWinsysCommands(g, "<zxwidget:", jimWidgetDelProc, cmdcon_lambda(void *, (const char *name, Jim_Cmd *cmd, void *ud) {
    if (cmd->u.native.privData == ud) return (void *)name;
    return NULL;
  }));
  //
  if (nm != NULL) {
    strcpy(namebuf, nm);
  } else {
    sprintf(namebuf, "<zxwidget:%ld>", Jim_GetId(jim));
    Jim_CreateCommand(jim, namebuf, wc, g, jimWidgetDelProc);
  }
  //
  return namebuf;
}


////////////////////////////////////////////////////////////////////////////////
static int jim_winsys_window_dispatcher (Jim_Interp *interp, int argc, Jim_Obj *const *argv);
static int jim_winsys_widget_dispatcher (Jim_Interp *interp, int argc, Jim_Obj *const *argv);


// winsys window scmname id x y w h [title]
JIMAPI_FN_NOREG(winsys_window_new) {
  const char *scm, *id, *title;
  int nums[4];
  ZXWindow *w;
  JimWinData *wd;
  //
  if (argc < 8 || argc > 9) {
    jim_SetResStrf(interp, "winsys window: invalid number of args");
    return JIM_ERR;
  }
  scm = Jim_String(argv[2]);
  if (!scm[0]) {
    scm = NULL;
  } else {
    if (zxcFindColorScheme(scm) == NULL) {
      jim_SetResStrf(interp, "winsys window: invalid color scheme: '%s'", scm);
      return JIM_ERR;
    }
  }
  //
  id = Jim_String(argv[3]);
  if (!id[0]) id = NULL;
  //
  title = (argc == 9 ? Jim_String(argv[8]) : NULL);
  //
  for (int f = 0; f < 4; ++f) {
    long v;
    //
    if (Jim_GetLong(interp, argv[f+4], &v) != JIM_OK || v < -65535 || v > 65535) {
      jim_SetResStrf(interp, "winsys window: invalid numeric arg: '%s'", Jim_String(argv[f+4]));
      return JIM_ERR;
    }
    nums[f] = v;
  }
  //
  if ((wd = calloc(1, sizeof(JimWinData))) == NULL) {
    jim_SetResStrf(interp, "winsys window: can't create window");
    return JIM_ERR;
  }
  //
  if ((w = zxwinNew(scm, id, nums[0], nums[1], nums[2], nums[3], title)) == NULL) {
    free(wd);
    jim_SetResStrf(interp, "winsys window: can't create window");
    return JIM_ERR;
  }
  //
  w->udata = wd;
  //
  wd->opaintCB = w->paintCB;
  w->paintCB = jwinPaint;
  wd->okeyCB = w->keyCB;
  w->keyCB = jwinKey;
  wd->omouseCB = w->mouseCB;
  w->mouseCB = jwinMouse;
  wd->oactivationCB = w->activationCB;
  w->activationCB = jwinActivation;
  wd->odestroyCB = w->destroyCB;
  w->destroyCB = jwinDestroy;
  //
  Jim_SetResultString(interp, createWinCmd(w, jim_winsys_window_dispatcher), -1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// winsys findbyid id
JIMAPI_FN_NOREG(winsys_window_findbyid) {
  ZXWindow *w;
  //
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  //
  if ((w = zxwinFindById(Jim_String(argv[2]))) != NULL) {
    Jim_SetResultString(interp, createWinCmd(w, jim_winsys_window_dispatcher), -1);
    return JIM_OK;
  }
  //
  Jim_SetResultString(interp, "", -1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
typedef int (*JimWinCmdEx) (ZXWindow *w, Jim_Interp *interp, int argc, Jim_Obj *const *argv);

// winobj xxx
#define JIMAPI_WIN_FN(_pname) \
static int jim_winsys_window_##_pname (ZXWindow *w, Jim_Interp *interp, int argc, Jim_Obj *const *argv)


// winobj close
JIMAPI_WIN_FN(close) {
  zxwinClose(w);
  //
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// winobj getid
JIMAPI_WIN_FN(getid) {
  Jim_SetResultString(interp, (zxwinIsAlive(w) && w->id != NULL ? w->id : ""), -1);
  return JIM_OK;
}


#define JIMWIN_GETINT(_pname,_fnname)  \
static int jim_winsys_window_##_pname (ZXWindow *w, Jim_Interp *interp, int argc, Jim_Obj *const *argv) { \
  Jim_SetResultInt(interp, _fnname(w)); \
  return JIM_OK; \
}

#define JIMWIN_SETINT(_pname,_func) \
static int jim_winsys_window_##_pname (ZXWindow *w, Jim_Interp *interp, int argc, Jim_Obj *const *argv) { \
  long val; \
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; } \
  if (Jim_GetLong(interp, argv[2], &val) != JIM_OK) { \
    jim_SetResStrf(interp, "winsys window: integer expected"); \
    return JIM_ERR; \
  } \
  if (zxwinIsAlive(w) && _func(w, val) == 0) { \
    Jim_SetResultBool(interp, 1); \
    return JIM_OK; \
  } \
  Jim_SetResultBool(interp, 0); \
  return JIM_ERR; \
}


// winobj isalive
// winobj isvisible
// winobj isactive
// winobj isenabled
// winobj canbeactivated
JIMWIN_GETINT(isalive, zxwinIsAlive)
JIMWIN_GETINT(isvisible, zxwinIsVisible)
JIMWIN_GETINT(isactive, zxwinIsActive)
JIMWIN_GETINT(isenabled, zxwinIsEnabled)
JIMWIN_GETINT(canbeactivated, zxwinCanBeActivated)

// winobj getx
// winobj gety
// winobj getw
// winobj geth
JIMWIN_GETINT(getx, zxwinX)
JIMWIN_GETINT(gety, zxwinY)
JIMWIN_GETINT(getw, zxwinW)
JIMWIN_GETINT(geth, zxwinH)

// winobj setx val
// winobj sety val
// winobj setw val
// winobj seth val
JIMWIN_SETINT(setx, zxwinSetX)
JIMWIN_SETINT(sety, zxwinSetY)
JIMWIN_SETINT(setw, zxwinSetW)
JIMWIN_SETINT(seth, zxwinSetH)


// winobj gettitle
JIMAPI_WIN_FN(gettitle) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinIsAlive(w)) {
    Jim_SetResultString(interp, zxwinCaption(w), -1);
    return JIM_OK;
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// winobj settitle title
JIMAPI_WIN_FN(settitle) {
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinIsAlive(w)) {
    if (zxwinSetCaption(w, Jim_String(argv[2])) == 0) {
      Jim_SetResultBool(interp, 0);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// winobj activate
JIMAPI_WIN_FN(activate) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinCanBeActivated(w)) {
    zxwinActivate(w);
    zxwinBringToTop(w);
    Jim_SetResultBool(interp, 1);
  } else {
    Jim_SetResultBool(interp, 0);
  }
  return JIM_OK;
}


// winobj hide
JIMAPI_WIN_FN(hide) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinIsAlive(w)) {
    if (zxwinHide(w)) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// winobj show
JIMAPI_WIN_FN(show) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinIsAlive(w)) {
    if (zxwinShow(w)) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// winobj getradioactive groupname
JIMAPI_WIN_FN(getradioactive) {
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinIsAlive(w)) {
    const char *r = zxwinRadioGet(w, Jim_String(argv[2]));
    //
    Jim_SetResultString(interp, (r == NULL ? "" : r), -1);
    return JIM_OK;
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// winobj setradioactive groupname widgetid
JIMAPI_WIN_FN(setradioactive) {
  if (argc != 4) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinIsAlive(w)) {
    if (zxwinRadioSet(w, Jim_String(argv[2]), Jim_String(argv[3])) >= 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
    Jim_SetResultBool(interp, 0);
    return JIM_OK;
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// winobj findbyid id
static int jim_winsys_widget_findbyid (ZXWindow *w, Jim_Interp *interp, int argc, Jim_Obj *const *argv) {
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwinIsAlive(w)) {
    ZXWidget *g = zxwFindById(w, Jim_String(argv[2]));
    //
    if (g != NULL) {
      Jim_SetResultString(interp, createWidgetCmd(g, jim_winsys_widget_dispatcher), -1);
      return JIM_OK;
    }
  }
  //
  Jim_SetResultString(interp, "", -1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
static void jimwidgetClickCB (ZXWidget *g, void *udata) {
  if (zxwIsAlive(g)) {
    JimWidgetData *wd = (JimWidgetData *)zxwUData(g);
    //
    if (wd->action != NULL) {
      Jim_Obj *lstb[2], *lst;
      //
      lstb[0] = wd->action;
      lstb[1] = Jim_NewStringObj(jim, createWidgetCmd(g, jim_winsys_widget_dispatcher), -1);
      lst = Jim_NewListObj(jim, lstb, 2);
      Jim_IncrRefCount(lst);
      if (Jim_EvalObj(jim, lst) != JIM_OK) {
        Jim_MakeErrorMessage(jim);
        conlogf("=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
      }
      Jim_DecrRefCount(jim, lst);
/*
      Jim_Obj *lstb[2], *lst, *eres;
      //
      lstb[0] = wd->action;
      lstb[1] = Jim_NewStringObj(jim, createWidgetCmd(g, jim_winsys_widget_dispatcher), -1);
      lst = Jim_NewListObj(jim, lstb, 2);
      //
      Jim_IncrRefCount(lst);
      fprintf(stderr, ">[%s]\n", Jim_String(lst));
      if (Jim_SubstObj(jim, lst, &eres, 0) == JIM_OK) {
        Jim_IncrRefCount(eres);
        fprintf(stderr, ">>[%s]\n", Jim_String(eres));
        if (Jim_EvalObj(jim, eres) != JIM_OK) {
          Jim_MakeErrorMessage(jim);
          conlogf("=== JIM ERROR ===\n%s\n=================\n", Jim_GetString(Jim_GetResult(jim), NULL));
        }
        Jim_DecrRefCount(jim, eres);
      }
      Jim_DecrRefCount(jim, lst);
*/
    } else {
      if (wd->oclickCB != NULL) wd->oclickCB(g, udata);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
// winobj radio group id x y w h [title] [clickh]
// winobj scrollbar vert|horiz id x y size [clickh]
// winobj listbox id x y w h [clickh]
// winobj <type> id x y w h [title] [clickh]
static int jim_winsys_widget_new (ZXWindow *w, int wtp, Jim_Interp *interp, int argc, Jim_Obj *const *argv) {
  JimWidgetData *wd;
  const char *id, *title = NULL, *group = NULL;
  int idnum = 2, numcnt = 4, vert = 0;
  int nums[4];
  ZXWidget *g;
  Jim_Obj *clickh = NULL;
  //
  //fprintf(stderr, "[%s] (%d): %d\n", Jim_String(argv[1]), wtp, argc);
  if (argc < 7) { jim_SetResStrf(interp, "new widget: invalid number of args"); return JIM_ERR; }
  if (w == NULL || !zxwinIsAlive(w)) { jim_SetResStrf(interp, "new widget: dead window object"); return JIM_ERR; }
  //
  switch (wtp) {
    case ZXWT_RADIO:
      group = Jim_String(argv[2]);
      if (!group[0]) group = NULL;
      if (argc < 8 || argc > 10) { jim_SetResStrf(interp, "new widget: invalid number of args"); return JIM_ERR; }
      idnum = 3;
      title = (argc > 8 ? Jim_String(argv[8]) : NULL);
      clickh = (argc > 9 ? argv[9] : NULL);
      break;
    case ZXWT_SCROLLBAR:
      if (argc < 7 || argc > 8) { jim_SetResStrf(interp, "new widget: invalid number of args"); return JIM_ERR; }
      if (strcmp("vert", Jim_String(argv[2])) == 0) vert = 1;
      else if (strcmp("horiz", Jim_String(argv[2])) == 0) vert = 0;
      else { jim_SetResStrf(interp, "new widget: invalid scrollbar type"); return JIM_ERR; }
      idnum = 3;
      numcnt = 3;
      clickh = (argc > 7 ? argv[7] : NULL);
      break;
    case ZXWT_LISTBOX:
      clickh = (argc > 7 ? argv[7] : NULL);
    default:
      //idnum = 2;
      title = (argc > 7 ? Jim_String(argv[7]) : NULL);
      clickh = (argc > 8 ? argv[8] : NULL);
      break;
  }
  //
  id = Jim_String(argv[idnum]);
  if (!id[0]) id = NULL;
  //
  if (clickh != NULL) {
    if (Jim_String(clickh)[0]) {
      clickh = Jim_DuplicateObj(jim, clickh);
      Jim_IncrRefCount(clickh);
    } else {
      clickh = NULL;
    }
    //if (clickh != NULL) fprintf(stderr, "clickh: [%s]\n", Jim_String(clickh));
  }
  //
  for (int f = 0; f < numcnt; ++f) {
    long v;
    //
    if (Jim_GetLong(interp, argv[idnum+f+1], &v) != JIM_OK || v < -65535 || v > 65535) {
      jim_SetResStrf(interp, "winsys widget new: invalid numeric arg: '%s'", Jim_String(argv[idnum+f+1]));
      return JIM_ERR;
    }
    nums[f] = v;
  }
  //
  if ((wd = calloc(1, sizeof(JimWidgetData))) == NULL) {
    jim_SetResStrf(interp, "new widget: can't create window");
    return JIM_ERR;
  }
  //
  switch (wtp) {
    case ZXWT_FRAME: g = zxwNewFrame(w, id, nums[0], nums[1], nums[2], nums[3], title); break;
    case ZXWT_STATIC: g = zxwNewStatic(w, id, nums[0], nums[1], nums[2], nums[3], title); break;
    case ZXWT_BUTTON: g = zxwNewButton(w, id, nums[0], nums[1], nums[2], nums[3], title, NULL); break;
    case ZXWT_CHECKBOX: g = zxwNewCheckBox(w, id, nums[0], nums[1], nums[2], nums[3], title); break;
    case ZXWT_RADIO: g = zxwNewRadio(w, group, id, nums[0], nums[1], nums[2], nums[3], title); break;
    case ZXWT_LINEEDIT: g = zxwNewLineEdit(w, id, nums[0], nums[1], nums[2], nums[3], title); break;
    case ZXWT_SCROLLBAR: g = zxwNewScrollBar(w, id, nums[0], nums[1], nums[2], vert); break;
    case ZXWT_LISTBOX: g = zxwNewListBox(w, id, nums[0], nums[1], nums[2], nums[3]); break;
    default:
      free(wd);
      jim_SetResStrf(interp, "new widget: unsupported widget type");
      return JIM_ERR;
  }
  //
  if (g == NULL) {
    free(wd);
    jim_SetResStrf(interp, "new widget: can't create widget");
    return JIM_ERR;
  }
  //
  wd->oclickCB = zxwWidgetClickCB(g);
  zxwWidgetSetClickCB(g, jimwidgetClickCB);
  zxwWidgetSetUData(g, wd, 0);
  //
  wd->opaintCB = g->paintCB;
  g->paintCB = jwPaint;
  wd->okeyCB = g->keyCB;
  g->keyCB = jwKey;
  wd->omouseCB = g->mouseCB;
  g->mouseCB = jwMouse;
  wd->oactivationCB = g->activationCB;
  g->activationCB = jwActivation;
  wd->odestroyCB = g->destroyCB;
  g->destroyCB = jwDestroy;
  wd->action = clickh;
  //
  Jim_SetResultString(interp, createWidgetCmd(g, jim_winsys_widget_dispatcher), -1);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
typedef int (*JimWidgetCmdEx) (ZXWidget *g, Jim_Interp *interp, int argc, Jim_Obj *const *argv);

// winobj xxx
#define JIMAPI_WIDGET_FN(_pname) \
static int jim_winsys_widget_##_pname (ZXWidget *g, Jim_Interp *interp, int argc, Jim_Obj *const *argv)


// wgobj type
JIMAPI_WIDGET_FN(type) {
  const char *s;
  //
  switch (zxwWidgetType(g)) {
    case ZXWT_FRAME: s = "frame"; break;
    case ZXWT_STATIC: s = "static"; break;
    case ZXWT_BUTTON: s = "button"; break;
    case ZXWT_CHECKBOX: s = "checkbox"; break;
    case ZXWT_RADIO: s = "radio"; break;
    case ZXWT_LINEEDIT: s = "lineedit"; break;
    case ZXWT_SCROLLBAR: s = "scrollbar"; break;
    case ZXWT_LISTBOX: s = "listbox"; break;
    //case ZXWT_TEXTEDIT: s = "textedit"; break;
    default: s = "unknown"; break;
  }
  Jim_SetResultString(interp, s, -1);
  return JIM_OK;
}


// wgobj remove
JIMAPI_WIDGET_FN(remove) {
  zxwRemoveWidget(g);
  Jim_SetResultBool(interp, 1);
  return JIM_OK;
}


// wgobj getid
JIMAPI_WIDGET_FN(getid) {
  Jim_SetResultString(interp, (zxwIsAlive(g) && g->id != NULL ? g->id : ""), -1);
  return JIM_OK;
}


// wgobj window
JIMAPI_WIDGET_FN(window) {
  if (zxwIsAlive(g)) {
    Jim_SetResultString(interp, createWinCmd(g->window, jim_winsys_window_dispatcher), -1);
    return JIM_OK;
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


#define JIMWIDGET_GETINT(_pname,_fnname)  \
static int jim_winsys_widget_##_pname (ZXWidget *g, Jim_Interp *interp, int argc, Jim_Obj *const *argv) { \
  Jim_SetResultInt(interp, _fnname(g)); \
  return JIM_OK; \
}

#define JIMWIDGET_SETINT(_pname,_func) \
static int jim_winsys_widget_##_pname (ZXWidget *g, Jim_Interp *interp, int argc, Jim_Obj *const *argv) { \
  long val; \
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; } \
  if (Jim_GetLong(interp, argv[2], &val) != JIM_OK) { \
    jim_SetResStrf(interp, "winsys widget: integer expected"); \
    return JIM_ERR; \
  } \
  if (zxwIsAlive(g) && _func(g, val) == 0) { \
    Jim_SetResultBool(interp, 1); \
    return JIM_OK; \
  } \
  Jim_SetResultBool(interp, 0); \
  return JIM_ERR; \
}


// wgobj isalive
// wgobj isvisible
// wgobj isactive
// wgobj isenabled
// wgobj canbevisible
// wgobj canbeactivated
// wgobj canprocesskeys
JIMWIDGET_GETINT(isalive, zxwIsAlive)
JIMWIDGET_GETINT(isvisible, zxwIsVisible)
JIMWIDGET_GETINT(isactive, zxwIsActive)
JIMWIDGET_GETINT(isenabled, zxwIsEnabled)
JIMWIDGET_GETINT(canbevisible, zxwCanBeVisible)
JIMWIDGET_GETINT(canbeactivated, zxwCanBeActivated)
JIMWIDGET_GETINT(canprocesskeys, zxwCanProcessKeys)

// wgobj getx
// wgobj gety
// wgobj getw
// wgobj geth
JIMWIDGET_GETINT(getx, zxwX)
JIMWIDGET_GETINT(gety, zxwY)
JIMWIDGET_GETINT(getw, zxwW)
JIMWIDGET_GETINT(geth, zxwH)

// wgobj setx val
// wgobj sety val
// wgobj setw val
// wgobj seth val
JIMWIDGET_SETINT(setx, zxwSetX)
JIMWIDGET_SETINT(sety, zxwSetY)
JIMWIDGET_SETINT(setw, zxwSetW)
JIMWIDGET_SETINT(seth, zxwSetH)


#define WIDGET_FUNC(_pname,_func)  \
static int jim_winsys_widget_##_pname (ZXWidget *g, Jim_Interp *interp, int argc, Jim_Obj *const *argv) { \
  if (zxwIsAlive(g)) { \
    if (_func(g) == 0) { \
      Jim_SetResultBool(interp, 1); \
      return JIM_OK; \
    } \
  } \
  Jim_SetResultBool(interp, 0); \
  return JIM_ERR; \
}


// wgobj activate
// wgobj hide
// wgobj show
WIDGET_FUNC(activate, zxwActivate)
WIDGET_FUNC(hide, zxwHideWidget)
WIDGET_FUNC(show, zxwShowWidget)


////////////////////////////////////////////////////////////////////////////////
// winsys widget wgobj default
// winsys widget wgobj cancel
// winsys widget wgobj normal
WIDGET_FUNC(button_setdefault, zxwButtonSetDefault)
WIDGET_FUNC(button_setcancel, zxwButtonSetCancel)
WIDGET_FUNC(button_setnormal, zxwButtonSetNormal)


////////////////////////////////////////////////////////////////////////////////
// wgobj getlinked
JIMAPI_WIDGET_FN(static_getlinked) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    ZXWidget *l = zxwStaticLinked(g);
    //
    if (l != NULL) {
      Jim_SetResultString(interp, createWidgetCmd(l, jim_winsys_widget_dispatcher), -1);
      return JIM_OK;
    }
    Jim_SetResultBool(interp, 0);
    return JIM_OK;
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj setlinked [wdt]
JIMAPI_WIDGET_FN(static_setlinked) {
  ZXWidget *l = NULL;
  //
  if (argc < 2 || argc > 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (argc == 3) l = commandWidgetFind(Jim_String(argv[2]));
  if (zxwIsAlive(g)) {
    if (zxwStaticSetLinked(g, l) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// wgobj getstate
JIMAPI_WIDGET_FN(checkbox_getstate) {
  if (zxwIsAlive(g)) {
    int res = zxwCheckBoxState(g);
    //
    if (res >= 0) {
      Jim_SetResultInt(interp, res);
      return JIM_OK;
    }
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_OK;
}


// wgobj setstate 0|1|2
JIMAPI_WIDGET_FN(checkbox_setstate) {
  long v;
  //
  if (argc != 3) {
    Jim_WrongNumArgs(interp, argc, argv, "cmd ...?");
    return JIM_ERR;
  }
  //
  if (Jim_GetLong(interp, argv[2], &v) != JIM_OK || v < 0 || v > 2) {
    jim_SetResStrf(interp, "checkbox widget setstate: invalid numeric arg: '%s'", Jim_String(argv[2]));
    return JIM_ERR;
  }
  //
  if (zxwIsAlive(g)) {
    zxwCheckBoxSetState(g, v);
    Jim_SetResultBool(interp, 1);
    return JIM_OK;
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// wgobj getradioactive
JIMAPI_WIDGET_FN(radio_getradioactive) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    int r = zxwRadioGetActive(g);
    //
    if (r >= 0) {
      Jim_SetResultBool(interp, r);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj setradioactive
JIMAPI_WIDGET_FN(radio_setradioactive) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    const char *grp = zxwRadioGetGroupName(g);
    //
    if (grp != NULL) zxwinRadioSet(g->window, grp, NULL); // reset all
    zxwRadioSetActive(g, 1);
    Jim_SetResultBool(interp, 1);
    return JIM_OK;
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj getgroup
JIMAPI_WIDGET_FN(radio_getgroup) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    const char *grp = zxwRadioGetGroupName(g);
    //
    Jim_SetResultString(interp, (grp != NULL ? grp : ""), -1);
    return JIM_OK;
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// wgobj gettext
JIMAPI_WIDGET_FN(lineedit_gettext) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    const char *t = zxwLineEditText(g);
    //
    Jim_SetResultString(interp, (t != NULL ? t : ""), -1);
    return JIM_OK;
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj settext text
JIMAPI_WIDGET_FN(lineedit_settext) {
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    if (zxwLineEditSetText(g, Jim_String(argv[2])) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  //
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// wgobj getlinked
JIMAPI_WIDGET_FN(scrollbar_getlinked) {
  if (argc != 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    ZXWidget *l = zxwScrollBarLinked(g);
    //
    if (l != NULL) {
      Jim_SetResultString(interp, createWidgetCmd(l, jim_winsys_widget_dispatcher), -1);
      return JIM_OK;
    }
    Jim_SetResultBool(interp, 0);
    return JIM_OK;
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj setlinked [wdt]
JIMAPI_WIDGET_FN(scrollbar_setlinked) {
  ZXWidget *l = NULL;
  //
  if (argc < 2 || argc > 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (argc == 3) l = commandWidgetFind(Jim_String(argv[2]));
  if (zxwIsAlive(g)) {
    if (zxwScrollBarSetLinked(g, l) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj isvert
// wgobj ishoriz
// wgobj getpos
// wgobj getmin
// wgobj getmax
// wgobj getpagesize
// wgobj getknobsize
// wgobj getknobpos
JIMWIDGET_GETINT(scrollbar_isvert, zxwScrollBarIsVertical)
JIMWIDGET_GETINT(scrollbar_ishoriz, !zxwScrollBarIsVertical)
JIMWIDGET_GETINT(scrollbar_getpos, zxwScrollBarPos)
JIMWIDGET_GETINT(scrollbar_getmin, zxwScrollBarMin)
JIMWIDGET_GETINT(scrollbar_getmax, zxwScrollBarMax)
JIMWIDGET_GETINT(scrollbar_getpagesize, zxwScrollBarPageSize)
JIMWIDGET_GETINT(scrollbar_getknobsize, zxwScrollBarKnobSize)
JIMWIDGET_GETINT(scrollbar_getknobpos, zxwScrollBarKnobPos)


// wgobj setpos val
// wgobj setmin val
// wgobj setmax val
// wgobj setpagesize val
// wgobj setknobpos val
JIMWIDGET_SETINT(scrollbar_setpos, zxwScrollBarSetPos)
JIMWIDGET_SETINT(scrollbar_setmin, zxwScrollBarSetMin)
JIMWIDGET_SETINT(scrollbar_setmax, zxwScrollBarSetMax)
JIMWIDGET_SETINT(scrollbar_setpagesize, zxwScrollBarSetPageSize)
JIMWIDGET_SETINT(scrollbar_setknobpos, zxwScrollBarSetKnobPos)


// wgobj setminmax min max
JIMAPI_WIDGET_FN(scrollbar_setminmax) {
  long amin, amax;
  //
  if (argc != 4) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (Jim_GetLong(interp, argv[2], &amin) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (Jim_GetLong(interp, argv[3], &amax) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (zxwIsAlive(g)) {
    Jim_SetResultBool(interp, zxwScrollBarSetMinMax(g, amin, amax));
    return JIM_OK;
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// wgobj ishbar
// wgobj isvbar
// wgobj ismulti
// wgobj getcount
// wgobj getcurrent
JIMWIDGET_GETINT(listbox_ishbar, zxwListBoxIsHBar)
JIMWIDGET_GETINT(listbox_isvbar, zxwListBoxIsVBar)
JIMWIDGET_GETINT(listbox_ismulti, zxwListBoxIsMulti)
JIMWIDGET_GETINT(listbox_getcount, zxwListBoxCount)
JIMWIDGET_GETINT(listbox_getcurrent, zxwListBoxCurrent)

// wgobj sethbar val
// wgobj setvbar val
// wgobj setmulti val
// wgobj setcurrent val
JIMWIDGET_SETINT(listbox_sethbar, zxwListBoxSetHBar)
JIMWIDGET_SETINT(listbox_setvbar, zxwListBoxSetVBar)
JIMWIDGET_SETINT(listbox_setmulti, zxwListBoxSetMulti)
JIMWIDGET_SETINT(listbox_setcurrent, zxwListBoxSetCurrent)


// wgobj ismarked idx
JIMAPI_WIDGET_FN(listbox_ismarked) {
  long idx;
  //
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (Jim_GetLong(interp, argv[2], &idx) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (zxwIsAlive(g)) {
    Jim_SetResultInt(interp, zxwListBoxIsMarked(g, idx));
    return JIM_OK;
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj setmarked idx val
JIMAPI_WIDGET_FN(listbox_setmarked) {
  long idx, val;
  //
  if (argc != 4) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (Jim_GetLong(interp, argv[2], &idx) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (Jim_GetLong(interp, argv[3], &val) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (zxwIsAlive(g)) {
    if (zxwListBoxSetMarked(g, idx, val) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj getitem idx
JIMAPI_WIDGET_FN(listbox_getitem) {
  long idx;
  //
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (Jim_GetLong(interp, argv[2], &idx) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (zxwIsAlive(g)) {
    const char *t = zxwListBoxItem(g, idx);
    //
    if (t != NULL) {
      Jim_SetResultString(interp, t, -1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj delitem idx
JIMAPI_WIDGET_FN(listbox_delitem) {
  long idx;
  //
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (Jim_GetLong(interp, argv[2], &idx) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (zxwIsAlive(g)) {
    if (zxwListBoxDeleteItem(g, idx) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj setitem idx val
JIMAPI_WIDGET_FN(listbox_setitem) {
  long idx;
  //
  if (argc != 4) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (Jim_GetLong(interp, argv[2], &idx) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (zxwIsAlive(g)) {
    if (zxwListBoxSetItem(g, idx, Jim_String(argv[3])) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj insitem idx val
JIMAPI_WIDGET_FN(listbox_insitem) {
  long idx;
  //
  if (argc != 4) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (Jim_GetLong(interp, argv[2], &idx) != JIM_OK) {
    jim_SetResStrf(interp, "winsys widget: integer expected");
    return JIM_ERR;
  }
  if (zxwIsAlive(g)) {
    if (zxwListBoxInsertItem(g, idx, Jim_String(argv[3])) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


// wgobj additem val
JIMAPI_WIDGET_FN(listbox_additem) {
  if (argc != 3) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  if (zxwIsAlive(g)) {
    if (zxwListBoxAddItem(g, Jim_String(argv[2])) == 0) {
      Jim_SetResultBool(interp, 1);
      return JIM_OK;
    }
  }
  Jim_SetResultBool(interp, 0);
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// wgobj cmd ...
JIMAPI_FN_NOREG(winsys_widget_dispatcher) {
  int cidx;
  ZXWidget *g = Jim_CmdPrivData(interp);
  //
  if (argc < 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  //
  static const char *const cmd0n[] = {
    "window",
    "type",
    "remove",
    "getid",
    "isalive",
    "isvisible",
    "isactive",
    "isenabled",
    "canbevisible",
    "canbeactivated",
    "canprocesskeys",
    "getx",
    "gety",
    "getw",
    "geth",
    "hide",
    "show",
    "activate",
    "setx",
    "sety",
    "setw",
    "seth",
    NULL
  };
  static JimWidgetCmdEx cmd0h[] = {
    jim_winsys_widget_window,
    jim_winsys_widget_type,
    jim_winsys_widget_remove,
    jim_winsys_widget_getid,
    jim_winsys_widget_isalive,
    jim_winsys_widget_isvisible,
    jim_winsys_widget_isactive,
    jim_winsys_widget_isenabled,
    jim_winsys_widget_canbevisible,
    jim_winsys_widget_canbeactivated,
    jim_winsys_widget_canprocesskeys,
    jim_winsys_widget_getx,
    jim_winsys_widget_gety,
    jim_winsys_widget_getw,
    jim_winsys_widget_geth,
    jim_winsys_widget_hide,
    jim_winsys_widget_show,
    jim_winsys_widget_activate,
    jim_winsys_widget_setx,
    jim_winsys_widget_sety,
    jim_winsys_widget_setw,
    jim_winsys_widget_seth,
  };
  if (Jim_GetEnum(interp, argv[1], cmd0n, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmd0h[cidx])(g, interp, argc, argv);
  switch (zxwWidgetType(g)) {
    case ZXWT_STATIC: {
      static const char *const cmdn[] = {
        "getlinked",
        "setlinked",
        NULL
      };
      static JimWidgetCmdEx cmdh[] = {
        jim_winsys_widget_static_getlinked,
        jim_winsys_widget_static_setlinked,
      };
      if (Jim_GetEnum(interp, argv[1], cmdn, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmdh[cidx])(g, interp, argc, argv);
      break; }
    case ZXWT_BUTTON: {
      static const char *const cmdn[] = {
        "setdefault",
        "setcancel",
        "setnormal",
        NULL
      };
      static JimWidgetCmdEx cmdh[] = {
        jim_winsys_widget_button_setdefault,
        jim_winsys_widget_button_setcancel,
        jim_winsys_widget_button_setnormal,
      };
      if (Jim_GetEnum(interp, argv[1], cmdn, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmdh[cidx])(g, interp, argc, argv);
      break; }
    case ZXWT_CHECKBOX: {
      static const char *const cmdn[] = {
        "getstate",
        "setstate",
        NULL
      };
      static JimWidgetCmdEx cmdh[] = {
        jim_winsys_widget_checkbox_getstate,
        jim_winsys_widget_checkbox_setstate,
      };
      if (Jim_GetEnum(interp, argv[1], cmdn, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmdh[cidx])(g, interp, argc, argv);
      break; }
    case ZXWT_RADIO: {
      static const char *const cmdn[] = {
        "getradioactive",
        "setradioactive",
        "getgroup",
        NULL
      };
      static JimWidgetCmdEx cmdh[] = {
        jim_winsys_widget_radio_getradioactive,
        jim_winsys_widget_radio_setradioactive,
        jim_winsys_widget_radio_getgroup,
      };
      if (Jim_GetEnum(interp, argv[1], cmdn, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmdh[cidx])(g, interp, argc, argv);
      break; }
    case  ZXWT_LINEEDIT: {
      static const char *const cmdn[] = {
        "gettext",
        "settext",
        NULL
      };
      static JimWidgetCmdEx cmdh[] = {
        jim_winsys_widget_lineedit_gettext,
        jim_winsys_widget_lineedit_settext,
      };
      if (Jim_GetEnum(interp, argv[1], cmdn, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmdh[cidx])(g, interp, argc, argv);
      break; }
    case ZXWT_SCROLLBAR: {
      static const char *const cmdn[] = {
        "isvert",
        "ishoriz",
        "getlinked",
        "setlinked",
        "getpos",
        "setpos",
        "getmin",
        "setmin",
        "getmax",
        "setmax",
        "setminmax",
        "getpagesize",
        "setpagesize",
        "getknobsize",
        "getknobpos",
        "setknobpos",
        NULL
      };
      static JimWidgetCmdEx cmdh[] = {
        jim_winsys_widget_scrollbar_isvert,
        jim_winsys_widget_scrollbar_ishoriz,
        jim_winsys_widget_scrollbar_getlinked,
        jim_winsys_widget_scrollbar_setlinked,
        jim_winsys_widget_scrollbar_getpos,
        jim_winsys_widget_scrollbar_setpos,
        jim_winsys_widget_scrollbar_getmin,
        jim_winsys_widget_scrollbar_setmin,
        jim_winsys_widget_scrollbar_getmax,
        jim_winsys_widget_scrollbar_setmax,
        jim_winsys_widget_scrollbar_setminmax,
        jim_winsys_widget_scrollbar_getpagesize,
        jim_winsys_widget_scrollbar_setpagesize,
        jim_winsys_widget_scrollbar_getknobsize,
        jim_winsys_widget_scrollbar_getknobpos,
        jim_winsys_widget_scrollbar_setknobpos,
      };
      if (Jim_GetEnum(interp, argv[1], cmdn, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmdh[cidx])(g, interp, argc, argv);
      break; }
    case ZXWT_LISTBOX: {
      static const char *const cmdn[] = {
        "ishbar",
        "isvbar",
        "sethbar",
        "setvbar",
        "getcount",
        "getitem",
        "setitem",
        "delitem",
        "insitem",
        "additem",
        "ismarked",
        "setmarked",
        "getcurrent",
        "setcurrent",
        "ismulti",
        "setmulti",
        NULL
      };
      static JimWidgetCmdEx cmdh[] = {
        jim_winsys_widget_listbox_ishbar,
        jim_winsys_widget_listbox_isvbar,
        jim_winsys_widget_listbox_sethbar,
        jim_winsys_widget_listbox_setvbar,
        jim_winsys_widget_listbox_getcount,
        jim_winsys_widget_listbox_getitem,
        jim_winsys_widget_listbox_setitem,
        jim_winsys_widget_listbox_delitem,
        jim_winsys_widget_listbox_insitem,
        jim_winsys_widget_listbox_additem,
        jim_winsys_widget_listbox_ismarked,
        jim_winsys_widget_listbox_setmarked,
        jim_winsys_widget_listbox_getcurrent,
        jim_winsys_widget_listbox_setcurrent,
        jim_winsys_widget_listbox_ismulti,
        jim_winsys_widget_listbox_setmulti,
      };
      if (Jim_GetEnum(interp, argv[1], cmdn, &cidx, "widget method", JIM_NONE) == JIM_OK) return (cmdh[cidx])(g, interp, argc, argv);
      break; }
  }
  //
  jim_SetResStrf(interp, "winsys widget: unknown command: '%s'", Jim_String(argv[1]));
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
static int wtp2ZXWT (const char *t) {
  if (strcmp(t, "frame") == 0) return ZXWT_FRAME;
  if (strcmp(t, "static") == 0) return ZXWT_STATIC;
  if (strcmp(t, "button") == 0) return ZXWT_BUTTON;
  if (strcmp(t, "checkbox") == 0) return ZXWT_CHECKBOX;
  if (strcmp(t, "radio") == 0) return ZXWT_RADIO;
  if (strcmp(t, "lineedit") == 0) return ZXWT_LINEEDIT;
  if (strcmp(t, "scrollbar") == 0) return ZXWT_SCROLLBAR;
  if (strcmp(t, "listbox") == 0) return ZXWT_LISTBOX;
  return 0;
}


// winobj cmd ...
JIMAPI_FN_NOREG(winsys_window_dispatcher) {
  ZXWindow *w = Jim_CmdPrivData(interp);
  int cidx, wtp;
  //
  if (argc < 2) { Jim_WrongNumArgs(interp, argc, argv, "cmd ...?"); return JIM_ERR; }
  //
  if (strcmp(Jim_String(argv[1]), "type") == 0) {
    if (w == NULL || !zxwinIsAlive(w)) {
      Jim_SetResultString(interp, "", -1);
    } else {
      Jim_SetResultString(interp, "window", -1);
    }
    return JIM_OK;
  }
  //
  if ((wtp = wtp2ZXWT(Jim_String(argv[1]))) != 0) {
    return jim_winsys_widget_new(w, wtp, interp, argc, argv);
  }
  //
  static const char *const cmd0n[] = {
    "close",
    "findbyid",
    "getid",
    "getx",
    "gety",
    "getw",
    "geth",
    "isalive",
    "isvisible",
    "isactive",
    "isenabled",
    "canbeactivated",
    "activate",
    "getradioactive",
    "setradioactive",
    "setx",
    "sety",
    "setw",
    "seth",
    "gettitle",
    "settitle",
    "hide",
    "show",
    NULL
  };
  static JimWinCmdEx cmd0h[] = {
    jim_winsys_window_close,
    jim_winsys_widget_findbyid,
    jim_winsys_window_getid,
    jim_winsys_window_getx,
    jim_winsys_window_gety,
    jim_winsys_window_getw,
    jim_winsys_window_geth,
    jim_winsys_window_isalive,
    jim_winsys_window_isvisible,
    jim_winsys_window_isactive,
    jim_winsys_window_isenabled,
    jim_winsys_window_canbeactivated,
    jim_winsys_window_activate,
    jim_winsys_window_getradioactive,
    jim_winsys_window_setradioactive,
    jim_winsys_window_setx,
    jim_winsys_window_sety,
    jim_winsys_window_setw,
    jim_winsys_window_seth,
    jim_winsys_window_gettitle,
    jim_winsys_window_settitle,
    jim_winsys_window_hide,
    jim_winsys_window_show,
  };
  if (Jim_GetEnum(interp, argv[1], cmd0n, &cidx, "window method", JIM_ERRMSG) == JIM_OK) {
    return (cmd0h[cidx])(w, interp, argc, argv);
  }
  //
  if (argc == 2) {
    if (zxwinIsAlive(w)) {
      ZXWidget *g = zxwFindById(w, Jim_String(argv[1]));
      //
      if (g != NULL) {
        Jim_SetResultString(interp, createWidgetCmd(g, jim_winsys_widget_dispatcher), -1);
        return JIM_OK;
      }
    }
    //
    Jim_SetResultBool(interp, 0);
    return JIM_OK;
  }
  //
  jim_SetResStrf(interp, "window: unknown command: '%s'", Jim_String(argv[1]));
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// winsys window|findbyid ...
JIMAPI_FN(winsys) {
  int cidx;
  //
  if (argc < 2) {
    Jim_WrongNumArgs(interp, argc, argv, "cmd ...?");
    return JIM_ERR;
  }
  //
  static const char *const cmd0n[] = {
    "window",
    "findbyid",
    NULL
  };
  static Jim_CmdProc cmd0h[] = {
    jim_winsys_window_new,
    jim_winsys_window_findbyid,
  };
  if (Jim_GetEnum(interp, argv[1], cmd0n, &cidx, "winsys method", JIM_ERRMSG) == JIM_OK) {
    return (cmd0h[cidx])(interp, argc, argv);
  }
  if (argc == 2) {
    ZXWindow *w;
    //
    if ((w = zxwinFindById(Jim_String(argv[1]))) != NULL) {
      Jim_SetResultString(interp, createWinCmd(w, jim_winsys_window_dispatcher), -1);
      return JIM_OK;
    }
    Jim_SetResultBool(interp, 0);
    return JIM_OK;
  }
  return JIM_ERR;
}
