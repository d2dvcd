// in: map coords; out: screen coords
// result: trace successfull
static ccBool traceindir (int xstart, int ystart, int xend, int yend, int *ex, int *ey) {
  int mex = FLDW*CELW, mey = FLDH*CELH; // max coords
  int x = xstart, y = ystart;
  int sx, sy; // directions (-1, 0, 1)
  int xd, yd; // xend and yend moved by (-xstart, -ystart), absolute values
  int d; // 'highest step delta'
  int lx, ly; // coords on map for the previous step
  int xsf, ysf; // previous 'fracs'
  int xfrac, yfrac;
  int maxdist, distcheck = 32;
  //
  // convert xd and yd to deltas from (x, y), determine directions
  sx = Z_sign(xd = xend-xstart);
  sy = Z_sign(yd = yend-ystart);
  //
  *ex = Z_MAP2SCR_X(xend);
  *ey = Z_MAP2SCR_Y(yend+(sy>>1)); // trick to avoid condition
  //
  if (!(xd|yd)) return CC_FALSE;
  //
  maxdist = xd*xd+yd*yd;
  if ((xd = abs(xd)) > (yd = abs(yd))) d = xd; else d = yd;
  xfrac = yfrac = xsf = ysf = 0;
  lx = x;
  ly = y;
  while (x >= 0 && x < mex && y >= 0 && y < mey) {
    if ((BM_get(x, y)&BM_WALL)) {
      // there is some wall here, do precision tracing
      //xpix3(Z_MAP2SCR_X(x), Z_MAP2SCR_Y(y));
      if (fldhit1(x, y)) {
        // we are inside the tile, trace to it's outer edge
        int xdist, ydist;
        //
        // restore x, y and fracs, effectively going back to the previous tile
        x = lx;
        y = ly;
        xfrac = xsf;
        yfrac = ysf;
        // trace
        while (!fldhit1(x, y)) {
          // save x and y
          lx = x;
          ly = y;
          if ((xfrac += xd) >= d) { xfrac -= d; x += sx; }
          if ((yfrac += yd) >= d) { yfrac -= d; y += sy; }
        }
        // got it, check if we are still 'in distance'
        xdist = lx-xstart;
        ydist = ly-ystart;
        if (xdist*xdist+ydist*ydist <= maxdist) {
          // ok, valid coords
          *ex = Z_MAP2SCR_X(lx);
          *ey = Z_MAP2SCR_Y(ly+(sy>>1)); // trick to avoid condition
        }
        break;
      }
      // no wall, step by almost one tile
      // save x, y and fracs
      lx = x;
      ly = y;
      xsf = xfrac;
      ysf = yfrac;
      if ((xfrac += (xd*(CELW-1))) >= d) {
        x += (xfrac/d)*sx;
        xfrac = xfrac%d;
      }
      if ((yfrac += (yd*(CELH-1))) >= d) {
        y += (yfrac/d)*sy;
        yfrac = yfrac%d;
      }
    } else {
      // there is no wall here, skip whole 'megatile' (4x4 ordinary tiles, 32x32 pixels)
      int skipx, skipy;
      //
      if (--distcheck < 1) {
        int xdist = x-xstart, ydist = y-ystart;
        //
        distcheck = 32;
        if (xdist*xdist+ydist*ydist > maxdist) break;
      }
      // calculate number x of pixels to skip to cross the end of 'megatile'
      // using trick to avoid condition: if (sx > 0) skipx = 31-skipx;
      skipx = (sx != 0 ? ((x&31)^(((-sx)>>1)&31))+1 : 0);
      // calculate number y of pixels to skip to cross the end of 'megatile'
      skipy = (sy != 0 ? ((y&31)^(((-sy)>>1)&31))+1 : 0);
      //
      // use 'skipx' as general 'skip'
      if ((skipy < skipx && skipy != 0) || skipx == 0) skipx = skipy;
      // save x, y and fracs
      lx = x;
      ly = y;
      xsf = xfrac;
      ysf = yfrac;
      //
      x += ((xd*skipx+xfrac)/d)*sx;
      xfrac = (xd*skipx+xfrac)%d;
      y += ((yd*skipx+yfrac)/d)*sy;
      yfrac = (yd*skipx+yfrac)%d;
    }
  }
  //
  //xpix1(Z_MAP2SCR_X(xstart), Z_MAP2SCR_Y(ystart));
  //xpix2(*ex, *ey);
  return CC_TRUE;
}
