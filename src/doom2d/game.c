/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "game.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"

#include "anm.h"
#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "config.h"
#include "dots.h"
#include "edit.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "gamemode.h"
#include "items.h"
#include "jimapi.h"
#include "keyb.h"
#include "map.h"
#include "mapio.h"
#include "membuffer.h"
#include "menu.h"
#include "misc.h"
#include "monster.h"
#include "netgame.h"
#include "player.h"
#include "saveload.h"
#include "sdldrv.h"
#include "smoke.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "vgaovl.h"
#include "view.h"
#include "weapons.h"
#include "wipe.h"


////////////////////////////////////////////////////////////////////////////////
#define LT_DELAY    (8)
#define LT_HITTIME  (6)

#define PL_FLASH  (90)

#define MAX_DMPOS  (100)

////////////////////////////////////////////////////////////////////////////////
int gnet_state;
int g_frame_advanced = 0;
uint8_t cli_warp = 0;
int g_plrcount = 1;
ccBool g_dm = CC_FALSE;
d2dLevelExit g_exit = D2D_LEVEL_EXIT_NONE;
uint8_t g_st = GS_TITLE;
uint8_t g_map = 1;
char g_music[8] = "MENU";
uint8_t _net = 0;
uint32_t g_time;
int dm_pnum, dm_pl1p, dm_pl2p;
pos_t dm_pos[MAX_DMPOS];
uint8_t pl_action_order = 0;

static const snd_t *telepsnd;
static vgaimg scrnh[3];
//static vgaimg cd_scr;

int lt_time, lt_type, lt_side, lt_ypos, lt_force;
static const snd_t *ltnsnd[2];

int g_trans = 0, g_transt;
static uint8_t transdraw = 0;

int g_frame_ticks_start = 0;


////////////////////////////////////////////////////////////////////////////////
int g_edit_mode = 0;
int g_edit_mode_prev = 0;

CONVAR_PROT_INT_CB(g_edit_mode, g_edit_mode, CMDCON_FLAGS_NET_RO, N_var_protector, "set edit mode") {
  if (g_st == GS_GAME && n_host == NULL) {
    if (!g_edit_mode_prev && g_edit_mode) {
      ed_ofs_x = pl[0].o.x;
      ed_ofs_y = pl[0].o.y-12;
    }
    if (g_edit_mode_prev != g_edit_mode) {
      (g_edit_mode ? ED_enter : ED_leave)();
    }
    //
    g_edit_mode_prev = g_edit_mode;
    if (g_edit_mode) GM_set(NULL);
  } else {
    if (g_edit_mode) ED_leave();
    g_edit_mode = g_edit_mode_prev = 0;
  }
}


////////////////////////////////////////////////////////////////////////////////
static void set_trans (int st) {
  g_trans = 0;
  g_transt = 0;
  //
  switch (st) {
    case GS_ENDANIM:
    case GS_END2ANIM:
      g_st = st;
      ANM_start((st == GS_ENDANIM ? "ENDANIM" : "END2ANIM"));
      return;
  }
  //
  if (st != GS_GAME) {
    if (g_edit_mode != 0) ED_leave();
    g_edit_mode_prev = g_edit_mode = 0;
    if (g_st == GS_GAME) demo_done();
  }
  //
  switch (g_st) {
    case GS_ENDANIM:
    case GS_END2ANIM:
    case GS_DARKEN:
    case GS_WAITING_CONNECTION:
      g_st = st;
      return;
  }
  //
  if (st == GS_GAME && n_host != NULL) {
    g_trans = 0;
    g_transt = 0;
  } else {
    g_trans = 1;
    g_transt = 0;
    //
    if (!wipe_init()) {
      g_trans = 0;
      g_transt = 0;
    }
  }
  //
  if ((g_st = st) == GS_GAME) {
    player_unpack_keys(0, 0);
    player_unpack_keys(1, 0);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void internal_init_game (void) {
  set_trans(GS_GAME);
  for (int f = 0; f < g_plrcount; ++f) {
    pl[f].drawst = 0xFF;
    pl[f].w_prev_x = pl[f].w_new_x = pl[f].o.x;
    pl[f].w_prev_y = pl[f].w_new_y = pl[f].o.y;
    pl[f].w_first_time = 1;
  }
  //BM_remapfld(0);
  BM_queue_remap();
  BM_clear_maybe_remap(BM_PLR1|BM_PLR2|BM_MONSTER);
  for (int f = 0; f < g_plrcount; ++f) BM_mark(&pl[f].o, (f == 0 ? BM_PLR1 : BM_PLR2));
  MN_mark();
  S_startmusic(music_time);
}


static int do_load_game (int (*ldr)(void)) {
  demo_done();
  F_freemus();
  W_init();
  if (ldr() == 0) {
    internal_init_game();
    return 0;
  } else {
    W_init();
    set_trans(GS_TITLE);
    return -1;
  }
}


int load_game (int n) {
  return do_load_game(cmdcon_lambda(int, (void) { return F_loadgame(n); }));
}


int load_game_fn (const char *fname) {
  return do_load_game(cmdcon_lambda(int, (void) { return Fl_loadgame(fname); }));
}


//TODO: send 'level start' network packet here
void G_start (void) {
  char s[8];
  //
  pl_action_order = 0;
  if (g_map < 0) g_map = 0;
  else if (g_map > 99) g_map = 99; //k8
  //
  F_freemus();
  W_init();
  sprintf(s, "MAP%02d", g_map);
  //
  if (F_loadmap(s) != 0) {
    // bad map, die here
    conlogf("error loading map '%s'!\n", s);
    set_trans(GS_TITLE);
    return;
  }
  //
  internal_init_game();
  //
  g_exit = D2D_LEVEL_EXIT_NONE;
  itm_rtime = (g_dm ? 1092 : 0);
  if (g_map == 1) p_immortal = 0;
  p_ghost = 0; //k8
  p_fly = 0;
  PL_JUMP = 10;
  g_time = 0;
  lt_time = 1000;
  lt_force = 1;
  if (g_plrcount == 1) pl[0].lives = 3;
  //
  /*
  pl[0].keys = pl[1].keys = 0x70;
  SW_cheat_open();
  */
  //
  {
    char cmd[128];
    //
    sprintf(cmd, "exec %smap%02d", (n_host != NULL ? "nw_" : ""), g_map);
    cmdcon_exec(cmd);
  }
}


void G_end (void) {
  set_trans(GS_TITLE);
}


#define GGAS_TOTAL  (MN__LAST-MN_DEMON+16+10)

void G_init (void) {
  conlogf("G_init: initializing game resources");
  conlogdots(5, GGAS_TOTAL);
  telepsnd = Z_getsnd("TELEPT");
  V_loadvga("TITLEPIC", &scrnh[0]);
  V_loadvga("INTERPIC", &scrnh[1]);
  V_loadvga("ENDPIC", &scrnh[2]);
  //cd_scr = M_lock(F_getresid("CD1PIC"));
  for (int i = 0; i < 2; ++i) {
    char s[9];
    //
    sprintf(s, "LTN%c", i+'1');
    for (int j = 0; j < 2; ++j) Z_getspr(&ltn[i][j], s, j, 0, NULL);
  }
  ltnsnd[0] = Z_getsnd("THUND1");
  ltnsnd[1] = Z_getsnd("THUND2");
  DOT_alloc();
  SMK_alloc();
  FX_alloc();
  WP_alloc();
  IT_alloc();
  SW_alloc();
  PL_alloc();
  MN_alloc();
  Z_initst();
  conlogdots(GGAS_TOTAL, GGAS_TOTAL);
  conlogf("G_init: resource initialization complete");
  GM_init();
  pl[0].color = 0x70;
  pl[1].color = 0x60;
  g_trans = 0;
}


CONCMD(anim, "test anim") {
  CMDCON_CHECK_NONET();
  set_trans(GS_ENDANIM);
}


////////////////////////////////////////////////////////////////////////////////
#define STOP_TRANS()  do { \
  wipe_deinit(); \
  g_trans = 0; \
  g_transt = 0; \
} while (0)

static int act_transition_fx (void) {
  if (g_trans) {
    convisible = 0;
    transdraw = 1;
    G_draw();
    transdraw = 0;
    //
    if (lastkey == SDLK_ESCAPE || lastkey == SDLK_TAB || lastkey == SDLK_BACKQUOTE) {
      STOP_TRANS();
    } else {
      if (!g_frame_advanced) return 1; // old frame, just wait
      if (!wipe_step()) STOP_TRANS();
    }
    //
    if (lastkey != SDLK_BACKQUOTE) {
      lastkey = 0;
      return 1;
    }
  }
  //
  if (lastkey == SDLK_BACKQUOTE) {
    if (!convisible || !coninput[0] || (lastkeyevt.keysym.mod&(KMOD_CTRL|KMOD_ALT|KMOD_META))) {
      lastkey = 0;
      convisible = !convisible;
      conbotline = 0;
    }
  }
  //
  return (g_trans);
}

#undef STOP_TRANS


////////////////////////////////////////////////////////////////////////////////
static int act_console (void) {
  if (convisible && lastkey != 0) {
    int eaten = 1;
    //
    switch (lastkey) {
      case SDLK_TAB: conhistoryCopy(); contabcompletion(); break;
      case SDLK_ESCAPE: convisible = 0; conbotline = 0; break;
      case SDLK_PAGEUP: case SDLK_KP9: conbotline += conheight-2; break;
      case SDLK_PAGEDOWN: case SDLK_KP3: conbotline -= conheight-2; break;
      case SDLK_UP: case SDLK_KP8:
        if (lastkeyevt.keysym.mod&KMOD_ALT) ++conbotline;
        else if (++conhistorypos >= MAX_CON_HISTORY || conhistory[conhistorypos] == NULL) --conhistorypos;
        break;
      case SDLK_DOWN:case SDLK_KP2:
        if (lastkeyevt.keysym.mod&KMOD_ALT) --conbotline;
        else if (--conhistorypos < -1) conhistorypos = -1;
        break;
      case SDLK_BACKSPACE:
        conhistoryCopy();
        if (lastkeyevt.keysym.mod&KMOD_CTRL) condelword(); else condelchar();
        break;
      case SDLK_RETURN: case SDLK_KP_ENTER:
        if (conhistorypos >= 0 && conhistorypos < MAX_CON_HISTORY && conhistory[conhistorypos] != NULL) {
          conhistoryCopy();
        }
        conhistorypos = -1;
        if (coninput[0]) {
          conHistoryAdd(coninput);
          if (coninput[0] == '.' && coninput[1]) {
            if (Jim_Eval(jim, coninput+1) == JIM_OK) {
              conlogf("%s\n", Jim_GetString(Jim_GetResult(jim), NULL));
            } else {
              Jim_MakeErrorMessage(jim);
              conlogf("JIM ERROR: %s\n", Jim_GetString(Jim_GetResult(jim), NULL));
            }
          } else if (coninput[0] == '<' && coninput[1]) {
            jimEvalFile(coninput+1, 0);
          } else if (cmdcon_exec(coninput) < 0) {
            conlogf("invalid command: %s\n", coninput);
          }
        }
        coninput[0] = 0;
        break;
      case SDLK_LEFT: case SDLK_KP4:
      case SDLK_RIGHT: case SDLK_KP6:
      case SDLK_HOME: case SDLK_KP7:
      case SDLK_END: case SDLK_KP1:
      case SDLK_KP5: case SDLK_KP0:
        break;
      case SDLK_y:
        if (lastkeyevt.keysym.mod&KMOD_CTRL) {
          coninput[0] = 0;
          break;
        }
        // fallthru
      default:
        if (lastkeychar >= 32 && lastkeychar < 127) {
          conhistoryCopy();
          conaddchar(lastkeychar);
        } else {
          eaten = 0;
        }
        break;
    }
    if (eaten) lastkey = lastkeychar = 0;
    lastkey = lastkeychar = 0;
  }
  //
  return (convisible && n_host == NULL);
}


////////////////////////////////////////////////////////////////////////////////
static int act_animations (void) {
  if (g_st == GS_ENDANIM || g_st == GS_END2ANIM) {
    if (!ANM_nextframe()) {
      ANM_close();
      switch (g_st) {
        case GS_ENDANIM: set_trans(GS_DARKEN); break;
        case GS_END2ANIM: set_trans(GS_ENDSCR); break;
      }
    } else {
      lastkey = 0;
    }
    //
    return 1;
  }
  //
  if (g_st == GS_DARKEN) {
    set_trans(GS_END2ANIM);
    return 1;
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static void act_sky (void) {
  if (sky_type == 2) {
    if (lt_time > LT_DELAY || lt_force) {
      if (!(myrandmask1(31)) || lt_force) {
        lt_force = 0;
        lt_time = -LT_HITTIME;
        lt_type = myrand1(2);
        lt_side = myrandbit1();
        lt_ypos = myrandmask1(31);
        Z_sound(ltnsnd[myrandbit1()], 128);
      }
    } else {
      ++lt_time;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static int is_interm_fire (void) {
  return (keys[SDLK_SPACE] || keys[SDLK_RETURN] || keys[SDLK_KP_ENTER] || (n_host != NULL && keys[SDLK_ESCAPE]));
}


static int act_misc_actions (void) {
  switch (g_st) {
    case GS_TITLE: case GS_ENDSCR:
      return 1;
    case GS_INTER:
      if (is_interm_fire()) G_start();
      return 1;
    case GS_NETINTER:
      return 1;
    case GS_WAITING_CONNECTION:
      if (lastkey == SDLK_ESCAPE) {
        N_disconnect();
        set_trans(GS_TITLE);
        lastkey = 0;
      }
      return 1;
    case GS_GAME:
      if (g_edit_mode) ED_act();
      if (g_paused || g_edit_mode) return 1;
      //
      if (n_host != NULL && gnet_state == GNETS_HANDSHAKING) {
        if (lastkey == SDLK_ESCAPE) {
          N_disconnect();
          set_trans(GS_TITLE);
          lastkey = 0;
        }
        return 1;
      }
      break;
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static void act_check_exit (void) {
  char mapname[16];
  //
  if ((int)g_exit <= D2D_LEVEL_EXIT_NONE || g_exit >= D2D_LEVEL_EXIT_MAXVALUE) return;
  //
  demo_done();
  //
  switch (g_exit) {
    case D2D_LEVEL_EXIT_NORMAL:
      switch (g_map) {
        /*
        case 19:
          set_trans(GS_ENDANIM);
          //A8_start("FINAL");
          break;
        */
        case 31:
        case 32:
          g_map = 16;
          break;
        default:
          ++g_map;
          if (g_map == 31 || g_map == 32) g_map = 33; // skip 2 secret maps
          break;
      }
next_level:
      if (g_map > 99) goto winner;
      sprintf(mapname, "MAP%02d", g_map);
      if (F_findres(mapname) < 0) goto winner; // no such map? hee-hoo!
      //
      set_trans(n_host ? GS_NETINTER : GS_INTER);
      //gnet_state = GNETS_INTER_WAIT_FIRE_PL12; // this will not hurt in non-network game
      F_freemus();
      F_loadmus("INTERMUS");
      break;
    case D2D_LEVEL_EXIT_SECRET:
      switch (g_map) {
        case 31: g_map = 32; break;
        case 32: g_map = 16; break;
        default: g_map = 31; break;
      }
      goto next_level;
    case D2D_LEVEL_EXIT_WINGAME:
    case D2D_LEVEL_EXIT_WINGAME_ROBO:
winner:
      set_trans((g_exit == D2D_LEVEL_EXIT_WINGAME_ROBO ? GS_ENDANIM : GS_END2ANIM));
      F_freemus();
      F_loadmus("\x8a\x8e\x8d\x85\x96\x0");
    default:
      return;
  }
  S_startmusic(0);
}


////////////////////////////////////////////////////////////////////////////////
static int act_check_startup_args (void) {
  char cmd[128];
  //
  if (cli_as_server) {
    cli_as_server = CC_FALSE;
    if (cli_warp < 1) cli_warp = 1;
    else if (cli_warp > 99) cli_warp = 99;
    sprintf(cmd, "gcmd_network_start_server %u %s", cli_warp, (cli_mode_dm ? "dm" : "coop"));
  } else if (cli_as_client) {
    cli_as_client = CC_FALSE;
    strcpy(cmd, "gcmd_network_start_client");
  } else {
    return 0;
  }
  //
  cmdcon_exec(cmd);
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
void G_act (void) {
  //FIXME: gum solution
  if (act_check_startup_args()) return;
  //
  if (act_transition_fx()) return;
  if (act_console()) return;
  //
  if (n_host != NULL) {
    if (lastkey == SDLK_ESCAPE && gnet_state == GNETS_HANDSHAKING) {
      N_disconnect();
      lastkey = 0;
      set_trans(GS_TITLE);
      return;
    }
  }
  // this is still old frame, don't do anything fancy
  if (!g_frame_advanced) {
    if (act_misc_actions()) return;
    if (g_st == GS_GAME && g_edit_mode) ED_act();
    return;
  }
  //
  if (act_animations()) return;
  //
  // menu actions
  if (!g_edit_mode || g_st != GS_GAME) {
    int m = GM_act();
    //
    if (n_host == NULL && m) return;
    if (m) lastkey = 0;
  }
  //
  if (act_misc_actions()) return;
  //
  act_sky();
  //
  ++g_time;
  for (int f = 0; f < g_plrcount; ++f) { pl[f].hit = 0; pl[f].hito = -3; }
  G_code(); // cheat codes
  //
  W_act(); // 'world' actions (tile animation)
  IT_act(); // item actions
  SW_act(); // switch actions
  //
  demo_begin_frame();
  for (int f = 0; f < g_plrcount; ++f) PL_act(&pl[(pl_action_order+f)%g_plrcount]);
  pl_action_order = (pl_action_order+1)%g_plrcount;
  demo_end_frame();
  //
  MN_act(); // monster actions
  //if (BM_need_remap()) BM_remapfld(0);
  BM_clear_maybe_remap(BM_PLR1|BM_PLR2|BM_MONSTER);
  for (int f = 0; f < g_plrcount; ++f) BM_mark(&pl[f].o, (f == 0 ? BM_PLR1 : BM_PLR1));
  MN_mark(); // mark monsters on 'bmap'
  WP_act(); // weapon projectile actions
  DOT_act(); // dot actions
  SMK_act(); // smoke, etc. actions
  FX_act(); // bulbs, etc actions
  //
  for (int f = 0; f < g_plrcount; ++f) {
    PL_damage(&pl[f]);
    if (!(pl[f].f&PLF_PNSND) && pl[f].pain) PL_cry(&pl[f]);
    if ((pl[f].pain -= 5) < 0) { pl[f].pain = 0; pl[f].f &= (0xFFFF-PLF_PNSND); }
  }
  //
  act_check_exit();
}



ccBool r_interpolate_panning = CC_FALSE;
CONVAR_BOOL(r_interpolate_panning, r_interpolate_panning, /*CMDCON_FLAGS_PERSISTENT*/CMDCON_FLAGS_READONLY, "interpolate panning")


static void drawview (player_t *p) {
  if (p->looky<-SCRH/4) p->looky = -SCRH/4;
  else if (p->looky > SCRH/4) p->looky = SCRH/4;
  //
  if (!g_edit_mode) {
    int passed;
    //
    p->w_new_x = w_viewofs_x = p->o.x;
    p->w_new_y = w_viewofs_y = p->o.y-12+p->looky;
    //
    if (r_interpolate_panning) {
      passed = SDL_GetTicks()-g_frame_ticks_start;
      if (p->w_first_time || passed >= DELAY) {
        p->w_first_time = 0;
        p->w_prev_x = p->w_new_x;
        p->w_prev_y = p->w_new_y;
      } else if (passed < DELAY) {
        int dx = p->w_new_x-p->w_prev_x, sx = Z_sign(dx);
        int dy = p->w_new_y-p->w_prev_y, sy = Z_sign(dy);
        //
        if (sx) w_viewofs_x = p->w_prev_x+abs(dx)*(passed)/DELAY*sx;
        if (sy) w_viewofs_y = p->w_prev_y+abs(dy)*(passed)/DELAY*sy;
      }
    } else {
      p->w_first_time = 0;
      p->w_prev_x = p->w_new_x;
      p->w_prev_y = p->w_new_y;
    }
  } else {
    w_viewofs_x = ed_ofs_x;
    w_viewofs_y = ed_ofs_y;
  }
  //
  W_draw(p);
}


static int get_pu_st (int t) {
  if (t >= PL_FLASH) return 1;
  if ((t/9)&1) return 0;
  return 1;
}


static void pl_info (player_t *p, int y) {
  uint32_t t = p->kills*10920/g_time;
  //
  Z_gotoxy(25, y);
  Z_printbf("KILLS");
  Z_gotoxy(25, y+15);
  Z_printbf("KPM");
  Z_gotoxy(25, y+30);
  Z_printbf("SECRETS %u OF %u", p->secrets, sw_secrets);
  Z_gotoxy(255, y);
  Z_printbf("%u", p->kills);
  Z_gotoxy(255, y+15);
  Z_printbf("%u.%u", t/10, t%10);
}


static ccBool r_show_version = CC_TRUE;
CONVAR_BOOL(r_show_version, r_show_version, CMDCON_FLAGS_PERSISTENT, "show game version")


static void show_version (void) {
  int len = Z_strfwidthsf("v" DOOM2D_VERSION);
  //
  Z_gotoxy(SCRW-len-1, 1);
  Z_printsf("v" DOOM2D_VERSION);
}


static int plr_check_health (const player_t *p) {
  if (p->invl) return get_pu_st(p->invl)*6;
  if (p->pain < 15) return 0;
  if (p->pain < 35) return 1;
  if (p->pain < 55) return 2;
  if (p->pain < 75) return 3;
  if (p->pain < 95) return 4;
  return 5;
}


static void center_msg (const char *msg) {
  int w = Z_strfwidthbf("%s", msg);
  //
  Z_gotoxy((SCRW-w)/2, (SCRH-16)/2);
  Z_printbf("%s", msg);
}


void G_draw (void) {
  int h;
  uint16_t hr, mn, sc;
  //
  V_setrect(0, SCRW, 0, SCRH);
  //
  if (g_trans && !transdraw) return;
  //
  memset(vscrbuf, 0, SCRW*SCRH);
  memset(scrlightmap, (r_lighting ? /*LMAP_BACK*/LMAP_NORMAL : LMAP_FULLLIT), SCRW*SCRH);
  //
  switch (g_st) {
    case GS_ENDANIM: case GS_END2ANIM: case GS_DARKEN:
      return;
    case GS_TITLE:
    case GS_WAITING_CONNECTION:
      V_clr(0, SCRW, 0, SCRH, 0);
      V_center(1);
      V_pic(0, 0, &scrnh[0]);
      V_center(0);
      if (r_show_version) show_version();
      break;
    case GS_ENDSCR:
      V_clr(0, SCRW, 0, SCRH, 0);
      V_center(1);
      V_pic(0, 0, &scrnh[2]);
      V_center(0);
      break;
    case GS_INTER: case GS_NETINTER:
      V_clr(0, SCRW, 0, SCRH, 0);
      V_center(1);
      V_pic(0, 0, &scrnh[1]);
      Z_gotoxy(60, 20);
      Z_printbf("LEVEL COMPLETE");
      Z_calc_time(g_time, &hr, &mn, &sc);
      Z_gotoxy(115, 40);
      Z_printbf("TIME %u:%02u:%02u", hr, mn, sc);
      h = 60;
      if (g_plrcount == 2) {
        Z_gotoxy(80, h);
        Z_printbf("PLAYER ONE");
        Z_gotoxy(80, h+70);
        Z_printbf("PLAYER TWO");
        h += SCRH/10;
      }
      pl_info(&pl[0], h);
      if (g_plrcount == 2) pl_info(&pl[1], h+70);
      V_center(0);
      break;
  }
  //
  if (g_st != GS_GAME) {
    if (g_st == GS_WAITING_CONNECTION || (n_host != NULL && gnet_state == GNETS_HANDSHAKING)) {
      V_fadescr(120);
      center_msg("WAITING");
      drawInn((SCRW-32)/2, (SCRH-32)/2+20, (SDL_GetTicks()/83)&0x07); // hotspot:(13,13)
      return;
    }
    if (!g_trans) {
      V_center(1);
      GM_draw();
      V_center(0);
      //V_copytoscr(0, SCRW, 0, SCRH);
    }
    return;
  }
  //
  w_viewofs_vert = 0;
  w_view_wdt = SCRW-120;
  w_view_hgt = SCRH;
  //
  for (int plnum = 0; plnum < g_plrcount; ++plnum) {
    int hgt = SCRH/g_plrcount, y = hgt*plnum;
    //
    if (n_host == NULL) {
      w_viewofs_vert = y;
      w_view_wdt = SCRW-120;
      w_view_hgt = hgt;
      if (plnum == 1) { ++w_viewofs_vert; w_view_hgt -= 2; }
    } else if (plnum != n_local_player_num) {
      goto nodrawview;
    }
    drawview(&pl[plnum]);
nodrawview:
    PL_drawst(&pl[plnum]);
  }
  //
  V_setrect(0, SCRW, 0, SCRH);
  if (g_plrcount > 1 && n_host == NULL) V_clr(0, SCRW-120, SCRH/g_plrcount, 1, 4);
  //
  if (g_st == GS_WAITING_CONNECTION || (n_host != NULL && gnet_state == GNETS_HANDSHAKING)) {
    V_fadescr(120);
    center_msg("WAITING");
    drawInn((SCRW-32)/2, (SCRH-32)/2+20, (SDL_GetTicks()/83)&0x07); // hotspot:(13,13)
    return;
  }
  //
  if (g_trans) return;
  //
  V_center(1);
  if (GM_draw()) {
    V_center(0);
    for (int f = 0; f < g_plrcount; ++f) pl[0].drawst = pl[1].drawst = 0xFF;
    V_setrect(0, SCRW, 0, SCRH);
    return;
  }
  V_center(0);
  //
  if (n_host == NULL) {
    for (int f = 0; f < g_plrcount; ++f) {
      int hgt = SCRH/g_plrcount, y = hgt*f;
      //
      h = plr_check_health(&pl[f]);
      if (h) V_maptoscr(0, SCRW-120, y, hgt, clrmap+h*256);
      //
      //if (pl[f].drawst) V_copytoscr(SCRW-120, 120, y, hgt);
      pl[f].drawst = 0xFF;
    }
  } else {
    h = plr_check_health(&pl[n_local_player_num]);
    if (h) V_maptoscr(0, SCRW-120, 0, SCRH, clrmap+h*256);
    pl[n_local_player_num].drawst = 0xFF;
  }
  //
  V_setrect(0, SCRW, 0, SCRH);
  if (g_edit_mode) ED_draw();
}


void G_respawn_player (player_t *p) {
  int i;
  //
  if (dm_pnum == 2) {
    if (p == &pl[0]) i = (dm_pl1p ^= 1); else i = (dm_pl2p ^= 1);
    p->o.x = dm_pos[i].x;
    p->o.y = dm_pos[i].y;
    p->d = dm_pos[i].d;
    FX_tfog(dm_pos[i].x, dm_pos[i].y);
    Z_sound(telepsnd, 128);
    return;
  }
  do { i = myrand(dm_pnum); } while (i == dm_pl1p || i == dm_pl2p);
  p->o.x = dm_pos[i].x;
  p->o.y = dm_pos[i].y;
  p->d = dm_pos[i].d;
  if (p == &pl[0]) dm_pl1p = i; else dm_pl2p = i;
  FX_tfog(dm_pos[i].x, dm_pos[i].y);
  Z_sound(telepsnd, 128);
}


////////////////////////////////////////////////////////////////////////////////
static BJRandCtx server_start_prng;

CONCMD(gcmd_network_start_server, "start network game as server\ngcmd_network_start_server [level] [coop|dm]") {
  CMDCON_HELP();
  int is_dm = 0;
  int lvl = (cli_warp ? cli_warp : 1);
  //
  if (argc < 1 || argc > 3) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  for (int f = 1; f < argc; ++f) {
    const char *s = argv[f];
    //
    if (strcasecmp(s, "dm") == 0) {
      is_dm = 1;
    } else if (strcasecmp(s, "coop") == 0) {
      is_dm = 0;
    } else if ((lvl = cmdcon_parseint(s, -1, 1, 99, NULL)) < 0) {
      conlogf("%s: invalid arg: '%s'!\n", argv[0], s);
      return;
    }
  }
  //
  GM_set(NULL);
  //
  if (N_create_host() == 0) {
    set_trans(GS_WAITING_CONNECTION);
    gnet_state = GNETS_HANDSHAKING;
    n_waiting_start_time = SDL_GetTicks();
    //
    demo_done();
    G_reset_cheats();
    g_plrcount = 2;
    g_dm = is_dm;
    g_map = lvl;
    cli_warp = 0; //k8
    server_start_prng = g_prng;
    M_init_players();
    G_start();
  } else {
    set_trans(GS_TITLE);
  }
}


CONCMD(gcmd_network_start_client, "start network game as client") {
  CMDCON_HELP();
  //
  if (argc != 1) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  GM_set(NULL);
  //
  if (N_create_client() == 0) {
    set_trans(GS_WAITING_CONNECTION);
    gnet_state = GNETS_HANDSHAKING;
    n_waiting_start_time = SDL_GetTicks();
    G_reset_cheats();
  } else {
    set_trans(GS_TITLE);
  }
}


////////////////////////////////////////////////////////////////////////////////
void G_reset_cheats (void) {
  PL_JUMP = 10;
  PL_RUN = 8;
  p_immortal = CC_FALSE;
  p_ghost = CC_FALSE;
  p_fly = CC_FALSE;
  m_notarget = CC_FALSE;
  cheatNoBack = CC_FALSE;
  cheatNoFront = CC_FALSE;
}


////////////////////////////////////////////////////////////////////////////////
//TODO: send console commands to setup game options
enum {
  NH_HANDSHAKE_SRV,
  NH_HANDSHAKE_CLI,
  NH_INTER_FIRED,
  NH_INTER_ACK,
  //
  NH_MAX
};


typedef struct GCC_PACKED {
  uint8_t type;
  uint8_t version; // protocol version
  uint8_t map; // map number
  uint8_t dm; // deathmatch?
  BJRandCtx prng;
} NetHandshakeFromServer;


typedef struct GCC_PACKED {
  uint8_t type;
  uint8_t version; // protocol version
} NetHandshakeFromClient;


typedef struct GCC_PACKED {
  uint8_t type;
} NetInterFired;


typedef struct GCC_PACKED {
  uint8_t type;
} NetInterAck;


int G_process_enet_connect (void) {
  ENetPacket *pkt;
  //
  conlogf("network peer connected\n");
  //
  if (n_network_meserver) {
    NetHandshakeFromServer hs;
    //
    hs.type = NH_HANDSHAKE_SRV;
    hs.version = NETGAME_PROTO_VERSION;
    hs.map = g_map;
    hs.dm = g_dm;
    hs.prng = server_start_prng;
    //
    pkt = enet_packet_create(&hs, sizeof(hs), ENET_PACKET_FLAG_RELIABLE);
  } else {
    NetHandshakeFromClient hs;
    //
    hs.type = NH_HANDSHAKE_CLI;
    hs.version = NETGAME_PROTO_VERSION;
    //
    pkt = enet_packet_create(&hs, sizeof(hs), ENET_PACKET_FLAG_RELIABLE);
  }
  enet_peer_send(n_peer, ENET_HANDSHAKE_CHANNEL, pkt);
  //
  return 0;
}


int G_process_enet_disconnect (void) {
  conlogf("network peer disconnected\n");
  N_disconnect();
  //
  GM_set(NULL);
  set_trans(GS_TITLE);
  //
  return 0;
}


static __attribute__((unused)) void send_ack (void) {
  NetInterFired fr;
  ENetPacket *pkt;
  //
  fr.type = NH_INTER_FIRED;
  //
  pkt = enet_packet_create(&fr, sizeof(fr), ENET_PACKET_FLAG_RELIABLE);
  enet_peer_send(n_peer, ENET_HANDSHAKE_CHANNEL, pkt);
  fprintf(stderr, "FIRE sent\n");
}


int G_process_enet_game_packet (ENetPacket *pkt) {
  switch (gnet_state) {
    case GNETS_HANDSHAKING:
      // handshake packet arrives
      if (n_network_meserver) {
        if (pkt->dataLength == sizeof(NetHandshakeFromClient) && pkt->data[0] == NH_HANDSHAKE_CLI) {
          const NetHandshakeFromClient *hs = (const NetHandshakeFromClient *)pkt->data;
          //
          if (hs->version == NETGAME_PROTO_VERSION) {
            conlogf("net: client handshake ok, starting game...");
            gnet_state = GNETS_PLAYING;
            return 1;
          } else {
            conlogf("net: client handshake: invalid protocol version (%u)", hs->version);
          }
        } else {
          conlogf("net: client handshake: invalid packed");
        }
      } else {
        if (pkt->dataLength == sizeof(NetHandshakeFromServer) && pkt->data[0] == NH_HANDSHAKE_SRV) {
          const NetHandshakeFromServer *hs = (const NetHandshakeFromServer *)pkt->data;
          //
          if (hs->version == NETGAME_PROTO_VERSION) {
            conlogf("net: server handshake: dm=%u; map=%u\n", hs->dm, hs->map);
            g_plrcount = 2;
            g_dm = (hs->dm ? 1 : 0);
            g_map = hs->map;
            cli_warp = 0; //k8
            g_prng = hs->prng;
            M_init_players();
            G_start();
            gnet_state = GNETS_PLAYING;
            return 1;
          }
        } else {
          conlogf("net: server handshake: invalid packed");
        }
      }
      break;
    case GNETS_PLAYING:
      // there should be no packets
      break;
  }
  //
  return 0;
}


void G_send_inter_fired (void) {
  NetInterFired fr;
  ENetPacket *pkt;
  //
  fr.type = NH_INTER_FIRED;
  //
  pkt = enet_packet_create(&fr, sizeof(fr), ENET_PACKET_FLAG_RELIABLE);
  enet_peer_send(n_peer, ENET_HANDSHAKE_CHANNEL, pkt);
  fprintf(stderr, "FIRE sent\n");
}


////////////////////////////////////////////////////////////////////////////////
static int G_mapload (MemBuffer *mbuf, const map_block_t *blk) {
  if (mbuf == NULL) return 0; // no post-processing
  //
  if (membuf_read_full(mbuf, g_music, 8) < 0) return -1;
  if (music_random) F_randmus(g_music);
  F_loadmus(g_music);
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int G_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui8(mbuf, g_plrcount);
    membuf_write_bool(mbuf, g_dm);
    membuf_write_ui8(mbuf, g_exit);
    membuf_write_ui8(mbuf, g_map);
    membuf_write_ui32(mbuf, g_time);
    membuf_write_ui32(mbuf, dm_pl1p);
    membuf_write_ui32(mbuf, dm_pl2p);
    membuf_write_ui32(mbuf, dm_pnum);
    membuf_write(mbuf, dm_pos, dm_pnum*sizeof(pos_t));
    membuf_write_bool(mbuf, g_cheats_enabled);
    membuf_write_i32(mbuf, PL_JUMP);
    membuf_write_i32(mbuf, PL_RUN);
    membuf_write_bool(mbuf, p_immortal);
    membuf_write_bool(mbuf, p_fly);
    membuf_write_bool(mbuf, p_ghost);
    membuf_write_bool(mbuf, m_notarget);
    membuf_write(mbuf, g_music, 8);
  }
  //
  return 0;
}


static int G_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'game' version!\n");
      return -1;
    }
    g_plrcount = membuf_read_ui8(mbuf);
    if (g_plrcount < 1 || g_plrcount > 2) {
      conlogf("LOADGAME ERROR: invalid number of players!\n");
      return -1;
    }
    g_dm = membuf_read_bool(mbuf);
    g_exit = membuf_read_ui8(mbuf);
    if (g_exit >= D2D_LEVEL_EXIT_MAXVALUE) {
      conlogf("LOADGAME ERROR: invalid exit type!\n");
      return -1;
    }
    g_map = membuf_read_ui8(mbuf);
    g_time = membuf_read_ui32(mbuf);
    dm_pl1p = membuf_read_ui32(mbuf);
    dm_pl2p = membuf_read_ui32(mbuf);
    dm_pnum = membuf_read_ui32(mbuf);
    if (dm_pnum > MAX_DMPOS) {
      conlogf("LOADGAME ERROR: too many deathmatch spawn positions!\n");
      return -1;
    }
    membuf_read_full(mbuf, dm_pos, dm_pnum*sizeof(pos_t));
    g_cheats_enabled = membuf_read_bool(mbuf);
    PL_JUMP = membuf_read_i32(mbuf);
    PL_RUN = membuf_read_i32(mbuf);
    p_immortal = membuf_read_bool(mbuf);
    p_fly = membuf_read_bool(mbuf);
    p_ghost = membuf_read_bool(mbuf);
    m_notarget = membuf_read_bool(mbuf);
    membuf_read_full(mbuf, g_music, 8);
    F_loadmus(g_music);
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(scrnh)/sizeof(scrnh[0]); ++f) V_initvga(&scrnh[f]);
  //
  F_registerMapLoad(MB_MUSIC, G_mapload, NULL);
  F_registerSaveLoad("GAMEINFO", G_savegame, G_loadgame, SAV_NORMAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(scrnh)/sizeof(scrnh[0]); ++f) V_freevga(&scrnh[f]);
}
