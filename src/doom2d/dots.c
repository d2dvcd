/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "dots.h"

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "game.h"
#include "saveload.h"
#include "view.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_DOTS  (2048)

#define MAXINI  (50)
#define MAXSR   (20)

#define BL_XV    (4)
#define BL_YV    (4)
#define BL_MINT  (10)
#define BL_MAXT  (14)

#define SP_V     (2)
#define SP_MINT  (5)
#define SP_MAXT  (7)


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  obj_t o;
  uint8_t c, t;
} dot_t;


typedef struct GCC_PACKED {
  int xv, yv;
  uint8_t c, t;
} init_t;


////////////////////////////////////////////////////////////////////////////////
static dot_t dot[MAX_DOTS];
static init_t bl_ini[MAXINI], sp_ini[MAXINI];
static int bl_r, sp_r, sr_r, sxr[MAXSR], syr[MAXSR];
static int maxdot;


////////////////////////////////////////////////////////////////////////////////
void DOT_init (void) {
  for (int f = 0; f < MAX_DOTS; ++f) {
    dot[f].t = 0;
    dot[f].o.type = OBJT_DOT;
    dot[f].o.r = 0;
    dot[f].o.h = 1;
  }
  maxdot = 0;
}


static inline int incrldot (void) {
  if (maxdot == MAX_DOTS) {
    // remove latest dot
    for (int f = 1; f < MAX_DOTS; ++f) dot[f-1] = dot[f];
    return maxdot-1;
  } else {
    return maxdot++;
  }
}


static void DOT_compact (void) {
  int src, dest = 0;
  //
  while (dest < MAX_DOTS && dot[dest].t) ++dest; // find place for this dot
  if (dest >= MAX_DOTS) {
    // the thing that should not be
    maxdot = MAX_DOTS;
    return;
  }
  //
  src = dest;
  while (src < maxdot) {
    if (!dot[src].t) { ++src; continue; } // this dot is not used, go on
    // copy used dot
    dot[dest++] = dot[src++];
  }
  maxdot = dest;
}


void DOT_alloc (void) {
  for (int f = 0; f < MAXINI; ++f) {
    bl_ini[f].xv = myrand1(BL_XV*2+1)-BL_XV;
    bl_ini[f].yv = -myrand1(BL_YV);
    bl_ini[f].c = 0xB0+myrand1(16);
    bl_ini[f].t = myrand1(BL_MAXT-BL_MINT+1)+BL_MINT;
    sp_ini[f].xv = myrand1(SP_V*2+1)-SP_V;
    sp_ini[f].yv = myrand1(SP_V*2+1)-SP_V;
    sp_ini[f].c = 0xA0+myrand1(6);
    sp_ini[f].t = myrand1(SP_MAXT-SP_MINT+1)+SP_MINT;
  }
  //
  for (int f = 0; f < MAXSR; ++f) {
    sxr[f] = myrand1(2*2+1)-2;
    syr[f] = myrand1(2*2+1)-2;
  }
  bl_r = sp_r = sr_r = 0;
}


void DOT_act (void) {
  int s, xv, yv;
  int docompact = 0;
  //
  for (int f = 0; f < maxdot; ++f) {
    if (dot[f].t) {
      xv = dot[f].o.xv+dot[f].o.vx;
      yv = dot[f].o.yv+dot[f].o.vy;
      s = Z_moveobj(&dot[f].o);
      if (dot[f].t < 254) { if ((--dot[f].t) == 0) docompact = 1; }
      if (s&(Z_HITWATER|Z_FALLOUT)) { dot[f].t = 0; docompact = 1; continue; }
      if (s&Z_HITLAND) {
        if (!dot[f].o.xv) {
          if (yv > 2) {
            dot[f].o.vx = (xv ? Z_sign(dot[f].o.vx) : (myrandbit1() ? -1 : 1));
            if (myrand1(yv) == 0) dot[f].o.vx *= 2;
            dot[f].o.yv = yv-2;
          }
        }
        dot[f].o.xv = 0;
        if (dot[f].t > 4 && dot[f].t != 255) dot[f].t = 4;
      }
      if (s&Z_HITWALL) {
        dot[f].o.vx = Z_sign(xv)*2;
        dot[f].o.yv = Z_sign(dot[f].o.yv);
        if (dot[f].o.yv >= 0 && myrandtwobits1()) --dot[f].o.yv;
        if (dot[f].o.yv >= 0 && myrandbit1()) --dot[f].o.yv;
      }
      if (s&Z_HITCEIL) {
        dot[f].o.xv = 0;
        dot[f].o.yv = (myrand1(100) ? -2 : 0);
      }
    }
  }
  //
  if (docompact) DOT_compact();
}


void DOT_draw (void) {
  for (int f = 0; f < maxdot; ++f) {
    if (dot[f].t) V_putpixel_lm(Z_MAP2SCR_X(dot[f].o.x), Z_MAP2SCR_Y(dot[f].o.y), dot[f].c, LMAP_NORMAL);
  }
}


void DOT_add (int x, int y, char xv, char yv, uint8_t c, uint8_t t) {
  if (Z_canfit(OBJT_DOT, x, y, 0, 1)) {
    int f = incrldot();
    //
    dot[f].o.x = x;
    dot[f].o.y = y;
    dot[f].o.xv = xv;
    dot[f].o.yv = yv;
    dot[f].c = c;
    dot[f].t = t;
    dot[f].o.vx = dot[f].o.vy = 0;
  }
}


void DOT_blood (int x, int y, int xv, int yv, int n) {
  for (int k = n; k; --k) {
    if (Z_canfit(OBJT_DOT, x, y, 0, 1)) {
      int dx = x+sxr[sr_r], dy = y+syr[sr_r], f = incrldot();
      //
      dot[f].o.x = dx;
      dot[f].o.y = dy;
      dot[f].o.xv = bl_ini[bl_r].xv+Z_dec(xv, 3);
      dot[f].o.yv = bl_ini[bl_r].yv+Z_dec(yv, 3)-3;
      dot[f].c = bl_ini[bl_r].c;
      dot[f].t = 255;
      dot[f].o.vx = dot[f].o.vy = 0;
      if (++bl_r >= MAXINI) bl_r = 0;
      if (++sr_r >= MAXSR) sr_r = 0;
    }
  }
}


void DOT_spark (int x, int y, int xv, int yv, int n) {
  for (int k = n; k; --k) {
    if (Z_canfit(OBJT_DOT, x, y, 0, 1)) {
      int dx = x+sxr[sr_r], dy = y+syr[sr_r], f = incrldot();
      //
      dot[f].o.x = dx;
      dot[f].o.y = dy;
      dot[f].o.xv = sp_ini[sp_r].xv-xv/4;
      dot[f].o.yv = sp_ini[sp_r].yv-yv/4;
      dot[f].c = sp_ini[sp_r].c;
      dot[f].t = sp_ini[sp_r].t;
      dot[f].o.vx = dot[f].o.vy = 0;
      if (++sp_r >= MAXINI) sp_r = 0;
      if (++sr_r >= MAXSR) sr_r = 0;
    }
  }
}


void DOT_water (int x, int y, int xv, int yv, int n, int c) {
  static const uint8_t ct[3] = {0xC0, 0x70, 0xB0};
  //
  if (c >= 0 && c < 3) {
    c = ct[c];
    for (int k = n; k; --k) {
      if (Z_canfit(OBJT_DOT, x, y, 0, 1)) {
        int dx = x+sxr[sr_r], dy = y+syr[sr_r], f = incrldot();
        //
        dot[f].o.x = dx;
        dot[f].o.y = dy;
        dot[f].o.xv = bl_ini[bl_r].xv-Z_dec(xv, 3);
        dot[f].o.yv = bl_ini[bl_r].yv-abs(yv);
        dot[f].c = bl_ini[bl_r].c-0xB0+c;
        dot[f].t = 254;
        dot[f].o.vx = dot[f].o.vy = 0;
        if (++bl_r >= MAXINI) bl_r = 0;
        if (++sr_r >= MAXSR) sr_r = 0;
      }
    }
  }
}


CONCMD(removedots, "remove all 'dots'") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  DOT_init();
}


static int DOT_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(dot_t)); // structure size
    membuf_write_ui32(mbuf, maxdot);
    for (int f = 0; f < maxdot; ++f) membuf_write(mbuf, &dot[f], sizeof(dot_t));
  }
  return 0;
}


static int DOT_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    DOT_init();
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'dots' version!\n");
      goto error;
    }
    if (membuf_read_ui16(mbuf) != sizeof(dot_t)) {
      conlogf("LOADGAME ERROR: invalid 'dots' size!\n");
      goto error;
    }
    cnt = membuf_read_ui32(mbuf);
    if (cnt > MAX_DOTS) {
      conlogf("LOADGAME ERROR: too many dots!\n");
      cnt = MAX_DOTS;
    }
    maxdot = cnt;
    if (membuf_read_full(mbuf, dot, cnt*sizeof(dot_t)) != cnt*sizeof(dot_t)) {
      conlogf("LOADGAME ERROR: can't read dots!\n");
      goto error;
    }
  }
  //
  if (waserror) DOT_init();
  return 0; // we are optional anyway
error:
  // we are optional anyway
  DOT_init();
  return 0;
}


GCC_CONSTRUCTOR_SYSTEM {
  F_registerSaveLoad("DOTSFX", DOT_savegame, DOT_loadgame, SAV_OPTIONAL);
}
