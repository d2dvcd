/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "monster.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "gamemode.h"
#include "items.h"
#include "misc.h"
#include "player.h"
#include "sdldrv.h"
#include "smoke.h"
#include "switch.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_MONSTERS  (200)
#define MANCOLOR    (0xD0)
#define MAX_ATM     (90)
#define GGAS_TOTAL  (MN__LAST-MN_DEMON+16+10)


////////////////////////////////////////////////////////////////////////////////
enum {
  MNST_SLEEP,
  MNST_GO,
  MNST_RUN,
  MNST_CLIMB,
  MNST_DIE,
  MNST_DEAD,
  MNST_ATTACK,
  MNST_SHOOT,
  MNST_PAIN,
  MNST_WAIT,
  MNST_REVIVE,
  MNST_RUNOUT
};


typedef struct GCC_PACKED {
  obj_t o;
  uint8_t t, d, st, ftime;
  int fobj;
  int s;
  const char *ap;
  int aim, life, pain, ac, tx, ty, ammo;
  short atm;
} mn_t;


////////////////////////////////////////////////////////////////////////////////
static const char *sleepanim[MN_TYPES_MAX] = {
  "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB",
  "A", "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB", "AAABBB",
  "A", "A", "AAABBB"
}, *goanim[MN_TYPES_MAX] = {
  "AABBCCDD", "AABBCCDD", "AABBDDAACCDD", "AABBDDAACCDD", "AABBDDCCDDBB",
  "AABBDDAACCDD", "AABBCCDD", "AABBCCDD", "A", "AABB", "AABBCCBB",
  "AABBCCDDEEFF", "AABBCCDDEEFF", "AABBCCDDEEFF", "AABBCCDDEEFF", "AABBCCDDEEFF",
  "AABB", "A", "DDEEFFGGHHIIJJKKLLAABBCC", "ACDABD"
}, *painanim[MN_TYPES_MAX] = {
  "H", "H", "G", "G", "G", "G", "H", "H", "F", "E", "G", "I", "I", "J", "L", "Q", "EECCDDCC",
  "A", "D", "G"
}, *waitanim[MN_TYPES_MAX] = {
  "A", "A", "A", "A", "A", "A", "A", "A", "A", "AABB", "A", "A", "A", "I", "K", "A", "A",
  "A", "D", "E"
}, *attackanim[MN_TYPES_MAX] = {
  "EEFFGG", "EEFFGG", "EEEEEF", "EEEEEF", "EEEEEF", "EF", "EEFFGG", "EEFFGG",
  "BBCCDD", "CCDD", "DDEEFF", "GH", "GH", "GGGGHH", "GGHHII",
  "QQGGGHHHIIJJKKLLMMNNOOPP", "BBFFAA", "A", "OOPPQQ", "EEEEFF"
}, *dieanim[MN_TYPES_MAX] = {
  "IIIJJJKKKLLLMMM", "IIIJJJKKKLLL", "HHHIIIJJJKKK", "HHHIIIJJJKKK",
  "HHHIIIJJJKKKLLLMMMNNNOOO", "HHHIIIJJJKKKLLLMMM",
  "IIIJJJKKKLLLMMMNNN", "IIIJJJKKKLLLMMMNNN", "GGGHHHIIIJJJKKK",
  "FFFGGGHHHIIIJJJKKK", "HHHIIIJJJKKKLLLMMM",
  "JJJJKKKKLLLLMMMMNNNNOOOOPPPPQQQQRRRR", "JJJKKKLLLMMMNNNOOO",
  "KKKLLLMMMNNNOOOPPPRRRSSS", "MMMNNNOOOPPP", "RRRSSSTTTUUUVVVWWWXXXYYY",
  "DDDD", "CCCDDDEEEFFFGGG", "D", "HHHHIIIIJJJJKKKKLLLLMMMM"
}, *slopanim[MN_TYPES_MAX] = {
  "", "NNNOOOPPPRRRSSSTTT", "MMMNNNOOOPPPRRRSSSTTT", "MMMNNNOOOPPPRRRSSSTTT", "",
  "OOOPPPQQQRRRSSS", "", "", "", "", "", "", "", "", "", "", "", "", "", "OOPPQQRRSSTTUUVV"
}, *deadanim[MN_TYPES_MAX] = {
  "N", "M", "L", "L", "P", "N", "O", "O", "L", "", "", "S", "P", "T", "Q", "Z", "C", "", "D", "N"
}, *messanim[MN_TYPES_MAX] = {
  "", "U", "U", "U", "", "T", "", "", "", "", "", "", "", "", "", "", "", "", "", "W"
};

int hit_xv, hit_yv;

static vgaimg spr[MN_TYPES_MAX][29*2], fspr[8];
static const snd_t *fsnd, *pauksnd, *trupsnd;
static vgaimg sgun[2], pl_spr[2];
static char sprd[MN_TYPES_MAX][29*2];
static const snd_t *snd[MN_TYPES_MAX][5];
static const snd_t *impsitsnd[2];
static const snd_t *impdthsnd[2];
static const snd_t *firsnd;
static const snd_t *slopsnd;
static const snd_t *gsnd[4];
static const snd_t *swgsnd, *pchsnd, *telesnd;
static const snd_t *positsnd[3], *podthsnd[3];
static mn_t mn[MAX_MONSTERS];
static int mnum, gsndt;


////////////////////////////////////////////////////////////////////////////////
const monster_stats_t mnsz[MN_TYPES_MAX+1] = {
  //rad   ht     life     pain    rv     jv      slop    min_pn
  {.r= 0, .h= 0, .l=  0, .mp= 0,  .rv=0, .jv= 0, .sp= 0, .minp= 0}, // none
  {.r=15, .h=28, .l= 60, .mp=20,  .rv=7, .jv=10, .sp= 0, .minp=10}, // demon
  {.r=10, .h=28, .l= 25, .mp=15,  .rv=3, .jv=10, .sp=30, .minp= 0}, // imp
  {.r=10, .h=28, .l= 15, .mp=10,  .rv=3, .jv=10, .sp=30, .minp= 0}, // zombie
  {.r=10, .h=28, .l= 20, .mp=10,  .rv=3, .jv=10, .sp=30, .minp= 0}, // sergeant
  {.r=20, .h=55, .l=500, .mp=70,  .rv=5, .jv=10, .sp= 0, .minp=50}, // cyberdemon
  {.r=12, .h=28, .l= 60, .mp=20,  .rv=3, .jv=10, .sp=30, .minp=10}, // chaingunner
  {.r=12, .h=32, .l=150, .mp=40,  .rv=3, .jv=10, .sp= 0, .minp=30}, // baron of hell
  {.r=12, .h=32, .l= 75, .mp=40,  .rv=3, .jv=10, .sp= 0, .minp=30}, // hell knight
  {.r=15, .h=28, .l=100, .mp=10,  .rv=4, .jv= 4, .sp= 0, .minp= 0}, // cacodemon
  {.r= 8, .h=18, .l= 60, .mp=10,  .rv=4, .jv= 4, .sp= 0, .minp= 0}, // lost soul
  {.r=15, .h=28, .l=100, .mp=10,  .rv=4, .jv= 4, .sp= 0, .minp= 0}, // pain elemental
  {.r=64, .h=50, .l=500, .mp=70,  .rv=4, .jv=10, .sp= 0, .minp=50}, // spider mastermind
  {.r=25, .h=27, .l=150, .mp=20,  .rv=4, .jv=10, .sp= 0, .minp= 0}, // arachnotron
  {.r=18, .h=30, .l=200, .mp=40,  .rv=3, .jv= 7, .sp= 0, .minp=20}, // mancubus
  {.r=17, .h=36, .l=200, .mp=40,  .rv=6, .jv=11, .sp= 0, .minp=20}, // revenant
  {.r=17, .h=36, .l=150, .mp=30,  .rv=7, .jv=12, .sp= 0, .minp=10}, // archvile
  {.r= 5, .h= 5, .l= 35, .mp=20, .rv=14, .jv= 6, .sp= 0, .minp=10}, // fish
  {.r= 5, .h=17, .l= 20, .mp= 0,  .rv=7, .jv= 6, .sp= 0, .minp= 0}, // barrel
  {.r=17, .h=38, .l= 20, .mp=40,  .rv=3, .jv= 6, .sp= 0, .minp=20}, // robot
  {.r= 8, .h=26, .l=400, .mp=70,  .rv=8, .jv=10, .sp=30, .minp=50}, // man
};


////////////////////////////////////////////////////////////////////////////////
void MN_alloc (void) {
  static const char sn[MN_TYPES_MAX][5][6] = {
    {"DMACT", "DMPAIN", "SGTATK", "SGTSIT", "SGTDTH"},
    {"BGACT", "POPAIN", "CLAW", "", ""},
    {"POSACT", "POPAIN", "", "", ""},
    {"POSACT", "POPAIN", "", "", ""},
    {"", "DMPAIN", "HOOF", "CYBSIT", "CYBDTH"},
    {"POSACT", "POPAIN", "", "", ""},
    {"", "DMPAIN", "", "BRSSIT", "BRSDTH"},
    {"", "DMPAIN", "", "KNTSIT", "KNTDTH"},
    {"DMACT", "DMPAIN", "", "CACSIT", "CACDTH"},
    {"DMACT", "DMPAIN", "SKLATK", "SKLATK", "FIRXPL"},
    {"DMACT", "PEPAIN", "", "PESIT", "PEDTH"},
    {"", "DMPAIN", "METAL", "SPISIT", "SPIDTH"},
    {"BSPACT", "DMPAIN", "BSPWLK", "BSPSIT", "BSPDTH"},
    {"DMACT", "MNPAIN", "MANATK", "MANSIT", "MANDTH"},
    {"SKEACT", "POPAIN", "SKEATK", "SKESIT", "SKEDTH"},
    {"VILACT", "VIPAIN", "VILATK", "VILSIT", "VILDTH"},
    {"", "", "BITE1", "", ""},
    {"", "", "", "", "BAREXP"},
    {"BSPACT", "", "BSPWLK", "BSPSIT", "BSPDTH"},
    {"HAHA1", "PLPAIN", "", "STOP1", "PDIEHI"}
  }, msn[MN_TYPES_MAX][4] = {
    "SARG", "TROO", "POSS", "SPOS", "CYBR", "CPOS", "BOSS", "BOS2", "HEAD", "SKUL",
    "PAIN", "SPID", "BSPI", "FATT", "SKEL", "VILE", "FISH", "BAR1", "ROBO", "PLAY"
  };
  static const int mms[MN_TYPES_MAX] = {
    14*2, 21*2, 21*2, 21*2, 16*2, 20*2, 15*2, 15*2, 12*2, 11*2, 13*2, 19*2, 16*2,
    20*2, 17*2, 29*2, 6*2, 2*2, 17*2, 23*2
  };
  static char gsn[6];
  //
  conlogf("MN_alloc: loading monster graphics");
  strcpy(gsn, "GOOD0");
  Z_getspr(&sgun[0], "PWP4", 0, 1, NULL);
  Z_getspr(&sgun[1], "PWP4", 1, 1, NULL);
  for (int j = 0; j < MN_TYPES_MAX; ++j) {
    for (int i = 0; i < mms[j]; ++i) Z_getspr(&spr[j][i], msn[j], i/2, (i&1)+1, &sprd[j][i]);
    if (j == MN_BARREL-1) {
      for(int i = 4; i < 14; ++i) Z_getspr(&spr[j][i], "BEXP", i/2-2, (i&1)+1, &sprd[j][i]);
    }
    for (int i = 0; i < 5; ++i) snd[j][i] = (sn[j][i][0] ? Z_getsnd(sn[j][i]) : NULL);
    conlogdots(j+5, GGAS_TOTAL);
  }
  for (int f = 0; f < 8; ++f) Z_getspr(&fspr[f], "FIRE", f, 0, NULL);
  Z_getspr(&pl_spr[0], "PLAY", 'N'-'A', 0, NULL);
  Z_getspr(&pl_spr[1], "PLAY", 'W'-'A', 0, NULL);
  impsitsnd[0] = Z_getsnd("BGSIT1");
  impsitsnd[1] = Z_getsnd("BGSIT2");
  impdthsnd[0] = Z_getsnd("BGDTH1");
  impdthsnd[1] = Z_getsnd("BGDTH2");
  positsnd[0] = Z_getsnd("POSIT1");
  positsnd[1] = Z_getsnd("POSIT2");
  positsnd[2] = Z_getsnd("POSIT3");
  podthsnd[0] = Z_getsnd("PODTH1");
  podthsnd[1] = Z_getsnd("PODTH2");
  podthsnd[2] = Z_getsnd("PODTH3");
  fsnd = Z_getsnd("FLAME");
  firsnd = Z_getsnd("FIRSHT");
  slopsnd = Z_getsnd("SLOP");
  swgsnd = Z_getsnd("SKESWG");
  pchsnd = Z_getsnd("SKEPCH");
  telesnd = Z_getsnd("TELEPT");
  pauksnd = Z_getsnd("PAUK1");
  trupsnd = Z_getsnd("UTRUP");
  for (int f = 0;f < 4; ++f) { gsn[4] = f+'1'; gsnd[f] = Z_getsnd(gsn); }
}


void MN_init (void) {
  memset(mn, 0, sizeof(mn));
  for (int f = 0; f < MAX_MONSTERS; ++f) { mn[f].t = 0; mn[f].st = MNST_SLEEP; }
  gsndt = 0;
  mnum = 0;
}


static void setst (int i, int st) {
  const char *a = NULL;
  int t;
  //
  switch (mn[i].st) {
    case MNST_DIE:
    case MNST_DEAD:
      if (st != MNST_DEAD && st != MNST_REVIVE) return;
  }
  mn[i].ac = 0;
  t = mn[i].t-1;
  switch (mn[i].st = st) {
    case MNST_SLEEP: a= sleepanim[t]; break;
    case MNST_PAIN: a = painanim[t]; break;
    case MNST_WAIT: a = waitanim[t]; break;
    case MNST_CLIMB: case MNST_RUN: case MNST_RUNOUT: case MNST_GO: a = goanim[t]; break;
    case MNST_SHOOT:
      if (t == MN_SKEL-1) { a = "KKKKJJ"; break; }
      if (t == MN_ROBO-1) { a = "MN"; break; }
      // fallthru
    case MNST_ATTACK:
      a = attackanim[t];
      if (st == MNST_ATTACK && t == MN_VILE-1) a = "[[\\\\]]";
      break;
    case MNST_DIE:
      if (g_map == 9 && t == MN_BSP-1) Z_sound(pauksnd, 128);
      a = dieanim[t];
      break;
    case MNST_DEAD:
      a = deadanim[t];
      if (mn[i].ap == slopanim[t]) a = messanim[t];
      if (t == MN_BARREL-1) mn[i].t = 0;
      break;
    case MNST_REVIVE:
      a = (mn[i].ap == messanim[t] ? slopanim[t] : dieanim[t]);
      mn[i].ac = strlen(a)-1;
      mn[i].o.type = OBJT_MONSTER;
      mn[i].o.r = mnsz[t+1].r;
      mn[i].o.h = mnsz[t+1].h;
      mn[i].life = mnsz[t+1].l;
      mn[i].ammo = 0;
      mn[i].pain = 0;
      ++mnum;
      break;
  }
  //
  if (a == NULL) ERR_fatal("setst(): the thing that should not be!");
  mn[i].ap = a;
}


int MN_spawn (int x, int y, uint8_t d, int t) {
  int idx = -1;
  //
  if (g_dm && g_no_monsters && t < MN_PL_DEAD) return -1;
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (!mn[f].t) { idx = f; break; }
    if (idx < 0 && mn[f].t >= MN_PL_DEAD) idx = f;
  }
  //
  if (idx >= 0) {
    mn[idx].o.type = OBJT_MONSTER;
    mn[idx].o.x = x;
    mn[idx].o.y = y;
    mn[idx].o.xv = mn[idx].o.yv = mn[idx].o.vx = mn[idx].o.vy = 0;
    mn[idx].d = d;
    mn[idx].t = t;
    mn[idx].st = MNST_SLEEP;
    if (t < MN_PL_DEAD) {
      mn[idx].o.r = mnsz[t].r;
      mn[idx].o.h = mnsz[t].h;
      mn[idx].life = mnsz[t].l;
      setst(idx, MNST_SLEEP);
      mn[idx].s = myrand(18);
      ++mnum;
    } else {
      mn[idx].o.r = 8;
      mn[idx].o.h = 6;
      mn[idx].life = 0;
      mn[idx].st = MNST_DEAD;
    }
    mn[idx].aim = -3;
    mn[idx].atm = 0;
    mn[idx].pain = 0;
    mn[idx].ammo = 0;
    mn[idx].ftime = 0;
  }
  //
  return idx;
}


CONCMD(soul, "spawn lost soul") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  //
  MN_spawn(Z_SCR2MAP_X(msLastX), Z_SCR2MAP_Y(msLastY), 0, MN_SOUL);
}


int MN_spawn_deadpl (obj_t *o, uint8_t c, int t) {
  int i;
  //
  if ((i = MN_spawn(o-> x, o-> y, c, t+MN_PL_DEAD)) == -1) return -1;
  mn[i].o = *o;
  mn[i].o.type = OBJT_MONSTER;
  return i;
}


static int isfriend (int a, int b) {
  if (a == MN_BARREL || b == MN_BARREL) return 1;
  if (a == b) {
    switch (a) {
      case MN_IMP: case MN_DEMON:
      case MN_BARON: case MN_KNIGHT:
      case MN_CACO: case MN_SOUL:
      case MN_MANCUB: case MN_SKEL:
      case MN_FISH:
        return 1;
    }
  }
  if (a == MN_SOUL && b == MN_PAIN) return 1;
  if (b == MN_SOUL && a == MN_PAIN) return 1;
  return 0;
}


ccBool m_notarget = CC_FALSE;

static int MN_findnewprey (int i) {
  int a, b, l;
  //
  a = !PL_isdead(&pl[0]);
  b = (g_plrcount == 2 ? !PL_isdead(&pl[1]) : 0);
  //
  if (m_notarget) a = b = 0;
  //
  if (a) {
    if (b) mn[i].aim = (abs(mn[i].o.x-pl[0].o.x)+abs(mn[i].o.y-pl[0].o.y) <= abs(mn[i].o.x-pl[1].o.x)+abs(mn[i].o.y-pl[1].o.y) ? -1 : -2);
    else mn[i].aim = -1;
  } else {
    if (b) {
      mn[i].aim = -2;
    } else {
      for (a = 0, b = 32000, mn[i].aim = -3; a < MAX_MONSTERS; ++a) {
        if (mn[a].t && mn[a].st != MNST_DEAD && a != i && !isfriend(mn[a].t, mn[i].t)) {
          if ((l = abs(mn[i].o.x-mn[a].o.x)+abs(mn[i].o.y-mn[a].o.y)) < b) {
            mn[i].aim = a;
            b = l;
          }
        }
      }
      if (mn[i].aim < 0) {
        mn[i].atm = MAX_ATM;
        return 0;
      } else {
        mn[i].atm = 0;
      }
    }
  }
  //
  //fprintf(stderr, "m%d: aim=%d; atm=%d\n", i, mn[i].aim, mn[i].atm);
  return 1;
}


int Z_getobjpos (int i, obj_t *o) {
  if (i == -1) {
    *o = pl[0].o;
    return !PL_isdead(&pl[0]);
  }
  if (g_plrcount == 2 && i == -2) {
    *o = pl[1].o;
    return !PL_isdead(&pl[1]);
  }
  if (i >= 0 && i < MAX_MONSTERS && mn[i].t && mn[i].st != MNST_DEAD) {
    *o = mn[i].o;
    return 1;
  }
  return 0;
}


static const void *wakeupsnd (int t) {
  switch (t) {
    case MN_IMP:
      return impsitsnd[myrand(2)];
    case MN_ZOMBY:
    case MN_SERG:
    case MN_CGUN:
      return positsnd[myrand(3)];
  }
  return snd[t-1][3];
}


static const void *dthsnd (int t) {
  switch (t) {
    case MN_IMP:
      return impdthsnd[myrand(2)];
    case MN_ZOMBY:
    case MN_SERG:
    case MN_CGUN:
      return podthsnd[myrand(3)];
  }
  return snd[t-1][4];
}


static int canshoot (int t) {
  switch (t) {
    case MN_DEMON:
    case MN_FISH:
    case MN_BARREL:
      return 0;
  }
  return 1;
}


static int shoot (int i, obj_t *o, int n) {
  int xd, yd, m;
  //
  if (mn[i].ammo < 0) return 0;
  if (!n) {
    switch (mn[i].t) {
      case MN_FISH:
      case MN_BARREL:
      case MN_DEMON:
        return 0;
      case MN_CGUN:
      case MN_BSP:
      case MN_ROBO:
        if (++mn[i].ammo >= 50) mn[i].ammo = (mn[i].t == MN_ROBO ? -200 : -50);
        break;
      case MN_MAN:
        break;
      case MN_MANCUB:
        if (++mn[i].ammo >= 5) mn[i].ammo = -50;
        break;
      case MN_SPIDER:
        if (++mn[i].ammo >= 100) mn[i].ammo = -50;
        break;
      case MN_CYBER:
        if (myrandbit()) return 0;
        if (++mn[i].ammo >= 10) mn[i].ammo = -50;
        break;
      case MN_BARON:
      case MN_KNIGHT:
        if (myrandmask(7)) return 0;
        break;
      case MN_SKEL:
        if (myrandmask(31)) return 0;
        break;
      case MN_VILE:
        if (myrandmask(7)) return 0;
        break;
      case MN_PAIN:
        if (myrandmask(7)) return 0;
        break;
      default:
        if (myrandmask(15)) return 0;
        break;
    }
  }
  if (!Z_look(&mn[i].o, o, mn[i].d)) return 0;
  mn[i].atm = 0;
  mn[i].tx = o->x+(o->xv+o->vx)*6;
  mn[i].ty = o->y-o->h/2+(o->yv+o->vy)*6;
  if (abs(mn[i].tx-mn[i].o.x) < abs(mn[i].ty-mn[i].o.y+mn[i].o.h/2)) return 0;
  switch (mn[i].t) {
    case MN_IMP:
    case MN_BARON:
    case MN_KNIGHT:
    case MN_CACO:
      setst(i, MNST_SHOOT);
      Z_sound(firsnd, 128);
      break;
    case MN_SKEL:
      setst(i, MNST_SHOOT);
      Z_sound(snd[MN_SKEL-1][2], 128);
      break;
    case MN_VILE:
      mn[i].tx = o->x;
      mn[i].ty = o->y;
      setst(i, MNST_SHOOT);
      Z_sound(fsnd, 128);
      Z_sound(snd[MN_VILE-1][2], 128);
      break;
    case MN_SOUL:
      setst(i, MNST_ATTACK);
      Z_sound(snd[MN_SOUL-1][2], 128);
      yd = mn[i].ty-mn[i].o.y+mn[i].o.h/2;
      xd = mn[i].tx-mn[i].o.x;
      if (!(m = K8MAX(abs(xd), abs(yd)))) m = 1;
      mn[i].o.xv = xd*16/m;
      mn[i].o.yv = yd*16/m;
      break;
    case MN_MANCUB:
      if (mn[i].ammo == 1) Z_sound(snd[MN_MANCUB-1][2], 128);
      // fallthru
    case MN_ZOMBY:
    case MN_SERG:
    case MN_BSP:
    case MN_ROBO:
    case MN_CYBER:
    case MN_CGUN:
    case MN_SPIDER:
    case MN_PAIN:
    case MN_MAN:
      setst(i, MNST_SHOOT);
      break;
    default:
      return 0;
  }
  return 1;
}

static int kick (int i, obj_t *o) {
  switch (mn[i].t) {
    case MN_FISH:
      setst(i, MNST_ATTACK);
      return 1;
    case MN_DEMON:
      setst(i, MNST_ATTACK);
      Z_sound(snd[0][2], 128);
      return 1;
    case MN_IMP:
      setst(i, MNST_ATTACK);
      Z_sound(snd[1][2], 128);
      return 1;
    case MN_SKEL:
      setst(i, MNST_ATTACK);
      Z_sound(swgsnd, 128);
      return 1;
    case MN_ROBO:
      setst(i, MNST_ATTACK);
      Z_sound(swgsnd, 128);
      return 1;
    case MN_BARON:
    case MN_KNIGHT:
    case MN_CACO:
    case MN_MANCUB:
      return shoot(i, o, 1);
  }
  return 0;
}


static int iscorpse (const obj_t *o, int n) {
  if (!n && myrandmask(7)) return -3;
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (mn[f].t && mn[f].st == MNST_DEAD && Z_overlap(o, &mn[f].o)) {
      switch(mn[f].t) {
        case MN_SOUL:
        case MN_PAIN:
        case MN_CYBER:
        case MN_SPIDER:
        case MN_PL_DEAD:
        case MN_PL_MESS:
        case MN_VILE:
        case MN_BARREL:
          continue;
        default:
          return f;
      }
    }
  }
  return -3;
}


void MN_act (void) {
  int i, st, sx, sy, t;
  static obj_t o;
  static int pt_x = 0, pt_xs = 1, pt_y = 0, pt_ys = 1;
  //
  if (abs(pt_x += pt_xs) > 123) pt_xs = -pt_xs;
  if (abs(pt_y += pt_ys) > 50) pt_ys = -pt_ys;
  if (gsndt > 0 && --gsndt == 0) Z_sound(gsnd[myrand(4)], 128);
  for (i = 0; i < MAX_MONSTERS; ++i) {
    if ((t = mn[i].t) != 0) {
      switch(t) {
        case MN_FISH:
          if (!Z_inwater(mn[i].o.x, mn[i].o.y, mn[i].o.r, mn[i].o.h)) break;
        case MN_SOUL: case MN_PAIN: case MN_CACO:
          if (mn[i].st != MNST_DIE && mn[i].st != MNST_DEAD) --mn[i].o.yv;
          break;
      }
      //z_mon = 1;
      st = Z_moveobj(&mn[i].o);
      //z_mon = 0;
      BM_mark(&mn[i].o, BM_MONSTER);
      if (st&Z_FALLOUT) {
        if (t == MN_ROBO) g_exit = D2D_LEVEL_EXIT_WINGAME_ROBO;
        mn[i].t = 0;
        --mnum;
        continue;
      }
      if (st&Z_HITWATER) Z_splash(&mn[i].o, mn[i].o.r+mn[i].o.h);
      SW_press(mn[i].o.x, mn[i].o.y, mn[i].o.r, mn[i].o.h, 8, i);
      if (mn[i].ftime) {
        --mn[i].ftime;
        SMK_flame(mn[i].o.x, mn[i].o.y-mn[i].o.h/2,
          mn[i].o.xv+mn[i].o.vx, mn[i].o.yv+mn[i].o.vy,
          mn[i].o.r/2, mn[i].o.h/2, myrand1(200*2+1)-200, -500, 1, mn[i].fobj);
      }
      if (st&Z_INWATER) mn[i].ftime = 0;
      if (mn[i].st == MNST_DEAD) continue;

      if (st&Z_INWATER && !(myrandmask1(31))) {
        switch (t) {
          case MN_FISH:
            if (myrandtwobits1()) break;
            // fallthru
          case MN_ROBO:
          case MN_BARREL:
          case MN_PL_DEAD:
          case MN_PL_MESS:
            FX_bubble(mn[i].o.x+((myrandbit1())*2-1)*myrand1(mn[i].o.r+1), mn[i].o.y-myrand1(mn[i].o.h+1), 0, 0, 1);
            break;
          default:
            FX_bubble(mn[i].o.x, mn[i].o.y-mn[i].o.h*3/4, 0, 0, 5);
            break;
        }
      }

      if (t == MN_BARREL) {
        if (!mn[i].ap[++mn[i].ac]) {
          mn[i].ac = 0;
          if (mn[i].st == MNST_DIE || mn[i].st == MNST_DEAD) mn[i].t = 0;
        } else if (mn[i].st == MNST_DIE && mn[i].ac == 2) {
          Z_explode(mn[i].o.x, mn[i].o.y-8, 30, mn[i].aim);
        }
        continue;
      }
      if (t == MN_SOUL && (st&Z_HITAIR)) Z_set_speed(&mn[i].o, 16);
      if (mn[i].ammo < 0) ++mn[i].ammo;
      if (mn[i].o.yv < 0 && (st&Z_INWATER)) mn[i].o.yv = -4;
      ++mn[i].atm;
      switch (mn[i].st) {
       case MNST_PAIN:
         if (mn[i].pain >= mnsz[t].mp) {
           mn[i].pain = mnsz[t].mp;
           Z_sound(snd[t-1][1], 128);
         }
         if ((mn[i].pain -= 5) <= mnsz[t].minp) {
           setst(i, MNST_GO);
           mn[i].pain = 0;
           mn[i].ammo = -9;
         }
         break;
       case MNST_SLEEP:
         if (++mn[i].s >= 18) mn[i].s = 0; else break;
         //
         if (!m_notarget) {
           for (int f = 0; f < g_plrcount; ++f) {
             if (Z_look(&mn[i].o, &pl[f].o, mn[i].d)) {
               setst(i, MNST_GO);
               mn[i].aim = pl[f].id;
               mn[i].atm = 0;
               Z_sound(wakeupsnd(t), 128);
             }
           }
         }
         break;
       case MNST_WAIT:
         if (--mn[i].s < 0) setst(i, MNST_GO);
         break;
       case MNST_GO:
         if (st&Z_BLOCK) {
           mn[i].d ^= 1;
           setst(i, MNST_RUNOUT);
           mn[i].s = 40;
           break;
         }
         if (t == MN_VILE) if(iscorpse(&mn[i].o, 0) >= 0) {
           setst(i, MNST_ATTACK);mn[i].o.xv = 0;break;
         }
         //
         if (!Z_getobjpos(mn[i].aim, &o) || mn[i].atm > MAX_ATM) {
           if (!MN_findnewprey(i)) {
             mn[i].aim = -3;
             o.x = mn[i].o.x+pt_x;o.y = mn[i].o.y+pt_y;
             o.xv = o.vx = o.yv = o.vy = o.r = 0;o.h = 1;
           } else {
             Z_getobjpos(mn[i].aim, &o);
           }
         }
         //
         if (Z_overlap(&mn[i].o, &o)) {
           mn[i].atm = 0;
           if(kick(i, &o)) break;
         }
         sx = o.x-mn[i].o.x;
         sy = o.y-o.h/2-mn[i].o.y+mn[i].o.h/2;
         if (!(st&Z_BLOCK) && abs(sx) < 20 && t != MN_FISH) {
           setst(i, MNST_RUN);
           mn[i].s = 15;
           mn[i].d = myrandbit();
           break;
         }
         if (st&Z_HITWALL) {
           if (SW_press(mn[i].o.x, mn[i].o.y, mn[i].o.r, mn[i].o.h, 2, i)) {
             setst(i, MNST_WAIT);
             mn[i].s = 4;
             break;
           }
           switch (t) {
             case MN_CACO: case MN_SOUL: case MN_PAIN: case MN_FISH: break;
             default:
               if (Z_canstand(OBJT_MONSTER, mn[i].o.x, mn[i].o.y, mn[i].o.r)) {
                 mn[i].o.yv = -mnsz[t].jv;
                 setst(i, MNST_CLIMB);
                 break;
               }
           }
           break;
         }
         mn[i].d = (sx > 0)?1:0;
         if (canshoot(t) && abs(sx) > abs(sy) && shoot(i, &o, 0)) break;
         switch (t) {
           case MN_FISH:
             if(!(st&Z_INWATER)) {
               if (Z_canstand(OBJT_MONSTER, mn[i].o.x, mn[i].o.y, mn[i].o.r)) {
                 mn[i].o.yv = -6;
                 mn[i].o.vx += myrand(17)-8;
               }
               setst(i, MNST_PAIN);
               mn[i].pain += 50;
               break;
             }
           case MN_CACO: case MN_SOUL: case MN_PAIN:
             if (abs(sy) > 4) mn[i].o.yv = (sy < 0 ? -4 : 4); else mn[i].o.yv = 0;
             if (t == MN_FISH && mn[i].o.yv < 0 && !Z_inwater(mn[i].o.x, mn[i].o.y-8, mn[i].o.r, mn[i].o.h)) {
               mn[i].o.yv = 0;
               setst(i, MNST_RUN);
               mn[i].d = myrandbit();
               mn[i].s = 20;
             }
             break;
           default:
             if (sy < -20 && Z_canstand(OBJT_MONSTER, mn[i].o.x, mn[i].o.y, mn[i].o.r) && !(myrandtwobits())) mn[i].o.yv = -mnsz[t].jv;
             break;
         }
         if (++mn[i].s >= 8) {
           mn[i].s = 0;
           if (!(myrandmask(7))) Z_sound(snd[t-1][0], 128);
         }
         mn[i].o.xv = (mn[i].d ? 1 : -1)*mnsz[t].rv;
         if (st&Z_INWATER) mn[i].o.xv /= 2;
         else if (t == MN_FISH) mn[i].o.xv = 0;
         break;
       case MNST_RUN:
         if (st&Z_BLOCK) {
           setst(i, MNST_RUNOUT);
           mn[i].d ^= 1;
           mn[i].s = 40;
           break;
         }
         if (--mn[i].s <= 0 || ((st&Z_HITWALL) && mn[i].o.yv+mn[i].o.vy == 0)) {
           setst(i, MNST_GO);
           mn[i].s = 0;
           if (st&(Z_HITWALL|Z_BLOCK)) mn[i].d ^= 1;
           if (!(myrandmask(7))) Z_sound(snd[t-1][0], 128);
         }
         mn[i].o.xv = (mn[i].d ? 1 : -1)*mnsz[t].rv;
         if (st&Z_INWATER) mn[i].o.xv /= 2;
         else if (t == MN_FISH) mn[i].o.xv = 0;
         break;
       case MNST_RUNOUT:
         if (!(st&Z_BLOCK) && mn[i].s > 0) mn[i].s = 0;
         if (--mn[i].s <= -18) {
           setst(i, MNST_GO);
           mn[i].s = 0;
           if (st&(Z_HITWALL|Z_BLOCK)) mn[i].d ^= 1;
           if (!(myrandmask(7))) Z_sound(snd[t-1][0], 128);
         }
         mn[i].o.xv = (mn[i].d ? 1 : -1)*mnsz[t].rv;
         if (st&Z_INWATER) mn[i].o.xv /= 2;
         else if (t == MN_FISH) mn[i].o.xv = 0;
         break;
       case MNST_CLIMB:
         if (mn[i].o.yv+mn[i].o.vy >= 0 || !(st&Z_HITWALL)) {
           setst(i, MNST_GO);
           mn[i].s = 0;
           if (st&(Z_HITWALL|Z_BLOCK)) {
             mn[i].d ^= 1;
             setst(i, MNST_RUN);
             mn[i].s = 15;
           }
         }
         mn[i].o.xv = (mn[i].d ? 1 : -1)*mnsz[t].rv;
         if (st&Z_INWATER) mn[i].o.xv /= 2;
         else if(t == MN_FISH) mn[i].o.xv = 0;
         break;
       case MNST_ATTACK:
       case MNST_SHOOT:
         if (t == MN_SOUL) {
           if (st&(Z_HITWALL|Z_HITCEIL|Z_HITLAND)) setst(i, MNST_GO);
           break;
         }
         if (t != MN_FISH) mn[i].o.xv = Z_dec(mn[i].o.xv, 1);
         if (t == MN_VILE && mn[i].st == MNST_SHOOT) {
           if (!Z_getobjpos(mn[i].aim, &o)) { setst(i, MNST_GO); break; }
           if (!Z_look(&mn[i].o, &o, mn[i].d)) { setst(i, MNST_GO); break; }
           if (Z_inwater(o.x, o.y, o.r, o.h)) { setst(i, MNST_GO); break; }
           mn[i].tx = o.x;
           mn[i].ty = o.y;
           Z_hitobj(mn[i].aim, 2, i, HIT_SOME);
         }
         break;
      }
      if  (mn[i].st == MNST_REVIVE) {
        if (--mn[i].ac == 0) setst(i, MNST_GO);
      } else {
        ++mn[i].ac;
      }
      if (!mn[i].ap[mn[i].ac]) {
        switch (mn[i].st) {
          case MNST_ATTACK:
            switch (t) {
              case MN_SOUL: mn[i].ac = 0;
              case MN_IMP:
              case MN_DEMON:
                if (Z_hit(&mn[i].o, 15, i, HIT_SOME) && t == MN_SOUL) setst(i, MNST_GO);
                break;
              case MN_FISH:
                if (Z_hit(&mn[i].o, 10, i, HIT_SOME)) Z_sound(snd[MN_FISH-1][2], 128);
                break;
              case MN_SKEL:
              case MN_ROBO:
                o = mn[i].o;
                o.xv = (mn[i].d ? 50 : -50);
                if (Z_hit(&o, 50, i, HIT_SOME)) Z_sound(pchsnd, 128);
                break;
              case MN_VILE:
                sx = iscorpse(&mn[i].o, 1);
                if (sx == -3) break;
                if (!mn[sx].t || mn[sx].st != MNST_DEAD) break;
                setst(sx, MNST_REVIVE);
                Z_sound(slopsnd, 128);
                hit_xv = hit_yv = 0;
                MN_hit(i, 5, -3, HIT_SOME);
                break;
            }
            if (t != MN_SOUL && mn[i].st != MNST_DIE) setst(i, MNST_GO);
            break;
          case MNST_SHOOT:
            switch(t) {
              case MN_IMP:
                WP_ball1(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_ZOMBY:
                WP_pistol(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_SERG:
                WP_shotgun(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_MAN:
                WP_dshotgun(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                mn[i].ammo = -36;
                break;
              case MN_CYBER:
                WP_rocket(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_SKEL:
                WP_revf(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i, mn[i].aim);
                break;
              case MN_CGUN:
              case MN_SPIDER:
                WP_mgun(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_BSP:
                WP_aplasma(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_ROBO:
                WP_plasma(mn[i].o.x+(mn[i].d*2-1)*15, mn[i].o.y-30, mn[i].tx, mn[i].ty, i);
                break;
              case MN_MANCUB:
                WP_manfire(mn[i].o.x+(mn[i].d*2-1)*mn[i].o.r, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_BARON: case MN_KNIGHT:
                WP_ball7(mn[i].o.x, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_CACO:
                WP_ball2(mn[i].o.x, mn[i].o.y-mn[i].o.h/2, mn[i].tx, mn[i].ty, i);
                break;
              case MN_PAIN:
                if ((sx = MN_spawn(mn[i].o.x, mn[i].o.y, mn[i].d, MN_SOUL)) == -1) break;
                Z_getobjpos((mn[sx].aim = mn[i].aim), &o);
                mn[sx].atm = 0;
                shoot(sx, &o, 1);
                break;
            }
            if (t == MN_CGUN || t == MN_SPIDER || t == MN_BSP || t == MN_MANCUB || t == MN_ROBO) {
              if (!Z_getobjpos(mn[i].aim, &o)) MN_findnewprey(i);
              else if (shoot(i, &o, 0)) break;
            }
            setst(i, MNST_GO);
            break;
          case MNST_DIE:
            setst(i, MNST_DEAD);
            if (t == MN_PAIN || t == MN_SOUL) mn[i].ftime = 0;
            if (t == MN_PAIN) {
              if ((sx = MN_spawn(mn[i].o.x-15, mn[i].o.y, 0, MN_SOUL)) == -1) break;
              setst(sx, MNST_GO);
              if ((sx = MN_spawn(mn[i].o.x+15, mn[i].o.y, 1, MN_SOUL)) == -1) break;
              setst(sx, MNST_GO);
              if ((sx = MN_spawn(mn[i].o.x, mn[i].o.y-10, 1, MN_SOUL)) == -1) break;
              setst(sx, MNST_GO);
            }
            break;
          default:
            mn[i].ac = 0;
            break;
        }
      }
      switch (mn[i].st) {
        case MNST_GO:
        case MNST_RUN:
        case MNST_CLIMB:
        case MNST_RUNOUT:
          if (t == MN_CYBER || t == MN_SPIDER || t == MN_BSP) {
            if (mn[i].ac == 0 || mn[i].ac == 6) Z_sound(snd[t-1][2], 128);
          } else if (t == MN_ROBO) {
            if(mn[i].ac == 0 || mn[i].ac == 12) Z_sound(snd[t-1][2], 128);
          }
          break;
      }
    }
  }
}


void MN_mark (void) {
  for (int f = 0; f < MAX_MONSTERS; ++f) if (mn[f].t != 0) BM_mark(&mn[f].o, BM_MONSTER);
}


static void MN_lighting (int i) {
  if (mn[i].t == MN_SOUL && mn[i].st != MNST_DEAD) Z_add_light(mn[i].o.x, mn[i].o.y-14, 32, 160, 0, 0, 220);
}


void MN_draw (void) {
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (mn[f].t) {
      MN_lighting(f);
      if (mn[f].t >= MN_PL_DEAD) {
        Z_drawmanspr(mn[f].o.x, mn[f].o.y, &pl_spr[mn[f].t-MN_PL_DEAD], 0, mn[f].d);
        continue;
      }
      if ((mn[f].t != MN_SOUL && mn[f].t != MN_PAIN) || mn[f].st != MNST_DEAD) {
        if (mn[f].t != MN_MAN) {
          Z_drawspr(mn[f].o.x, mn[f].o.y, &spr[mn[f].t-1][(mn[f].ap[mn[f].ac]-'A')*2+mn[f].d], sprd[mn[f].t-1][(mn[f].ap[mn[f].ac]-'A')*2+mn[f].d]);
        } else {
          if (mn[f].ap[mn[f].ac] == 'E' || mn[f].ap[mn[f].ac] == 'F') {
            Z_drawspr(mn[f].o.x, mn[f].o.y, &sgun[mn[f].ap[mn[f].ac]-'E'], mn[f].d);
          }
          Z_drawmanspr(mn[f].o.x, mn[f].o.y, &spr[mn[f].t-1][(mn[f].ap[mn[f].ac]-'A')*2+mn[f].d], sprd[mn[f].t-1][(mn[f].ap[mn[f].ac]-'A')*2+mn[f].d], MANCOLOR);
        }
      }
      if (mn[f].t == MN_VILE && mn[f].st == MNST_SHOOT) Z_drawspr(mn[f].tx, mn[f].ty, &fspr[mn[f].ac/3], 0);
    }
  }
}


int MN_hit (int n, int d, int o, int t) {
  int i;
  //
  if (mn[n].st == MNST_DEAD || mn[n].st == MNST_DIE) return 0;
  if (o == n) {
    if (t != HIT_ROCKET && t != HIT_ELECTRO) return 0;
    if (mn[n].t == MN_CYBER || mn[n].t == MN_BARREL) return 1;
  }
  if (o >= 0) {
    if (mn[o].t == MN_SOUL && mn[n].t == MN_PAIN) return 0;
    if (mn[o].t == mn[n].t) {
      switch (mn[n].t) {
        case MN_IMP: case MN_DEMON:
        case MN_BARON: case MN_KNIGHT:
        case MN_CACO: case MN_SOUL:
        case MN_MANCUB: case MN_SKEL:
        case MN_FISH:
          return 0;
      }
    }
  }
  if (t == HIT_FLAME) {
    if (mn[n].ftime && mn[n].fobj == o) {
      if (g_time&31) return 1;
    } else {
      mn[n].ftime = 255;
      mn[n].fobj = o;
    }
  }
  if (t == HIT_ELECTRO && mn[n].t == MN_FISH) {
    setst(n, MNST_RUN);
    mn[n].s = 20;
    mn[n].d = myrandbit();
    return 1;
  }
  if (t == HIT_TRAP) mn[n].life = -100;
  if (mn[n].t == MN_ROBO) d = 0;
  if ((mn[n].life -= d) <= 0) --mnum;
  if (!mn[n].pain) mn[n].pain = 3;
  mn[n].pain += d;
  if (mn[n].st != MNST_PAIN && mn[n].pain >= mnsz[mn[n].t].minp) setst(n, MNST_PAIN);
  if (mn[n].t != MN_BARREL) DOT_blood(mn[n].o.x, mn[n].o.y-mn[n].o.h/2, hit_xv, hit_yv, d*2);
  mn[n].aim = o;
  mn[n].atm = 0;
  if (mn[n].life <= 0) {
    if (mn[n].t != MN_BARREL) {
      if (o < 0 && -o >= g_plrcount) ++(pl[(-o)-1].kills);
    }
    setst(n, MNST_DIE);
    switch (mn[n].t) {
      case MN_ZOMBY: i = I_CLIP;break;
      case MN_SERG: i = I_SGUN;break;
      case MN_CGUN: i = I_MGUN;break;
      case MN_MAN: i = I_KEYR;break;
      default: i = 0;
    }
    if (i) IT_spawn(mn[n].o.x, mn[n].o.y, i);
    mn[n].o.xv = 0;
    mn[n].o.h = 6;
    if (mn[n].life <= -mnsz[mn[n].t].sp) {
      switch (mn[n].t) {
        case MN_IMP:
        case MN_ZOMBY:
        case MN_SERG:
        case MN_CGUN:
        case MN_MAN:
          mn[n].ap = slopanim[mn[n].t-1];
          Z_sound(slopsnd, 128);
          break;
        case MN_BSP:
          if (g_map == 9) break;
          // fallthru
        default:
          Z_sound(dthsnd(mn[n].t), 128);
          break;
      }
    } else if (mn[n].t != MN_BSP || g_map != 9) {
      Z_sound(dthsnd(mn[n].t), 128);
    }
    mn[n].life = 0;
  } else if (mn[n].st == MNST_SLEEP) {
    setst(n, MNST_GO);
    mn[n].pain = mnsz[mn[n].t].mp;
  }
  //
  return 1;
}


#define hit(o, x, y) (y <= o.y && y > o.y-o.h && x >= o.x-o.r && x <= o.x+o.r)

int Z_gunhit (int x, int y, int objid_shooter, int xv, int yv) {
  for (int f = 0; f < g_plrcount; ++f) {
    if (objid_shooter != -(f+1) && hit(pl[f].o, x, y) && PL_hit(&pl[f], 3, objid_shooter, HIT_SOME)) {
      pl[0].o.vx += xv;
      pl[0].o.vy += yv;
      return -(f+1);
    }
  }
  //
  for (int f = 0;f < MAX_MONSTERS;++f) {
    if (mn[f].t && objid_shooter != f && hit(mn[f].o, x, y) && MN_hit(f, 3, objid_shooter, HIT_SOME)) {
      mn[f].o.vx += xv;
      mn[f].o.vy += yv;
      return 1;
    }
  }
  //
  return 0;
}


static void goodsnd (void) {
  if (!g_dm) return;
  gsndt = 18;
}


int Z_hit (obj_t *o, int d, int own, int t) {
  hit_xv = o->xv+o->vx;
  hit_yv = o->yv+o->vy;
  //
  for (int f = 0; f < g_plrcount; ++f) {
    if (Z_overlap(o, &pl[f].o) && PL_hit(&pl[f], d, own, t)) {
      pl[f].o.vx += (o->xv+o->vx)*(t == HIT_BFG ? 8 : 1)/4;
      pl[f].o.vy += (o->yv+o->vy)*(t == HIT_BFG ? 8 : 1)/4;
      if (t == HIT_BFG) goodsnd();
      //
      return -(f+1);
    }
  }
  //
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (mn[f].t && Z_overlap(o, &mn[f].o) && MN_hit(f, d, own, t)) {
      mn[f].o.vx += (o->xv+o->vx)*(t == HIT_BFG ? 8 : 1)/4;
      mn[f].o.vy += (o->yv+o->vy)*(t == HIT_BFG ? 8 : 1)/4;
      //
      return 1;
    }
  }
  //
  return 0;
}


void MN_killedp (void) {
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (mn[f].t == MN_MAN && mn[f].st != MNST_DEAD && mn[f].st != MNST_DIE && mn[f].st != MNST_SLEEP) Z_sound(trupsnd, 128);
  }
}


int Z_hitobj (int obj, int d, int own, int t) {
  hit_xv = hit_yv = 0;
  if (obj == -1) return PL_hit(&pl[0], d, own, t);
  if (obj == -2 && g_plrcount == 2) return PL_hit(&pl[1], d, own, t);
  if (obj < 0 || obj >= MAX_MONSTERS) return 0;
  if (mn[obj].t) return MN_hit(obj, d, own, t);
  return 0;
}


void Z_explode (int x, int y, int rad, int own) {
  long r;
  int m;
  //
  if (x < -100 || x > FLDW*CELW+100) return;
  if (y < -100 || y > FLDH*CELH+100) return;
  //
  r = (long)rad*rad;
  //
  for (int f = 0; f < g_plrcount; ++f) {
    int dx = pl[f].o.x-x;
    int dy = pl[f].o.y-pl[f].o.h/2-y;
    //
    if ((long)dx*dx+(long)dy*dy < r) {
      if (!(m = K8MAX(abs(dx), abs(dy)))) m = 1;
      pl[f].o.vx += hit_xv = dx*10/m;
      pl[f].o.vy += hit_yv = dy*10/m;
      PL_hit(&pl[f], 100*(rad-m)/rad, own, HIT_ROCKET);
    }
  }
  //
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (mn[f].t) {
      int dx = mn[f].o.x-x;
      int dy = mn[f].o.y-mn[f].o.h/2-y;
      //
      if ((long)dx*dx+(long)dy*dy < r) {
        if (!(m = K8MAX(abs(dx), abs(dy)))) m = 1;
        mn[f].o.vx += hit_xv = dx*10/m;
        mn[f].o.vy += hit_yv = dy*10/m;
        MN_hit(f, mn[f].o.r*10*(rad-m)/rad, own, HIT_ROCKET);
      }
    }
  }
}


void Z_bfg9000 (int x, int y, int own) {
  int dx, dy;
  //
  hit_xv = hit_yv = 0;
  //
  if (x < -100 || x > FLDW*CELW+100) return;
  if (y < -100 || y > FLDH*CELH+100) return;
  //
  dx = pl[0].o.x-x;
  dy = pl[0].o.y-pl[0].o.h/2-y;
  //
  if (own != -1 && (long)dx*dx+(long)dy*dy < 16000 && Z_cansee(x, y, pl[0].o.x, pl[0].o.y-pl[0].o.h/2)) {
    if (PL_hit(&pl[0], 50, own, HIT_SOME)) WP_bfghit(pl[0].o.x, pl[0].o.y-pl[0].o.h/2, own);
  }
  //
  if (g_plrcount == 2) {
    dx = pl[1].o.x-x;
    dy = pl[1].o.y-pl[1].o.h/2-y;
    //
    if (own != -2 && (long)dx*dx+(long)dy*dy < 16000 && Z_cansee(x, y, pl[1].o.x, pl[1].o.y-pl[1].o.h/2)) {
      if (PL_hit(&pl[1], 50, own, HIT_SOME)) WP_bfghit(pl[1].o.x, pl[1].o.y-pl[1].o.h/2, own);
    }
  }
  //
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (mn[f].t && own != f) {
      dx = mn[f].o.x-x;
      dy = mn[f].o.y-mn[f].o.h/2-y;
      if ((long)dx*dx+(long)dy*dy < 16000 && Z_cansee(x, y, mn[f].o.x, mn[f].o.y-mn[f].o.h/2)) {
        if (MN_hit(f, 50, own, HIT_SOME)) WP_bfghit(mn[f].o.x, mn[f].o.y-mn[f].o.h/2, own);
      }
    }
  }
}


int Z_chktrap (int t, int d, int o, int ht) {
  int s = 0;
  //
  hit_xv = hit_yv = 0;
  //
  for (int f = 0; f < g_plrcount; ++f) {
    if (Z_istrapped(pl[f].o.x, pl[f].o.y, pl[f].o.r, pl[f].o.h)) {
      s = 1;
      if (t) PL_hit(&pl[f], d, o, ht);
    }
  }
  //
  for (int f = 0; f < MAX_MONSTERS; ++f) {
    if (mn[f].t && mn[f].st != MNST_DEAD) {
      if (Z_istrapped(mn[f].o.x, mn[f].o.y, mn[f].o.r, mn[f].o.h)) {
        s = 1;
        if (t) MN_hit(f, d, o, ht);
      }
    }
  }
  //
  return s;
}


void Z_teleobj (int o, int x, int y) {
  obj_t *p;
  //
  if (o == -1) p = &pl[0].o;
  else if (o == -2) p = &pl[1].o;
  else if (o >= 0 && o < MAX_MONSTERS) p = &mn[o].o;
  else return;
  //
  FX_tfog(p->x, p->y);
  FX_tfog((p->x = x), (p->y = y));
  Z_sound(telesnd, 128);
}


void MN_warning (int l, int t, int r, int b) {
  for (int f = 0;f < MAX_MONSTERS; ++f) {
    if (mn[f].t && mn[f].t != MN_CACO && mn[f].t != MN_SOUL && mn[f].t != MN_PAIN && mn[f].t != MN_FISH) {
      if (mn[f].st != MNST_DIE && mn[f].st != MNST_DEAD && mn[f].st != MNST_SLEEP) {
        if (mn[f].o.x+mn[f].o.r >= l && mn[f].o.x-mn[f].o.r <= r && mn[f].o.y >= t && mn[f].o.y-mn[f].o.h <= b) {
          if (Z_canstand(OBJT_MONSTER, mn[f].o.x, mn[f].o.y, mn[f].o.r)) {
            mn[f].o.yv = -mnsz[mn[f].t].jv;
          }
        }
      }
    }
  }
}


static int MN_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    for (cnt = MAX_MONSTERS; cnt > 0; --cnt) if (mn[cnt-1].t) break;
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(mn[0])); // structure size
    membuf_write_i32(mbuf, mnum);
    membuf_write_i32(mbuf, gsndt);
    membuf_write_ui16(mbuf, cnt);
    membuf_write(mbuf, mn, cnt*sizeof(mn[0]));
  }
  //
  return 0;
}


static int MN_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    MN_init();
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'monsters' version!\n");
      return -1;
    }
    if (membuf_read_ui16(mbuf) != sizeof(mn[0])) {
      conlogf("LOADGAME ERROR: invalid 'monsters' size!\n");
      return -1;
    }
    mnum = membuf_read_i32(mbuf);
    gsndt = membuf_read_i32(mbuf);
    cnt = membuf_read_ui16(mbuf);
    if (cnt > MAX_MONSTERS) {
      conlogf("LOADGAME ERROR: too many 'monsters'!\n");
      return -1;
    }
    if (membuf_read_full(mbuf, mn, cnt*sizeof(mn[0])) < 0) {
      conlogf("LOADGAME ERROR: can't read 'monsters'!\n");
      return -1;
    }
    for (int f = 0; f < cnt; ++f) if (mn[f].t) { int c = mn[f].ac; setst(f, mn[f].st); mn[f].ac = c; }
  }
  //
  if (waserror) MN_init();
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) {
    for (size_t c = 0; c < 29*2; ++c) V_initvga(&spr[f][c]);
  }
  for (size_t f = 0; f < sizeof(fspr)/sizeof(fspr[0]); ++f) V_initvga(&fspr[f]);
  for (size_t f = 0; f < sizeof(sgun)/sizeof(sgun[0]); ++f) V_initvga(&sgun[f]);
  for (size_t f = 0; f < sizeof(pl_spr)/sizeof(pl_spr[0]); ++f) V_initvga(&pl_spr[f]);
  //
  F_registerSaveLoad("ENTITIES", MN_savegame, MN_loadgame, SAV_NORMAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) {
    for (size_t c = 0; c < 29*2; ++c) V_freevga(&spr[f][c]);
  }
  for (size_t f = 0; f < sizeof(fspr)/sizeof(fspr[0]); ++f) V_freevga(&fspr[f]);
  for (size_t f = 0; f < sizeof(sgun)/sizeof(sgun[0]); ++f) V_freevga(&sgun[f]);
  for (size_t f = 0; f < sizeof(pl_spr)/sizeof(pl_spr[0]); ++f) V_freevga(&pl_spr[f]);
}
