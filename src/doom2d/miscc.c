/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "misc.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "gamemode.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "game.h"
#include "gamemode.h"
#include "map.h"
#include "monster.h"
#include "player.h"
#include "sound.h"
#include "vga.h"
#include "view.h"


//#define w_view_wdt 200
//#define w_view_hgt 98

#define MAX_YV  (30)
#define MAXAIR  (1091)


static vgaimg sth[22], bfh[160-'!'], sfh[160-'!'];
static const snd_t *bulsnd[2];
static vgaimg stone, stone2, keys[3];
int z_print_x = 0, z_print_y = 0, z_print_xstart = 0;


void Z_getspr (vgaimg *img, const char n[4], int s, int d, char *dir) {
  int h = F_getsprid(n, s, d);
  void *v;
  //
  if (dir) *dir = (h&0x8000 ? 1 : 0);
  v = M_lock(h);
  V_loadvga_ptr(img, v);
  M_unlock(v);
}


const snd_t *Z_getsnd (const char n[6]) {
  char s[9];
  const snd_t *res;
  //
  //if (snd_type == -1) return NULL;
  strncpy(s+2, n, 6);
  s[0] = 'D';
  s[1] = 'S';
  s[8] = '\0';
  res = M_lock(F_getresid(s));
  //dlogf("Z_getsnd(\"%s\")=%p", s, res);
  //dlogf("Z_getsnd(\"%s\")=%p; len=%u; rate=%u; lstart=%u; llen=%u", s, res, res->len, res->rate, res->lstart, res->llen);
  return res;
}


int Z_sound (const snd_t *s, int v) {
  //if (snd_type == -1) return 0;
  if (s != NULL) {
    S_play(s, -1, 1024, v);
    //return F_getreslen(((const int *)s)[-1])/605;
    //return s->len/605;
    return s->len/(s->rate/18);
  }
  return 0;
}


#define GAS_START  (MN__LAST-MN_DEMON+5)
#define GAS_TOTAL  (MN__LAST-MN_DEMON+16+10)


////////////////////////////////////////////////////////////////////////////////
char *Z_vasprintf (const char *fmt, va_list vaorig) {
  char *buf = NULL;
  int olen, len = 128;
  //
  buf = malloc(len);
  if (buf == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
  for (;;) {
    char *nb;
    va_list va;
    //
    va_copy(va, vaorig);
    olen = vsnprintf(buf, len, fmt, va);
    va_end(va);
    if (olen >= 0 && olen < len) return buf;
    if (olen < 0) olen = len*2-1;
    nb = realloc(buf, olen+1);
    if (nb == NULL) { fprintf(stderr, "\nFATAL: out of memory!\n"); abort(); }
    buf = nb;
    len = olen+1;
  }
}


GCC_PRINTF(1,2) char *Z_sprintf (const char *fmt, ...) {
  char *buf = NULL;
  va_list va;
  //
  va_start(va, fmt);
  buf = Z_vasprintf(fmt, va);
  va_end(va);
  return buf;
}


////////////////////////////////////////////////////////////////////////////////
void Z_initst (void) {
  static const char nm[22][8] = {
    "STTNUM0", "STTNUM1", "STTNUM2", "STTNUM3", "STTNUM4",
    "STTNUM5", "STTNUM6", "STTNUM7", "STTNUM8", "STTNUM9",
    "STTMINUS", "STTPRCNT",
    "FISTA0", "CSAWA0", "PISTA0", "SHOTA0", "SGN2A0", "MGUNA0", "LAUNA0",
    "PLASA0", "BFUGA0", "GUN2A0"
  };
  char s[10];
  //
  V_loadvga("STONE", &stone);
  V_loadvga("STONE2", &stone2);
  V_loadvga("KEYRA0", &keys[0]);
  V_loadvga("KEYGA0", &keys[1]);
  V_loadvga("KEYBA0", &keys[2]);
  //
  for (int i = 0; i < 22; ++i) V_loadvga(nm[i], &sth[i]);
  // big letters
  strcpy(s, "STBF_*");
  for (int i = '!'; i < 160; ++i) {
    s[5] = i;
    if (F_findres(s) >= 0) V_loadvga(s, &bfh[i-'!']);
    if (!(i&15)) conlogdots(GAS_START+((i-'!')>>4), GAS_TOTAL);
  }
  // small letters
  for (int i = '!'; i < 160; ++i) {
    sprintf(s, "STCFN%03d", i);
    if (F_findres(s) >= 0) V_loadvga(s, &sfh[i-'!']);
    if (!(i&15)) conlogdots(GAS_START+8+((i-'!')>>4), GAS_TOTAL);
  }
  // numbers
  strcpy(s, "WINUM*");
  for (int i = '0'; i <= '9'; ++i) {
    s[5] = i;
    if (F_findres(s) >= 0) V_loadvga(s, &bfh[i-'!']);
  }
  //
  V_loadvga("WICOLON", &bfh[':'-'!']);
  //
  bulsnd[0] = Z_getsnd("BUL1");
  bulsnd[1] = Z_getsnd("BUL2");
}


void Z_gotoxy (int x, int y) {
  z_print_x = z_print_xstart = x;
  z_print_y = y;
}


static int Z_putbfch (int c, int dodraw) {
  vgaimg *p = (c > 32 && c < 160 ? &bfh[c-'!'] : NULL);
  int res = 12;
  //
  if (p != NULL && p->w < 1) p = NULL;
  if (p != NULL) {
    res = p->w-1;
    if (dodraw) {
      V_spr(z_print_x, z_print_y, p);
      z_print_x += p->w-1;
    }
  } else {
    switch (c) {
      case '\n': if (dodraw) z_print_y += 13; res = 0;// fallthru
      case '\r': if (dodraw) z_print_xstart = 0; res = 0; break;
      default:
        if (islower(c)) return Z_putbfch(toupper(c), dodraw);
        else if (dodraw) z_print_x += 12;
        break;
    }
  }
  //
  return res;
}


static int Z_putsfch (int c, int dodraw) {
  vgaimg *p = (c > 32 && c < 160 ? &sfh[c-'!'] : NULL);
  int res = 7;
  //
  if (p != NULL && p->w < 1) p = NULL;
  if (p != NULL) {
    res = p->w-1;
    if (dodraw)  {
      V_spr(z_print_x, z_print_y, p);
      z_print_x += p->w-1;
    }
  } else {
    switch (c) {
      case '\n': if (dodraw) z_print_y += 8; res = 0; // fallthru
      case '\r': if (dodraw) z_print_xstart = 0; res = 0; break;
      default:
        if (islower(c)) return Z_putsfch(toupper(c), dodraw);
        else if (dodraw) z_print_x += 7;
        break;
    }
  }
  //
  return res;
}


static int Z_vaprintfCB (int dodraw, const char *fmt, va_list vaorig, int (*putcfn)(int c, int dodraw)) {
  int res = 0;
  //
  if (fmt != NULL && fmt[0]) {
    if (strchr(fmt, '%') != NULL) {
      char *buf = Z_vasprintf(fmt, vaorig);
      //
      if (buf != NULL) {
        for (const char *p = buf; *p; ++p) res += putcfn((unsigned char)(p[0]), dodraw);
        free(buf);
      }
    } else {
      for (; *fmt; ++fmt) res += putcfn((unsigned char)(fmt[0]), dodraw);
    }
  }
  //
  return res;
}


GCC_PRINTF(1,2) void Z_printbf (const char *fmt, ...) {
  va_list va;
  //
  va_start(va, fmt);
  Z_vaprintfCB(1, fmt, va, Z_putbfch);
  va_end(va);
}


GCC_PRINTF(1,2) void Z_printsf (const char *fmt, ...) {
  va_list va;
  //
  va_start(va, fmt);
  Z_vaprintfCB(1, fmt, va, Z_putsfch);
  va_end(va);
}


GCC_PRINTF(1,2) int Z_strfwidthbf (const char *fmt, ...) {
  va_list va;
  int res;
  //
  va_start(va, fmt);
  res = Z_vaprintfCB(0, fmt, va, Z_putbfch);
  va_end(va);
  //
  return res;
}


GCC_PRINTF(1,2) int Z_strfwidthsf (const char *fmt, ...) {
  va_list va;
  int res;
  //
  va_start(va, fmt);
  res = Z_vaprintfCB(0, fmt, va, Z_putsfch);
  va_end(va);
  //
  return res;
}


////////////////////////////////////////////////////////////////////////////////
void Z_drawspr (int x, int y, vgaimg *p, char d) {
  if (d) V_spr2_lm(Z_MAP2SCR_X(x), Z_MAP2SCR_Y(y), p); else V_spr_lm(Z_MAP2SCR_X(x), Z_MAP2SCR_Y(y), p);
}


void Z_drawspr_lit (int x, int y, vgaimg *p, char d) {
  if (d) V_spr2(Z_MAP2SCR_X(x), Z_MAP2SCR_Y(y), p); else V_spr(Z_MAP2SCR_X(x), Z_MAP2SCR_Y(y), p);
}


void Z_drawmanspr (int x, int y, vgaimg *p, char d, uint8_t color) {
  if (d) V_manspr2(Z_MAP2SCR_X(x), Z_MAP2SCR_Y(y), p, color);
  else V_manspr(Z_MAP2SCR_X(x), Z_MAP2SCR_Y(y), p, color);
}


void Z_clrst (void) {
  int h = stone.h;
  //
  V_pic(SCRW-120, w_viewofs_vert, &stone);
  for (int y = h; y < w_view_hgt; y += h) V_pic(SCRW-120, w_viewofs_vert+y, &stone2);
}


void Z_drawstlives (int n) {
  if (n < 0) n = 0; else if (n > 9) n = 9;
  V_setrect(SCRW-40, 30, w_viewofs_vert, 40);
  Z_clrst();
  V_spr(SCRW-35, w_viewofs_vert+17, &sth[n]);
}


void Z_drawstkeys (uint8_t k) {
  int x, n;
  //
  V_setrect(SCRW-120, 70, w_viewofs_vert+77, 23);
  Z_clrst();
  for (k >>= 4, n = 0, x = SCRW-75; n < 3; ++n, k >>= 1, x += 9) if (k&1) V_spr(x, w_viewofs_vert+91, &keys[n]);
}


void Z_drawstair (int a) {
  V_setrect(SCRW-120, 120, w_viewofs_vert+49, 2);
  Z_clrst();
  if (a <= 0) return;
  if (a > MAXAIR) a = MAXAIR;
  a = a*100/MAXAIR;
  if (!a) return;
  V_clr(SCRW-110, a, w_viewofs_vert+49, 2, 0xC8);
}


void Z_drawstprcnt (int y, int n) {
  char s[20];
  int l, x;
  //
  V_setrect(SCRW-120, 70, y*19+7+w_viewofs_vert, 19);
  Z_clrst();
  sprintf(s, "%3d%%", n);
  l = strlen(s);
  x = SCRW-110;
  for (int i = 0; i < l; ++i, x += 14) {
    int c;
    //
    if (s[i] >= '0' && s[i] <= '9') c = s[i]-'0';
    else if (s[i] == '-') c = 10;
    else if (s[i] == '%') c = 11;
    else c = -1;
    if (c >= 0) V_spr(x, y*19+7+w_viewofs_vert, &sth[c]);
  }
}


void Z_drawstnum (int n) {
  char s[20];
  int l, x;
  //
  V_setrect(SCRW-50, 50, w_viewofs_vert+77, 23);
  Z_clrst();
  if (!g_dm) return;
  sprintf(s, "%d", n);
  l = strlen(s);
  x = (115-l*14)+SCRW-120;
  for (int i = 0; i < l; ++i, x += 14) {
    int c;
    //
    if (s[i] >= '0' && s[i] <= '9') c = s[i]-'0';
    else if (s[i] == '-') c = 10;
    else if (s[i] == '%') c = 11;
    else c = -1;
    if (c >= 0) V_spr(x, w_viewofs_vert+77+5, &sth[c]);
  }
}


void Z_drawstwpn (int n, int a) {
  char s[20];
  int i;
  //
  i = n;
  V_setrect(SCRW-120, 120, w_viewofs_vert+58, 23);
  Z_clrst();
  if (i >= 0) V_spr(SCRW-88, w_viewofs_vert+58+19, &sth[i+12]);
  if (n >= 2) {
    int l, x;
    //
    sprintf(s, "%d", a);
    l = strlen(s);
    x = SCRW-10-l*14;
    for (i = 0; i < l; ++i, x += 14) {
      int c;
      //
      if (s[i] >= '0' && s[i] <= '9') c = s[i]-'0';
      else if (s[i] == '-') c = 10;
      else if (s[i] == '%') c = 11;
      else c = -1;
      if (c >= 0) V_spr(x, w_viewofs_vert+58+2, &sth[c]);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static int canStandOrHitCeil (int asdot, int x, int y, int r, int h, int (*checkCB)(uint8_t fldv)) {
  int i;
  //
  i = (x-r)/CELW;
  x = (x+r)/CELW;
  y = (y-h+1)/CELH;
  if (y >= FLDH || y < 0) return 0;
  if (i < 0) i = 0;
  if (x >= FLDW) x = FLDW-1;
  for (; i <= x; ++i) {
    if (checkCB(FIELD_MAP(i, y))) {
      if (!asdot) return 1;
      if (!((walf[FIELD_TFG(i, y)]|walf[FIELD_TBG(i, y)])&2)) return 1;
    }
  }
  //
  return 0;
}


int Z_canstand (ObjectType otype, int x, int y, int r) {
  return canStandOrHitCeil((otype == OBJT_DOT), x, y, r, 0, cmdcon_lambda(int, (uint8_t fldv) { return (fldv == TILE_WALL || fldv == TILE_DOORC || fldv == TILE_STEP); }));
}


int Z_cangodown (ObjectType otype, int x, int y, int r) {
  return canStandOrHitCeil((otype == OBJT_DOT), x, y, r, 0, cmdcon_lambda(int, (uint8_t fldv) { return (fldv == TILE_STEP); }));
}


int Z_hitceil (ObjectType otype, int x, int y, int r, int h) {
  return canStandOrHitCeil((otype == OBJT_DOT), x, y, r, h, cmdcon_lambda(int, (uint8_t fldv) { return (fldv == TILE_WALL || fldv == TILE_DOORC); }));
}


int Z_canfit (ObjectType otype, int x, int y, int r, int h) {
  int sx, sy;
  //
  sx = (x-r)/CELW;
  sy = (y-h+1)/CELH;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x+r)/CELW;
  y = (y-0)/CELH;
  if (x >= FLDW) x = FLDW-1;
  if (y >= FLDH) y = FLDH-1;
  for (int i = sx; i <= x; ++i) {
    for (int j = sy; j <= y; ++j) {
      if (FIELD_MAP(i, j) == TILE_WALL || FIELD_MAP(i, j) == TILE_DOORC) {
        if (otype != OBJT_DOT) return 0;
        if (!((walf[FIELD_TFG(i, j)]|walf[FIELD_TBG(i, j)])&2)) return 0;
      }
    }
  }
  //
  return 1;
}


static int checkerInIs (int x, int y, int r, int h, int xv, int (*checkCB)(uint8_t fldv)) {
  int sx, sy;
  //
  sx = (x-r)/CELW;
  sy = (y-h+1)/CELH;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x+r)/CELW;
  y = (y-1)/CELH;
  if (xv < 0) x = sx; else if (xv > 0) sx = x;
  if (x >= FLDW) x = FLDW-1;
  if (y >= FLDH) y = FLDH-1;
  for (int i = sx; i <= x; ++i) {
    for (int j = sy; j <= y; ++j) {
      int r = checkCB(FIELD_MAP(i, j));
      //
      if (r) return r;
    }
  }
  //
  return 0;
}


int Z_inlift (int x, int y, int r, int h) {
  return checkerInIs(x, y, r, h, 0, cmdcon_lambda(int, (uint8_t fldv) { return (fldv == 9 || fldv == 10 ? fldv-8 : 0); }));
}


int Z_isblocked (int x, int y, int r, int h, int xv) {
  return checkerInIs(x, y, r, h, xv, cmdcon_lambda(int, (uint8_t fldv) { return (fldv == 8); }));
}


int Z_istrapped (int x, int y, int r, int h) {
  return checkerInIs(x, y, r, h, 0, cmdcon_lambda(int, (uint8_t fldv) { return (fldv == 255); }));
}


////////////////////////////////////////////////////////////////////////////////
static uint8_t wfront;


int Z_inwater (int x, int y, int r, int h) {
  int sx, sy;
  //
  sx = (x-r)/CELW;
  sy = (y-h+1)/CELH;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x+r)/CELW;
  y = (y-h/2)/CELH;
  if (x >= FLDW) x = FLDW-1;
  if (y >= FLDH) y = FLDH-1;
  for (int i = sx; i <= x; ++i) {
    for (int j = sy; j <= y; ++j) {
      if (FIELD_MAP(i, j) >= 5 && FIELD_MAP(i, j) <= 7) {
        wfront = FIELD_TFG(i, j);
        return 1;
      }
    }
  }
  return 0;
}


void Z_splash (const obj_t *p, int n) {
  Z_sound(bulsnd[0], 128);
  DOT_water(p->x, p->y-p->h/2, p->xv+p->vx, p->yv+p->vy, n, /*(int)walp[wfront]*/walp[wfront]->wanim-1);
}


int Z_getacid (int x, int y, int r, int h) {
  static const uint8_t tab[4] = {0, 5, 10, 20};
  int sx, sy, a;
  //
  a = 0;
  sx = (x-r)/CELW;
  sy = (y-h+1)/CELH;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x+r)/CELW;
  y = y/CELH;
  if (x >= FLDW) x = FLDW-1;
  if (y >= FLDH) y = FLDH-1;
  for (int i = sx; i <= x; ++i) {
    for (int j = sy; j <= y; ++j) {
      switch (FIELD_MAP(i, j)) {
        case 6: a |= 1; break;
        case 7: a |= 2; break;
      }
    }
  }
  //
  return tab[a];
}


int Z_canbreathe (int x, int y, int r, int h) {
  int sx, sy;
  //
  sx = (x-r)/CELW;
  sy = (y-h+1)/CELH;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x+r)/CELW;
  y = (y-h/2)/CELH;
  if (x >= FLDW) x = FLDW-1;
  if (y >= FLDH) y = FLDH-1;
  if (sx > x || sy > y) return 1;
  for (int i = sx; i <= x; ++i) {
    for (int j = sy; j <= y; ++j) {
      switch (FIELD_MAP(i, j)) {
        case TILE_EMPTY:
        case TILE_DOORO:
        case 9:
        case 10:
          return 1;
      }
    }
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
int Z_overlap (const obj_t *a, const obj_t *b) {
  if (a->x-a->r > b->x+b->r) return 0;
  if (a->x+a->r < b->x-b->r) return 0;
  if (a->y <= b->y-b->h) return 0;
  if (a->y-a->h >= b->y) return 0;
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
void Z_kickobj (obj_t *o, int x, int y, int pwr) {
  int dx, dy, m;
  //
  dx = o->x-x;
  dy = o->y-o->h/2-y;
  if (!(m = K8MAX(abs(dx), abs(dy)))) m = 1;
  o->vx += (long)dx*pwr/m;
  o->vy += (long)dy*pwr/m;
}


////////////////////////////////////////////////////////////////////////////////
int Z_cansee (int x, int y, int xd, int yd) {
  if (gm_new_cansee) {
    int ex, ey;
    ccBool res;
    //
    res = Z_traceray(x, y, xd, yd, &ex, &ey, BM_WALL,
      cmdcon_lambda(int, (int mappixx, int mappixy) {
        return
          (FIELD_MAP(mappixx/CELW, mappixy/CELH) == TILE_WALL ||
           FIELD_MAP(mappixx/CELW, mappixy/CELH) == TILE_DOORC);
      })
    );
    //
    return !res;
  } else {
    int d;
    int sx, sy;
    int xe, ye;
    //
    if ((xd -= x) > 0) sx = 1;
    else if (xd < 0) sx = -1;
    else sx = 0;
    if ((yd -= y) > 0) sy = 1;
    else if (yd < 0) sy = -1;
    else sy = 0;
    if (!xd && !yd) return 1;
    if ((xd = abs(xd)) > (yd = abs(yd))) d = xd; else d = yd;
    xe = ye = 0;
    for (uint32_t i = 0; i <= d; ) {
      if (x < 0 || x >= FLDW*8 || y < 0 || y >= FLDH*8) return 0;
      if (BM_get(x, y)&BM_WALL) {
        if (FIELD_MAP(x>>3, y>>3) == TILE_WALL || FIELD_MAP(x>>3, y>>3) == TILE_DOORC) return 0;
        if ((xe += (xd<<3)) >= d) {
          x += xe/d*sx;
          xe = xe%d;
        }
        if ((ye += (yd<<3)) >= d) {
          y += ye/d*sy;
          ye = ye%d;
        }
        i += 8;
      } else {
        int s, m;
        //
        if (sx == 0) {
          m = 0;
        } else {
          m = x&31;
          if(sx > 0) m ^= 31;
          ++m;
        }
        if (sy == 0) {
          s = 0;
        } else {
          s = y&31;
          if(sy > 0) s ^= 31;
          ++s;
        }
        if ((s < m && s != 0) || m == 0) m = s;
        i += m;
        x += (xd*m+xe)/d*sx;
        xe = (xd*m+xe)%d;
        y += (yd*m+ye)/d*sy;
        ye = (yd*m+ye)%d;
      }
    }
    //
    return 1;
  }
}


////////////////////////////////////////////////////////////////////////////////
int Z_look (const obj_t *a, const obj_t *b, int d) {
  if (Z_sign(b->x-a->x) != d*2-1) return 0;
  return Z_cansee(a->x, a->y-a->h/2, b->x, b->y-b->h/2);
}


////////////////////////////////////////////////////////////////////////////////
#define wvel(v) if((xv = abs(v)+1) > 5) v = Z_dec(v, xv/2-2)


static inline int is_ghost_player (const obj_t *o) { return (o->type == OBJT_PLAYER && p_ghost); }
static inline int is_player (const obj_t *o) { return (o->type == OBJT_PLAYER); }

static inline int clamp7 (int v) { return (abs(v) <= 7 ? v : (v > 0 ? 7 : -7)); }

static inline int is_monster (const obj_t *o) { return (o->type == OBJT_MONSTER); }
static inline int is_dot (const obj_t *o) { return (o->type == OBJT_DOT); }


int Z_moveobj (obj_t *p) {
  int x, y, xv, yv, r, h, lx, ly, st;
  uint8_t inw;
  //
  st = 0;
  x = p->x;
  y = p->y;
  r = p->r;
  h = p->h;
  //
  if (!is_ghost_player(p)) {
    switch (Z_inlift(x, y, r, h)) {
      case 0:
        if (++p->yv > MAX_YV) --p->yv;
        break;
      case 1:
        if (--p->yv < -5) ++p->yv;
        break;
      case 2:
        if (p->yv > 5) --p->yv; else ++p->yv;
        break;
    }
  }
  //
  inw = Z_inwater(x, y, r, h);
  if (inw != 0) {
    st |= Z_INWATER;
    wvel(p->xv);
    wvel(p->yv);
    wvel(p->vx);
    wvel(p->vy);
  }
  //
  p->vx = Z_dec(p->vx, 1);
  p->vy = Z_dec(p->vy, 1);
  //
  xv = p->xv+p->vx;
  yv = p->yv+p->vy;
  //
  while (xv || yv) {
    if (x < -100 || x >= FLDW*8+100 || y < -100 || y >= FLDH*8+100) {
      // out of map
      st |= Z_FALLOUT;
    }
    //
    lx = x;
    x += clamp7(xv);
    //
    if (is_monster(p)) {
      // for monster
      if (Z_isblocked(x, y, r, h, xv)) st |= Z_BLOCK;
    }
    //
    if (!Z_canfit(is_dot(p), x, y, r, h)) {
      if (!is_ghost_player(p)) {
        if (gm_climb_stairs && is_player(p)) {
          // check if we can climb upstairs
          if (yv == 1 && (xv == 1 || xv == -1)) {
            if (Z_canfit(p->type, x, y-CELH, r, h) && Z_canfit(p->type, x+Z_sign(xv)*(CELW-1), y-CELH, r, h)) {
              p->vy = -8;
              return Z_moveobj(p);
            }
          }
        }
        if (xv == 0) x = lx;
        else if (xv < 0) x = ((lx-r)&0xFFF8)+r;
        else x = ((lx+r)&0xFFF8)-r+7;
        xv = p->xv = p->vx = 0;
        st |= Z_HITWALL;
      }
    }
    xv -= clamp7(xv);
    //
    ly = y;
    y += clamp7(yv);
    if (yv >= 8) --y;
    // moving up and hit the ceiling
    if (!is_ghost_player(p)) {
      if (yv < 0 && Z_hitceil(p->type, x, y, r, h)) {
        y = ((ly-h+1)&0xFFF8)+h-1;
        yv = p->vy = 1;
        p->yv = 0;
        st |= Z_HITCEIL;
      }
      //
      if (yv > 0 && Z_canstand(p->type, x, y, r)) {
        y = ((y+1)&0xFFF8)-1;
        yv = p->yv = p->vy = 0;
        st |= Z_HITLAND;
      }
    }
    //
    yv -= clamp7(yv);
  }
  //
  p->x = x;
  p->y = y;
  //
  if (Z_inwater(x, y, r, h)) {
    st |= Z_INWATER;
    if (!inw) st |= Z_HITWATER;
  } else if (inw) {
    st |= Z_HITAIR;
  }
  //
  return st;
}


void Z_set_speed (obj_t *o, int s) {
  int m;
  //
  if (!(m = K8MAX(abs(o->xv), abs(o->yv)))) m = 1;
  o->xv = o->xv*s/m;
  o->yv = o->yv*s/m;
}


void Z_calc_time (uint32_t t, uint16_t *h, uint16_t *m, uint16_t *s) {
  t *= DELAY;
  t /= 1000;
  t -= (*s = t%60);
  t /= 60;
  t -= (*m = t%60);
  *h = t/60;
}


////////////////////////////////////////////////////////////////////////////////
// in, out: map coords
// result: CC_TRUE if trace ends inside the line; if we traced out of level, return CC_FALSE too
ccBool Z_traceray (int xstart, int ystart, int xend, int yend, int *ex, int *ey,
  int bmask, int (*fldhit)(int mappixx, int mappixy))
{
  int mex = FLDW*CELW, mey = FLDH*CELH; // max coords
  int x = xstart, y = ystart;
  int sx, sy; // directions (-1, 0, 1)
  int xd, yd; // xend and yend moved by (-xstart, -ystart), absolute values
  int d; // 'highest step delta'
  int lx, ly; // coords on map for the previous step
  int xsf, ysf; // previous 'fracs'
  int xfrac, yfrac;
  int maxdist, distcheck = 32;
  //
  // convert xd and yd to deltas from (x, y), determine directions
  sx = Z_sign(xd = xend-xstart);
  sy = Z_sign(yd = yend-ystart);
  //
  *ex = xend;
  *ey = yend;
  //
  if (!(xd|yd)) return CC_TRUE;
  //
  if ((xd = abs(xd)) > (yd = abs(yd))) d = xd; else d = yd;
  maxdist = xd*xd+yd*yd;
  xfrac = yfrac = xsf = ysf = 0;
  lx = x;
  ly = y;
  //
  while (x >= 0 && x < mex && y >= 0 && y < mey) {
    if (!bmask || (BM_get(x, y)&bmask)) {
      // there is some wall here, do precision tracing
      if (fldhit(x, y)) {
        // we are inside the tile, trace to it's outer edge
        int xdist, ydist;
        //
        // restore x, y and fracs, effectively going back to the previous tile
        x = lx;
        y = ly;
        xfrac = xsf;
        yfrac = ysf;
        // trace
        while (!fldhit(x, y)) {
          // save x and y
          lx = x;
          ly = y;
          if ((xfrac += xd) >= d) { xfrac -= d; x += sx; }
          if ((yfrac += yd) >= d) { yfrac -= d; y += sy; }
        }
        // got it, check if we are still 'in distance'
        *ex = lx;
        *ey = ly;
        xdist = lx-xstart;
        ydist = ly-ystart;
        return (xdist*xdist+ydist*ydist <= maxdist);
      }
      // no wall, step by almost one tile
      // save x, y and fracs
      lx = x;
      ly = y;
      xsf = xfrac;
      ysf = yfrac;
      //
      if ((xfrac += (xd*(CELW-1))) >= d) {
        x += (xfrac/d)*sx;
        xfrac = xfrac%d;
      }
      if ((yfrac += (yd*(CELH-1))) >= d) {
        y += (yfrac/d)*sy;
        yfrac = yfrac%d;
      }
    } else {
      // there is no wall here, skip whole 'megatile' (4x4 ordinary tiles, 32x32 pixels)
      int skipx, skipy;
      //
      if (--distcheck < 1) {
        int xdist = x-xstart, ydist = y-ystart;
        //
        distcheck = 32;
        if (xdist*xdist+ydist*ydist > maxdist) break;
      }
      //
      // calculate number x of pixels to skip to cross the end of 'megatile'
      // using trick to avoid condition: if (sx > 0) skipx = 31-skipx;
      skipx = (sx != 0 ? ((x&31)^(((-sx)>>1)&31))+1 : 0);
      // calculate number y of pixels to skip to cross the end of 'megatile'
      skipy = (sy != 0 ? ((y&31)^(((-sy)>>1)&31))+1 : 0);
      //
      // use 'skipx' as general 'skip'
      if ((skipy < skipx && skipy != 0) || skipx == 0) skipx = skipy;
      // save x, y and fracs
      lx = x;
      ly = y;
      xsf = xfrac;
      ysf = yfrac;
      //
      x += ((xd*skipx+xfrac)/d)*sx;
      xfrac = (xd*skipx+xfrac)%d;
      y += ((yd*skipx+yfrac)/d)*sy;
      yfrac = (yd*skipx+yfrac)%d;
    }
  }
  //
  return CC_FALSE;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(sth)/sizeof(sth[0]); ++f) V_initvga(&sth[f]);
  for (size_t f = 0; f < sizeof(bfh)/sizeof(bfh[0]); ++f) V_initvga(&bfh[f]);
  for (size_t f = 0; f < sizeof(sfh)/sizeof(sfh[0]); ++f) V_initvga(&sfh[f]);
  V_initvga(&stone);
  V_initvga(&stone2);
  V_initvga(&keys[0]);
  V_initvga(&keys[1]);
  V_initvga(&keys[2]);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(sth)/sizeof(sth[0]); ++f) V_freevga(&sth[f]);
  for (size_t f = 0; f < sizeof(bfh)/sizeof(bfh[0]); ++f) V_freevga(&bfh[f]);
  for (size_t f = 0; f < sizeof(sfh)/sizeof(sfh[0]); ++f) V_freevga(&sfh[f]);
  V_freevga(&stone);
  V_freevga(&stone2);
  V_freevga(&keys[0]);
  V_freevga(&keys[1]);
  V_freevga(&keys[2]);
}
