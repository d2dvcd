/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
/* command line parser */
#include "cmdline.h"
#ifdef _WIN32

#include <windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX_ARGS  (256)

static char *kMyImage = NULL;

int k8argc = 0;
char **k8argv = NULL;


void cmdLineParseStr (const char *cmdline) {
  char ech, *cp;
  int maxLen = strlen(cmdline)+64;
  char *pbuf = malloc(maxLen+4);
  //
  /* skip spaces */
  if (k8argv) free(k8argv);
  k8argv = calloc(MAX_ARGS, sizeof(char *));
  k8argc = 0;
  while (*cmdline == ' ') ++cmdline;
  /* parse args */
  while (*cmdline) {
    /* check first char: quoted? */
    if (*cmdline == '"' || *cmdline == '\'') {
      /* quoted arg */
      ech = *cmdline++;
    } else {
      /* normal arg */
      ech = ' ';
    }
    /* collect chars */
    cp = pbuf;
    while (*cmdline && *cmdline != ech) {
      *cp++ = *cmdline++;
      if (ech != ' ' && cmdline[0] == ech && cmdline[1] == ech) {
        /* two quotes is just a quote */
        *cp++ = *cmdline++;
        ++cmdline;
      }
    }
    *cp = '\0';
    //fprintf(stderr, "[%s]\n", pbuf);
    if (k8argc < MAX_ARGS) k8argv[k8argc++] = strdup(pbuf);
    /* skip quote, if any */
    if (ech != ' ' && cmdline[0] == ech) ++cmdline;
    /* skip spaces */
    while (*cmdline == ' ') ++cmdline;
  }
  free(pbuf);
  //
  if (kMyImage == NULL) {
    kMyImage = calloc(MAX_PATH+1, 1);
    if (kMyImage != NULL) {
      GetModuleFileName(NULL, kMyImage, MAX_PATH);
      if (k8argv[0]) free(k8argv[0]);
      k8argv[0] = strdup(kMyImage);
    }
  }
}


void cmdLineParse (void) {
  return cmdLineParseStr(GetCommandLine());
}

#endif
