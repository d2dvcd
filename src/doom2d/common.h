/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Globals
#ifndef D2D_COMMON_H
#define D2D_COMMON_H

#define d2d_assert_static(_e)  do { \
  enum { d2d_assert_static__ = 1/(_e) }; \
} while (0)


#ifdef _WIN32
#include <windows.h>
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "adbglog.h"
#include "cmdcon.h"

#ifdef JIM_INTERNAL
# include "../libjim/jim.h"
#else
# include <jim.h>
#endif

extern Jim_Interp *jim;


#define DOOM2D_VERSION  "1.352"

#define SAVE_DIR  ".doom2d-vcd"


#define D2DBIT(bitn)  (1<<(bitn))

#define GCC_PACKED    __attribute__((packed)) __attribute__((gcc_struct))
#define GCC_NORETURN  __attribute__((noreturn))

#define GCC_PRINTF(_f,_a)  __attribute__((format(printf,_f,_a)))


enum {
  CMDCON_FLAGS_SERVER = D2DBIT(0), // this variable should be taken from server, and can be changed only before network game starts
  CMDCON_FLAGS_NET_RO = D2DBIT(1)  // this variable cannot be changed while network game is going on
};

extern int SCRW, SCRH;
extern int SCRWorig, SCRHorig;

extern int gammaa;


// original tick time is ~18.2, so we will use 55: 1000/55 ~= 18.(18)
//#define DELAY  (50)
#define DELAY  (55)

extern void conlogdots (int cur, int total);


static inline uint32_t hashInt32 (uint32_t a) {
  a -= (a<<6);
  a ^= (a>>17);
  a -= (a<<9);
  a ^= (a<<4);
  a -= (a<<3);
  a ^= (a<<10);
  a ^= (a>>15);
  return a;
}


////////////////////////////////////////////////////////////////////////////////
#define K8MIN(__a,__b)  ({typeof(__a)_a = (__a); typeof(__b)_b = (__b); _a < _b ? _a : _b; })
#define K8MAX(__a,__b)  ({typeof(__a)_a = (__a); typeof(__b)_b = (__b); _a > _b ? _a : _b; })


#define _GCC_TOKENPASTE_(x, y)   x ## y
#define _GCC_TOKENPASTE2_(x, y)  _GCC_TOKENPASTE_(x, y)

#define _GCC_UNIQUE_CTOR_(_prio)  \
  static __attribute__((constructor(_prio))) void _GCC_TOKENPASTE2_(__module__constructor__,__COUNTER__) (void)

#define _GCC_UNIQUE_DTOR_(_prio)  \
  static __attribute__((destructor(_prio))) void _GCC_TOKENPASTE2_(__module__destructor__,__COUNTER__) (void)


#define GCC_CONSTRUCTOR(_prio)  _GCC_UNIQUE_CTOR_(_prio)
#define GCC_DESTRUCTOR(_prio)   _GCC_UNIQUE_DTOR_(_prio)

#define GCC_CONSTRUCTOR_SYSTEM_PRIORITY  2048
#define GCC_CONSTRUCTOR_USER_PRIORITY    4096

#define GCC_CONSTRUCTOR_SYSTEM  GCC_CONSTRUCTOR(GCC_CONSTRUCTOR_SYSTEM_PRIORITY)
#define GCC_CONSTRUCTOR_USER    GCC_CONSTRUCTOR(GCC_CONSTRUCTOR_USER_PRIORITY)

#define GCC_DESTRUCTOR_SYSTEM  GCC_DESTRUCTOR(GCC_CONSTRUCTOR_SYSTEM_PRIORITY)
#define GCC_DESTRUCTOR_USER    GCC_DESTRUCTOR(GCC_CONSTRUCTOR_USER_PRIORITY)


#define if_expect(_cond_, _val_)  if (__builtin_expect((_cond_), (_val_)))


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  uint8_t sign[4]; // 'WAD2'
  uint32_t dircount;
  uint32_t dirofs;
} WAD2Header;


typedef struct GCC_PACKED {
  uint32_t offset;
  uint32_t dsize; // disk size
  uint32_t size; // uncompressed size
  uint8_t type; // type of entry
  uint8_t comp; // compression: 0==none, 'H'==huffman
  uint16_t unused;
  char name[16];
  uint8_t zero; // should be set to 0 to allow string operations on `name`; not in WAD
} WAD2DirEntry;


#define WAD2DESIZE    (32)
#define WAD2SIGN      "WAD2"
#define WAD2SIGNSIZE  (4)


////////////////////////////////////////////////////////////////////////////////
// by Bob Jenkins
// public domain
// http://burtleburtle.net/bob/rand/smallprng.html
////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  uint32_t a, b, c, d;
} BJRandCtx;


extern uint32_t bjprngRand (BJRandCtx *x);
extern void bjprngInit (BJRandCtx *x, uint32_t seed);


extern BJRandCtx g_prng;
extern BJRandCtx g_prng1;


extern void myrandomize (void);
extern void xrandomize (BJRandCtx *x);

#define myrand(a)        (bjprngRand(&g_prng)%(a))
#define myrandbit()      (bjprngRand(&g_prng)&0x01)
#define myrandtwobits()  (bjprngRand(&g_prng)&0x03)
#define myrandmask(_m)   (bjprngRand(&g_prng)&(_m))

#define myrand1(a)        (bjprngRand(&g_prng1)%(a))
#define myrandbit1()      (bjprngRand(&g_prng1)&0x01)
#define myrandtwobits1()  (bjprngRand(&g_prng1)&0x03)
#define myrandmask1(_m)   (bjprngRand(&g_prng1)&(_m))


extern int PL_JUMP;
extern int PL_RUN;


#define CMDCON_CHECK_INGAME()  do { \
  if (g_st != GS_GAME) { \
    conlogf("command '%s': 'game' mode required\n", argv[0]); \
    return; \
  } \
} while (0)

#define CMDCON_CHECK_NONET()  do { \
  if (n_host != NULL) { \
    conlogf("command '%s' not allowed in network mode\n", argv[0]); \
    return; \
  } \
} while (0)


extern int boolval (int argc, char *argv[]);

extern const char *get_bin_dir (void);
extern const char *get_home_dir (void);

extern int xmkdir (const char *name);
extern int fexists (const char *filename);

extern void fputqstr (FILE *fo, const char *str);

extern const char *file_get_ext (const char *fname); // can return NULL
extern const char *file_get_name_only (const char *fname, int *len); // can return NULL


#endif
