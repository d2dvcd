/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "anm.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "files.h"
#include "vga.h"

#include "../libhuff/libhuff.h"


////////////////////////////////////////////////////////////////////////////////
#define SQ      (4)
#define ANIWDT  (320)
#define ANIHGT  (200)


////////////////////////////////////////////////////////////////////////////////
enum {
  AB_END,
  AB_SCREEN,
  AB_UPDATE
};


typedef struct GCC_PACKED {
  uint16_t type;
  int16_t st;
  int32_t len;
  uint8_t data[0];
} anm_blk_t;


////////////////////////////////////////////////////////////////////////////////
static anm_blk_t *anm = NULL;
static int anm_size;
static void *rsrc = NULL;
static HuffmanDState *huf = NULL;
static const uint8_t *ubuf;
static int bytes_left;


////////////////////////////////////////////////////////////////////////////////
static int get_bytes (void *buf, int len) {
  int res = -1;
  //
  //fprintf(stderr, "get_bytes: len=%d; left=%d\n", len, bytes_left);
  if (huf != NULL) {
    if ((res = huffman_ds_get_bytes(huf, buf, len)) != len) res = -1;
  } else {
    if (len <= bytes_left) {
      memcpy(buf, ubuf, len);
      ubuf += len;
      bytes_left -= len;
      res = len;
    }
  }
  //fprintf(stderr, "get_bytes: res=%d\n", res);
  //
  return (res >= 0 ? res : -1);
}


////////////////////////////////////////////////////////////////////////////////
static uint8_t cnum[256];


static void init_cpack (void) {
  for (int f = 0; f < 256; ++f) cnum[f] = f;
}


static inline uint8_t cpack (uint8_t n) {
  uint8_t c = cnum[n];
  //
  if (n) memmove(cnum+1, cnum, n);
  cnum[0] = c;
  //
  return c;
}


static const uint8_t *unpack (int x, int y, const uint8_t *s, int l, int *bleft) {
  //
  void putpix (uint8_t c) {
    V_putpixel(x+(SCRW-ANIWDT)/2, y+(SCRH-ANIHGT)/2, c);
    if ((++x) >= ANIWDT) { x = 0; ++y; }
  }
  //
  y += (x/ANIWDT);
  x %= ANIWDT;
  for (; y < ANIHGT && l > 0 && *bleft > 0; --l, ++s) {
    uint8_t c = cpack(*s);
    //
    --(*bleft);
    if (s[0] == 0) {
      int cnt = *(++s);
      //
      if (*bleft < 1) break;
      --(*bleft);
      l -= cnt;
      while (cnt-- >= 0) putpix(c);
    } else {
      putpix(c);
    }
  }
  //
  return s;
}


static const uint8_t *line (int y, const uint8_t *u, int *bleft) {
  for (int x = 0; x < ANIWDT/SQ && *bleft > 0; ) {
    --(*bleft);
    if (u[0]&0x80) {
      int n = ((*u++)&0x7F)+1;
      //
      for (int sy = 0; sy < SQ; ++sy) u = unpack(x*SQ, y+sy, u, n*SQ, bleft);
      x += n;
    } else {
      x += (*u++)+1;
    }
  }
  //
  return u;
}


static int huff_read (void *buf, int len, void *udata) {
  if (len > bytes_left) len = bytes_left;
  if (len > 0) {
    memcpy(buf, ubuf, len);
    ubuf += len;
    bytes_left -= len;
  }
  return len;
}


static const HuffmanIO hio = {
  .read = huff_read
};


static void anm_close (void) {
  if (huf != NULL) { huffman_decode_done(huf); huf = NULL; }
  if (anm != NULL) { free(anm); anm = NULL; }
  anm_size = 0;
  if (rsrc != NULL) { M_unlock(rsrc); rsrc = NULL; }
  ubuf = NULL;
  bytes_left = 0;
}


void ANM_close (void) {
  anm_close();
}


void ANM_start (const char *name) {
  int idx, len;
  anm_close();
  //
  init_cpack();
  //
  if ((idx = F_findres(name)) < 0) {
    conlogf("NO ANM: '%s'", name);
    return;
  }
  //
  len = F_getreslen(idx);
  if (len < sizeof(anm_blk_t)) {
    conlogf("INVALID ANM: '%s'", name);
    return;
  }
  //
  rsrc = M_lock(idx);
  ubuf = (const uint8_t *)rsrc;
  bytes_left = len;
  //
  if (rsrc != NULL) {
    if (memcmp(ubuf, "HUF\0", 4) == 0) {
      ubuf += 4;
      bytes_left -= 4;
      //
      if ((huf = huffman_decode_start(&hio, NULL)) == NULL) {
        conlogf("INVALID PACKED ANM: '%s'", name);
        M_unlock(rsrc);
        anm_close();
        return;
      }
      conlogf("ANM-HUF: '%s'", name);
    } else {
      conlogf("ANM: '%s'", name);
    }
    //memcpy(oldscreen->pixels, screen->pixels, oldscreen->pitch*oldscreen->h);
    memset(oldscreen->pixels, 0, oldscreen->pitch*oldscreen->h);
  } else {
    conlogf("NO ANM: '%s'", name);
  }
}


int ANM_nextframe (void) {
  const uint8_t *u;
  anm_blk_t aa;
  int bleft;
  //
  //if (anm != NULL) dlogf("anm=%p; len=%u; type=%d; st=%d", anm, anm->len, anm->type, anm->st);
  //
  if (rsrc == NULL) return 0;
  //
  if (get_bytes(&aa, sizeof(aa)) < 0 || aa.len < 0 || aa.len > 1024*1024*8) {
    ANM_close();
    conlogf("ANM: read error or invalid ANM!\n");
    return 0;
  }
  //
  if (aa.type == AB_END) {
    ANM_close();
    return 0;
  }
  //
  if (anm_size < sizeof(aa)+aa.len) {
    int newsz = sizeof(aa)+aa.len;
    anm_blk_t *nb = realloc(anm, newsz);
    //
    if (nb == NULL) {
      ANM_close();
      conlogf("ANM: out of memory!\n");
      return 0;
    }
    anm_size = newsz;
    anm = nb;
  }
  //
  memcpy(anm, &aa, sizeof(aa));
  if (get_bytes(anm->data, anm->len) < 0) {
    ANM_close();
    conlogf("ANM: read error!\n");
    return 0;
  }
  //
  memcpy(screen->pixels, oldscreen->pixels, oldscreen->pitch*oldscreen->h);
  u = (const uint8_t *)(&anm->data);
  bleft = anm->len;
  //
  switch (anm->type) {
    case AB_SCREEN:
      unpack(0, 0, u, 64000, &bleft);
      break;
    case AB_UPDATE:
      for (int y = 0; y < ANIHGT/SQ && bleft > 0; ) {
        --bleft;
        if (u[0]&0x80) {
          for (int x = ((*u++)&0x7F)+1; x != 0 && bleft > 0; --x, ++y) u = line(y*SQ, u, &bleft);
        } else {
          y += *(u++)+1;
        }
      }
      break;
    default:
      ANM_close();
      conlogf("ANM: invalid ANM frame type!");
      return 0;
  }
  memcpy(oldscreen->pixels, screen->pixels, oldscreen->pitch*oldscreen->h);
  //
  return 1;
}
