/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "mapio.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>

#include "error.h"
#include "common.h"
#include "cmdcon.h"
#include "files.h"
#include "items.h"
#include "map.h"
#include "misc.h"
#include "switch.h"
#include "vga.h"
#include "view.h"


////////////////////////////////////////////////////////////////////////////////
static const char *blknames[] = {
  "END",
  "WALLNAMES",
  "BACK",
  "WTYPE",
  "FRONT",
  "THING",
  "SWITCH",
  "MUSIC",
  "SKY",
  "SWITCH2",
};


static const char *get_mapblock_name (uint16_t type) {
  static char bn[64];
  //
  if (type < sizeof(blknames)/sizeof(blknames[0])) return blknames[type];
  if (type == MB_COMMENT) return "COMMENT";
  sprintf(bn, "<UNKNOWN:%u>", type);
  return bn;
}


////////////////////////////////////////////////////////////////////////////////
#define MAX_MAP_LOADERS (64)


typedef struct {
  uint16_t type;
  F_maploadCB lcb;
  F_mapsaveCB scb;
} MapBlockLoader;

static MapBlockLoader maploadCB[MAX_MAP_LOADERS];
static int maploaderCount = 0;


void F_registerMapLoad (uint16_t type, F_maploadCB lcb, F_mapsaveCB scb) {
  if (maploaderCount >= MAX_MAP_LOADERS) ERR_failinit("too many map loaders\n");
  maploadCB[maploaderCount].type = type;
  maploadCB[maploaderCount].lcb = lcb;
  maploadCB[maploaderCount].scb = scb;
  ++maploaderCount;
}


////////////////////////////////////////////////////////////////////////////////
static ccBool dbg_dump_map_loading = CC_FALSE;
CONVAR_BOOL(dbg_dump_map_loading, dbg_dump_map_loading, 0, "show map loader debug messages")


////////////////////////////////////////////////////////////////////////////////
int F_loadmap (const char *name) {
  char nm[9];
  map_header_t hdr;
  MemBuffer *mbuf;
  int res;
  //
  if (name == NULL || !name[0] || strlen(name) > 8) {
    conlogf("F_loadmap: invalid map name!\n");
    return -1;
  }
  //
  if (setjmp(flJmpBuf) != 0) {
    conlogf("F_loadmap: map loading error!\n");
    return -1;
  }
  //
  memset(nm, 0, sizeof(nm));
  strncpy(nm, name, 8);
  //
  if ((res = F_findres(nm)) < 0) {
    conlogf("%s: not found!", nm);
    return -1;
  }
  //
  if ((mbuf = F_loadres2buf(res)) == NULL) {
    conlogf("%s: not found", name);
    return -1;
  }
  //
  if (membuf_read_full(mbuf, &hdr, sizeof(hdr)) < 0 || memcmp(hdr.id, "Doom2D\x1A", 8) != 0) {
    membuf_free(mbuf);
    conlogf("%s: not a level", name);
    return -1;
  }
  //
  if (hdr.ver != MAP_CURRENT_VERSION) {
    membuf_free(mbuf);
    conlogf("%s: invalid level version: %u", name, hdr.ver);
    return -1;
  }
  //
  W_set_field_size(100, 100);
  W_init();
  res = -1;
  //
  for (;;) {
    map_block_t blk;
    int found = 0;
    //
    if (membuf_used(mbuf) == membuf_size(mbuf)) { res = 0; break; }
    //
    if (membuf_read_full(mbuf, &blk, sizeof(blk)) < 0) {
      conlogf("%s: level loading error (block)", name);
      break;
    }
    //
    if (dbg_dump_map_loading) conlogf("%s: map block (%s:%u); size=%u; pos=%u", name, get_mapblock_name(blk.t), blk.st, blk.sz, membuf_used(mbuf)-sizeof(blk));
    if (blk.t == MB_END) { res = 0; break; }
    if (blk.sz > 1024*1024*8) break;
    if (membuf_used(mbuf)+blk.sz > membuf_size(mbuf)) {
      conlogf("%s: map block too big (%s:%u)", name, get_mapblock_name(blk.t), blk.st);
      break;
    }
    //
    for (int f = 0; f < maploaderCount; ++f) {
      if (blk.t == maploadCB[f].type && maploadCB[f].lcb != NULL) {
        MemBuffer *bbuf = membuf_new_read(((const uint8_t *)membuf_data_const(mbuf))+membuf_used(mbuf), blk.sz);
        int r;
        //
        found = 1;
        if (bbuf == NULL) break;
        r = (maploadCB[f].lcb)(bbuf, &blk);
        membuf_free(bbuf);
        if (r < 0) conlogf("%s: error loading map block (%s:%u)", name, get_mapblock_name(blk.t), blk.st);
        //
        break;
      }
    }
    //
    if (!found && blk.t != MB_COMMENT) conlogf("%s: skipped block (%s:%u)", name, get_mapblock_name(blk.t), blk.st);
    membuf_read_skip(mbuf, blk.sz);
  }
  //
  membuf_free(mbuf);
  //
  if (res == 0) {
    // do postprocessing
    for (int f = 0; f < maploaderCount; ++f) {
      if (maploadCB[f].lcb != NULL && maploadCB[f].lcb(NULL, NULL) < 0) { res = -1; break; }
    }
  }
  //
  if (res < 0) conlogf("%s: level loading error", name);
  //
  return res;
}
