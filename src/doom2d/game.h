/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_GAME_H
#define D2D_GAME_H

#include "common.h"
#include "misc.h"
#include "netgame.h"


// current game state
enum {
  GS_NONE, // for menus
  //
  GS_TITLE, // showing title screen
  GS_GAME, // playing the game, local or network
  GS_INTER, // inter-level intermission screen
  GS_ENDANIM, // playing ENDANIM (falling robot)
  GS_DARKEN, // darken the screen after ENDANIM
  GS_END2ANIM, // playing END2ANIM (KONEC)
  GS_ENDSCR, // showing 'game complete' screen
  //
  GS_WAITING_CONNECTION,
  GS_NETINTER, // COOPERATIVE, level completed
  //
  GS_MAXSTATE
};


enum {
  GNETS_HANDSHAKING,
  GNETS_PLAYING,
  // for GS_NETINTER
  //
  GNETS_DUMMY_YUMMY
};

extern int gnet_state; // GNETS_XXX

extern int g_frame_advanced;
extern int g_edit_mode;
extern int g_frame_ticks_start;
extern ccBool r_interpolate_panning;

extern uint8_t cli_warp;
extern int g_plrcount; // 1 or 2
extern ccBool g_dm;
extern d2dLevelExit g_exit;
extern uint8_t g_st;
extern uint8_t g_map;
extern char g_music[8];
extern uint32_t g_time;
extern int dm_pnum, dm_pl1p, dm_pl2p;
extern pos_t dm_pos[100];
extern uint8_t pl_action_order;
extern int lt_time, lt_type, lt_side, lt_ypos, lt_force;
extern int g_trans, g_transt;


extern void G_end (void);

extern void G_reset_cheats (void);


// <0: error
//  0: continue processing
// >0: no more packets for this frame
extern int G_process_enet_game_packet (ENetPacket *pkt);

extern int G_process_enet_connect (void);
extern int G_process_enet_disconnect (void);

extern void G_send_inter_fired (void);


#endif
