/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include <string.h>
#include <malloc.h>

#include "map_jimapi.h"

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "items.h"
#include "jimapi.h"
#include "map.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "sdldrv.h"
#include "smoke.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"
#include "widgets.h"


////////////////////////////////////////////////////////////////////////////////
// map width [newwidth]
JIMAPI_FN_NOREG(_map_width_) {
  if (argc > 2) {
    long val;
    //
    if (Jim_GetLong(interp, argv[2], &val) != JIM_OK) {
      jim_SetResStrf(interp, "%s: new map width expected", Jim_String(argv[0]));
      return JIM_ERR;
    }
    if (val < 16 || val > 32767) {
      jim_SetResStrf(interp, "%s: invalid map width: %d", Jim_String(argv[0]), (int)val);
      return JIM_ERR;
    }
    //
    W_set_field_size(val, FLDH);
  }
  //
  Jim_SetResultInt(interp, FLDW);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// map height [newheight]
JIMAPI_FN_NOREG(_map_height_) {
  if (argc > 2) {
    long val;
    //
    if (Jim_GetLong(interp, argv[2], &val) != JIM_OK) {
      jim_SetResStrf(interp, "%s: new map height expected", Jim_String(argv[0]));
      return JIM_ERR;
    }
    if (val < 16 || val > 32767) {
      jim_SetResStrf(interp, "%s: invalid map height: %d", Jim_String(argv[0]), (int)val);
      return JIM_ERR;
    }
    //
    W_set_field_size(FLDW, val);
  }
  //
  Jim_SetResultInt(interp, FLDH);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
// xxx xxx [bg|fg|wl] x y
static int _mapfld_ (Jim_Interp *interp, int argc, Jim_Obj *const *argv, int doset) {
  const char *s;
  uint8_t *fld = NULL;
  int type = -1, apos;
  long x, y, v;
  //
  if (argc < 4+doset || argc > 5+doset) {
    Jim_WrongNumArgs(interp, argc, argv, "map");
    return JIM_ERR;
  }
  //
  s = Jim_String(argv[2]);
  if (s[0] == 'b') type = 0;
  else if (s[0] == 'f') type = 1;
  else if (s[0] == 'w') type = 2;
  //
  apos = (type >= 0 ? 3 : 2);
  //
  if (apos >= argc || Jim_GetLong(interp, argv[apos], &x) != JIM_OK || x < 0 || x >= FLDW) {
    jim_SetResStrf(interp, "%s %s: map x coord expected", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }
  //
  ++apos;
  if (apos >= argc || Jim_GetLong(interp, argv[apos], &y) != JIM_OK || y < 0 || y >= FLDH) {
    jim_SetResStrf(interp, "%s %s: map x coord expected", Jim_String(argv[0]), Jim_String(argv[1]));
    return JIM_ERR;
  }
  //
  switch (type) {
    case 0: fld = &FIELD_TBG(x, y); break;
    case 1: fld = &FIELD_TFG(x, y); break;
    default: fld = &FIELD_MAP(x, y); break;
  }
  //
  if (doset) {
    ++apos;
    if (apos >= argc || Jim_GetLong(interp, argv[apos], &v) != JIM_OK || v < 0 || v > 255) {
      jim_SetResStrf(interp, "%s %s: new map value expected", Jim_String(argv[0]), Jim_String(argv[1]));
      return JIM_ERR;
    }
    *fld = v;
    BM_remapfld(0);
  }
  //
  Jim_SetResultInt(interp, *fld);
  return JIM_OK;
}


// map get [bf|fg|wl] x y
JIMAPI_FN_NOREG(_map_get_) {
  return _mapfld_(interp, argc, argv, 0);
}


// map set [bf|fg|wl] x y val
JIMAPI_FN_NOREG(_map_set_) {
  return _mapfld_(interp, argc, argv, 1);
}


////////////////////////////////////////////////////////////////////////////////
static const char *const map_command_names[] = {
  "width",
  "height",
  "get",
  "set",
  NULL
};
static Jim_CmdProc map_command_handlers[] = {
  JIMAPI_NAME(_map_width_),
  JIMAPI_NAME(_map_height_),
  JIMAPI_NAME(_map_get_),
  JIMAPI_NAME(_map_set_),
};


// map command [args]
JIMAPI_FN(map) {
  int cidx;
  //
  if (argc < 2) {
    Jim_WrongNumArgs(interp, argc, argv, "map command [args]");
    return JIM_ERR;
  }
  //
  if (Jim_GetEnum(interp, argv[1], map_command_names, &cidx, "map method", JIM_ERRMSG) == JIM_OK) {
    return (map_command_handlers[cidx])(interp, argc, argv);
  }
  jim_SetResStrf(interp, "%s: invalid command: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
  return JIM_ERR;
}


////////////////////////////////////////////////////////////////////////////////
// editor active [newstate]
JIMAPI_FN_NOREG(_editor_active_) {
  if (argc > 2) {
    long val;
    //
    if (Jim_GetLong(interp, argv[2], &val) != JIM_OK) {
      jim_SetResStrf(interp, "%s: new editor state expected", Jim_String(argv[0]));
      return JIM_ERR;
    }
    cmdcon_exec(val ? "g_edit_mode 1" : "g_edit_mode 0");
  }
  //
  Jim_SetResultInt(interp, g_edit_mode);
  return JIM_OK;
}


////////////////////////////////////////////////////////////////////////////////
static const char *const editor_command_names[] = {
  "active",
  NULL
};
static Jim_CmdProc editor_command_handlers[] = {
  JIMAPI_NAME(_editor_active_),
};


// editor command [args]
JIMAPI_FN(editor) {
  int cidx;
  //
  if (argc < 2) {
    Jim_WrongNumArgs(interp, argc, argv, "editor command [args]");
    return JIM_ERR;
  }
  //
  if (Jim_GetEnum(interp, argv[1], editor_command_names, &cidx, "editor method", JIM_ERRMSG) == JIM_OK) {
    return (editor_command_handlers[cidx])(interp, argc, argv);
  }
  jim_SetResStrf(interp, "%s: invalid command: '%s'", Jim_String(argv[0]), Jim_String(argv[1]));
  return JIM_ERR;
}
