/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "switch.h"

#include <string.h>

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "error.h"
#include "files.h"
#include "game.h"
#include "map.h"
#include "mapio.h"
#include "misc.h"
#include "player.h"
#include "view.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_REACTORS  (1000)


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  uint8_t x, y;
  uint8_t t, tm;
  uint8_t a, b, c, d;
  uint8_t f;
} sw_t;


////////////////////////////////////////////////////////////////////////////////
static sw_t sw[MAX_REACTORS];
static int swused = 0;

static const snd_t *sndswn, *sndswx, *sndnoway, *sndbdo, *sndbdc, *sndnotele;
static int swsnd;
static uint8_t cht, chto, chf, f_ch;

int sw_secrets;


////////////////////////////////////////////////////////////////////////////////
void SW_alloc (void) {
  sndswn = Z_getsnd("SWTCHN");
  sndswx = Z_getsnd("SWTCHX");
  sndnoway = Z_getsnd("NOWAY");
  sndbdo = Z_getsnd("BDOPN");
  sndbdc = Z_getsnd("BDCLS");
  sndnotele = Z_getsnd("NOTELE");
}


void SW_init (void) {
  for (int i = 0; i < MAX_REACTORS; ++i) sw[i].t = 0;
  swsnd = 0;
  swused = 0;
  sw_secrets = 0;
}


static void door (uint8_t x, uint8_t y) {
  uint8_t ex;
  //
  if (x >= FLDW || y >= FLDH) return;
  if (FIELD_MAP(x, y) != cht) return;
  ex = x+1;
  for (; x && FIELD_MAP(x-1, y) == cht; --x) ;
  for (; ex < FLDW && FIELD_MAP(ex, y) == cht; ++ex) ;
  memset(fldm+y*FLDW+x, chto, ex-x);
  if (f_ch) memset(fldf+y*FLDW+x, chf, ex-x);
  for (; x < ex; ++x) {
    door(x, y-1);
    door(x, y+1);
  }
}


void Z_water_trap (const obj_t *o) {
  int sx, sy, x, y;
  //
  if ((y = o->y) >= FLDH*CELH+o->h) return;
  if ((x = o->x) < 0 || o->x > FLDW*CELW) return;
  sx = (x-o->r)/CELW;
  sy = (y-o->h+1)/CELH;
  x = (x+o->r)/CELW;
  y = (y-o->h/2)/CELH;
  for (int i = sx; i <= x; ++i) {
    for (int j = sy; j <= y; ++j) {
      if (FIELD_MAP(i, j) == TILE_WATER) {
        cht = 5;
        chto = 255;
        f_ch = 0;
        door(i, j);
      }
    }
  }
}


void Z_untrap (uint8_t t) {
  int n = FLDW*FLDH;
  //
  for (uint8_t *p = (uint8_t *)fldm; n; --n, ++p) if (*p == TILE_ACTTRAP) *p = t;
}


static void opendoor (int i) {
  int j;
  //
  swsnd = Z_sound(sndbdo, 128);
  j = FIELD_TFG(sw[i].a, sw[i].b);
  cht = 2;
  chto = 3;
  chf = 0;
  f_ch = 1;
  door(sw[i].a, sw[i].b);
  FIELD_TFG(sw[i].a, sw[i].b) = j;
  BM_queue_remap();
}


static int shutdoor (int i) {
  int j;
  //
  cht = 3;
  chto = 255;
  chf = FIELD_TFG(sw[i].a, sw[i].b);
  f_ch = 1;
  door(sw[i].a, sw[i].b);
  cht = 255;
  if (Z_chktrap(0, 0, -3, HIT_SOME)) {
    j = FIELD_TFG(sw[i].a, sw[i].b);
    chto = 3;
    chf = 0;
    f_ch = 1;
    door(sw[i].a, sw[i].b);
    FIELD_TFG(sw[i].a, sw[i].b) = j;
    return 0;
  }
  chto = 2;
  door(sw[i].a, sw[i].b);
  BM_queue_remap();
  swsnd = Z_sound(sndbdc, 128);
  return 1;
}


void SW_act (void) {
  if (swsnd) --swsnd;
  for (int i = 0; i < swused; ++i) {
    if (sw[i].t) {
      if (sw[i].tm) --sw[i].tm;
      //
      switch (sw[i].t) {
        case SW_DOOR5:
        case SW_DOOR:
        case SW_SHUTDOOR:
          if (!sw[i].d) break;
          if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_DOORO) {
            sw[i].d = 0;
            break;
          }
          if (--sw[i].d == 0 && !shutdoor(i)) sw[i].d = 9;
          break;
        case SW_TRAP:
          if (!sw[i].d) break;
          if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_DOORC) {
            sw[i].d = 0;
            break;
          }
          if (--sw[i].d == 0) {
            opendoor(i);
            sw[i].tm = 18;
          }
          break;
      }
    }
  }
}


static inline int doortime (int t) {
  return (t == SW_DOOR5 ? 90 : 0);
}


void SW_cheat_open (void) {
  for (int i = 0; i < swused; ++i) {
    if (sw[i].t && !sw[i].tm) {
      switch (sw[i].t) {
        case SW_DOOR:
        case SW_DOOR5:
        case SW_OPENDOOR:
          if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_DOORC) break;
          SW_press(sw[i].x*CELW+4, sw[i].y*CELH+4, 1, 1, 0xFF, -3);
          break;
      }
    }
  }
}


int SW_press (int x, int y, int r, int h, uint8_t t, int o) {
  int sx, sy, res = 0;
  //
  sx = (x-r)/CELW;
  sy = (y-h+1)/CELH;
  x = (x+r)/CELW;
  y /= CELH;
  for (int i = 0; i < swused; ++i) {
    if (sw[i].t && !sw[i].tm) {
      if (sw[i].x >= sx && sw[i].x <= x && sw[i].y >= sy && sw[i].y <= y && ((sw[i].f&0x8F)&t)) {
        if (sw[i].f&0x70 && (sw[i].f&(t&0x70)) != (sw[i].f&0x70)) continue;
        switch (sw[i].t) {
          case SW_EXIT:
            g_exit = D2D_LEVEL_EXIT_NORMAL;
            sw[i].tm = 9;
            swsnd = Z_sound(sndswx, 128);
            break;
          case SW_EXITS:
            g_exit = D2D_LEVEL_EXIT_SECRET;
            sw[i].tm = 9;
            swsnd = Z_sound(sndswx, 128);
            break;
          case SW_WINGAME:
            g_exit = D2D_LEVEL_EXIT_WINGAME;
            sw[i].tm = 9;
            swsnd = Z_sound(sndswx, 128);
            break;
          case SW_DOOR:
          case SW_DOOR5:
            switch (FIELD_MAP(sw[i].a, sw[i].b)) {
              case TILE_DOORC:
                opendoor(i);
                sw[i].tm = 9;
                sw[i].d = doortime(sw[i].t);
                break;
              case TILE_DOORO:
                if (shutdoor(i)) {
                  sw[i].tm = 9;
                  sw[i].d = 0;
                } else {
                  if (!swsnd) swsnd = Z_sound(sndnoway, 128);
                  sw[i].d = 2;
                }
                break;
            }
            break;
          case SW_PRESS:
            sw[i].tm = 9;
            SW_press((uint32_t)sw[i].a*8+4, (uint32_t)sw[i].b*8+12, 8, 16, (t&0x70)|0x80, o);
            break;
          case SW_TELE:
            if (o < -2) break;
            if (!Z_canfit(OBJT_NONE, (uint32_t)sw[i].a*8+4, (uint32_t)sw[i].b*8+7, r, h)) {
              if (!swsnd) swsnd = Z_sound(sndnotele, 128);
              break;
            }
            Z_teleobj(o, (uint32_t)sw[i].a*8+4, (uint32_t)sw[i].b*8+7);
            sw[i].tm = 1;
            break;
          case SW_OPENDOOR:
            if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_DOORC) break;
            opendoor(i);
            sw[i].tm = 1;
            break;
          case SW_SHUTDOOR:
            if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_DOORO) break;
            if (shutdoor(i)) {
              sw[i].tm = 1;
              sw[i].d = 0;
            } else {
              if (!swsnd) swsnd = Z_sound(sndnoway, 128);
              sw[i].d = 2;
            }
            break;
          case SW_SHUTTRAP:
          case SW_TRAP:
            if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_DOORO) break;
            cht = TILE_DOORO;
            chto = TILE_ACTTRAP;
            chf = FIELD_TFG(sw[i].a, sw[i].b);
            f_ch = 1;
            door(sw[i].a, sw[i].b);
            Z_chktrap(1, 100, -3, HIT_TRAP);
            cht = TILE_ACTTRAP;
            chto = TILE_DOORC;
            door(sw[i].a, sw[i].b);
            BM_queue_remap();
            swsnd = Z_sound(sndswn, 128);
            sw[i].tm = 1;
            sw[i].d = 20;
            break;
          case SW_LIFT:
            if (FIELD_MAP(sw[i].a, sw[i].b) == TILE_LIFTD) {
              cht = TILE_LIFTD;
              chto = TILE_LIFTU;
              f_ch = 0;
            } else if (FIELD_MAP(sw[i].a, sw[i].b) == TILE_LIFTU) {
              cht = TILE_LIFTU;
              chto = TILE_LIFTD;
              f_ch = 0;
            } else {
              break;
            }
            door(sw[i].a, sw[i].b);
            BM_queue_remap();
            swsnd = Z_sound(sndswx, 128);
            sw[i].tm = 9;
            break;
          case SW_LIFTUP:
            if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_LIFTD) break;
            cht = TILE_LIFTD;
            chto = TILE_LIFTU;
            f_ch = 0;
            door(sw[i].a, sw[i].b);
            BM_queue_remap();
            swsnd = Z_sound(sndswx, 128);
            sw[i].tm = 1;
            break;
          case SW_LIFTDOWN:
            if (FIELD_MAP(sw[i].a, sw[i].b) != TILE_LIFTU) break;
            cht = TILE_LIFTU;
            chto = TILE_LIFTD;
            f_ch = 0;
            door(sw[i].a, sw[i].b);
            BM_queue_remap();
            swsnd = Z_sound(sndswx, 128);
            sw[i].tm = 1;
            break;
          case SW_SECRET:
            if (o != -1 && o != -2) break;
            if (o == -1) ++pl[0].secrets; else ++pl[1].secrets;
            sw[i].tm = 1;
            sw[i].t = 0;
            break;
        }
        if (sw[i].tm) {
          FIELD_TBG(sw[i].x, sw[i].y) = walswp[FIELD_TBG(sw[i].x, sw[i].y)];
          res = 1;
        }
        if (sw[i].tm == 1) sw[i].tm = 0;
      }
    }
  }
  //
  return res;
}


void SW_draw (void) {
}


////////////////////////////////////////////////////////////////////////////////
static int SW_mapload (MemBuffer *mbuf, const map_block_t *blk) {
  if (mbuf == NULL) return 0; // no post-processing
  //
  int sz = blk->sz;
  //
  sw_secrets = 0;
  for (swused = 0; sz > 0; ++swused, sz -= sizeof(sw_t)) {
    if (swused >= MAX_REACTORS) {
      conlogf("FATAL: too many switches on level");
      return -1;
    }
    if (membuf_read_full(mbuf, &sw[swused], sizeof(sw_t)) < 0) return -1;
    sw[swused].tm = 0;
    sw[swused].d = 0;
    sw[swused].f |= 0x80;
    if (sw[swused].t == SW_SECRET) ++sw_secrets;
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static int SW_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(sw[0])); // structure size
    membuf_write_ui16(mbuf, sw_secrets);
    membuf_write_ui16(mbuf, swused);
    for (int f = 0; f < swused; ++f) membuf_write(mbuf, &sw[f], sizeof(sw[0]));
  }
  return 0;
}


static int SW_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    SW_init();
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'switches' version!\n");
      return -1;
    }
    if (membuf_read_ui16(mbuf) != sizeof(sw[0])) {
      conlogf("LOADGAME ERROR: invalid 'switches' size!\n");
      return -1;
    }
    sw_secrets = membuf_read_ui16(mbuf);
    swused = membuf_read_ui16(mbuf);
    if (swused > MAX_REACTORS) {
      conlogf("LOADGAME ERROR: too many switches!\n");
      return -1;
    }
    if (sw_secrets > swused) {
      conlogf("LOADGAME ERROR: too many secret switches!\n");
      return -1;
    }
    if (membuf_read_full(mbuf, sw, swused*sizeof(sw[0])) < 0) {
      conlogf("LOADGAME ERROR: ca't read switches!\n");
      return -1;
    }
  }
  //
  if (waserror) SW_init();
  return 0; // we are optional anyway
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  F_registerMapLoad(MB_SWITCH2, SW_mapload, NULL);
  F_registerSaveLoad("REACTORS", SW_savegame, SW_loadgame, SAV_NORMAL);
}
