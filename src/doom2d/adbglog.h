/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
/* debug logs */
#ifndef D2D_DBGLOG_H
#define D2D_DBGLOG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>


typedef void (*DbgLogUpdateFn) (void);
extern DbgLogUpdateFn dbgLogPostUpdate;
extern DbgLogUpdateFn dbgLogPreUpdate;


/*
#ifdef _WIN32
# define NO_DEBUG_LOG
#endif
*/

/* defaults: write to file; don't write to stderr */

extern void dbgLock (void);
extern void dbgUnlock (void);
extern void dbgLogSetFileName (const char *fname);
extern int dbgSetScreenOut (int doIt); /* returns previous value */
extern int dbgLogSetFileOut (int doIt); /* returns previous value */
#ifndef NO_DEBUG_LOG
extern void dlogf (const char *fmt, ...) __attribute__((format(printf, 1, 2)));
extern void dlogfVA (const char *fmt, va_list ap);
#else
# define dlogf(fmt,...)          ((void)0)
# define dlogfVA(fmt,ap)         ((void)0)
#endif


#ifdef __cplusplus
}
#endif
#endif
