/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Player data and functions
#ifndef D2D_PLAYER_H
#define D2D_PLAYER_H

#include "common.h"
#include "misc.h"


enum {
  PL_DRAWLIFE  = D2DBIT(0),
  PL_DRAWARMOR = D2DBIT(1),
  PL_DRAWWPN   = D2DBIT(2),
  PL_DRAWFRAG  = D2DBIT(3),
  PL_DRAWAIR   = D2DBIT(4),
  PL_DRAWKEYS  = D2DBIT(5),
  PL_DRAWLIVES = D2DBIT(6)
};

enum {
  PLK_U = D2DBIT(0),
  PLK_D = D2DBIT(1),
  PLK_L = D2DBIT(2),
  PLK_R = D2DBIT(3),
  PLK_F = D2DBIT(4),
  PLK_J = D2DBIT(5),
  PLK_P = D2DBIT(6),
  PLK_W = D2DBIT(7)
};

enum {
  PLK_WL = D2DBIT(0),
  PLK_WR = D2DBIT(1)
};

enum {
  PLF_FIRE  = D2DBIT(0),
  PLF_PNSND = D2DBIT(1),
  PLF_UP    = D2DBIT(2),
  PLF_DOWN  = D2DBIT(3)
};

enum {
  PL_POWERUP_TIME = 546
};


// player states
enum {
  PLST_STAND,
  PLST_GO,
  PLST_DIE,
  PLST_SLOP,
  PLST_DEAD,
  PLST_MESS,
  PLST_PLOUT,
  PLST_FALL
};


enum {
  KB_UP,
  KB_DOWN,
  KB_LEFT,
  KB_RIGHT,
  KB_FIRE,
  KB_JUMP,
  KB_WPNPREV,
  KB_WPNNEXT,
  KB_USE,
  // 9 items
  //
  KB_MAX
};


typedef struct GCC_PACKED {
  obj_t o;
  int looky;
  int st, s;
  int life, armor, hit, hito;
  int pain, air;
  int invl, suit;
  char d;
  int frag, ammo, shel, rock, cell, fuel, kills, secrets;
  uint8_t fire, cwpn, csnd;
  uint8_t amul;
  uint16_t wpns;
  char wpn;
  uint8_t f;
  uint8_t drawst;
  uint8_t color;
  int id;
  uint8_t keys;
  char lives;
  int endofsavemark;
  int kb[KB_MAX][2];
  int w_prev_x, w_prev_y;
  int w_new_x, w_new_y;
  int w_first_time;
  //
  //int is_networked;
} player_t;


extern ccBool p_immortal, p_ghost, p_fly;
extern player_t pl[2];


typedef enum {
  DEMO_NONE,
  DEMO_PLAY,
  DEMO_RECORD
} DEMO_MODE;

extern DEMO_MODE demo_mode;
extern void *demo_buf;
extern int demo_buf_pos;
extern int demo_buf_size;
extern BJRandCtx demo_initial_prng;
extern BJRandCtx demo_saved_prng;

//extern int demo_init_record (void);
extern void demo_done (void);
extern void demo_begin_frame (void);
extern int demo_end_frame (void); // 0: demo complete
// <0: error; 0: ok
extern int demo_init_play_fl (FILE *fl);
extern int demo_init_play (const char *fname);

extern uint16_t player_build_keys (int plnum);
extern void player_unpack_keys (int plnum, uint16_t keys);

extern void PL_init (void);
extern void PL_alloc (void);
extern void PL_spawn (player_t *p, int x, int y, char d);
extern int PL_hit (player_t *p, int d, int o, int t);
extern int PL_isdead (const player_t *p);
extern void PL_act (player_t *p);
extern void PL_draw (player_t *p);
extern void PL_drawst (player_t *p);
extern void PL_cry (player_t *p);
extern void PL_damage (player_t *p);
extern int PL_give (player_t *p, int t);
extern void G_respawn_player (player_t *p);

extern void PL_reset (void);
extern vgaimg *PL_getspr (int c, int d);

extern void bfg_fly (int x, int y, int o);


#endif
