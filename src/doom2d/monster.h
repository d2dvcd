/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Monsters
#ifndef D2D_MONSTER_H
#define D2D_MONSTER_H

#include "common.h"
#include "saveload.h"
#include "view.h"


enum {
  MN_NONE,
  MN_DEMON,
  MN_IMP,
  MN_ZOMBY,
  MN_SERG,
  MN_CYBER,
  MN_CGUN,
  MN_BARON,
  MN_KNIGHT,
  MN_CACO,
  MN_SOUL,
  MN_PAIN,
  MN_SPIDER,
  MN_BSP,
  MN_MANCUB,
  MN_SKEL,
  MN_VILE,
  MN_FISH,
  MN_BARREL,
  MN_ROBO,
  MN_MAN,
  //
  MN__LAST,
  MN__FIRST = MN_DEMON,
  MN_TYPES_MAX = MN__LAST-MN__FIRST,
  //
  MN_PL_DEAD = 100,
  MN_PL_MESS,
};


typedef struct GCC_PACKED {
  int r; // radius
  int h; // ht (???)
  int l; // life
  int mp; // pain
  int rv; // rv?
  int jv; // jv?
  int sp; // slop?
  int minp; // min_pn? minimal pain to became re-angry?
} monster_stats_t;


extern int hit_xv, hit_yv;

extern ccBool m_notarget;


extern void MN_init (void);
extern void MN_alloc (void);
extern int MN_spawn (int x, int y, uint8_t d, int t);
extern int MN_spawn_deadpl (obj_t *o, uint8_t c, int t);
extern void MN_act (void);
extern void MN_mark (void);
extern void MN_draw (void);
extern void MN_warning (int l, int t, int r, int b);

extern void MN_killedp (void);

extern int MN_hit (int n, int d, int o, int t);


#endif
