/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#ifndef D2D_NETGAME_H
#define D2D_NETGAME_H

#include "common.h"

#include "enet/enet.h"


#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
# define NETGAME_PROTO_VERSION  (0x00)
#else
# define NETGAME_PROTO_VERSION  (0x80)
#endif


enum {
  ENET_HANDSHAKE_CHANNEL = 0,
  ENET_GAME_CHANNEL,
  ENET_CHAT_CHANNEL,
  //
  ENET_MAX_CHANNELS
};


typedef enum {
  NETPKT_DELTA, // server delta
  NETPKT_ACK // client ack
} NetPacketType;


#define ENET_GAME_PKT_FLAGS  (ENET_PACKET_FLAG_UNSEQUENCED)
//#define ENET_GAME_PKT_FLAGS  (ENET_PACKET_FLAG_RELIABLE)


extern ccBool n_enet_initialized;
extern ccBool n_network_meserver;
extern ccBool n_network_game;
extern int n_local_player_num;

extern char *n_host_addr;
extern uint16_t n_host_port;
extern ccBool n_client_uses_pl1_keys;

extern ENetHost *n_host;
extern ENetPeer *n_peer;
extern uint32_t n_waiting_start_time;

extern ccBool cli_as_server;
extern ccBool cli_as_client;
extern ccBool cli_mode_dm;
extern ccBool cli_mode_coop;


extern int N_var_protector (CmdConCmd *me, const char *newval);


extern int N_init (void); // can be called multiple times, it's ok; return <0 on error

extern int N_create_host (void);
extern int N_create_client (void);

extern int N_disconnect (void);


#define NET_IS_PLAYING()  (n_host != NULL && g_st == GS_GAME && !g_trans && gnet_state == GNETS_PLAYING)


#endif
