/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include "edit.h"

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "cmdcon.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "items.h"
#include "jimapi.h"
#include "keyb.h"
#include "map.h"
#include "mapio.h"
#include "menu.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "sdldrv.h"
#include "sound.h"
#include "things.h"
#include "vga.h"
#include "vgaovl.h"
#include "view.h"
#include "widgets.h"
#include "widgets_jimapi.h"


int ed_ofs_x = 0;
int ed_ofs_y = 0;


#define STEPLO (8)
#define STEPHI (16)


void ED_draw (void) {
  //if (!r_mouse_cursor) drawMouseCursor(msLastX, msLastY, AID_DEFAULT);
}


void ED_act (void) {
  int dx = 0, dy = 0;
  /*
  int x = Z_SCR2MAP_X(msLastX);
  int y = Z_SCR2MAP_Y(msLastY);
  */
  //
  if (keys[SDLK_LEFT]) dx -= (keys[SDLK_LALT] ? STEPHI : STEPLO);
  if (keys[SDLK_RIGHT]) dx += (keys[SDLK_LALT] ? STEPHI : STEPLO);
  if (keys[SDLK_UP]) dy -= (keys[SDLK_LALT] ? STEPHI : STEPLO);
  if (keys[SDLK_DOWN]) dy += (keys[SDLK_LALT] ? STEPHI : STEPLO);
  //
  if (dx || dy) {
    ed_ofs_x += dx;
    ed_ofs_y += dy;
    //
    if (ed_ofs_x < 0) ed_ofs_x = 0;
    else if (ed_ofs_x >= FLDW*CELW) ed_ofs_x = FLDW*CELW-1;
    //
    if (ed_ofs_y < 0) ed_ofs_y = 0;
    else if (ed_ofs_y >= FLDH*CELH) ed_ofs_y = FLDH*CELH-1;
    //
    return;
  }
  //
  if (IT_edit_act()) return;
}


void ED_enter (void) {
}


void ED_leave (void) {
}
