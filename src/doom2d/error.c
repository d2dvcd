/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "error.h"

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"

#include "common.h"
#include "config.h"
#include "files.h"
#include "jimapi.h"
#include "keyb.h"
#include "misc.h"
#include "player.h"
#include "sound.h"
#include "vga.h"
#include "widgets.h"


void close_all (void) {
  S_done();
  K_done();
  V_done();
  M_shutdown();
}


static void trim_str (char *s) {
  while (isspace(*s)) memmove(s, s+1, strlen(s));
  while (*s &&isspace(s[strlen(s)-1])) s[strlen(s)-1] = 0;
}


static void print_str_padded (const char *s) {
  fputc(' ', stderr);
  for (; *s; ++s) {
    fputc(*s, stderr);
    if (*s == '\n') fputc(' ', stderr);
  }
}


GCC_PRINTF(1,2) GCC_NORETURN void ERR_failinit (const char *fmt, ...) {
  va_list ap;
  char *msg;
  //
  close_all();
  //
  va_start(ap, fmt);
  msg = Z_vasprintf(fmt, ap);
  vprintf(fmt, ap);
  va_end(ap);
  //
  trim_str(msg);
  //
  fprintf(stderr, "==================== DooM2D:VCD initialization error ====================\n\n ");
  print_str_padded(msg);
  fprintf(stderr, "\n\n=========================================================================\n");
  free(msg);
  //
  exit(1);
}


//FIXME: this function can be used to print 'out of memory' message!
GCC_PRINTF(1,2) GCC_NORETURN void ERR_fatal (const char *fmt, ...) {
  va_list ap;
  char *msg;
  //
  close_all();
  //
  va_start(ap, fmt);
  msg = Z_vasprintf(fmt, ap);
  vprintf(fmt, ap);
  va_end(ap);
  //
  trim_str(msg);
  //
  fprintf(stderr, "======================== DooM2D:VCD fatal error! ========================\n\n");
  print_str_padded(msg);
  fprintf(stderr, "\n\n=========================================================================\n");
  free(msg);
  //
  exit(2);
}


//TODO: convert DOS 80x25 text screen to ANSI
GCC_NORETURN void ERR_quit (int closed) {
  //void *p;
  //V_done();
  //if (!(p = malloc(4000)))
  demo_done();
  puts("Thank you for playing Doom2D:Vaya Con Dios!");
  //
  if (!closed) {
    int idx = F_findres("ENDOOM");
    //
    if (idx >= 0) {
      void *endoom = M_lock(idx);
      int done = 0;
      int lastsym = 0;
      //
      v_endtext = endoom;
      while (!done) {
        SDL_Event event;
        //
        V_setrect(0, SCRW, 0, SCRH);
        V_clr(0, SCRW, 0, SCRH, 0);
        V_blitscr();
        //
        if (SDL_WaitEvent(&event) == 0) break;
        switch (event.type) {
          case SDL_KEYDOWN:
            if (event.key.keysym.sym == SDLK_RETURN ||
                event.key.keysym.sym == SDLK_ESCAPE ||
                event.key.keysym.sym == SDLK_SPACE) lastsym = event.key.keysym.sym;
            break;
          case SDL_KEYUP:
            //if (event.key.keysym.sym == SDLK_RETURN && (event.key.keysym.mod&(KMOD_LALT|KMOD_RALT))) { V_toggle(); break; }
            if (lastsym != 0 && event.key.keysym.sym == lastsym) done = 1;
            lastsym = 0;
            break;
          case SDL_QUIT:
            done = 1;
            break;
          default: ;
        }
      }
    }
  }
  //else {
  //  F_loadres(F_getresid("ENDOOM"),p,0,4000);
  //  memcpy((void*)0xB8000,p,4000);free(p);gotoxy(1,24);
  //}
  zxwinDeinit();
  jimDeinit();
  //
  close_all();
  CFG_save();
  exit(0);
}
