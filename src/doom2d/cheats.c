/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
typedef const void *(*cheatCB) (void);

typedef struct {
  const char *cstr;
  cheatCB ccb;
} Cheat;


static const void *cheatIDDQD (void) {
  for (int f = 0; f < g_plrcount; ++f) PL_hit(&pl[f], 400, 0, HIT_SOME);
  return csnd1;
}


static const void *cheatTANK (void) {
  for (int f = 0; f < g_plrcount; ++f) {
    pl[f].life = pl[f].armor = 200;
    pl[f].drawst |= PL_DRAWARMOR|PL_DRAWLIFE;
  }
  return NULL;
}

CONCMD(tank, "engage 'tank' mode") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  cheatTANK();
  conlogf("tank mode engaged");
}


static const void *cheatBULLFROG (void) {
  PL_JUMP = (PL_JUMP == 10 ? 20 : 10);
  return NULL;
}

CONCMD(bullfrog, "toggle 'high jump' mode") {
  int bv;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if ((bv = boolval(argc, argv)) >= 0) {
    PL_JUMP = (bv ? 20 : 10);
  } else {
    cheatBULLFROG();
  }
  conlogf("'high jump' mode is %s", (PL_JUMP == 10 ? "off" : "on"));
}


static const void *cheatFORMULA1 (void) {
  PL_RUN = (PL_RUN == 8 ? 24 : 8);
  return NULL;
}

CONCMD(formula1, "toggle 'fast run' mode") {
  int bv;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if ((bv = boolval(argc, argv)) >= 0) {
    PL_RUN = (bv ? 24 : 8);
  } else {
    cheatFORMULA1();
  }
  conlogf("'fast run' mode is %s", (PL_RUN == 8 ? "off" : "on"));
}


static const void *cheatRAMBO (void) {
  for (int f = 0; f < g_plrcount; ++f) {
    pl[f].ammo = pl[f].shel = pl[f].rock = pl[f].cell = pl[f].fuel = 30000;
    pl[f].wpns = 0x7FF;
    pl[f].drawst |= PL_DRAWWPN/*|PL_DRAWKEYS*/;
    //pl[f].keys = 0x70;
  }
  return NULL;
}


CONCMD(rambo, "give all weapons and ammo") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  cheatRAMBO();
  conlogf("got weapons and ammo");
}


static int parse_key_names (int argc, char *argv[]) {
  int kmask = 0;
  //
  for (int f = 1; f < argc; ++f) {
    const char *s = argv[f];
    //
    if (strcasecmp(s, "all") == 0) kmask |= 0x70;
    else if (strcasecmp(s, "red") == 0) kmask |= 0x10;
    else if (strcasecmp(s, "green") == 0) kmask |= 0x20;
    else if (strcasecmp(s, "blue") == 0) kmask |= 0x40;
    else if (strcasecmp(s, "yellow") == 0) kmask |= 0x20;
    else { conlogf("%s: unknown key: '%s'\n", argv[0], s); return -1; }
  }
  //
  return kmask;
}


//TODO: keys for player 2
CONCMD(give_key, "give keys\neither all or red, green (yellow), blue") {
  int kmask;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc < 2) {
    conlogf("%s: which key?\n", argv[0]);
    return;
  }
  if ((kmask = parse_key_names(argc, argv)) < 0) return;
  for (int f = 0; f < g_plrcount; ++f) {
    pl[f].keys |= kmask;
    pl[f].drawst |= PL_DRAWKEYS;
  }
}


//TODO: keys for player 2
CONCMD(ungive_key, "'ungive' keys\neither all or red, green (yellow), blue") {
  int kmask;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (argc < 2) {
    conlogf("%s: which key?\n", argv[0]);
    return;
  }
  if ((kmask = parse_key_names(argc, argv)) < 0) return;
  for (int f = 0; f < g_plrcount; ++f) {
    if ((pl[f].keys &= ~kmask) == 0) pl[f].drawst &= ~PL_DRAWKEYS;
  }
}


static const void *cheatGOD (void) {
  p_immortal = !p_immortal;
  //fprintf(stderr, "you are %smortal!\n", (p_immortal ? "im" : ""));
  return NULL;
}

static void do_god (int argc, char *argv[]) {
  int bv;
  //
  CMDCON_CHECK_INGAME();
  if ((bv = boolval(argc, argv)) >= 0) {
    p_immortal = bv;
  } else {
    cheatGOD();
  }
  conlogf("you are %smortal!", (p_immortal ? "im" : ""));
}

CONCMD(god, "toggle 'god' mode") {
  CMDCON_HELP();
  CMDCON_CHECK_NONET();
  do_god(argc, argv);
}


CONCMD(highlander, "toggle 'god' mode") {
  CMDCON_HELP();
  CMDCON_CHECK_NONET();
  do_god(argc, argv);
}


static const void *cheatGHOST (void) {
  p_ghost = !p_ghost;
  return NULL;
}


CONCMD(ghost, "toggle 'ghost' mode") {
  int bv;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if ((bv = boolval(argc, argv)) >= 0) {
    p_ghost = bv;
  } else {
    cheatGHOST();
  }
  conlogf("ghost mode is %s!", (p_ghost ? "on" : "off"));
}


static const void *cheatFLY (void) {
  p_fly = !p_fly;
  return NULL;
}


CONCMD(fly, "toggle 'fly' mode") {
  int bv;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if ((bv = boolval(argc, argv)) >= 0) {
    p_fly = bv;
  } else {
    cheatFLY();
  }
  conlogf("fly mode is %s!", (p_fly ? "on" : "off"));
}


CONCMD(notarget, "toggle 'notarget' mode") {
  int bv;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if ((bv = boolval(argc, argv)) >= 0) {
    m_notarget = bv;
  } else {
    m_notarget = !m_notarget;
  }
  conlogf("notarget mode is %s!", (m_notarget ? "on" : "off"));
}


static const void *cheatOPEN (void) {
  SW_cheat_open();
  return NULL;
}

CONCMD(simsim, "open all doors") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  cheatOPEN();
  conlogf("all doors are opened");
}


static const void *cheatEXIT (void) {
  g_exit = D2D_LEVEL_EXIT_NORMAL;
  return NULL;
}

CONCMD(mapcomplete, "complete map (add 'secret' to use secret exit)") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  //CMDCON_CHECK_NONET();
  if (argc > 1 && strcasecmp(argv[1], "secret") == 0) {
    g_exit = D2D_LEVEL_EXIT_SECRET;
  } else {
    g_exit = D2D_LEVEL_EXIT_NORMAL;
  }
  conlogf("map complete");
}


#if 0
static const void *cheatMAP (void) {
  /*if (cbuf[30] >= '0' && cbuf[30] <= '9' && cbuf[31] >= '0' && cbuf[31] <= '9')*/ {
    g_map = (cbuf[30]-'0')*10+(cbuf[31]-'0');
    G_start();
  }
  return NULL;
}
#endif


CONCMD(map, "warp to map n") {
  int n, error;
  char s[8];
  //
  CMDCON_HELP();
  CMDCON_CHECK_NONET();
  //if (g_st != GS_GAME)
  //CMDCON_CHECK_INGAME();
  if (argc != 2) {
    conlogf("%s: invalid number of arguments\n", argv[0]);
    return;
  }
  //
  n = cmdcon_parseint(argv[1], g_map, 0, 99, &error);
  if (error) {
    conlogf("%s: invalid number: '%s'\n", argv[0], argv[1]);
    return;
  }
  //
  sprintf(s, "MAP%02d", n);
  if (F_findres(s) < 0) {
    conlogf("%s: can't warp to MAP%02d: no such map!\n", argv[0], n);
  } else {
    g_map = n;
    M_init_players();
    G_start();
    GM_set(NULL);
  }
}


static const void *cheatSKIP (void) {
  ++g_map;
  G_start();
  return NULL;
}


static const void *cheatBACK (void) {
  cheatNoBack = !cheatNoBack;
  return NULL;
}


static const void *cheatFRONT (void) {
  cheatNoFront = !cheatNoFront;
  return NULL;
}

CONCMD(tilesback, "toggle 'background' tiles") {
  int bv;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if ((bv = boolval(argc, argv)) >= 0) {
    cheatNoBack = !bv;
  } else {
    cheatNoBack = !cheatNoBack;
  }
  conlogf("background tiles: %s!", (cheatNoBack ? "off" : "on"));
}

CONCMD(tilesfront, "toggle 'foreground' tiles") {
  int bv;
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if ((bv = boolval(argc, argv)) >= 0) {
    cheatNoFront = !bv;
  } else {
    cheatNoFront = !cheatNoFront;
  }
  conlogf("foreground tiles: %s!", (cheatNoFront ? "off" : "on"));
}


static const Cheat cheats[] = {
  //{"SESHAT##", cheatMAP},
  {"ANUBIS", cheatGOD},
  {"BASTET", cheatGOD},
  {"HORUS", cheatFLY},
  //
  {"KKSKIP", cheatSKIP},
  {"KKGHOST", cheatGHOST},
  {"KKBACK", cheatBACK},
  {"KKFRONT", cheatFRONT},
  //
  //{",TKSQJHTK", cheatFLY}, // ���������
  //{"GJITKYF##", cheatMAP}, //�������
  //{"BULLFROG", cheatBULLFROG},
  //{"FORMULA1", cheatFORMULA1},
  {"GOODBYE", cheatEXIT},
  //{"CBVCBV", cheatOPEN}, // ������
  {"IDDQD", cheatIDDQD},
  //{"RAMBO", cheatRAMBO},
  //{"UJHTW", cheatGOD}, // �����
  //{"TANK", cheatTANK},
};


void G_code (void) {
  for (size_t f = 0; f < sizeof(cheats)/sizeof(Cheat); ++f) {
    if (isCheat(cheats[f].cstr)) {
      const void *s;
      //
      if (n_host != NULL) {
        memset(cbuf, 0, 32);
        if ((s = cheats[f].ccb()) == NULL) s = csnd2;
        Z_sound(s, 128);
      }
    }
  }
}
