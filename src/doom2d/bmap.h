/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Block map
#ifndef D2D_BMAP_H
#define D2D_BMAP_H

#include "common.h"
#include "misc.h"


enum {
  BM_WALL    = D2DBIT(0),
  BM_PLR1    = D2DBIT(1),
  BM_PLR2    = D2DBIT(2),
  BM_MONSTER = D2DBIT(3)
};


extern void BM_set_size (void); // same size as level map; will be called by W_set_field_size()
extern void BM_clear (uint8_t f);
extern void BM_mark (const obj_t *o, uint8_t f);
extern int BM_get (int x, int y);
extern void BM_remapfld (uint8_t clearbits);
extern ccBool BM_need_remap (void);
extern void BM_queue_remap (void);
extern void BM_clear_maybe_remap (uint8_t f); // do BM_clear() and BM_remapfld() if necessary


#endif
