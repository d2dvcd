/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Effects
#ifndef D2D_FX_H
#define D2D_FX_H

#include "common.h"


extern void FX_init (void);
extern void FX_alloc (void);
extern void FX_act (void);
extern void FX_draw (void);
extern void FX_tfog (int x, int y);
extern void FX_ifog (int x, int y);
extern void FX_bubble (int x, int y, int xv, int yv, int n);


#endif
