/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_SAVELOAD_H
#define D2D_SAVELOAD_H

#include <stdio.h>

#include "common.h"
#include "files.h"
#include "membuffer.h"


////////////////////////////////////////////////////////////////////////////////
extern char savname[10][42];
extern uint8_t savok[10];


////////////////////////////////////////////////////////////////////////////////
typedef int (*F_saveloadCB) (MemBuffer *mbuf, int waserror);

// saveCB: !0 (<0): error; everything will called with mbuf==NULL after saving
// loadCB: everything will called with mbuf==NULL after loading (to actualize)


////////////////////////////////////////////////////////////////////////////////
typedef enum {
  SAV_NORMAL,
  SAV_OPTIONAL
} SaveGameChunkType;

extern void F_registerSaveLoad (const char *chunkName, F_saveloadCB scb, F_saveloadCB lcb, SaveGameChunkType ctype);


////////////////////////////////////////////////////////////////////////////////
extern void F_getsavnames (void);
extern int F_loadgame (int);
extern int F_savegame (int n, const char *s);

extern int Fl_loadgame (const char *fname);
extern int Fl_savegame (const char *fname, const char *gamename);


#endif
