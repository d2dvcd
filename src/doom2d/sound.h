/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_SOUND_H
#define D2D_SOUND_H

#include "common.h"


// ��������� ����� � ������ (0-128)
extern int snd_vol, mus_vol;
extern ccBool music_random;
extern int music_time;
extern int music_fade;

extern ccBool snddisabled;
extern ccBool musdisabled;

extern ccBool musdisabledc;
extern ccBool snddisabledc;


// ��������� ����� (DS)
// �� ������!
typedef struct GCC_PACKED {
  uint16_t len;    // ����� � ������
  uint16_t rate;   // ������� � ��.
  // unused in DS_xxx
  uint16_t lstart; // ������ ������� � ������ �� ������ ������
  uint16_t llen;   // ����� ������� � ������
} snd_t;


extern void S_init (void);
extern void S_done (void);

// ��������� ���� s �� ������ c (1-8), ������� r � ��������� v (0-255)
// ���������� ����� ������, �� ������� �������� ���� (0..) ��� -1
// ���� c == -1, �� ���� ������� � ����� ��������� �����
// r - ��� ������������� ������� (������ 1024)
extern short S_play (const snd_t *s, short c, unsigned r, short v);

// ���������� ���� �� ������ c (1-8)
extern void S_stop (short c);

extern void S_startmusic (int);
extern void S_stopmusic (void);

extern void S_volumemusic (int v);
extern void S_volume (int v);
extern void free_chunks (void);
extern void S_wait (void);

extern void F_freemus (void);
extern void S_updatemusic (void);


#endif
