/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
////////////////////////////////////////////////////////////////////////////////
// server data
static uint16_t *plkeys_buf[2]; // our saved keys
static int32_t srv_seq_id; // current sequence id
static int32_t srv_last_cli_seq_id; // last received client ack
static int plkb_len = (60*60*19*10)*4; // NOT SIZE!
static int plkb_pos[2];
static Uint32 last_ack_time = 0;
static uint16_t client_keys = 0; // client keys for this frame


static int n_server_delay = 0;
//CONVAR_INT(n_server_delay, n_server_delay, 0, "DON'T TOUCH!")


//TODO: optimize discarding for RLEs
// remove first count frames from key buffer
static int plk_skip (uint16_t *buf, int *pos, int32_t count) {
  int dpos;
  //
  if (count < 0) {
    // the thing that should not be
    //HACK! we shouldn't know about srv_last_cli_seq_id
    conlogf("network: client seq_id is greater than server seq_it! (%d : %d)\n", srv_last_cli_seq_id+count-1, srv_seq_id);
    return -1;
  }
  //
#if NET_USE_RLE
  dpos = 0;
  while (count-- > 0) {
    fuck off compression for now
    if (buf[dpos]&0x8000) {
      // we are in RLE, discard one
      if (buf[dpos]&0x7fff) {
        --(buf[dpos]);
      } else {
        ++dpos; // fuck counter, one data value left
      }
    } else {
      // just fuck this data value
      ++dpos;
    }
    //
    ++dpos;
  }
#else
  if (count > *pos) {
    fprintf(stderr, "WTF!\n");
    abort();
  }
  dpos = count;
#endif
  //
  if (dpos > 0) {
    // compress
    (*pos) -= dpos;
    memmove(buf, buf+dpos, *pos);
  }
  //
  return 0;
}


/*
 * format of packed client data:
 *   word with bit 15 reset: just keys
 *   word with bit 15 set: other bits: counter, next word: data
 */
static void add_keys_to_buffer (uint16_t *buf, int *pos, uint16_t keywd) {
  int pp = *pos;
  //
#if NET_USE_RLE
  fuck off compression for now
  if (pp > 0) {
    if (buf[pp-2]&0x8000) {
      // we are doing RLE, check if we can continue packing
      if (buf[pp-1] == keywd && buf[pp-2] < 0xffff) {
        // yes, we can; just increase the counter
        ++(buf[pp-2]);
        return;
      }
      // no, we can't continue, store new value
    } else if (buf[pp-1] == keywd) {
      // we can start new RLE sequence
      buf[pp-1] = 0x8000; // '0' means 'two repeats'
    }
  }
#endif
  // either we can't, or we have to store new data for RLE
  buf[pp++] = keywd;
  *pos = pp;
}


static int network_send_keys (void) {
  if (NET_IS_PLAYING()) {
    ENetPacket *pkt;
    uint8_t *d;
    //
    pkt = enet_packet_create(NULL, 1+4*2+2*2+plkb_pos[0]*2+plkb_pos[1]*2, ENET_GAME_PKT_FLAGS); // we don't care
    // type
    d = pkt->data;
    *d++ = NETPKT_DELTA;
    // current server seq_id
    memcpy(d, &srv_seq_id, 4);
    d += 4;
    // delta from this seq_id
    memcpy(d, &srv_last_cli_seq_id, 4);
    d += 4;
    // length[2]
    for (int f = 0; f < 2; ++f) {
      memcpy(d, &plkb_pos[f], 2);
      d += 2;
    }
    // keys
    for (int f = 0; f < 2; ++f) {
      memcpy(d, plkeys_buf[f], plkb_pos[f]*2);
      d += plkb_pos[f]*2;
    }
    //
    if (d-pkt->data != 1+4*2+2*2+plkb_pos[0]*2+plkb_pos[1]*2) abort(); // assertion, hehe
    //
    enet_peer_send(n_peer, ENET_GAME_CHANNEL, pkt);
    enet_host_flush(n_host); // just send it
    //
    dlogfX("%d: delta packet sent (%d, %d)\n", srv_seq_id, g_time, plkb_pos[0]);
  }
  //
  return 0;
}


typedef struct GCC_PACKED {
  NetPacketType type;
  int32_t seq_id;
  uint16_t keys;
} NetAckPacket;


static int process_ack_packet (ENetPacket *pkt) {
  if (pkt->dataLength == sizeof(NetAckPacket)) {
    const NetAckPacket *ack = (const NetAckPacket *)(pkt->data);
    //
    if (ack->type != NETPKT_ACK) {
      conlogf("net error: ack packet is not ack! (%u)\n", ack->type);
    }
    //
    if (ack->seq_id > srv_seq_id) {
      conlogf("net error: srv_seq_id=%d; pkt->seq_id=%d\n", srv_seq_id, ack->seq_id);
      return -1;
    }
    //
    dlogfX("frm#%05d: srv_last_cli_seq_id=%d; ack->seq_id=%d\n", srv_seq_id, srv_last_cli_seq_id, ack->seq_id);
    if (ack->seq_id >= srv_last_cli_seq_id) client_keys = ack->keys;
    //
    if (ack->seq_id > srv_last_cli_seq_id) {
      // srv_last_cli_seq_id+1: first frame in key buffer
      // ack->seq_id: frame, already played by client
      // so we need to remove frames from srv_last_cli_seq_id+1 to ack->seq_id inclusive
      //
      for (int f = 0; f < 2; ++f) {
        if (plk_skip(plkeys_buf[f], &plkb_pos[f], ack->seq_id-(srv_last_cli_seq_id+1)+1) < 0) {
          conlogf("network: can't skip server deltas for plr#%d!\n", f);
          return -1;
        }
      }
      srv_last_cli_seq_id = ack->seq_id;
    }
    //
    last_ack_time = SDL_GetTicks();
    return 0;
  }
  //
  conlogf("network: invalid client ack packet size! (%u : %u : %u)\n", pkt->dataLength, sizeof(NetAckPacket),
    (pkt->dataLength > 0 ? pkt->data[0] : 255));
  return -1;
}


static void process_network_for_server (enet_uint32 tout) {
  if (n_host != NULL) {
    ENetEvent event;
    //
    while (enet_host_service(n_host, &event, tout) > 0) {
      int res;
      //
      tout = 0;
      switch (event.type) {
        case ENET_EVENT_TYPE_CONNECT:
          if (n_peer == NULL) {
            n_peer = event.peer;
            G_process_enet_connect();
            last_ack_time = SDL_GetTicks();
          } else {
            enet_peer_reset(event.peer);
          }
          return;
        case ENET_EVENT_TYPE_DISCONNECT:
          G_process_enet_disconnect();
          last_ack_time = 0;
          return;
        case ENET_EVENT_TYPE_RECEIVE:
          if (event.channelID == ENET_GAME_CHANNEL) {
            if (gnet_state == GNETS_PLAYING) {
              // we are playing the game, so it's client movemen
              res = process_ack_packet(event.packet);
              enet_packet_destroy(event.packet);
              if (res < 0) {
                // some error occured, just disconnect
                G_process_enet_disconnect();
              }
              return;
            } else {
              conlogf("network: game packed received, but we aren't playing!\n");
            }
            res = 0;
          } else if (event.channelID == ENET_HANDSHAKE_CHANNEL) {
            res = G_process_enet_game_packet(event.packet);
          } else if (event.channelID == ENET_CHAT_CHANNEL) {
            res = 0; // G_process_enet_chat_packet(event.packet);
          } else {
            res = -1;
          }
          enet_packet_destroy(event.packet);
          //
          if (res < 0) {
            conlogf("network: disconnected due to some errors...\n");
            N_disconnect();
          }
          return;
        case ENET_EVENT_TYPE_NONE:
          break;
      }
      break;
    }
  } else if (tout > 0) {
    SDL_Delay(tout);
  }
}


static int srv_oldkeycnt[16];
static uint8_t srv_oldkeystate[16];


static void network_game_server_init_vars (void) {
  srv_seq_id = -1;
  srv_last_cli_seq_id = -1;
  plkb_pos[0] = plkb_pos[1] = 0;
  last_ack_time = 0;
  client_keys = 0;
  //
  player_unpack_keys(n_local_player_num, 0);
  player_unpack_keys(1-n_local_player_num, 0);
  //
  memset(srv_oldkeycnt, 0, sizeof(srv_oldkeycnt));
  memset(srv_oldkeystate, 0, sizeof(srv_oldkeystate));
}


static void network_game_loop_server (void) {
  dlogfX("network_game_loop_server: enter\n");
  plkeys_buf[0] = malloc(plkb_len*2);
  plkeys_buf[1] = malloc(plkb_len*2);
  //
  network_game_server_init_vars();
  //
  cmdcon_exec("r_interpolate_panning 0");
  while (n_host != NULL) {
    Uint32 t;
    uint16_t keywd;
    //
    g_frame_ticks_start = SDL_GetTicks();
    S_updatemusic();
    widgetsUpdateActiveFlag();
    //
    G_act();
    draw_all();
    g_frame_advanced = 0;
    if (n_host == NULL) break;
    //
    dump_frame_time();
    //
    if (NET_IS_PLAYING()) {
      ++srv_seq_id; // advance frame
      //
      keywd = player_build_keys(n_local_player_num);
      //
      if (n_server_delay > 0) {
        uint16_t nk = 0;
        //
        for (int f = 0; f < 16; ++f) {
          if (srv_oldkeystate[f] == ((keywd>>f)&0x01)) {
            if ((++srv_oldkeycnt[f]) > n_server_delay) {
              nk |= (keywd&(1<<f));
              srv_oldkeycnt[f] = n_server_delay;
            }
          } else {
            if (srv_oldkeystate[f]>>f) nk |= (1<<f);
            srv_oldkeystate[f] = ((keywd>>f)&0x01);
            srv_oldkeycnt[f] = 0;
          }
        }
        keywd = nk;
      }
      //
      add_keys_to_buffer(plkeys_buf[0], &plkb_pos[0], keywd);
      add_keys_to_buffer(plkeys_buf[1], &plkb_pos[1], client_keys);
      //
      player_unpack_keys(n_local_player_num, keywd);
      player_unpack_keys(1-n_local_player_num, client_keys);
      //
      dlogfX("FRM#%05d: 0x%02x 0x%02x (%d)\n", srv_seq_id, keywd, client_keys, g_time);
      if (srv_last_cli_seq_id+4 < srv_seq_id) client_keys = 0; // client is slowpoke, assume that he is unconscious
      //
      g_frame_advanced = 1;
      if (last_ack_time == 0) last_ack_time = SDL_GetTicks();
    } else {
      g_frame_advanced = 0;
      network_game_server_init_vars();
    }
    //
    // now send our pressed keys and wait for frame end
    network_send_keys();
    //
    if ((t = SDL_GetTicks()) < g_frame_ticks_start+DELAY) {
      if (g_st == GS_GAME && !g_trans) {
        ++frame_count;
        ticks_used += t-g_frame_ticks_start;
        ticks_free += DELAY-(t-g_frame_ticks_start);
      }
      //
      while (t < g_frame_ticks_start+DELAY) {
        sdl_process_events();
        process_network_for_server(0);
        //
        if ((t = SDL_GetTicks()) < g_frame_ticks_start+DELAY) {
          Uint32 left = g_frame_ticks_start+DELAY-t;
          //
          if (left > 1) process_network_for_server(left-1);
          t = SDL_GetTicks();
        }
      }
    } else {
      sdl_process_events();
      process_network_for_server(0);
      //
      if (g_st == GS_GAME && !g_trans) {
        vo_turtle_alpha = 255;
        ++frame_count;
        ticks_used += t-g_frame_ticks_start;
      }
      t = SDL_GetTicks();
    }
    //
    if (NET_IS_PLAYING()) {
      if (last_ack_time > 0 && t-last_ack_time >= 3000) {
        // if client sends nothing for 3 seconds, assume it's disconnected
        conlogf("net: timeout!");
        G_process_enet_disconnect();
        last_ack_time = 0;
      } else if (last_ack_time > 0 && t-last_ack_time > DELAY+3) {
        vo_turtle_alpha = 255;
      }
    } else {
      network_game_server_init_vars();
    }
  }
  //
  free(plkeys_buf[1]);
  free(plkeys_buf[0]);
  dlogfX("network_game_loop_server: leave\n");
}
