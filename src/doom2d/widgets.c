/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include "widgets.h"


////////////////////////////////////////////////////////////////////////////////
int zxwidsys_active = 0;


////////////////////////////////////////////////////////////////////////////////
// color schemes
enum {
  FNV_PRIME = 16777619,
  FNV_OFFSET = 2166136261
};


// slightly modified FNV hash from http://home.comcast.net/~bretm/hash/6.html
static uint16_t fnv16 (const void *data, int len) {
  uint32_t hash = FNV_OFFSET;
  const uint8_t *d = (const uint8_t *)data;
  uint64_t h2;
  // calc hash
  while (len-- > 0) hash = (hash^(*d++))*FNV_PRIME;
  // mix the bits even further
  h2 = hash;
  h2 += (h2<<13);
  h2 ^= (h2>>7);
  h2 += (h2<<3);
  h2 ^= (h2>>17);
  h2 += (h2<<5);
  // return value %-)
  return (h2&0xffff)^((h2>>16)&0xffff);
}


////////////////////////////////////////////////////////////////////////////////
struct ZXWColor {
  uint16_t hash;
  char *name;
  int namelen;
  Uint8 clr;
};


struct ZXWColorScheme {
  uint16_t hash;
  char *name;
  int namelen;
  int cavail;
  int ccount;
  ZXWColor *colors;
};


static ZXWColorScheme *schemes = NULL;
static int schemeAmount = 0;
static int schemeCount = 0;


////////////////////////////////////////////////////////////////////////////////
static void __attribute__((destructor)) _zxw_deinit_ (void) {
  for (int s = schemeCount-1; s >= 0; --s) {
    for (int c = schemes[s].ccount-1; c >= 0; --c) free(schemes[s].colors[c].name);
    free(schemes[s].name);
    free(schemes[s].colors);
  }
  free(schemes);
}


ZXWColorScheme *zxcFindColorScheme (const char *scmname) {
  if (scmname != NULL && scmname[0]) {
    int len = strlen(scmname);
    uint16_t hash = fnv16(scmname, len);
    ZXWColorScheme *res = schemes;
    //
    for (int f = schemeCount; f > 0; --f, ++res) {
      if (res->hash == hash && res->namelen == len && strcmp(res->name, scmname) == 0) return res;
    }
  }
  //
  return NULL;
}


ZXWColorScheme *zxcNewColorScheme (const char *scmname) {
  ZXWColorScheme *scm = NULL;
  //
  if (scmname != NULL && scmname[0]) {
    if ((scm = zxcFindColorScheme(scmname)) == NULL) {
      if (schemeCount >= schemeAmount) {
        int newsz = schemeAmount+64;
        ZXWColorScheme *ns = realloc(schemes, sizeof(ZXWColorScheme)*newsz);
        //
        if (ns == NULL) return NULL;
        schemeAmount = newsz;
        schemes = ns;
      }
      scm = &schemes[schemeCount];
      if ((scm->name = strdup(scmname)) != NULL) {
        scm->namelen = strlen(scmname);
        scm->hash = fnv16(scmname, scm->namelen);
        scm->cavail = scm->ccount = 0;
        scm->colors = NULL;
        ++schemeCount;
      } else {
        scm = NULL;
      }
    } else {
      // just clear
      for (int f = scm->ccount-1; f >= 0; --f) free(scm->colors[f].name);
      scm->ccount = 0;
    }
  }
  return scm;
}


const char *zxcColorSchemeName (ZXWColorScheme *scm) {
  return (scm != NULL ? scm->name : NULL);
}


ZXWColor *zxscmFindColorInScheme (ZXWColorScheme *scm, const char *clrname) {
  if (scm != NULL && clrname != NULL && clrname[0]) {
    int len = strlen(clrname);
    uint16_t hash = fnv16(clrname, len);
    ZXWColor *res = scm->colors;
    //
    for (int f = scm->ccount; f > 0; --f, ++res) {
      if (res->hash == hash && res->namelen == len && strcmp(res->name, clrname) == 0) return res;
    }
  }
  return NULL;
}


ZXWColor *zxscmFindColor (const char *scmname, const char *clrname) {
  return zxscmFindColorInScheme(zxcFindColorScheme(scmname), clrname);
}


Uint8 zxscmColor (ZXWColorScheme *scm, const char *clrname) {
  ZXWColor *clr = zxscmFindColorInScheme(scm, clrname);
  //
  return (clr != NULL ? clr->clr : 255);
}


int zxcSetColor (const char *scmname, const char *clrname, Uint8 clr) {
  ZXWColorScheme *scm = zxcFindColorScheme(scmname);
  //
  if (scm != NULL && clrname != NULL && clrname[0]) {
    int len = strlen(clrname);
    uint16_t hash = fnv16(clrname, len);
    ZXWColor *res = scm->colors;
    //
    for (int f = scm->ccount; f > 0; --f, ++res) {
      if (res->hash == hash && res->namelen == len && strcmp(res->name, clrname) == 0) {
        res->clr = clr;
        return 0;
      }
    }
    // try to add new color
    if (scm->ccount >= scm->cavail) {
      int newsz = scm->cavail+64;
      ZXWColor *nc = realloc(scm->colors, sizeof(ZXWColor)*newsz);
      //
      if (nc == NULL) return -1;
      for (int f = scm->cavail; f < newsz; ++f) nc[f].name = NULL;
      scm->cavail = newsz;
      scm->colors = nc;
    }
    //
    res = &scm->colors[scm->ccount];
    if ((res->name = strdup(clrname)) != NULL) {
      res->hash = hash;
      res->namelen = len;
      res->clr = clr;
      ++(scm->ccount);
      return 0;
    }
  }
  //
  return -1;
}


int zxcEnumSchemes (ZXCSchemeEnum enumFn) {
  if (enumFn != NULL) {
    ZXWColorScheme *scm = schemes;
    //
    for (int f = schemeCount; f > 0; --f, ++scm) {
      int res = enumFn(scm);
      //
      if (res != 0) return res;
    }
  }
  return 0;
}


int zxcEnumColors (ZXWColorScheme *scm, ZXSColorEnum enumFn) {
  if (scm != NULL && enumFn != NULL) {
    ZXWColor *clr = scm->colors;
    //
    for (int f = scm->ccount; f > 0; --f, ++clr) {
      int res = enumFn(scm, clr->name, clr->clr);
      //
      if (res != 0) return res;
    }
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static Uint8 wGetClr (ZXWColorScheme *scm, const char *clrname) {
  char name[256], *p, *e;
  ZXWColor *clr = zxscmFindColorInScheme(scm, clrname);
  //
  //fprintf(stderr, "wGetClr: [%s]\n", clrname);
  if (clr == NULL) {
    //fprintf(stderr, " NO\n");
    strcpy(name, clrname);
    p = strchr(name+1, '/')+1; // points just after "/caption/"
    while ((e = strrchr(p, '/')) != NULL) {
      // 'e' points to "/ink"
      char *t;
      //
      for (t = e-1; *t != '/'; --t) ;
      // 't' points to "/active/ink"
      memmove(t, e, strlen(e)+1);
      //fprintf(stderr, " [%s]", name);
      if ((clr = zxscmFindColorInScheme(scm, name)) != NULL) {
        //fprintf(stderr, "  OK\n");
        return clr->clr;
      }
      //fprintf(stderr, "  NO\n");
    }
    //fprintf(stderr, " ***SHIT\n");
    return 255;
  }
  //
  //fprintf(stderr, " OK\n");
  return clr->clr;
}


static Uint8 wGetClrX (ZXWColorScheme *scm, const char *widname, const char *qualif, const char *papink) {
  char name[256];
  //
  if (qualif != NULL) sprintf(name, "/%s/%s/%s", widname, qualif, papink);
  else sprintf(name, "/%s/%s", widname, papink);
  //
  return wGetClr(scm, name);
}


Uint8 zxwFindColor (ZXWidget *g, const char *widname, const char *papink, int clrflags) {
  if (g != NULL) {
    ZXWColorScheme *scm = g->window->scm;
    const char *qual = NULL;
    Uint8 clr;
    //
    if (widname == NULL) widname = "window";
    if (g->window->flags&ZXWIN_DISABLED) clrflags = ZXWCLR_DISABLED;
    else if (strcmp(widname, "window") == 0) {
      if (clrflags != ZXWCLR_DISABLED && g->window != zxwinactive) clrflags = ZXWCLR_NORMAL;
      else if (clrflags == ZXWCLR_NORMAL && g->window == zxwinactive && strstr(papink, "paper") != NULL) {
        // HACK: paper color for inactive items in active window is 'active'
        clrflags = ZXWCLR_ACTIVE;
      }
    }
    //
    switch (clrflags&ZXWCLR_STATE_MASK) {
      case ZXWCLR_DISABLED: qual = "disabled"; break;
      case ZXWCLR_ACTIVE: qual = "active"; break;
    }
    if ((clr = wGetClrX(scm, widname, qual, papink)) != 255) return clr;
    if (strcmp(widname, "base") == 0) return zxwFindColor(g, "window", papink, clrflags);
    if (strcmp(widname, "window") == 0) return 255;
    return zxwFindColor(g, "base", papink, clrflags);
  }
  // alas
  return 255;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  const char *name;
  Uint8 clr;
} ClrInfo;

static const ClrInfo defScm[] = {
  // caption inactive
  {"/caption/stripes", 2}, // 2: dim
  {"/caption/ink", 0},
  {"/caption/paper", 7},
  // caption active
  {"/caption/active/stripes", 1}, // 1: normal
  {"/caption/active/ink", 0},
  {"/caption/active/paper", 15},
  // caption disabled
  {"/caption/disabled/stripes", 2},
  {"/caption/disabled/ink", 6},
  {"/caption/disabled/paper", 7},
  // window
  {"/window/moving/alpha", 190},
  // window inactive
  {"/window/alpha", 140},
  {"/window/ink", 7},
  {"/window/paper", 1},
  {"/window/hotkey_ink", 4},
  {"/window/hotkey_paper", 1},
  // window active
  {"/window/active/alpha", 255},
  {"/window/active/ink", 15},
  {"/window/active/paper", 9},
  {"/window/active/hotkey_ink", 12},
  {"/window/active/hotkey_paper", 9},
  // window disabled
  {"/window/disabled/ink", 6},
  {"/window/disabled/paper", 1},
  {"/window/disabled/hotkey_ink", 6},
  {"/window/disabled/hotkey_paper", 1},
  //
  // frame
  // static
  // button inactive
  {"/button/ink", 0},
  {"/button/paper", 7},
  {"/button/hotkey_ink", 2},
  {"/button/hotkey_paper", 7},
  // button active
  {"/button/active/ink", 0},
  {"/button/active/paper", 15},
  {"/button/active/hotkey_ink", 10},
  {"/button/active/hotkey_paper", 15},
  // button disabled
  {"/button/disabled/ink", 0},
  {"/button/disabled/paper", 6},
  {"/button/disabled/hotkey_ink", 0},
  {"/button/disabled/hotkey_paper", 6},
  // checkbox
  // radio
  // lineedit
  {"/lineedit/ink", 7},
  {"/lineedit/paper", 0},
  {"/lineedit/active/ink", 15},
  {"/lineedit/disabled/ink", 6},
  {"/lineedit/arrow_ink", 6},
  {"/lineedit/active/arrow_ink", 14},
  {"/lineedit/disabled/arrow_ink", 6},
  // scrollbar
  {"/scrollbar/ink", 0},
  {"/scrollbar/paper", 5},
  {"/scrollbar/knob_ink", 7},
  {"/scrollbar/active/knob_ink", 15},
  {"/scrollbar/disabled/ink", 6},
  {"/scrollbar/disabled/paper", 5},
  {"/scrollbar/disabled/knob_ink", 6},
  // listbox
  {"/listbox/ink", 7},
  {"/listbox/paper", 0},
  {"/listbox/cursor_ink", 0},
  {"/listbox/cursor_paper", 7},
  {"/listbox/mark_ink", 0},
  {"/listbox/mark_paper", 5},
  {"/listbox/curmark_ink", 1},
  {"/listbox/curmark_paper", 7},
  {"/listbox/active/ink", 15},
  {"/listbox/active/cursor_ink", 0},
  {"/listbox/active/cursor_paper", 15},
  {"/listbox/active/mark_ink", 0},
  {"/listbox/active/mark_paper", 13},
  {"/listbox/active/curmark_ink", 9},
  {"/listbox/active/curmark_paper", 15},
};


static void setSchemeColors (const char *name, const ClrInfo *nfo, size_t len) {
  zxcNewColorScheme(name);
  while (len-- > 0) {
    zxcSetColor(name, nfo->name, nfo->clr);
    ++nfo;
  }
}


//TODO: check for errors
int zxcInitDefaultColorSchemes (void) {
  setSchemeColors("default", defScm, sizeof(defScm)/sizeof(ClrInfo));
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
ZXWindow *zxwinfirst = NULL; // can be NULL
ZXWindow *zxwinlast = NULL; // can be NULL
ZXWindow *zxwinactive = NULL; // can be NULL


////////////////////////////////////////////////////////////////////////////////
int zxwStrLenAmp (const char *str) {
  int len = 0;
  //
  if (str != NULL) {
    while (*str) {
      if (*str++ == '&') {
        if (*str) ++str;
      }
      ++len;
    }
  }
  //
  return len;
}


static inline int isXYInWidget (const ZXWidget *g, int x, int y) {
  return (x >= 0 && y >= 0 && x < g->w && y < g->h);
}


static inline int isXYInButton (const ZXWidget *g, int x, int y) {
  return
    (isXYInWidget(g, x, y) &&
     !((x == 0 || x == g->w-1) && (y == 0 || y == g->h-1)));
}


static inline int isXYInWindow (const ZXWindow *w, int x, int y) {
  return
    (w->sfc.vo != NULL &&
     x >= 0 && y >= 0 && x < w->sfc.vo->w && y < w->sfc.vo->h &&
     !((x == 0 || x == w->sfc.vo->w-1) && (y == 0 || y == w->sfc.vo->h-1)));
}


static int wIsKey (const SDL_KeyboardEvent *key, SDLKey k, SDLMod mon, SDLMod moff) {
  if (key->keysym.sym == k) {
    if ((mon&KMOD_CTRL) && !(key->keysym.mod&KMOD_CTRL)) return 0;
    if ((mon&KMOD_ALT) && !(key->keysym.mod&KMOD_ALT)) return 0;
    if ((mon&KMOD_SHIFT) && !(key->keysym.mod&KMOD_SHIFT)) return 0;
    //
    if ((moff&KMOD_CTRL) && (key->keysym.mod&KMOD_CTRL)) return 0;
    if ((moff&KMOD_ALT) && (key->keysym.mod&KMOD_ALT)) return 0;
    if ((moff&KMOD_SHIFT) && (key->keysym.mod&KMOD_SHIFT)) return 0;
    //
    return 1;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////////
static SDLKey char2key (char ch) {
  if (tolower(ch) >= 'a' && tolower(ch) <= 'z') {
    return SDLK_a+tolower(ch)-'a';
  } else if (ch >= '0' && ch <= '9') {
    return SDLK_0+ch-'0';
  } else {
    switch (ch) {
      case '=': return SDLK_EQUALS; break;
      case '+': return SDLK_PLUS; break;
      case ',': return SDLK_COMMA; break;
      case '.': return SDLK_PERIOD; break;
      //case '.': return SDLK_COLON; break;
      case '/': return SDLK_SLASH; break;
      case '\'': return SDLK_QUOTE; break;
      case '\\': return SDLK_BACKSLASH; break;
      case '[': return SDLK_LEFTBRACKET; break;
      case ']': return SDLK_RIGHTBRACKET; break;
      case '`': return SDLK_BACKQUOTE; break;
    }
  }
  return 0;
}


static int setTitle (const char *title, char **ptitle, SDLKey *pkey, int *pkeypos) {
  char *nt, *dt;
  int pos = 0, fpos = 0;
  //
  if (title == NULL) title = "";
  if (pkey != NULL) *pkey = 0;
  if (pkeypos != NULL) *pkeypos = -1;
  if ((dt = nt = calloc(1, strlen(title)+1)) == NULL) return -1;
  //
  for (const char *p = title; *p; ++p, ++pos) {
    if (p[0] == '&' && p[1] == '&') {
      ++p;
    } else if (p[0] == '&' && p[1]) {
      ++p;
      if (!fpos) {
        fpos = 1;
        SDLKey k = char2key(p[0]);
        //
        if (k != 0) {
          if (pkey != NULL) *pkey = k;
          if (pkeypos != NULL) *pkeypos = pos;
        }
      }
    }
    *dt++ = *p;
  }
  *dt = 0;
  //
  if (ptitle != NULL) {
    if (*ptitle) free(*ptitle);
    *ptitle = nt;
  } else {
    free(nt);
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static void defwDestroyCB (ZXWidget *g) {
  if (g->id != NULL) free(g->id);
}


static void defwPaintCB (ZXWidget *g, VExSurface *sfc) {
}


static int defwKeyCB (ZXWidget *g, SDL_KeyboardEvent *key) {
  return 0;
}


static int defwMouseCB (ZXWidget *g, int x, int y, int btn, int buttons) {
  if (btn == MS_BUTTON_LEFT && (g->flags&ZXW_CANFOCUS) && g->window->active != g && isXYInWidget(g, x, y)) {
    zxwActivate(g);
    return 1;
  }
  return 0;
}


static void defwActivationCB (ZXWidget *g, int activated) {
}


////////////////////////////////////////////////////////////////////////////////
static void defWinDestroyCB (ZXWindow *w) {
  for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
    if (!(g->flags&ZXW_DEAD)) {
      if (w->active == g && g->activationCB != NULL) g->activationCB(g, 0);
      if (g->destroyCB != NULL) g->destroyCB(g);
      g->flags |= ZXW_DEAD;
    }
  }
  w->flags |= ZXWIN_HASZOMBIES;
}


static void defWinPaintCB (ZXWindow *w, VExSurface *sfc) {
  if (zxwinIsVisible(w)) {
    Uint8 capI, capP, winP, strp, alpha;
    const char *qualif = (w->flags&ZXWIN_DISABLED ? "disabled" : (w == zxwinactive ? "active" : NULL));
    int dy = (w->title[0] ? 8 : 1);
    //
    capI = wGetClrX(w->scm, "caption", qualif, "ink");
    capP = wGetClrX(w->scm, "caption", qualif, "paper");
    strp = wGetClrX(w->scm, "caption", qualif, "stripes"); // 1: normal; 2: dim
    //winI = wGetClrX(w->scm, "window", qualif, "ink");
    winP = wGetClrX(w->scm, "window", qualif, "paper");
    if (w->moving) {
      alpha = zxscmColor(w->scm, "/window/moving/alpha");
    } else {
      alpha = wGetClrX(w->scm, "window", qualif, "alpha");
    }
    //
    vResetClip(&w->sfc);
    clearVO(w->sfc.vo, 255);
    vDrawWindow(&w->sfc, 0, 0, w->sfc.vo->w, w->sfc.vo->h, w->title, capI, capP, winP,
      (strp == 1 ? ZXVIDWF_STRIPES : (strp == 2 ? ZXVIDWF_STRIPES|ZXVIDWF_STRIPES_DIM : 0)));
    //
    for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
      if ((g->flags&(ZXW_DEAD|ZXW_HIDDEN)) == 0 && g->paintCB != NULL) {
        g->x += 1;
        g->y += dy;
        vSetClip(&w->sfc, g->x, g->y, g->w, g->h, g->x, g->y);
        vRestrictClip(&w->sfc, 1, 8, w->sfc.vo->w-2, w->sfc.vo->h-9);
        g->paintCB(g, &w->sfc);
        g->x -= 1;
        g->y -= dy;
        vResetClip(&w->sfc);
      }
    }
    blitVO(w->sfc.vo, w->x, w->y, alpha);
  }
}


static int defWinKeyCB (ZXWindow *w, SDL_KeyboardEvent *key) {
  ZXWidget *pa = w->active;
  //
  if (pa != NULL && pa->keyCB != NULL) {
    if (pa->keyCB(pa, key)) return 1;
  }
  //
  for (ZXWidget *g = w->wlast; g != NULL; g = g->prev) {
    if (g != pa && (g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD)) == 0 && g->keyCB != NULL) {
      if (g->keyCB(g, key)) return 1;
    }
  }
  // ok, process tab and shift+tab
  if (key->type == SDL_KEYDOWN) {
    if (wIsKey(key, SDLK_TAB, KMOD_SHIFT, KMOD_CTRL|KMOD_ALT) ||
        wIsKey(key, SDLK_LEFT, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT) ||
        wIsKey(key, SDLK_KP4, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT) ||
        wIsKey(key, SDLK_UP, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT) ||
        wIsKey(key, SDLK_KP8, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT)) {
      zxwinFocusPrev(w);
      return 1;
    }
    if (wIsKey(key, SDLK_TAB, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT) ||
        wIsKey(key, SDLK_RIGHT, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT) ||
        wIsKey(key, SDLK_KP6, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT) ||
        wIsKey(key, SDLK_DOWN, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT) ||
        wIsKey(key, SDLK_KP2, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT)) {
      zxwinFocusNext(w);
      return 1;
    }
    if (wIsKey(key, SDLK_F4, KMOD_ALT, KMOD_CTRL|KMOD_SHIFT)) {
      zxwinClose(w);
      return 1;
    }
  }
  //
  return 0;
}


static int defWinMouseCB (ZXWindow *w, int x, int y, int btn, int buttons) {
  int dy = (w->title[0] ? 8 : 1), res;
  //
  if (w->moving) {
    if (zxwinIsEnabled(w)) {
      w->x += (x-w->moveDX);
      w->y += (y-w->moveDY);
      if ((btn&(MS_BUTTON_DEPRESSED|MS_BUTTON_LEFT)) == (MS_BUTTON_DEPRESSED|MS_BUTTON_LEFT)) w->moving = 0;
    } else {
      w->moving = 0;
    }
    return 1;
  }
  //
  if (w->sfc.vo == NULL || !zxwinIsEnabled(w) || !isXYInWindow(w, x, y)) return 0;
  //
  if (btn != 0) {
    // button event
    // LMB?
    if (btn == MS_BUTTON_LEFT) {
      if (zxwinactive != w) {
        // activate
        zxwinActivate(w);
        return 1;
      } else {
        // on title: start moving
        if (w->wmbdown == NULL && w->title[0] && y < 8) {
          // on title: start moving
          w->moving = 1;
          w->moveDX = x;
          w->moveDY = y;
          return 1;
        }
      }
    }
    // have 'mslocked' widget and button depressed?
    if (btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED) && w->wmbdown != NULL) {
      ZXWidget *g = w->wmbdown;
      //
      if ((g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD)) == 0) {
        if (g->mouseCB != NULL) {
          g->x += 1;
          g->y += dy;
          res = g->mouseCB(g, x-g->x, y-g->y, btn, buttons);
          g->x -= 1;
          g->y -= dy;
          if (res) return 1;
        }
      }
      w->wmbdown = NULL; // force reset
      return 1; // eaten
    }
    // try all widgets
    for (ZXWidget *g = w->wlast; g != NULL; g = g->prev) {
      if ((g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD)) == 0) {
        res = 0;
        g->x += 1;
        g->y += dy;
        if (btn == MS_BUTTON_LEFT &&
            (g->flags&ZXW_CANFOCUS) && g != w->active && x >= g->x && x < g->x+g->w && y >= g->y && y < g->y+g->h) {
          if (g->mouseCB != NULL) {
            res = g->mouseCB(g, x-g->x, y-g->y, btn, buttons);
          } else {
            zxwActivate(g);
          }
        } else {
          if (g->mouseCB != NULL) res = g->mouseCB(g, x-g->x, y-g->y, btn, buttons);
        }
        g->x -= 1;
        g->y -= dy;
        if (res) return 1;
      }
    }
  } else {
    if (w->wmbdown != NULL && w->wmbdown->mouseCB != NULL) {
      res = 0;
      w->wmbdown->x += 1;
      w->wmbdown->y += dy;
      res = w->wmbdown->mouseCB(w->wmbdown, x-w->wmbdown->x, y-w->wmbdown->y, btn, buttons);
      w->wmbdown->x -= 1;
      w->wmbdown->y -= dy;
      if (res) return res;
    }
    // try all widgets
    for (ZXWidget *g = w->wlast; g != NULL; g = g->prev) {
      if ((g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD)) == 0 && g->mouseCB != NULL) {
        res = 0;
        g->x += 1;
        g->y += dy;
        res = g->mouseCB(g, x-g->x, y-g->y, btn, buttons);
        g->x -= 1;
        g->y -= dy;
        if (res) return res;
      }
    }
  }
  //
  return 1; // eaten anyway
}


static void defWinActivationCB (ZXWindow *w, int activated) {
}


////////////////////////////////////////////////////////////////////////////////
static int isValidWindow (ZXWindow *w) {
  for (ZXWindow *ww = zxwinfirst; ww != NULL; ww = ww->next) {
    if (ww == w) return 1;
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static void widgetRemoveFromList (ZXWidget *g) {
  if (g->prev != NULL) g->prev->next = g->next; else g->window->wfirst = g->next;
  if (g->next != NULL) g->next->prev = g->prev; else g->window->wlast = g->prev;
}


static void windowRemoveFromList (ZXWindow *w) {
  if (w->prev != NULL) w->prev->next = w->next; else zxwinfirst = w->next;
  if (w->next != NULL) w->next->prev = w->prev; else zxwinlast = w->prev;
}


////////////////////////////////////////////////////////////////////////////////
void zxwinInit (void) {
  zxcInitDefaultColorSchemes();
}


void zxwinDeinit (void) {
  for (ZXWindow *w = zxwinfirst; w != NULL; w = w->next) {
    if (!(w->flags&ZXWIN_DEAD)) {
      if (zxwinactive == w && w->activationCB != NULL) w->activationCB(w, 0);
      w->flags |= ZXWIN_DEAD;
    }
  }
  zxwinactive = NULL;
  zxwinGC();
  zxwinlast = NULL;
}


void zxwinGC (void) {
  ZXWindow *n = NULL;
  //
  for (ZXWindow *w = zxwinfirst; w != NULL; w = n) {
    n = w->next;
    //
    if (w->flags&ZXWIN_DEAD) {
      for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
        if (!(g->flags&ZXW_DEAD)) {
          if (w->active == g && g->activationCB != NULL) g->activationCB(g, 0);
          if (g->destroyCB != NULL) { g->destroyCB(g); g->destroyCB = NULL; }
          g->flags |= ZXW_DEAD;
        }
      }
      w->flags |= ZXWIN_HASZOMBIES;
    }
    //
    if (w->flags&ZXWIN_HASZOMBIES) {
      ZXWidget *gn = NULL;
      //
      for (ZXWidget *g = w->wfirst; g != NULL; g = gn) {
        gn = g->next;
        //
        if (g->flags&ZXW_DEAD) {
          widgetRemoveFromList(g);
          free(g);
        }
      }
      w->flags &= ~ZXWIN_HASZOMBIES;
    }
    //
    if (w->flags&ZXWIN_DEAD) {
      windowRemoveFromList(w);
      if (w->destroyCB != NULL) w->destroyCB(w);
      if (w->sfc.vo != NULL) freeVO(w->sfc.vo);
      free(w);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
int zxwinIsAlive (ZXWindow *w) {
  return (w != NULL && !(w->flags&ZXWIN_DEAD));
}


int zxwinIsVisible (ZXWindow *w) {
  return
    (w != NULL && (w->flags&(ZXWIN_HIDDEN|ZXWIN_DEAD)) == 0 &&
     w->sfc.vo != NULL && w->x+w->sfc.vo->w-1 >= 0 && w->y+w->sfc.vo->h-1 >= 0 &&
     w->x < 320 && w->y < 240);
}


int zxwinIsEnabled (ZXWindow *w) {
  return (w != NULL && (w->flags&(ZXWIN_HIDDEN|ZXWIN_DISABLED|ZXWIN_DEAD)) == 0);
}


int zxwinCanBeActivated (ZXWindow *w) {
  return (w != NULL && (w->flags&(ZXWIN_HIDDEN|ZXWIN_DISABLED|ZXWIN_DEAD)) == 0);
}


int zxwinIsActive (ZXWindow *w) {
  return (w != NULL && w == zxwinactive && !(w->flags&(ZXWIN_DISABLED|ZXWIN_HIDDEN|ZXW_DEAD)));
}


////////////////////////////////////////////////////////////////////////////////
ZXWindow *zxwinNew (const char *scmname, const char *aId,
  int x, int y, int aw, int ah, const char *aTitle)
{
  ZXWindow *w = calloc(1, sizeof(ZXWindow));
  //
  if (w != NULL) {
    w->scm = zxcFindColorScheme(scmname != NULL ? scmname : "default");
    if (aId != NULL) {
      if ((w->id = strdup(aId)) == NULL) { free(w); return NULL; }
    }
    if (aw < 0) aw = 0;
    if (ah < 0) ah = 0;
    //
    w->paintCB = defWinPaintCB;
    w->keyCB = defWinKeyCB;
    w->mouseCB = defWinMouseCB;
    w->activationCB = defWinActivationCB;
    w->destroyCB = defWinDestroyCB;
    //
    w->sfc.vo = createVO(aw, ah);
    w->x = x;
    w->y = y;
    //
    if (aTitle != NULL) strncpy(w->title, aTitle, sizeof(w->title)-1);
    // add to list
    w->prev = zxwinlast;
    w->next = NULL;
    if (zxwinlast != NULL) zxwinlast->next = w; else zxwinfirst = w;
    zxwinlast = w;
    //
    for (ZXWindow *w = zxwinfirst; w != NULL; w = w->next) w->moving = 0;
    //
    zxwinActivate(w);
  }
  //
  return w;
}


int zxwinClose (ZXWindow *w) {
  if (isValidWindow(w)) {
    w->flags |= ZXW_DEAD;
    if (zxwinactive == w) {
      ZXWindow *n;
      //
      if (w->activationCB != NULL) w->activationCB(w, 0);
      for (n = zxwinactive; n != NULL; n = n->prev) if (zxwinCanBeActivated(n)) break;
      if (n == NULL) {
        for (n = zxwinlast; n != zxwinactive; n = n->prev) if (zxwinCanBeActivated(n)) break;
        if (n == zxwinactive) n = NULL;
      }
      zxwinactive = n;
      if (n != NULL && n->activationCB != NULL) n->activationCB(n, 1);
    }
    // mark all widgets as 'dead'
    // we have to do this here to call 'destroy' callbacks first
    for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
      if (!(g->flags&ZXW_DEAD)) {
        if (w->active == g && g->activationCB != NULL) g->activationCB(g, 0);
        if (g->destroyCB != NULL) { g->destroyCB(g); g->destroyCB = NULL; }
        g->flags |= ZXW_DEAD;
      }
    }
    w->flags |= ZXWIN_HASZOMBIES;
    if (w->destroyCB != NULL) { w->destroyCB(w); w->destroyCB = NULL; }
    return 0;
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
int zxwinSetX (ZXWindow *w, int v) {
  if (isValidWindow(w)) { w->x = v; return 0; }
  return -1;
}


int zxwinSetY (ZXWindow *w, int v) {
  if (isValidWindow(w)) { w->y = v; return 0; }
  return -1;
}


int zxwinSetW (ZXWindow *w, int v) {
  if (isValidWindow(w)) {
    int hgt = zxwinH(w);
    //
    if (v < 0) v = 0;
    if (w->sfc.vo != NULL) freeVO(w->sfc.vo);
    w->sfc.vo = createVO(v, hgt);
    return 0;
  }
  return -1;
}


int zxwinSetH (ZXWindow *w, int v) {
  if (isValidWindow(w)) {
    int wdt = zxwinW(w);
    //
    if (v < 0) v = 0;
    if (w->sfc.vo != NULL) freeVO(w->sfc.vo);
    w->sfc.vo = createVO(wdt, v);
    return 0;
  }
  return -1;
}


int zxwinSetCaption (ZXWindow *w, const char *title) {
  if (isValidWindow(w)) {
    if (title == NULL) title = "";
    strncpy(w->title, title, sizeof(w->title)-1);
    return 0;
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
static ZXWindow *findPossibleTopWindow (void) {
  for (ZXWindow *w = zxwinlast; w != NULL; w = w->prev) if (zxwinCanBeActivated(w)) return w;
  return NULL;
}


int zxwinHide (ZXWindow *w) {
  if (isValidWindow(w)) {
    if (zxwinIsVisible(w)) {
      w->flags |= ZXWIN_HIDDEN;
      if (w == zxwinactive) {
        ZXWindow *a;
        //
        zxwinactive = NULL;
        if (w->activationCB != NULL) w->activationCB(w, 0);
        if ((a = findPossibleTopWindow()) != NULL) zxwinActivate(a);
      }
    }
    return 0;
  }
  return -1;
}


int zxwinShow (ZXWindow *w) {
  if (isValidWindow(w)) {
    if (!zxwinIsVisible(w)) {
      ZXWindow *a;
      //
      w->flags &= ~ZXWIN_HIDDEN;
      if ((a = findPossibleTopWindow()) != w) zxwinActivate(w);
    }
    return 0;
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
ZXWindow *zxwinFindById (const char *id) {
  if (id != NULL && id[0]) {
    for (ZXWindow *w = zxwinfirst; w != NULL; w = w->next) {
      if (w->id != NULL && strcmp(w->id, id) == 0) return w;
    }
  }
  return NULL;
}


int zxwinActivate (ZXWindow *w) {
  if (isValidWindow(w)) {
    if (w->flags&(ZXWIN_HIDDEN|ZXWIN_DISABLED|ZXWIN_DEAD)) return 0;
    if (w != zxwinactive) {
      for (ZXWindow *w = zxwinfirst; w != NULL; w = w->next) w->moving = 0;
      //
      if (zxwinactive != NULL && zxwinactive->activationCB != NULL) zxwinactive->activationCB(zxwinactive, 0);
      zxwinactive = w;
      if (w->activationCB != NULL) w->activationCB(w, 1);
    }
    return 1;
  }
  return -1;
}


int zxwinBringToTop (ZXWindow *w) {
  if (isValidWindow(w)) {
    if (w->flags&(ZXWIN_HIDDEN|ZXWIN_DEAD)) return 0;
    if (w != zxwinlast) {
      for (ZXWindow *w = zxwinfirst; w != NULL; w = w->next) w->moving = 0;
      //
      windowRemoveFromList(w);
      w->prev = zxwinlast;
      w->next = NULL;
      if (zxwinlast != NULL) zxwinlast->next = w; else zxwinfirst = w;
      zxwinlast = w;
    }
    return 1;
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
ZXWidget *zxwinFindFocusPrev (ZXWindow *w) {
  if (w != NULL) {
    if (w->active != NULL) {
      for (ZXWidget *g = w->active->prev; g != NULL; g = g->prev) if (zxwCanBeActivated(g)) return g;
    }
    for (ZXWidget *g = w->wlast; g != w->active; g = g->prev) if (zxwCanBeActivated(g)) return g;
  }
  return NULL;
}


void zxwinFocusPrev (ZXWindow *w) {
  ZXWidget *na = zxwinFindFocusPrev(w);
  //
  if (na != NULL) zxwActivate(na);
}


ZXWidget *zxwinFindFocusNext (ZXWindow *w) {
  if (w != NULL) {
    if (w->active != NULL) {
      for (ZXWidget *g = w->active->next; g != NULL; g = g->next) if (zxwCanBeActivated(g)) return g;
    }
    for (ZXWidget *g = w->wfirst; g != w->active; g = g->next) if (zxwCanBeActivated(g)) return g;
  }
  return NULL;
}


void zxwinFocusNext (ZXWindow *w) {
  ZXWidget *na = zxwinFindFocusNext(w);
  //
  if (na != NULL) zxwActivate(na);
}


////////////////////////////////////////////////////////////////////////////////
ZXWidget *zxwNewWidget (ZXWindow *w, const char *id, int x, int y, int aw, int ah, int udatasize) {
  ZXWidget *g = calloc(1, sizeof(ZXWidget));
  //
  if (g != NULL) {
    if (id != NULL) {
      if ((g->id = strdup(id)) == NULL) { free(g); return NULL; }
    } else {
      g->id = NULL;
    }
    if (udatasize > 0) {
      if ((g->udata = calloc(1, udatasize)) == NULL) { free(g->id); free(g); return NULL; }
    }
    g->paintCB = defwPaintCB;
    g->keyCB = defwKeyCB;
    g->mouseCB = defwMouseCB;
    g->activationCB = defwActivationCB;
    g->destroyCB = defwDestroyCB;
    //
    g->x = x;
    g->y = y;
    g->w = aw;
    g->h = ah;
    // add to list
    if (w != NULL) {
      g->window = w;
      g->prev = w->wlast;
      g->next = NULL;
      if (w->wlast != NULL) w->wlast->next = g; else w->wfirst = g;
      w->wlast = g;
    }
  }
  //
  return g;
}


static void wCommonDestroyCB (ZXWidget *g) {
  ZXWWidgetDataHeader *data = (ZXWWidgetDataHeader *)g->udata;
  //
  if (data != NULL) {
    if (data->clearCB != NULL) data->clearCB(g, data);
    if (data->title != NULL) free(data->title);
    if (data->freeUData && data->udata != NULL) free(data->udata);
    free(data);
  }
  defwDestroyCB(g);
}


ZXWidget *zxwNewCommonWidget (ZXWindow *w, int wtype, const char *id, int x, int y, int aw, int ah, int udsize) {
  ZXWidget *g = zxwNewWidget(w, id, x, y, aw, ah, udsize);
  //
  if (g != NULL) {
    ZXWWidgetDataHeader *hdr = (ZXWWidgetDataHeader *)g->udata;
    //
    hdr->magic0 = hdr->magic1 = ZXWMAGICK;
    hdr->type = wtype;
    //
    g->destroyCB = wCommonDestroyCB;
  }
  return g;
}


int zxwRemoveWidget (ZXWidget *g) {
  if (g != NULL && !(g->flags&ZXW_DEAD)) {
    g->flags |= ZXW_DEAD;
    g->window->flags |= ZXWIN_HASZOMBIES;
    if (g->destroyCB != NULL) { g->destroyCB(g); g->destroyCB = NULL; }
    return 0;
  }
  return -1;
}


int zxwHideWidget (ZXWidget *g) {
  if (g != NULL) {
    g->flags |= ZXW_HIDDEN;
    if (g->window->active == g) zxwinFocusNext(g->window);
    return 0;
  }
  return 1;
}


int zxwShowWidget (ZXWidget *g) {
  if (g != NULL && (g->flags&ZXW_DEAD) == 0) {
    g->flags &= ~ZXW_HIDDEN;
    return 0;
  }
  return -1;
}


ZXWidget *zxwFindById (ZXWindow *w, const char *id) {
  if (w != NULL && id != NULL && id[0]) {
    for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
      if (g->id != NULL && strcmp(g->id, id) == 0) return g;
    }
  }
  return NULL;
}


int zxwWidgetSetId (ZXWidget *g, const char *id) {
  if (g != NULL) {
    char *s;
    //
    if (id != NULL) {
      if ((s = strdup(id)) == NULL) return -1;
    } else {
      s = NULL;
    }
    if (g->id != NULL) free(g->id);
    g->id = s;
    return 0;
  }
  return -1;
}


const char *zxwId (ZXWidget *g) {
  return (g != NULL ? g->id : NULL);
}


////////////////////////////////////////////////////////////////////////////////
int zxwCanBeVisible (ZXWidget *g) {
  return (g != NULL && !(g->flags&(ZXW_HIDDEN|ZXW_DEAD)));
}


int zxwCanBeActivated (ZXWidget *g) {
  return (g != NULL && (g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD|ZXW_CANFOCUS)) == ZXW_CANFOCUS);
}


int zxwCanProcessKeys (ZXWidget *g) {
  return (g != NULL && (g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD)) == 0);
}


int zxwActivate (ZXWidget *g) {
  if (zxwCanBeActivated(g)) {
    if (g->window->active != g) {
      if (g->window->active != NULL && g->window->active->activationCB != NULL) g->window->active->activationCB(g->window->active, 0);
      g->window->active = g;
      if (g->activationCB != NULL) g->activationCB(g, 1);
    }
    return 0;
  }
  return -1;
}


int zxwIsAlive (ZXWidget *g) {
  return (g != NULL && !(g->flags&ZXW_DEAD));
}


int zxwIsVisible (ZXWidget *g) {
  return (g != NULL && !(g->flags&(ZXW_HIDDEN|ZXW_DEAD)));
}


int zxwIsActive (ZXWidget *g) {
  return (g != NULL && g->window->active == g && !(g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD)));
}


int zxwIsEnabled (ZXWidget *g) {
  return (g != NULL && !(g->flags&(ZXW_DISABLED|ZXW_HIDDEN|ZXW_DEAD)));
}


////////////////////////////////////////////////////////////////////////////////
int zxwWidgetType (ZXWidget *g) {
  if (g != NULL) {
    ZXWWidgetDataHeader *hdr = (ZXWWidgetDataHeader *)g->udata;
    //
    if (hdr != NULL && hdr->magic0 == ZXWMAGICK && hdr->magic1 == ZXWMAGICK && hdr->type != 0) return hdr->type;
  }
  return 0;
}


ZXWWidgetDataHeader *zxwWidgetHeader (ZXWidget *g) {
  if (g != NULL) {
    ZXWWidgetDataHeader *hdr = (ZXWWidgetDataHeader *)g->udata;
    //
    if (hdr != NULL && hdr->magic0 == ZXWMAGICK && hdr->magic1 == ZXWMAGICK && hdr->type != 0) return hdr;
  }
  return NULL;
}


void *zxwWidgetHeaderEx (ZXWidget *g, int type) {
  if (g != NULL) {
    ZXWWidgetDataHeader *hdr = (ZXWWidgetDataHeader *)g->udata;
    //
    if (hdr != NULL && hdr->magic0 == ZXWMAGICK && hdr->magic1 == ZXWMAGICK && hdr->type == type) return hdr;
  }
  return NULL;
}


int zxwWidgetSetUData (ZXWidget *g, void *udata, int freeUData) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  if (hdr != NULL) {
    if (hdr->freeUData && hdr->udata != NULL) free(hdr->udata);
    hdr->freeUData = (freeUData && udata != NULL);
    hdr->udata = udata;
  }
  return 0;
}


void *zxwUData (ZXWidget *g) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  return (hdr != NULL ? hdr->udata : NULL);
}


int zxwWidgetSetClickCB (ZXWidget *g, zxwClickCB cb) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  if (hdr != NULL) {
    hdr->clickCB = cb;
    return 0;
  }
  return -1;
}


zxwClickCB zxwWidgetClickCB (ZXWidget *g) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  return (hdr != NULL ? hdr->clickCB : NULL);
}


void zxwClick (ZXWidget *g) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  if (hdr != NULL && hdr->clickCB != NULL) hdr->clickCB(g, hdr->udata);
}


int zxwWidgetSetSBarCB (ZXWidget *g, zxwSBarCB cb) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  if (hdr != NULL) {
    hdr->sbarCB = cb;
    return 0;
  }
  return -1;
}


zxwSBarCB zxwWidgetSBarCB (ZXWidget *g) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  return (hdr != NULL ? hdr->sbarCB : NULL);
}


void zxwSBar (ZXWidget *g, ZXWidget *sb) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  if (hdr != NULL && hdr->sbarCB != NULL) hdr->sbarCB(g, sb);
}


int zxwSetTitle (ZXWidget *g, const char *aTitle) {
  ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
  //
  if (hdr != NULL) {
    return setTitle(aTitle, &hdr->title, &hdr->actKey, &hdr->actCPos);
  }
  return -1;
}


int wIsActivateKeyPressed (ZXWidget *g, const SDL_KeyboardEvent *key) {
  if (key->type == SDL_KEYDOWN && zxwCanProcessKeys(g)) {
    ZXWWidgetDataHeader *hdr = zxwWidgetHeader(g);
    //
    if (hdr != NULL) {
      return
        (g->window->active == g && wIsKey(key, SDLK_SPACE, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT)) ||
        (hdr->actKey != 0 && wIsKey(key, hdr->actKey, KMOD_ALT, KMOD_CTRL));
    }
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static inline int buildClrFlags (const ZXWidget *g) {
  return
    (g->flags&ZXW_DISABLED ? ZXWCLR_DISABLED :
     (g == g->window->active ? ZXWCLR_ACTIVE : ZXWCLR_NORMAL));
}


static void paintTitle (ZXWidget *g, VExSurface *sfc, const char *widname, int w, int h, int cflg, Uint8 i, Uint8 p) {
  ZXWWidgetDataHeader *data = (ZXWWidgetDataHeader *)g->udata;
  //
  if (data->title != NULL && data->title[0]) {
    int len = strlen(data->title);
    int tx = (w-len*6)/2;
    int ty = (h-8)/2;
    //
    vDrawBar(sfc, tx-2, ty, strlen(data->title)*6+4, 8, p);
    vDrawText(sfc, tx, ty, data->title, i, 255);
    //
    if (data->actCPos >= 0 && data->actCPos < len) {
      Uint8 i = zxwFindColor(g, widname, "hotkey_ink", cflg);
      Uint8 p = zxwFindColor(g, widname, "hotkey_paper", cflg);
      //
      vDrawChar(sfc, tx+data->actCPos*6, ty, data->title[data->actCPos], i, p);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
} ZXWFrameData;


static void wFramePaintCB (ZXWidget *g, VExSurface *sfc) {
  ZXWWidgetDataHeader *hdr = (ZXWWidgetDataHeader *)g->udata;
  int cflg = buildClrFlags(g);
  Uint8 i = zxwFindColor(g, "frame", "ink", cflg);
  //
  vDrawRectW(sfc, 0, 4, g->w, g->h-4, i);
  //
  if (hdr->title != NULL && hdr->title[0]) {
    vRestrictClip(sfc, g->x+3, g->y, g->w-6, 8);
    paintTitle(g, sfc, "frame", g->w-6, 8, cflg, i, zxwFindColor(g, "frame", "paper", cflg));
  }
}


extern ZXWidget *zxwNewFrame (ZXWindow *w, const char *id, int x, int y, int aw, int ah, const char *aTitle) {
  ZXWidget *g;
  ZXWWidgetDataHeader *hdr;
  //
  if (aw < 0) aw = 0;
  if (ah < 0) ah = 0;
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_FRAME, id, x, y, aw, ah, sizeof(ZXWFrameData))) == NULL) return NULL;
  //
  hdr = (ZXWWidgetDataHeader *)g->udata;
  if (aTitle != NULL && aTitle[0]) hdr->title = strdup(aTitle);
  hdr->actCPos = -1;
  //
  g->paintCB = wFramePaintCB;
  //
  return g;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
  ZXWidget *link;
} ZXWStaticData;


static void wStaticClickCB (ZXWidget *g, void *udata) {
  ZXWStaticData *data = (ZXWStaticData *)g->udata;
  //
  if (data->link != NULL && zxwCanBeActivated(data->link)) {
    zxwActivate(data->link);
    zxwClick(data->link);
  }
}


static void wStaticPaintCB (ZXWidget *g, VExSurface *sfc) {
  int cflg = buildClrFlags(g);
  Uint8 i = zxwFindColor(g, "static", "ink", cflg);
  Uint8 p = zxwFindColor(g, "static", "paper", cflg);
  //
  paintTitle(g, sfc, "static", g->w, g->h, cflg, i, p);
}


static int wStaticKeyCB (ZXWidget *g, SDL_KeyboardEvent *key) {
  if (zxwCanProcessKeys(g)) {
    ZXWStaticData *data = (ZXWStaticData *)g->udata;
    //
    if (data->hdr.actKey != 0 && wIsKey(key, data->hdr.actKey, KMOD_ALT, KMOD_CTRL)) {
      // hotkey
      zxwClick(g);
      return 1;
    }
  }
  //
  return 0;
}


static int wStaticMouseCB (ZXWidget *g, int x, int y, int btn, int buttons) {
  if (btn == MS_BUTTON_LEFT && isXYInWidget(g, x, y)) {
    g->window->wmbdown = g;
    zxwClick(g);
    return 1;
  }
  //
  if (btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED)) {
    if (g->window->wmbdown == g) {
      g->window->wmbdown = NULL;
      if (isXYInButton(g, x, y)) zxwClick(g);
      return 1;
    }
    if (g->window->wmbdown == NULL && isXYInButton(g, x, y)) return 1;
  }
  //
  return isXYInWidget(g, x, y);
}


ZXWidget *zxwNewStatic (ZXWindow *w, const char *id, int x, int y, int aw, int ah, const char *aTitle) {
  ZXWidget *g;
  //
  if (aw < 0) aw = (aTitle != NULL ? zxwStrLenAmp(aTitle)*6 : 0);
  if (ah < 0) ah = (aTitle != NULL ? 8 : 0);
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_STATIC, id, x, y, aw, ah, sizeof(ZXWStaticData))) == NULL) return NULL;
  zxwSetTitle(g, aTitle);
  zxwWidgetSetClickCB(g, wStaticClickCB);
  //
  g->paintCB = wStaticPaintCB;
  g->keyCB = wStaticKeyCB;
  g->mouseCB = wStaticMouseCB;
  //
  return g;
}


int zxwStaticSetLinked (ZXWidget *g, ZXWidget *link) {
  ZXWStaticData *data = zxwWidgetHeaderEx(g, ZXWT_STATIC);
  //
  if (data != NULL && (link == NULL || zxwCanBeActivated(link))) {
    data->link = link;
    return 0;
  }
  return -1;
}


ZXWidget *zxwStaticLinked (ZXWidget *g) {
  ZXWStaticData *data = zxwWidgetHeaderEx(g, ZXWT_STATIC);
  //
  return (data != NULL ? data->link : NULL);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
  int def; // <0: esc; >0: enter
} ZXWButtonData;


static ZXWidget *wWinFindDefButton (ZXWindow *w, int def) {
  for (ZXWidget *g = w->wlast; g != NULL; g = g->prev) {
    if (zxwCanProcessKeys(g)) {
      ZXWButtonData *data = zxwWidgetHeaderEx(g, ZXWT_BUTTON);
      //
      if (data != NULL) {
        if ((def < 0 && data->def < 0) || (def > 0 && data->def > 0)) return g;
      }
    }
  }
  return NULL;
}


static void wButtonPaintCB (ZXWidget *g, VExSurface *sfc) {
  int cflg = buildClrFlags(g);
  Uint8 i = zxwFindColor(g, "button", "ink", cflg);
  Uint8 p = zxwFindColor(g, "button", "paper", cflg);
  //
  // button body
  vDrawRectW(sfc, 0, 0, g->w, g->h, p);
  vDrawBar(sfc, 1, 1, g->w-2, g->h-2, p);
  //
  vChangeClip(sfc, -1, -1);
  paintTitle(g, sfc, "button", g->w, g->h, cflg, i, p);
}


static int wButtonKeyCB (ZXWidget *g, SDL_KeyboardEvent *key) {
  if (zxwCanProcessKeys(g) && key->type == SDL_KEYDOWN) {
    ZXWButtonData *data = (ZXWButtonData *)g->udata;
    //
    if (wIsActivateKeyPressed(g, key)) {
      zxwActivate(g);
      zxwClick(g);
      return 1;
    }
    // check other keys
    if (wIsKey(key, SDLK_RETURN, 0, KMOD_ALT|KMOD_SHIFT)) {
      // ctrl+enter: do default action
      if (data->def <= 0) g = wWinFindDefButton(g->window, 1);
      if (g != NULL) {
        zxwActivate(g);
        zxwClick(g);
      }
      return 1;
    }
    //
    if (wIsKey(key, SDLK_ESCAPE, 0, KMOD_CTRL|KMOD_ALT|KMOD_SHIFT)) {
      // default 'esc'
      if (data->def >= 0) g = wWinFindDefButton(g->window, -1);
      if (g != NULL) {
        zxwActivate(g);
        zxwClick(g);
      }
      return 1;
    }
  }
  //
  return 0;
}


static int wButtonMouseCB (ZXWidget *g, int x, int y, int btn, int buttons) {
  if (btn == MS_BUTTON_LEFT && isXYInButton(g, x, y)) {
    g->window->wmbdown = g;
    zxwActivate(g);
    return 1;
  }
  //
  if (btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED)) {
    if (g->window->wmbdown == g) {
      g->window->wmbdown = NULL;
      if (isXYInButton(g, x, y)) zxwClick(g);
      return 1;
    }
    if (g->window->wmbdown == NULL && isXYInButton(g, x, y)) return 1;
  }
  //
  return 0;
}


ZXWidget *zxwNewButton (ZXWindow *w, const char *id, int x, int y, int aw, int ah,
  const char *aTitle, zxwClickCB aClickCB)
{
  ZXWidget *g;
  //
  if (aw < 0) {
    aw = (aTitle != NULL ? zxwStrLenAmp(aTitle)*6+4 : 32);
    if (aTitle != NULL && aTitle[0] && isupper(aTitle[strlen(aTitle)-1])) ++aw;
  }
  if (ah < 0) ah = 10;
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_BUTTON, id, x, y, aw, ah, sizeof(ZXWButtonData))) == NULL) return NULL;
  g->flags = ZXW_CANFOCUS;
  //
  zxwSetTitle(g, aTitle);
  zxwWidgetSetClickCB(g, aClickCB);
  //
  g->paintCB = wButtonPaintCB;
  g->keyCB = wButtonKeyCB;
  g->mouseCB = wButtonMouseCB;
  //
  return g;
}


static int zxwButtonSetDefFlag (ZXWidget *g, int df) {
  ZXWButtonData *data = zxwWidgetHeaderEx(g, ZXWT_BUTTON);
  //
  if (data != NULL) {
    if (df && g->window != NULL) {
      // reset other defs
      for (ZXWidget *gg = g->window->wfirst; gg != NULL; gg = gg->next) {
        ZXWButtonData *bt = zxwWidgetHeaderEx(gg, ZXWT_BUTTON);
        //
        if (bt != NULL) {
          if ((df < 0 && bt->def < 0) || (df > 0 && bt->def > 0)) bt->def = 0;
        }
      }
    }
    //
    data->def = (df < 0 ? -1 : (df > 0 ? 1: 0));
    return 0;
  }
  //
  return -1;
}


int zxwButtonSetDefault (ZXWidget *g) {
  return zxwButtonSetDefFlag(g, 1);
}


int zxwButtonSetCancel (ZXWidget *g) {
  return zxwButtonSetDefFlag(g, -1);
}


int zxwButtonSetNormal (ZXWidget *g) {
  return zxwButtonSetDefFlag(g, 0);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
  int state; // 0: unchecked; 1: checked; 2: question mark
} ZXWCheckBoxData;


static void checkBoxClickCB (ZXWidget *g, void *udata) {
  ZXWCheckBoxData *data = (ZXWCheckBoxData *)g->udata;
  //
  data->state = !data->state;
  //data->state = (data->state+1)&0x03;
}


static void wCheckBoxPaintCB (ZXWidget *g, VExSurface *sfc) {
  ZXWCheckBoxData *data = (ZXWCheckBoxData *)g->udata;
  int cflg = buildClrFlags(g);
  Uint8 i = zxwFindColor(g, "checkbox", "ink", cflg);
  Uint8 p = zxwFindColor(g, "checkbox", "paper", cflg);
  int y = (g->h-8)/2;
  //
  vDrawBar(sfc, 0, 0, g->w, g->h, p);
  // checkbox
  vDrawRectW(sfc, 0, y, 8, 8, i);
  switch (data->state) {
    case 0: break;
    case 1: // checked
/*
.@@@@@@.
@......@
@.@..@.@
@..@@..@
@..@@..@
@.@..@.@
@......@
.@@@@@@.
*/
      vDrawLine(sfc, 2, y+2, 5, y+5, i);
      vDrawLine(sfc, 5, y+2, 2, y+5, i);
      break;
    case 2: // '?'
/*
.@@@@@@.
@......@
@..@@..@
@....@.@
@...@..@
@...@..@
@......@
.@@@@@@.
*/
      vDrawHLine(sfc, 3, y+2, 2, i);
      vPutPixel(sfc, 5, y+3, i);
      vDrawVLine(sfc, 4, y+4, 2, i);
      break;
  }
  //
  if (data->hdr.title[0]) {
    sfc->clip.vClipX0 += 10;
    sfc->clip.vOfsX += 10;
    paintTitle(g, sfc, "checkbox", g->w-10, g->h, cflg, i, p);
  }
}


static int wCheckBoxKeyCB (ZXWidget *g, SDL_KeyboardEvent *key) {
  if (zxwCanProcessKeys(g) && key->type == SDL_KEYDOWN && wIsActivateKeyPressed(g, key)) {
    // hotkey
    zxwActivate(g);
    zxwClick(g);
    return 1;
  }
  //
  return 0;
}


static int wCheckBoxMouseCB (ZXWidget *g, int x, int y, int btn, int buttons) {
  if (btn == MS_BUTTON_LEFT && isXYInWidget(g, x, y)) {
    g->window->wmbdown = g;
    zxwActivate(g);
    zxwClick(g);
    return 1;
  }
  //
  if (btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED)) {
    if (g->window->wmbdown == g) {
      g->window->wmbdown = NULL;
      return 1;
    }
    if (g->window->wmbdown == NULL && isXYInWidget(g, x, y)) return 1;
  }
  //
  return 0;
}


ZXWidget *zxwNewCheckBox (ZXWindow *w, const char *id, int x, int y, int aw, int ah, const char *aTitle) {
  ZXWidget *g;
  //
  if (aw < 0) aw = (aTitle != NULL ? zxwStrLenAmp(aTitle)*6+10 : 8);
  if (ah < 0) ah = 8;
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_CHECKBOX, id, x, y, aw, ah, sizeof(ZXWCheckBoxData))) == NULL) return NULL;
  g->flags = ZXW_CANFOCUS;
  //
  zxwSetTitle(g, aTitle);
  zxwWidgetSetClickCB(g, checkBoxClickCB);
  //
  g->paintCB = wCheckBoxPaintCB;
  g->keyCB = wCheckBoxKeyCB;
  g->mouseCB = wCheckBoxMouseCB;
  //
  return g;
}


int zxwCheckBoxSetState (ZXWidget *g, int state) {
  ZXWCheckBoxData *data = zxwWidgetHeaderEx(g, ZXWT_CHECKBOX);
  //
  if (data != NULL) {
    if (state < 0 || state > 1) state = 2;
    data->state = state;
    return 0;
  }
  return -1;
}


// >=0: state; <0: error
int zxwCheckBoxState (ZXWidget *g) {
  ZXWCheckBoxData *data = zxwWidgetHeaderEx(g, ZXWT_CHECKBOX);
  //
  return (data != NULL ? data->state : -1);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
  char *group;
  int active;
} ZXWRadioData;


static void radioResetActive (ZXWindow *w, const char *group) {
  if (w != NULL) {
    for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
      if (zxwWidgetType(g) == ZXWT_RADIO) {
        ZXWRadioData *data = (ZXWRadioData *)g->udata;
        //
        data->active = 0;
      }
    }
  }
}


static void radioClickCB (ZXWidget *g, void *udata) {
  ZXWRadioData *data = (ZXWRadioData *)g->udata;
  //
  if (data->group != NULL) radioResetActive(g->window, data->group);
  data->active = 1;
}


static void wRadioPaintCB (ZXWidget *g, VExSurface *sfc) {
  ZXWRadioData *data = (ZXWRadioData *)g->udata;
  int cflg = buildClrFlags(g);
  Uint8 i = zxwFindColor(g, "radio", "ink", cflg);
  Uint8 p = zxwFindColor(g, "radio", "paper", cflg);
  int y = (g->h-8)/2;
  //
  vDrawBar(sfc, 0, 0, g->w, g->h, p);
/*
..@@@@..
.@....@.
@..@@..@
@.@@@@.@
@.@@@@.@
@..@@..@
.@....@.
..@@@@..
*/
  // circle
  vDrawHLine(sfc, 2, y+0, 4, i);
  vDrawHLine(sfc, 2, y+7, 4, i);
  vDrawVLine(sfc, 0, y+2, 4, i);
  vDrawVLine(sfc, 7, y+2, 4, i);
  vPutPixel(sfc, 1, y+1, i);
  vPutPixel(sfc, 6, y+1, i);
  vPutPixel(sfc, 1, y+6, i);
  vPutPixel(sfc, 6, y+6, i);
  // dot
  if (data->active) {
    vDrawRectW(sfc, 2, y+2, 4, 4, i);
    vDrawBar(sfc, 3, y+3, 2, 2, i);
  }
  //
  if (data->hdr.title[0]) {
    sfc->clip.vClipX0 += 10;
    sfc->clip.vOfsX += 10;
    paintTitle(g, sfc, "radio", g->w-10, g->h, cflg, i, p);
  }
}


ZXWidget *zxwNewRadio (ZXWindow *w, const char *group, const char *id,
  int x, int y, int aw, int ah, const char *aTitle)
{
  ZXWidget *g;
  //
  if (aw < 0) aw = (aTitle != NULL ? zxwStrLenAmp(aTitle)*6+10 : 8);
  if (ah < 0) ah = 8;
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_RADIO, id, x, y, aw, ah, sizeof(ZXWRadioData))) == NULL) return NULL;
  g->flags = ZXW_CANFOCUS;
  //
  if (group != NULL) {
    ZXWRadioData *data = (ZXWRadioData *)g->udata;
    //
    if ((data->group = strdup(group)) == NULL) { zxwRemoveWidget(g); return NULL; }
  }
  //
  zxwSetTitle(g, aTitle);
  zxwWidgetSetClickCB(g, radioClickCB);
  //
  g->paintCB = wRadioPaintCB;
  g->keyCB = wCheckBoxKeyCB;
  g->mouseCB = wCheckBoxMouseCB;
  //
  return g;
}


int zxwinRadioSet (ZXWindow *w, const char *group, const char *id) {
  if (w != NULL && group != NULL && group[0]) {
    for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
      ZXWRadioData *data = zxwWidgetHeaderEx(g, ZXWT_RADIO);
      //
      if (data != NULL && data->group != NULL && strcmp(data->group, group) == 0) {
        data->active = 0;
        if (id != NULL && id[0] && g->id != NULL && strcmp(g->id, id) == 0) {
          data->active = 1;
        }
      }
    }
    return 0;
  }
  return -1;
}


ZXWidget *zxwinRadioGetActiveWidget (ZXWindow *w, const char *group) {
  if (w != NULL && group != NULL && group[0]) {
    for (ZXWidget *g = w->wfirst; g != NULL; g = g->next) {
      ZXWRadioData *data = zxwWidgetHeaderEx(g, ZXWT_RADIO);
      //
      if (data != NULL && data->group != NULL && data->active && strcmp(data->group, group) == 0) return g;
    }
  }
  return NULL;
}


const char *zxwinRadioGet (ZXWindow *w, const char *group) {
  ZXWidget *g = zxwinRadioGetActiveWidget(w, group);
  //
  return (g != NULL ? g->id : NULL);
}


const char *zxwRadioGetGroupName (ZXWidget *g) {
  ZXWRadioData *data = zxwWidgetHeaderEx(g, ZXWT_RADIO);
  //
  return (data != NULL ? data->group : NULL);
}


int zxwRadioSetActive (ZXWidget *g, int act) {
  ZXWRadioData *data = zxwWidgetHeaderEx(g, ZXWT_RADIO);
  //
  if (data != NULL) {
    data->active = (act > 0);
    return 0;
  }
  return -1;
}


int zxwRadioGetActive (ZXWidget *g) {
  ZXWRadioData *data = zxwWidgetHeaderEx(g, ZXWT_RADIO);
  //
  return (data != NULL ? data->active : -1);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
  char *text;
  int textlen;
  int textsize;
  int curpos; // in chars
  int xofs; // in pixels
} ZXWLineEditData;


static void wLEPaintCB (ZXWidget *g, VExSurface *sfc) {
  ZXWLineEditData *data = (ZXWLineEditData *)g->udata;
  int cflg = buildClrFlags(g);
  Uint8 i = zxwFindColor(g, "lineedit", "ink", cflg);
  Uint8 p = zxwFindColor(g, "lineedit", "paper", cflg);
  int y = (g->h-8)/2;
  //
  vDrawBar(sfc, 0, 0, g->w, g->h, p);
  vDrawText(sfc, data->xofs, y, data->text, i, 255);
  //
  if (data->xofs) {
    Uint8 i = zxwFindColor(g, "lineedit", "arrow_ink", cflg);
    //
    vDrawBar(sfc, 0, y, 5, 8, p);
    vDrawLeftArrow(sfc, 1, y+1, i, 255);
  }
  //
  if (zxwIsActive(g) && SDL_GetTicks()%800 < 400) {
    vDrawBar(sfc, data->curpos*6+data->xofs, y, 6, 8, i);
  }
}


//TODO: convert ch to cp866
static int leInsChar (ZXWLineEditData *data, int ch) {
  if (ch >= 32 && ch < 128) {
    int len = data->textlen;
    //
    if (len+2 > data->textsize) {
      int newsz = len+512;
      char *nd = realloc(data->text, newsz);
      //
      if (nd == NULL) return 1;
      data->text = nd;
      data->textsize = newsz;
    }
    //
    memmove(data->text+data->curpos+1, data->text+data->curpos, strlen(data->text+data->curpos)+1);
    data->text[data->curpos++] = ch;
    ++(data->textlen);
    return 1;
  }
  //
  if (ch == 8) {
    if (data->curpos > 0) {
      --(data->curpos);
      --(data->textlen);
      memmove(data->text+data->curpos, data->text+data->curpos+1, strlen(data->text+data->curpos));
    }
    return 1;
  }
  //
  return 0;
}


static void leNormOfs (ZXWidget *g, ZXWLineEditData *data) {
  int len = strlen(data->text);
  //
  if (data->curpos < 0) data->curpos = 0; else if (data->curpos > len) data->curpos = len;
  if (data->curpos > g->w/6-1) {
    // len: length
    // g->w/6: visible chars
    // data->curpos: curpos
    data->xofs = -((data->curpos*6)-(g->w-6));
  } else {
    data->xofs = 0;
  }
}


static int wLEKeyCB (ZXWidget *g, SDL_KeyboardEvent *key) {
  if (zxwCanProcessKeys(g) && key->type == SDL_KEYDOWN && zxwIsActive(g)) {
    ZXWLineEditData *data = (ZXWLineEditData *)g->udata;
    //
    switch (key->keysym.sym) {
      case SDLK_DELETE:
        if (data->curpos < strlen(data->text)) {
          --(data->textlen);
          memmove(data->text+data->curpos, data->text+data->curpos+1, strlen(data->text+data->curpos));
          leNormOfs(g, data);
        }
        return 1;
      case SDLK_TAB: return 0;
      case SDLK_LEFT: case SDLK_KP4:
        if (data->curpos > 0) {
          --(data->curpos);
          leNormOfs(g, data);
        }
        return 1;
      case SDLK_RIGHT: case SDLK_KP6:
        if (data->curpos < strlen(data->text)) {
          ++(data->curpos);
          leNormOfs(g, data);
        }
        return 1;
      case SDLK_HOME: case SDLK_KP7:
        data->curpos = 0;
        leNormOfs(g, data);
        return 1;
      case SDLK_END: case SDLK_KP1:
        data->curpos = strlen(data->text);
        leNormOfs(g, data);
        return 1;
      default: ;
    }
    if (key->keysym.unicode != 0) {
      if (leInsChar(data, key->keysym.unicode)) {
        leNormOfs(g, data);
        return 1;
      }
    }
  }
  //
  return 0;
}


static int wLEMouseCB (ZXWidget *g, int x, int y, int btn, int buttons) {
  if (btn == MS_BUTTON_LEFT && isXYInWidget(g, x, y)) {
    //TODO: position cursor
    g->window->wmbdown = g;
    zxwActivate(g);
    zxwClick(g);
    return 1;
  }
  //
  if (btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED)) {
    if (g->window->wmbdown == g) {
      g->window->wmbdown = NULL;
      return 1;
    }
    if (g->window->wmbdown == NULL && isXYInWidget(g, x, y)) return 1;
  }
  //
  return 0;
}


static void wLEClearCB (ZXWidget *g, ZXWWidgetDataHeader *hdr) {
  ZXWLineEditData *data = (ZXWLineEditData *)g->udata;
  //
  if (data->text != NULL) free(data->text);
  data->text = NULL;
}


ZXWidget *zxwNewLineEdit (ZXWindow *w, const char *id,
  int x, int y, int aw, int ah,
  const char *aText)
{
  ZXWidget *g;
  ZXWLineEditData *data;
  //
  if (aw < 0) aw = (aText != NULL ? strlen(aText) : 10)*6+6;
  if (ah < 0) ah = 8;
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_LINEEDIT, id, x, y, aw, ah, sizeof(ZXWLineEditData))) == NULL) return NULL;
  g->flags = ZXW_CANFOCUS;
  //
  data = (ZXWLineEditData *)g->udata;
  data->hdr.clearCB = wLEClearCB;
  data->text = strdup(aText != NULL ? aText : "");
  data->textsize = (data->textlen = data->curpos = strlen(data->text))+1;
  leNormOfs(g, data);
  //
  g->paintCB = wLEPaintCB;
  g->keyCB = wLEKeyCB;
  g->mouseCB = wLEMouseCB;
  //
  return g;
}


const char *zxwLineEditText (ZXWidget *g) {
  ZXWLineEditData *data = zxwWidgetHeaderEx(g, ZXWT_LINEEDIT);
  //
  return (data != NULL ? data->text : NULL);
}


int zxwLineEditSetText (ZXWidget *g, const char *txt) {
  ZXWLineEditData *data = zxwWidgetHeaderEx(g, ZXWT_LINEEDIT);
  //
  if (txt == NULL) txt = "";
  if (data != NULL) {
    char *nd = realloc(data->text, strlen(txt)+1);
    //
    if (nd != NULL) {
      data->text = nd;
      return 0;
    }
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
  int vmin;
  int vmax;
  int pos;
  int vert;
  int pagesize;
  ZXWidget *link;
  int64_t lastLMB;
  int maction; // SBBTP_xxx
  int knobmsofs; // mouse offset from knob start for doknobmove
} ZXWSBarData;


static void sbarCallLinked (ZXWidget *sb) {
  ZXWSBarData *data = (ZXWSBarData *)sb->udata;
  //
  if (data->link != NULL) {
    ZXWWidgetDataHeader *dh = zxwWidgetHeader(data->link);
    //
    if (dh != NULL && dh->sbarCB != NULL) dh->sbarCB(data->link, sb);
  }
}


static void sbarKnobInfo (ZXWidget *g, int *kpos, int *ksize) {
  ZXWSBarData *data = (ZXWSBarData *)g->udata;
  int len = data->vmax-data->vmin;
  int wh = (data->vert ? g->h : g->w)-12;
  int pos = data->pos-data->vmin;
  //
  if (len > 0) {
    if (wh-len < 4) {
      // knob is too small
      if (kpos != NULL) *kpos = (wh-4)*pos/len;
      if (ksize != NULL) *ksize = 4;
    } else {
      // big knob
      if (kpos != NULL) *kpos = pos;
      if (ksize != NULL) *ksize = wh-len;
    }
  } else {
    if (kpos != NULL) *kpos = 0;
    if (ksize != NULL) *ksize = wh;
  }
}


// ofs from the start
static int sbarKnobOfs2Pos (ZXWidget *g, int ofs) {
  ZXWSBarData *data = (ZXWSBarData *)g->udata;
  int len = data->vmax-data->vmin;
  int wh = (data->vert ? g->h : g->w)-12;
  int pos;
  //
  if (len > 0) {
    if (wh-len < 4) {
      // knob is too small
      if (ofs > wh-4) ofs = wh-4;
      if (ofs < 0) ofs = 0;
      pos = len*ofs/(wh-4);
    } else {
      // big knob
      if (ofs > wh-(wh-len)) ofs = wh-(wh-len);
      if (ofs < 0) ofs = 0;
      pos = ofs;
    }
  } else {
    pos = 0;
  }
  if ((pos += data->vmin) > data->vmax) pos = data->vmax;
  //
  return pos;
}


static void wSBarPaintCB (ZXWidget *g, VExSurface *sfc) {
  ZXWSBarData *data = (ZXWSBarData *)g->udata;
  int cflg = buildClrFlags(g);
  //
  if (cflg != ZXWCLR_DISABLED && data->link != NULL && zxwIsActive(data->link)) cflg = ZXWCLR_ACTIVE;
  //
  Uint8 i = zxwFindColor(g, "scrollbar", "ink", cflg);
  Uint8 p = zxwFindColor(g, "scrollbar", "paper", cflg);
  Uint8 ki = zxwFindColor(g, "scrollbar", "knob_ink", cflg);
  Uint8 di = zxwFindColor(g, "scrollbar", "ink", ZXWCLR_DISABLED);
  int kpos, ksize;
  //
  sbarKnobInfo(g, &kpos, &ksize);
  //
  vDrawBar(sfc, 0, 0, g->w, g->h, p);
  //
  if (data->vert) {
    vDrawHLine(sfc, 0, 5, g->w, i);
    vDrawHLine(sfc, 0, g->h-6, g->w, i);
    vDrawUpArrow(sfc, 1, 1, (data->pos > data->vmin ? i : di), 255);
    vDrawDownArrow(sfc, 1, g->h-4, (data->pos < data->vmax ? i : di), 255);
    //
    vDrawBar(sfc, 0, 6+kpos, 7, ksize, ki);
  } else {
    vDrawVLine(sfc, 5, 0, g->h, i);
    vDrawVLine(sfc, g->w-6, 0, g->h, i);
    vDrawLeftArrow(sfc, 1, 1, (data->pos > data->vmin ? i : di), 255);
    vDrawRightArrow(sfc, g->w-4, 1, (data->pos < data->vmax ? i : di), 255);
    //
    vDrawBar(sfc, 6+kpos, 0, ksize, 7, ki);
  }
}


static int wSBarKeyCB (ZXWidget *g, SDL_KeyboardEvent *key) {
  if (zxwCanProcessKeys(g) && key->type == SDL_KEYDOWN && zxwIsActive(g)) {
    ZXWSBarData *data = (ZXWSBarData *)g->udata;
    //
    switch (key->keysym.sym) {
      case SDLK_LEFT: case SDLK_KP4:
        if (!data->vert) {
          if (data->pos > data->vmin) {
            --(data->pos);
            sbarCallLinked(g);
          }
          return 1;
        }
        break;
      case SDLK_RIGHT: case SDLK_KP6:
        if (!data->vert) {
          if (data->pos < data->vmax) {
            ++(data->pos);
            sbarCallLinked(g);
          }
          return 1;
        }
        break;
      case SDLK_UP: case SDLK_KP8:
        if (data->vert) {
          if (data->pos > data->vmin) {
            --(data->pos);
            sbarCallLinked(g);
          }
          return 1;
        }
        break;
      case SDLK_DOWN: case SDLK_KP2:
        if (data->vert) {
          if (data->pos < data->vmax) {
            ++(data->pos);
            sbarCallLinked(g);
          }
          return 1;
        }
        break;
      case SDLK_HOME: case SDLK_KP7:
        data->pos = data->vmin;
        return 1;
      case SDLK_END: case SDLK_KP3:
        data->pos = data->vmax;
        return 1;
      default: ;
    }
  }
  //
  return 0;
}


enum {
  SBBTP_NONE,
  SBBTP_KNOB,
  SBBTP_UP,
  SBBTP_DOWN,
  SBBTP_PGUP,
  SBBTP_PGDOWN
};


static int sbarWhereIsMouse (ZXWidget *g, int x, int y, int *kbofs) {
  if (kbofs != NULL) *kbofs = 0;
  //
  if (isXYInWidget(g, x, y)) {
    ZXWSBarData *data = (ZXWSBarData *)g->udata;
    int kpos, ksize;
    int crd = (data->vert ? y : x);
    int sz = (data->vert ? g->h : g->w);
    //
    sbarKnobInfo(g, &kpos, &ksize);
    //
    if (crd >= 0 && crd < 5) return SBBTP_UP;
    if (crd >= sz-5 && crd < sz) return SBBTP_DOWN;
    if (crd >= 6 && crd < 6+kpos) return SBBTP_PGUP;
    if (crd >= 6+kpos+ksize && crd < sz-6) return SBBTP_PGDOWN;
    if (crd >= 6+kpos && crd < 6+kpos+ksize) {
      if (kbofs != NULL) *kbofs = crd-(6+kpos);
      return SBBTP_KNOB;
    }
  }
  //
  return SBBTP_NONE;
}


static int wSBarMouseCB (ZXWidget *g, int x, int y, int btn, int buttons) {
  ZXWSBarData *data = (ZXWSBarData *)g->udata;
  //
  if (btn == (MS_BUTTON_LEFT|MS_BUTTON_AUTO) && g->window->wmbdown == g) {
    // autorepeat event
    int msw, kbofs;
    //
    msw = sbarWhereIsMouse(g, x, y, &kbofs);
    switch (data->maction) {
      case SBBTP_UP:
        if (data->pos > data->vmin) {
          if (msw == SBBTP_UP) --(data->pos);
        } else {
          data->maction = SBBTP_NONE;
        }
        break;
      case SBBTP_DOWN:
        if (data->pos < data->vmax) {
          if (msw == SBBTP_DOWN) ++(data->pos);
        } else {
          data->maction = SBBTP_NONE;
        }
        break;
      case SBBTP_PGUP:
        if (data->pos > data->vmin && msw == SBBTP_PGUP) {
          if ((data->pos -= data->pagesize) < data->vmin) data->pos = data->vmin;
        } else {
          data->maction = SBBTP_NONE;
        }
        break;
      case SBBTP_PGDOWN:
        if (data->pos < data->vmax && msw == SBBTP_PGDOWN) {
          if ((data->pos += data->pagesize) > data->vmax) data->pos = data->vmax;
        } else {
          data->maction = SBBTP_NONE;
        }
        break;
    }
    //
    if (data->maction != SBBTP_NONE) {
      sbarCallLinked(g);
      data->lastLMB = SDL_GetTicks();
    } else {
      data->lastLMB = 0;
    }
    //
    return 1;
  }
  //
  if (btn == 0 && (buttons&MS_BUTTON_LEFT) && g->window->wmbdown == g) {
    // motion event, left button pressed
    if (data->maction == SBBTP_KNOB) {
      //TODO
      int kpos, ksize;
      int crd = (data->vert ? y : x), c1;
      //
      sbarKnobInfo(g, &kpos, &ksize);
      c1 = crd-(6+kpos)-data->knobmsofs;
      //
      //fprintf(stderr, "ofs=%d; crd=%d; c1=%d\n", data->knobmsofs, crd, c1);
      zxwScrollBarSetKnobPos(g, kpos+c1);
    }
    return 1;
  }
  //
  if (g->window->wmbdown != g && (btn&(MS_BUTTON_DEPRESSED|MS_BUTTON_AUTO)) == MS_BUTTON_AUTO) return 0;
  //
  if (btn == MS_BUTTON_LEFT && isXYInWidget(g, x, y)) {
    int msw, kbofs;
    //
    g->window->wmbdown = g;
    if (data->link != NULL && zxwCanBeActivated(data->link)) {
      zxwActivate(data->link);
    } else {
      zxwActivate(g);
    }
    zxwClick(g);
    //
    data->maction = SBBTP_NONE;
    data->lastLMB = 0;
    msw = sbarWhereIsMouse(g, x, y, &kbofs);
    switch (msw) {
      case SBBTP_UP:
        if (data->pos > data->vmin) --(data->pos); else msw = SBBTP_NONE;
        break;
      case SBBTP_DOWN:
        if (data->pos < data->vmax) ++(data->pos); else msw = SBBTP_NONE;
        break;
      case SBBTP_PGUP:
        if (data->pos > data->vmin) {
          if ((data->pos -= data->pagesize) < data->vmin) data->pos = data->vmin;
        } else {
          msw = SBBTP_NONE;
        }
        break;
      case SBBTP_PGDOWN:
        if (data->pos < data->vmax) {
          if ((data->pos += data->pagesize) > data->vmax) data->pos = data->vmax;
        } else {
          msw = SBBTP_NONE;
        }
        break;
      case SBBTP_KNOB:
        data->knobmsofs = kbofs;
        break;
    }
    //
    if (msw != SBBTP_NONE) {
      sbarCallLinked(g);
      data->maction = msw;
      data->lastLMB = SDL_GetTicks();
    } else {
      data->maction = 0;
      data->lastLMB = 0;
    }
    //
    return 1;
  }
  //
  if (btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED)) {
    data->maction = 0;
    data->lastLMB = 0;
    if (g->window->wmbdown == g) {
      g->window->wmbdown = NULL;
      return 1;
    }
    if (g->window->wmbdown == NULL && isXYInWidget(g, x, y)) return 1;
  }
  //
  return 0;
}


ZXWidget *zxwNewScrollBar (ZXWindow *w, const char *id, int x, int y, int aSizePx, int isvert) {
  ZXWidget *g;
  ZXWSBarData *data;
  //
  if (aSizePx < 0) aSizePx = 64;
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_SCROLLBAR, id, x, y, (isvert ? 7 : aSizePx), (!isvert ? 7 : aSizePx), sizeof(ZXWSBarData))) == NULL) return NULL;
  g->flags = ZXW_CANFOCUS;
  //
  data = (ZXWSBarData *)g->udata;
  data->vmin = 0;
  data->vmax = 100;
  data->pos = 0;
  data->vert = !!isvert;
  data->pagesize = 32;
  //
  g->paintCB = wSBarPaintCB;
  g->keyCB = wSBarKeyCB;
  g->mouseCB = wSBarMouseCB;
  //
  return g;
}


int zxwScrollBarIsVertical (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  return (data != NULL ? data->vert : -1);
}


int zxwScrollBarPos (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  return (data != NULL ? data->pos : -1);
}


int zxwScrollBarMin (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  return (data != NULL ? data->vmin : -1);
}


int zxwScrollBarMax (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  return (data != NULL ? data->vmax : -1);
}


int zxwScrollBarSetPos (ZXWidget *g, int aPos) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL) {
    if (aPos < data->vmin) aPos = data->vmin;
    if (aPos > data->vmax) aPos = data->vmax;
    if (data->pos != aPos) {
      data->pos = aPos;
      sbarCallLinked(g);
    }
    return 0;
  }
  return -1;
}


int zxwScrollBarSetMin (ZXWidget *g, int aVal) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL) {
    if (aVal > data->vmax) aVal = data->vmax;
    data->vmax = aVal;
    if (data->pos > data->vmax) {
      data->pos = data->vmax;
      sbarCallLinked(g);
    }
    return 0;
  }
  return -1;
}


int zxwScrollBarSetMax (ZXWidget *g, int aVal) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL) {
    if (aVal < data->vmin) aVal = data->vmin;
    data->vmax = aVal;
    if (data->pos < data->vmin) {
      data->pos = data->vmin;
      sbarCallLinked(g);
    }
    return 0;
  }
  return -1;
}


int zxwScrollBarSetMinMax (ZXWidget *g, int aMin, int aMax) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL) {
    int newp = data->pos;
    //
    if (aMin > aMax) aMin = aMax;
    data->vmin = aMin;
    data->vmax = aMax;
    if (newp < data->vmin) newp = data->vmin;
    if (newp > data->vmax) newp = data->vmax;
    if (data->pos != newp) {
      data->pos = newp;
      sbarCallLinked(g);
    }
    return 0;
  }
  return -1;
}


ZXWidget *zxwScrollBarLinked (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  return (data != NULL ? data->link : NULL);
}


int zxwScrollBarSetLinked (ZXWidget *g, ZXWidget *link) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL && (link == NULL || zxwIsAlive(link))) {
    data->link = link;
    return 0;
  }
  //
  return -1;
}


int zxwScrollBarPageSize (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  return (data != NULL ? data->pagesize : -1);
}


int zxwScrollBarSetPageSize (ZXWidget *g, int pgsz) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL && pgsz > 1) {
    data->pagesize = pgsz;
    return 0;
  }
  //
  return -1;
}


int zxwScrollBarKnobPos (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL) {
    int kpos, ksize;
    //
    sbarKnobInfo(g, &kpos, &ksize);
    return kpos;
  }
  //
  return -1;
}


int zxwScrollBarKnobSize (ZXWidget *g) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL) {
    int kpos, ksize;
    //
    sbarKnobInfo(g, &kpos, &ksize);
    return ksize;
  }
  //
  return -1;
}


int zxwScrollBarSetKnobPos (ZXWidget *g, int pos) {
  ZXWSBarData *data = zxwWidgetHeaderEx(g, ZXWT_SCROLLBAR);
  //
  if (data != NULL) {
    int np = sbarKnobOfs2Pos(g, pos);
    //int kpos, ksize;
    //
    //sbarKnobInfo(g, &kpos, &ksize);
    //fprintf(stderr, "okp: %d; nkp: %d; pos: %d; np: %d\n", kpos, pos, data->pos, np);
    //
    if (np != data->pos) { data->pos = np; sbarCallLinked(g); }
    return 0;
  }
  //
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  ZXWWidgetDataHeader hdr;
  ZXWidget *sbh;
  ZXWidget *sbv;
  //
  int multisel;
  //
  int top; // top visible item
  int cur; // current item
  //
  int maxlen;
  int count;
  int amount;
  char **items;
  int *marks;
} ZXWListBoxData;


static int lbGrow (ZXWidget *g, int newsize) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  if (newsize > data->amount) {
    char **newi;
    int *newm;
    //
    newsize = (newsize|0x3f)+1;
    if ((newi = realloc(data->items, sizeof(char *)*newsize)) == NULL) return -1;
    data->items = newi;
    if ((newm = realloc(data->marks, sizeof(char *)*newsize)) == NULL) return -1;
    data->marks = newm;
    //
    data->amount = newsize;
  }
  return 0;
}


static void lbRecalcWdt (ZXWidget *g) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  data->maxlen = 0;
  for (int f = data->count-1; f >= 0; --f) {
    int l = strlen(data->items[f]);
    //
    if (l > data->maxlen) data->maxlen = l;
  }
}


static void lbFixCursor (ZXWidget *g) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  if (data->count != 0) {
    if (data->cur >= data->count) data->cur = data->count-1;
    if (data->cur < 0) data->cur = 0;
    if (data->cur < data->top) {
      data->top = data->cur;
    } else {
      int pe = data->top+g->h/8; // first out-of-page item
      //
      if (pe > data->count) pe = data->count;
      if (data->cur >= pe) data->top = data->cur-(g->h/8-1);
    }
    if (data->top < 0) data->top = 0;
  } else {
    data->cur = data->top = 0;
  }
  if (data->sbv != NULL) zxwScrollBarSetPos(data->sbv, data->cur);
}


static void wLBoxSBarCB (ZXWidget *g, ZXWidget *sb) {
  ZXWListBoxData *data = (ZXWListBoxData *)g->udata;
  int pos = zxwScrollBarPos(sb);
  //
  if (zxwScrollBarIsVertical(sb)) {
    if (pos != data->cur) {
      data->cur = pos;
      lbFixCursor(g);
    }
  }
}


static void wLBoxPaintCB (ZXWidget *g, VExSurface *sfc) {
  ZXWListBoxData *data = (ZXWListBoxData *)g->udata;
  int cflg = buildClrFlags(g);
  Uint8 i = zxwFindColor(g, "listbox", "ink", cflg);
  Uint8 p = zxwFindColor(g, "listbox", "paper", cflg);
  Uint8 mi = zxwFindColor(g, "listbox", "mark_ink", cflg);
  Uint8 mp = zxwFindColor(g, "listbox", "mark_paper", cflg);
  Uint8 ci = zxwFindColor(g, "listbox", "cursor_ink", cflg);
  Uint8 cp = zxwFindColor(g, "listbox", "cursor_paper", cflg);
  Uint8 cmi = zxwFindColor(g, "listbox", "curmark_ink", cflg);
  Uint8 cmp = zxwFindColor(g, "listbox", "curmark_paper", cflg);
  int idx = data->top;
  //
  vDrawBar(sfc, 0, 0, g->w, g->h, p);
  for (int y = 0; y < g->h; y += 8) {
    if (idx < data->count) {
      Uint8 ti, tp;
      //
      if (idx == data->cur) {
        if (data->marks[idx]) { ti = cmi; tp = cmp; } else { ti = ci; tp = cp; }
      } else {
        if (data->marks[idx]) { ti = mi; tp = mp; } else { ti = i; tp = p; }
      }
      vDrawBar(sfc, 0, y, g->w, 8, tp);
      vDrawText(sfc, 0, y, data->items[idx], ti, 255);
      ++idx;
    } else {
      break;
    }
  }
}


static void lbPageUp (ZXWidget *g) {
  ZXWListBoxData *data = (ZXWListBoxData *)g->udata;
  //
  if (g->h >= 8) {
    if (data->cur != data->top) {
      data->cur = data->top;
    } else {
      if (g->h/8 > 1) data->cur -= g->h/8-1; else data->cur -= g->h/8;
    }
    lbFixCursor(g);
  }
}


static void lbPageDown (ZXWidget *g) {
  ZXWListBoxData *data = (ZXWListBoxData *)g->udata;
  //
  if (g->h >= 8) {
    if (g->h/8 > 1 && data->cur != data->top+g->h/8-1) {
      data->cur = data->top+g->h/8-1;
    } else {
      if (g->h/8 > 1) data->cur += g->h/8-1; else data->cur += g->h/8;
    }
    lbFixCursor(g);
  }
}


static int wLBoxKeyCB (ZXWidget *g, SDL_KeyboardEvent *key) {
  if (zxwCanProcessKeys(g) && key->type == SDL_KEYDOWN && zxwIsActive(g)) {
    ZXWListBoxData *data = (ZXWListBoxData *)g->udata;
    //
    switch (key->keysym.sym) {
      case SDLK_UP: case SDLK_KP8:
        if (data->cur > 0) {
          --(data->cur);
          lbFixCursor(g);
        }
        return 1;
      case SDLK_DOWN: case SDLK_KP2:
        if (data->cur < data->count-1) {
          ++(data->cur);
          lbFixCursor(g);
        }
        return 1;
      case SDLK_PAGEUP: case SDLK_KP9:
        lbPageUp(g);
        return 1;
      case SDLK_PAGEDOWN: case SDLK_KP3:
        lbPageDown(g);
        return 1;
      case SDLK_HOME: case SDLK_KP7:
        if (data->cur != 0) {
          data->cur = 0;
          lbFixCursor(g);
        }
        return 1;
      case SDLK_END: case SDLK_KP1:
        if (data->count > 0 && data->cur != data->count-1) {
          data->cur = data->count-1;
          lbFixCursor(g);
        }
        return 1;
      case SDLK_SPACE:
        if (data->multisel && data->count > 0) {
          data->marks[data->cur] = !data->marks[data->cur];
        }
        return 1;
      default: ;
    }
  }
  //
  return 0;
}


static int wLBoxMouseCB (ZXWidget *g, int x, int y, int btn, int buttons) {
  ZXWListBoxData *data = (ZXWListBoxData *)g->udata;
  //
  if (btn == MS_BUTTON_LEFT && isXYInWidget(g, x, y)) {
    int pst = data->top+(y/8);
    //
    g->window->wmbdown = g;
    zxwActivate(g);
    zxwClick(g);
    //
    if (pst >= 0 && pst < data->count) {
      data->cur = pst;
      lbFixCursor(g);
      //if (data->multisel) data->marks[pst] = !data->marks[pst];
    }
    //
    return 1;
  }
  //
  if (btn == MS_BUTTON_RIGHT && isXYInWidget(g, x, y)) {
    int pst = data->top+(y/8);
    //
    zxwActivate(g);
    if (pst >= 0 && pst < data->count) {
      if (data->multisel) data->marks[pst] = !data->marks[pst];
    }
    //
    return 1;
  }
  //
  if ((btn == MS_BUTTON_WHEELUP || btn == MS_BUTTON_WHEELDOWN) && isXYInWidget(g, x, y)) {
    if (btn == MS_BUTTON_WHEELUP) --(data->cur); else ++(data->cur);
    lbFixCursor(g);
    //
    return 1;
  }
  //
  if (btn == (MS_BUTTON_LEFT|MS_BUTTON_DEPRESSED)) {
    if (g->window->wmbdown == g) {
      g->window->wmbdown = NULL;
      return 1;
    }
    if (g->window->wmbdown == NULL && isXYInWidget(g, x, y)) return 1;
  }
  //
  return 0;
}


static void wLBoxClearCB (ZXWidget *g, ZXWWidgetDataHeader *hdr) {
  ZXWListBoxData *data = (ZXWListBoxData *)g->udata;
  //
  for (int f = data->count-1; f >= 0; --f) free(data->items[f]);
  if (data->items != NULL) free(data->items);
  if (data->marks != NULL) free(data->marks);
  data->items = NULL;
  data->marks = NULL;
  data->count = data->amount = 0;
  //
  if (data->sbv != NULL) zxwRemoveWidget(data->sbv);
  if (data->sbh != NULL) zxwRemoveWidget(data->sbh);
}


ZXWidget *zxwNewListBox (ZXWindow *w, const char *id, int x, int y, int aw, int ah) {
  ZXWidget *g;
  ZXWListBoxData *data;
  //
  if (aw < 0) aw = 32;
  if (ah < 0) ah = 32;
  //
  if ((g = zxwNewCommonWidget(w, ZXWT_LISTBOX, id, x, y, aw, ah, sizeof(ZXWListBoxData))) == NULL) return NULL;
  g->flags = ZXW_CANFOCUS;
  //
  data = (ZXWListBoxData *)g->udata;
  data->hdr.clearCB = wLBoxClearCB;
  data->hdr.sbarCB = wLBoxSBarCB;
  //
  g->paintCB = wLBoxPaintCB;
  g->keyCB = wLBoxKeyCB;
  g->mouseCB = wLBoxMouseCB;
  //
  return g;
}


int zxwListBoxIsHBar (ZXWidget *g) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  return (data != NULL && data->sbh != NULL && zxwIsVisible(data->sbh));
}


int zxwListBoxIsVBar (ZXWidget *g) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  return (data != NULL && data->sbv != NULL && zxwIsVisible(data->sbv));
}


int zxwListBoxSetHBar (ZXWidget *g, int onflag) {
  return 0;//FIXME
}


int zxwListBoxSetVBar (ZXWidget *g, int onflag) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  if (data != NULL) {
    if (onflag) {
      int pgsz = (g->h/8);
      //
      if (pgsz < 1) pgsz = 1;
      //
      if (data->sbv != NULL && data->sbv->h != g->h) {
        zxwRemoveWidget(data->sbv);
        data->sbv = NULL;
      }
      //
      if (data->sbv == NULL) {
        int cpos = data->cur, top = data->top;
        //
        data->sbv = zxwNewScrollBar(g->window, NULL, g->x+g->w, g->y, g->h, 1);
        data->sbv->flags &= ~ZXW_CANFOCUS;
        zxwScrollBarSetMax(data->sbv, data->count);
        zxwScrollBarSetPos(data->sbv, cpos);
        zxwScrollBarSetLinked(data->sbv, g);
        data->cur = cpos;
        data->top = top;
      } else {
        data->sbv->x = g->x+g->w;
        data->sbv->y = g->y;
      }
      zxwScrollBarSetPageSize(data->sbv, pgsz);
    } else {
      if (data->sbv != NULL) {
        zxwRemoveWidget(data->sbv);
        data->sbv = NULL;
      }
    }
    return 0;
  }
  //
  return -1;
}


int zxwListBoxIsMulti (ZXWidget *g) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  return (data != NULL ? data->multisel : -1);
}


int zxwListBoxSetMulti (ZXWidget *g, int multi) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  if (data != NULL) {
    data->multisel = !!multi;
    return 0;
  }
  //
  return -1;
}


int zxwListBoxCount (ZXWidget *g) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  return (data != NULL ? data->count : -1);
}


const char *zxwListBoxItem (ZXWidget *g, int idx) {
  if (idx >= 0) {
    ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
    //
    return (data != NULL && idx < data->count ? data->items[idx] : NULL);
  }
  //
  return NULL;
}


int zxwListBoxSetItem (ZXWidget *g, int idx, const char *txt) {
  if (idx >= 0) {
    ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
    //
    if (data != NULL && idx < data->count) {
      char *ns = strdup(txt != NULL ? txt : "");
      //
      if (ns != NULL) {
        int recalcwdt = (strlen(ns) >= data->maxlen ? 1 : (strlen(data->items[idx]) == data->maxlen ? -1 : 0));
        //
        free(data->items[idx]);
        data->items[idx] = ns;
        data->marks[idx] = 0;
        if (recalcwdt < 0) lbRecalcWdt(g); else data->maxlen = strlen(ns);
        return 0;
      }
    }
  }
  //
  return -1;
}


int zxwListBoxDeleteItem (ZXWidget *g, int idx) {
  if (idx >= 0) {
    ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
    //
    if (data != NULL && idx < data->count) {
      int recalcwdt = (strlen(data->items[idx]) == data->maxlen);
      //
      free(data->items[idx]);
      if (idx != data->count-1) {
        // non-last item
        for (int f = idx+1; f < data->count; ++f) {
          data->items[f-1] = data->items[f];
          data->marks[f-1] = data->marks[f];
        }
      }
      --(data->count);
      if (recalcwdt) lbRecalcWdt(g);
      if (data->sbv != NULL) zxwScrollBarSetMax(data->sbv, data->count-1);
      lbFixCursor(g);
      return 0;
    }
  }
  //
  return -1;
}


int zxwListBoxInsertItem (ZXWidget *g, int idx, const char *txt) {
  if (idx >= 0) {
    ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
    //
    if (data != NULL && idx <= data->count) {
      char *ns = strdup(txt != NULL ? txt : "");
      //
      if (ns != NULL) {
        int recalcwdt = (strlen(ns) > data->maxlen);
        //
        if (lbGrow(g, data->count+1) < 0) { free(ns); return -1; } // alas
        if (idx != data->count) {
          // non-last item
          for (int f = data->count; f >= idx; --f) {
            data->items[f] = data->items[f-1];
            data->marks[f] = data->marks[f-1];
          }
        }
        data->items[idx] = ns;
        data->marks[idx] = 0;
        ++(data->count);
        if (recalcwdt) lbRecalcWdt(g);
        if (data->sbv != NULL) zxwScrollBarSetMax(data->sbv, data->count-1);
        lbFixCursor(g);
        return 0;
      }
    }
  }
  //
  return -1;
}


int zxwListBoxAddItem (ZXWidget *g, const char *txt) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  return (data != NULL ? zxwListBoxInsertItem(g, data->count, txt) : -1);
}


int zxwListBoxIsMarked (ZXWidget *g, int idx) {
  if (idx >= 0) {
    ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
    //
    return (data != NULL && idx < data->count ? data->marks[idx] : -1);
  }
  //
  return -1;
}


int zxwListBoxSetMarked (ZXWidget *g, int idx, int mark) {
  if (idx >= 0) {
    ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
    //
    if (data != NULL && idx < data->count) {
      data->marks[idx] = mark;
      return 0;
    }
  }
  //
  return -1;
}


int zxwListBoxCurrent (ZXWidget *g) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  return (data != NULL ? data->cur : -1);
}


int zxwListBoxSetCurrent (ZXWidget *g, int idx) {
  ZXWListBoxData *data = zxwWidgetHeaderEx(g, ZXWT_LISTBOX);
  //
  if (data != NULL && idx >= 0 && idx < data->count) {
    if (data->cur != idx) {
      data->cur = idx;
      lbFixCursor(g);
    }
    return 0;
  }
  //
  return -1;
}
