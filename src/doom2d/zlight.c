#include <math.h>


////////////////////////////////////////////////////////////////////////////////
/* lightbuf format (bytes):
 * (b,g,r,a) triplets
 * (b,g,r) are premultiplied (wow! i'm smart!)
 */
static uint32_t *lightbuf = NULL;
static int lb_size = 0;


////////////////////////////////////////////////////////////////////////////////
GCC_DESTRUCTOR_SYSTEM {
  if (lightbuf != NULL) free(lightbuf);
}


void lb_reset (int resize) {
  if (resize && lightbuf != NULL) {
    free(lightbuf);
    lightbuf = NULL;
  }
  //
  if (lightbuf == NULL) {
    lb_size = (SCRW*4)*SCRH;
    lightbuf = malloc(lb_size);
  }
  memset(lightbuf, 0, lb_size);
}


static ccBool r_lights_additive = CC_FALSE;
CONVAR_BOOL(r_lights_additive, r_lights_additive, 0, "additive intensity lights?")


static inline void lb_add_rgb (int x, int y, Uint32 bgr, Uint8 alpha) {
  uint32_t *pos = lightbuf+SCRW*y+x, aa;
  //
  if_expect (!r_lights_additive, 1) {
    aa = K8MAX(alpha, (pos[0]>>24));
  } else {
    aa = byteclamp((pos[0]>>24)+alpha);
  }
  // premultiply light color
  if (bgr) {
    uint32_t add, add_nc, cbits, cmask;
    //
    bgr =
      ((((bgr&0xff00ff)*(alpha+1)+0x800080)>>8)&0xff00ff)|
      ((((bgr&0x00ff00)*(alpha+1)+0x008000)>>8)&0x00ff00);
    // you are not expected to understand this
    // but if you are really curious, this is
    // 8-bit rgb adding with clamping
    add = (pos[0]&0xffffffu)+bgr;
    add_nc = (pos[0]&0xffffffu)^bgr;
    cbits = (add^add_nc)&0x01010100;
    cmask = cbits-(cbits>>8);
    bgr = ((add^cbits)|cmask)&0xffffffu;
    *pos = bgr|(aa<<24);
  } else {
    *pos = (pos[0]&0x00ffffffu)|(aa<<24);
  }
}


void lbx_draw (int x, int y, int w, int h) {
  //x += vga_xofs;
  //y += vga_yofs;
  if (x+w >= vidcx1 && x < vidcx2 && y+h >= vidcy1 && y < vidcy2) {
    int xlen = w, ylen = h;
    //
    if (y < vidcy1) { ylen -= vidcy1-y; y = vidcy1; }
    if (y+ylen > vidcy2) ylen -= y+ylen-vidcy2;
    if (x < vidcx1) { xlen -= vidcx1-x; x = vidcx1; }
    if (x+xlen > vidcx2) xlen -= x+xlen-vidcx2;
    //
    const Uint8 *lm = scrlightmap+SCRW*y+x;
    const Uint8 *src = vscrbuf+SCRW*y+x;
    const uint8_t *pos = (uint8_t *)(lightbuf+SCRW*y+x);
    Uint8 *sd = (((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4);
    //
    for (int ly = ylen; ly > 0; --ly) {
      for (int lx = xlen; lx > 0; --lx, ++src, sd += 4, pos += 4) memcpy(sd, &palette[*src], 4);
      lm += SCRW-xlen;
      src += SCRW-xlen;
      sd += frameSfc->pitch-(4*xlen);
      pos += (SCRW*4)-(4*xlen);
    }
  }
}


void lb_draw (int x, int y, int w, int h) {
  //x += vga_xofs;
  //y += vga_yofs;
  if (x+w >= vidcx1 && x < vidcx2 && y+h >= vidcy1 && y < vidcy2) {
    int xlen = w, ylen = h;
    //
    if (y < vidcy1) { ylen -= vidcy1-y; y = vidcy1; }
    if (y+ylen > vidcy2) ylen -= y+ylen-vidcy2;
    if (x < vidcx1) { xlen -= vidcx1-x; x = vidcx1; }
    if (x+xlen > vidcx2) xlen -= x+xlen-vidcx2;
    //
    if (y < vidcy1) { ylen -= vidcy1-y; y = vidcy1; }
    if (y+ylen > vidcy2) ylen -= y+ylen-vidcy2;
    if (x < vidcx1) { xlen -= vidcx1-x; x = vidcx1; }
    if (x+xlen > vidcx2) xlen -= x+xlen-vidcx2;
    //
    const Uint8 *lm = scrlightmap+SCRW*y+x;
    const Uint8 *src = vscrbuf+SCRW*y+x;
    const uint32_t *pos = lightbuf+SCRW*y+x;
    Uint32 *sd = (Uint32 *)(((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4);
    //
    for (int ly = ylen; ly > 0; --ly) {
      for (int lx = xlen; lx > 0; --lx, ++src, ++sd, ++pos) {
        switch (*lm++) {
          case LMAP_FULLLIT:
            memcpy(sd, &palette[*src], 4);
            break;
          //case LMAP_BACK:
            //if (pos[0]) ALPHA_MIX(sd[VGA_RIDX], sd[VGA_GIDX], sd[VGA_BIDX], sd[VGA_RIDX], sd[VGA_GIDX], sd[VGA_BIDX], palRGB[*src][0], palRGB[*src][1], palRGB[*src][2], 255-pos[0]);
            //break;
          case LMAP_NORMAL:
            if_expect ((pos[0]&0xff000000u) > 0, 1) {
              // mix lights
              //const uint32_t *pp = (const uint32_t *)(pos+2);
              int32_t alpha = 256-(pos[0]>>24);
              uint32_t pic = (palette[*src]|(((uint32_t)alpha)<<24));
              uint32_t drb = (pic&0xff00ff), dg = (pic&0x00ff00);
              //
              // real pixel, do alpha
              drb = (drb+(((-drb)*alpha+0x800080)>>8))&0xff00ff;
              dg = (dg+(((-dg)*alpha+0x008000)>>8))&0x00ff00;
              // lights, premultiplied
              if_expect (pos[0]&0xffffffu, 0) {
                uint32_t add, add_nc, cbits, cmask, bgr = (drb|dg);
                //
                // you are not expected to understand this
                // but if you are really curious, this is
                // 8-bit rgb adding with clamping
                add = (pos[0]&0xffffffu)+bgr;
                add_nc = (pos[0]&0xffffffu)^bgr;
                cbits = (add^add_nc)&0x01010100;
                cmask = cbits-(cbits>>8);
                *sd = ((add^cbits)|cmask)&0xffffffu;
              } else {
                *sd = (drb|dg);
              }
            }
            break;
        }
      }
      lm += SCRW-xlen;
      src += SCRW-xlen;
      sd = (Uint32 *)(((Uint8 *)sd)+frameSfc->pitch-(4*xlen));
      pos += SCRW-xlen;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
CONCMD(plr_coords, "show current player coords") {
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  int x = pl[0].o.x;
  int y = pl[0].o.y;
  int sx = Z_MAP2SCR_X(x);
  int sy = Z_MAP2SCR_Y(y);
  conlogf("x=%d; y=%d; sx=%d; sy=%d\n", x, y, sx, sy);
  fprintf(stderr, "x=%d; y=%d; sx=%d; sy=%d\n", x, y, sx, sy);
}


CONCMD(coords, "show current mouse coords") {
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  if (!r_mouse_cursor) {
    conlogf("not in edit mode!\n");
    return;
  }
  int x = Z_SCR2MAP_X(msLastX);
  int y = Z_SCR2MAP_Y(msLastY);
  int sx = Z_MAP2SCR_X(x);
  int sy = Z_MAP2SCR_Y(y);
  conlogf("x=%d; y=%d; sx=%d; sy=%d\n", x, y, sx, sy);
  fprintf(stderr, "x=%d; y=%d; sx=%d; sy=%d\n", x, y, sx, sy);
}


////////////////////////////////////////////////////////////////////////////////
static int lcx, lcy, lrad;
static int las, lae;
static Uint8 _alpha;
static Uint32 _bgr;
static uint16_t sqrttbl[200000];

static int z_light_mindist = 0;


static void _light_constructor_ (void) {
  BJRandCtx r;
  //
  xrandomize(&r);
  for (int f = 0; f < 200000; ++f) sqrttbl[f] = sqrt(f);
}


#include "zpolymod.c"


static inline int fldhit (int x, int y) {
  x = Z_SCR2MAP_X(x)/CELW;
  y = Z_SCR2MAP_Y(y)/CELH;
  if (x >= 0 && y >= 0 && x < FLDW && y < FLDH) {
    switch (FIELD_MAP(x, y)) {
      case TILE_WALL:
      case TILE_DOORC:
        return 1;
    }
    return 0;
  }
  return 1;
}


static inline int fldhit1 (int x, int y) {
  x /= CELW;
  y /= CELH;
  if (x >= 0 && y >= 0 && x < FLDW && y < FLDH) {
    switch (FIELD_MAP(x, y)) {
      case TILE_WALL:
      case TILE_DOORC:
        return 1;
    }
    //if (FIELD_TBG(x, y)) return 1;
    return 0;
  }
  return 1;
}


#include "zlight_trc.c"


static int cspX[8192];
static int cspY[8192];
static int cspCount;


//0,2,4,6
static void buildCircleSector (int cx, int cy, int radius, int sector) {
  int sx = Z_MAP2SCR_X(cx);
  int sy = Z_MAP2SCR_Y(cy);
  int stc = cspCount;
  //
  int error = -radius, x = radius, y = 0;
  //
  cx = sx;
  cy = sy;
  //
  while (x > y) {
    int xe, ye;
    //
    switch (sector) {
      case 0: xe = cx+x; ye = cy-y; break;
      case 1: xe = cx+x; ye = cy+y; break;
      case 2: xe = cx+y; ye = cy+x; break;
      case 3: xe = cx-y; ye = cy+x; break;
      case 4: xe = cx-x; ye = cy+y; break;
      case 5: xe = cx-x; ye = cy-y; break;
      case 6: xe = cx-y; ye = cy-x; break;
      case 7: xe = cx+y; ye = cy-x; break;
    }
    if (las == 0 && lae == 359) {
      traceindir(Z_SCR2MAP_X(cx), Z_SCR2MAP_Y(cy), Z_SCR2MAP_X(xe), Z_SCR2MAP_Y(ye), &cspX[cspCount], &cspY[cspCount]);
      //xpix1(cspX[cspCount], cspY[cspCount]);
      ++cspCount;
    } else {
      int ang = (int)(atan2f(ye-cy, xe-cx)*180.0f/M_PI);
      //
      ang = (ang+360)%360;
      //
      if ((las < lae && ang >= las && ang <= lae) ||
          (las > lae && (ang >= las || ang <= lae))) {
        traceindir(Z_SCR2MAP_X(cx), Z_SCR2MAP_Y(cy), Z_SCR2MAP_X(xe), Z_SCR2MAP_Y(ye), &cspX[cspCount], &cspY[cspCount]);
        //xpix(cspX[cspCount], cspY[cspCount]);
        ++cspCount;
      }
    }
    error += y*2+1;
    ++y;
    if (error >= 0) { --x; error -= x*2; }
  }
  //
  if ((sector&0x01) == 0 && cspCount > stc+1) {
    // reverse sector points
    int end = (cspCount-stc)/2;
    //
    for (int f = 0; f < end; ++f) {
      int x = cspX[stc+f], y = cspY[stc+f];
      //
      cspX[stc+f] = cspX[cspCount-f-1];
      cspY[stc+f] = cspY[cspCount-f-1];
      cspX[cspCount-f-1] = x;
      cspY[cspCount-f-1] = y;
    }
  }
}


void Z_renderlight_sector (int x, int y, int radius, int as, int ae, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) {
  int sx = Z_MAP2SCR_X(x);
  int sy = Z_MAP2SCR_Y(y);
  //
  if (!r_lighting) goto quit;
  //fprintf(stderr, "x=%d; y=%d; sx=%d; sy=%d\n", x, y, sx, sy);
  if (sx-radius >= SCRW || sx+radius < 0 || sy-radius >= w_view_hgt+w_viewofs_vert || sy+radius < w_viewofs_vert) goto quit;
  if (radius < 2 || fldhit1(x, y) || alpha == 0 || z_light_mindist >= radius) goto quit;
  //
  lcx = sx;
  lcy = sy;
  lrad = radius;
  las = (as%360+360)%360;
  lae = (ae%360+360)%360;
  //if (las > lae) { int t = las; las = lae; lae = t; }
  //
  _alpha = alpha;
  _bgr = (r<<16)|(g<<8)|(b);
  //
  //if (las != 0 || lae != 359) fprintf(stderr, "las=%d; lae=%d\n", las, lae);
  cspCount = 0;
  for (int f = 0; f < 8; ++f) buildCircleSector(x, y, radius, (f+1+las/45)&0x07);
  if (cspCount > 2) {
    //xpix(sx, sy);
    polymod_start();
    if (las != 0 || lae != 359) polymod_add_vertex(sx, sy);
    for (int f = cspCount-1; f >= 0; --f) polymod_add_vertex(cspX[f]-1, cspY[f]+1);
    polymod_end();
    polymod_fill();
  }
quit:
  z_light_mindist = 0;
  //xpix2(sx, sy);
}


void Z_renderlight (int x, int y, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) {
  Z_renderlight_sector(x, y, radius, 0, 359, r, g, b, alpha);
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int x;
  int y;
  int radius;
  int as;
  int ae;
  Uint8 r;
  Uint8 g;
  Uint8 b;
  Uint8 alpha;
  int minrad;
} LightInfo;

#define MAX_LIGHTS  (4096)

static LightInfo lights[MAX_LIGHTS];
static int light_count;
static int level_light_count;


////////////////////////////////////////////////////////////////////////////////
void Z_reset_level_lights (void) {
  level_light_count = light_count = 0;
}


void Z_reset_lights (void) {
  light_count = level_light_count;
}


void Z_fix_level_lights (void) {
  level_light_count = light_count;
}


int Z_add_light_sector_mr (int x, int y, int radius, int as, int ae, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha, int minrad) {
  if (light_count < MAX_LIGHTS && radius >= 2 && alpha > 0 && minrad <= radius) {
    lights[light_count].x = x;
    lights[light_count].y = y;
    lights[light_count].radius = radius;
    lights[light_count].as = as;
    lights[light_count].ae = ae;
    lights[light_count].r = r;
    lights[light_count].g = g;
    lights[light_count].b = b;
    lights[light_count].alpha = alpha;
    lights[light_count].minrad = minrad;
    return light_count++;
  }
  return -1;
}


int Z_add_light_sector (int x, int y, int radius, int as, int ae, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) {
  return Z_add_light_sector_mr(x, y, radius, as, ae, r, g, b, alpha, 0);
}


int Z_add_light_mr (int x, int y, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha, int minrad) {
  return Z_add_light_sector_mr(x, y, radius, 0, 359, r, g, b, alpha, minrad);
}


int Z_add_light (int x, int y, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) {
  return Z_add_light_sector(x, y, radius, 0, 359, r, g, b, alpha);
}


void Z_render_lights (void) {
  const LightInfo *lt = lights;
  //
  for (int f = light_count; f > 0; --f, ++lt) {
    z_light_mindist = lt->minrad;
    Z_renderlight_sector(lt->x, lt->y, lt->radius, lt->as, lt->ae, lt->r, lt->g, lt->b, lt->alpha);
  }
}


////////////////////////////////////////////////////////////////////////////////
CONCMD(lights_reset, "reset static lights") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  //
  Z_reset_level_lights();
  Z_reset_lights();
}


CONCMD(lights_fix, "'fix' static lights") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  //
  Z_fix_level_lights();
}


CONCMD(lights_save, "save static lights") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  //
  FILE *fo;
  //
  if (argc != 2) {
    conlogf("%s: what?!\n", argv[0]);
    return;
  }
  //
  if ((fo = fopen(argv[1], "w")) == NULL) {
    conlogf("%s: can't create file: '%s'!\n", argv[0], argv[1]);
    return;
  }
  //
  fprintf(fo, "lights_reset\n");
  for (int f = 0; f < level_light_count; ++f) {
    fprintf(fo, "add_light_at %d %d  %d  %d %d %d  %d\n",
      lights[f].x, lights[f].y,
      lights[f].radius,
      lights[f].r, lights[f].g, lights[f].b,
      lights[f].alpha
    );
  }
  fprintf(fo, "lights_fix\n");
  //
  fclose(fo);
}


static int parse_numeric_args (int cnt, int *dest, int argc, char **argv, const int *minmax) {
  for (int f = 0; f < cnt; ++f) {
    int error;
    //
    if ((dest[f] = cmdcon_parseint(argv[f+1], -1, minmax[f*2+0], minmax[f*2+1], &error)) < 0 || error) {
      conlogf("%s: invalid arg: '%s'!\n", argv[0], argv[f+1]);
      return 0;
    }
  }
  //
  return 1;
}


CONCMD(add_light, "add level light\nadd_light radius r g b alpha") {
  static const int minmax[5*2] = {
    2, 255,
    0, 255,
    0, 255,
    0, 255,
    1, 255,
  };
  int iargs[5];
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  //
  if (argc != 6) {
    conlogf("what??\n");
    return;
  }
  //
  if (!parse_numeric_args(5, iargs, argc, argv, minmax)) return;
  //
  Z_add_light(Z_SCR2MAP_X(msLastX), Z_SCR2MAP_Y(msLastY), iargs[0], iargs[1], iargs[2], iargs[3], iargs[4]);
  Z_fix_level_lights();
}


CONCMD(add_light_at, "add level light\nadd_light_at x y radius r g b alpha") {
  static int minmax[7*2] = {
    0, /*FLDW**/8,
    0, /*FLDH**/8,
    2, 255,
    0, 255,
    0, 255,
    0, 255,
    1, 255,
  };
  int iargs[7];
  //
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  CMDCON_CHECK_NONET();
  //
  if (argc != 8) {
    conlogf("what??\n");
    return;
  }
  //
  minmax[0*2+1] = FLDW*CELW;
  minmax[1*2+1] = FLDH*CELH;
  //
  if (!parse_numeric_args(7, iargs, argc, argv, minmax)) return;
  //
  Z_add_light(iargs[0], iargs[1], iargs[2], iargs[3], iargs[4], iargs[5], iargs[6]);
  Z_fix_level_lights();
}


static int W_savelights (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(lights[0]));
    membuf_write_ui16(mbuf, level_light_count);
    membuf_write(mbuf, lights, level_light_count*sizeof(lights[0]));
  }
  //
  return 0;
}


static int W_loadlights (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    Z_reset_level_lights();
    //
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'lights' version!\n");
      goto error;
    }
    if (membuf_read_ui16(mbuf) != sizeof(lights[0])) {
      conlogf("LOADGAME ERROR: invalid 'lights' format!\n");
      goto error;
    }
    level_light_count = membuf_read_ui16(mbuf);
    if (level_light_count > MAX_LIGHTS) {
      conlogf("LOADGAME ERROR: too many level lights!\n");
      level_light_count = MAX_LIGHTS;
    }
    if (membuf_read_full(mbuf, lights, level_light_count*sizeof(lights[0])) < 0) {
      conlogf("LOADGAME ERROR: error reading level lights!\n");
      goto error;
    }
  }
  //
  if (waserror) Z_reset_level_lights(); else Z_reset_lights();
  return 0;
error:
  Z_reset_level_lights();
  return 0;
}
