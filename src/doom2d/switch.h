/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Switches
#ifndef D2D_SWITCH_H
#define D2D_SWITCH_H

#include <stdio.h>

#include "common.h"
#include "saveload.h"


enum {
  SW_NONE,
  SW_EXIT,
  SW_EXITS,
  SW_OPENDOOR,
  SW_SHUTDOOR,
  SW_SHUTTRAP,
  SW_DOOR,
  SW_DOOR5,
  SW_PRESS,
  SW_TELE,
  SW_SECRET,
  SW_LIFTUP,
  SW_LIFTDOWN,
  SW_TRAP,
  SW_LIFT,
  //
  SW_WINGAME = 64
};


extern int sw_secrets;


extern void SW_init (void);
extern void SW_alloc (void);
extern void SW_act (void);
extern void SW_draw (void);
extern int SW_press (int x, int y, int r, int h, uint8_t t, int o);

extern void SW_cheat_open (void);


#endif
