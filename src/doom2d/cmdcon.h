/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef CMDCON_H
#define CMDCON_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


////////////////////////////////////////////////////////////////////////////////
// fuckin' preprocessor trickery
#define cmdcon_lambda(_return_type, _body_and_args)  ({ \
  _return_type __fn_lmb__ _body_and_args \
  __fn_lmb__; \
})


////////////////////////////////////////////////////////////////////////////////
typedef int ccBool;

enum {
  CC_FALSE = 0,
  CC_TRUE = 1
};


////////////////////////////////////////////////////////////////////////////////
typedef enum {
  CMDCON_CMD,
  CMDCON_BOOL, // int-sized
  CMDCON_BYTE,
  CMDCON_WORD,
  CMDCON_DWORD,
  CMDCON_INT,
  CMDCON_STRING // dynamically-allocated
} CmdConType;


enum {
  CMDCON_FLAGS_NONE = 0,
  CMDCON_FLAGS_PROTECTED  = 0x10000000U, // call change callback, but don't change the var value
  CMDCON_FLAGS_NOCHANGE   = 0x20000000U, // call change callback, but don't change the var value
  CMDCON_FLAGS_READONLY   = 0x40000000U,
  CMDCON_FLAGS_PERSISTENT = 0x80000000U
};


typedef struct CmdConCmd CmdConCmd;

typedef void (*CmdConFn) (CmdConCmd *me, int argc, char *argv[]);
typedef void (*CmdConVarChangeFn) (CmdConCmd *me, const char *newval);
typedef int (*CmdConVarProtectFn) (CmdConCmd *me, const char *newval); // return 0 to protect

struct CmdConCmd {
  const char *name;
  const char *help;
  CmdConType type;
  CmdConFn fn;
  CmdConVarChangeFn cfn;
  CmdConVarProtectFn pfn; // this will be called if CMDCON_FLAGS_PROTECTED is set
  uint32_t flags;
  void *udata; // never touched by cmdcon engine
  union {
    ccBool *vbool;
    uint8_t *vu8;
    uint16_t *vu16;
    uint32_t *vu32;
    int *vint;
    char **vstr;
  };
};


////////////////////////////////////////////////////////////////////////////////
extern void cmdcon_add (const CmdConCmd *cmd);

// <0: unknown command
extern int cmdcon_exec (const char *str);

//WARNING! call cmdcon_update () if you updated some fileds
//WARNING! NEVER change the name!
extern CmdConCmd *cmdcon_find (const char *name);
extern void cmdcon_update (CmdConCmd *cmd);


////////////////////////////////////////////////////////////////////////////////
// return !0 to stop (and return this result)
typedef int (*CmdConIteratorCB) (CmdConCmd *cmd);

extern int cmdcon_enum (CmdConIteratorCB cb);


////////////////////////////////////////////////////////////////////////////////
extern void _cmdcon_default_BOOL_cmd (CmdConCmd *me, int argc, char *argv[]);
extern void _cmdcon_default_BYTE_cmd (CmdConCmd *me, int argc, char *argv[]);
extern void _cmdcon_default_WORD_cmd (CmdConCmd *me, int argc, char *argv[]);
extern void _cmdcon_default_DWORD_cmd (CmdConCmd *me, int argc, char *argv[]);
extern void _cmdcon_default_INT_cmd (CmdConCmd *me, int argc, char *argv[]);
extern void _cmdcon_default_STRING_cmd (CmdConCmd *me, int argc, char *argv[]);


////////////////////////////////////////////////////////////////////////////////
#define CONCMDEX(_name, _flags, _help) \
static void __cmdcon_##_name##__ (CmdConCmd *me, int argc, char *argv[]); \
static void __attribute__((constructor(666))) _cmdcon_##_name (void) { \
  cmdcon_add(&(CmdConCmd){ \
    .name = #_name, \
    .help = (_help), \
    .type = CMDCON_CMD, \
    .fn = __cmdcon_##_name##__, \
    .cfn = NULL, \
    .pfn = NULL, \
    .flags = (_flags), \
  }); \
} \
static void __cmdcon_##_name##__ (CmdConCmd *me, int argc, char *argv[])

#define CONCMD(_name, _help)  CONCMDEX(_name, 0, (_help))


#define _CONVAR_DECLARE_CB2(_type, _fldname, _name, _vname, _flags, _protcb, _help) \
static void __cmdcon_var##_type##_changed_##_name##__ (CmdConCmd *me, const char *newval); \
static void __attribute__((constructor(666))) _cmdcon_var##_type_##_name (void) { \
  cmdcon_add(&(CmdConCmd){ \
    .name = #_name, \
    .help = (_help), \
    .type = CMDCON_##_type, \
    .fn = _cmdcon_default_##_type##_cmd, \
    .v##_fldname = &(_vname), \
    .cfn = __cmdcon_var##_type##_changed_##_name##__, \
    .pfn = _protcb, \
    .flags = (_flags), \
  }); \
} \
static void __cmdcon_var##_type##_changed_##_name##__ (CmdConCmd *me, const char *newval)

#define _CONVAR_DECLARE_CB(_type, _fldname, _name, _vname, _flags, _help) _CONVAR_DECLARE_CB2(_type, _fldname, _name, _vname, (_flags), NULL, (_help))


#define CMDCON_HELP()  do { \
  if (argc == 2 && (strcmp(argv[1], "?") == 0 || strcmp(argv[1], "help") == 0)) { \
    conlogf("%s\n%s", me->name, me->help); \
    return; \
  } \
} while (0)


#define CONVAR_BOOL_CB(_name, _vname, _flags, _help)  _CONVAR_DECLARE_CB(BOOL, bool, _name, _vname, (_flags), (_help))
#define CONVAR_BOOL(_name, _vname, _flags, _help)     CONVAR_BOOL_CB(_name, _vname, (_flags), (_help)) {}

#define CONVAR_BYTE_CB(_name, _vname, _flags, _help)  _CONVAR_DECLARE_CB(BYTE, u8, _name, _vname, (_flags), (_help))
#define CONVAR_BYTE(_name, _vname, _flags, _help)     CONVAR_BYTE_CB(_name, _vname, (_flags), (_help)) {}

#define CONVAR_WORD_CB(_name, _vname, _flags, _help)  _CONVAR_DECLARE_CB(WORD, u16, _name, _vname, (_flags), (_help))
#define CONVAR_WORD(_name, _vname, _flags, _help)     CONVAR_WORD_CB(_name, _vname, (_flags), (_help)) {}

#define CONVAR_DWORD_CB(_name, _vname, _flags, _help)  _CONVAR_DECLARE_CB(DWORD, u32, _name, _vname, (_flags), (_help))
#define CONVAR_DWORD(_name, _vname, _flags, _help)     CONVAR_DWORD_CB(_name, _vname, (_flags), (_help)) {}

#define CONVAR_INT_CB(_name, _vname, _flags, _help)  _CONVAR_DECLARE_CB(INT, int, _name, _vname, (_flags), (_help))
#define CONVAR_INT(_name, _vname, _flags, _help)     CONVAR_INT_CB(_name, _vname, (_flags), (_help)) {}

#define CONVAR_STRING_CB(_name, _vname, _flags, _help)  _CONVAR_DECLARE_CB(STRING, str, _name, _vname, (_flags), (_help))
#define CONVAR_STRING(_name, _vname, _flags, _help)     CONVAR_STRING_CB(_name, _vname, (_flags), (_help)) {}


#define CONVAR_PROT_BOOL_CB(_name, _vname, _flags, _protcb, _help)  _CONVAR_DECLARE_CB2(BOOL, bool, _name, _vname, (_flags)|CMDCON_FLAGS_PROTECTED, _protcb, (_help))
#define CONVAR_PROT_BOOL(_name, _vname, _flags, _protcb, _help)     CONVAR_PROT_BOOL_CB(_name, _vname, (_flags), _protcb, (_help)) {}

#define CONVAR_PROT_BYTE_CB(_name, _vname, _flags, _protcb, _help)  _CONVAR_DECLARE_CB2(BYTE, u8, _name, _vname, (_flags)|CMDCON_FLAGS_PROTECTED, _protcb, (_help))
#define CONVAR_PROT_BYTE(_name, _vname, _flags, _protcb, _help)     CONVAR_PROT_BYTE_CB(_name, _vname, (_flags), _protcb, (_help)) {}

#define CONVAR_PROT_WORD_CB(_name, _vname, _flags, _protcb, _help)  _CONVAR_DECLARE_CB2(WORD, u16, _name, _vname, (_flags)|CMDCON_FLAGS_PROTECTED, _protcb, (_help))
#define CONVAR_PROT_WORD(_name, _vname, _flags, _protcb, _help)     CONVAR_PROT_WORD_CB(_name, _vname, (_flags), _protcb, (_help)) {}

#define CONVAR_PROT_DWORD_CB(_name, _vname, _flags, _protcb, _help)  _CONVAR_DECLARE_CB2(DWORD, u32, _name, _vname, (_flags)|CMDCON_FLAGS_PROTECTED, _protcb, (_help))
#define CONVAR_PROT_DWORD(_name, _vname, _flags, _protcb, _help)     CONVAR_PROT_DWORD_CB(_name, _vname, (_flags), _protcb, (_help)) {}

#define CONVAR_PROT_INT_CB(_name, _vname, _flags, _protcb, _help)  _CONVAR_DECLARE_CB2(INT, int, _name, _vname, (_flags)|CMDCON_FLAGS_PROTECTED, _protcb, (_help))
#define CONVAR_PROT_INT(_name, _vname, _flags, _protcb, _help)     CONVAR_PROT_INT_CB(_name, _vname, (_flags), _protcb, (_help)) {}

#define CONVAR_PROT_STRING_CB(_name, _vname, _flags, _protcb, _help)  _CONVAR_DECLARE_CB2(STRING, str, _name, _vname, (_flags)|CMDCON_FLAGS_PROTECTED, _protcb, (_help))
#define CONVAR_PROT_STRING(_name, _vname, _flags, _protcb, _help)     CONVAR_PROT_STRING_CB(_name, _vname, (_flags), _protcb, (_help)) {}


////////////////////////////////////////////////////////////////////////////////
extern FILE *cmdcon_dumpto[2]; // default: stderr; NULL; non-std* will be closed on exit


extern void conlogf (const char *fmt, ...) __attribute__((format(printf,1,2)));
extern void conlogf_va (const char *fmt, va_list ap);

extern int get_max_con_width (void);
extern int get_con_width (void);
extern void set_con_width (int wdt);


////////////////////////////////////////////////////////////////////////////////
#define CMDCON_MAX_LINES  (512)
extern const char *cmdcon_line (int idx); // from bottom


////////////////////////////////////////////////////////////////////////////////
extern void concmdLock (void);
extern void concmdUnlock (void);


////////////////////////////////////////////////////////////////////////////////
extern int64_t cmdcon_parseint (const char *str, int64_t defval, int64_t min, int64_t max, int *error);
extern int cmdcon_parsebool (const char *str, int defval, int *error);


////////////////////////////////////////////////////////////////////////////////
typedef void (*CmdConTabCompleteCB) (const char *s);

/*
 * ���������� �� ������ ������, ������� ��������
 * ���� � ����� 0: �����, ������, �������, ����, ������, ����������
 * ���� � ����� 1: �������, ���������� ���� 1
 * ���� � ����� �����: �������� �������� ������� �������, ����������; ������ �������� ��� ��������� �������
 * ��������� �� ������ NULL, � ��� ���� free()
 * ����! `cmds` ������ ���� �������� �������, ��� ������ ��������
 * ���� `cmds` -- NULL, �� ���������� �ӣ
 * ���� ������� ������ � �������� � ����� -- ���, ��� ��� ������������ �������
 */
extern char *cmdcon_tabcomplete (const char *cmds, CmdConTabCompleteCB printcb);

/*
 * ���������� �� ������ ������, ������� ��������
 * ���� � ����� 0: �����, ������, �������, ����, ������, ����������
 * ���� � ����� 1: �������, ���������� ���� 1
 * ���� � ����� �����: �������� �������� ������� �������, ����������; ������ �������� ��� ��������� �������
 * ��������� �� ������ NULL, � ��� ���� free()
 * ����! `cmds` ������ ���� �������� �������, ��� ������ ��������
 * ���� `cmds` -- NULL, �� ���������� �ӣ
 * ���� ������� ������ � �������� � ����� -- ���, ��� ��� ������������ �������
 */
extern char *cmdcon_completeabbrev (const char *cmds, CmdConTabCompleteCB printcb, ...) __attribute__((sentinel));
extern char *cmdcon_completeabbrevArray (const char *cmds, CmdConTabCompleteCB printcb, const char *array[]);


#ifdef __cplusplus
}
#endif
#endif
