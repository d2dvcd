/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Weapons
#ifndef D2D_WEAPONS_H
#define D2D_WEAPONS_H

#include "common.h"
#include "saveload.h"


extern void WP_init (void);
extern void WP_alloc (void);
extern void WP_act (void);
extern void WP_draw (void);
extern void WP_punch (int x, int y, int d, int own);
extern int WP_chainsaw (int x, int y, int d, int own);
extern void WP_gun (int x, int y, int xd, int yd, int objid_shooter, int v);
extern void WP_pistol (int x, int y, int xd, int yd, int o);
extern void WP_mgun (int x, int y, int xd, int yd, int o);
extern void WP_rocket (int x, int y, int xd, int yd, int o);
extern void WP_revf (int x, int y, int xd, int yd, int o, int t);
extern void WP_plasma (int x, int y, int xd, int yd, int o);
extern void WP_ball1 (int x, int y, int xd, int yd, int o);
extern void WP_ball2 (int x, int y, int xd, int yd, int o);
extern void WP_ball7 (int x, int y, int xd, int yd, int o);
extern void WP_aplasma (int x, int y, int xd, int yd, int o);
extern void WP_manfire (int x, int y, int xd, int yd, int o);
extern void WP_bfgshot (int x, int y, int xd, int yd, int o);
extern void WP_bfghit (int x, int y, int o);
extern void WP_shotgun (int x, int y, int xd, int yd, int o);
extern void WP_dshotgun (int x, int y, int xd, int yd, int o);
extern void WP_flamethrower (int x, int y, int xd, int yd, int xv, int yv, int o);


#endif
