/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#ifndef D2D_JIMAPI_H
#define D2D_JIMAPI_H

#include <stdint.h>
#include "SDL.h"

#include "common.h"


////////////////////////////////////////////////////////////////////////////////
typedef struct JIMAPI_Command {
  struct JIMAPI_Command *next;
  const char *name;
  void *func;
} JIMAPI_Command;


extern JIMAPI_Command *jimapi_command_list;


////////////////////////////////////////////////////////////////////////////////
#define JIMAPI_FN(_name) \
static int jim_##_name (Jim_Interp *interp, int argc, Jim_Obj *const *argv); \
static __attribute__((constructor)) void __jimapi__init__##_name##__##__COUNTER##__ (void) { \
  static JIMAPI_Command cmd = { \
    .name = #_name, \
    .func = jim_##_name \
  }; \
  cmd.next = jimapi_command_list; \
  jimapi_command_list = &cmd; \
} \
static int jim_##_name (Jim_Interp *interp, int argc, Jim_Obj *const *argv)

#define JIMAPI_FN_NOREG(_name)  static int jim_##_name (Jim_Interp *interp, int argc, Jim_Obj *const *argv)

#define JIMAPI_NAME(_name)  jim_##_name


////////////////////////////////////////////////////////////////////////////////
extern void jimInit (void);
extern void jimDeinit (void);


////////////////////////////////////////////////////////////////////////////////
extern int jimEvalFile (const char *fname, int okifabsent);


////////////////////////////////////////////////////////////////////////////////
extern void jim_SetResStrf (Jim_Interp *interp, const char *fmt, ...) __attribute((format(printf,2,3)));


#endif
