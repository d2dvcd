/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include "netgame.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"

#include "anm.h"
#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "config.h"
#include "dots.h"
#include "edit.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "gamemode.h"
#include "items.h"
#include "jimapi.h"
#include "keyb.h"
#include "map.h"
#include "mapio.h"
#include "membuffer.h"
#include "menu.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "saveload.h"
#include "sdldrv.h"
#include "smoke.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"
#include "wipe.h"


////////////////////////////////////////////////////////////////////////////////
ccBool n_enet_initialized = CC_FALSE;
ccBool n_network_meserver = CC_FALSE;
ccBool n_network_game = CC_FALSE;
int n_local_player_num = -1;

char *n_host_addr = NULL;
uint16_t n_host_port = 1666;
ENetHost *n_host = NULL;
ccBool n_client_uses_pl1_keys = CC_TRUE;

ENetPeer *n_peer = NULL;
uint32_t n_waiting_start_time;

static ccBool n_enet_error_init = CC_FALSE;
static ENetPeer *n_mypeer = NULL;


////////////////////////////////////////////////////////////////////////////////
CONVAR_STRING(n_host_addr, n_host_addr, CMDCON_FLAGS_PERSISTENT, "server address")
CONVAR_WORD_CB(n_host_port, n_host_port, CMDCON_FLAGS_PERSISTENT, "server port") {
  if (n_host_port < 1024) n_host_port = 1024; // shit!
}
CONVAR_BOOL(n_client_uses_pl1_keys, n_client_uses_pl1_keys, CMDCON_FLAGS_PERSISTENT, "will 2nd player use pl1 keys in network games?")


////////////////////////////////////////////////////////////////////////////////
CONCMD(n_peer_info, "show peer info") {
  if (n_peer != NULL) {
    char buf[128];
    //
    sprintf(buf, "%u.%u.%u.%u",
      n_peer->address.host&0xff,
      (n_peer->address.host>>8)&0xff,
      (n_peer->address.host>>16)&0xff,
      (n_peer->address.host>>24)&0xff
    );
    //
    conlogf(
      "peer info:\n"
      "==========\n"
    );
    conlogf("IP:port: %s:%u\n", buf, n_peer->address.port);
    conlogf("lastSendTime: %u\n", n_peer->lastSendTime);
    conlogf("lastReceiveTime: %u\n", n_peer->lastReceiveTime);
    conlogf("packetsSent: %u\n", n_peer->packetsSent);
    conlogf("packetsLost: %u\n", n_peer->packetsLost);
    conlogf("packetLoss: %u\n", n_peer->packetLoss);
    conlogf("packetLossVariance: %u\n", n_peer->packetLossVariance);
    conlogf("pingInterval: %u\n", n_peer->pingInterval);
    conlogf("lastRoundTripTime: %u\n", n_peer->lastRoundTripTime);
    conlogf("lowestRoundTripTime: %u\n", n_peer->lowestRoundTripTime);
    conlogf("roundTripTime: %u\n", n_peer->roundTripTime);
    conlogf("roundTripTimeVariance: %u\n", n_peer->roundTripTimeVariance);
    conlogf("mtu: %u\n", n_peer->mtu);
  }
}


////////////////////////////////////////////////////////////////////////////////
// return 0 to protect
int N_var_protector (CmdConCmd *me, const char *newval) {
  if ((me->flags&CMDCON_FLAGS_SERVER) && n_host != NULL) {
    if (!n_network_meserver) {
      conlogf("only server can do this!\n");
      return 0;
    } else if (NET_IS_PLAYING()) {
      conlogf("can't set this while game in progress!\n");
      return 0;
    }
  }
  if ((me->flags&CMDCON_FLAGS_NET_RO) && n_host != NULL) {
    conlogf("not available in network mode!\n");
    return 0;
  }
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
int N_init (void) {
  if (!n_enet_error_init) {
    if (!n_enet_initialized) {
      if (enet_initialize() != 0) {
        n_enet_error_init = CC_TRUE;
        conlogf("ENET: can't initialize!\n");
      } else {
        n_enet_initialized = CC_TRUE;
        atexit(enet_deinitialize);
      }
    }
  }
  //
  return (!n_enet_error_init && n_enet_initialized ? 0 : -1);
}


int N_create_host (void) {
  N_disconnect();
  if (N_init() == 0) {
    ENetAddress address;
    //
    address.host = ENET_HOST_ANY;
    address.port = n_host_port;
    n_host = enet_host_create(&address, 2/*max connections*/, ENET_MAX_CHANNELS, 0, 0); /*no bandwidth restrictions*/
    if (n_host != NULL) {
      n_network_meserver = CC_TRUE;
      n_local_player_num = 0;
      conlogf("ENET: waiting for client...\n");
      return 0;
    }
    conlogf("ENET: can't create server!\n");
  }
  //
  return -1;
}


int N_create_client (void) {
  N_disconnect();
  if (N_init() == 0) {
    ENetAddress address;
    //
    n_host = enet_host_create(NULL, 1/*max connections*/, ENET_MAX_CHANNELS, 0, 0); /*no bandwidth restrictions*/
    if (n_host != NULL) {
      conlogf("ENET: connecting to %s:%u\n", n_host_addr, n_host_port);
      enet_address_set_host(&address, n_host_addr);
      address.port = n_host_port;
      n_mypeer = enet_host_connect(n_host, &address, ENET_MAX_CHANNELS, 0);
      if (n_mypeer != NULL) {
        n_network_meserver = CC_FALSE;
        n_local_player_num = 1;
        return 0;
      }
      conlogf("ENET: can't create peer!\n");
      N_disconnect();
    } else {
      conlogf("ENET: can't create client!\n");
    }
  }
  //
  return -1;
}


int N_disconnect (void) {
  if (n_host != NULL) {
    conlogf("ENET: networking down.\n");
    conlogf("sent: %u bytes in %u packets\n", n_host->totalSentData, n_host->totalSentPackets);
    conlogf("recv: %u bytes in %u packets\n", n_host->totalReceivedData, n_host->totalReceivedPackets);
  }
  if (n_peer != NULL) enet_peer_reset(n_peer);
  n_peer = NULL;
  if (n_host != NULL) enet_host_destroy(n_host);
  n_host = NULL;
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  n_host_addr = strdup("localhost");
}


GCC_DESTRUCTOR_USER {
  free(n_host_addr);
}
