/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#ifndef D2D_EDIT_H
#define D2D_EDIT_H

#include <stdio.h>

#include "common.h"


extern int ed_ofs_x;
extern int ed_ofs_y;


extern void ED_enter (void);
extern void ED_leave (void);

extern void ED_act (void);
extern void ED_draw (void);


#endif
