/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Dots
#ifndef D2D_DOTS_H
#define D2D_DOTS_H

#include "common.h"


extern void DOT_init (void);
extern void DOT_alloc (void);
extern void DOT_act (void);
extern void DOT_draw (void);
extern void DOT_add (int x, int y, char xv, char yv, uint8_t color, uint8_t time);
extern void DOT_blood (int x, int y, int xv, int yv, int n);
extern void DOT_spark (int x, int y, int xv, int yv, int n);
extern void DOT_water (int x, int y, int xv, int yv, int n, int c);


#endif
