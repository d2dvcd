/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "common.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef _WIN32
# include <sys/stat.h>
# include <sys/types.h>
#endif

#include "cmdcon.h"


Jim_Interp *jim = NULL;


////////////////////////////////////////////////////////////////////////////////
// by Bob Jenkins
// public domain
// http://burtleburtle.net/bob/rand/smallprng.html
//
////////////////////////////////////////////////////////////////////////////////
#define BJPRNG_ROT(x,k)  (((x)<<(k))|((x)>>(32-(k))))


////////////////////////////////////////////////////////////////////////////////
uint32_t bjprngRand (BJRandCtx *x) {
  uint32_t e;
  /* original:
     e = x->a-BJPRNG_ROT(x->b, 27);
  x->a = x->b^BJPRNG_ROT(x->c, 17);
  x->b = x->c+x->d;
  x->c = x->d+e;
  x->d = e+x->a;
  */
  /* better, but slower at least in idiotic m$vc */
     e = x->a-BJPRNG_ROT(x->b, 23);
  x->a = x->b^BJPRNG_ROT(x->c, 16);
  x->b = x->c+BJPRNG_ROT(x->d, 11);
  x->c = x->d+e;
  x->d = e+x->a;
  //
  return x->d;
}


void bjprngInit (BJRandCtx *x, uint32_t seed) {
  x->a = 0xf1ea5eed;
  x->b = x->c = x->d = seed;
  for (int i = 0; i < 20; ++i) bjprngRand(x);
}


////////////////////////////////////////////////////////////////////////////////
void xrandomize (BJRandCtx *x) {
  uint32_t seed = 0xdeadf00d;
  //
#ifndef _WIN32
  FILE *fl;
  //
  if ((fl = fopen("/dev/urandom", "r")) != NULL) {
    fread(&seed, 4, 1, fl);
    fclose(fl);
  }
#else
  seed = GetTickCount();
#endif
  //
  bjprngInit(x, seed);
}


////////////////////////////////////////////////////////////////////////////////
BJRandCtx g_prng;
BJRandCtx g_prng1;


void myrandomize (void) {
  uint32_t seed = 0xdeadf00d;
  //
#ifndef _WIN32
  FILE *fl;
  //
  if ((fl = fopen("/dev/urandom", "r")) != NULL) {
    fread(&seed, 4, 1, fl);
    fclose(fl);
  }
#else
  seed = GetTickCount();
#endif
  //
  bjprngInit(&g_prng, seed);
  bjprngInit(&g_prng1, seed);
}


////////////////////////////////////////////////////////////////////////////////
CONCMD(about, "show about") {
  CMDCON_HELP();
  conlogf("================================");
  conlogf("Doom2D:Vaya Con Dios v" DOOM2D_VERSION);
  conlogf("compile date: %s %s", __DATE__, __TIME__);
  conlogf("\n");
  conlogf("Copyright (C) Prikol Software 1996-1997");
  conlogf("Copyright (C) Aleksey Volynskov 1996-1997");
  conlogf("Copyright (C) <ARembo@gmail.com> 2011");
  conlogf("Copyright (C) Ketmar Dark 2013");
  conlogf("\n");
  conlogf("       DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE\n");
  conlogf("            Version 2, December 2004\n");
  conlogf("\n");
  conlogf("Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>\n");
  conlogf("\n");
  conlogf("Everyone is permitted to copy and distribute verbatim or modified\n");
  conlogf("copies of this license document, and changing it is allowed as long\n");
  conlogf("as the name is changed.\n");
  conlogf("\n");
  conlogf("  DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE\n");
  conlogf("TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n");
  conlogf("\n");
  conlogf("  0. You just DO WHAT THE FUCK YOU WANT TO.\n");
  conlogf("\n");
  conlogf("--------------------------------");
}


////////////////////////////////////////////////////////////////////////////////
int boolval (int argc, char *argv[]) {
  if (argc == 2) return cmdcon_parsebool(argv[1], -1, NULL);
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
int xmkdir (const char *name) {
#ifndef _WIN32
  return mkdir(name, S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH);
#else
  return mkdir(name);
#endif
}


int fexists (const char *filename) {
  return (access(filename, R_OK) == 0);
}


////////////////////////////////////////////////////////////////////////////////
const char *get_bin_dir (void) {
  static char myDir[8192];
  static int myDirInited = 0;
  //
  if (!myDirInited) {
#ifndef _WIN32
    memset(myDir, 0, sizeof(myDir));
    if (readlink("/proc/self/exe", myDir, sizeof(myDir)-1) < 0) {
      strcpy(myDir, ".");
    } else {
      char *p = (char *)strrchr(myDir, '/');
      //
      if (p == NULL) strcpy(myDir, "."); else *p = '\0';
    }
#else
    char *p;
    //
    memset(myDir, 0, sizeof(myDir));
    GetModuleFileName(GetModuleHandle(NULL), myDir, sizeof(myDir)-1);
    for (p = myDir; *p; ++p) if (*p == '\\') *p = '/';
    p = strrchr(myDir, '/');
    if (p == NULL) strcpy(myDir, "."); else *p = '\0';
#endif
    myDirInited = 1;
  }
  //
  return myDir;
}


const char *get_home_dir (void) {
#ifdef _WIN32
  return get_bin_dir();
#else
  return getenv("HOME");
#endif
}


////////////////////////////////////////////////////////////////////////////////
void fputqstr (FILE *fo, const char *str) {
  fputc('"', fo);
  if (str != NULL) {
    while (*str) {
      if (*str == '\\' || *str == '"') fputc('\\', fo);
      fputc(*str, fo);
      ++str;
    }
  }
  fputc('"', fo);
}


////////////////////////////////////////////////////////////////////////////////
const char *file_get_ext (const char *fname) {
  if (fname != NULL && fname[0]) {
    const char *e = strrchr(fname, '.');
    //
    if (e != NULL && strrchr(e+1, '/') == NULL) {
      return (e > fname && e[-1] == '/' ? NULL : e);
    }
  }
  //
  return NULL;
}


const char *file_get_name_only (const char *fname, int *len) {
  if (fname != NULL && fname[0]) {
    const char *fn = strrchr(fname, '/'), *e;
    //
    if (fn++ == NULL) fn = fname;
    e = strrchr(fn, '.');
    if (e == NULL || e == fn) e = fn+strlen(fn);
    //
    if (len != NULL) *len = e-fn;
    return fn;
  }
  //
  if (len != NULL) *len = 0;
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_SYSTEM {
  // this should be true for GNU/Linux on x86_64
  //d2d_assert_static(sizeof(int) == 4);
}
