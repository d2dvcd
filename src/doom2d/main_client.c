/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
////////////////////////////////////////////////////////////////////////////////
// client data
static int32_t cli_seq_id;


static ccBool n_experimental_client = CC_FALSE;
CONVAR_BOOL(n_experimental_client, n_experimental_client, 0, "DON'T TOUCH!")


// return # of processed frames, if any, or <0 for some error (if any)
static int process_network_for_client (enet_uint32 tout) {
  if (n_host != NULL) {
    ENetEvent event;
    //
    while (enet_host_service(n_host, &event, tout) > 0) {
      int res;
      //
      tout = 0;
      switch (event.type) {
        case ENET_EVENT_TYPE_CONNECT:
          if (n_peer == NULL) {
            n_peer = event.peer;
            G_process_enet_connect();
            last_ack_time = SDL_GetTicks();
          } else {
            enet_peer_reset(event.peer);
          }
          return 0;
        case ENET_EVENT_TYPE_DISCONNECT:
          G_process_enet_disconnect();
          last_ack_time = 0;
          return 0;
        case ENET_EVENT_TYPE_RECEIVE:
          if (event.channelID == ENET_GAME_CHANNEL) {
            int frc = 0;
            //
            if (gnet_state == GNETS_PLAYING) {
              ENetPacket *pkt = event.packet;
              //
              if (pkt->dataLength >= 1+4*2+2*2+2 && pkt->dataLength%2 != 0) {
                // packet format:
                //   uint32_t srv_seq_id
                //   uint32_t delta_start_seq_id
                //   uint16_t length of data for server player
                //   uint16_t length of data for client player
                //   uint16_t*?: server player data
                //   uint16_t*?: client player data
                uint16_t *ddd = (uint16_t *)(pkt->data+1); // skip type
                int32_t s_seq_id;
                int32_t ds_seq_id;
                //
                if (pkt->data[0] != NETPKT_DELTA) {
                  conlogf("network: server packet is not delta! (%u)", pkt->data[0]);
                  goto badpacket;
                }
                //
                memcpy(&s_seq_id, ddd, 4); // current server seq_id
                ddd += 2;
                memcpy(&ds_seq_id, ddd, 4); // delta from this seq_id
                ddd += 2;
                dlogfX("network: 'delta' cli_seq_id=%d; s_seq_id=%d; ds_seq_id=%d\n", cli_seq_id, s_seq_id, ds_seq_id);
                if (ds_seq_id >= s_seq_id) {
                  // somebody wants to fuck us!
                  conlogf("network: somebody wants to fuck us!\n");
                  goto badpacket;
                }
                //
                if (cli_seq_id < s_seq_id) {
                  // this is not a guest from the past, do fast-forward
                  uint16_t dlen[2];
                  uint16_t *data[2];
                  //
                  dlen[0] = *ddd++;
                  dlen[1] = *ddd++;
                  if (1+4*2+2*2+(dlen[0]+dlen[1])*2 != pkt->dataLength) {
                    conlogf("network: invalid server packet (size)!");
                    conlogf(" packet size: %u\n", pkt->dataLength);
                    conlogf(" srv_seq_id: %d\n", s_seq_id);
                    conlogf(" cli_seq_id: %d\n", cli_seq_id);
                    conlogf(" len0: %u (%u)\n", dlen[0], dlen[0]*2);
                    conlogf(" len1: %u (%u)\n", dlen[1], dlen[1]*2);
                    conlogf(" len0+1: %u (%u)\n", dlen[0]+dlen[1], (dlen[0]+dlen[1])*2);
                    goto badpacket;
                  }
                  data[0] = ddd;
                  data[1] = ddd+dlen[0];
                  //
                  // do unpacking and fast-forwarding
                  //TODO: process 10-12 frames, then check how much time passed
                  //      and and process SDL events
                  //      this way we will not hang on badly crafted (overlong) packets
                  while (cli_seq_id < s_seq_id) {
                    uint16_t keywd[2];
                    //
                    for (int f = 0; f < 2; ++f) {
#if NET_USE_RLE
                      fuck off compression for now
                      if (data[f][0]&0x8000) {
                        if (dlen[f] < 2) {
                          conlogf("network: invalid server packet (out of pl#%d RLE data)!", f);
                          goto badpacket;
                        }
                        keywd[f] = data[f][0];
                        if (data[f][0]&0x7fff) {
                          --(data[f][0]);
                        } else {
                          ++(data[f]);
                          --(dlen[f]);
                        }
                      } else {
                        if (dlen[f] < 1) {
                          conlogf("network: invalid server packet (out of pl#%d data)!", f);
                          goto badpacket;
                        }
                        keywd[f] = data[f][0];
                        ++(data[f]);
                        --(dlen[f]);
                      }
#else
                      if (dlen[f] < 1) {
                        conlogf("network: invalid server packet (out of pl#%d data)!", f);
                        goto badpacket;
                      }
                      keywd[f] = data[f][0];
                      ++(data[f]);
                      --(dlen[f]);
#endif
                    }
                    //
                    if (ds_seq_id >= cli_seq_id) {
                      // key states unpacked, do one simulation step
                      ++ds_seq_id;
                      ++cli_seq_id;
                      ++frc;
                      dlogfX("FRM#%05d: 0x%02x 0x%02x (%d)\n", cli_seq_id, keywd[0], keywd[1], g_time);
                      //
                      player_unpack_keys(1-n_local_player_num, keywd[0]);
                      player_unpack_keys(n_local_player_num, keywd[1]);
                      // play frame
                      g_frame_advanced = 1;
                      G_act();
                      g_frame_advanced = 0;
                    } else {
                      // this frame was already played, just skip it
                      ++ds_seq_id;
                    }
                  }
                }
                // it's ok here, go on
                enet_packet_destroy(event.packet);
                last_ack_time = SDL_GetTicks();
              } else {
                // some error occured, just disconnect
                conlogf("network: bad server packet size!\n");
badpacket:      enet_packet_destroy(event.packet);
                G_process_enet_disconnect();
              }
            } else {
              conlogf("network: game packed received, but we aren't playing!\n");
              enet_packet_destroy(event.packet);
            }
            return (n_experimental_client ? frc : 0);
          } else if (gnet_state == ENET_HANDSHAKE_CHANNEL) {
            res = G_process_enet_game_packet(event.packet);
          } else if (gnet_state == ENET_CHAT_CHANNEL) {
            res = 0;
          } else {
            res = -1;
          }
          enet_packet_destroy(event.packet);
          //
          if (res < 0) {
            conlogf("network: disconnected due to some errors...\n");
            N_disconnect();
          }
          return 0;
        case ENET_EVENT_TYPE_NONE:
          break;
      }
      break;
    }
  } else if (tout > 0) {
    SDL_Delay(tout);
  }
  //
  return 0;
}


static void network_game_client_init_vars (void) {
  cli_seq_id = -1; // no frames replayed yet
  last_ack_time = 0;
  //
  player_unpack_keys(n_local_player_num, 0);
  player_unpack_keys(1-n_local_player_num, 0);
  //
  last_ack_time = 0;
}


static void network_game_loop_client (void) {
  int wasdraw;
  //
  dlogfX("network_game_loop_client: enter\n");
  //
  network_game_client_init_vars();
  //
  wasdraw = 0;
  cmdcon_exec("r_interpolate_panning 0");
  while (n_host != NULL) {
    Uint32 t;
    //
//new_frame_immidiately:
    g_frame_ticks_start = SDL_GetTicks();
    S_updatemusic();
    widgetsUpdateActiveFlag();
    //
    // send keys, draw frame, keep rest of the time waiting for server packet
    // this way we reacting to keypresses one frame later; ok, nobody will notice
    //
    // let's render frame; note, that we don't advancing it, as we got no deltas
    // from server yet
    g_frame_advanced = 0;
    G_act(); // for control keys
    if (!wasdraw) draw_all();
    wasdraw = 0;
    g_frame_advanced = 0;
    if (n_host == NULL) break;
    //
    dump_frame_time();
    //
    // send our beloved keys to server along with our seq_id
    if (NET_IS_PLAYING()) {
      ENetPacket *pkt;
      NetAckPacket ack;
      //
      ack.type = NETPKT_ACK;
      ack.seq_id = cli_seq_id; // this is the id we already processed
      ack.keys = player_build_keys(n_local_player_num^(!!n_client_uses_pl1_keys));
      //
      pkt = enet_packet_create(&ack, sizeof(ack), ENET_GAME_PKT_FLAGS); // we don't care
      enet_peer_send(n_peer, ENET_GAME_CHANNEL, pkt);
      enet_host_flush(n_host); // just send it
      //
      dlogfX("%d: ack packet sent (%d)\n", cli_seq_id, g_time);
    } else {
      network_game_client_init_vars();
    }
    //
    // wait for server packet(s) and process local keyboard
    if ((t = SDL_GetTicks()) < g_frame_ticks_start+DELAY) {
      if (g_st == GS_GAME && !g_trans) {
        ++frame_count;
        ticks_used += t-g_frame_ticks_start;
        ticks_free += DELAY-(t-g_frame_ticks_start);
      }
      //
      while (t < g_frame_ticks_start+DELAY) {
        sdl_process_events();
        if (process_network_for_client(0) > 1) {
          wasdraw = 1;
          draw_all();
        }
        //
        if ((t = SDL_GetTicks()) < g_frame_ticks_start+DELAY) {
          Uint32 left = g_frame_ticks_start+DELAY-t;
          //
          if (left > 1) {
            if (process_network_for_client(left-1) > 1) {
              wasdraw = 1;
              draw_all();
            }
          }
          t = SDL_GetTicks();
        }
      }
    } else {
      sdl_process_events();
      process_network_for_client(0);
      //
      if (g_st == GS_GAME && !g_trans) {
        vo_turtle_alpha = 255;
        ++frame_count;
        ticks_used += t-g_frame_ticks_start;
      }
      t = SDL_GetTicks();
    }
    //
    if (NET_IS_PLAYING()) {
      if (last_ack_time > 0 && t-last_ack_time >= 3000) {
        // if client sends nothing for 3 seconds, assume it's disconnected
        conlogf("net: timeout!");
        G_process_enet_disconnect();
      } else if (last_ack_time > 0 && t-last_ack_time > DELAY+3) {
        vo_turtle_alpha = 255;
        dlogfX("*delay=%u (%u)\n", t-last_ack_time, DELAY);
      }
      //fprintf(stderr, "*delay=%u (%u)\n", t-last_ack_time, DELAY);
    } else {
      network_game_client_init_vars();
    }
    //
    if (n_host != NULL && g_st == GS_WAITING_CONNECTION) {
      if (SDL_GetTicks()-n_waiting_start_time >= 3000) {
        cmdcon_exec("gcmd_network_start_client");
      }
    }
  }
  //
  dlogfX("network_game_loop_client: leave\n");
}
