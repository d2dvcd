/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "vga.h"

#include <ctype.h>

#include "common.h"
#include "cmdcon.h"
#include "error.h"
#include "game.h"
#include "jimapi.h"
#include "menu.h"
#include "smoke.h"
#include "view.h"

#include "vga1_fnt.c"
#include "vdfont6x8.c"
#include "vdfont8x8.c"


////////////////////////////////////////////////////////////////////////////////
static void init_dos_palette (void);


////////////////////////////////////////////////////////////////////////////////
int VID_HQ = 2;

int SCRW = 800;
int SCRH = 600;
int SCRWorig = 800;
int SCRHorig = 600;

SDL_Surface *screen = NULL, *oldscreen = NULL, *screenReal = NULL;
Uint8 *vscrbuf = NULL;
Uint8 *scrlightmap = NULL;
SDL_Surface *frameSfc = NULL;
int vidcx1, vidcx2, vidcy1, vidcy2;
ccBool fullscreen = CC_FALSE;

uint8_t clrmap[256*12];
uint8_t bright[256];
uint8_t mixmap[256][256];
uint8_t main_pal[768], std_pal[768], cur_pal[768];
ccBool shot_vga = CC_TRUE;

unsigned char palRGB[256][3];
//int vidRIdx, vidGIdx, vidBIdx;
Uint32 palette[256]; // converted


////////////////////////////////////////////////////////////////////////////////
ccBool r_mouse_cursor = CC_FALSE;


CONVAR_BOOL_CB(r_mouse_cursor, r_mouse_cursor, 0/*CMDCON_FLAGS_PERSISTENT*/, "show/hide mouse cursor") {
  int v = (r_mouse_cursor ? SDL_ENABLE : SDL_DISABLE);
  //
  if (SDL_ShowCursor(SDL_QUERY) != v) SDL_ShowCursor(v);
}


////////////////////////////////////////////////////////////////////////////////
static void draw_rect (int x, int y, int w, int h, Uint8 c);


////////////////////////////////////////////////////////////////////////////////
int convisible = 0;
char coninput[CONINPUT_SIZE] = {0};
int conheight = 16;
int conbotline = 0;

char *conhistory[MAX_CON_HISTORY];
int conhistorypos;

static ccBool concurblink = CC_TRUE;
static int conblinktime = 800; // ms


////////////////////////////////////////////////////////////////////////////////
CONVAR_INT_CB(con_height, conheight, CMDCON_FLAGS_PERSISTENT, "console height") {
  if (conheight < 2) conheight = 2;
  if (conheight > SCRH/8-2) conheight = SCRH/8-2;
}


CONVAR_INT_CB(con_cursor_blink_time, conblinktime, CMDCON_FLAGS_PERSISTENT, "cursor blinking time in milliseconds") {
  if (conblinktime < 50) conblinktime = 50;
  if (conblinktime > 5000) conblinktime = 5000;
}

CONVAR_BOOL(con_cursor_blink, concurblink, CMDCON_FLAGS_PERSISTENT, "cursor blinking flag")


////////////////////////////////////////////////////////////////////////////////
void conaddchar (char ch) {
  int len = strlen(coninput);
  //
  if (len < sizeof(coninput)-1) {
    if (len == 0 && ch <= 32) return;
    coninput[len++] = ch;
    coninput[len] = 0;
  }
}


void condelchar (void) {
  int len = strlen(coninput);
  //
  if (len > 0) coninput[--len] = 0;
}


void condelword (void) {
  int len = strlen(coninput);
  //
  while (len > 0 && isspace(coninput[len-1])) --len;
  while (len > 0 && !isspace(coninput[len-1])) --len;
  coninput[len] = 0;
}


////////////////////////////////////////////////////////////////////////////////
static void __attribute__((constructor(342))) conhistory_init (void) {
  conhistorypos = -1;
  for (int f = 0; f < MAX_CON_HISTORY; ++f) conhistory[f] = NULL;
}


static void __attribute__((destructor(342))) conhistory_deinit (void) {
  for (int f = 0; f < MAX_CON_HISTORY; ++f) if (conhistory[f] != NULL) free(conhistory[f]);
}


////////////////////////////////////////////////////////////////////////////////
void conHistoryAdd (const char *str) {
  if (str != NULL && str[0] && !isspace(str[0])) {
    for (int f = 0; conhistory[f] != NULL; ++f) {
      if (strcmp(conhistory[f], str) == 0) {
        // duplicate found, move it to history top
        char *s = conhistory[f];
        //
        for (int c = f; c > 0; --c) conhistory[c] = conhistory[c-1];
        conhistory[0] = s;
        return;
      }
    }
    //
    if (conhistory[MAX_CON_HISTORY-1] != NULL) free(conhistory[MAX_CON_HISTORY-1]);
    for (int f = MAX_CON_HISTORY-1; f > 0; --f) conhistory[f] = conhistory[f-1];
    conhistory[0] = strdup(str);
  }
}


void conhistoryCopy (void) {
  if (conhistorypos >= 0 && conhistorypos < MAX_CON_HISTORY && conhistory[conhistorypos] != NULL) {
    memset(coninput, 0, sizeof(coninput));
    strncpy(coninput, conhistory[conhistorypos], sizeof(coninput));
    conhistorypos = -1;
  }
}


void contabcompletion (void) {
  int showdivider = 1;
  //
  void cmdprint (const char *s) {
    if (showdivider) { conlogf("=================\n"); showdivider = 0; }
    conlogf("%s\n", s);
  }
  //
  if (coninput[0] && coninput[0] != '"' && coninput[0] != '\'') {
    // ok, check if there is only command name
    const char *p;
    //
    for (p = coninput; *p && !isspace(*p); ++p) ;
    if (!p[0]) {
      // ok, let's find completion
      char *cmps = cmdcon_tabcomplete(coninput, cmdprint);
      //
      if (cmps[0]) strcpy(coninput, cmps);
      free(cmps);
    }
  } else if (!coninput[0]) {
    // show all commands
    char *cmps = cmdcon_tabcomplete(NULL, cmdprint);
    //
    free(cmps);
  }
}


////////////////////////////////////////////////////////////////////////////////
static SDL_Rect orect;


static void con_saverect (void) {
  SDL_Rect r;
  //
  SDL_GetClipRect(screen, &orect);
  r.x = 0;
  r.y = 0;
  r.w = SCRWorig;
  r.h = SCRHorig;
  SDL_SetClipRect(screen, &r);
}


static void con_restrect (void) {
  SDL_SetClipRect(screen, &orect);
}


//FIXME: make this overlayed
static void condrawch (int x, int y, char ch, Uint8 col) {
  int c = (ch&0xff)*8;
  //
  for (int dy = 0; dy < 8; ++dy) {
    unsigned char bt = vga_font6x8[c++];
    //
    for (int dx = 0; dx < 8; ++dx) {
      if (bt&0x80) putPixel(x+dx, y+dy, col);
      bt = (bt&0x7f)<<1;
    }
  }
}


static void condrawstr (int x, int y, const char *str, Uint8 col) {
  if (str != NULL) {
    int sx = x;
    //
    while (*str) {
      switch (*str) {
        case '\n': y += 8; // fallthru
        case '\r': x = sx; break;
        default: condrawch(x, y, *str, col); x += 6; break;
      }
      ++str;
    }
  }
}


static void condrawstrO (int x, int y, const char *str, Uint8 col, Uint8 ocol) {
  for (int dy = -1; dy <= 1; ++dy) {
    for (int dx = -1; dx <= 1; ++dx) {
      if (dx || dy) condrawstr(x+dx, y+dy, str, ocol);
    }
  }
  condrawstr(x, y, str, col);
}


#define RSZ  (24)
static ccBool palshow = CC_FALSE;
CONVAR_BOOL(d_showpalette, palshow, 0, "show/hide palette")

static void show_palette (void) {
  //for (int y = 0; y < 6; ++y) for (int f = 0; f < 255; ++f) V_putpixel(f, y, f);
  for (int y = 0; y < 16; ++y) {
    for (int x = 0; x < 16; ++x) {
      char s[8];
      //
      draw_rect(x*RSZ, y*RSZ, RSZ-1, RSZ-1, y*16+x);
      sprintf(s, "%d", y*16+x);
      condrawstrO(x*RSZ+2, y*RSZ+2, s, 4, 0);
    }
  }
}


static void drawconsole (void) {
  int ly;
  //
  con_saverect();
  /*
  for (int y = 0; y < conheight*8+2; ++y) {
    for (int x = 0; x < SCRW; ++x) {
      V_putpixel(x, y, 0);
    }
  }
  */
  draw_rect(0, 0, SCRW, conheight*8+2, 0);
  //
  if (conbotline < 0) conbotline = 0;
  if (conbotline > CMDCON_MAX_LINES-conheight-1) conbotline = CMDCON_MAX_LINES-conheight-1;
  ly = conbotline;
  for (int y = (conheight-2); y >= 0; --y, ++ly) {
    int sy = y*8+1, sx = 1;
    const char *s = cmdcon_line(ly);
    //
    while (*s && sx < SCRW) {
      condrawch(sx, sy, *s++, 64);
      sx += 6;
    }
  }
  // draw command line
  {
    const char *pp = (conhistorypos < 0 ? coninput : conhistorypos < MAX_CON_HISTORY ? conhistory[conhistorypos] : "");
    int sy = (conheight-1)*8+1, sx = 1+6;
    int len = strlen(pp), wc = (SCRH-2)/6-2;
    //
    condrawch(1, sy, '>', 163);
    if (wc > 1) {
      // fuck it, let it be SLOW
      while (len > wc) {
        ++pp;
        --len;
      }
    }
    while (*pp) {
      condrawch(sx, sy, *pp++, 160);
      sx += 6;
    }
    //condrawch(sx, sy, '_', 4);
    if (!concurblink || (conblinktime > 0 && SDL_GetTicks()%(conblinktime*2) < conblinktime)) {
      draw_rect(sx, sy, 6, 8, 168);
    }
  }
  //
  if (palshow) show_palette();
  con_restrect();
}


////////////////////////////////////////////////////////////////////////////////
int vga_xofs = 0;
int vga_yofs = 0;


static void buildColorCache (void) {
  for (int f = 0; f < 256; ++f) palette[f] = SDL_MapRGB(screen->format, cur_pal[f*3+0], cur_pal[f*3+1], cur_pal[f*3+2]);
  for (int f = 0; f < 256; ++f) SDL_GetRGB(palette[f], screen->format, &palRGB[f][0], &palRGB[f][1], &palRGB[f][2]);
}


void palSetColor (unsigned char idx, unsigned char r, unsigned char g, unsigned char b) {
  cur_pal[idx*3+0] = r;
  cur_pal[idx*3+1] = g;
  cur_pal[idx*3+2] = b;
  palette[idx] = SDL_MapRGB(screen->format, r, g, b);
  SDL_GetRGB(palette[idx], screen->format, &palRGB[idx][0], &palRGB[idx][1], &palRGB[idx][2]);
}


static SDL_Surface *createSurface (int w, int h) {
  return SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, screen->format->BitsPerPixel, screen->format->Rmask, screen->format->Gmask, screen->format->Bmask, 0);
}


GCC_DESTRUCTOR_SYSTEM {
  if (scrlightmap != NULL) free(scrlightmap);
  if (vscrbuf != NULL) free(vscrbuf);
  if (screen != NULL) SDL_FreeSurface(screen);
  //if (screenReal != NULL) SDL_FreeSurface(screenReal);
  if (oldscreen != NULL) SDL_FreeSurface(oldscreen);
}


void V_init (void) {
  int vidRIdx, vidGIdx, vidBIdx;
  //
  SCRWorig = SCRW;
  SCRHorig = SCRH;
  VID_HQ = (SCRW > 320 ? 2 : 1);
  SCRW /= VID_HQ;
  SCRH /= VID_HQ;
  //
  if (scrlightmap != NULL) free(scrlightmap);
  if (vscrbuf != NULL) free(vscrbuf);
  if (screen != NULL) SDL_FreeSurface(screen);
  if (screenReal != NULL) SDL_FreeSurface(screenReal);
  if (oldscreen != NULL) SDL_FreeSurface(oldscreen);
  //
  screenReal = SDL_SetVideoMode(SCRWorig, SCRHorig, 32,
    SDL_SWSURFACE|
    /*SDL_HWSURFACE|SDL_DOUBLEBUF|*/
    (fullscreen ? SDL_FULLSCREEN : 0)
  );
  if (screenReal == NULL) ERR_fatal("Unable to set video mode: %s\n", SDL_GetError());
  screen = SDL_CreateRGBSurface(SDL_SWSURFACE, SCRW, SCRH, screenReal->format->BitsPerPixel, screenReal->format->Rmask, screenReal->format->Gmask, screenReal->format->Bmask, 0);
  //
  if (screen->format->BitsPerPixel != 32 || (screenReal != NULL && screenReal->format->BitsPerPixel != 32) ||
      (screen->format->Rshift%8 || screen->format->Gshift%8 || screen->format->Bshift%8)) {
    ERR_fatal("videomode initialization error: invalid videosurface format!\n");
    //dlogf("Rmask=0x%08x; Gmask=0x%08x; Bmask=0x%08x\n", screen->format->Rmask, screen->format->Gmask, screen->format->Bmask);
  }
  oldscreen = SDL_CreateRGBSurface(SDL_SWSURFACE, SCRW, SCRH, screen->format->BitsPerPixel, screen->format->Rmask, screen->format->Gmask, screen->format->Bmask, 0);
  //
  vidRIdx = screen->format->Rshift/8;
  vidGIdx = screen->format->Gshift/8;
  vidBIdx = screen->format->Bshift/8;
  //conlogf("BPP: %u; ridx=%d; gidx=%d; bidx=%d\n", screen->format->BytesPerPixel, vidRIdx, vidGIdx, vidBIdx);
  if (vidRIdx == 0 && vidGIdx == 1 && vidBIdx == 2) {
    conlogf("surface format is RGBA");
    ERR_fatal("invalid videosurface format!");
  } else if (vidRIdx == 2 && vidGIdx == 1 && vidBIdx == 0) {
    //conlogf("surface format is BGRA");
  } else {
    ERR_fatal("invalid videosurface format!");
  }
  //dlogf("BPP: %u\n", screen->format->BytesPerPixel);
  frameSfc = screen;
  //SDL_ShowCursor(0);
  buildColorCache();
  //
  set_con_width((SCRW-4)/6);
  //
  //cmdcon_dumpto[0] = NULL; // stop trashing console
  /*if (!r_mouse_cursor)*/ SDL_ShowCursor(SDL_DISABLE);
  polymod_init();
  init_dos_palette();
  //
  vscrbuf = calloc(SCRW*SCRH, 1);
  scrlightmap = calloc(SCRW*SCRH, 1);
  lb_reset(1);
}


void V_done (void) {
  //SDL_Quit();
}


void V_toggle (void) {
  if (!SDL_WM_ToggleFullScreen(screen)) {
    fullscreen = !fullscreen;
    SCRW = SCRWorig;
    SCRH = SCRHorig;
    V_init();
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef struct GCC_PACKED {
  uint16_t w, h; // W-������, H-������
  int16_t sx, sy; // ����� ������ �����������
  uint8_t data[0];
} vgaimg_o;


void V_initvga (vgaimg *img) {
  if (img != NULL) {
    memset(img, 0, sizeof(*img));
    img->wanim = -1;
  }
}


void V_freevga (vgaimg *img) {
  if (img != NULL) {
    if (img->rawdata != NULL) free(img->rawdata);
    if (img->sfcM != NULL) SDL_FreeSurface(img->sfcM);
    if (img->sfc != NULL) SDL_FreeSurface(img->sfc);
    memset(img, 0, sizeof(*img));
    img->wanim = -1;
  }
}


static void convertImg (vgaimg *img) {
  const Uint8 *src = (const Uint8 *)img->rawdata;
  Uint32 *dst;
  //
  if (img->w == 0 || img->h == 0) ERR_fatal("invalid VGA image");
  return;
  img->sfc = createSurface(img->w, img->h);
  dst = (Uint32 *)img->sfc->pixels;
  for (int y = 0; y < img->h; ++y) {
    Uint32 *d = dst;
    //
    for (int x = 0; x < img->w; ++x) *d++ = palette[*src++];
    dst += img->sfc->pitch;
  }
  //
  src = (const Uint8 *)img->rawdata;
  img->sfcM = createSurface(img->w, img->h);
  dst = (Uint32 *)img->sfcM->pixels;
  for (int y = 0; y < img->h; ++y) {
    Uint32 *d = dst;
    //
    for (int x = 0; x < img->w; ++x) *d++ = palette[src[img->w-x-0]];
    src += img->w;
    dst += img->sfcM->pitch;
  }
}


void V_loadvga_ptr (vgaimg *img, const void *vdata) {
  if (img != NULL && vdata != NULL) {
    const vgaimg_o *i = vdata;
    //
    V_freevga(img);
    if (i->w == 0 || i->h == 0) ERR_fatal("invalid VGA image");
    img->w = i->w;
    img->h = i->h;
    img->sx = i->sx;
    img->sy = i->sy;
    if ((img->rawdata = malloc(img->w*img->h)) == NULL) ERR_fatal("out of memory");
    memcpy(img->rawdata, i->data, img->w*img->h);
    convertImg(img);
  }
}


void V_loadvga (const char *name, vgaimg *img) {
  if (img != NULL && name != NULL && name[0]) {
    vgaimg_o *i = M_lock(F_getresid(name));
    //
    V_loadvga_ptr(img, i);
    M_unlock(i);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void draw_rect (int x, int y, int w, int h, Uint8 c) {
  SDL_Rect dstrect;
  //
  dstrect.x = x;
  dstrect.y = y;
  dstrect.w = w;
  dstrect.h = h;
  SDL_FillRect(screen, &dstrect, palette[c]);
}


// ���������� ������� ������
void V_setrect (int x, int w, int y, int h) {
  SDL_Rect r;
  //
  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;
  SDL_SetClipRect(screen, &r);
  //SDL_GetClipRect(screen, &r);
  vidcx1 = x;
  vidcx2 = x+w;
  vidcy1 = y;
  vidcy2 = y+h;
  if (vidcx1 < 0) vidcx1 = 0;
  if (vidcx2 > SCRW) vidcx2 = SCRW;
  if (vidcy1 < 0) vidcy1 = 0;
  if (vidcy2 > SCRH) vidcy2 = SCRH;
}


static void V_offset (int ox, int oy) {
  vga_xofs = ox;
  vga_yofs = oy;
}


void V_center (int f) {
  if (f) {
    V_offset(SCRW/2-320/2, SCRH/2-200/2);
  } else {
    V_offset(0, 0);
  }
}


static void draw_spr_lm (int x, int y, vgaimg *i, int dir, int semi) {
  if (i != NULL) {
    x += vga_xofs;
    y += vga_yofs-i->sy;
    if (dir&1) x = x-i->w+i->sx; else x -= i->sx;
    //
    if (x+i->w >= vidcx1 && x < vidcx2 && y+i->h >= vidcy1 && y < vidcy2) {
      const uint8_t *p = (const uint8_t *)i->rawdata;
      int xofs = 0, xlen = i->w, ylen = i->h;
      //
      if (y < vidcy1) { p += i->w*(vidcy1-y); ylen -= vidcy1-y; y = vidcy1; }
      if (y+ylen > vidcy2) ylen -= y+ylen-vidcy2;
      if (x < vidcx1) { xlen -= (xofs = vidcx1-x); x = vidcx1; }
      if (x+xlen > vidcx2) xlen -= x+xlen-vidcx2;
      //
      for (int ly = ylen; ly > 0; --ly, ++y) {
        const uint8_t *p1 = (dir&1 ? p+i->w-xofs-1 : p+xofs);
        Uint8 *lm = scrlightmap+SCRW*y+x;
        Uint8 *d = vscrbuf+SCRW*y+x;
        //
        for (int lx = xlen; lx > 0; --lx, ++d, ++lm) {
          uint8_t t = *p1;
          //
          p1 = p1+(~dir&1)-(dir&1);
          if (t && (!semi || (ly&1)^(lx&1))) {
            *lm = (r_lighting ? LMAP_NORMAL : LMAP_FULLLIT);
            *d = t;
          }
        }
        p += i->w;
      }
    }
  }
}


static void draw_spr (int x, int y, vgaimg *i, int d, int c, int semi) {
  if (i != NULL) {
    x += vga_xofs;
    y += vga_yofs;
    if (d&1) x = x-i->w+i->sx; else x -= i->sx;
    if (d&2) y = y-i->h+i->sy; else y -= i->sy;
    if (x+i->w >= vidcx1 && x < vidcx2 && y+i->h >= vidcy1 && y < vidcy2) {
      const uint8_t *p = (const uint8_t *)i->rawdata;
      //
      for (int ly = 0; ly < i->h; ++ly) {
        for (int lx = 0; lx < i->w; ++lx) {
          uint8_t t = *p++;
          //
          if (t && (!semi || (ly&1)^(lx&1))) {
            int rx = (d&1 ? i->w-lx-1 : lx);
            int ry = (d&2 ? i->h-ly-1 : ly);
            //
            if (c && t >= 0x70 && t <= 0x7F) t = t-0x70+c;
            V_putpixel_lm(x+rx, y+ry, t, LMAP_FULLLIT);
          }
        }
      }
    }
  }
}


void V_draw_tile_lm (int x, int y, vgaimg *i, int semi) {
  draw_spr_lm(x, y, i, 0, semi);
}


void V_draw_tile (int x, int y, vgaimg *i, int semi) {
  draw_spr(x, y, i, 0, 0, semi);
}


void V_rotspr (int x, int y, vgaimg *i, int d) {
  x += i->w*(d&1 ? 1 : 0);
  y += i->h*(d&2 ? 1 : 0);
  draw_spr(x, y, i, d, 0, 0);
}


void V_pic (int x, int y, vgaimg *i) {
  draw_spr(x, y, i, 0, 0, 0);
}


void V_pic1 (int x, int y, vgaimg *i, int semi) {
  draw_spr(x, y, i, 0, 0, semi);
}


void V_manspr (int x, int y, vgaimg *p, unsigned char c) {
  draw_spr(x, y, p, 0, c, 0);
}


void V_manspr2 (int x, int y, vgaimg *p, unsigned char c) {
  draw_spr(x, y, p, 1, c, 0);
}


void smoke_sprf (int x, int y, uint8_t c) {
  uint8_t t = V_getpixel(x, y);
  c += bright[t]+0x60;
  c ^= 0xF;
  V_putpixel(x, y, mixmap[c][t]);
}


void flame_sprf (int x, int y, uint8_t c) {
  uint8_t t = V_getpixel(x, y);
  c += bright[t];
  V_putpixel(x, y, flametab[c&0x0f]);
}


void V_sprf (int x, int y, vgaimg *i, SpriteDrawFn *f) {
  if (i != NULL) {
    const uint8_t *p = (const uint8_t *)i->rawdata;
    //
    x -= i->sx;
    y -= i->sy;
    p += sizeof(vgaimg);
    for (int cy = y; cy < y+i->h; ++cy) {
      for (int cx = x; cx < x+i->w; ++cx) {
        if (*p) (*f)(cx, cy, *p);
        ++p;
      }
    }
  }
}


void V_spr (int x, int y, vgaimg *i) {
  draw_spr(x, y, i, 0, 0, 0);
}


void V_spr2 (int x, int y, vgaimg *i) {
  draw_spr(x, y, i, 1, 0, 0);
}


void V_spr_lm (int x, int y, vgaimg *i) {
  draw_spr_lm(x, y, i, 0, 0);
}


void V_spr2_lm (int x, int y, vgaimg *i) {
  draw_spr_lm(x, y, i, 1, 0);
}


void V_clr (int x, int w, int y, int h, unsigned char c) {
  draw_rect(x, y, w, h, c);
}


// ���������� ������� �� ������� p
void VP_setpal (const void *p) {
  VP_set(p, 0, 256);
}


static inline Uint8 mul4x (Uint8 c) {
  return (c&0x01 ? c<<2|0x03 : c<<2);
}


// ���������� n ������, ������� � f, �� ������� p
void VP_set (const void *p, int f, int n) {
  const uint8_t *ptr = (const uint8_t *)p;
  //
  for (int i = f; i < f+n; ++i) {
    palSetColor(i, mul4x(ptr[0]), mul4x(ptr[1]), mul4x(ptr[2]));
    ptr += 3;
  }
}


void V_maptoscr (int x, int w, int y, int h, const void *cmap) {
  for (int cx = x; cx < x+w; ++cx) {
    for (int cy = y; cy < y+h; ++cy) {
      V_mappixel(cx, cy, (const uint8_t *)cmap);
    }
  }
  //V_copytoscr(x, w, y, h);
}


void V_remap_rect (int x, int y, int w, int h, const uint8_t *cmap) {
  for (int cx = x; cx < x+w; ++cx) {
    for (int cy = y; cy < y+h; ++cy) {
      V_mappixel(cx, cy, cmap);
    }
  }
}


int vidColorMode = 0; // 0: ok; 1: b/w; 2: green
ccBool r_tv = CC_TRUE;


CONVAR_INT(r_color_mode, vidColorMode, 0, "rendering mode")
CONVAR_BOOL(r_tv, r_tv, 0, "use 'tv-mode'")


static inline Uint32 fixColor (Uint32 clr) {
  if (vidColorMode) {
    Uint8 *ca = (Uint8 *)&clr;
    Uint8 r = ca[VGA_RIDX], g = ca[VGA_GIDX], b = ca[VGA_BIDX];
    //Uint32 lumi = (0.3*((double)r/255.0)+0.59*((double)g/255.0)+0.11*((double)b/255.0))*255.0;
    Uint32 lumi = (4915*r+9666*g+1802*b);
    //
    if (vidColorMode > 2) {
      if (lumi >= 128*16384) lumi /= 16384; else lumi /= 11000;
    } else {
      lumi /= 16384;
    }
    if (lumi > 255) lumi = 255;
    //
    switch (vidColorMode) {
      case 1: case 3: // b/w
        ca[VGA_RIDX] = ca[VGA_GIDX] = ca[VGA_BIDX] = lumi;
        break;
      case 2: case 4: // green
        ca[VGA_RIDX] = ca[VGA_BIDX] = 0;
        ca[VGA_GIDX] = lumi;
        break;
    }
  }
  return clr;
}


////////////////////////////////////////////////////////////////////////////////
// bit 0: blue
// bit 1: green
// bit 2: red
// bit 3: intensity
static Uint32 dospal[16];


static void init_dos_palette (void) {
  for (int f = 0; f < 16; ++f) {
    Uint8 r = ((f>>2)&0x01)*(0x7f|((f<<4)&0x80));
    Uint8 g = ((f>>1)&0x01)*(0x7f|((f<<4)&0x80));
    Uint8 b = (f&0x01)*(0x7f|((f<<4)&0x80));
    //
    dospal[f] = SDL_MapRGB(screen->format, r, g, b);
  }
}


static inline void putDosPixel (int x, int y, Uint8 col) {
  if (screenReal != NULL && x >= 0 && y >= 0 && x < screenReal->w && y < screenReal->h) {
    *(Uint32 *)(((Uint8 *)screenReal->pixels)+(y*screenReal->pitch)+x*4) = dospal[col];
  }
}


static void dos_put_char (int x, int y, uint8_t ch, uint8_t attr) {
  for (int dy = 0; dy < 16; ++dy) {
    uint8_t b = vgafnt[ch][dy];
    //
    for (int dx = 0; dx < 8; ++dx) {
      putDosPixel(x+dx, y+dy, ((b&0x80 ? attr : attr>>4)&0x0f));
      b = (b&0x7f)<<1;
    }
  }
}


const void *v_endtext = NULL; // !NULL: show it overlayed

static void V_draw_dosscr (int x, int y, const void *scrbuf) {
  const uint8_t *scr = (const uint8_t *)scrbuf;
  //
  SDL_LockSurface(screenReal);
  for (int dy = 0; dy < 25; ++dy) {
    for (int dx = 0; dx < 80; ++dx) {
      dos_put_char(x+dx*8, y+dy*16, scr[0], scr[1]);
      scr += 2;
    }
  }
  SDL_UnlockSurface(screenReal);
}


////////////////////////////////////////////////////////////////////////////////
static void blit1x (void) {
  Uint32 *scrB = screen->pixels; // small
  Uint32 *scrR = screenReal->pixels; // big, real
  int needUnlock = ((SDL_MUSTLOCK(screenReal)) ? SDL_LockSurface(screenReal) == 0 : 0);
  //
  if (vidColorMode > 4) vidColorMode = 0;
  for (int y = 0; y < screen->h; ++y) {
    Uint32 *d = scrR, *s = scrB;
    //
    for (int x = 0; x < screen->w; ++x) {
      Uint32 clr = fixColor(*s++);
      //
      *d++ = clr;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*2);
  }
  //
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void blit2x (void) {
  Uint32 *scrB = screen->pixels; // small
  Uint32 *scrR = screenReal->pixels; // big, real
  int needUnlock = ((SDL_MUSTLOCK(screenReal)) ? SDL_LockSurface(screenReal) == 0 : 0);
  //
  if (vidColorMode > 4) vidColorMode = 0;
  for (int y = 0; y < screen->h; ++y) {
    Uint32 *d = scrR, *d2 = (Uint32 *)((Uint8 *)d+screenReal->pitch), *s = scrB;
    //
    for (int x = 0; x < screen->w; ++x) {
      Uint32 clr = fixColor(*s++);
      //
      d[0] = d[1] = clr;
      d2[0] = d2[1] = clr;
      d += 2; d2 += 2;
      //++s;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*2);
  }
  //
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


static void blit2xTV (void) {
  Uint32 *scrB = screen->pixels; // small
  Uint32 *scrR = screenReal->pixels; // big, real
  int needUnlock = ((SDL_MUSTLOCK(screenReal)) ? SDL_LockSurface(screenReal) == 0 : 0);
  //
  if (vidColorMode > 4) vidColorMode = 0;
  for (int y = 0; y < screen->h; ++y) {
    Uint32 *d = scrR, *d2 = (Uint32 *)((Uint8 *)d+screenReal->pitch), *s = scrB;
    //
    for (int x = 0; x < screen->w; ++x) {
      Uint32 c0 = fixColor(*s), c1;
      //
      c1 = (((c0&0x00ff00ff)*7)>>3)&0x00ff00ff;
      c1 |= (((c0&0x0000ff00)*7)>>3)&0x0000ff00;
      d[0] = d[1] = c0;
      d2[0] = d2[1] = c1;
      d += 2; d2 += 2;
      ++s;
    }
    scrB = (Uint32 *)((Uint8 *)scrB+screen->pitch);
    scrR = (Uint32 *)((Uint8 *)scrR+screenReal->pitch*2);
  }
  //
  if (needUnlock) SDL_UnlockSurface(screenReal);
}


VGA_preblit_cb vga_preblit_cb = NULL;

void V_blitscr (void) {
  V_center(0);
  V_setrect(0, SCRW, 0, SCRH);
  //
  if (convisible && !g_trans) drawconsole();
  //
  if (vga_preblit_cb != NULL) vga_preblit_cb();
  //
  if (screenReal != NULL) {
    if (VID_HQ == 1) blit1x();
    else if (r_tv /*&& !fullscreen*/) blit2xTV();
    else blit2x();
    //
    if (SCRWorig >= 640 && SCRHorig >= 400 && v_endtext != NULL) {
      V_draw_dosscr((SCRWorig-640)/2, (SCRHorig-400)/2, v_endtext);
    }
    //
    SDL_Flip(screenReal);
    screenReal = SDL_GetVideoSurface();
  } else {
    SDL_Flip(screen);
  }
}


////////////////////////////////////////////////////////////////////////////////
CONCMD(fullscreen, "toggle fullscreen mode") {
  int bv, dotoggle = 0;
  //
  CMDCON_HELP();
  if ((bv = boolval(argc, argv)) >= 0) {
    if (bv != fullscreen) dotoggle = 1;
  } else {
    dotoggle = 1;
  }
  //
  if (dotoggle) {
    if (screen != NULL) V_toggle(); else fullscreen = !fullscreen;
  }
}


CONCMD(screen_mode, "set screen mode:\nscreen_mode width height") {
  int w, h;
  //
  CMDCON_HELP();
  if (argc != 3) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if ((w = cmdcon_parseint(argv[1], -1, 320, 8192, NULL)) < 0) {
    conlogf("%s: invalid width: '%s'!\n", argv[0], argv[1]);
    return;
  }
  //
  if ((h = cmdcon_parseint(argv[2], -1, 200, 8192, NULL)) < 0) {
    conlogf("%s: invalid height: '%s'!\n", argv[0], argv[2]);
    return;
  }
  //
  if (screen == NULL) {
    SCRW = w;
    SCRH = h;
    SCRWorig = SCRW;
    SCRHorig = SCRH;
  } else {
    //SDL_FreeSurface(screen);
    SCRW = w;
    SCRH = h;
    SCRWorig = SCRW;
    SCRHorig = SCRH;
    V_init();
    setgamma(gammaa);
  }
  if ((conheight+1)*8+2 > SCRH) conheight = (SCRH-2)/8;
}


CONVAR_BOOL(screenshots, shot_vga, CMDCON_FLAGS_PERSISTENT, "allow screenshots?")


////////////////////////////////////////////////////////////////////////////////
#include "vdzfnt.c"

int drawCharZ (char ch, int x, int y, Uint8 c1, Uint8 c2) {
  if (ch >= 'a' && ch <= 'z') ch -= 32;
  if (ch < 33 || ch > 95) ch = '?';
  if (ch != ' ') {
    const unsigned char *st = zfont+(ch-32)*2;
    int ofs = st[0]+256*st[1], wdt, yofs, hgt = zfont[0];
    //
    st = zfont+ofs;
    wdt = *st++;
    yofs = ((wdt>>4)&0x0f);
    wdt &= 0x0f;
    if (c1 == 255 && c2 == 255) return wdt;
    y += yofs;
    while (hgt-- > 0) {
      int px = x;
      //
      for (int dx = 0; dx < wdt; dx += 4) {
        uint8_t b = *st++;
        //
        for (int c = 0; c < 4; ++c, ++px) {
          switch ((b>>6)&0x03) {
            case 0: break;
            case 1: putPixel(px, y, c1); break;
            case 2: putPixel(px, y, c2); break;
            case 3: /*putPixel(x+dx+c, y+dy, 2);*/ break;
          }
          b = ((b&0x3f)<<2);
        }
      }
      ++y;
    }
    return wdt;
  } else {
    return zfont[1];
  }
}


int drawStrZ (const char *str, int x, int y, Uint8 c1, Uint8 c2) {
  int wdt = 0;
  //
  while (*str) {
    int w = drawCharZ(*str++, x, y, c1, c2);
    //
    wdt += w;
    x += w;
  }
  return wdt;
}


void V_fadescr (Uint8 alpha) {
  Uint32 s = ((255-alpha)<<24);
  //
  for (int y = 0; y < SCRH; ++y) {
    Uint32 *d = (Uint32 *)(((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch));
    for (int x = 0; x < SCRW; ++x, ++d) {
      pixel_blend32(d, s);
    }
  }
}
