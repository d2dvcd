/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Items
#ifndef D2D_ITEMS_H
#define D2D_ITEMS_H

#include <stdio.h>

#include "common.h"
#include "saveload.h"


enum {
  I_NONE,
  I_CLIP,
  I_SHEL,
  I_ROCKET,
  I_CELL,
  I_AMMO,
  I_SBOX,
  I_RBOX,
  I_CELP,
  I_STIM,
  I_MEDI,
  I_BPACK,
  I_CSAW,
  I_SGUN,
  I_SGUN2,
  I_MGUN,
  I_LAUN,
  I_PLAS,
  I_BFG,
  I_ARM1,
  I_ARM2,
  I_MEGA,
  I_INVL,
  I_AQUA,
  I_KEYR,
  I_KEYG,
  I_KEYB,
  I_SUIT,
  I_SUPER,
  I_RTORCH,
  I_GTORCH,
  I_BTORCH,
  I_GOR1,
  I_FCAN,
  I_GUN2
};


typedef struct GCC_PACKED {
  int16_t x;
  int16_t y;
  int16_t t;
  uint16_t f;
} thing_t;

extern int itm_rtime;

extern void IT_init (void);
extern void IT_alloc (void);
extern void IT_act (void);
extern void IT_draw (void);
extern int IT_spawn (int x, int y, int i); // returns item id
extern void IT_drop_ammo (int i, int n, int x, int y);

extern int IT_edit_act (void);


#endif
