/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// File operations
#ifndef D2D_FILES_H
#define D2D_FILES_H

#include <setjmp.h>
#include <stdio.h>

#include "common.h"
#include "membuffer.h"


//extern int d_start, d_end;

extern void M_startup (void);
extern void M_shutdown (void);
extern void *M_lock (int);
extern void M_unlock (void *p);

extern void F_startup (void);
extern void F_addwad (const char *fn, int required);
extern void F_initwads (void);
extern void F_allocres (void);

extern MemBuffer *F_loadres2buf (int r); // NULL if r is invalid or error
//extern MemBuffer *F_loadres2bufex (int r, uint32_t ofs, uint32_t size);

extern void F_loadres (int r, void *p, uint32_t ofs, uint32_t size);
extern int F_getresid (const char *n);
extern void F_getresname (char *n, int r);
extern int F_findres (const char *n);
extern int F_getsprid (const char n[4], int s, int d);
extern int F_getreslen (int r);
extern void F_loadmus (const char n[8]);
extern void F_freemus (void);
extern void F_nextmus (char *s);

extern void F_randmus (char *s);

extern void *F_loadres1 (int idx, int *size);


// flXXX() will do longjmp() on error
extern jmp_buf flJmpBuf; // set this before using flXXX()

// error codes for flXXX()
enum {
  FL_NO_ERROR,
  FL_READ_ERROR,
  FL_WRITE_ERROR,
  FL_OTHER_ERROR
};

extern void flError (int errCode);

extern ccBool flReadBool (FILE *fl);
extern uint8_t flReadUI8 (FILE *fl);
extern uint16_t flReadUI16 (FILE *fl);
extern uint32_t flReadUI32 (FILE *fl);
extern int8_t flReadI8 (FILE *fl);
extern int16_t flReadI16 (FILE *fl);
extern int32_t flReadI32 (FILE *fl);

extern void flWriteBool (FILE *fl, ccBool v);
extern void flWriteUI8 (FILE *fl, uint8_t v);
extern void flWriteUI16 (FILE *fl, uint16_t v);
extern void flWriteUI32 (FILE *fl, uint32_t v);
extern void flWriteI8 (FILE *fl, int8_t v);
extern void flWriteI16 (FILE *fl, int16_t v);
extern void flWriteI32 (FILE *fl, int32_t v);

extern void flReadBuf (FILE *fl, void *buf, size_t buflen);
extern void flWriteBuf (FILE *fl, const void *buf, size_t buflen);


#endif
