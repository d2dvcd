/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include "vgaovl.h"


////////////////////////////////////////////////////////////////////////////////
#include "vdcur.c"


////////////////////////////////////////////////////////////////////////////////
VOverlay *createVO (int w, int h) {
  if (w > 0 && h > 0) {
    VOverlay *res = malloc(sizeof(VOverlay));
    //
    if (res != NULL) {
      if ((res->data = malloc(w*h)) != NULL) {
        res->w = w;
        res->h = h;
        return res;
      }
      free(res);
    }
  }
  return NULL;
}


VOverlay *createVOFromIcon (const void *iconbuf) {
  if (iconbuf != NULL) {
    const uint8_t *ibuf = (const uint8_t *)iconbuf;
    int w = ibuf[0], h = ibuf[1];
    VOverlay *res = createVO(w, h);
    //
    if (res != NULL) {
      clearVO(res, 255);
      ibuf += 2;
      for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
          putPixelVO(res, x, y, *ibuf++);
        }
      }
      return res;
    }
  }
  return NULL;
}


#include "vga_icons.c"

VOverlay *createVOFromStockIcon (int icn) {
  const uint8_t *i;
  //
  switch (icn) {
    case VGA_ICON_PENTA: i = vgaicon_penta; break;
    case VGA_ICON_TURTLE: i = vgaicon_turtle; break;
    case VGA_ICON_RAM: i = vgaicon_ram; break;
    default: return NULL;
  }
  //
  return createVOFromIcon(i);
}


void freeVO (VOverlay *v) {
  if (v != NULL) {
    if (v->data != NULL) free(v->data);
    free(v);
  }
}

void clearVO (VOverlay *v, Uint8 c) {
  if (v != NULL && v->w > 0 && v->h > 0) memset(v->data, c, v->w*v->h);
}


////////////////////////////////////////////////////////////////////////////////
void drawChar6VO (VOverlay *v, char ch, int x, int y, Uint8 fg, Uint8 bg) {
  if (v != NULL && x > -6 && y > -8 && x < v->w+6 && y < v->h+8) {
    int pos = (ch&0xff)*8;
    //
    for (int dy = 0; dy < 8; ++dy, ++y) {
      uint8_t b = vga_font6x8[pos++];
      //
      for (int dx = 0; dx < 6; ++dx) {
        Uint8 c = (b&0x80 ? fg : bg);
        //
        if (c != 255) putPixelVO(v, x+dx, y, c);
        b = (b&0x7f)<<1;
      }
    }
  }
}


void drawStr6VO (VOverlay *v, const char *str, int x, int y, Uint8 fg, Uint8 bg) {
  if (str != NULL) for (; *str; ++str, x += 6) drawChar6VO(v, *str, x, y, fg, bg);
}


void drawChar8VO (VOverlay *v, char ch, int x, int y, Uint8 fg, Uint8 bg) {
  if (v != NULL && x > -8 && y > -8 && x < v->w+8 && y < v->h+8) {
    int pos = (ch&0xff)*8;
    //
    for (int dy = 0; dy < 8; ++dy, ++y) {
      uint8_t b = vga_font8x8[pos++];
      //
      for (int dx = 0; dx < 8; ++dx) {
        Uint8 c = (b&0x80 ? fg : bg);
        //
        if (c != 255) putPixelVO(v, x+dx, y, c);
        b = (b&0x7f)<<1;
      }
    }
  }
}


void drawStr8VO (VOverlay *v, const char *str, int x, int y, Uint8 fg, Uint8 bg) {
  if (str != NULL) for (; *str; ++str, x += 8) drawChar8VO(v, *str, x, y, fg, bg);
}


void drawFrameVO (VOverlay *v, int x0, int y0, int w, int h, Uint8 c) {
  if (v != NULL && w > 0 && h > 0 && x0 > -w && y0 > -h && x0 < v->w && y0 < v->h) {
    if (x0 < 0) { if ((w += x0) < 1) return; x0 = 0; }
    if (y0 < 0) { if ((h += y0) < 1) return; y0 = 0; }
    if (x0+w > v->w) { if ((w = v->w-x0) < 1) return; }
    if (y0+h > v->h) { if ((h = v->h-y0) < 1) return; }
    //
    memset(v->data+y0*v->w+x0, c, w);
    if (h > 1) memset(v->data+(y0+h-1)*v->w+x0, c, w);
    if (h > 2) {
      --w;
      h -= 2;
      for (uint8_t *d = v->data+(y0+1)*v->w+x0; h > 0; --h, d += v->w) d[0] = d[w] = c;
    }
  }
}


void fillRectVO (VOverlay *v, int x0, int y0, int w, int h, Uint8 c) {
  if (v != NULL && w > 0 && h > 0 && x0 > -w && y0 > -h && x0 < v->w && y0 < v->h) {
    if (x0 < 0) { if ((w += x0) < 1) return; x0 = 0; }
    if (y0 < 0) { if ((h += y0) < 1) return; y0 = 0; }
    if (x0+w > v->w) { if ((w = v->w-x0) < 1) return; }
    if (y0+h > v->h) { if ((h = v->h-y0) < 1) return; }
    //
    for (uint8_t *d = v->data+y0*v->w+x0; h > 0; --h, d += v->w) memset(d, c, w);
  }
}


////////////////////////////////////////////////////////////////////////////////
void blitVO (VOverlay *v, int x0, int y0, Uint8 alpha) {
  if (v != NULL && x0 > -v->w && y0 > -v->h && x0 < frameSfc->w && y0 < frameSfc->h && alpha > 0) {
    int w = v->w, h = v->h;
    const uint8_t *vs = v->data;
    Uint8 *dest;
    //
    if (y0 < 0) {
      // skip invisible top part
      if ((h += y0) < 1) return;
      vs -= y0*v->w;
      y0 = 0;
    }
    if (y0+h > frameSfc->h) {
      // don't draw invisible bottom part
      if ((h = frameSfc->h-y0) < 1) return;
    }
    //
    if (x0 < 0) {
      // skip invisible left part
      if ((w += x0) < 1) return;
      vs -= x0;
      x0 = 0;
    }
    if (x0+w > frameSfc->w) {
      // don't draw invisible right part
      if ((w = frameSfc->w-x0) < 1) return;
    }
    //
    dest = (((Uint8 *)frameSfc->pixels)+(y0*frameSfc->pitch)+x0*4);
    if (alpha == 255) {
      for (; h > 0; --h, dest += frameSfc->pitch, vs += v->w) {
        Uint32 *d = (Uint32 *)dest;
        const uint8_t *s = vs;
        //
        for (int dx = w; dx > 0; --dx, ++d, ++s) if (*s != 255) *d = palette[*s];
      }
    } else {
      for (; h > 0; --h, dest += frameSfc->pitch, vs += v->w) {
        Uint32 *d = (Uint32 *)dest;
        const uint8_t *s = vs;
        const uint32_t a = alpha+1; // to not loose bits
        //
        for (int dx = w; dx > 0; --dx, ++d, ++s) {
          uint8_t c;
          //
          if ((c = *s) != 255) {
            const uint32_t s = palette[c];
            const uint32_t srb = (s&0xff00ff);
            const uint32_t sg = (s&0x00ff00);
            const uint32_t drb = ((*d)&0xff00ff);
            const uint32_t dg = ((*d)&0x00ff00);
            const uint32_t orb = (drb+(((srb-drb)*a+0x800080)>>8))&0xff00ff;
            const uint32_t og = (dg+(((sg-dg)*a+0x008000)>>8))&0x00ff00;
            //
            *d = (orb|og);
          }
        }
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void vDrawChar (VExSurface *sfc, int x, int y, char ch, Uint8 ink, Uint8 paper) {
  int ca = ((uint8_t)ch)*8;
  //
  for (int dy = 0; dy < 8; ++dy, ++y, x -= 6) {
    uint8_t b = vga_font6x8[ca++];
    //
    for (int dx = 0; dx < 6; ++dx, ++x, b <<= 1) {
      vPutPixel(sfc, x, y, b&0x80?ink:paper);
    }
  }
}


void vDrawText (VExSurface *sfc, int x, int y, const char *str, Uint8 ink, Uint8 paper) {
  int sx = x;
  //
  for (; *str; ++str, x += 6) {
    switch (*str) {
      case '\n': x = sx-6; y += 8; break;
      default:
        vDrawChar(sfc, x, y, *str, ink, paper);
        break;
    }
  }
}


void vDrawOutlineText (VExSurface *sfc, int x, int y, const char *str, Uint8 ink, int oink) {
  for (int dy = -1; dy <= 1; ++dy) {
    for (int dx = -1; dx <= 1; ++dx) {
      if (dx || dy) vDrawText(sfc, x+dx, y+dy, str, oink, -1);
    }
  }
  vDrawText(sfc, x, y, str, ink, -1);
}


void vDrawHLine (VExSurface *sfc, int x, int y, int len, Uint8 clr) {
  for (; len > 0; --len, ++x) vPutPixel(sfc, x, y, clr);
}


void vDrawVLine (VExSurface *sfc, int x, int y, int len, Uint8 clr) {
  for (; len > 0; --len, ++y) vPutPixel(sfc, x, y, clr);
}


void vDrawBar (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr) {
  for (int dy = 0; dy < height; ++dy, ++y) vDrawHLine(sfc, x, y, width, clr);
}


void vDrawRect (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr) {
  if (width > 0 && height > 0) {
    vDrawHLine(sfc, x, y, width, clr);
    vDrawHLine(sfc, x, y+height-1, width, clr);
    vDrawVLine(sfc, x, y+1, height-2, clr);
    vDrawVLine(sfc, x+width-1, y+1, height-2, clr);
  }
}


void vDrawRectW (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr) {
  if (width > 0 && height > 0) {
    vDrawHLine(sfc, x+1, y, width-2, clr);
    vDrawHLine(sfc, x+1, y+height-1, width-2, clr);
    vDrawVLine(sfc, x, y+1, height-2, clr);
    vDrawVLine(sfc, x+width-1, y+1, height-2, clr);
  }
}


void vDrawZXStripe (VExSurface *sfc, int x, int y, int dim) {
  static const Uint8 clrs[4] = {2+8, 6+8, 4+8, 5+8};
  //
  dim = (dim ? 8 : 0);
  for (int f = 0; f < 4; ++f, x += 8) {
    for (int y = 0; y < 8; ++y) vDrawHLine(sfc, x+(7-y), y, 8, clrs[f]-dim);
  }
}


void vDrawWindow (VExSurface *sfc, int x, int y, int width, int height, const char *title, int tink, int tpaper, int wpaper, int flags) {
  char tit[52];
  int w;
  //
  if (width < 1 || (title && height < 9) || (!title && height < 1)) return;
  strncpy(tit, (title ? title : ""), sizeof(tit));
  tit[sizeof(tit)-1] = '\0';
  w = width/6;
  if (strlen(tit) > w) tit[w] = '\0';
  // frame and title
  vDrawRectW(sfc, x, y, width, height, tpaper);
  vDrawBar(sfc, x+1, y+1, width-2, 7, tpaper);
  if (width >= 56 && (flags&ZXVIDWF_STRIPES)) vDrawZXStripe(sfc, x+width-44, y, (flags&ZXVIDWF_STRIPES_DIM));
  //
  if (title != NULL && title[0]) {
    VClipInfo ci;
    //
    vSaveClip(sfc, &ci);
    vRestrictClip(sfc, x+1, y, width-2, 8);
    vDrawText(sfc, x+1, y, tit, tink, 255);
    vRestoreClip(sfc, &ci);
  }
  // window
  vDrawBar(sfc, x+1, y+8, width-2, height-9, wpaper);
  //
  vPutPixel(sfc, x+1, y+height-2, tpaper);
  vPutPixel(sfc, x+width-2, y+height-2, tpaper);
}


////////////////////////////////////////////////////////////////////////////////
static void vDrawVArrow (VExSurface *sfc, const char *bmp, int x, int y, Uint8 ink, Uint8 paper) {
  for (int dy = 0; dy < 3; ++dy) {
    for (int dx = 0; dx < 5; ++dx) {
      vPutPixel(sfc, x+dx, y+dy, bmp[dy*5+dx]?ink:paper);
    }
  }
}


static void vDrawHArrow (VExSurface *sfc, const char *bmp, int x, int y, Uint8 ink, Uint8 paper) {
  for (int dy = 0; dy < 5; ++dy) {
    for (int dx = 0; dx < 3; ++dx) {
      vPutPixel(sfc, x+dx, y+dy, bmp[dy*3+dx]?ink:paper);
    }
  }
}


void vDrawUpArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[3][5] = {
    {0,0,1,0,0},
    {0,1,1,1,0},
    {1,1,1,1,1},
  };
  vDrawVArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


void vDrawDownArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[3][5] = {
    {1,1,1,1,1},
    {0,1,1,1,0},
    {0,0,1,0,0},
  };
  vDrawVArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


void vDrawLeftArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[5][3] = {
    {0,0,1},
    {0,1,1},
    {1,1,1},
    {0,1,1},
    {0,0,1},
  };
  vDrawHArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


void vDrawRightArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper) {
  static const char bmp[5][3] = {
    {1,0,0},
    {1,1,0},
    {1,1,1},
    {1,1,0},
    {1,0,0},
  };
  vDrawHArrow(sfc, (const char *)bmp, x, y, ink, paper);
}


////////////////////////////////////////////////////////////////////////////////
int vLineCountMaxLen (const char *str, int *maxlenp) {
  int maxlen = 0, lineCnt = 0;
  //
  if (str) {
    while (*str) {
      const char *np = strchr(str, '\n');
      //
      if (!np) np = str+strlen(str);
      if (np-str > maxlen) maxlen = np-str;
      str = np;
      if (*str) ++str;
      ++lineCnt;
    }
  }
  if (maxlenp) *maxlenp = maxlen;
  return lineCnt;
}


////////////////////////////////////////////////////////////////////////////////
void vDrawLine (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr) {
  int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
  int err = dx+dy; /* error value e_xy */
  //
  for (;;) {
    int e2;
    //
    vPutPixel(sfc, x0, y0, clr);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


void vDrawCircle (VExSurface *sfc, int cx, int cy, int radius, Uint8 clr) {
  int error = -radius, x = radius, y = 0;
  //
  void plot4points (int cx, int cy, int x, int y) {
    vPutPixel(sfc, cx+x, cy+y, clr);
    if (x != 0) vPutPixel(sfc, cx-x, cy+y, clr);
    if (y != 0) vPutPixel(sfc, cx+x, cy-y, clr);
    vPutPixel(sfc, cx-x, cy-y, clr);
  }
  //
  void plot8points (int cx, int cy, int x, int y) {
    plot4points(cx, cy, x, y);
    plot4points(cx, cy, y, x);
  }
  //
  if (radius < 1) return;
  if (radius == 1) { vPutPixel(sfc, cx, cy, clr); return; }
  //
  while (x > y) {
    plot8points(cx, cy, x, y);
    error += y*2+1;
    ++y;
    if (error >= 0) { --x; error -= x*2; }
  }
  plot4points(cx, cy, x, y);
}


void vDrawEllipse (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr) {
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; /* values of diameter */
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  long err = dx+dy+b1*a*a; /* error of 1.step */
  //
  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points... */
  if (y0 > y1) y0 = y1; /* ...exchange them */
  y0 += (b+1)/2; y1 = y0-b1;  /* starting pixel */
  a *= 8*a; b1 = 8*b*b;
  //
  do {
    int e2;
    //
    vPutPixel(sfc, x1, y0, clr); /*   I. Quadrant */
    vPutPixel(sfc, x0, y0, clr); /*  II. Quadrant */
    vPutPixel(sfc, x0, y1, clr); /* III. Quadrant */
    vPutPixel(sfc, x1, y1, clr); /*  IV. Quadrant */
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } /* x step */
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  /* y step */
  } while (x0 <= x1);
  //
  while (y0-y1 < b) {
    /* too early stop of flat ellipses a=1 */
    vPutPixel(sfc, x0-1, ++y0, clr); /* -> complete tip of ellipse */
    vPutPixel(sfc, x0-1, --y1, clr);
  }
}


void vDrawFilledCircle (VExSurface *sfc, int cx, int cy, int radius, Uint8 clr) {
  int error = -radius, x = radius, y = 0;
  //
  /*
  void plot4points (int cx, int cy, int x, int y) {
    vPutPixel(sfc, cx+x, cy+y, clr);
    if (x != 0) vPutPixel(sfc, cx-x, cy+y, clr);
    if (y != 0) vPutPixel(sfc, cx+x, cy-y, clr);
    vPutPixel(sfc, cx-x, cy-y, clr);
  }
  //
  void plot8points (int cx, int cy, int x, int y) {
    plot4points(cx, cy, x, y);
    plot4points(cx, cy, y, x);
  }
  */
  //
  if (radius < 1) return;
  if (radius == 1) { vPutPixel(sfc, cx, cy, clr); return; }
  //
  while (x > y) {
    vDrawHLine(sfc, cx-x, cy+y, x*2, clr);
    vDrawHLine(sfc, cx-y, cy+x, y*2, clr);
    vDrawHLine(sfc, cx-x, cy-y, x*2, clr);
    vDrawHLine(sfc, cx-y, cy-x, y*2, clr);
    error += y*2+1;
    ++y;
    if (error >= 0) { --x; error -= x*2; }
  }
  vDrawHLine(sfc, cx-x, cy-y, x*2, clr);
  vDrawHLine(sfc, cx-x, cy+y, x*2, clr);
  //plot4points(cx, cy, x, y);
}


void vDrawFilledEllipse (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr) {
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; /* values of diameter */
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; /* error increment */
  long err = dx+dy+b1*a*a; /* error of 1.step */
  //
  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points... */
  if (y0 > y1) y0 = y1; /* ...exchange them */
  y0 += (b+1)/2; y1 = y0-b1;  /* starting pixel */
  a *= 8*a; b1 = 8*b*b;
  //
  do {
    int e2;
    //
    vDrawHLine(sfc, x0, y0, x1-x0+1, clr);
    vDrawHLine(sfc, x0, y1, x1-x0+1, clr);
    //vPutPixel(sfc, x1, y0, 2); /*   I. Quadrant */
    //vPutPixel(sfc, x0, y0, 2); /*  II. Quadrant */
    //vPutPixel(sfc, x0, y1, clr); /* III. Quadrant */
    //vPutPixel(sfc, x1, y1, clr); /*  IV. Quadrant */
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } /* x step */
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  /* y step */
  } while (x0 <= x1);
  //
  while (y0-y1 < b) {
    /* too early stop of flat ellipses a=1 */
    vPutPixel(sfc, x0-1, ++y0, clr); /* -> complete tip of ellipse */
    vPutPixel(sfc, x0-1, --y1, clr);
  }
}


////////////////////////////////////////////////////////////////////////////////
void vDrawSelectionRect (VExSurface *sfc, int phase, int x0, int y0, int wdt, int hgt, Uint8 col0, Uint8 col1) {
  if (wdt < 1 || hgt < 1) return;
  // top
  for (int f = x0; f < x0+wdt; ++f, ++phase) vPutPixel(sfc, f, y0, ((phase %= 4)<2)?col0:col1);
  // right
  for (int f = y0+1; f < y0+hgt; ++f, ++phase) vPutPixel(sfc, x0+wdt-1, f, ((phase %= 4)<2)?col0:col1);
  // bottom
  for (int f = x0+wdt-2; f >= x0; --f, ++phase) vPutPixel(sfc, f, y0+hgt-1, ((phase %= 4)<2)?col0:col1);
  // left
  for (int f = y0+hgt-2; f >= y0; --f, ++phase) vPutPixel(sfc, x0, f, ((phase %= 4)<2)?col0:col1);
}


////////////////////////////////////////////////////////////////////////////////
Uint8 msCurClr0 = 0;
Uint8 msCurClr1 = 4;


void drawMouseCursor (int x, int y, int type) {
  if (type < 0) type = AID_COOKE; else if (type > AID_EMPTY) type = AID_COOKE;
  for (int dy = 0; dy < 16; ++dy, ++y) {
    for (int dx = 0; dx < 16; ++dx) {
      switch (winCur[type][dy][dx]) {
        case 1: putPixel(x+dx, y, msCurClr0); break;
        case 2: putPixel(x+dx, y, msCurClr1); break;
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
#include "vga_inn.c"


void drawInn (int x, int y, uint8_t phase) {
  phase &= 0x07;
  for (int dy = 0; dy < 32; ++dy) {
    for (int dx = 0; dx < 4; ++dx) {
      uint8_t msk = ~(inn[phase][dy*8+dx*2]);
      uint8_t bt = inn[phase][dy*8+dx*2+1];
      //
      for (int sx = 0; sx < 8; ++sx, msk = (msk&0x7f)<<1, bt = (bt&0x7f)<<1) {
        if (msk&0x80) putPixel(x+dx*8+sx, y+dy, (bt&0x80 ? msCurClr1 : msCurClr0));
      }
    }
  }
}
