/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "sound.h"

#include <ctype.h>

#include "SDL.h"
#ifndef NOSOUND
# include "SDL_mixer.h"
#endif

#include "common.h"
#include "cmdcon.h"
#include "config.h"
#include "error.h"
#include "files.h"
#include "game.h"

#include "../libdmmxm/dmmxm.h"


#define NUM_CHANNELS  (16)


int sound_inited = 0;

ccBool snddisabled = CC_TRUE;
int snd_vol = 50;
int mus_vol = 50;


ccBool musdisabled = CC_TRUE;
ccBool music_random = CC_TRUE;
int music_time = 3;
int music_fade = 5;
Uint32 muscount;

ccBool musdisabledc = CC_FALSE;
ccBool snddisabledc = CC_FALSE;


#ifndef NOSOUND
void S_initmusic (void);
void S_donemusic (void);


static SDL_RWops *musRW = NULL;
static char *musBuf = NULL;

Mix_Music *muschan = NULL;


GCC_DESTRUCTOR_USER {
  if (musRW != NULL) SDL_FreeRW(musRW);
  musRW = NULL;
  if (musBuf != NULL) free(musBuf);
  musBuf = NULL;
}


typedef struct SndChunk {
  const snd_t *s;
  Mix_Chunk *c;
  struct SndChunk *next;
} SndChunk;

static SndChunk *sclist = NULL;


static Mix_Chunk *getChunk (const snd_t *s, int r, int v) {
  const Uint8 *data;
  Uint32 dlen;
  SDL_AudioCVT cvt;
  Mix_Chunk *chunk;
  SndChunk *sc;
  int srate = s->rate;
  //
  //dlogf("getChunk(%p): len=%u; rate=%u; lstart=%u; llen=%u\n", s, s->len, s->rate, s->lstart, s->llen);
  dlogf("getChunk(%p): len=%u; rate=%u; lstart=%u; llen=%u\n", s, s->len, s->rate, s->lstart, s->llen);
  //sc = NULL; sc->s = s;
  //
  for (sc = sclist; sc != NULL; sc = sc->next) {
    if (sc->s == s) return sc->c;
  }
  //
  //if (srate == 11111) srate = 11025;
  if (srate%11025) {
    if (srate%11025 > 0 && srate%11025 < 512) srate = (srate/11025)*11025;
    else if (srate%11025 > 11025-512)  srate = ((srate/11025)+1)*11025;
    dlogf(" fixed srate: %d\n", srate);
  }
  //
  if ((sc = malloc(sizeof(SndChunk))) == NULL) ERR_fatal("Out of memory\n");
  data = (const Uint8 *)s+sizeof(snd_t);
  dlen = s->len;
  SDL_BuildAudioCVT(&cvt, AUDIO_S8, 1, srate, AUDIO_S16, 1, 22050);
  if (!(cvt.buf = malloc(dlen*cvt.len_mult))) ERR_fatal("Out of memory\n");
  memcpy(cvt.buf, data, dlen);
  cvt.len = dlen;
  SDL_ConvertAudio(&cvt);
  //
  if (!(chunk = malloc(sizeof(Mix_Chunk)))) ERR_fatal("Out of memory\n");
  chunk->abuf = cvt.buf;
  chunk->alen = cvt.len_cvt;
  chunk->allocated = 0;
  chunk->volume = (float)v/255*SDL_MIX_MAXVOLUME;
  //
  sc->s = s;
  sc->c = chunk;
  sc->next = sclist;
  sclist = sc;
  //
  return chunk;
}


void free_chunks (void) {
  if (!snddisabled) {
    Mix_HaltChannel(-1);
    while (sclist != NULL) {
      SndChunk *sc = sclist;
      //
      sclist = sc->next;
      if (sc->c != NULL) {
        free(sc->c->abuf);
        free(sc->c);
      }
      free(sc);
    }
  }
}


void S_init (void) {
  if (!snddisabledc && SDL_WasInit(SDL_INIT_AUDIO) == 0) {
    snddisabled = 1;
    if (SDL_InitSubSystem(SDL_INIT_AUDIO) < 0) {
      fprintf(stderr, "\nUnable to initialize audio:  %s\n", SDL_GetError());
      return;
    }
    if (Mix_OpenAudio(22050, AUDIO_S16, 1, 1000) < 0) {
      fprintf(stderr, "Error initializing SDL_mixer: %s\n", Mix_GetError());
      return;
    }
    if (Mix_AllocateChannels(NUM_CHANNELS) != NUM_CHANNELS) {
      fprintf(stderr, "Error allocation channels: %s\n", Mix_GetError());
      return;
    }
  }
  //
  S_initmusic();
  //
  snddisabled = snddisabledc;
  S_volume(snd_vol);
  //
  sound_inited = 1;
}


void S_done (void) {
  S_donemusic();
  free_chunks();
  if (SDL_WasInit(SDL_INIT_AUDIO)) {
    Mix_CloseAudio();
    SDL_QuitSubSystem(SDL_INIT_AUDIO);
  }
  snddisabled = 1;
}


short S_play (const snd_t *s, short c, unsigned r, short v) {
  if (!snddisabled) {
    Mix_Chunk *chunk = getChunk(s, r, v);
    //
    if (chunk != NULL) return Mix_PlayChannel(c, chunk, 0);
  }
  //
  return -1;
}


void S_stop (short c) {
  if (!snddisabled) {
    if (c >= 0) Mix_HaltChannel(c);
  }
}


void S_volume (int v) {
  if (!snddisabled) {
    snd_vol = v;
    if (snd_vol > 128) snd_vol = 128;
    if (snd_vol < 0) snd_vol = 0;
    Mix_Volume(-1, snd_vol);
  }
}


void S_wait (void) {
  if (!snddisabled) {
    while (Mix_Playing(-1)) SDL_Delay(10);
  }
}


void S_initmusic (void) {
  if (!musdisabledc && SDL_WasInit(SDL_INIT_AUDIO) == 0) {
    musdisabled = 1;
    if (SDL_InitSubSystem(SDL_INIT_AUDIO) < 0) {
      fprintf(stderr, "\nUnable to initialize audio:  %s\n", SDL_GetError());
      return;
    }
    if (Mix_OpenAudio(22050, AUDIO_S16, 1, 1000) < 0) {
      fprintf(stderr, "Error initializing SDL_mixer: %s\n", Mix_GetError());
      musdisabled = 1;
      return;
    }
  }
  //
  muschan = NULL;
  muscount = 0;
  musdisabled = musdisabledc;
  S_volumemusic(mus_vol);
}


void S_donemusic (void) {
  if (SDL_WasInit(SDL_INIT_AUDIO) && !musdisabled) {
    S_stopmusic();
    F_freemus();
    //Mix_CloseAudio();
    //SDL_QuitSubSystem(SDL_INIT_AUDIO);
  }
}


static volatile int restartMusic = 0;

static void musicFinishedCB (void) {
  //dlogf("music finished!");
  restartMusic = 1;
}


void S_startmusic (int time) {
  if (!musdisabled) {
    restartMusic = 0;
    if (muschan != NULL) {
      if (Mix_PlayMusic(muschan, 1) == 0) {
        Mix_HookMusicFinished(musicFinishedCB);
        Mix_VolumeMusic(mus_vol);
      } else {
        Mix_HookMusicFinished(NULL);
      }
    } else {
      Mix_HookMusicFinished(NULL);
      Mix_HaltMusic();
    }
    muscount = time*60*1000/DELAY;
  }
}


void S_stopmusic (void) {
  if (!musdisabled) {
    Mix_HookMusicFinished(NULL);
    restartMusic = 0;
    Mix_HaltMusic();
    muscount = 0;
  }
}


void S_volumemusic (int v) {
  if (!musdisabled) {
    mus_vol = v;
    if (mus_vol > 128) mus_vol = 128;
    if (mus_vol < 0) mus_vol = 0;
    if (mus_vol == 0 && Mix_PlayingMusic()) {
      S_stopmusic();
    } else if (mus_vol > 0 && !Mix_PlayingMusic()) {
      S_startmusic(music_time);
    } else {
      Mix_VolumeMusic(mus_vol);
    }
  }
}


static void restoreMusicVol (void) {
  /*if (mus_vol > 0)*/ Mix_VolumeMusic(mus_vol);
}


static const struct {
  Uint8 ascii;
  Uint8 asciilc;
  const char *ch;
} atrans[] = {
  {0x80, 0xA0, "a"},//�
  {0x81, 0xA1, "b"},//�
  {0x82, 0xA2, "v"},//�
  {0x83, 0xA3, "g"},//�
  {0x84, 0xA4, "d"},//�
  {0x85, 0xA5, "e"},//�
  {0x86, 0xA6, "zh"},//�
  {0x87, 0xA7, "z"},//�
  {0x88, 0xA8, "i"},//�
  {0x89, 0xA9, "j"},//�
  {0x8A, 0xAA, "k"},//�
  {0x8B, 0xAB, "l"},//�
  {0x8C, 0xAC, "m"},//�
  {0x8D, 0xAD, "n"},//�
  {0x8E, 0xAE, "o"},//�
  {0x8F, 0xAF, "p"},//�
  {0x90, 0xE0, "r"},//�
  {0x91, 0xE1, "s"},//�
  {0x92, 0xE2, "t"},//�
  {0x93, 0xE3, "u"},//�
  {0x94, 0xE4, "f"},//�
  {0x95, 0xE5, "h"},//�
  {0x96, 0xE6, "c"},//�
  {0x97, 0xE7, "ch"},//�
  {0x98, 0xE8, "sh"},//�
  {0x99, 0xE9, "sch"},//�
  {0x9A, 0xEA, "x"},//�
  {0x9B, 0xEB, "y"},//�
  {0x9C, 0xEC, "j"},//�
  {0x9D, 0xED, "e"},//�
  {0x9E, 0xEE, "hu"},//�
  {0x9F, 0xEF, "ja"},//�
  {0}
};


static const char *get_trans_char (Uint8 c) {
  for (int i = 0; atrans[i].ascii; ++i) {
    if (atrans[i].ascii == c || atrans[i].asciilc == c) return atrans[i].ch;
  }
  return NULL;
}


static void trans_ascii_str (char *dest, const char *src) {
  for (int i = 0; i < strlen(src); i++) {
    const char *ch = get_trans_char(src[i]);
    //
    if (ch != NULL) {
      for (; *ch; ++ch) *dest++ = tolower(*ch);
    } else {
      *dest++ = tolower(src[i]);
    }
  }
  *dest = '\0';
}


// bool
static int loadXM (const char n[8]) {
  static char name[4096], f[16];
  //
  strcpy(name, "/usr/share/doom2d-vcd/music/");
  strncpy(f, n, 8);
  f[8] = '\0';
  trans_ascii_str(name+strlen(name), f);
  strcat(name, ".xm");
  muschan = Mix_LoadMUS(name);
  if (muschan == NULL) {
    //dlogf("Music not found: '%s'", name);
    sprintf(name, "%s/music/", get_bin_dir());
    strncpy(f, n, 8);
    f[8] = '\0';
    trans_ascii_str(name+strlen(name), f);
    strcat(name, ".xm");
    muschan = Mix_LoadMUS(name);
    if (muschan == NULL) {
      /*
      name[0] = 0;
      strncpy(f, n, 8);
      f[8] = '\0';
      trans_ascii_str(name+strlen(name), f);
      strcat(name, ".xm");
      dlogf("Music not found: '%s'", name);
      */
      return 0;
    } else {
      dlogf("music: '%s'", name);
    }
  } else {
    dlogf("music: '%s'", name);
  }
  //
  return 1;
}


//CP866, DO NOT RECODE!
static const struct {
  int q;
  const char *name;
} knownQ[] = {
  { 8, "ALLRIGHT"},
  { 4, "BEST"},
  { 8, "GLAD"},
  { 8, "MENU"},
  {16, "INTERMUS"},
  {12, "SUPER"},
  { 8, "��"},
  { 6, "����"},
  { 8, "����"},
  { 8, "���_���"},
  {12, "��������"},
  { 8, "�_�����"},
  { 4, "�����"},
  { 8, "��������"},
  {16, "������"},
  { 8, "������"},
  { 4, "�����"},
  { 4, "����"},
  { 8, "������#6"},
  { 4, "������"},
  { 8, "�����"},
  { 8, "��������"},
  { 8, "����"},
  {12, "����"},
  { 4, "�����"},
  {12, "����"},
  {10, "��_�����"},
  {16, "��_����"},
  { 4, "����"},
  {12, "�������"},
};


//FIXME: search only inside music section
static void *openFileWAD (const char *fname, int *fsize) {
  int len;
  char nm[9];
  //
  if (fname == NULL || !fname[0]) return NULL;
  for (len = 0; len < 8 && fname[len]; ++len) ;
  memset(nm, 0, sizeof(nm));
  memcpy(nm, fname, len);
  dlogf("openFileWAD: [%s]", nm);
  //
  return F_loadres1(F_getresid(nm), fsize);
}


// bool
static int loadDMM (const char n[8]) {
  int indmmsize;
  void *indmm;
  int outxmsize;
  void *outxm;
  DMMCvtInfo cvt;
  char nm[9];
  int idx;
  //
  if ((idx = F_findres(n)) < 0) return 0;
  indmm = F_loadres1(idx, &indmmsize);
  nm[8] = 0;
  strncpy(nm, n, 8);
  //
  if (musBuf != NULL) free(musBuf);
  musBuf = NULL;
  //
  dmmCvtInfoInit(&cvt, openFileWAD);
  cvt.Quantization = 4;
  for (size_t f = 0; f < sizeof(knownQ)/sizeof(knownQ[0]); ++f) {
    if (strncmp(knownQ[f].name, n, 8) == 0) {
      cvt.Quantization = knownQ[f].q;
      break;
    }
  }
  cvt.brxmWriteLog = goobers;
  dlogf("DMM LOADING: '%s'", nm);
  outxm = dmm2xm(&cvt, nm, indmm, indmmsize, &outxmsize);
  dmmCvtInfoDeinit(&cvt);
  free(indmm);
  //
  if (outxm == NULL) {
    dlogf("DMM LOADING FAILED: '%s'", nm);
    if (musBuf != NULL) free(musBuf);
    musBuf = NULL;
    return 0;
  }
  dlogf("DMM LOADING OK: '%s' (%d --> %d)", nm, indmmsize, outxmsize);
  //
  if (musRW != NULL) SDL_FreeRW(musRW);
  musRW = NULL;
  //
  musBuf = outxm;
  musRW = SDL_RWFromConstMem(musBuf, outxmsize);
  //
  dlogf("XM LOADING: '%s'", nm);
  muschan = Mix_LoadMUS_RW(musRW);
  if (muschan == NULL) {
    dlogf("XM FAIL: '%s'", nm);
    if (musRW != NULL) SDL_FreeRW(musRW);
    musRW = NULL;
    if (musBuf != NULL) free(musBuf);
    //
    return 0;
  }
  //
  dlogf("XM OK: '%s'", nm);
  //
  return 1;
}


void F_loadmus (const char n[8]) {
  if (!musdisabled) {
    F_freemus();
    if (!loadXM(n)) loadDMM(n);
  }
}


void F_freemus (void) {
  if (!musdisabled) {
    if (muschan != NULL) {
      Mix_HaltMusic();
      Mix_FreeMusic(muschan);
    }
    muschan = NULL;
    if (musRW != NULL) SDL_FreeRW(musRW);
    musRW = NULL;
    if (musBuf != NULL) free(musBuf);
    musBuf = NULL;
  }
}


void S_updatemusic (void) {
  if (!musdisabled) {
    if (restartMusic) {
      restartMusic = 0;
      if (muschan != NULL) {
        Mix_PlayMusic(muschan, 1);
        restoreMusicVol();
      }
      //dlogf("RESTARTMUSIC!");
    }
    //
    if (muscount > 0) {
      //if (muscount < music_fade*1100/DELAY) Mix_FadeOutMusic(music_fade*1000);
      --muscount;
      if (muscount == 0) {
        if (music_random) F_randmus((void *)g_music); else F_nextmus((void *)g_music);
        F_freemus();
        F_loadmus((const void *)g_music);
        S_startmusic(music_time);
      }
    }
  }
}

#else

void S_init (void) {}
void S_done (void) {}
void free_chunks (void) {}
short S_play (const snd_t *s, short c, unsigned r, short v) { return -1; }
void S_stop (short c) {}
void S_volume (int v) {}
void S_wait (void) {}

void S_initmusic (void) {}
void S_donemusic (void) {}
void S_startmusic (int time) {}
void S_stopmusic (void) {}
void S_volumemusic (int v) {}
void F_loadmus (const char n[8]) {}
void F_freemus (void) {}
void S_updatemusic (void) {}

#endif



CONVAR_INT_CB(sound_volume, snd_vol, CMDCON_FLAGS_PERSISTENT, "sound volume [0..128]") {
  if (snd_vol < 0) snd_vol = 0;
  else if (snd_vol > 128) snd_vol = 128;
  if (sound_inited) S_volume(snd_vol);
}


CONVAR_INT_CB(music_volume, mus_vol, CMDCON_FLAGS_PERSISTENT, "music volume [0..128]") {
  if (mus_vol < 0) mus_vol = 0;
  else if (mus_vol > 128) mus_vol = 128;
  if (sound_inited) S_volumemusic(mus_vol);
}


CONVAR_BOOL_CB(music_random, music_random, CMDCON_FLAGS_PERSISTENT, "use random music") {
  if (sound_inited) {
    if (!musdisabled) {
      if (music_random) F_randmus((void *)g_music); else F_nextmus((void *)g_music);
      F_freemus();
      F_loadmus((const void *)g_music);
      S_startmusic(music_time);
    }
  }
}


CONCMD(music, "enable or disable music (on startup only)") {
  int bv;
  //
  CMDCON_HELP();
  if (sound_inited) {
    conlogf("%s: too late!\n", argv[0]);
    return;
  }
  //
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if ((bv = boolval(argc, argv)) >= 0) {
    musdisabledc = !bv;
  } else {
    conlogf("%s: invalid boolean: '%s'\n", argv[0], argv[1]);
  }
}


CONCMD(sound, "enable or disable sound (on startup only)") {
  int bv;
  //
  CMDCON_HELP();
  if (sound_inited) {
    conlogf("%s: too late!\n", argv[0]);
    return;
  }
  //
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if ((bv = boolval(argc, argv)) >= 0) {
    snddisabledc = !bv;
  } else {
    conlogf("%s: invalid boolean: '%s'\n", argv[0], argv[1]);
  }
}


CONVAR_INT_CB(music_time, music_time, CMDCON_FLAGS_PERSISTENT, "time before music change") {
  if (music_time < 1) music_time = 1;
}


CONVAR_INT_CB(music_fade, music_fade, CMDCON_FLAGS_PERSISTENT, "time for music to fade") {
  if (music_fade < 1) music_fade = 1;
}
