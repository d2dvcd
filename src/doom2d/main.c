/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include "SDL.h"

#include "adbglog.h"
#include "bind.h"
#include "common.h"
#include "cmdcon.h"
#include "cmdline.h"
#include "config.h"
#include "error.h"
#include "files.h"
#include "game.h"
#include "jimapi.h"
#include "keyb.h"
#include "map.h"
#include "mapio.h"
#include "menu.h"
#include "misc.h"
#include "netgame.h"
#include "player.h"
#include "saveload.h"
#include "sdldrv.h"
#include "sound.h"
#include "view.h"
#include "vga.h"
#include "vgaovl.h"
#include "widgets.h"
#include "widgets_jimapi.h"


////////////////////////////////////////////////////////////////////////////////
int gammaa = 0;


// progress points
void conlogdots (int cur, int total) {
  //fprintf(stderr, "conlogdots: cur=%d; all=%d\n", cur, all);
}


static const uint8_t gamcor[5][64] = {
 { 0, 1, 2, 3, 4, 4, 6, 7, 8, 9, 9,11,12,13,14,15,16,16,18,18,19,20,22,23,24,25,26,26,28,29,29,31,32,33,33,35,36,37,38,38,39,41,41,42,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63},
 { 0, 2, 3, 5, 6, 8, 9,10,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,28,29,30,31,31,32,33,34,35,36,37,38,39,40,41,42,42,43,44,45,46,47,48,48,49,50,51,52,53,54,54,55,56,57,58,58,59,60,61,62,63},
 { 0, 3, 6, 8,10,11,13,14,15,17,18,19,20,21,23,24,25,26,27,28,29,30,31,32,33,34,34,35,36,37,38,39,40,40,41,42,43,44,44,45,46,47,48,48,49,50,51,51,52,53,54,54,55,56,56,57,58,58,59,60,60,61,62,63},
 { 0, 5, 8,11,13,14,16,17,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,37,38,39,40,41,42,42,43,44,45,45,46,47,47,48,49,49,50,51,51,52,53,53,54,55,55,56,57,57,58,58,59,60,60,61,61,62,63},
 { 0, 7,11,13,15,17,19,20,22,23,25,26,27,28,29,30,31,32,33,34,35,36,37,38,38,39,40,41,41,42,43,44,44,45,46,46,47,48,48,49,50,50,51,52,52,53,53,54,54,55,56,56,57,57,58,58,59,59,60,60,61,61,62,63}
};


void setgamma (int g) {
  if (g > 4) g = 4;
  if (g < 0) g = 0;
  gammaa = g;
  for (int t = 0; t < 256; ++t) {
    std_pal[t*3+0] = gamcor[gammaa][main_pal[t*3+0]];
    std_pal[t*3+1] = gamcor[gammaa][main_pal[t*3+1]];
    std_pal[t*3+2] = gamcor[gammaa][main_pal[t*3+2]];
  }
  VP_setpal(std_pal);
}


CONVAR_INT_CB(gamma, gammaa, CMDCON_FLAGS_PERSISTENT, "set gamma [0..4]") {
  if (gammaa < 0) gammaa = 0;
  else if (gammaa > 4) gammaa = 4;
  //
  if (screen != NULL) setgamma(gammaa);
}


static char *condumpfname = NULL;

CONCMD(condump, "dump console output to file") {
  CMDCON_HELP();
  //
  if (argc > 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if (argc == 1) {
    if (condumpfname == NULL) conlogf("console dump is off\n");
    else conlogf("dumping console to '%s'\n", condumpfname);
    return;
  }
  //
  if (argv[1][0]) {
    if (boolval(argc, argv) == 0) {
      if (condumpfname != NULL) {
        fclose(cmdcon_dumpto[1]);
        cmdcon_dumpto[1] = NULL;
        free(condumpfname);
        condumpfname = NULL;
        conlogf("console dumping turned off\n");
      }
    } else {
      FILE *fl;
      //
      if (condumpfname != NULL) {
        fclose(cmdcon_dumpto[1]);
        cmdcon_dumpto[1] = NULL;
      }
      //
      if ((fl = fopen(argv[1], "a")) == NULL) {
        if (condumpfname != NULL) {
          if ((cmdcon_dumpto[1] = fopen(condumpfname, "a")) == NULL) {
            free(condumpfname);
            condumpfname = NULL;
          }
        }
        conlogf("failed to dump console to '%s'\n", argv[1]);
      } else {
        conlogf("dumping console to '%s'\n", argv[1]);
        cmdcon_dumpto[1] = fl;
        if (condumpfname != NULL) free(condumpfname);
        condumpfname = strdup(argv[1]);
      }
    }
  }
}


CONCMD(wad, "add wad file(s) (on startup only)") {
  CMDCON_HELP();
  if (!startup_in_process) {
    conlogf("%s: too late!\n", argv[0]);
    return;
  }
  //
  if (argc < 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  for (int f = 1; f < argc; ++f) F_addwad(argv[f], 0);
}


static void set_default_keys (void) {
  bind_reset_all();
  //
  pl[0].kb[KB_UP][0] = SDLK_KP8;
  pl[0].kb[KB_DOWN][0] = SDLK_KP2;
  pl[0].kb[KB_LEFT][0] = SDLK_KP4;
  pl[0].kb[KB_RIGHT][0] = SDLK_KP6;
  pl[0].kb[KB_FIRE][0] = SDLK_LCTRL;
  pl[0].kb[KB_JUMP][0] = SDLK_LALT;
  pl[0].kb[KB_WPNPREV][0] = SDLK_KP7;
  pl[0].kb[KB_WPNNEXT][0] = SDLK_KP9;
  pl[0].kb[KB_USE][0] = SDLK_LSHIFT;
  //
  pl[0].kb[KB_UP][1] = SDLK_UP;
  pl[0].kb[KB_DOWN][1] = SDLK_DOWN;
  pl[0].kb[KB_LEFT][1] = SDLK_LEFT;
  pl[0].kb[KB_RIGHT][1] = SDLK_RIGHT;
  //pl[0].kb[KB_FIRE][1] = -1;
  //pl[0].kb[KB_JUMP][1] = -1;
  pl[0].kb[KB_WPNPREV][1] = SDLK_DELETE;
  pl[0].kb[KB_WPNNEXT][1] = SDLK_PAGEDOWN;
  //pl[0].kb[KB_USE][1] = -1;
  //
  pl[1].kb[KB_UP][0] = SDLK_e;
  pl[1].kb[KB_DOWN][0] = SDLK_d;
  pl[1].kb[KB_LEFT][0] = SDLK_s;
  pl[1].kb[KB_RIGHT][0] = SDLK_f;
  pl[1].kb[KB_FIRE][0] = SDLK_a;
  pl[1].kb[KB_JUMP][0] = SDLK_q;
  pl[1].kb[KB_WPNPREV][0] = SDLK_1;
  pl[1].kb[KB_WPNNEXT][0] = SDLK_2;
  pl[1].kb[KB_USE][0] = SDLK_e;
}


////////////////////////////////////////////////////////////////////////////////
static void widgetsUpdateActiveFlag (void) {
  zxwidsys_active = 0;
  for (ZXWindow *w = zxwinfirst; w != NULL; w = w->next) {
    if (zxwinIsVisible(w)) {
      zxwidsys_active = 1;
      break;
    }
  }
}


static void widgetsPaint (void) {
  for (ZXWindow *w = zxwinfirst; w != NULL; w = w->next) {
    if (zxwinIsVisible(w)) {
      w->paintCB(w, &w->sfc);
    }
  }
}


// btn==0: motion
static int widgetsProcessMouse (int x, int y, int btn, int buttons) {
  for (ZXWindow *w = zxwinlast; w != NULL; w = w->prev) {
    if (zxwinIsVisible(w) && w->mouseCB != NULL) {
      if (w->mouseCB(w, x-w->x, y-w->y, btn, buttons)) {
        zxwinBringToTop(zxwinactive);
        return 1; // eaten
      }
    }
  }
  //
  return 0;
}


static int widgetsProcessKeys (SDL_KeyboardEvent *key) {
  if (!convisible) {
    ZXWindow *preva = zxwinactive;
    //
    if (preva != NULL && preva->keyCB != NULL) {
      if (preva->keyCB(zxwinactive, key)) return 1;
    }
    //
    for (ZXWindow *w = zxwinlast; w != NULL; w = w->prev) {
      if (w != preva && zxwinIsVisible(w) && w->keyCB != NULL) {
        if (w->keyCB(w, key)) return 1; // eaten
      }
    }
  }
    //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static void sdlMouseCB (int x, int y, int xrel, int yrel, int buttons) {
  if (widgetsProcessMouse(x, y, 0, buttons) || zxwidsys_active) return;
}


static void sdlMouseButtonCB (int x, int y, int btn, int buttons) {
  if (widgetsProcessMouse(x, y, btn, buttons) || zxwidsys_active) return;
}


static void sdlKeyCB (SDL_KeyboardEvent *key) {
  if (key->type == SDL_KEYDOWN && key->keysym.sym == SDLK_RETURN && (key->keysym.mod&(KMOD_LALT|KMOD_RALT))) {
    V_toggle();
    return;
  }
  if (widgetsProcessKeys(key)) return;
  if (k_key_proc != NULL) k_key_proc(key, (key->type == SDL_KEYDOWN));
}


////////////////////////////////////////////////////////////////////////////////
static ccBool dbg_dump_frame_time = CC_FALSE;
CONVAR_BOOL(dbg_dump_frame_time, dbg_dump_frame_time, 0, "dump frame time")


CONCMD(echo, "echo text") {
  CMDCON_HELP();
  for (int f = 1; f < argc; ++f) conlogf(argv[f]);
}


CONCMD(exec, "execute console script") {
  FILE *fl;
  char *fname;
  //
  CMDCON_HELP();
  if (argc != 2) {
    conlogf("%s: invalid number of arguments!\n", argv[0]);
    return;
  }
  //
  fname = Z_sprintf("%s/%s.cfg", get_bin_dir(), argv[1]);
  fl = fopen(fname, "r");
  if (fl == NULL) {
    free(fname);
    fname = Z_sprintf("%s/scripts/%s.cfg", get_bin_dir(), argv[1]);
    fl = fopen(fname, "r");
  }
  //
  if (fl != NULL) {
    char s[1024];
    //
    while (fgets(s, sizeof(s), fl) != NULL) cmdcon_exec(s);
    fclose(fl);
  } else {
    conlogf("script not found: '%s'\n", fname);
  }
  free(fname);
}


static void draw_all (void) {
  G_draw();
  widgetsPaint();
  V_blitscr();
}


static VOverlay *vo_net;
static VOverlay *vo_turtle;
static int vo_turtle_alpha = 0;

static void preblit (void) {
  if (vo_turtle_alpha > 0) {
    blitVO(vo_turtle, SCRW-32, SCRH-32, vo_turtle_alpha);
    if (g_frame_advanced) {
      //if ((vo_turtle_alpha -= 10) < 0) vo_turtle_alpha = 0;
      vo_turtle_alpha -= 10;
    }
  }
  //
  if (n_host != NULL) {
    blitVO(vo_net, SCRW-32-32-2, SCRH-32, 255);
  }
  //
  if (r_mouse_cursor || zxwidsys_active || g_edit_mode) drawMouseCursor(msLastX, msLastY, AID_DEFAULT);
}


static uint32_t frame_count = 0;
static uint64_t ticks_used = 0;
static uint64_t ticks_free = 0;


static void sdl_process_events (void) {
  switch (processEvents(0)) {
    case SDLDRV_ER_QUIT:
      if (frame_count > 0) {
        fprintf(stderr, "%u frames, frame averages: %d ticks used, %d ticks free\n", frame_count, (int)(ticks_used/frame_count), (int)(ticks_free/frame_count));
      }
      ERR_quit(1);
      break;
    case SDLDRV_ER_USER_EVENT: break;
    case SDLDRV_ER_OK: break;
  }
}


static void dump_frame_time (void) {
  if (dbg_dump_frame_time && g_st == GS_GAME && !g_trans) {
    Uint32 ticks = g_frame_ticks_start+DELAY, t = SDL_GetTicks();
    //
    if (t <= ticks) {
      fprintf(stderr, "ticks used: %u; ticks left: %u\n", DELAY-(ticks-t), ticks-t);
    } else {
      fprintf(stderr, "ticks used: %u; ticks overused: %u\n", DELAY+(t-ticks), t-ticks);
    }
  }
}


//#define dlogfX       dlogf
#define dlogfX(...)  ((void)0)


#include "main_single.c"
#include "main_server.c"
#include "main_client.c"


int main (int argc, char *argv[]) {
  int nettest = 0;
  //
#ifdef _WIN32
  cmdLineParse();
  argc = k8argc;
  argv = k8argv;
#endif
  //
  dbgLogSetFileOut(0);
  for (int f = 1; f < argc; ++f) {
    if (strcmp(argv[f], "-goobers") == 0) {
      goobers = 1;
      dbgLogSetFileOut(1);
      dbgSetScreenOut(1);
    }
    if (strcmp(argv[f], "-nettest") == 0) {
      nettest = 1;
    }
  }
  //
  dlogf("===============================\nstarting DooM2D:VCD");
  dlogf("compile date: %s %s", __DATE__, __TIME__);
  //
  if (SDL_Init(SDL_INIT_VIDEO) < 0) ERR_failinit("Unable to init SDL: %s\n", SDL_GetError());
  atexit(SDL_Quit);
  //
  if (nettest) {
    SDL_WM_SetCaption("NETTEST: Doom 2D v" DOOM2D_VERSION, "NETTEST: Doom 2D");
  } else {
    SDL_WM_SetCaption("Doom 2D v" DOOM2D_VERSION, "Doom 2D");
  }
  SDL_EnableKeyRepeat(200, 25);
  SDL_EnableUNICODE(1);
  //
  set_default_keys();
  pl[0].id = -1;
  pl[1].id = -2;
  //
  if (setjmp(flJmpBuf) != 0) {
    ERR_failinit("file reading error");
    exit(1);
  }
  //
  myrandomize();
  F_startup();
  //
  F_addwad("doom2d", 1);
  for (int f = 0; f < 99; ++f) {
    char wadname[32];
    //
    sprintf(wadname, "patch%02d.wad", f);
    F_addwad(wadname, 0);
  }
  //
  CFG_load();
  CFG_args(argc, argv);
  //
  startup_in_process = 0;
  //
  F_initwads();
  M_startup();
  F_allocres();
  F_loadres(F_getresid("PLAYPAL"), main_pal, 0, 768);
  for (int i = 0; i < 256; ++i) bright[i] = ((int)main_pal[i*3+0]+main_pal[i*3+1]+main_pal[i*3+2])*8/(63*3);
  F_loadres(F_getresid("MIXMAP"), mixmap, 0, 0x10000);
  F_loadres(F_getresid("COLORMAP"), clrmap, 0, 256*12);
  //
  conlogf("V_init: initializing video");
  V_init();
  //
  zxwinInit();
  jimInit();
  jimEvalFile("init/init.tcl", 1);
  //
  G_init();
  K_init();
  conlogf("S_init: initializing sound");
  S_init();
  setgamma(gammaa);
  V_blitscr();
  //
  GM_init();
  F_loadmus("MENU");
  S_startmusic(0);
  //
  keyCB = sdlKeyCB;
  mouseCB = sdlMouseCB;
  mouseButtonCB = sdlMouseButtonCB;
  //
  jimEvalFile("init/widgets/init.tcl", 1);
  jimEvalFile("autoexec.tcl", 1);
  //
  //Uint32 ticks = SDL_GetTicks();
  g_frame_ticks_start = 0;
  //
  vo_turtle = createVOFromStockIcon(VGA_ICON_TURTLE);
  vo_net = createVOFromStockIcon(VGA_ICON_RAM); //FIXME
  vga_preblit_cb = preblit;
  //
  for (;;) {
    int lastWinGC = 0;
    //
    if (--lastWinGC < 0) {
      zxwinGC();
      lastWinGC = 18*3;
    }
    //
    if (n_host == NULL) normal_game_loop();
    else if (n_network_meserver) network_game_loop_server();
    else network_game_loop_client();
  }
  //
  return 0;
}


#ifdef WIN32
int CALLBACK WinMain (HINSTANCE hInstance, HINSTANCE unused__, LPSTR lpszCmdLine, int nCmdShow) {
  char *shit[] = { (char *)"shit", NULL };
  return SDL_main(1, shit);
}
#endif
