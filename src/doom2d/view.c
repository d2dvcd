/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include <string.h>
#include <malloc.h>

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "dots.h"
#include "error.h"
#include "files.h"
#include "fx.h"
#include "game.h"
#include "items.h"
#include "jimapi.h"
#include "map.h"
#include "mapio.h"
#include "misc.h"
#include "monster.h"
#include "player.h"
#include "sdldrv.h"
#include "smoke.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"


////////////////////////////////////////////////////////////////////////////////
#define ANIT  (5)

/*
#define w_view_wdt 200
#define w_view_hgt 98

#define MAXX (FLDW*CELW-w_view_wdt/2)
#define MAXY (FLDH*CELH-w_view_hgt/2)
*/


////////////////////////////////////////////////////////////////////////////////
int w_view_wdt;
int w_view_hgt;

vgaimg ltn[2][2];

ccBool r_drawsky = CC_TRUE;
static vgaimg horiz;
int w_viewofs_vert, w_viewofs_x, w_viewofs_y;
int sky_type = 1;
vgaimg walpix[256];
vgaimg *walp[256];
uint32_t walf[256];
int walh[256];
uint8_t walswp[256];
uint8_t walani[256];
int anih[ANIT][5];
vgaimg anispr[ANIT][5];
uint8_t anic[ANIT];

int FLDW = 0, FLDH = 0; // 100x100
uint8_t *fldb = NULL;
uint8_t *fldf = NULL;
uint8_t *fldm = NULL;

ccBool cheatNoBack = CC_FALSE;
ccBool cheatNoFront = CC_FALSE;


////////////////////////////////////////////////////////////////////////////////
CONVAR_BOOL(r_drawsky, r_drawsky, CMDCON_FLAGS_PERSISTENT, "draw sky?")


////////////////////////////////////////////////////////////////////////////////
GCC_DESTRUCTOR_USER {
  if (fldb) free(fldb);
  if (fldf) free(fldf);
  if (fldm) free(fldm);
}


void W_set_field_size (int w, int h) {
  // arbitrary limits
  if (w < 1) w = 1; else if (w > 32767) w = 32767;
  if (h < 1) h = 1; else if (h > 32767) h = 32767;
  if (w != FLDW || h != FLDH) {
    if ((fldb = realloc(fldb, w*h)) == NULL) ERR_fatal("W_set_field_size: out of memory!");
    if ((fldf = realloc(fldf, w*h)) == NULL) ERR_fatal("W_set_field_size: out of memory!");
    if ((fldm = realloc(fldm, w*h)) == NULL) ERR_fatal("W_set_field_size: out of memory!");
    FLDW = w;
    FLDH = h;
    BM_set_size();
  }
}


////////////////////////////////////////////////////////////////////////////////
static const char *anm[ANIT-1][5] = {
  {"WALL22_1", "WALL23_1", "WALL23_2", NULL, NULL},
  {"WALL58_1", "WALL58_2", "WALL58_3", NULL, NULL},
  {"W73A_1", "W73A_2", NULL, NULL, NULL},
  {"RP2_1", "RP2_2", "RP2_3", "RP2_4", NULL}
};


//???
static void getname (int n, char *s) {
  switch (walh[n]) {
    case -1:
      memset(s, 0, 8);
      break;
    case -2:
      memcpy(s, "_WATER_", 8);
      //s[7] = (char)((((uintptr_t)walp[n])&0xff)-1+'0');
      s[7] = walpix[n].wanim-1+'0';
      //fprintf(stderr, "n=%d; wanim=%d\n", n, walpix[n].wanim);
      break;
    default:
      F_getresname(s, walh[n]&0x7FFF);
      break;
  }
}


static short getani (const char *n) {
  for (int f = 0; f < ANIT-1; ++f) if (strncasecmp(n, anm[f][0], 8) == 0) return f+1;
  return 0;
}


void W_adjust (void) {
  int MAXX = (FLDW*CELW-w_view_wdt/2);
  int MAXY = (FLDH*CELH-w_view_hgt/2);
  //
  if (w_viewofs_x < w_view_wdt/2) w_viewofs_x = w_view_wdt/2;
  if (w_viewofs_y < w_view_hgt/2) w_viewofs_y = w_view_hgt/2;
  if (w_viewofs_x > MAXX) w_viewofs_x = MAXX;
  if (w_viewofs_y > MAXY) w_viewofs_y = MAXY;
}


ccBool r_lighting = CC_FALSE;
static int r_lights_tnum = 0;
static int r_lights_trad = 250;
static ccBool r_flashlight = CC_FALSE;

CONVAR_BOOL(r_lighting, r_lighting, 0, "test dynamic lighting")
CONVAR_INT(r_lights_tnum, r_lights_tnum, 0, "# of additional lights for testing")
CONVAR_INT(r_lights_trad, r_lights_trad, 0, "radius of additional lights")
CONVAR_BOOL(r_flashlight, r_flashlight, 0, "test marine flashlight")


////////////////////////////////////////////////////////////////////////////////
#include "zlight.c"


////////////////////////////////////////////////////////////////////////////////
// bg!=0: cheatNoBack
// bg==0: cheatNoFront
void Z_drawfld (const uint8_t *xfld, int bg) {
  const uint8_t *p = xfld, *pb = &FIELD_TBG(0, 0), *pf = &FIELD_TFG(0, 0), *pm = &FIELD_MAP(0, 0);
  int semi = (bg ? cheatNoBack : cheatNoFront);
  //
  for (int y = 0; y < FLDH; ++y) {
    for (int x = 0; x < FLDW; ++x) {
      int sx = Z_MAP2SCR_X(x*CELW);
      int sy = Z_MAP2SCR_Y(y*CELH);
      //
      if (sx >= -CELW*256 && sx < w_view_wdt && sy >= -CELH*256 && sy < SCRH) {
        if (*p > 0 && *p < 255) {
          // non-empty tile
          vgaimg *pic = walp[*p];
          //
          // check the following in postprocessor
          //if (pic == NULL) continue; // the thing that should not be; no, really!
          //
          if (pic->wanim <= 0) {
            // normal texture, not 'liquid'
            // check the following in postprocessor
            //if (pic->w < 1) continue; // the thing that should not be; no, really!
            //
            if (r_lighting) {
              // 'FitL' mode
              if (!bg || pm[0] == TILE_WALL /*|| (pf[0] && pm[0]) || pf[0]*/) {
                // this is impassable or foreground wall, draw it fulllit
                V_draw_tile(sx, sy, pic, semi);
              } else {
                // this is passable wall, light it later
                V_draw_tile_lm(sx, sy, pic, semi);
              }
            } else {
              // vanilla mode
              V_draw_tile(sx, sy, pic, semi);
            }
          }
        } else {
          // empty tile
          if (r_lighting && bg && *p == 255) {
            // unoccupied background tile
            // fill background, so light will have something to lit
            V_draw_tile_lm(sx, sy, walp[1], semi);
          }
        }
      }
      ++p;
      ++pb;
      ++pf;
      ++pm;
    }
  }
}


void Z_drawliquids (void) {
  const uint8_t *p = &FIELD_TFG(0, 0);
  //
  for (int y = 0; y < FLDH; ++y) {
    for (int x = 0; x < FLDW; ++x) {
      int sx = Z_MAP2SCR_X(x*CELW);
      int sy = Z_MAP2SCR_Y(y*CELH);
      //
      if (sx >= -CELW && sx < w_view_wdt && sy >= -CELH && sy < SCRH) {
        if (*p > 0 && *p < 255) {
          // non-empty tile
          vgaimg *pic = walp[*p];
          //
          if (pic == NULL) continue; // the thing that should not be; no, really!
          //
          if (pic->wanim > 0 && pic->wanim <= 3 && sy < w_viewofs_vert+w_view_hgt) {
            // one of the 'water' textures
            // 1: water (blue)
            // 2: acid (green)
            // 3: lava (red)
            uint32_t src = 0;
            static const Uint32 aa[8] = {
              0x5f<<24, 0x4f<<24, 0x3f<<24, 0x2f<<24, 0x1f<<24, 0x2f<<24, 0x3f<<24, 0x4f<<24,
            };
            //
            switch (pic->wanim) {
              case 1: src = 0x5f0000ff; break; // water
              case 2: src = 0x5f00ff00; break; // acid
              case 3: src = 0x5fff0000; break; // lava
            }
            //
            int ypix = (sy >= vidcy1 ? sy : vidcy1), ye = (sy+CELH < vidcy2 ? sy+CELH : vidcy2);
            int xpix = (sx >= vidcx1 ? sx : vidcx1), xe = (sx+CELW < vidcx2 ? sx+CELW : vidcx2);
            //
            if (r_lighting) {
              src = (src&0xffffff)|aa[(g_time/2)&7];
            } else {
              src = (src&0xffffff)|aa[(g_time/2)&7];
              //src = (src&0xffffff)|(0x5f<<24);
            }
            //
            for (; ypix < ye; ++ypix) {
              Uint32 *d = (Uint32 *)(((Uint8 *)frameSfc->pixels)+(ypix*frameSfc->pitch)+xpix*4);
              /*
              const uint8_t *lm = scrlightmap+SCRW*ypix+xpix;
              const uint8_t *vs = vscrbuf+SCRW*ypix+xpix;
              const uint8_t *lb = lightbuf+(SCRW*LB_ITEM_SZ)*ypix+(xpix*LB_ITEM_SZ);
              */
              //
              for (int h = xpix; h < xe; ++h, ++d/*, ++lm, ++vs, lb += LB_ITEM_SZ*/) {
                /*
                if (*lm == LMAP_NORMAL && *lb == 0) {
                  // *d = palette[*vs];
                  *d = 0;
                  // pixel_blend32(d, palette[*vs]|(64<<24));
                }
                */
                pixel_blend32(d, 0x8f000000); // darken
                pixel_blend32(d, src);
              }
            }
          }
        }
      }
      ++p;
    }
  }
}


static void Z_draw_sky (void) {
  if (!r_lighting) {
    if (r_drawsky && horiz.w > 0) {
      //V_pic(127-(uint16_t)(w_viewofs_x-w_view_wdt/2)*56U/(uint16_t)(MAXX-w_view_wdt/2), w_viewofs_vert+123-(uint16_t)(w_viewofs_y-w_view_hgt/2)*28U/(uint16_t)(MAXY-w_view_hgt/2), horiz);
      vgaimg *img = &horiz;
      int x = 0, d = 0;
      //
      do {
         int y = w_viewofs_vert;
         //
         d &= ~2;
         do {
           V_rotspr(x, y, img, d);
           y += img->h;
           d ^= 2;
         } while (y < w_view_hgt+w_viewofs_vert);
         x += img->w;
         d ^= 1;
      } while (x < w_view_wdt);
      //
      if (sky_type == 2 && lt_time < 0) {
        if (!lt_side) V_spr(0, w_viewofs_vert+lt_ypos, &ltn[lt_type][lt_time < -5 ? 0 : 1]);
        else V_spr2(w_view_wdt-1, w_viewofs_vert+lt_ypos, &ltn[lt_type][lt_time < -5 ? 0 : 1]);
      }
    } else {
      V_clr(0, w_view_wdt, w_viewofs_vert+1, w_view_hgt, 0x97);
    }
  } else {
    V_clr(0, w_view_wdt, w_viewofs_vert, w_view_hgt, 0);
  }
}


////////////////////////////////////////////////////////////////////////////////
#define DECL_CONVAR_INT(_xname,_xdef,_xfags,_xhelp)  \
static int _xname = (_xdef); \
CONVAR_INT(_xname, _xname, (_xfags), (_xhelp))

DECL_CONVAR_INT(r_flashlight_r, 128, 0, "flashlight red")
DECL_CONVAR_INT(r_flashlight_g, 128, 0, "flashlight green")
DECL_CONVAR_INT(r_flashlight_b, 0, 0, "flashlight blue")
DECL_CONVAR_INT(r_flashlight_a, 255, 0, "flashlight alpha")



////////////////////////////////////////////////////////////////////////////////
static void Z_add_lights (player_t *p) {
  if (r_lighting) {
    Uint32 stt = SDL_GetTicks();
    //int ang = g_time%360*2;
    int ang = g_time/2%64;
    //
    if (g_map == 1) {
      int lang = sin(ang*2*(M_PI/32.0f))*20;
      //
      //Z_add_light_sector(197, 330, 192, 35+lang, 145+lang, 255, 0, 0, 120);
      Z_add_light_sector(197, 330, 192, 35+lang, 145+lang, 255, 0, 0, 128);
      //
      Z_add_light_sector(388, 410, 192, 35+lang, 145+lang, 255, 0, 0, 180);
      //
      /*
      int lpx = sin(ang*(M_PI/32.0f))*40;
      int lpy = sin(ang*(M_PI/32.0f))*10;
      Z_add_light(200+lpx, 300+lpy, 128, 255, 127, 0, 200);
      */
    }
    //
    if (r_lights_tnum > 0 && r_lights_trad > 2) {
      if (r_lights_trad > 250) r_lights_trad = 250;
      //
      Z_add_light(p->o.x, p->o.y, 64, 0, 128, 0, 255);
      //Z_add_light(p->o.x, p->o.y, 64, 0, 0, 0, 128);
      //Z_add_light(197, 330, 192, 255, 0, 0, 92);
      //Z_add_light_sector(p->o.x, p->o.y, 92, ang, ang+42, 0, 255, 0, 64);
      //
      //Z_add_light_sector(197, 330, 192, 90-35+ang, 90+35+ang, 255, 0, 0, 92);
      //Z_add_light_sector(197, 360, 192, 90-35+ang, 90+35+ang, 255, 0, 0, 255);
      //
      //Z_add_light_sector(197, 360, 192, 90-35+ang, 90+35+ang, 255, 0, 0, 255);
      //Z_add_light_sector(197, 360, 192, 0+ang, 35+ang, 255, 0, 0, 255);
      //Z_add_light_sector(197, 360, 192, 0, ang%180, 255, 0, 0, 255);
      //Z_add_light_sector(197, 360, 192, 0/*90-35+ang*/, 359/*90+35+ang*/, 255, 0, 0, 255);
      /*
      for (int f = 0; f < 30; ++f) {
        Z_add_light(f*64, f*64, 250, 0, 0, 255-f*10, 98);
      }
      */
      for (int f = 1; f < r_lights_tnum; ++f) {
        Z_add_light(p->o.x, p->o.y-8, r_lights_trad, 0, 0, 60, 40);
      }
      //
      stt = SDL_GetTicks()-stt;
      //fprintf(stderr, "draw: %u msecs\n", stt);
    }
    //
    if (r_flashlight) {
      for (int plnum = 0; plnum < g_plrcount; ++plnum) {
        player_t *p = &pl[plnum];
        int x, y, ss, se;
        //
        if (n_host != NULL && !(g_st == GS_GAME && !g_trans && gnet_state == GNETS_PLAYING)) {
          continue;
        }
        //
        if (p->d) {
          // to the right
          x = p->o.x-2+(p->st == PLST_GO ? 5 : 0);
          y = p->o.y-24-(p->st == PLST_GO ? 1 : 0);
          ss = 340;
          se = 375;
          if (p->f&PLF_UP) { ss -= 45; se -= 45; }
          else if (p->f&PLF_DOWN) { ss += 45; se += 45; }
          //
          x -= 10;
          if (p->f&PLF_UP) { x += 2; y += 4; }
          if (p->f&PLF_DOWN) { x += 0; y -= 6; }
        } else {
          // to the left
          x = p->o.x-1-(p->st == PLST_GO ? 4 : 0);
          y = p->o.y-24-(p->st == PLST_GO ? 1 : 0);
          ss = 160;
          se = 195;
          if (p->f&PLF_UP) { ss += 45; se += 45; }
          else if (p->f&PLF_DOWN) { ss -= 45; se -= 45; }
          x += 10;
          if (p->f&PLF_UP) { x += 1; y += 4; }
          if (p->f&PLF_DOWN) { x += 0; y -= 7; }
        }
        y += 3;
        if (x > p->o.x) {
          while (fldhit1(x, y) && x >= p->o.x-6) --x;
        } else {
          while (fldhit1(x, y) && x <= p->o.x+6) ++x;
        }
        Z_add_light_sector_mr(x, y, 220, ss, se, r_flashlight_r, r_flashlight_g, r_flashlight_b, r_flashlight_a, 8);
      }
    }
  }
}


static ccBool dbg_dump_light_time = CC_FALSE;
CONVAR_BOOL(dbg_dump_light_time, dbg_dump_light_time, 0, "dump lightning time")


////////////////////////////////////////////////////////////////////////////////
void Z_light_start (void) {
  lb_reset(0);
  Z_reset_lights();
}


void W_draw (player_t *p) {
  //memset(vscrbuf, 0, SCRW*SCRH);
  //memset(scrlightmap, (r_lighting ? /*LMAP_BACK*/LMAP_NORMAL : LMAP_FULLLIT), SCRW*SCRH);
  //
  W_adjust();
  V_setrect(0, w_view_wdt, w_viewofs_vert, w_view_hgt);
  //
  if (r_lighting) Z_light_start();
  //
  Z_draw_sky();
  Z_drawfld((const uint8_t *)fldb, 1);
  DOT_draw();
  SW_draw();
  IT_draw();
  MN_draw();
  WP_draw();
  //
  if (n_host == NULL) {
    for (int f = 0; f < g_plrcount; ++f) PL_draw(&pl[f]);
  } else {
    if (g_st == GS_GAME && !g_trans && gnet_state == GNETS_PLAYING) {
      for (int f = 0; f < g_plrcount; ++f) PL_draw(&pl[f]);
    } else {
      PL_draw(&pl[n_local_player_num]);
    }
  }
  //
  SMK_draw();
  FX_draw();
  Z_drawfld((const uint8_t *)fldf, 0);
  //
  if (r_lighting) {
    Uint32 st = SDL_GetTicks();
    //
    Z_add_lights(p);
    Z_render_lights();
    lb_draw(0, w_viewofs_vert, w_view_wdt, w_view_hgt);
    if (dbg_dump_light_time) {
      st = SDL_GetTicks()-st;
      fprintf(stderr, "light time: %u ms\n", st);
    }
  } else {
    lbx_draw(0, w_viewofs_vert, w_view_wdt, w_view_hgt);
  }
  Z_drawliquids();
  //
  if (sky_type == 2 && (lt_time == -4 || lt_time == -2)) V_remap_rect(0, w_view_wdt, w_viewofs_vert+1, w_view_hgt, clrmap+256*11);
  //
  V_setrect(0, SCRW, 0, SCRH);
}


////////////////////////////////////////////////////////////////////////////////
void W_init (void) {
  Z_reset_level_lights();
  //
  for (int i = 1; i < ANIT; ++i) {
    int j;
    //
    for (j = 0; anm[i-1][j]; ++j) {
      anih[i][j] = F_getresid(anm[i-1][j]);
      V_loadvga(anm[i-1][j], &anispr[i][j]);
    }
    for (; j < 5; ++j) anih[i][j] = -1;
  }
  //
  memset(anic, 0, sizeof(anic));
  cheatNoBack = cheatNoFront = CC_FALSE;
  //
  DOT_init();
  SMK_init();
  FX_init();
  WP_init();
  IT_init();
  SW_init();
  PL_init();
  MN_init();
  //M_unlock(horiz);
  //horiz = M_lock(F_getresid("RSKY1"));
  V_loadvga("RSKY1", &horiz);
  //
  free_chunks();
  //
  m_notarget = CC_FALSE;
}


static void fix_anim (void) {
  walp[0] = NULL;
  //
  for (int f = 1; f < 256; ++f) {
    int a = walani[f];
    //
    walp[f] = &walpix[f];
    if (a != 0) walp[f] = &anispr[a][anic[a]];
  }
}


void W_act (void) {
  if (g_time%3 != 0) return;
  //
  for (int f = 1; f < 256; ++f) {
    int a = walani[f];
    //
    if (a != 0) {
      if (anih[a][++anic[a]] == -1) anic[a] = 0;
      //walp[f] = M_lock(anih[a][anic[a]]);
      //fprintf(stderr, "f=%d; a=%d; anic=%u; anih=%d\n", f, a, anic[a], anih[a][anic[a]]);
      walp[f] = &anispr[a][anic[a]];
      //walp[f]->wanim = 666;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static wall_t w_wallnames[256];

static int W_mapload_wallnames (MemBuffer *mbuf, const map_block_t *blk) {
  if (mbuf == NULL) return 0; // no post-processing
  //
  int sz = blk->sz;
  int f, c;
  wall_t w;
  //
  for (f = 0; f < 256; ++f) {
    walh[f] = -1;
    walswp[f] = f;
    walani[f] = 0;
  }
  memset(w_wallnames, 0, sizeof(w_wallnames));
  //
  for (f = 1; f < 256 && sz > 0; ++f, sz -= sizeof(w)) {
    if (membuf_read_full(mbuf, &w, sizeof(w)) < 0) return -1;
    //
    w_wallnames[f] = w;
    if (strncasecmp(w.n, "_WATER_", 7) == 0) {
      // animated water/laval/acid texture
      V_freevga(&walpix[f]);
      walpix[f].wanim = (w.n[7]-'0'+1);
      walh[f] = -2;
      continue;
    }
    walh[f] = F_getresid(w.n);
    V_loadvga(w.n, &walpix[f]);
    if (w.n[0] == 'S' && w.n[1] == 'W' && w.n[4] == '_') walswp[f] = 0;
    walf[f] = (w.t ? 1 : 0);
    if (w.t) walh[f] |= 0x8000;
    if (strncasecmp(w.n, "VTRAP01", 8) == 0) walf[f] |= 2;
    walani[f] = getani(w.n);
  }
  //
  for (c = f, f = 1; f < 256; ++f) {
    if (walswp[f] == 0) {
      int k, g;
      //
      if (c >= 256) break;
      F_getresname(w.n, walh[f]&0x7FFF);
      w.n[5] ^= 1;
      g = F_getresid(w.n)|(walh[f]&0x8000);
      for (k = 1; k < 256; ++k) if (walh[k] == g) break;
      if (k >= 256) {
        walh[k = c++] = g;
        V_loadvga(w.n, &walpix[k]);// = M_lock(g);
        walf[k] = (g&0x8000)?1:0;
      }
      walswp[f] = k;
      walswp[k] = f;
    }
  }
  fix_anim();
  //
  return 0;
}


static int W_mapload_sky (MemBuffer *mbuf, const map_block_t *blk) {
  char n[9];
  //
  if (mbuf == NULL) return 0; // no post-processing
  //
  memset(n, 0, sizeof(n));
  sky_type = membuf_read_ui16(mbuf);
  if (sky_type > 2) sky_type = 0;
  strcpy(n, "RSKY1");
  n[4] = sky_type+'0';
  V_loadvga(n, &horiz);
  //
  return 0;
}


static void rle_unpack (const void *buf, int len, void *obuf) {
  const uint8_t *p;
  uint8_t *o;
  int l, n;
  //
  for (p = (const uint8_t *)buf, o = (uint8_t *)obuf, l = len; l; ++p, --l) {
    if (*p == 255) {
      n = *((const uint16_t *)(++p));
      memset(o, *(p += 2), n);
      o += n;
      l -= 3;
    } else {
      *(o++) = *p;
    }
  }
}


// fill tiles occupied by big textures
static int postprocess_field (uint8_t *fld) {
  uint8_t *fp = fld;
  char reported[256];
  //
  memset(reported, 0, sizeof(reported));
  //
  for (int y = 0; y < FLDH; ++y) {
    for (int x = 0; x < FLDW; ++x, ++fp) {
      //
      if (*fp == 255) {
        conlogf("too many different tiles on level!\n");
        return -1;
      }
      //
      if (*fp == 0) {
        // let's fill this with 255, so light will have something to lit
        // for foreground tiles -- mark as 'unoccupied'
        *fp = 255;
      } else {
        const vgaimg *pic = walp[*fp];
        //
        if (pic == NULL || pic->w < 1) {
          if (!reported[*fp]) {
            char n[9];
            //
            reported[*fp] = 1;
            //
            n[8] = 0;
            memcpy(n, w_wallnames[*fp].n, 8);
            if (strncasecmp(n, "_water_", 7) != 0) {
              conlogf("invalid texture on level: %u! [%s]\n", *fp, n);
              //return -1;
              *fp = 0;
            } else {
              *fp = 255;
            }
          }
        }
      }
    }
  }
  //
  fp = fld;
  for (int y = 0; y < FLDH; ++y) {
    for (int x = 0; x < FLDW; ++x, ++fp) {
      if (*fp > 0 && *fp < 255) {
        // this is textured tile, let's try to mark adjacent tiles as used if necessary
        const vgaimg *pic = walp[*fp];
        //
        if (pic != NULL && pic->w > 0 && pic->wanim <= 0) {
          int sx = x*CELW-pic->sx, ex = sx+pic->w;
          int sy = y*CELH-pic->sy, ey = sy+pic->h;
          //
          for (int yy = sy; yy < ey; yy += CELH) {
            for (int xx = sx; xx < ex; xx += CELW) {
              int fx = xx/CELW, fy = yy/CELH;
              //
              if (fx >= 0 && fx < FLDW && fy >= 0 && fy < FLDH) {
                uint8_t *ff = fld+fy*FLDW+fx;
                //
                if (*ff == 255) *ff = 0; // this tile is 'occupied'
              }
            }
          }
        }
      }
    }
  }
  //
  return 0;
}


static int W_mapload_field (MemBuffer *mbuf, const map_block_t *blk, uint8_t *fld) {
  void *buf;
  //
  switch (blk->st) {
    case 0:
      if (membuf_read_full(mbuf, fld, FLDW*FLDH) < 0) return -1;
      break;
    case 1:
      if ((buf = malloc(blk->sz)) == NULL) {
        conlogf("map loader: out of memory!\n");
        return -1;
      }
      if (membuf_read_full(mbuf, buf, blk->sz) < 0) { free(buf); return -1; }
      rle_unpack(buf, blk->sz, fld);
      free(buf);
      break;
    default: return 0;
  }
  //
  return 0;
}


static int W_mapload_back (MemBuffer *mbuf, const map_block_t *blk) {
  if (mbuf == NULL) return postprocess_field(fldb);
  //
  return W_mapload_field(mbuf, blk, fldb);
}

static int W_mapload_front (MemBuffer *mbuf, const map_block_t *blk) {
  if (mbuf == NULL) return postprocess_field(fldf);
  //
  return W_mapload_field(mbuf, blk, fldf);
}

static int W_mapload_wtype (MemBuffer *mbuf, const map_block_t *blk) {
  if (mbuf == NULL) return 0; // no post-processing
  //
  return W_mapload_field(mbuf, blk, fldm);
}


////////////////////////////////////////////////////////////////////////////////
static int W_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, FLDW);
    membuf_write_ui16(mbuf, FLDH);
    membuf_write_ui8(mbuf, sky_type&0x7f);
    //
    for (int f = 1; f < 256; ++f) {
      char s[8];
      //
      getname(f, s);
      membuf_write(mbuf, s, 8);
    }
    //
    membuf_write(mbuf, walf, sizeof(walf));
    membuf_write(mbuf, walswp, sizeof(walswp));
    membuf_write(mbuf, fldb, FLDW*FLDH);
    membuf_write(mbuf, fldm, FLDW*FLDH);
    membuf_write(mbuf, fldf, FLDW*FLDH);
  }
  //
  return 0;
}


static int W_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint16_t w, h;
    char s[9];
    //
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid map version!\n");
      return -1;
    }
    w = membuf_read_ui16(mbuf);
    h = membuf_read_ui16(mbuf);
    if (w < 1 || w > 32767 || h < 1 || h > 32767) {
      conlogf("LOADGAME ERROR: invalid map size!\n");
      return -1;
    }
    W_set_field_size(w, h);
    sky_type = membuf_read_ui8(mbuf);
    if (sky_type > 2) {
      conlogf("LOADGAME WARNING: invalid sky!\n");
      sky_type = 0;
    }
    //
    for (int f = 1; f < 256; ++f) {
      memset(s, 0, sizeof(s));
      walani[f] = 0;
      walp[f] = &walpix[f];
      if (membuf_read_full(mbuf, s, 8) < 0) return -1;
      if (!s[0]) {
        walh[f] = -1;
        V_freevga(&walpix[f]);
        walp[f] = NULL;
        continue;
      }
      walani[f] = getani(s);
      if (strncasecmp(s, "_WATER_", 7) == 0) {
        walh[f] = -2;
        walpix[f].wanim = (s[7]-'0'+1);
      } else {
        walh[f] = F_getresid(s);
        V_loadvga(s, &walpix[f]);
      }
    }
    //
    membuf_read_full(mbuf, walf, sizeof(walf));
    for (int f = 1; f < 256; ++f) if (walf[f]&1) walh[f] |= 0x8000;
    membuf_read_full(mbuf, walswp, sizeof(walswp));
    membuf_read_full(mbuf, fldb, FLDW*FLDH);
    membuf_read_full(mbuf, fldm, FLDW*FLDH);
    membuf_read_full(mbuf, fldf, FLDW*FLDH);
    //
    strcpy(s, "RSKY1");
    s[4] = sky_type+'0';
    V_loadvga(s, &horiz);
    //
    fix_anim();
  }
  //
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(ltn)/sizeof(ltn[0]); ++f) { V_initvga(&ltn[f][0]); V_initvga(&ltn[f][1]); }
  for (size_t f = 0; f < sizeof(walpix)/sizeof(walpix[0]); ++f) V_initvga(&walpix[f]);
  for (size_t f = 0; f < sizeof(anih)/sizeof(anih[0]); ++f) {
    V_initvga(&anispr[f][0]);
    V_initvga(&anispr[f][1]);
    V_initvga(&anispr[f][2]);
    V_initvga(&anispr[f][3]);
    V_initvga(&anispr[f][4]);
  }
  V_initvga(&horiz);
  //
  _light_constructor_();
  //
  F_registerMapLoad(MB_WALLNAMES, W_mapload_wallnames, NULL);
  F_registerMapLoad(MB_BACK, W_mapload_back, NULL);
  F_registerMapLoad(MB_WTYPE, W_mapload_wtype, NULL);
  F_registerMapLoad(MB_FRONT, W_mapload_front, NULL);
  F_registerMapLoad(MB_SKY, W_mapload_sky, NULL);
  //
  F_registerSaveLoad("LEVELMAPS", W_savegame, W_loadgame, SAV_NORMAL);
  F_registerSaveLoad("LEVELLIGHTS", W_savelights, W_loadlights, SAV_OPTIONAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(ltn)/sizeof(ltn[0]); ++f) { V_freevga(&ltn[f][0]); V_freevga(&ltn[f][1]); }
  for (size_t f = 0; f < sizeof(walpix)/sizeof(walpix[0]); ++f) V_freevga(&walpix[f]);
  for (size_t f = 0; f < sizeof(anih)/sizeof(anih[0]); ++f) {
    V_freevga(&anispr[f][0]);
    V_freevga(&anispr[f][1]);
    V_freevga(&anispr[f][2]);
    V_freevga(&anispr[f][3]);
    V_freevga(&anispr[f][4]);
  }
  V_freevga(&horiz);
}
