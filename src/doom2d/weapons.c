/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include <stdlib.h>

#include "bmap.h"
#include "common.h"
#include "cmdcon.h"
#include "dots.h"
#include "error.h"
#include "game.h"
#include "misc.h"
#include "map.h"
#include "monster.h"
#include "player.h"
#include "smoke.h"
#include "sound.h"
#include "vga.h"
#include "view.h"
#include "weapons.h"


////////////////////////////////////////////////////////////////////////////////
#define MAX_WEAPONFX  (300)


////////////////////////////////////////////////////////////////////////////////
enum {
  NONE = 0,
  ROCKET,
  PLASMA,
  APLASMA,
  BALL_IMP,
  BALL_CACO,
  BALL_BARON,
  BFGBALL,
  BFGHIT,
  MANF,
  REVF,
  FIRE
};


typedef struct GCC_PACKED {
  obj_t o;
  uint8_t t, s;
  int own;
  short target;
} weapon_t;


////////////////////////////////////////////////////////////////////////////////
static const snd_t *snd[14];
static vgaimg spr[49*2];
static char sprd[49*2];
static weapon_t wp[MAX_WEAPONFX];


////////////////////////////////////////////////////////////////////////////////
static void throw (int i, int x, int y, int xd, int yd, int r, int h, int s);


////////////////////////////////////////////////////////////////////////////////
void WP_alloc (void) {
  static const char nm[14][6] = {
    "PISTOL",
    "SHOTGN",
    "DSHTGN",
    "RLAUNC",
    "RXPLOD",
    "PLASMA",
    "FIRSHT",
    "FIRXPL",
    "BAREXP",
    "PUNCH",
    "SAWHIT",
    "MGUN",
    "SPARK1",
    "SPARK2"
  };
  int i;
  //
  for (i = 0; i < 4; ++i) {
    Z_getspr(&spr[i*2], "MISL", i, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "MISL", i, 2, sprd+i*2+1);
  }
  for (; i < 6; ++i) {
    Z_getspr(&spr[i*2], "PLSS", i-4, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "PLSS", i-4, 2, sprd+i*2+1);
  }
  for (; i < 11; ++i) {
    Z_getspr(&spr[i*2], "PLSE", i-6, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "PLSE", i-6, 2, sprd+i*2+1);
  }
  for (; i < 13; ++i) {
    Z_getspr(&spr[i*2], "APLS", i-11, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "APLS", i-11, 2, sprd+i*2+1);
  }
  for (; i < 18; ++i) {
    Z_getspr(&spr[i*2], "APBX", i-13, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "APBX", i-13, 2, sprd+i*2+1);
  }
  for (; i < 20; ++i) {
    Z_getspr(&spr[i*2], "BFS1", i-18, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "BFS1", i-18, 2, sprd+i*2+1);
  }
  for (; i < 26; ++i) {
    Z_getspr(&spr[i*2], "BFE1", i-20, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "BFE1", i-20, 2, sprd+i*2+1);
  }
  for (; i < 30; ++i) {
    Z_getspr(&spr[i*2], "BFE2", i-26, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "BFE2", i-26, 2, sprd+i*2+1);
  }
  for (; i < 32; ++i) {
    Z_getspr(&spr[i*2], "MISL", i-30+4, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "MISL", i-30+4, 2, sprd+i*2+1);
  }
  for (; i < 37; ++i) {
    Z_getspr(&spr[i*2], "BAL1", i-32, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "BAL1", i-32, 2, sprd+i*2+1);
  }
  for (; i < 42; ++i) {
    Z_getspr(&spr[i*2], "BAL7", i-37, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "BAL7", i-37, 2, sprd+i*2+1);
  }
  for (; i < 47; ++i) {
    Z_getspr(&spr[i*2], "BAL2", i-42, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "BAL2", i-42, 2, sprd+i*2+1);
  }
  for (; i < 49; ++i) {
    Z_getspr(&spr[i*2], "MANF", i-47, 1, sprd+i*2);
    Z_getspr(&spr[i*2+1], "MANF", i-47, 2, sprd+i*2+1);
  }
  for (i = 0; i < 14; ++i) snd[i] = Z_getsnd(nm[i]);
}


void WP_init (void) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    wp[i].o.type = OBJT_PROJECTILE;
    wp[i].t = NONE;
  }
}


void WP_act (void) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (wp[i].t) {
      int st;
      obj_t o;
      //
      if (!r_lighting && (wp[i].t == ROCKET || wp[i].t == REVF)) {
        SMK_gas(wp[i].o.x+Z_sign(wp[i].o.xv)*2, wp[i].o.y-wp[i].o.h/2, 3, 3, wp[i].o.xv+wp[i].o.vx, wp[i].o.yv+wp[i].o.vy, 64);
      }
      --wp[i].o.yv;
      st = Z_moveobj(&wp[i].o);
      if (st&Z_FALLOUT) { wp[i].t = 0; continue; }
      if (st&Z_HITWATER) {
        switch (wp[i].t) {
          case PLASMA: case APLASMA: case BFGBALL: break;
          default: Z_splash(&wp[i].o, wp[i].o.r+wp[i].o.h); break;
        }
      }
      switch (wp[i].t) {
        case REVF:
          if (Z_getobjpos(wp[i].target, &o)) throw(i, wp[i].o.x, wp[i].o.y-2, o.x+o.xv+o.vx, o.y+o.yv+o.vy, 2, 5, 12);
          // fallthru
        case ROCKET:
          if (wp[i].s >= 2) {
            if (++wp[i].s >= 8) wp[i].t = 0;
            break;
          }
          if (st&Z_HITAIR) Z_set_speed(&wp[i].o, 12);
          if (st&(Z_HITWALL|Z_HITCEIL|Z_HITLAND)) {
            wp[i].s = 2;
            wp[i].o.xv = wp[i].o.yv = 0;
            Z_sound(snd[4], 128);
            Z_explode(wp[i].o.x, wp[i].o.y, 30, wp[i].own);
            break;
          } else if (Z_hit(&wp[i].o, 10, wp[i].own, HIT_SOME)) {
            wp[i].s = 2;
            wp[i].o.xv = wp[i].o.yv = 0;
            Z_sound(snd[4], 128);
            Z_explode(wp[i].o.x, wp[i].o.y-wp[i].o.h/2, 30, wp[i].own);
            break;
          }
          bfg_fly(wp[i].o.x, wp[i].o.y-wp[i].o.h/2, wp[i].own);
          break;
        case PLASMA:
        case APLASMA:
          if (st&Z_INWATER) {
            Z_sound(snd[12], 128);
            Z_water_trap(&wp[i].o);
            Z_chktrap(1, 10, wp[i].own, HIT_ELECTRO);
            Z_untrap(5);
            wp[i].t = 0;
            break;
          }
          // fallthru
        case BALL_IMP:
        case BALL_BARON:
        case BALL_CACO:
        case MANF:
          if (wp[i].s >= 2) {
            if (++wp[i].s >= ((wp[i].t == BALL_IMP || wp[i].t == BALL_BARON || wp[i].t == BALL_CACO || wp[i].t == MANF) ? 8 : 12)) wp[i].t = 0;
            break;
          }
          if (st&Z_HITAIR) Z_set_speed(&wp[i].o, 16);
          if (st&(Z_HITWALL|Z_HITCEIL|Z_HITLAND)) {
            wp[i].s = 2;
            wp[i].o.xv = wp[i].o.yv = 0;
            Z_sound(snd[7], 128);
            break;
          } else if (Z_hit(&wp[i].o, (wp[i].t == BALL_BARON || wp[i].t == MANF) ? 40 : ((wp[i].t == BALL_CACO) ? 20 : 5), wp[i].own, HIT_SOME)) {
            wp[i].s = 2;
            wp[i].o.xv = wp[i].o.yv = 0;
            Z_sound(snd[7], 128);
            break;
          }
          wp[i].s ^= 1;
          break;
        case BFGBALL:
          if (st&Z_INWATER) {
            Z_sound(snd[8], 40);
            Z_sound(snd[13], 128);
            Z_water_trap(&wp[i].o);
            Z_chktrap(1, 1000, wp[i].own, HIT_ELECTRO);
            Z_untrap(5);
            wp[i].t = 0;
            break;
          }
          if (wp[i].s >= 2) {
            if(++wp[i].s >= 14) wp[i].t = 0;
            break;
          } else if (st&(Z_HITWALL|Z_HITCEIL|Z_HITLAND)) {
            Z_bfg9000(wp[i].o.x, wp[i].o.y, wp[i].own);
            wp[i].s = 2;
            wp[i].o.xv = wp[i].o.yv = 0;
            Z_sound(snd[8], 128);
            break;
          } else if (Z_hit(&wp[i].o, 100, wp[i].own, HIT_BFG)) {
            Z_bfg9000(wp[i].o.x, wp[i].o.y, wp[i].own);
            wp[i].s = 2;
            wp[i].o.xv = wp[i].o.yv = 0;
            Z_sound(snd[8], 128);
            break;
          }
          bfg_fly(wp[i].o.x, wp[i].o.y-wp[i].o.h/2, wp[i].own);
          wp[i].s ^= 1;
          break;
        case BFGHIT:
          if (++wp[i].s >= 8) wp[i].t = 0;
          break;
        default: break;
      }
    }
  }
}


static void WP_lighting (int i) {
  switch (wp[i].t) {
    case ROCKET:
      if (wp[i].s < 2) {
        Z_add_light(wp[i].o.x, wp[i].o.y, 120, 255, 127, 0, 120);
      } else {
        Z_add_light(wp[i].o.x, wp[i].o.y, 240-(wp[i].s-2)*32, 255, 0, 0, 200);
      }
      break;
    case PLASMA: Z_add_light(wp[i].o.x, wp[i].o.y, 62, 0, 0, 255, 100); break;
    case BALL_IMP: Z_add_light(wp[i].o.x, wp[i].o.y, 48, 255, 0, 0, 100); break;
    case BALL_CACO: Z_add_light(wp[i].o.x, wp[i].o.y, 64, 255, 0, 0, 200); break;
    case BALL_BARON: Z_add_light(wp[i].o.x, wp[i].o.y, 64, 0, 255, 0, 200); break;
    case BFGBALL: Z_add_light(wp[i].o.x, wp[i].o.y, 82, 0, 255, 0, 200); break;
    case MANF: Z_add_light(wp[i].o.x, wp[i].o.y, 82, 255, 0, 0, 200); break;
  }
}


void WP_draw (void) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    int s, d, x, y;
    //
    s = -1;
    d = 0;
    switch (wp[i].t) {
      case NONE: break;
      case REVF:
      case ROCKET:
        if ((d = wp[i].s) < 2) {
          d = (wp[i].o.xv > 0 ? 1 : 0);
          x = abs(wp[i].o.xv);
          y = wp[i].o.yv;
          s = 0;
          if (y < 0) { if (-y >= x) s = 30; }
          else if (y > 0) { if (y >= x/2) s = 31; }
        } else {
          s = (d-2)/2+1;
          d = 0;
        }
        break;
      case MANF:
        if ((s = wp[i].s) >= 2) {
          s /= 2;
          break;
        }
        // fallthru
      case PLASMA:
      case APLASMA:
      case BALL_IMP:
      case BALL_BARON:
      case BALL_CACO:
        if ((s = wp[i].s) >= 2) s = s/2+1;
        switch (wp[i].t) {
          case PLASMA: s += 4; break;
          case APLASMA: s += 11; break;
          case BALL_IMP: s += 32; break;
          case BALL_CACO: s += 42; break;
          case BALL_BARON: s += 37; d = (wp[i].o.xv >= 0 ? 1 : 0); break;
          case MANF: s += 47; d = (wp[i].o.xv >= 0 ? 1 : 0); break;
        }
        break;
      case BFGBALL:
        if ((s = wp[i].s) >= 2) s = s/2+1;
        s += 18;
        break;
      case BFGHIT:
        s = wp[i].s/2+26;
        break;
      default: break;
    }
    //
    if (s >= 0) {
      WP_lighting(i);
      Z_drawspr(wp[i].o.x, wp[i].o.y, &spr[s*2+d], sprd[s*2+d]);
    }
  }
}


void WP_gun (int x, int y, int xd, int yd, int objid_shooter, int v) {
  uint32_t d, m;
  int sx, sy, lx, ly;
  uint32_t xe, ye, s;
  uint8_t f;
  //
  f = BM_MONSTER|BM_WALL;
  if (objid_shooter != -1) f |= BM_PLR1;
  if (objid_shooter != -2) f |= BM_PLR2;
  //
  if ((xd -= x) > 0) sx = 1;
  else if (xd < 0) sx = -1;
  else sx = 0;
  //
  if ((yd -= y) > 0) sy = 1;
  else if (yd < 0) sy = -1;
  else sy = 0;
  //
  if (!xd && !yd) return;
  if ((xd = abs(xd)) > (yd = abs(yd))) d = xd; else d = yd;
  hit_xv = xd*10/d*sx;
  hit_yv = yd*10/d*sy;
  xe = ye = 0;
  lx = x;
  ly = y;
  for (;;) {
    if (x < 0 || x >= FLDW*8 || y < 0 || y >= FLDH*8) break;
    if ((m = BM_get(x, y)&f)) {
      if ((m&BM_WALL) && (FIELD_MAP(x>>3, y>>3) == TILE_WALL || FIELD_MAP(x>>3, y>>3) == TILE_DOORC)) {
        for (x = lx, y = ly, xe = ye = 0; FIELD_MAP(x>>3, y>>3) != TILE_WALL && FIELD_MAP(x>>3, y>>3) != TILE_DOORC; ) {
          lx = x;
          ly = y;
          if ((xe += xd) >= d) { xe -= d; x += sx; }
          if ((ye += yd) >= d) { ye -= d; y += sy; }
        }
        DOT_spark(lx, ly, sx*10, sy*10, 1);
        break;
      }
      if ((m&(BM_MONSTER|BM_PLR1|BM_PLR2)) && Z_gunhit(x, y, objid_shooter, sx*v, sy*v)) break;
      lx = x;
      ly = y;
      if ((xe += (xd<<3)) >= d) {
        x += xe/d*sx;
        xe = xe%d;
      }
      if ((ye += (yd<<3)) >= d) {
        y += ye/d*sy;
        ye = ye%d;
      }
    } else {
      if (sx == 0) m = 0;
      else { m = x&31; if (sx > 0) m ^= 31; ++m; }
      //
      if (sy == 0) s = 0;
      else { s = y&31; if (sy > 0) s ^= 31; ++s; }
      //
      if ((s < m && s != 0) || m == 0) m = s;
      lx = x;
      ly = y;
      x += (xd*m+xe)/d*sx;
      xe = (xd*m+xe)%d;
      y += (yd*m+ye)/d*sy;
      ye = (yd*m+ye)%d;
    }
  }
}


void WP_punch (int x, int y, int d, int own) {
  obj_t o;
  //
  o.x = x;
  o.y = y;
  o.r = 12;
  o.h = 26;
  o.xv = o.yv = o.vx = o.vy = 0;
  if (Z_hit(&o, d, own, HIT_SOME)) Z_sound(snd[9], 128);
}


int WP_chainsaw (int x, int y, int d, int own) {
  obj_t o;
  //
  o.x = x;
  o.y = y;
  o.r = 12;
  o.h = 26;
  o.xv = o.yv = o.vx = o.vy = 0;
  if (Z_hit(&o, d, own, HIT_SOME)) return 1;
  return 0;
}


static void throw (int i, int x, int y, int xd, int yd, int r, int h, int s) {
  int m;
  //
  wp[i].o.type = OBJT_PROJECTILE;
  wp[i].o.x = x;
  wp[i].o.y = y+h/2;
  yd -= y;
  xd -= x;
  if (!(m = K8MAX(abs(xd), abs(yd)))) m = 1;
  wp[i].o.xv = xd*s/m;
  wp[i].o.yv = yd*s/m;
  wp[i].o.r = r;
  wp[i].o.h = h;
  wp[i].o.vx = wp[i].o.vy = 0;
}


void WP_rocket (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      Z_sound(snd[3], 128);
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = ROCKET;
      wp[i].s = (xd >= x ? 1 : 0);
      wp[i].own = o;
      throw(i, x, y, xd, yd, 2, 5, 12);
      return;
    }
  }
}


void WP_revf (int x, int y, int xd, int yd, int o, int t) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      Z_sound(snd[3], 128);
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = REVF;
      wp[i].s = (xd >= x ? 1 : 0);
      wp[i].own = o;
      wp[i].target = t;
      throw(i, x, y, xd, yd, 2, 5, 12);
      return;
    }
  }
}


void WP_plasma (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      Z_sound(snd[5], 64);
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = PLASMA;
      wp[i].s = 0;
      wp[i].own = o;
      throw(i, x, y, xd, yd, 2, 5, 16);
      return;
    }
  }
}


void WP_ball1 (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = BALL_IMP;
      wp[i].s = 0;
      wp[i].own = o;
      throw(i, x, y, xd, yd, 2, 5, 16);
      return;
    }
  }
}


void WP_ball2 (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = BALL_CACO;
      wp[i].s = 0;
      wp[i].own = o;
      throw(i, x, y, xd, yd, 2, 5, 16);
      return;
    }
  }
}


void WP_ball7 (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = BALL_BARON;
      wp[i].s = 0;
      wp[i].own = o;
      throw(i, x, y, xd, yd, 2, 5, 16);
      return;
    }
  }
}


void WP_aplasma (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      Z_sound(snd[5], 64);
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = APLASMA;
      wp[i].s = 0;
      wp[i].own = o;
      throw(i, x, y, xd, yd, 2, 5, 16);
      return;
    }
  }
}


void WP_manfire (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      Z_sound(snd[6], 128);
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = MANF;
      wp[i].s = 0;
      wp[i].own = o;
      throw(i, x, y, xd, yd, 5, 11, 16);
      return;
    }
  }
}


void WP_bfgshot (int x, int y, int xd, int yd, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = BFGBALL;
      wp[i].s = 0;
      wp[i].own = o;
      throw(i, x, y, xd, yd, 5, 12, 16);
      return;
    }
  }
}


void WP_bfghit (int x, int y, int o) {
  for (int i = 0; i < MAX_WEAPONFX; ++i) {
    if (!wp[i].t) {
      wp[i].o.type = OBJT_PROJECTILE;
      wp[i].t = BFGHIT;
      wp[i].s = 0;
      wp[i].o.x = x;
      wp[i].o.y = y;
      wp[i].o.xv = wp[i].o.yv = 0;
      wp[i].o.r = 0;
      wp[i].o.h = 1;
      wp[i].o.vx = wp[i].o.vy = 0;
      wp[i].own = o;
      return;
    }
  }
}


void WP_pistol (int x, int y, int xd, int yd, int o) {
  Z_sound(snd[0], 96);
  WP_gun(x, y, xd, yd, o, 1);
  if (g_dm) {
    WP_gun(x, y+1, xd, yd+1, o, 1);
    WP_gun(x, y-1, xd, yd-1, o, 1);
  }
}


void WP_mgun (int x, int y, int xd, int yd, int o) {
  Z_sound(snd[11], 128);
  WP_gun(x, y, xd, yd, o, 1);
}


void WP_shotgun (int x, int y, int xd, int yd, int o) {
  Z_sound(snd[1], 128);
  for (int i = 0; i < 10; ++i) {
    int j = myrand(4*2+1)-4;
    //
    WP_gun(x, y+j, xd, yd+j, o, i&1);
  }
}


void WP_dshotgun (int x, int y, int xd, int yd, int o) {
  Z_sound(snd[2], 128);
  for (int i = (g_dm ? 25 : 20); i >= 0; --i) {
    int j = myrand(10*2+1)-10;
    //
    WP_gun(x, y+j, xd, yd+j, o, (i%3 ? 0 : 1));
  }
}


void WP_flamethrower (int x, int y, int xd, int yd, int xv, int yv, int o) {
  int m = abs(xd-x);
  //
  if (!m) m = abs(yd-y);
  SMK_flame(x, y, xv, yv, 2, 2, (xd-x)*3000/m, (yd-y)*3000/m, 1, o);
}


static int WP_savegame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    for (cnt = MAX_WEAPONFX; cnt > 0; --cnt) if (wp[cnt-1].t) break;
    membuf_write_ui8(mbuf, 0); // version
    membuf_write_ui16(mbuf, sizeof(wp[0])); // structure size
    membuf_write_ui16(mbuf, cnt);
    membuf_write(mbuf, wp, cnt*sizeof(wp[0]));
  }
  //
  return 0;
}


static int WP_loadgame (MemBuffer *mbuf, int waserror) {
  if (mbuf != NULL) {
    uint32_t cnt;
    //
    WP_init();
    if (membuf_read_ui8(mbuf) != 0) {
      conlogf("LOADGAME ERROR: invalid 'weapons' version!\n");
      return -1;
    }
    if (membuf_read_ui16(mbuf) != sizeof(wp[0])) {
      conlogf("LOADGAME ERROR: invalid 'weapons' size!\n");
      return -1;
    }
    cnt = membuf_read_ui16(mbuf);
    if (cnt > MAX_WEAPONFX) {
      conlogf("LOADGAME ERROR: too many 'weapons'!\n");
      return -1;
    }
    if (membuf_read_full(mbuf, wp, cnt*sizeof(wp[0])) < 0) {
      conlogf("LOADGAME ERROR: can't read 'weapons'!\n");
      return -1;
    }
  }
  //
  if (waserror) WP_init();
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_initvga(&spr[f]);
  //
  F_registerSaveLoad("WEAPONFX", WP_savegame, WP_loadgame, SAV_NORMAL);
}


GCC_DESTRUCTOR_USER {
  for (size_t f = 0; f < sizeof(spr)/sizeof(spr[0]); ++f) V_freevga(&spr[f]);
}
