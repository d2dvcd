/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Smoke
#ifndef D2D_SMOKE_H
#define D2D_SMOKE_H

#include "common.h"
#include "saveload.h"


extern const uint8_t flametab[16];

extern void SMK_init (void);
extern void SMK_alloc (void);
extern void SMK_act (void);
extern void SMK_draw (void);
extern void SMK_add (int x, int y, int xv, int yv, uint8_t t, uint8_t s, short o);
extern void SMK_gas (int x, int y, int rx, int ry, int xv, int yv, int k);
extern void SMK_flame (int x, int y, int ox, int oy, int rx, int ry, int xv, int yv, int k, int o);


#endif
