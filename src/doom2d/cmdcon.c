/*
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include "cmdcon.h"

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


////////////////////////////////////////////////////////////////////////////////
#define CMDCON_LINE_WIDTH  (128)

static int con_width = CMDCON_LINE_WIDTH-1;
static char cmdcon_lines[CMDCON_MAX_LINES][CMDCON_LINE_WIDTH];

FILE *cmdcon_dumpto[2] = {NULL, NULL};


////////////////////////////////////////////////////////////////////////////////
int get_max_con_width (void) {
  return CMDCON_LINE_WIDTH-1;
}


int get_con_width (void) {
  return con_width;
}


void set_con_width (int wdt) {
  if (wdt < 1) wdt = 1; else if (wdt > get_max_con_width()) wdt = get_max_con_width();
  con_width = wdt;
}


////////////////////////////////////////////////////////////////////////////////
static int cmdaSize = 0;
static int cmdaUsed = 0;
static CmdConCmd *cmda = NULL;


static int find_command (const char *name) {
  int imin = 0, imax = cmdaUsed-1;
  //
  if (name == NULL || !name[0] || cmdaUsed < 1) return -1;
  // continually narrow search until just one element remains
  while (imin < imax) {
    int imid = (imin+imax)/2; // we will never overflow here; no, really!
    //
    //assert(imid < imax);
    // note: 0 <= imin < imax implies imid will always be less than imax
    // reduce the search
    if (strcmp(cmda[imid].name, name) < 0) imin = imid+1; else imax = imid;
  }
  // At exit of while:
  //   if A[] is empty, then imax < imin
  //   otherwise imax == imin
  // deferred test for equality
  return (imax == imin && strcmp(cmda[imin].name, name) == 0 ? imin : -1);
}


CmdConCmd *cmdcon_find (const char *name) {
  if (name != NULL && name[0]) {
    int idx = find_command(name);
    //
    if (idx >= 0) return &cmda[idx];
  }
  //
  return NULL;
}


void cmdcon_update (CmdConCmd *cmd) {
  // nothing for now
}


void cmdcon_add (const CmdConCmd *cmd) {
  if (cmd != NULL && cmd->name != NULL && cmd->name[0]) {
    int idx = find_command(cmd->name);
    //
    if (idx < 0) {
      // not found, have to instert it
      // we can use binary search here too, but hey... who cares?
      for (idx = 0; idx < cmdaUsed; ++idx) if (strcmp(cmda[idx].name, cmd->name) > 0) break;
      // now idx points to the place where we want to see the command
      if (cmdaUsed >= cmdaSize) {
        // have to grow
        int newsz = cmdaSize+128;
        CmdConCmd *n = realloc(cmda, sizeof(CmdConCmd)*newsz);
        //
        if (n == NULL) { fprintf(stderr, "FATAL: out of memory in concmd!\n"); abort(); }
        cmdaSize = newsz;
        cmda = n;
      }
      // move down
      for (int f = cmdaUsed; f > idx; --f) cmda[f] = cmda[f-1];
      ++cmdaUsed;
    }
    // copy
    cmda[idx] = *cmd;
  }
}


////////////////////////////////////////////////////////////////////////////////
static char ccLock = 0;


void concmdLock (void) {
  while (__sync_bool_compare_and_swap(&ccLock, 0, 1)) usleep(1);
}


void concmdUnlock (void) {
  /*
  __sync_synchronize();
  ccLock = 0;
  */
  __sync_fetch_and_and(&ccLock, 0);
}


////////////////////////////////////////////////////////////////////////////////
// this will be run after all command constructors
static void __attribute__((constructor(669))) __cmdcon_init_ (void) {
  memset(cmdcon_lines, 0, sizeof(cmdcon_lines));
  cmdcon_dumpto[0] = stdout;
  cmdcon_dumpto[1] = NULL;
}


static void __attribute__((destructor(669))) __cmdcon_deinit_ (void) {
  if (cmda != NULL) free(cmda);
  for (size_t f = 0; f < sizeof(cmdcon_dumpto)/sizeof(cmdcon_dumpto[0]); ++f) {
    if (cmdcon_dumpto[f] != NULL && cmdcon_dumpto[f] != stdout && cmdcon_dumpto[f] != stderr) fclose(cmdcon_dumpto[f]);
  }
}


////////////////////////////////////////////////////////////////////////////////
const char *cmdcon_line (int idx) {
  return (idx >= 0 && idx < CMDCON_MAX_LINES ? cmdcon_lines[CMDCON_MAX_LINES-idx-1] : "");
}


////////////////////////////////////////////////////////////////////////////////
static void conscroll (void) {
  memmove(&cmdcon_lines[0], &cmdcon_lines[1], sizeof(cmdcon_lines)-CMDCON_LINE_WIDTH);
  memset(cmdcon_lines[CMDCON_MAX_LINES-1], 0, CMDCON_LINE_WIDTH);
}


////////////////////////////////////////////////////////////////////////////////
static inline int is_printf_fmt (const char *fmt) {
  while ((fmt = strchr(fmt, '%')) != NULL) {
    if (fmt[1] != '%') return 1;
    fmt += 2;
  }
  return 0;
}


static char *find_wrap_pos (char *s, int width) {
  char *le = s, *lastwb = NULL;
  int wdt = width;
  //
  while (wdt > 0) {
    if (!le[0]) return le; // line ends here
    if (isspace(*le) && strchr("([{", *le) != NULL) {
      lastwb = le++;
      --wdt;
      continue;
    }
    if (strchr(".,!?;:-)}]", *le) != NULL) lastwb = le+1;
    ++le;
    --wdt;
  }
  //
  if (lastwb == NULL) {
    // force break
    lastwb = s+width;
  }
  //
  return lastwb;
}


// NOT REENTRANT!
void conlogf_va (const char *fmt, va_list ap) {
  if (fmt != NULL) {
    static char prbuf[65536]; // don't print too long messages! %-)
    //
    concmdLock();
    //
    prbuf[sizeof(prbuf)-1] = 0;
    if (is_printf_fmt(fmt)) {
      vsnprintf(prbuf, sizeof(prbuf)-1, fmt, ap);
    } else {
      strncpy(prbuf, fmt, sizeof(prbuf)-1);
    }
    //
    if (prbuf[0]) {
      char *p = prbuf;
      int len;
      //
      prbuf[sizeof(prbuf)-2] = 0;
      //
      len = strlen(prbuf);
      if (prbuf[0] && prbuf[len-1] != '\n') { strcpy(prbuf+len, "\n"); ++len; }
      //
      for (int f = 0; f < 2; ++f) if (cmdcon_dumpto[f] != NULL) fwrite(prbuf, len, 1, cmdcon_dumpto[f]);
      //
      while (*p) {
        char *nl = strchr(p, '\n'), *s = p, *el;
        int len;
        int neednl = 1;
        //
        if (nl == NULL) nl = p+strlen(p);
        p = nl+(!!nl[0]);
        *nl = 0;
        //
        if (*s) {
          while (*s) {
            el = find_wrap_pos(s, con_width);
            len = el-s;
            //
            if (neednl) { conscroll(); neednl = 0; }
            if (len > con_width) len = con_width;
            strncpy(cmdcon_lines[CMDCON_MAX_LINES-1], s, len);
            //
            for (s = el; *s && isspace(*s); ++s) if (*s == '\n' && (++neednl) > 1) conscroll();
            if (!neednl) neednl = 1;
          }
        } else {
          conscroll(); 
        }
      }
    }
    concmdUnlock();
  }
}


__attribute__((format(printf,1,2))) void conlogf (const char *fmt, ...) {
  va_list ap;
  //
  if (fmt != NULL) {
    va_start(ap, fmt);
    conlogf_va(fmt, ap);
    va_end(ap);
  }
}


////////////////////////////////////////////////////////////////////////////////
//TODO: check overflow... ah, fuck it!
int64_t cmdcon_parseint (const char *str, int64_t defval, int64_t min, int64_t max, int *error) {
  if (str != NULL) while (*str && isspace(*str)) ++str;
  //
  if (str != NULL && str[0] && min <= max) {
    int neg = 0, base = 0;
    int64_t res = 0;
    //
    switch (*str++) {
      case '+': break;
      case '-': neg = 1; break;
      default: --str;
    }
    //
    if (str[0] == '0' && str[1]) {
      switch (str[1]) {
        case 'b': case 'B': base = 2; break;
        case 'd': case 'D': base = 10; break;
        case 'o': case 'O': base = 8; break;
        case 'x': case 'X': base = 16; break;
      }
      if (base != 0) str += 2;
    }
    if (!base) base = 10;
    //
    if (str[0]) {
      while (*str) {
        int c = toupper(*str)-'0';
        //
        if (c < 0 || (c > 9 && c < 17)) break;
        if (c > 9) c -= 7;
        if (c >= base) break;
        res = res*base+c;
        ++str;
      }
      if (str != NULL) while (*str && isspace(*str)) ++str;
      if (!str[0] && neg) res = -res;
      //
      if (!str[0] && res >= min && res <= max) {
        if (error) *error = 0;
        return res;
      }
    }
  }
  //
  if (error != NULL) *error = 1;
  return defval;
}


////////////////////////////////////////////////////////////////////////////////
int cmdcon_parsebool (const char *str, int defval, int *error) {
  int val = -1;
  //
  if (strcasecmp(str, "on") == 0 || strcasecmp(str, "true") == 0 ||
      strcasecmp(str, "yes") == 0 || strcasecmp(str, "tan") == 0 ||
      strcasecmp(str, "1") == 0) {
    val = 1;
  } else if (strcasecmp(str, "off") == 0 || strcasecmp(str, "false") == 0 ||
             strcasecmp(str, "no") == 0 || strcasecmp(str, "ona") == 0 ||
             strcasecmp(str, "0") == 0) {
    val = 0;
  }
  if (error) *error = (val >= 0);
  return (val >= 0 ? val : defval);
}


////////////////////////////////////////////////////////////////////////////////
static int64_t default_ival (const CmdConCmd *me, int argc, char *argv[], int64_t val, int64_t min, int64_t max, int *changed) {
  if (changed != NULL) *changed = 0;
  //
  if (argc == 2 && (strcmp(argv[1], "?") == 0 || strcmp(argv[1], "help") == 0)) {
    conlogf("%s\n%s", argv[0], me->help);
    return val;
  }
  //
  if (argc > 2) {
    conlogf("%s: too many arguments!", argv[0]);
    return val;
  }
  //
  if (argc > 1) {
    int64_t i;
    int error;
    //
    if (me->flags&CMDCON_FLAGS_READONLY) {
      conlogf("%s: read-only", argv[0]);
    } else {
      i = cmdcon_parseint(argv[1], val, min, max, &error);
      if (!error) {
        if (i != val && changed != NULL) *changed = 1;
        val = i;
      } else {
        conlogf("%s: invalid number: '%s'", argv[0], argv[1]);
      }
    }
  } else {
    conlogf("%s: %d", argv[0], *(me->vint));
  }
  //
  return val;
}


////////////////////////////////////////////////////////////////////////////////
void _cmdcon_default_BYTE_cmd (CmdConCmd *me, int argc, char *argv[]) {
  int changed;
  int64_t v = default_ival(me, argc, argv, *(me->vint), 0, 255, &changed);
  //
  if (changed) {
    if ((me->flags&CMDCON_FLAGS_PROTECTED) == 0 || me->pfn == NULL || me->pfn(me, argv[1])) {
      if ((me->flags&CMDCON_FLAGS_NOCHANGE) == 0) *(me->vu8) = v;
      if (me->cfn != NULL) me->cfn(me, argv[1]);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void _cmdcon_default_WORD_cmd (CmdConCmd *me, int argc, char *argv[]) {
  int changed;
  int64_t v = default_ival(me, argc, argv, *(me->vint), 0, 65535, &changed);
  //
  if (changed) {
    if ((me->flags&CMDCON_FLAGS_PROTECTED) == 0 || me->pfn == NULL || me->pfn(me, argv[1])) {
      if ((me->flags&CMDCON_FLAGS_NOCHANGE) == 0) *(me->vu16) = v;
      if (me->cfn != NULL) me->cfn(me, argv[1]);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void _cmdcon_default_DWORD_cmd (CmdConCmd *me, int argc, char *argv[]) {
  int changed;
  int64_t v = default_ival(me, argc, argv, *(me->vint), 0, 0xffffffffLL, &changed);
  //
  if (changed) {
    if ((me->flags&CMDCON_FLAGS_PROTECTED) == 0 || me->pfn == NULL || me->pfn(me, argv[1])) {
      if ((me->flags&CMDCON_FLAGS_NOCHANGE) == 0) *(me->vu32) = v;
      if (me->cfn != NULL) me->cfn(me, argv[1]);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void _cmdcon_default_INT_cmd (CmdConCmd *me, int argc, char *argv[]) {
  int changed;
  int64_t v = default_ival(me, argc, argv, *(me->vint), -2147483648, 2147483647, &changed);
  //
  if (changed) {
    if ((me->flags&CMDCON_FLAGS_PROTECTED) == 0 || me->pfn == NULL || me->pfn(me, argv[1])) {
      if ((me->flags&CMDCON_FLAGS_NOCHANGE) == 0) *(me->vint) = v;
      if (me->cfn != NULL) me->cfn(me, argv[1]);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void _cmdcon_default_BOOL_cmd (CmdConCmd *me, int argc, char *argv[]) {
  CMDCON_HELP();
  if (argc > 2) {
    conlogf("%s: too many arguments!", argv[0]);
    return;
  }
  //
  if (argc > 1) {
    if (me->flags&CMDCON_FLAGS_READONLY) {
      conlogf("%s: read-only", argv[0]);
    } else {
      int val = cmdcon_parsebool(argv[1], -1, NULL);
      //
      if (val < 0) {
        conlogf("%s: invalid boolean: '%s'", argv[0], argv[1]);
      } else if (*(me->vbool) != val) {
        if ((me->flags&CMDCON_FLAGS_PROTECTED) == 0 || me->pfn == NULL || me->pfn(me, argv[1])) {
          if ((me->flags&CMDCON_FLAGS_NOCHANGE) == 0) *(me->vbool) = val;
          if (me->cfn != NULL) me->cfn(me, argv[1]);
        }
      }
    }
  } else {
    conlogf("%s: %s", argv[0], (*(me->vbool) ? "true" : "false"));
  }
}


////////////////////////////////////////////////////////////////////////////////
void _cmdcon_default_STRING_cmd (CmdConCmd *me, int argc, char *argv[]) {
  CMDCON_HELP();
  if (argc > 2) {
    conlogf("%s: too many arguments!", argv[0]);
    return;
  }
  //
  if (argc > 1) {
    if (me->flags&CMDCON_FLAGS_READONLY) {
      conlogf("%s: read-only", argv[0]);
    } else {
      if ((me->flags&CMDCON_FLAGS_PROTECTED) == 0 || me->pfn == NULL || me->pfn(me, argv[1])) {
        if ((me->flags&CMDCON_FLAGS_NOCHANGE) == 0) {
          if (*(me->vstr) != NULL) free(*(me->vstr));
          *(me->vstr) = strdup(argv[1]);
        }
        if (me->cfn != NULL) me->cfn(me, argv[1]);
      }
    }
  } else {
    conlogf("%s: %s", argv[0], *(me->vstr));
  }
}


////////////////////////////////////////////////////////////////////////////////
static const char *skipSpaces (const char *s) {
  while (*s && isspace(*s)) ++s;
  return s;
}


static const char *getArg (char *dest, int *alen, const char *s) {
  char qch = 0;
  //
  void savech (int ch) {
    if (dest != NULL) *dest++ = ch;
    if (alen != NULL) ++(*alen);
  }
  //
  if (alen != NULL) *alen = 0;
  s = skipSpaces(s);
  if (*s && (*s == '"' || *s == '\'')) qch = *s++;
  //
  while (*s) {
    if (qch) {
      if (*s == qch) { ++s; break; }
    } else {
      if (isspace(*s) || *s == ';' || *s == '#') break;
    }
    //
    if (*s == '\\') {
      int n;
      //
      ++s;
      if (!s[0]) { savech('\\'); break; }
      switch (*s++) {
        case 'a': savech('\a'); break;
        case 'b': savech('\b'); break;
        case 'e': savech('\x1b'); break;
        case 'f': savech('\f'); break;
        case 'n': savech('\n'); break;
        case 'r': savech('\r'); break;
        case 'v': savech('\v'); break;
        case 'x': case 'X': // hex
          n = 0;
          for (int f = 0; f < 2; ++f) {
            if (*s && isxdigit(*s)) {
              n = n*16+(toupper(s[0])-'0'-(s[0] > '9' ? 7 : 0));
              ++s;
            }
          }
          savech(n);
          break;
        case '0': // octal
          n = 0;
          for (int f = 0; f < 3; ++f) {
            if (*s && s[0] >= '0' && s[0] <= '7') {
              int nn = n*8+s[0]-'0';
              //
              if (nn > 255) break;
              n = nn;
              ++s;
            }
          }
          savech(n);
          break;
        case '1' ... '9':
          n = 0;
          --s;
          for (int f = 0; f < 3; ++f) {
            if (*s && s[0] >= '0' && s[0] <= '9') {
              int nn = n*10+s[0]-'0';
              //
              if (nn > 255) break;
              n = nn;
              ++s;
            }
          }
          savech(n);
          break;
        default: savech(s[-1]); break;
      }
    } else {
      savech(*s++);
    }
  }
  //
  savech(0);
  //
  return skipSpaces(s);
}


#define MAX_ARGS  (128)
int cmdcon_exec (const char *str) {
  if (str != NULL) {
    char *argv[MAX_ARGS];
    int argc, len, idx;
    int res = 0;
    //
again:
    argc = 0;
    str = skipSpaces(str);
    if (str[0] == ';') { ++str; goto again; }
    if (!str[0] || str[0] == '#') return res; // ignore comments and empty lines
    getArg(NULL, &len, str);
    if (len > 0) {
      while (*str && str[0] != ';' && str[0] != '#') {
        getArg(NULL, &len, str);
        argv[argc] = calloc(len, sizeof(char));
        str = getArg(argv[argc], &len, str);
        ++argc;
      }
      //
      if ((idx = find_command(argv[0])) >= 0) {
        cmda[idx].fn(&cmda[idx], argc, argv);
        //res = 0;
      } else {
        res = -1;
      }
      //
      while (--argc >= 0) free(argv[argc]);
      //
      if (str[0] == ';') { ++str; goto again; }
      return res;
    }
  }
  //
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
/*
 * ���������� �� ������ ������, ������� ��������
 * ���� � ����� 0: �����, ������, �������, ����, ������, ����������
 * ���� � ����� 1: �������, ���������� ���� 1
 * ���� � ����� �����: �������� �������� ������� �������, ����������; ������ �������� ��� ��������� �������
 * ��������� �� ������ NULL, � ��� ���� free()
 * ����! `cmds` ������ ���� �������� �������, ��� ������ ��������
 * ���� `cmds` -- NULL, �� ���������� �ӣ
 * ���� ������� ������ � �������� � ����� -- ���, ��� ��� ������������ �������
 */
char *cmdcon_tabcomplete (const char *cmds, CmdConTabCompleteCB printcb) {
  if (cmds != NULL && cmds[0]) {
    const char *found = NULL;
    int foundlen = 0;
    int pfxcount = 0;
    int slen = strlen(cmds);
    char *res;
    //
    // ������ ������: ������� ��������, ���������� ����� ������� ��������
    for (int idx = 0; idx < cmdaUsed; ++idx) {
      const CmdConCmd *cmd = &cmda[idx];
      const char *s = cmd->name;
      int l0 = strlen(s);
      //
      if (slen <= l0 && strncmp(s, cmds, slen) == 0) {
        // �����
        if (l0 > foundlen) {
          found = s;
          foundlen = l0;
        }
        ++pfxcount;
      }
    }
    //
    if (!pfxcount) return strdup(cmds); // �� ����� ������ ������, ����� ������, ������!
    if (pfxcount == 1) {
      // ����� ����, �������, ��� � ���Σ�
      res = calloc(strlen(found)+2, sizeof(char));
      sprintf(res, "%s ", found);
      return res;
    }
    // ����� ������, ��� ����������
    // ���� ����� ������� �������, ������ �������� �ӣ, ��� �����
    res = strdup(found); // ������� �� ������, � ���������� ���
    for (int idx = 0; idx < cmdaUsed; ++idx) {
      const CmdConCmd *cmd = &cmda[idx];
      const char *s = cmd->name;
      //
      if (strncmp(s, res, slen) == 0) {
        // ����, �������� � ������ �������
        char *t = res;
        //
        if (printcb != NULL) printcb(s);
        while (*t && *s && *t == *s) { ++t; ++s; }
        *t = '\0'; // �� ������, ��� � �������
      }
    }
    // ���, ���������� ��������
    return res;
  } else if (cmds == NULL && printcb != NULL) {
    // ���� ���������� ��� ���������
    for (int idx = 0; idx < cmdaUsed; ++idx) printcb(cmda[idx].name);
  }
  return strdup("");
}


////////////////////////////////////////////////////////////////////////////////
__attribute__((sentinel)) char *cmdcon_completeabbrev (const char *cmds, CmdConTabCompleteCB printcb, ...) {
  if (cmds != NULL && cmds[0]) {
    va_list va;
    const char *found = NULL, *s;
    int foundlen = 0;
    int pfxcount = 0;
    int slen = strlen(cmds);
    char *res;
    //
    // ������ ������: ������� ��������, ���������� ����� ������� ��������
    va_start(va, printcb);
    while ((s = va_arg(va, const char *)) != NULL) {
      int l0 = strlen(s);
      //
      if (slen <= l0 && strncmp(s, cmds, slen) == 0) {
        // �����
        if (l0 > foundlen) {
          found = s;
          foundlen = l0;
        }
        ++pfxcount;
      }
    }
    va_end(va);
    //
    if (!pfxcount) return strdup(cmds); // �� ����� ������ ������, ����� ������, ������!
    if (pfxcount == 1) {
      // ����� ����, �������, ��� � ���Σ�
      res = calloc(strlen(found)+2, sizeof(char));
      sprintf(res, "%s ", found);
      return res;
    }
    // ����� ������, ��� ����������
    // ���� ����� ������� �������, ������ �������� �ӣ, ��� �����
    res = strdup(found); // ������� �� ������, � ���������� ���
    va_start(va, printcb);
    while ((s = va_arg(va, const char *)) != NULL) {
      if (strncmp(s, res, slen) == 0) {
        // ����, �������� � ������ �������
        char *t = res;
        //
        if (printcb) printcb(s);
        while (*t && *s && *t == *s) { ++t; ++s; }
        *t = '\0'; // �� ������, ��� � �������
      }
    }
    va_end(va);
    // ���, ���������� ��������
    return res;
  } else if (cmds == NULL && printcb != NULL) {
    // ���� ���������� ��� ���������
    va_list va;
    const char *s;
    //
    va_start(va, printcb);
    while ((s = va_arg(va, const char *)) != NULL) printcb(s);
    va_end(va);
  }
  return strdup("");
}


//FIXME: fuckin' copypasta!
char *cmdcon_completeabbrevArray (const char *cmds, CmdConTabCompleteCB printcb, const char *array[]) {
  if (cmds != NULL && cmds[0]) {
    const char *found = NULL, *s;
    int foundlen = 0;
    int pfxcount = 0;
    int slen = strlen(cmds);
    char *res;
    //
    // ������ ������: ������� ��������, ���������� ����� ������� ��������
    for (int f = 0; (s = array[f]) != NULL; ++f) {
      int l0 = strlen(s);
      //
      if (slen <= l0 && strncmp(s, cmds, slen) == 0) {
        // �����
        if (l0 > foundlen) {
          found = s;
          foundlen = l0;
        }
        ++pfxcount;
      }
    }
    //
    if (!pfxcount) return strdup(cmds); // �� ����� ������ ������, ����� ������, ������!
    if (pfxcount == 1) {
      // ����� ����, �������, ��� � ���Σ�
      res = calloc(strlen(found)+2, sizeof(char));
      sprintf(res, "%s ", found);
      return res;
    }
    // ����� ������, ��� ����������
    // ���� ����� ������� �������, ������ �������� �ӣ, ��� �����
    res = strdup(found); // ������� �� ������, � ���������� ���
    for (int f = 0; (s = array[f]) != NULL; ++f) {
      if (strncmp(s, res, slen) == 0) {
        // ����, �������� � ������ �������
        char *t = res;
        //
        if (printcb) printcb(s);
        while (*t && *s && *t == *s) { ++t; ++s; }
        *t = '\0'; // �� ������, ��� � �������
      }
    }
    // ���, ���������� ��������
    return res;
  } else if (cmds == NULL && printcb != NULL) {
    // ���� ���������� ��� ���������
    const char *s;
    //
    for (int f = 0; (s = array[f]) != NULL; ++f) printcb(s);
  }
  return strdup("");
}


////////////////////////////////////////////////////////////////////////////////
int cmdcon_enum (CmdConIteratorCB cb) {
  int res = 0;
  //
  if (cb != NULL) {
    for (int idx = 0; idx < cmdaUsed; ++idx) {
      CmdConCmd *cmd = &cmda[idx];
      //
      if ((res = cb(cmd)) != 0) break;
    }
  }
  //
  return res;
}
