/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Game menus
#ifndef D2D_MENU_H
#define D2D_MENU_H

#include "SDL.h"

#include "common.h"


typedef struct menu_item_t menu_item_t;
typedef struct menu_t menu_t;


enum {
  LANG_EN,
  LANG_RU,
  //
  LANG_MAX
};


extern int lastkey;
extern int lastkeychar; // changes with lastkey
extern SDL_KeyboardEvent lastkeyevt;
extern int gm_cur_lang; // 0: english; 1: russian


extern void GM_init (void);
extern int GM_act (void);
extern int GM_isactive (void);
extern int GM_draw (void);
extern void GM_set (menu_t *m);

extern void G_code (void);

extern int load_game (int n); // 0: ok
extern int load_game_fn (const char *fname); // 0: ok

extern void M_init_players (void);

// main.c
extern void setgamma (int);


//TODO: event system!
extern void evt_language_changed (void);


#endif
