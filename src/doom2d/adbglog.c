/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
/* debug logs */
#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
# include <windows.h>
#endif

#include "adbglog.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/types.h>


DbgLogUpdateFn dbgLogPreUpdate = NULL;
DbgLogUpdateFn dbgLogPostUpdate = NULL;


static char adbgLock = 0;
#ifndef NO_DEBUG_LOG
static pid_t mypid;
static char dbgLogFileName[8192] = {0};
static char modulePath[8192];
static int dbgLogWriteToFile = 1; /* default: yes */
static int dbgLogWriteToScr = 0;
static FILE *dbgLogFile = NULL;
static int dbgLogFileErr = 0;


static void dbgLogShutdown (void) {
  dbgLock();
  if (dbgLogFile != NULL) {
    fclose(dbgLogFile);
    dbgLogFile = NULL;
  }
  dbgUnlock();
}


__attribute__((constructor(256))) static void dbgLogInit (void) {
#ifndef _WIN32
  mypid = getpid();
  memset(modulePath, 0, sizeof(modulePath));
  if (readlink("/proc/self/exe", modulePath, sizeof(modulePath)-1) < 0) {
    strcpy(modulePath, "./");
  } else {
    char *p = (char *)strrchr(modulePath, '/');
    //
    if (p == NULL) strcpy(modulePath, "./"); else p[1] = '\0';
  }
#else
  char *p;
  //
  memset(modulePath, 0, sizeof(modulePath));
  GetModuleFileName(GetModuleHandle(NULL), modulePath, sizeof(modulePath)-1);
  for (p = modulePath; *p; ++p) if (*p == '\\') *p = '/';
  p = strrchr(modulePath, '/');
  if (p == NULL) strcpy(modulePath, "./"); else p[1] = '\0';
#endif
  if (modulePath[strlen(modulePath)-1] != '/') strcat(modulePath, "/"); // just in case, hehe
  if (!dbgLogFileName[0]) dbgLogSetFileName("debug.log");
  atexit(dbgLogShutdown);
}


void dbgLock (void) {
  while (__sync_bool_compare_and_swap(&adbgLock, 0, 1)) usleep(1);
}


void dbgUnlock (void) {
  /*
  __sync_synchronize();
  adbgLock = 0;
  */
  __sync_fetch_and_and(&adbgLock, 0);
}


void dbgLogSetFileName (const char *fname) {
  dbgLock();
  if (fname && fname[0]) {
    static char buf[8192];
    //
    buf[0] = '\0';
    if (fname[0] != '/' && fname[0] != '.') strcpy(buf, modulePath);
    strcat(buf, fname);
    strcpy(dbgLogFileName, buf);
  } else {
    dbgLogFileName[0] = '\0';
  }
  if (dbgLogFile != NULL) {
    fclose(dbgLogFile);
    dbgLogFile = NULL;
  }
  dbgLogFileErr = 0;
  dbgUnlock();
}


int dbgLogSetFileOut (int doIt) {
  int old;
  dbgLock();
  old = dbgLogWriteToFile;
  dbgLogWriteToFile = !!doIt;
  dbgUnlock();
  return old;
}


int dbgSetScreenOut (int doIt) {
  int old;
  dbgLock();
  old = dbgLogWriteToScr;
  dbgLogWriteToScr = !!doIt;
  dbgUnlock();
  return old;
}


void dlogf (const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  dlogfVA(fmt, ap);
  va_end(ap);
}


static void writeToFile (FILE *fl, const char *pfx, const char *buf) {
  int pfxlen = strlen(pfx);
  //
  while (*buf) {
    const char *eol;
    // first write prefix
    if (pfxlen > 0) fwrite(pfx, pfxlen, 1, fl);
    // now look for EOL
    if ((eol = strpbrk(buf, "\r\n")) == NULL) eol = buf+strlen(buf);
    // write string part
    if (eol > buf) fwrite(buf, eol-buf, 1, fl);
    // write EOL
    fputc('\n', fl);
    // skip EOL
    switch ((buf = eol)[0]) {
      case '\0': break; // done with buf
      case '\r':
        if (*(++buf) != '\n') break;
        // fallthru
      default: ++buf; break;
    }
  }
}


void dlogfVA (const char *fmt, va_list inap) {
  static char timebuf[128];
  static char buf[65536];
  time_t t;
  struct tm bt;
  //
  if (fmt == NULL) return;
  //
  dbgLock();
  //
  if (!dbgLogWriteToFile && !dbgLogWriteToScr) goto quit;
  t = time(NULL);
#ifndef _WIN32
  localtime_r(&t, &bt);
#else
  {
    struct tm *tres = localtime(&t);
    //
    bt = *tres;
  }
#endif
  sprintf(timebuf, "[%05i] %04i/%02i/%02i %02i:%02i:%02i: ",
    mypid,
    bt.tm_year+1900, bt.tm_mon+1, bt.tm_mday,
    bt.tm_hour, bt.tm_min, bt.tm_sec
  );
  //
  memset(buf, 0, sizeof(buf));
  //va_copy(ap2, inap);
  vsnprintf(buf, sizeof(buf)-1, fmt, inap);
  //va_end(ap2);
  buf[sizeof(buf)-1] = '\0';
  //
  if (dbgLogPreUpdate) dbgLogPreUpdate();
  //
  if (dbgLogWriteToFile && dbgLogFile == NULL && dbgLogFileErr == 0 && dbgLogFileName[0]) {
    // try to open debug log file
    dbgLogFile = fopen(dbgLogFileName, "a");
    ++dbgLogFileErr; // don't do this again
  }
  //
  if (dbgLogWriteToFile && dbgLogFile != NULL) {
    writeToFile(dbgLogFile, timebuf, buf);
    fflush(dbgLogFile);
  }
  //
  if (dbgLogWriteToScr) {
    fputs("\r\x1b[K", stderr);
    writeToFile(stderr, timebuf+13, buf); // skip PID and year
  }
  //
  if (dbgLogPostUpdate) dbgLogPostUpdate();
  //
quit:
  dbgUnlock();
}

#else

void dbgLogInit (void) {}
void dbgLock (void) { while (__sync_bool_compare_and_swap(&adbgLock, 0, 1)) usleep(1); }
void dbgUnlock (void) { __sync_fetch_and_and(&adbgLock, 0); }
void dbgLogSetFileName (const char *fname) {}
int dbgSetScreenOut (int doIt) { return 0; }
int dbgLogSetFileOut (int doIt) { return 0; }

#endif


#ifdef __cplusplus
}
#endif
