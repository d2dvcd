/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#include "screenshot.h"

#if defined(HAVE_IMLIB2)
# include <Imlib2.h>
#endif
#if defined(HAVE_LIBPNG)
# include <png.h>
#endif

#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"
#include "membuffer.h"
#include "vga.h"


#if defined(HAVE_IMLIB2) || defined(HAVE_LIBPNG)
#if defined(HAVE_LIBPNG)
static void pngWrite (png_structp png_ptr, png_bytep data, png_size_t length) {
  FILE *fp = (FILE *)png_get_io_ptr(png_ptr);
  fwrite(data, length, 1, fp);
}
#endif


static int save_png (const char *fname, SDL_Surface *sfc) {
  if (fname != NULL && fname[0] && sfc != NULL && sfc->w > 0 && sfc->h > 0) {
#if defined(HAVE_LIBPNG)
    png_structp png_ptr;
    png_infop info_ptr;
    int res = -1;
    //png_colorp palette;
    png_byte **row_pointers = NULL;
    png_ptr = NULL;
    info_ptr = NULL;
    //palette = NULL;
    FILE *fp = fopen(fname, "wb");
    //
    if (fp == NULL) return -1;
    row_pointers = (png_byte **)malloc(sfc->h*sizeof(png_byte *));
    if (row_pointers == NULL) return -1;
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL,NULL,NULL);
    if (png_ptr == NULL) { free(row_pointers); return -1; }
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) { png_destroy_write_struct(&png_ptr, &info_ptr); free(row_pointers); return -1; }
    // setup custom writer function
    png_set_write_fn(png_ptr, (voidp)fp, pngWrite, NULL);
    if (setjmp(png_jmpbuf(png_ptr))) {
       png_destroy_write_struct(&png_ptr, &info_ptr);
       free(row_pointers);
       return -1;
    }
    /*
    if (compression > Z_BEST_COMPRESSION) compression = Z_BEST_COMPRESSION;
    if (compression == Z_NO_COMPRESSION) {
      // No compression
      png_set_filter(png_ptr, 0, PNG_FILTER_NONE);
      png_set_compression_level(png_ptr, Z_NO_COMPRESSION);
    } else if (compression < 0) {
      png_set_compression_level(png_ptr, Z_DEFAULT_COMPRESSION);
    } else {
      png_set_compression_level(png_ptr, compression);
    }
    */
    png_set_filter(png_ptr, 0, PNG_FILTER_NONE);
    png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);
    //png_set_compression_level(png_ptr, Z_NO_COMPRESSION);
    png_set_IHDR(png_ptr, info_ptr, sfc->w, sfc->h, 8,
      PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);
    uint8_t *tcImg = (uint8_t *)malloc((sfc->w*3)*sfc->h), *tc = tcImg;
    //
    for (int y = 0; y < sfc->h; ++y) {
      const uint8_t *scr = ((const uint8_t *)sfc->pixels)+(y*sfc->pitch);
      //
      row_pointers[y] = (png_bytep)tc;
      for (int x = 0; x < sfc->w; ++x, scr += 4) {
        uint8_t b = scr[VGA_BIDX], g = scr[VGA_GIDX], r = scr[VGA_RIDX];
        //
        *tc++ = r;
        *tc++ = g;
        *tc++ = b;
        //*tc++ = 255; //0?
      }
    }
    png_write_image(png_ptr, row_pointers);
    png_write_end(png_ptr, NULL);
    fclose(fp);
    res = 0;
    free(tcImg);
    //
    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(row_pointers);
    //
    return res;
#elif defined(HAVE_IMLIB2)
    Imlib_Image img = imlib_create_image(sfc->w, sfc->h);
    Imlib_Load_Error err;
    DATA32 *raw;
    uint8_t *pp;
    //
    if (img == NULL) return -1;
    imlib_context_set_image(img);
    raw = imlib_image_get_data(); //brga
    pp = (uint8_t *)raw;
    for (int y = 0; y < sfc->h; y++) {
      const uint8_t *scr = ((const uint8_t *)sfc->pixels)+(y*sfc->pitch);
      //
      for (int x = 0; x < sfc->w; x++, scr += 4) {
        uint8_t b = scr[VGA_BIDX], g = scr[VGA_GIDX], r = scr[VGA_RIDX];
        //
        *pp++ = b;
        *pp++ = g;
        *pp++ = r;
        *pp++ = 255; //0?
      }
    }
    imlib_image_set_format("png");
    imlib_save_image_with_error_return(fname, &err);
    imlib_free_image_and_decache();
    return (err ? -1 : 0);
#else
    BUG!
#endif
  }
  //
  return -1;
}

#else

static int save_bmp (const char *fname, SDL_Surface *sfc) {
  if (fname != NULL && fname[0] && sfc != NULL && sfc->w > 0 && sfc->h > 0) {
    static const uint8_t bmp_header[54] = {
      0x42, 0x4D, 0xB6, 0x4F, 0x12, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x36, 0x00,
      0x00, 0x00, 0x28, 0x00, 0x00, 0x00,
      0x20, 0x03, 0x00, 0x00, 0xF4, 0x01,
      0x00, 0x00, 0x01, 0x00, 0x18, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x80, 0x4F,
      0x12, 0x00, 0x33, 0x0B, 0x00, 0x00,
      0x33, 0x0B, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };
    uint8_t hdr[sizeof(bmp_header)];
    FILE *fo;
    MemBuffer *buf;
    //
    if ((buf = membuf_new_write()) == NULL) return -1;
    memcpy(hdr, bmp_header, sizeof(bmp_header));
    hdr[0x12] = sfc->w&0xff;
    hdr[0x13] = (sfc->w>>8)&0xff;
    hdr[0x16] = sfc->h&0xff;
    hdr[0x17] = (sfc->h>>8)&0xff;
    if (membuf_write(buf, hdr, sizeof(hdr)) != sizeof(hdr)) goto error;
    //
    for (int y = sfc->h-1; y >= 0; --y) {
      const uint8_t *scr = ((const uint8_t *)sfc->pixels)+(y*sfc->pitch);
      //
      for (int x = 0; x < sfc->w; ++x, scr += 4) {
        uint8_t b = scr[VGA_BIDX], g = scr[VGA_GIDX], r = scr[VGA_RIDX];
        //
        if (membuf_write(buf, &b, 1) != 1) goto error;
        if (membuf_write(buf, &g, 1) != 1) goto error;
        if (membuf_write(buf, &r, 1) != 1) goto error;
      }
      // align on word boundary
      if (sfc->w%2 && membuf_write(buf, "", 1) != 1) goto error;
    }
    //
    if ((fo = fopen(fname, "wb")) != NULL) {
      if (fwrite(membuf_data(buf), membuf_used(buf), 1, fo) != 1) {
        fclose(fo);
        unlink(fname);
        goto error;
      }
      fclose(fo);
      //
      return 0;
    }
error:
    membuf_free(buf);
  }
  //
  return -1;
}

#endif


void save_screenshot (void) {
  static int num = 1;
  static char fn[4096];
  const char *home = get_home_dir();
  const char *ext;
  int err;
  //
  /*
  strncpy(fn, home, 60);
  sprintf(&fn[strlen(fn)], "/%s", SAVE_DIR);
  xmkdir(fn);
  sprintf(&fn[strlen(fn)], "/shot%04d.bmp", num);
  SDL_SaveBMP(screen, fn);
  */
  //
  home = get_bin_dir();
  strncpy(fn, home, 60);
  sprintf(&fn[strlen(fn)], "/shot%04d", num);
#if defined(HAVE_IMLIB2) || defined(HAVE_LIBPNG)
  ext = ".png";
  strcat(fn, ".png");
  SDL_LockSurface(screenReal);
  err = save_png(fn, screenReal);
  SDL_UnlockSurface(screenReal);
#else
  ext = ".bmp";
  strcat(fn, ".bmp");
  //SDL_SaveBMP(screenReal, fn);
  SDL_LockSurface(screenReal);
  err = save_bmp(fn, screenReal);
  SDL_UnlockSurface(screenReal);
#endif
  //
  if (!err) {
    conlogf("screenshot shot%04d%s taken\n", num, ext);
    ++num;
  } else {
    conlogf("error writing screenshot shot%04d%s!\n", num, ext);
  }
}
