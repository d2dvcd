/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_VGA_H
#define D2D_VGA_H

#include "SDL.h"

#include "common.h"


////////////////////////////////////////////////////////////////////////////////
extern int VID_HQ;

extern ccBool r_mouse_cursor;


////////////////////////////////////////////////////////////////////////////////
#define MAX_CON_HISTORY  (512)
#define CONINPUT_SIZE    (128)

extern int convisible;
extern char coninput[CONINPUT_SIZE];
extern int conheight; // default: 16
extern int conbotline; // console bottom line; 0..CMDCON_MAX_LINES-conheight

extern char *conhistory[MAX_CON_HISTORY];
extern int conhistorypos;

extern void conaddchar (char ch);
extern void condelchar (void);
extern void condelword (void);

extern void conHistoryAdd (const char *str);
extern void conhistoryCopy (void);

extern void contabcompletion (void);


////////////////////////////////////////////////////////////////////////////////
// ��������� �����������
typedef struct {
  uint16_t w, h;  // W-������, H-������
  int sx, sy; // ����� ������ ����������� ����� � ����� ��� ������������� ���������
  int wanim; // ��������� ��� ������
  void *rawdata; // �������, �������������
  // ������ �� ������ � �� ������������ �����
  SDL_Surface *sfc;
  SDL_Surface *sfcM; // mirrored
} vgaimg;


////////////////////////////////////////////////////////////////////////////////
typedef void SpriteDrawFn (int x, int y, unsigned char c);


////////////////////////////////////////////////////////////////////////////////
extern ccBool fullscreen;
extern SDL_Surface *screenReal, *screen, *oldscreen, *frameSfc; // oldscreen: for melt effect
extern Uint8 *vscrbuf;
extern Uint8 *scrlightmap; // LMAP_xxx
extern int vidcx1, vidcx2, vidcy1, vidcy2;

#define VGA_BIDX  (0)
#define VGA_GIDX  (1)
#define VGA_RIDX  (2)
//extern int vidRIdx, vidGIdx, vidBIdx;
extern uint8_t palRGB[256][3];
extern Uint32 palette[256]; // converted
//
extern uint8_t main_pal[768], std_pal[768], cur_pal[768];

extern uint8_t bright[256];
extern uint8_t mixmap[256][256];
extern uint8_t clrmap[256*12];

// ������� ������
extern int scrw, scrh, scrx, scry;
extern int vga_xofs, vga_yofs;

extern ccBool shot_vga;

extern const unsigned char vga_font6x8[2048];
extern const unsigned char vga_font8x8[2048];


typedef void (*VGA_preblit_cb) (void);

extern VGA_preblit_cb vga_preblit_cb;


////////////////////////////////////////////////////////////////////////////////
/*
#define ALPHA_MIX(_dr, _dg, _db, _r0, _g0, _b0, _r1, _g1, _b1, _alpha)  do { \
  (_dr) = ((_r0)*(255-(_alpha))+(_r1)*(_alpha))>>8; \
  (_dg) = ((_g0)*(255-(_alpha))+(_g1)*(_alpha))>>8; \
  (_db) = ((_b0)*(255-(_alpha))+(_b1)*(_alpha))>>8; \
} while (0)
*/

#define ALPHA_MIX(_dr, _dg, _db, _r0, _g0, _b0, _r1, _g1, _b1, _alpha)  do { \
  (_db) = ((((_b1)-(_b0))*(_alpha))>>8)+(_b0); \
  (_dg) = ((((_g1)-(_g0))*(_alpha))>>8)+(_g0); \
  (_dr) = ((((_r1)-(_r0))*(_alpha))>>8)+(_r0); \
} while (0)


static inline void pixel_blend32 (uint32_t *d, const uint32_t s) {
  const uint32_t a = ((s>>24)&0xff)+1; // to not loose bits
  const uint32_t srb = (s&0xff00ff);
  const uint32_t sg = (s&0x00ff00);
  const uint32_t drb = ((*d)&0xff00ff);
  const uint32_t dg = ((*d)&0x00ff00);
  const uint32_t orb = (drb+(((srb-drb)*a+0x800080)>>8))&0xff00ff;
  const uint32_t og = (dg+(((sg-dg)*a+0x008000)>>8))&0x00ff00;
  //
  (*d) = (orb|og);
}


////////////////////////////////////////////////////////////////////////////////
// NO LOCKING!
static inline void putPixel32 (int x, int y, Uint32 col) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    *(Uint32 *)(((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4) = col;
  }
}


static inline void putPixelRGB (int x, int y, Uint8 r, Uint8 g, Uint8 b) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    Uint8 *d = (((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4);
    //
    d[VGA_BIDX] = b;
    d[VGA_GIDX] = g;
    d[VGA_RIDX] = r;
  }
}


static inline void putPixel (int x, int y, Uint8 col) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    *(Uint32 *)(((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4) = palette[col];
  }
}


static inline void putPixelC (int x, int y, Uint8 col) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    *(Uint32 *)(((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4) = palette[col];
    vscrbuf[y*SCRW+x] = col;
  }
}


static inline Uint8 getPixelU (int x, int y) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    return vscrbuf[y*SCRW+x];
  }
  return 0;
}


static inline void putPixelMixU (int x, int y, Uint8 pr, Uint8 pg, Uint8 pb, Uint8 alpha) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    Uint8 *d = (((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4);
    Uint32 b = d[VGA_BIDX], g = d[VGA_GIDX], r = d[VGA_RIDX];
    //
    ALPHA_MIX(d[VGA_RIDX], d[VGA_GIDX], d[VGA_BIDX], r, g, b, pr, pg, pb, alpha);
  }
}


//TODO: do clever trick here
static inline Uint8 byteclamp (Uint32 v) { return (v > 255 ? 255 : v); }


static inline void putPixelMixNAU (int x, int y, Uint8 pr, Uint8 pg, Uint8 pb) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    Uint8 *d = (((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4);
    Uint32 b = d[VGA_BIDX], g = d[VGA_GIDX], r = d[VGA_RIDX];
    //
    d[VGA_BIDX] = byteclamp((pb+b)>>1);
    d[VGA_GIDX] = byteclamp((pg+g)>>1);
    d[VGA_RIDX] = byteclamp((pr+r)>>1);
  }
}


static inline void putPixelMixUD (int x, int y, Uint8 pr, Uint8 pg, Uint8 pb, Uint8 alpha) {
  if (frameSfc != NULL && x >= 0 && y >= 0 && x < frameSfc->w && y < frameSfc->h) {
    Uint8 *d = (((Uint8 *)frameSfc->pixels)+(y*frameSfc->pitch)+x*4);
    Uint32 b = d[VGA_BIDX], g = d[VGA_GIDX], r = d[VGA_RIDX];
    //
    if (pr != 0 || pg != 0 || pb != 0) {
      d[VGA_BIDX] = (((pb-b)*alpha)>>8)+b;
      d[VGA_GIDX] = (((pg-g)*alpha)>>8)+g;
      d[VGA_RIDX] = (((pr-r)*alpha)>>8)+r;
    } else {
      d[VGA_BIDX] = (b*alpha)>>8;
      d[VGA_GIDX] = (g*alpha)>>8;
      d[VGA_RIDX] = (r*alpha)>>8;
    }
  }
}


static inline void putpixelMix (int x, int y, Uint8 pr, Uint8 pg, Uint8 pb, Uint8 alpha) {
  if (x >= vidcx1 && x < vidcx2 && y >= vidcy1 && y < vidcy2) putPixelMixUD(x, y, pr, pg, pb, alpha);
}


static inline void V_putpixel (int x, int y, Uint8 color) {
  if (x >= vidcx1 && x < vidcx2 && y >= vidcy1 && y < vidcy2) putPixelC(x, y, color);
}


typedef enum {
  LMAP_FULLLIT,
  LMAP_NORMAL,
  LMAP_BACK,
  LMAP_PROGRESS
} LightMapType;


static inline void V_putpixel_lm (int x, int y, Uint8 color, LightMapType lt) {
  if (x >= vidcx1 && x < vidcx2 && y >= vidcy1 && y < vidcy2) {
    vscrbuf[y*SCRW+x] = color;
    scrlightmap[y*SCRW+x] = lt;
    if (lt == LMAP_FULLLIT) putPixel32(x, y, palette[color]);
  }
}


static inline uint8_t V_getpixel (int x, int y) {
  if (x >= vidcx1 && x < vidcx2 && y >= vidcy1 && y < vidcy2) return getPixelU(x, y);
  return 0;
}


static inline void V_mappixel (int x, int y, const uint8_t *cmap) {
  V_putpixel(x, y, cmap[V_getpixel(x, y)]);
}


////////////////////////////////////////////////////////////////////////////////
extern void palSetColor (unsigned char idx, unsigned char r, unsigned char g, unsigned char b);


////////////////////////////////////////////////////////////////////////////////
extern void V_init (void);
extern void V_done (void);
extern void V_toggle (void); // fullscreen toggle


////////////////////////////////////////////////////////////////////////////////
extern void V_initvga (vgaimg *img);
extern void V_freevga (vgaimg *img);

//WARNING: clears `img`
extern void V_loadvga (const char *name, vgaimg *img); // aborts immideately on error
extern void V_loadvga_ptr (vgaimg *img, const void *vdata);


////////////////////////////////////////////////////////////////////////////////
// ������� �������� i � ����������� (x, y)
extern void V_pic (int x, int y, vgaimg *i);
extern void V_pic1 (int x, int y, vgaimg *i, int semi);

// ������� ������ i � ����������� (x, y) ��� ������ ������� f
extern void V_sprf (int x, int y, vgaimg *i, SpriteDrawFn *f);

extern void smoke_sprf (int x, int y, unsigned char c);
extern void flame_sprf (int x, int y, unsigned char c);

// ������� ������ i � ����������� (x, y)
extern void V_spr (int x, int y, vgaimg *i);
// ������� ��������� ������������ ������ i � ����������� (x, y)
extern void V_spr2 (int x, int y, vgaimg *i);

// ������� ������ i � ����������� (x, y)
extern void V_spr_lm (int x, int y, vgaimg *i);
// ������� ��������� ������������ ������ i � ����������� (x, y)
extern void V_spr2_lm (int x, int y, vgaimg *i);

// ������� ����� ������� i � ����������� (x, y) ����� ������ c
// (�������� ��� ��������� �����)
extern void V_spr1color (int x, int y, vgaimg *i, unsigned char c);

extern void V_manspr (int x, int y, vgaimg *p, unsigned char c);

extern void V_manspr2 (int x, int y, vgaimg *p, unsigned char c);

// �������� ������������� ������ c
// x-����� �������, w-������, y-����, h-������
extern void V_clr (int x, int w, int y, int h, unsigned char c);

// ���������� ������� �� ������� p
extern void VP_setpal (const void *p);

// ���������� n ������, ������� � f, �� ������� p
extern void VP_set (const void *p, int f, int n);

// ���������� ������� ������
extern void V_setrect (int x, int w, int y, int h);

extern void V_remap_rect (int x, int y, int w, int h, const uint8_t *cmap);

// draw 'lightmapped' tile
extern void V_draw_tile_lm (int x, int y, vgaimg *i, int semi);
// draw 'full bright' tile
extern void V_draw_tile (int x, int y, vgaimg *i, int semi);

extern void V_maptoscr (int x, int w, int y, int h, const void *cmap);

extern void V_rotspr (int x, int y, vgaimg *i, int d);
extern void V_center (int f);
//extern void V_offset (int ox, int oy);

extern void V_fadescr (Uint8 alpha);

extern const void *v_endtext; // !NULL: show it overlayed

extern void V_blitscr (void);


////////////////////////////////////////////////////////////////////////////////
extern void polymod_init (void);
extern void polymod_deinit (void);


#endif
