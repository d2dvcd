/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#ifndef D2D_VGAOVL_H
#define D2D_VGAOVL_H

#include "SDL.h"

#include "common.h"
#include "vga.h"


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  int w, h;
  uint8_t *data;
} VOverlay;


extern VOverlay *createVO (int w, int h);
extern void freeVO (VOverlay *v);

extern void clearVO (VOverlay *v, Uint8 c);

extern void drawChar6VO (VOverlay *v, char ch, int x, int y, Uint8 fg, Uint8 bg);
extern void drawStr6VO (VOverlay *v, const char *str, int x, int y, Uint8 fg, Uint8 bg);
extern void drawChar8VO (VOverlay *v, char ch, int x, int y, Uint8 fg, Uint8 bg);
extern void drawStr8VO (VOverlay *v, const char *str, int x, int y, Uint8 fg, Uint8 bg);

extern void drawFrameVO (VOverlay *v, int x0, int y0, int w, int h, Uint8 c);
extern void fillRectVO (VOverlay *v, int x0, int y0, int w, int h, Uint8 c);

extern void blitVO (VOverlay *v, int x0, int y0, Uint8 alpha);


extern VOverlay *createVOFromIcon (const void *iconbuf);


enum {
  VGA_ICON_PENTA = 0, // 24x24
  VGA_ICON_TURTLE = 1, // 32x32
  VGA_ICON_RAM = 2 // 32x32
};

extern VOverlay *createVOFromStockIcon (int icn);


static inline void putPixelVO (VOverlay *v, int x, int y, Uint8 c) {
  if (v != NULL && x >= 0 && y >= 0 && x < v->w && y < v->h && c != 255) v->data[y*v->w+x] = c;
}


////////////////////////////////////////////////////////////////////////////////
// rectangle that *includes* right and bottom edges
typedef struct {
  int vClipX0, vClipY0;
  int vClipX1, vClipY1;
  int vOfsX, vOfsY;
} VClipInfo;


typedef struct {
  VOverlay *vo;
  VClipInfo clip;
} VExSurface;


////////////////////////////////////////////////////////////////////////////////
static inline void vPutPixel (const VExSurface *sfc, int x, int y, Uint8 clr) {
  if (sfc != NULL && sfc->vo != NULL) {
    x += sfc->clip.vOfsX;
    y += sfc->clip.vOfsY;
    if (x >= sfc->clip.vClipX0 && y >= sfc->clip.vClipY0 &&
        x <= sfc->clip.vClipX1 && y <= sfc->clip.vClipY1) putPixelVO(sfc->vo, x, y, clr);
  }
}


////////////////////////////////////////////////////////////////////////////////
static inline void vResetClip (VExSurface *sfc) {
  if (sfc != NULL) {
    sfc->clip.vClipX0 = sfc->clip.vClipY0 = 0;
    sfc->clip.vClipX1 = sfc->vo->w-1;
    sfc->clip.vClipY1 = sfc->vo->h-1;
    sfc->clip.vOfsX = sfc->clip.vOfsY = 0;
  }
}


static inline void vSetClip (VExSurface *sfc, int x, int y, int w, int h, int ox, int oy) {
  if (sfc != NULL) {
    if (w < 0) w = 0;
    if (h < 0) h = 0;
    sfc->clip.vClipX0 = x;
    sfc->clip.vClipY0 = y;
    sfc->clip.vClipX1 = x+w-1;
    sfc->clip.vClipY1 = y+h-1;
    sfc->clip.vOfsX = ox;
    sfc->clip.vOfsY = oy;
  }
}


static inline void vSaveClip (const VExSurface *sfc, VClipInfo *ci) {
  if (sfc != NULL && ci != NULL) *ci = sfc->clip;
}


static inline void vRestoreClip (VExSurface *sfc, const VClipInfo *ci) {
  if (sfc != NULL && ci != NULL) sfc->clip = *ci;
}


static inline void vRestrictClip (VExSurface *sfc, int x, int y, int w, int h) {
  if (sfc != NULL) {
    if (w < 1 || h < 1 || x > sfc->clip.vClipX1 || y > sfc->clip.vClipY1) {
      sfc->clip.vClipX1 = sfc->clip.vClipX0-1;
      sfc->clip.vClipY1 = sfc->clip.vClipY0-1;
    } else {
      if (sfc->clip.vClipX0 < x) sfc->clip.vClipX0 = x;
      if (sfc->clip.vClipY0 < y) sfc->clip.vClipY0 = y;
      x += w-1;
      y += h-1;
      if (sfc->clip.vClipX1 > x) sfc->clip.vClipX1 = x;
      if (sfc->clip.vClipY1 > y) sfc->clip.vClipY1 = y;
    }
  }
}


static inline void vChangeClip (VExSurface *sfc, int dhor, int dvert) {
  if (sfc != NULL) {
    sfc->clip.vClipX0 += dhor;
    sfc->clip.vClipY0 += dvert;
    sfc->clip.vClipX1 -= dhor;
    sfc->clip.vClipY1 -= dvert;
  }
}


////////////////////////////////////////////////////////////////////////////////
extern int vLineCountMaxLen (const char *str, int *maxlenp);

extern void vDrawChar (VExSurface *sfc, int x, int y, char ch, Uint8 ink, Uint8 paper);
extern void vDrawText (VExSurface *sfc, int x, int y, const char *str, Uint8 ink, Uint8 paper);
extern void vDrawOutlineText (VExSurface *sfc, int x, int y, const char *str, Uint8 ink, int oink);

////////////////////////////////////////////////////////////////////////////////
extern void vDrawHLine (VExSurface *sfc, int x, int y, int len, Uint8 clr);
extern void vDrawVLine (VExSurface *sfc, int x, int y, int len, Uint8 clr);
extern void vDrawBar (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr);
extern void vDrawRect (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr);
extern void vDrawRectW (VExSurface *sfc, int x, int y, int width, int height, Uint8 clr);

////////////////////////////////////////////////////////////////////////////////
extern void vDrawZXStripe (VExSurface *sfc, int x, int y, int dim);

enum {
  ZXVIDWF_STRIPES = 0x01,
  ZXVIDWF_STRIPES_DIM = 0x02, // dim colors
};

extern void vDrawWindow (VExSurface *sfc, int x, int y, int width, int height, const char *title,
  int tink, int tpaper, int wpaper, int flags);

////////////////////////////////////////////////////////////////////////////////
extern void vDrawUpArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper);
extern void vDrawDownArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper);
extern void vDrawLeftArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper);
extern void vDrawRightArrow (VExSurface *sfc, int x, int y, Uint8 ink, Uint8 paper);

////////////////////////////////////////////////////////////////////////////////
extern void vDrawLine (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr);
extern void vDrawCircle (VExSurface *sfc, int cx, int cy, int radius, Uint8 clr);
extern void vDrawEllipse (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr);
extern void vDrawFilledCircle (VExSurface *sfc, int cx, int cy, int radius, Uint8 clr);
extern void vDrawFilledEllipse (VExSurface *sfc, int x0, int y0, int x1, int y1, Uint8 clr);

////////////////////////////////////////////////////////////////////////////////
extern void vDrawSelectionRect (VExSurface *sfc, int phase, int x0, int y0, int wdt, int hgt, Uint8 col0, Uint8 col1);


////////////////////////////////////////////////////////////////////////////////
// there are many predefined cursors...
enum {
  AID_DEFAULT = -1, // AID_COOKE
  AID_BIG = 0,
  AID_LESSER,
  AID_UGLY,
  AID_UGLYIER,
  AID_TINY_BGE,
  AID_COOKE,
  AID_ARTSTUDIO,
  AID_SMALL_BYTEX,
  AID_UGLY_EXCESS,
  AID_EMPTY
};

extern Uint8 msCurClr0, msCurClr1;

extern void drawMouseCursor (int x, int y, int id);

extern void drawInn (int x, int y, uint8_t phase);


#endif
