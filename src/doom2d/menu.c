/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "SDL.h"

#include "common.h"
#include "cmdcon.h"
#include "error.h"
#include "files.h"
#include "game.h"
#include "gamemode.h"
#include "jimapi.h"
#include "keyb.h"
#include "menu.h"
#include "monster.h"
#include "misc.h"
#include "player.h"
#include "saveload.h"
#include "screenshot.h"
#include "sound.h"
#include "switch.h"
#include "vga.h"
#include "view.h"


////////////////////////////////////////////////////////////////////////////////
static const uint8_t cursmall_vga[43] = {
  0x05, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x6C, 0x6C, 0x00, 0x00, 0x00, 0x6C,
  0x60, 0x6C, 0x00, 0x00, 0x6C, 0x5D, 0x58, 0x6C, 0x00, 0x6C, 0x5C, 0x54, 0x50, 0x6C,
  0x6C, 0x5D, 0x58, 0x6C, 0x00, 0x6C, 0x60, 0x6C, 0x00, 0x00, 0x6C, 0x6C, 0x00, 0x00,
  0x00
};


////////////////////////////////////////////////////////////////////////////////
typedef void (*menu_item_init_cb) (menu_item_t *mi); // >0: key eaten; lastkey is key
typedef int (*menu_item_key_cb) (menu_item_t *mi); // >0: key eaten; lastkey is key
typedef int (*menu_item_enter_cb) (menu_item_t *mi); // >0: key eaten
typedef int (*menu_item_esc_cb) (menu_item_t *mi); // >0: key eaten
typedef int (*menu_item_width_cb) (menu_item_t *mi);
typedef int (*menu_item_height_cb) (menu_item_t *mi);
typedef void (*menu_item_draw_cb) (menu_item_t *mi, int y);

typedef void (*menu_init_cb) (menu_t *mnu);


struct menu_item_t {
  menu_t *enter_menu;
  const char *text[2]; // starts with '\1': use small font; english and russian
  int height; // <=0: use corresponding font height
  int x; // x position
  menu_item_init_cb init_cb;
  menu_item_key_cb key_cb; // can be NULL
  menu_item_enter_cb enter_cb; // can be NULL
  menu_item_enter_cb esc_cb; // can be NULL
  menu_item_width_cb width_cb; // can be NULL
  menu_item_height_cb height_cb; // can be NULL
  menu_item_draw_cb draw_cb; // can be NULL
  const char *saysnd;
  const char *concmd;
  int clear_menu; // !0: do GM_set(NULL)
  void *udata;
  // never set this!
  int inited;
  int orig_x, orig_height;
};


struct menu_t {
  //D2DMenuType type;
  menu_t *esc_menu;
  const char *title[2]; // menu title or message
  const char *saysnd;
  menu_init_cb init_cb;
  menu_item_t *items;
  int item_count; // number of items in this menu; <=0: count
  int curpos; // remembered cursor position
  int x; // title x position; 0: center menu
  int y; // menu y position; 0: center menu
  int defcurposp1; // >0: reset curpos to defcurposp1-1 on init
  void *udata;
  // never set this!
  int inited;
  int orig_x, orig_y;
};


////////////////////////////////////////////////////////////////////////////////
#define QSND_NUM  (14)


////////////////////////////////////////////////////////////////////////////////
int lastkey = 0;
int lastkeychar = 0;
SDL_KeyboardEvent lastkeyevt;

static const uint8_t panim[]= "BBDDAACCDDAABBDDAACCDDAABBDDAACCDDAAEEEEEFEFEFEFEFEFEFEFEFEFEEEEE";
static __attribute__((unused)) const uint8_t *panimp = panim;

#define PCOLORN  (10)
static const uint8_t pcolortab[PCOLORN] = {
  0x18, 0x20, 0x40, 0x58, 0x60, 0x70, 0x80, 0xB0, 0xC0, 0xD0
};
static int p1color = 5, p2color = 4;

static char ibuf[256];
static uint8_t input = 0;
static int icur, imax;


static const int kmap[] = {
  KB_LEFT,
  KB_RIGHT,
  KB_UP,
  KB_DOWN,
  KB_FIRE,
  KB_USE,
  KB_JUMP,
  KB_WPNPREV,
  KB_WPNNEXT,
};


static menu_t *mnu = NULL;
static uint8_t gm_redraw = 0;
static int gm_tm = 0;

static int qsnd[QSND_NUM];
static const snd_t *csnd1, *csnd2, *msnd1, *msnd2, *msnd3, *msnd4, *msnd5, *msnd6;
static int movsndt = 0;
static vgaimg msklh[2], mbarl, mbarm, mbarr, mbaro, mslotl, mslotm, mslotr, cursmall;
static uint8_t cbuf[32];

static snd_t *voc = NULL;
static int voc_ch = -1;

static int ctrl_plr_num = 0;
static int ctrl_key_alt = -1; // <0: not waiting for a key
int gm_cur_lang = LANG_EN;


////////////////////////////////////////////////////////////////////////////////
CONVAR_INT_CB(gm_cur_lang, gm_cur_lang, CMDCON_FLAGS_PERSISTENT, "current language id") {
  if (gm_cur_lang < 0) gm_cur_lang = 0;
  else if (gm_cur_lang >= LANG_MAX) gm_cur_lang = LANG_MAX-1;
  //
  GM_set(mnu);
  evt_language_changed();
}


////////////////////////////////////////////////////////////////////////////////
static const struct {
  int keysym;
  uint8_t ch;
} keychar[] = {
  {SDLK_SPACE, ' '},
  {SDLK_0, '0'},
  {SDLK_1, '1'},
  {SDLK_2, '2'},
  {SDLK_3, '3'},
  {SDLK_4, '4'},
  {SDLK_5, '5'},
  {SDLK_6, '6'},
  {SDLK_7, '7'},
  {SDLK_8, '8'},
  {SDLK_9, '9'},
  {SDLK_UNDERSCORE, '_'},
  {SDLK_a, 'A'},
  {SDLK_b, 'B'},
  {SDLK_c, 'C'},
  {SDLK_d, 'D'},
  {SDLK_e, 'E'},
  {SDLK_f, 'F'},
  {SDLK_g, 'G'},
  {SDLK_h, 'H'},
  {SDLK_i, 'I'},
  {SDLK_j, 'J'},
  {SDLK_k, 'K'},
  {SDLK_l, 'L'},
  {SDLK_m, 'M'},
  {SDLK_n, 'N'},
  {SDLK_o, 'O'},
  {SDLK_p, 'P'},
  {SDLK_q, 'Q'},
  {SDLK_r, 'R'},
  {SDLK_s, 'S'},
  {SDLK_t, 'T'},
  {SDLK_u, 'U'},
  {SDLK_v, 'V'},
  {SDLK_w, 'W'},
  {SDLK_x, 'X'},
  {SDLK_y, 'Y'},
  {SDLK_z, 'Z'},
  {SDLK_COMMA, ','},
};


static uint8_t get_keychar (int keysym) {
  for (size_t f = 0; f < sizeof(keychar)/sizeof(keychar[0]); ++f) {
    if (keychar[f].keysym == keysym) return keychar[f].ch;
  }
  return 0;
}


static void G_keyf (SDL_KeyboardEvent *key, int press) {
  if (press) {
    lastkeyevt = *key;
    lastkey = key->keysym.sym;
    lastkeychar = key->keysym.unicode;
    if (!convisible) {
      if (g_plrcount == 1 || g_cheats_enabled) {
        for (int f = 0; f < 31; ++f) cbuf[f] = cbuf[f+1];
        cbuf[31] = get_keychar(lastkey);
      }
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
void GM_init (void) {
  static const char nm[QSND_NUM][6] = {
    "CYBSIT", "KNTDTH", "MNPAIN", "PEPAIN", "SLOP", "MANSIT", "BOSPN", "VILACT",
    "PLFALL", "BGACT", "BGDTH2", "POPAIN", "SGTATK", "VILDTH"
  };
  char s[8];
  //
  s[0] = 'D';
  s[1] = 'S';
  for (int f = 0; f < QSND_NUM; ++f) {
    memcpy(s+2, nm[f], 6);
    qsnd[f] = F_getresid(s);
  }
  csnd1 = Z_getsnd("HAHA1");
  csnd2 = Z_getsnd("RADIO");
  msnd1 = Z_getsnd("PSTOP");
  msnd2 = Z_getsnd("PISTOL");
  msnd3 = Z_getsnd("SWTCHN");
  msnd4 = Z_getsnd("SWTCHX");
  msnd5 = Z_getsnd("SUDI");
  msnd6 = Z_getsnd("TUDI");
  V_loadvga("M_SKULL1", &msklh[0]);
  V_loadvga("M_SKULL2", &msklh[1]);
  V_loadvga("M_THERML", &mbarl);
  V_loadvga("M_THERMM", &mbarm);
  V_loadvga("M_THERMR", &mbarr);
  V_loadvga("M_THERMO", &mbaro);
  V_loadvga("M_LSLEFT", &mslotl);
  V_loadvga("M_LSCNTR", &mslotm);
  V_loadvga("M_LSRGHT", &mslotr);
  V_loadvga_ptr(&cursmall, cursmall_vga);
  K_setkeyproc(G_keyf);
}


////////////////////////////////////////////////////////////////////////////////
static menu_t menu_game_type;
static menu_t menu_game_type_network;
static menu_t menu_main;
static menu_t menu_main_ingame;
static menu_t menu_quit[3];
static menu_t menu_game_options;
static menu_t menu_gamma;
static menu_t menu_volume;
static menu_t menu_game_players;


////////////////////////////////////////////////////////////////////////////////
static void enter_input_state (const char *text) {
  input = 1;
  icur = 0;
  memset(ibuf, 0, sizeof(ibuf));
  if (text != NULL) strncpy(ibuf, text, sizeof(ibuf)-1);
  icur = strlen(ibuf);
}


static void cancel_input_state (void) {
  input = 0;
}


////////////////////////////////////////////////////////////////////////////////
#define MENU_TITLE_OFS  (8)


static int get_menu_item_height (menu_item_t *i) {
  if (i->height == 0) {
    if (i->height_cb != NULL) {
      i->height = i->height_cb(i);
    } else {
      i->height = (i->text[gm_cur_lang] != NULL && i->text[gm_cur_lang][0] != '\1' ? 16 : 8);
    }
  }
  return i->height;
}


static int get_menu_item_width (menu_item_t *i) {
  if (i->width_cb != NULL) return i->width_cb(i);
  if (i->text[gm_cur_lang] != NULL) {
    if (i->text[gm_cur_lang][0] != '\1') return Z_strfwidthbf(i->text[gm_cur_lang]);
    return Z_strfwidthsf(i->text[gm_cur_lang]+1);
  }
  return 0;
}


static int get_menu_item_count (menu_t *mnu) {
  if (mnu->item_count == 0) {
    for (const menu_item_t *i = mnu->items; i->height != -666; ++i) ++mnu->item_count;
  }
  return mnu->item_count;
}


static int get_menu_item_pos (menu_t *m, const menu_item_t *i) {
  for (int f = 0; f < get_menu_item_count(m); ++f) if (&m->items[f] == i) return f;
  return -1;
}


static int get_menu_y (menu_t *mnu) {
  if (mnu->y == 0) {
    int hgt = 0;
    //
    if (mnu->title[gm_cur_lang] != NULL) {
      hgt = (mnu->title[gm_cur_lang][0] != '\1' ? 16 : 8)+MENU_TITLE_OFS;
    }
    //
    for (int f = 0; f < mnu->item_count; ++f) hgt += get_menu_item_height(&mnu->items[f]);
    //
    mnu->y = (200-hgt)/2;
  }
  //
  return mnu->y;
}


static int get_menu_x (menu_t *mnu) {
  if (mnu->x == 0) {
    int iwdt = 0, titwdt = 0;
    //
    if (mnu->title[gm_cur_lang] != NULL) {
      if (mnu->title[gm_cur_lang][0] != '\1') titwdt = Z_strfwidthbf(mnu->title[gm_cur_lang]);
      else titwdt = Z_strfwidthsf(mnu->title[gm_cur_lang]+1);
    }
    //
    for (int f = 0; f < mnu->item_count; ++f) {
      menu_item_t *i = &mnu->items[f];
      //
      iwdt = K8MAX(iwdt, get_menu_item_width(i));
    }
    mnu->x = (320-titwdt)/2;
    //
    for (menu_item_t *i = mnu->items; i->height != -666; ++i) if (i->x == 0) i->x = (320-iwdt)/2;
  }
  //
  return mnu->x;
}


static void menu_initialize (menu_t *mnu) {
  if (mnu != NULL) {
    if (mnu->init_cb != NULL) mnu->init_cb(mnu);
    if (mnu->inited) {
      // restore values
      mnu->x = mnu->orig_x;
      mnu->y = mnu->orig_y;
    } else {
      // save values
      mnu->inited = 1;
      mnu->orig_x = mnu->x;
      mnu->orig_y = mnu->y;
    }
    //
    get_menu_item_count(mnu);
    for (int f = 0; f < mnu->item_count; ++f) {
      menu_item_t *i = &mnu->items[f];
      //
      if (i->inited) {
        // restore values
        i->x = i->orig_x;
        i->height = i->orig_height;
      } else {
        // save values
        i->inited = 1;
        i->orig_x = i->x;
        i->orig_height = i->height;
      }
      //
      if (i->init_cb != NULL) i->init_cb(i);
      get_menu_item_width(i);
      get_menu_item_height(i);
    }
    get_menu_x(mnu);
    get_menu_y(mnu);
    if (mnu->defcurposp1 > 0) mnu->curpos = mnu->defcurposp1-1;
  }
}


////////////////////////////////////////////////////////////////////////////////
GCC_DESTRUCTOR_USER {
  if (voc != NULL) free(voc);
}


static void GMV_stop (void) {
  if (voc != NULL) {
    if (voc_ch >= 0) {
      S_stop(voc_ch);
      voc_ch = -1;
    }
    free(voc);
    voc = NULL;
  }
}


static void GMV_say (const char *nm) {
  if (nm != NULL && nm[0]) {
    int r;
    //
    if ((r = F_findres(nm)) != -1) {
      int len;
      snd_t *p;
      //
      if (!(p = malloc((len = F_getreslen(r))+sizeof(snd_t)))) return;
      p->len = len;
      p->rate = 11025;
      p->lstart = p->llen = 0;
      GMV_stop();
      F_loadres(r, p+1, 0, len);
      for (uint8_t *d = (uint8_t *)(p+1); len; --len, ++d) *d ^= 128;
      voc = p;
      voc_ch = S_play(voc, -1, 1024, 255);
    } else {
      GMV_stop();
    }
  } else {
    GMV_stop();
  }
}


static inline int isCheat (const char *s) {
  int l = strlen(s);
  const char *cb = (const char *)cbuf+32-l;
  //
  for (; l > 0; --l, ++cb, ++s) {
    if (s[0] == '#') {
      if (cb[0] < '0' || cb[0] > '9') return 0;
    } else if (s[0] != cb[0]) {
      return 0;
    }
  }
  //
  return 1;
}


#include "cheats.c"


void GM_set (menu_t *m) {
  mnu = m;
  gm_redraw = 1;
  if (m != NULL) menu_initialize(m);
}


void GM_update (menu_t *m) {
  if (m != NULL) gm_redraw = 1;
}


void M_init_players (void) {
  PL_reset();
  if (g_plrcount == 2) {
    pl[0].color = pcolortab[p1color];
    pl[1].color = pcolortab[p2color];
  } else {
    pl[0].color = 0x70;
  }
}


CONCMD(restart, "restart current level") {
  CMDCON_HELP();
  //
  if (g_st == GS_GAME) {
    N_disconnect();
    demo_done();
    PL_reset();
    G_start();
    GM_set(NULL);
  } else {
    conlogf("%s: can't restart nothing!\n", argv[0]);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void menu_do_enter (menu_item_t *mi, int skipcb) {
  int doboom = 1;
  menu_t *curmnu = mnu;
  //
  if (!skipcb && mi->enter_cb != NULL) { mi->enter_cb(mi); return; }
  //
  if (mi->saysnd != NULL && (mi->enter_menu == NULL || mi->enter_menu->saysnd == NULL)) {
    GMV_say(mi->saysnd);
    doboom = 0;
  }
  //
  if (mi->enter_menu != NULL) {
    mi->enter_menu->esc_menu = curmnu;
    GM_set(mi->enter_menu);
    if (mi->enter_menu->saysnd != NULL) {
      GMV_say(mi->enter_menu->saysnd);
      doboom = 0;
    }
  }
  //
  if (doboom) Z_sound(msnd1, 128);
  //
  if (mi->concmd != NULL) {
    cmdcon_exec(mi->concmd);
    GM_set(mnu);
  }
  if (mi->clear_menu) GM_set(NULL);
}


// pos: [0..132]
static void menu_draw_slider (menu_item_t *mi, int y, int pos) {
  V_spr(mi->x, y, &mbarl);
  for (int k = 8; k < 144; k += 8) V_spr(mi->x+k, y, &mbarm);
  V_spr(mi->x+144, y, &mbarr);
  V_spr(mi->x+8+pos, y, &mbaro);
}


static int menu_slider_width (menu_item_t *mi) {
  return 152;
}


////////////////////////////////////////////////////////////////////////////////
static int menu_quit_enter (menu_item_t *mi) {
  int idx = myrand(3);
  //
  for (size_t f = 0; f < sizeof(menu_quit)/sizeof(menu_quit[0]); ++f) if (mnu == &menu_quit[f]) return 1;
  GMV_say((idx&0x1 ? "_EXIT1" : "_EXIT2"));
  menu_quit[idx].esc_menu = mnu;
  GM_set(&menu_quit[idx]);
  mnu->curpos = 0;
  return 1;
}


static int menu_yes_no_keys (menu_item_t *mi) {
  switch (lastkey) {
    case SDLK_y: case SDLK_t: mnu->curpos = 0; break;
    case SDLK_n: case SDLK_o: mnu->curpos = 1; break;
    default: return 0;
  }
  menu_do_enter(&mnu->items[mnu->curpos], 0);
  return 1;
}


static int menu_yes_no_no (menu_item_t *mi) {
  GMV_say(NULL);
  Z_sound(msnd1, 128);
  GM_set(mnu->esc_menu);
  return 1;
}


static int menu_yes_no_yes (menu_item_t *mi) {
  GMV_say(NULL);
  if (mi->concmd != NULL) cmdcon_exec(mi->concmd);
  if (mi->enter_menu != NULL) GM_set(mi->enter_menu);
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_quit_items[] = {
  {
    .text = {"\1YES", "\1��"},
    .key_cb = menu_yes_no_keys,
    .enter_cb = menu_yes_no_yes,
    .concmd = "gcmd_do_quit",
  },
  {
    .text = {"\1NO", "\1���"},
    .key_cb = menu_yes_no_keys,
    .enter_cb = menu_yes_no_no,
  },
  {.height=-666},
};

static menu_t menu_quit[3] = {
  {
    .title = {"\1DO YOU THINK YOU CAN QUIT SO EASY?", "\1�� �������, ������ ��� ������ �����?"}, //FIXME: ENGILISH!
    .items = menu_quit_items,
    .defcurposp1 = 1,
  },
  {
    .title = {"\1DID YOU THINK TWICE BEFORE QUITING?", "\1���������� ��������� ����� ��� ��� �����"}, //FIXME: ENGILISH!
    .items = menu_quit_items,
    .defcurposp1 = 1,
  },
  {
    .title = {"\1ARE YOU OUT OF AMMO YET?", "\1� ��� ���, ��������� �������?"}, //FIXME: ENGILISH!
    .items = menu_quit_items,
    .defcurposp1 = 1,
  },
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_restart_yesno_items[] = {
  {
    .text = {"\1YES", "\1��"},
    .key_cb = menu_yes_no_keys,
    .enter_cb = menu_yes_no_yes,
    .concmd = "gcmd_restart_level",
  },
  {
    .text = {"\1NO", "\1���"},
    .key_cb = menu_yes_no_keys,
    .enter_cb = menu_yes_no_no,
  },
  {.height=-666},
};

static menu_t menu_restart_yesno = {
  .title = {"\1RESTART CURRENT LEVEL?", "\1������ ������?"},
  .items = menu_restart_yesno_items,
  .defcurposp1 = 1,
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_end_yesno_items[] = {
  {
    .text = {"\1YES", "\1��"},
    .key_cb = menu_yes_no_keys,
    .enter_cb = menu_yes_no_yes,
    .concmd = "gcmd_end_game",
  },
  {
    .text = {"\1NO", "\1���"},
    .key_cb = menu_yes_no_keys,
    .enter_cb = menu_yes_no_no,
  },
  {.height=-666},
};

static menu_t menu_end_yesno = {
  .title = {"\1END CURRENT LEVEL?", "\1��������� ����?"},
  .items = menu_end_yesno_items,
  .defcurposp1 = 1,
};


////////////////////////////////////////////////////////////////////////////////
static int menu_controls_width (menu_item_t *mi) {
  return 180;
}


#define MCC_INFO \
  int kidx = get_menu_item_pos(mnu, mi); \
  int plr = (intptr_t)mnu->udata; \
  int waiting_key = ((plr&0x80) != 0); \
  int is_alt = ((plr>>4)&0x01); \
  plr &= 0x0f;

#define MCC_RESET_WAIT  mnu->udata = (void *)(plr|(is_alt<<4))
#define MCC_SET_WAIT    mnu->udata = (void *)(plr|0x80|(is_alt<<4))
#define MCC_SETKEY(_v)  pl[plr].kb[kmap[kidx]][is_alt] = (_v)
#define MCC_GETKEY      (pl[plr].kb[kmap[kidx]][is_alt])
static void menu_controls_draw (menu_item_t *mi, int y) {
  int need_comma = 0;
  MCC_INFO
  //
  Z_gotoxy(mi->x, y);
  Z_printsf("%s", mi->text[gm_cur_lang]+1);
  //
  Z_gotoxy(mi->x+92, y);
  for (int alt = 0; alt <= 1; ++alt) {
    int kk = pl[plr].kb[kmap[kidx]][alt];
    //
    if ((is_alt == alt && kidx == mnu->curpos) || kk > 0) {
      const char *s = (kk > 0 ? SDL_GetKeyName(kk) : "???");
      //
      if (need_comma) Z_printsf(", "); else need_comma = 1;
      if (is_alt != alt || kidx != mnu->curpos || !waiting_key || (gm_tm/6)&1) {
        Z_printsf("%s", s);
      } else {
        z_print_x += Z_strfwidthsf("%s", s);
      }
    }
  }
}


static int menu_controls_keys (menu_item_t *mi) {
  MCC_INFO
  //
  switch (lastkey) {
    case SDLK_ESCAPE:
      if (waiting_key) {
        MCC_RESET_WAIT;
        break;
      }
      return 0;
    case SDLK_RETURN: case SDLK_KP_ENTER:
      if (waiting_key) {
        MCC_RESET_WAIT;
        MCC_SETKEY(lastkey);
      } else {
        is_alt = 0;
        MCC_SET_WAIT;
      }
      break;
    case SDLK_SPACE:
      if (waiting_key) {
        MCC_RESET_WAIT;
        MCC_SETKEY(lastkey);
      } else {
        is_alt = 1;
        MCC_SET_WAIT;
      }
      break;
    case SDLK_DELETE: case SDLK_KP_PERIOD:
      if (waiting_key) {
        MCC_RESET_WAIT;
        MCC_SETKEY(lastkey);
      } else {
        pl[plr&0x0f].kb[kmap[kidx]][0] = pl[plr&0x0f].kb[kmap[kidx]][1] = -1;
      }
      break;
    case SDLK_F1 ... SDLK_F15:
      // this keys are forbidden
      break;
    default:
      if (waiting_key && lastkey) {
        MCC_RESET_WAIT;
        MCC_SETKEY(lastkey);
        break;
      }
      return 0;
  }
  return 1;
}
#undef MCC_SETKEY
#undef MCC_SET_WAIT
#undef MCC_RESET_WAIT
#undef MCC_INFO


static menu_item_t menu_controls_items[] = {
  {
    .text = {"\1LEFT", "\1�����"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1RIGHT", "\1������"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1UP", "\1�����"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1DOWN", "\1����"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1FIRE", "\1�����"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1USE", "\1������������"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1JUMP", "\1������"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1PREV WEAPON", "\1����. ������"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {
    .text = {"\1NEXT WEAPON", "\1����. ������"},
    .key_cb = menu_controls_keys,
    .width_cb = menu_controls_width,
    .draw_cb = menu_controls_draw,
  },
  {.height=-666},
};

static menu_t menu_controls = {
  .title = {"CONTROLS", "����������"},
  .items = menu_controls_items,
  .defcurposp1 = 1,
};


////////////////////////////////////////////////////////////////////////////////
static int menu_saveload_width (menu_item_t *mi) {
  return 180;
}


static void menu_saveload_draw (menu_item_t *mi, int y) {
  int x = mi->x, j, idx = get_menu_item_pos(mnu, mi);
  //
  V_spr(x, (j = y+9), &mslotl);
  for (int k = 8; k < 184; k += 8) V_spr(x+k, j, &mslotm);
  V_spr(x+184, j, &mslotr);
  Z_gotoxy(x+4, j-8);
  //
  if (input && idx == mnu->curpos) Z_printsf("%s_", ibuf);
  else Z_printsf("%s", savname[idx]);
}


static int menu_loaditem_enter_cb (menu_item_t *mi) {
  int idx = get_menu_item_pos(mnu, mi);
  //
  if (savok[idx]) {
    char cmd[128];
    //
    sprintf(cmd, "load_game %d", idx);
    cmdcon_exec(cmd);
    GM_set(NULL);
  }
  //
  return 1;
}


static int menu_saveitem_enter_cb (menu_item_t *mi) {
  int idx = get_menu_item_pos(mnu, mi);
  //
  if (input) {
    cancel_input_state();
    F_savegame(idx, ibuf);
    GM_set(NULL);
  } else {
    enter_input_state(savname[idx]);
    imax = 23;
  }
  //
  return 1;
}


static void menu_game_load_init (menu_t *mnu) {
  menu_item_t *items = mnu->items;
  //
  F_getsavnames();
  for (int f = 0; f < 8; ++f) {
    items[f].text[0] = items[f].text[1] = savname[f];
    items[f].enter_cb = menu_loaditem_enter_cb;
  }
}


static void menu_game_save_init (menu_t *mnu) {
  menu_item_t *items = mnu->items;
  //
  F_getsavnames();
  for (int f = 0; f < 8; ++f) {
    items[f].text[0] = items[f].text[1] = savname[f];
    items[f].enter_cb = menu_saveitem_enter_cb;
  }
}


static menu_item_t menu_saveload_items[] = {
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  { .height = 16, .width_cb = menu_saveload_width, .draw_cb = menu_saveload_draw, .clear_menu=1 },
  {.height=-666},
};


static menu_t menu_game_load = {
  .title = {"LOAD GAME", "������ ����"},
  .saysnd = "_OLDGAME",
  .init_cb = menu_game_load_init,
  .items = menu_saveload_items,
};


static menu_t menu_game_save = {
  .title = {"SAVE GAME", "��������� ����"},
  .saysnd = "_SAVEGAM",
  .init_cb = menu_game_save_init,
  .items = menu_saveload_items,
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_main_items[] = {
  {
    .enter_menu = &menu_game_players,
    .text = {"NEW GAME", "����� ����"},
  },
  {
    .enter_menu = &menu_game_load,
    .text = {"LOAD GAME", "������ ����"},
  },
  {
    .enter_menu = &menu_game_options,
    .text = {"OPTIONS", "������"},
  },
  {
    .text = {"QUIT", "�����"},
    .enter_cb = menu_quit_enter,
    .concmd = "gcmd_quit",
  },
  {.height=-666},
};

static menu_t menu_main = {
  .title = {"MAIN MENU", "����"},
  .items = menu_main_items,
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_main_ingame_items[] = {
  {
    .enter_menu = &menu_restart_yesno,
    .text = {"RESTART LEVEL", "������ ������"},
  },
  {
    .enter_menu = &menu_game_save,
    .text = {"SAVE GAME", "��������� ����"},
  },
  {
    .enter_menu = &menu_game_load,
    .text = {"LOAD GAME", "������ ����"},
  },
  {
    .enter_menu = &menu_game_options,
    .text = {"OPTIONS", "������"},
  },
  {
    .enter_menu = &menu_end_yesno,
    .text = {"END_GAME", "��������� ����"},
  },
  {
    .text = {"QUIT", "�����"},
    .enter_cb = menu_quit_enter,
    .concmd = "gcmd_quit",
  },
  {.height=-666},
};

static menu_t menu_main_ingame = {
  .title = {"MAIN MENU", "����"},
  .items = menu_main_ingame_items,
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_game_players_items[] = {
  {
    .text = {"ONE PLAYER", "���� �����"},
    .saysnd = "_1PLAYER",
    .concmd = "gcmd_new_game_1pl",
  },
  {
    .enter_menu = &menu_game_type,
    .text = {"TWO PLAYERS", "��� ������"},
    .saysnd = "_2PLAYER",
  },
  {
    .enter_menu = &menu_game_type_network,
    .text = {"NETWORK GAME", "������� ����"},
  },
  {.height=-666},
};

static menu_t menu_game_players = {
  .title = {"NEW GAME", "����� ����"},
  .saysnd = "_NEWGAME",
  .items = menu_game_players_items,
  .defcurposp1 = 1,
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_controls_plsel_items[] = {
  {
    .text = {"FIRST PLAYER", "������ �����"},
    .concmd = "gcmd_controls 1",
  },
  {
    .text = {"SECOND PLAYER", "������ �����"},
    .concmd = "gcmd_controls 2",
  },
  {
    .text = {"SWAP CONTROLS", "�������� �������"},
    .concmd = "gcmd_swap_controls",
  },
  {.height=-666},
};

static menu_t menu_controls_plsel = {
  .title = {"CONTROLS", "����������"},
  .items = menu_controls_plsel_items,
};


////////////////////////////////////////////////////////////////////////////////
static void menu_item_slang_init (menu_item_t *mi) {
  if ((intptr_t)mi->udata == gm_cur_lang) {
    int idx = get_menu_item_pos(mnu, mi);
    //
    if (idx >= 0) mnu->curpos = idx;
  }
}


static menu_item_t menu_select_lang_items[] = {
  {
    .text = {"ENGLISH", "����������"},
    .concmd = "gcmd_set_language en",
    .init_cb = menu_item_slang_init,
    .udata = (void *)LANG_EN,
  },
  {
    .text = {"RUSSIAN", "�������"},
    .concmd = "gcmd_set_language ru",
    .init_cb = menu_item_slang_init,
    .udata = (void *)LANG_RU,
  },
  {.height=-666},
};

static menu_t menu_select_lang = {
  .title = {"LANGUAGE", "����"},
  .items = menu_select_lang_items,
};


////////////////////////////////////////////////////////////////////////////////
static const char *mopt_gt_text[2][2] = {
  {"\1GAME TYPE: COOP", "\1��� ����: COOP"},
  {"\1GAME TYPE: DM",   "\1��� ����: DM"},
};
static ccBool mopt_server_dm = CC_TRUE;
static int mopt_map = 1;
static char srvstart_cmd[256];


static void menu_server_options_build_command (void) {
  sprintf(srvstart_cmd, "gcmd_network_start_server %d %s", mopt_map, (mopt_server_dm ? "dm" : "coop"));
}


static int norm_map_num (int newmap, int dx) {
  if (newmap < 1) newmap = 1; else if (newmap > 99) newmap = 99;
  //
  for (int f = 0; f < 100; ++f) {
    char mname[16];
    //
    sprintf(mname, "MAP%02d", newmap);
    if (F_findres(mname) >= 0) break;
    newmap += (dx < 0 ? -1 : 1);
    if (newmap < 1) newmap = 99; else if (newmap > 99) newmap = 1;
  }
  //
  return newmap;
}


static void menu_server_options_init (menu_t *mnu) {
  mopt_server_dm = (mnu->items[1].udata != NULL);
  mopt_map = norm_map_num(mopt_map, 1);
  menu_server_options_build_command();
}


static void menu_server_options_gt_init (menu_item_t *mi) {
  mi->text[0] = mopt_gt_text[!!mopt_server_dm][0];
  mi->text[1] = mopt_gt_text[!!mopt_server_dm][1];
}


static int menu_server_options_gt_width (menu_item_t *mi) {
  return Z_strfwidthsf(mopt_gt_text[0][gm_cur_lang]+1);
}


static int menu_server_options_gt_key (menu_item_t *mi) {
  switch (lastkey) {
    case SDLK_RETURN: case SDLK_KP_ENTER:
      mopt_server_dm = !mopt_server_dm;
      break;
    case SDLK_LEFT: case SDLK_KP4:
      mopt_server_dm = CC_TRUE;
      break;
    case SDLK_RIGHT: case SDLK_KP6:
      mopt_server_dm = CC_FALSE;
      break;
    default: return 0;
  }
  menu_server_options_gt_init(mi);
  menu_server_options_build_command();
  return 1;
}


static int menu_server_options_map_width (menu_item_t *mi) {
  return Z_strfwidthsf(gm_cur_lang == 0 ? "START MAP: MAP99" : "�����: MAP99");
}


static void menu_server_options_map_draw (menu_item_t *mi, int y) {
  char tbuf[128];
  //int x = mi->x, idx = get_menu_item_pos(mnu, mi);
  //
  sprintf(tbuf, (gm_cur_lang == 0 ? "START MAP: MAP%02d" : "�����: MAP%02d"), mopt_map);
  Z_gotoxy(mi->x, y);
  Z_printsf("%s", tbuf);
}


static int menu_server_options_map_key (menu_item_t *mi) {
  switch (lastkey) {
    case SDLK_LEFT: case SDLK_KP4:
      if (--mopt_map < 1) mopt_map = 99;
      mopt_map = norm_map_num(mopt_map, -1);
      break;
    case SDLK_RETURN: case SDLK_KP_ENTER:
    case SDLK_RIGHT: case SDLK_KP6:
      if (++mopt_map > 99) mopt_map = 1;
      mopt_map = norm_map_num(mopt_map, 1);
      break;
    default: return 0;
  }
  menu_server_options_build_command();
  return 1;
}


static int menu_server_options_port_width (menu_item_t *mi) {
  return Z_strfwidthsf(gm_cur_lang == 0 ? "PORT: 99999" : "����: 99999");
}


static void menu_server_options_port_draw (menu_item_t *mi, int y) {
  char tbuf[128];
  int idx = get_menu_item_pos(mnu, mi);
  //
  if (input && idx == mnu->curpos) {
    sprintf(tbuf, (gm_cur_lang == 0 ? "PORT: %s_" : "����: %s_"), ibuf);
  } else {
    sprintf(tbuf, (gm_cur_lang == 0 ? "PORT: %u" : "����: %u"), n_host_port);
  }
  Z_gotoxy(mi->x, y);
  Z_printsf("%s", tbuf);
}


static int menu_server_options_port_key (menu_item_t *mi) {
  switch (lastkey) {
    case SDLK_RETURN: case SDLK_KP_ENTER:
      if (input) {
        int res;
        //
        cancel_input_state();
        if ((res = cmdcon_parseint(ibuf, -1, 1024, 65535, NULL)) > 0) {
          n_host_port = res;
        }
      } else {
        char buf[16];
        //
        sprintf(buf, "%u", n_host_port);
        enter_input_state(buf);
        imax = 5;
      }
      break;
    case SDLK_LEFT: case SDLK_KP4:
      if (n_host_port > 1024) --n_host_port;
      break;
    case SDLK_RIGHT: case SDLK_KP6:
      if (n_host_port < 65535) ++n_host_port;
      break;
    default: return 0;
  }
  return 1;
}


static menu_item_t menu_server_options_items[] = {
  {
    .text = {"START", "���������"},
    .concmd = srvstart_cmd,
  },
  {
    .text = {"\1GAME TYPE: COOP", "\1��� ����: COOP"},
    .init_cb = menu_server_options_gt_init,
    .key_cb = menu_server_options_gt_key,
    .width_cb = menu_server_options_gt_width,
  },
  {
    .width_cb = menu_server_options_map_width,
    .draw_cb = menu_server_options_map_draw,
    .key_cb = menu_server_options_map_key,
  },
  {
    .width_cb = menu_server_options_port_width,
    .draw_cb = menu_server_options_port_draw,
    .key_cb = menu_server_options_port_key,
  },
  {.height=-666},
};

static menu_t menu_server_options = {
  .title = {"SERVER", "������"},
  .init_cb = menu_server_options_init,
  .items = menu_server_options_items,
};


////////////////////////////////////////////////////////////////////////////////
static int menu_client_options_host_width (menu_item_t *mi) {
  return Z_strfwidthsf(gm_cur_lang == 0 ? "HOST: 999.999.999" : "����: 999.999.999");
}


static void menu_client_options_host_draw (menu_item_t *mi, int y) {
  char tbuf[256];
  int idx = get_menu_item_pos(mnu, mi);
  //
  sprintf(tbuf, (gm_cur_lang == 0 ? "HOST: %s%s" : "����: %s%s"),
    (input && idx == mnu->curpos? ibuf : n_host_addr), (input && idx == mnu->curpos ? "_" : ""));
  Z_gotoxy(mi->x, y);
  Z_printsf("%s", tbuf);
}


static int menu_client_options_host_key (menu_item_t *mi) {
  switch (lastkey) {
    case SDLK_RETURN: case SDLK_KP_ENTER:
      if (input) {
        int res;
        //
        cancel_input_state();
        if (ibuf[0]) {
          free(n_host_addr);
          n_host_addr = strdup(ibuf);
        }
        if ((res = cmdcon_parseint(ibuf, -1, 1024, 65535, NULL)) > 0) {
          n_host_port = res;
        }
      } else {
        enter_input_state(n_host_addr);
        imax = 42;
      }
      break;
    default: return 0;
  }
  return 1;
}


static menu_item_t menu_client_options_items[] = {
  {
    .text = {"START", "���������"},
    .concmd = "gcmd_network_start_client",
  },
  {
    .width_cb = menu_client_options_host_width,
    .draw_cb = menu_client_options_host_draw,
    .key_cb = menu_client_options_host_key,
  },
  {
    .width_cb = menu_server_options_port_width,
    .draw_cb = menu_server_options_port_draw,
    .key_cb = menu_server_options_port_key,
  },
  {.height=-666},
};


static menu_t menu_client_options = {
  .title = {"CLIENT", "������"},
  .items = menu_client_options_items,
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_game_type_network_items[] = {
  {
    .enter_menu = &menu_server_options,
    .text = {"SERVER", "������"},
  },
  {
    .enter_menu = &menu_client_options,
    .text = {"CLIENT", "������"},
  },
  {.height=-666},
};

static menu_t menu_game_type_network = {
  .title = {"NETWORK GAME", "������� ����"},
  .items = menu_game_type_network_items,
};


////////////////////////////////////////////////////////////////////////////////
static menu_item_t menu_game_type_items[] = {
  {
    //.enter_menu = &menu_game_type,
    .text = {"DEATHMATCH", "DEATHMATCH"},
    .saysnd = "_DM",
    .concmd = "gcmd_new_game_2pl dm",
  },
  {
    //.enter_menu = &menu_game_type,
    .text = {"COOPERATIVE", "COOPERATIVE"},
    .saysnd = "_COOP",
    .concmd = "gcmd_new_game_2pl coop",
  },
  {.height=-666},
};

static menu_t menu_game_type = {
  .title = {"GAME TYPE", "��� ����"},
  .items = menu_game_type_items,
  .defcurposp1 = 1,
};


////////////////////////////////////////////////////////////////////////////////
static void menu_item_fullscreen_init (menu_item_t *mi) {
  if (fullscreen) {
    mi->text[0] = "FULLSCREEN  YES";
    mi->text[1] = "������ �����  ��";
  } else {
    mi->text[0] = "FULLSCREEN  NO";
    mi->text[1] = "������ �����  ���";
  }
}


static menu_item_t menu_game_options_items[] = {
  {
    .enter_menu = &menu_volume,
    .text = {"VOLUME", "���������"},
  },
  {
    .enter_menu = &menu_gamma,
    .text = {"BRIGHTNESS", "�������"},
  },
  /*
  {
    //.enter_menu = &menu_game_type,
    .text = {"MUSIC","������"},
  },
  */
  {
    .text = {"FULLSCREEN:YES", "������ �����:���"},
    .init_cb = menu_item_fullscreen_init,
    .concmd = "fullscreen",
  },
  {
    .enter_menu = &menu_select_lang,
    .text = {"LANGUAGE", "����"},
  },
  {
    .enter_menu = &menu_controls_plsel,
    .text = {"CONTROLS", "����������"},
  },
  {.height=-666},
};

static menu_t menu_game_options = {
  .title = {"OPTIONS", "������"},
  .saysnd = "_RAZNOE",
  .items = menu_game_options_items,
};


////////////////////////////////////////////////////////////////////////////////
static void menu_draw_gamma_slider (menu_item_t *mi, int y) {
  menu_draw_slider(mi, y, gammaa<<5);
}


static int menu_gamma_key (menu_item_t *mi) {
  switch (lastkey) {
    case SDLK_LEFT: case SDLK_KP4:
      if (gammaa > 0) {
        if (!movsndt) movsndt = Z_sound(msnd5, 255);
        setgamma(gammaa-1);
      }
      break;
    case SDLK_RIGHT: case SDLK_KP6:
      if (gammaa < 4) {
        if (!movsndt) movsndt = Z_sound(msnd6, 255);
        setgamma(gammaa+1);
      }
      break;
    default:
      return 0;
  }
  return 1;
}


static menu_item_t menu_gamma_items[] = {
  {
    .key_cb = menu_gamma_key,
    .draw_cb = menu_draw_gamma_slider,
    .width_cb = menu_slider_width,
    .height = 16,
  },
  {.height=-666},
};

static menu_t menu_gamma = {
  .title = {"BRIGHTNESS", "�������"},
  .saysnd = "_GAMMA",
  .items = menu_gamma_items,
};


////////////////////////////////////////////////////////////////////////////////
static const char *volume_texts[2] = {"SFX", "MUS"};


static int menu_soundvol_width (menu_item_t *mi) {
  int w = K8MAX(Z_strfwidthbf(volume_texts[0]), Z_strfwidthbf(volume_texts[1]));
  //
  return (menu_slider_width(mi)+4+w);
}


static void menu_draw_sndmus_slider (menu_item_t *mi, int y, int lvl, const char *txt) {
  int w = K8MAX(Z_strfwidthbf(volume_texts[0]), Z_strfwidthbf(volume_texts[1]));
  //
  Z_gotoxy(mi->x, y);
  Z_printbf(txt);
  mi->x += w+4;
  menu_draw_slider(mi, y, lvl);
  mi->x -= w+4;
}

static void menu_draw_sound_slider (menu_item_t *mi, int y) {
  menu_draw_sndmus_slider(mi, y, snd_vol, volume_texts[0]);
}

static void menu_draw_music_slider (menu_item_t *mi, int y) {
  menu_draw_sndmus_slider(mi, y, mus_vol, volume_texts[1]);
}


static int menu_volume_key (menu_item_t *mi, int lvl, void (*set)(int lvl)) {
  switch (lastkey) {
    case SDLK_LEFT: case SDLK_KP4:
      if (lvl <= 0) return 1;
      lvl -= 8;
      break;
    case SDLK_RIGHT: case SDLK_KP6:
      if (lvl >= 128) return 1;
      lvl += 8;
      break;
    default:
      return 0;
  }
  set(lvl);
  if (!movsndt) movsndt = Z_sound(msnd5, 255);
  return 1;
}

static int menu_soundvol_key (menu_item_t *mi) {
  return menu_volume_key(mi, snd_vol, S_volume);
}

static int menu_musicvol_key (menu_item_t *mi) {
  return menu_volume_key(mi, mus_vol, S_volumemusic);
}


static menu_item_t menu_volume_items[] = {
  {
    .key_cb = menu_soundvol_key,
    .draw_cb = menu_draw_sound_slider,
    .width_cb = menu_soundvol_width,
    .height = 16,
  },
  {
    .key_cb = menu_musicvol_key,
    .draw_cb = menu_draw_music_slider,
    .width_cb = menu_soundvol_width,
    .height = 16,
  },
  {.height=-666},
};

static menu_t menu_volume = {
  .title = {"VOLUME", "���������"},
  .saysnd = "_VOLUME",
  .items = menu_volume_items,
};


////////////////////////////////////////////////////////////////////////////////
/*!!
  if (mnu == &plcolor_mnu) {
    if (*(++panimp) == 0) panimp = panim;
    GM_set(mnu);
  }
*/

////////////////////////////////////////////////////////////////////////////////
CONCMD(gcmd_new_game_1pl, "start new game\ngcmd_new_game_1pl [level]") {
  CMDCON_HELP();
  //
  int lvl = cli_warp;
  //
  if (argc < 1 || argc > 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if (argc > 1) {
    if ((lvl = cmdcon_parseint(argv[1], -1, 1, 99, NULL)) < 0) {
      conlogf("%s: invalid level number: '%s'!\n", argv[0], argv[1]);
      return;
    }
  }
  //
  N_disconnect();
  demo_done();
  cli_warp = 0; //k8
  g_plrcount = 1;
  g_dm = 0;
  g_map = (lvl ? lvl : 1);
  M_init_players();
  G_start();
  GM_set(NULL);
}


CONCMD(gcmd_new_game_2pl, "start new game\ngcmd_new_game_2pl <dm|coop> [level]") {
  CMDCON_HELP();
  //
  int lvl = cli_warp;
  //
  if (argc < 2 || argc > 3) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if (strcasecmp(argv[1], "dm") == 0) g_dm = 1;
  else if (strcasecmp(argv[1], "coop") == 0) g_dm = 0;
  else { conlogf("%s: invalid game mode: '%s'!\n", argv[0], argv[1]); return; }
  //
  if (argc > 2) {
    if ((lvl = cmdcon_parseint(argv[2], -1, 1, 99, NULL)) < 0) {
      conlogf("%s: invalid level number: '%s'!\n", argv[0], argv[2]);
      return;
    }
  }
  //
  N_disconnect();
  demo_done();
  cli_warp = 0; //k8
  g_plrcount = 2;
  g_map = (lvl ? lvl : 1);
  M_init_players();
  G_start();
  GM_set(NULL);
}


CONCMD(gcmd_end_game, "end current game") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  //
  N_disconnect();
  demo_done();
  PL_reset();
  G_end();
  GM_set(NULL);
}


CONCMD(gcmd_restart_level, "restart current level") {
  CMDCON_HELP();
  CMDCON_CHECK_INGAME();
  //
  N_disconnect();
  demo_done();
  PL_reset();
  //myrandomize(); //k8
  G_start();
  GM_set(NULL);
}


CONCMD(gcmd_cancel, "cancel menu") {
  CMDCON_HELP();
  //
  GM_set(NULL);
}


CONCMD(gcmd_quit, "quit menu") {
  CMDCON_HELP();
  //
  menu_quit_enter(NULL);
}


CONCMD(gcmd_do_quit, "quit with sound") {
  CMDCON_HELP();
  //
  //int c;
  //
  demo_done();
  F_freemus();
  GMV_stop();
  /*c =*/ Z_sound(M_lock(qsnd[myrand1(QSND_NUM)]), 256);//for(c = (Z_sound(M_lock(qsnd[random2(QSND_NUM)]), 256)+9)<<16, timer = 0;timer < c;);
  S_wait();
  ERR_quit(0);
}


CONCMD(gcmd_next_music, "next music") {
  CMDCON_HELP();
  //
  F_freemus();
  F_nextmus(g_music);
  F_loadmus(g_music);
  S_startmusic(music_time*2);
  GM_set(mnu);
}


CONCMD(gcmd_options, "options menu") {
  CMDCON_HELP();
  //
  //Z_sound(msnd3, 128);
  if (mnu != &menu_game_options) menu_game_options.esc_menu = mnu;
  GM_set(&menu_game_options);
  GMV_say(mnu->saysnd);
}


CONCMD(gcmd_gamma, "gamma menu") {
  CMDCON_HELP();
  //
  //Z_sound(msnd3, 128);
  if (mnu != &menu_gamma) menu_gamma.esc_menu = mnu;
  GM_set(&menu_gamma);
  GMV_say(mnu->saysnd);
}


CONCMD(gcmd_set_language, "set menu language\ngcmd_set_language <ru|en>") {
  CMDCON_HELP();
  static const struct {
    const char *name;
    int id;
  } lang_list[] = {
    {"en", LANG_EN},
    {"ru", LANG_RU},
  };
  //
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  for (size_t f = 0; f < sizeof(lang_list)/sizeof(lang_list[0]); ++f) {
    if (strcasecmp(lang_list[f].name, argv[1]) == 0) {
      if (gm_cur_lang != lang_list[f].id) {
        gm_cur_lang = lang_list[f].id;
        GM_set(mnu);
        evt_language_changed();
      }
      return;
    }
  }
  //
  conlogf("%s: unknown language: '%s'!\n", argv[0], argv[1]);
}


CONCMD(gcmd_controls, "player controls menu\ngcmd_controls <1|2>") {
  CMDCON_HELP();
  //
  int plr;
  //
  if (argc != 2) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  if ((plr = cmdcon_parseint(argv[1], -1, 1, 2, NULL)) < 0) {
    conlogf("%s: invalid player number: '%s'!\n", argv[0], argv[1]);
    return;
  }
  //
  if (mnu != &menu_controls) menu_controls.esc_menu = mnu;
  menu_controls.udata = (void *)(plr-1);
  GM_set(&menu_controls);
  GMV_say(mnu->saysnd);
}


CONCMD(gcmd_swap_controls, "swap player 1 and player 2 controls") {
  CMDCON_HELP();
  //
  if (argc != 1) {
    conlogf("%s: invalid number of args!\n", argv[0]);
    return;
  }
  //
  for (int f = 0; f < KB_MAX; ++f) {
    int p00 = pl[0].kb[f][0], p01 = pl[0].kb[f][1];
    //
    pl[0].kb[f][0] = pl[1].kb[f][0];
    pl[0].kb[f][1] = pl[1].kb[f][1];
    pl[1].kb[f][0] = p00;
    pl[1].kb[f][1] = p01;
  }
}


/*
static void GM_command (int c) {
  switch (c) {
    case CONTROLS_SWAP:
      ctrl_key_alt = -1;
      //!!GM_set(&opt_mnu);
      break;
    case PL1CM:
      if (--p1color < 0) p1color = PCOLORN-1;
      break;
    case PL1CP:
      if (++p1color >= PCOLORN) p1color = 0;
      break;
    case PL2CM:
      if (--p2color < 0) p2color = PCOLORN-1;
      break;
    case PL2CP:
      if (++p2color >= PCOLORN) p2color = 0;
      break;
  }
}
*/


static int processInput (void) {
  if (input) {
    uint8_t c;
    //
    switch (lastkey) {
      case SDLK_RETURN:
      case SDLK_KP_ENTER:
        if (mnu != NULL && mnu->curpos >= 0 && mnu->curpos < get_menu_item_count(mnu)) {
          if (mnu->items[mnu->curpos].key_cb != NULL && mnu->items[mnu->curpos].key_cb(&mnu->items[mnu->curpos])) break;
          menu_do_enter(&mnu->items[mnu->curpos], 0);
        } else {
          cancel_input_state();
          GM_set(NULL);
        }
        break;
      case SDLK_ESCAPE:
        cancel_input_state();
        GM_set(mnu);
        break;
      case SDLK_BACKSPACE:
        if (icur > 0) {
          ibuf[--icur] = 0;
          GM_set(mnu);
        }
        break;
      default:
        c = get_keychar(lastkey);
        if (c && icur < imax) {
          ibuf[icur] = c;
          ibuf[++icur] = 0;
          GM_set(mnu);
        }
        break;
    }
    //
    return 1;
  }
  //
  return 0;
}


static int processKeySelect (void) {
  if (ctrl_key_alt >= 0 && lastkey > 0) {
    switch (lastkey) {
      case SDLK_ESCAPE:
        break;
      default:
        pl[ctrl_plr_num].kb[kmap[mnu->curpos]][ctrl_key_alt] = lastkey;
        break;
    }
    //
    Z_sound(msnd3, 128);
    ctrl_key_alt = -1;
    return 1;
  }
  //
  return 0;
}


static int processHotMenuKeys (void) {
  if (input) return 0;
  //
  switch (lastkey) {
    case SDLK_F2:
      if (g_st != GS_GAME) return 0;
      Z_sound(msnd3, 128);
      F_getsavnames();
      GM_set(&menu_game_save);
      break;
    case SDLK_F3:
      Z_sound(msnd3, 128);
      F_getsavnames();
      GM_set(&menu_game_load);
      break;
    case SDLK_F5:
      cmdcon_exec("gcmd_gamma");
      break;
    case SDLK_F4:
      cmdcon_exec("gcmd_options");
      break;
    case SDLK_F10:
      //Z_sound(msnd3, 128);
      cmdcon_exec("gcmd_quit");
      break;
    default:
      return 0;
  }
  //
  return 1;
}


// mnu == NULL
static int processNonMenuKeys (void) {
  if (mnu != NULL || input) return 0;
  //
  switch (lastkey) {
    case SDLK_ESCAPE:
      if (mnu != NULL) return 0;
      GM_set(g_st == GS_GAME ? &menu_main_ingame : &menu_main);
      Z_sound(msnd3, 128);
      break;
    default:
      return processHotMenuKeys();
  }
  //
  return 1;
}


static int processMenuTypeMenu (void) {
  menu_item_t *i;
  //
  if (mnu == NULL || input) return 0;
  //
  i = (mnu->curpos >= 0 && mnu->curpos < mnu->item_count ? &mnu->items[mnu->curpos] : NULL);
  if (lastkey && i != NULL && i->key_cb != NULL) {
    if (i->key_cb(&mnu->items[mnu->curpos])) return 1;
  }
  //
  switch (lastkey) {
    case SDLK_UP: case SDLK_KP8:
      if (--mnu->curpos < 0) mnu->curpos = mnu->item_count-1;
      Z_sound(msnd1, 128);
      break;
    case SDLK_DOWN: case SDLK_KP2:
      if (++mnu->curpos >= mnu->item_count) mnu->curpos = 0;
      Z_sound(msnd1, 128);
      break;
    case SDLK_HOME: case SDLK_KP7:
      if (mnu->curpos > 0) {
        mnu->curpos = 0;
        Z_sound(msnd1, 128);
      }
      break;
    case SDLK_END: case SDLK_KP1:
      if (mnu->curpos < mnu->item_count-1) {
        mnu->curpos = mnu->item_count-1;
        Z_sound(msnd1, 128);
      }
      break;
    case SDLK_SPACE: case SDLK_RETURN: case SDLK_KP_ENTER:
      if (i != NULL) menu_do_enter(i, 0);
      break;
/*
      if (mnu == &ctrl_keys_mnu) {
        ctrl_key_alt = (lastkey == SDLK_SPACE);
        Z_sound(msnd2, 128);
        break;
      }
*/
    case SDLK_ESCAPE:
      if (i != NULL) {
        menu_t *m = mnu->esc_menu;
        //
        if (i->esc_cb != NULL && i->esc_cb(i)) return 1;
        mnu = NULL;
        GM_set(m);
        Z_sound(msnd1, 128);
      }
      break;
    case SDLK_DELETE: case SDLK_KP_PERIOD:
/*
      if (mnu == &ctrl_keys_mnu) {
        pl[ctrl_plr_num].kb[kmap[mnu->curpos]][0] = -1;
        pl[ctrl_plr_num].kb[kmap[mnu->curpos]][1] = -1;
      }
*/
      break;
    default:
      return processHotMenuKeys();
  }
  //
  return 1;
}


// mnu != NULL
static int processMenuKeys (void) {
  int eaten = 0, icnt;
  //
  if (mnu == NULL || input) return 0;
  //
  icnt = mnu->item_count;
  if (mnu->curpos >= 0 && mnu->curpos < icnt) {
    if ((eaten = processMenuTypeMenu()) != 0) GM_update(mnu);
  }
  //
  if (!eaten) {
    switch (lastkey) {
      case SDLK_ESCAPE:
        if (mnu == &menu_main || mnu == &menu_main_ingame) GM_set(NULL);
        else GM_set((g_st == GS_TITLE ? &menu_main : NULL));
        Z_sound(msnd4, 128);
        break;
      default:
        return processHotMenuKeys();
    }
  }
  //
  return 1;
}


static int processTitleKeys (void) {
  return processHotMenuKeys();
}


int GM_isactive (void) {
  return (mnu != NULL);
}


int GM_act (void) {
  int eaten = 0;
  //
  if (lastkey == SDLK_F1 && shot_vga) {
    save_screenshot();
    Z_sound(msnd4, 128);
    lastkey = 0;
  }
  //
  if (movsndt > 0) --movsndt; else movsndt = 0;
  //
  if (g_st == GS_TITLE && mnu == NULL) {
    if (K_is_any_key(lastkey)) {
      if (!processTitleKeys()) {
        GM_set(g_st == GS_GAME ? &menu_main_ingame : &menu_main);
        Z_sound(msnd3, 128);
      }
    }
    //
    lastkey = 0;
    return 1;
  } else {
    eaten = processInput();
    if (!eaten) eaten = processKeySelect();
    if (!eaten) eaten = processMenuKeys();
    if (!eaten) eaten = processNonMenuKeys();
    //
    lastkey = 0;
    return (mnu != NULL ? 1 : 0);
  }
}


static void print_menu_text (const char *str) {
  if (str != NULL) {
    if (str[0] != '\1') Z_printbf(str); else Z_printsf(str+1);
  }
}


int GM_draw (void) {
  int y;
  //
  if (mnu == NULL && !gm_redraw) return 0;
  gm_redraw = 0;
  if (mnu == NULL) return 1;
  //
  V_fadescr(160);
  ++gm_tm;
  V_setrect(0, SCRW, 0, SCRH);
  //
  y = get_menu_y(mnu);
  if (mnu->title[gm_cur_lang] != NULL) {
    Z_gotoxy(mnu->x, y);
    print_menu_text(mnu->title[gm_cur_lang]);
    y += (mnu->title[gm_cur_lang][0] != '\1' ? 16 : 8)+MENU_TITLE_OFS;
  }
  //
  for (int idx = 0; idx < mnu->item_count; ++idx) {
    menu_item_t *i = &mnu->items[idx];
    //
    // draw menu item
    Z_gotoxy(i->x, y);
    if (i->draw_cb != NULL) i->draw_cb(i, y);
    else if (i->text[gm_cur_lang] != NULL) print_menu_text(i->text[gm_cur_lang]);
    //
    // draw cursor
    if (mnu->curpos == idx) {
      if (i->height >= 16) {
        // big cursor
        int cy = (i->height-8)/2;
        //
        V_spr(i->x-25, y-cy, &msklh[(gm_tm/6)&1]);
      } else {
        // small cursor
        int cy = (i->height-8)/2;
        //
        if ((gm_tm/6)&1) V_spr(i->x-6, y-cy, &cursmall);
      }
    }
    //
    y += i->height;
  }
  //
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
CONCMD(quit, "quit game") {
  CMDCON_HELP();
  F_freemus();
  GMV_stop();
  ERR_quit(1);
}


////////////////////////////////////////////////////////////////////////////////
void evt_language_changed (void) {
}


////////////////////////////////////////////////////////////////////////////////
GCC_CONSTRUCTOR_USER {
  V_initvga(&msklh[0]);
  V_initvga(&msklh[1]);
  V_initvga(&mbarl);
  V_initvga(&mbarm);
  V_initvga(&mbarr);
  V_initvga(&mbaro);
  V_initvga(&mslotl);
  V_initvga(&mslotm);
  V_initvga(&mslotr);
  V_initvga(&cursmall);
}


GCC_DESTRUCTOR_USER {
  V_freevga(&msklh[0]);
  V_freevga(&msklh[1]);
  V_freevga(&mbarl);
  V_freevga(&mbarm);
  V_freevga(&mbarr);
  V_freevga(&mbaro);
  V_freevga(&mslotl);
  V_freevga(&mslotm);
  V_freevga(&mslotr);
  V_freevga(&cursmall);
}
