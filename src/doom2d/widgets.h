/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 */
#ifndef ZXEMUT_WIDGETS_H
#define ZXEMUT_WIDGETS_H

#include <stdint.h>

#include "sdldrv.h"
#include "vga.h"
#include "vgaovl.h"


#ifdef __cplusplus
extern "C" {
#endif


////////////////////////////////////////////////////////////////////////////////
typedef struct ZXWindow ZXWindow;
typedef struct ZXWidget ZXWidget;


////////////////////////////////////////////////////////////////////////////////
enum {
  ZXWMAGICK = 0x4d57585aU // 'ZXWM'
};


////////////////////////////////////////////////////////////////////////////////
extern int zxwStrLenAmp (const char *str);


////////////////////////////////////////////////////////////////////////////////
// color schemes
typedef struct ZXWColorScheme ZXWColorScheme;
typedef struct ZXWColor ZXWColor;

extern ZXWColorScheme *zxcNewColorScheme (const char *scmname);
extern ZXWColorScheme *zxcFindColorScheme (const char *scmname);
extern const char *zxcColorSchemeName (ZXWColorScheme *scm);

extern ZXWColor *zxscmFindColorInScheme (ZXWColorScheme *scm, const char *clrname);
extern ZXWColor *zxscmFindColor (const char *scmname, const char *clrname);

extern Uint8 zxscmColor (ZXWColorScheme *scm, const char *clrname);
extern int zxcSetColor (const char *scmname, const char *clrname, Uint8 clr);

// return value is undefined for unknown color/scheme
/*
static Uint8 zxcColor (const char *scmname, const char *clrname) {
  return zxscmColor(zxcFindColorScheme(scmname), clrname);
}
*/

// !0: stop, return this value
typedef int (*ZXCSchemeEnum) (ZXWColorScheme *scm);
typedef int (*ZXSColorEnum) (ZXWColorScheme *scm, const char *clrname, Uint8 clr);

extern int zxcEnumSchemes (ZXCSchemeEnum enumFn);
extern int zxcEnumColors (ZXWColorScheme *scm, ZXSColorEnum enumFn);


////////////////////////////////////////////////////////////////////////////////
enum {
  ZXWCLR_NORMAL   = 0,
  ZXWCLR_ACTIVE   = 1,
  ZXWCLR_DISABLED = 2,
  ZXWCLR_STATE_MASK = 0x0f,
};


extern Uint8 zxwFindColor (ZXWidget *g, const char *widname, const char *papink, int clrflags);


////////////////////////////////////////////////////////////////////////////////
extern int zxcInitDefaultColorSchemes (void);


////////////////////////////////////////////////////////////////////////////////
typedef void (*ZXWDestroyCB) (ZXWidget *g);
typedef void (*ZXWPaintCB) (ZXWidget *g, VExSurface *sfc);
typedef int (*ZXWKeyCB) (ZXWidget *g, SDL_KeyboardEvent *key); // !0: eaten
typedef int (*ZXWMouseCB) (ZXWidget *g, int x, int y, int btn, int buttons); // !0: eaten
typedef void (*ZXWActivationCB) (ZXWidget *g, int activated);

typedef void (*ZXWinDestroyCB) (ZXWindow *w);
typedef void (*ZXWinPaintCB) (ZXWindow *w, VExSurface *sfc);
typedef int (*ZXWinKeyCB) (ZXWindow *w, SDL_KeyboardEvent *key); // !0: eaten
typedef int (*ZXWinMouseCB) (ZXWindow *w, int x, int y, int btn, int buttons); // !0: eaten
typedef void (*ZXWinActivationCB) (ZXWindow *w, int activated);


////////////////////////////////////////////////////////////////////////////////
enum {
  ZXW_CANFOCUS = 0x01, // widget can take focus
  ZXW_DISABLED = 0x100,
  ZXW_HIDDEN   = 0x200,
  ZXW_DEAD     = 0x1000, // this widget is dead and awaits for garbage collection
};


struct ZXWidget {
  ZXWindow *window;
  int x, y, w, h;
  Uint32 flags;
  char *id;
  void *udata;
  //
  ZXWidget *prev;
  ZXWidget *next;
  //
  ZXWPaintCB paintCB; // clip for sfc is set
  ZXWKeyCB keyCB;
  ZXWMouseCB mouseCB;
  ZXWActivationCB activationCB;
  ZXWDestroyCB destroyCB;
};


enum {
  ZXWIN_HIDDEN     = 0x200,
  ZXWIN_DISABLED   = 0x100,
  ZXWIN_DEAD       = 0x1000, // this window is dead and awaits for garbage collection
  ZXWIN_HASZOMBIES = 0x2000, // this window has some dead widgets
};


struct ZXWindow {
  VExSurface sfc; // can be NULL
  int x, y;
  Uint32 flags;
  char *id;
  ZXWColorScheme *scm;
  void *udata;
  //
  // draw: first to last; events: last to first
  ZXWidget *wfirst;
  ZXWidget *wlast;
  ZXWidget *wmbdown; // any
  ZXWidget *active;
  //
  char title[60];
  // title color
  Uint8 tInk;
  Uint8 tPaper;
  // window color
  Uint8 wInk;
  Uint8 wPaper;
  //
  ZXWindow *prev;
  ZXWindow *next;
  //
  int moving;
  int moveDX;
  int moveDY;
  //
  ZXWinPaintCB paintCB;
  ZXWinKeyCB keyCB;
  ZXWinMouseCB mouseCB;
  ZXWinActivationCB activationCB;
  ZXWinDestroyCB destroyCB;
};


// draw: first to last; events: last to first
extern ZXWindow *zxwinfirst; // can be NULL
extern ZXWindow *zxwinlast; // can be NULL
extern ZXWindow *zxwinactive; // can be NULL

extern int zxwidsys_active; // should be updated externally


////////////////////////////////////////////////////////////////////////////////
extern void zxwinInit (void);
extern void zxwinDeinit (void);
extern void zxwinGC (void); // kill all 'dead' widgets and windows

extern ZXWindow *zxwinNew (const char *scmname, const char *aId,
  int x, int y, int aw, int ah, const char *aTitle);
extern int zxwinClose (ZXWindow *w); // <0: error; 0: ok; >0: can't close

extern ZXWindow *zxwinFindById (const char *id);

extern int zxwinIsAlive (ZXWindow *w);
extern int zxwinIsVisible (ZXWindow *w);
extern int zxwinIsEnabled (ZXWindow *w);
extern int zxwinIsActive (ZXWindow *w);

extern int zxwinCanBeActivated (ZXWindow *w);

extern int zxwinActivate (ZXWindow *w); // <0: error; 0: not activated; 1: already active or activated; doesn't brings to top
extern int zxwinBringToTop (ZXWindow *w); // <0: error; 0: not activated; 1: already active or activated

extern int zxwinHide (ZXWindow *w);
extern int zxwinShow (ZXWindow *w);

static inline int zxwinX (ZXWindow *w) { return (w != NULL ? w->x : 0); }
static inline int zxwinY (ZXWindow *w) { return (w != NULL ? w->y : 0); }
static inline int zxwinW (ZXWindow *w) { return (w != NULL && w->sfc.vo != NULL ? w->sfc.vo->w : 0); }
static inline int zxwinH (ZXWindow *w) { return (w != NULL && w->sfc.vo != NULL ? w->sfc.vo->h : 0); }

static inline const char *zxwinCaption (ZXWindow *w) { return (w != NULL ? w->title : ""); }
static inline int zxwinCaptionH (ZXWindow *w) { return (w != NULL && w->title[0] ? 8 : 0); }

extern int zxwinSetX (ZXWindow *w, int v);
extern int zxwinSetY (ZXWindow *w, int v);
extern int zxwinSetW (ZXWindow *w, int v);
extern int zxwinSetH (ZXWindow *w, int v);

extern int zxwinSetCaption (ZXWindow *w, const char *title);


////////////////////////////////////////////////////////////////////////////////
extern ZXWidget *zxwinFindFocusPrev (ZXWindow *w);
extern ZXWidget *zxwinFindFocusNext (ZXWindow *w);
extern void zxwinFocusPrev (ZXWindow *w);
extern void zxwinFocusNext (ZXWindow *w);


////////////////////////////////////////////////////////////////////////////////
enum {
  ZXWT_STD_WIDGETS = -666,
  ZXWT_FRAME,
  ZXWT_STATIC,
  ZXWT_BUTTON,
  ZXWT_CHECKBOX,
  ZXWT_RADIO,
  ZXWT_LINEEDIT,
  ZXWT_SCROLLBAR,
  ZXWT_LISTBOX,
  ZXWT_TEXTEDIT,
};


typedef struct ZXWWidgetDataHeader ZXWWidgetDataHeader;

typedef void (*zxwClickCB) (ZXWidget *g, void *udata);
typedef void (*zxwSBarCB) (ZXWidget *g, ZXWidget *sb); // scrollbar position changed
typedef void (*zxwWidgetClearCB) (ZXWidget *g, ZXWWidgetDataHeader *hdr);


struct ZXWWidgetDataHeader {
  uint32_t magic0; // ZXWMAGICK
  int type; // must be != 0
  zxwClickCB clickCB;
  zxwSBarCB sbarCB;
  //
  char *title; // can be NULL
  SDLKey actKey; // activation key: 0 or SDLK_x
  int actCPos; // hotkey position in title (chars, not pixels)
  //
  void *udata;
  int freeUData;
  zxwWidgetClearCB clearCB; // will be called before freeing anything
  //
  uint32_t magic1; // ZXWMAGICK
};


// udatasize>0: alloc udata
extern ZXWidget *zxwNewWidget (ZXWindow *w, const char *id, int x, int y, int aw, int ah, int udatasize);
extern ZXWidget *zxwNewCommonWidget (ZXWindow *w, int wtype, const char *id, int x, int y, int aw, int ah, int udsize);
extern int zxwRemoveWidget (ZXWidget *g);
extern int zxwHideWidget (ZXWidget *g);
extern int zxwShowWidget (ZXWidget *g);

extern ZXWidget *zxwFindById (ZXWindow *w, const char *id);
extern int zxwWidgetSetId (ZXWidget *g, const char *id);
extern const char *zxwId (ZXWidget *g);

extern int zxwIsAlive (ZXWidget *g);
extern int zxwIsVisible (ZXWidget *g);
extern int zxwIsActive (ZXWidget *g);
extern int zxwIsEnabled (ZXWidget *g);

extern int zxwCanBeVisible (ZXWidget *g);
extern int zxwCanBeActivated (ZXWidget *g);
extern int zxwCanProcessKeys (ZXWidget *g);

extern int zxwActivate (ZXWidget *g); // <0: error; 0: not activated; 1: already active or activated; doesn't brings to top

static inline int zxwX (ZXWidget *g) { return (g != NULL ? g->x : 0); }
static inline int zxwY (ZXWidget *g) { return (g != NULL ? g->y : 0); }
static inline int zxwW (ZXWidget *g) { return (g != NULL ? g->w : 0); }
static inline int zxwH (ZXWidget *g) { return (g != NULL ? g->h : 0); }

static inline int zxwSetX (ZXWidget *g, int v) { if (zxwIsAlive(g)) { g->x = v; return 0; } return -1; }
static inline int zxwSetY (ZXWidget *g, int v) { if (zxwIsAlive(g)) { g->y = v; return 0; } return -1; }
static inline int zxwSetW (ZXWidget *g, int v) { if (zxwIsAlive(g)) { g->w = (v < 0 ? 0 : v); return 0; } return -1; }
static inline int zxwSetH (ZXWidget *g, int v) { if (zxwIsAlive(g)) { g->h = (v < 0 ? 0 : v); return 0; } return -1; }

////////////////////////////////////////////////////////////////////////////////
// for widgets with ZXWWidgetDataHeader
extern int zxwWidgetType (ZXWidget *g);
extern ZXWWidgetDataHeader *zxwWidgetHeader (ZXWidget *g); // NULL: not a common widget
extern void *zxwWidgetHeaderEx (ZXWidget *g, int type); // NULL: not a common widget

// can release previous udata
extern int zxwWidgetSetUData (ZXWidget *g, void *udata, int freeUData);
extern void *zxwUData (ZXWidget *g);

extern int zxwWidgetSetUData (ZXWidget *g, void *udata, int freeUData);
extern void *zxwUData (ZXWidget *g);

extern int zxwWidgetSetClickCB (ZXWidget *g, zxwClickCB cb);
extern zxwClickCB zxwWidgetClickCB (ZXWidget *g);
extern void zxwClick (ZXWidget *g);

extern int zxwWidgetSetSBarCB (ZXWidget *g, zxwSBarCB cb);
extern zxwSBarCB zxwWidgetSBarCB (ZXWidget *g);
extern void zxwSBar (ZXWidget *g, ZXWidget *sb);

// set/replace title; process hotkeys and set actKey and actCPos
extern int zxwSetTitle (ZXWidget *g, const char *aTitle);

extern int wIsActivateKeyPressed (ZXWidget *g, const SDL_KeyboardEvent *key);


////////////////////////////////////////////////////////////////////////////////
extern ZXWidget *zxwNewFrame (ZXWindow *w, const char *id, int x, int y, int aw, int ah, const char *aTitle);


////////////////////////////////////////////////////////////////////////////////
extern ZXWidget *zxwNewStatic (ZXWindow *w, const char *id, int x, int y, int aw, int ah, const char *aTitle);

extern ZXWidget *zxwStaticLinked (ZXWidget *g);
extern int zxwStaticSetLinked (ZXWidget *g, ZXWidget *link);


////////////////////////////////////////////////////////////////////////////////
// aw,ah <0: autosize
extern ZXWidget *zxwNewButton (ZXWindow *w, const char *id, int x, int y, int aw, int ah,
  const char *aTitle, zxwClickCB aClickCB);

extern int zxwButtonSetDefault (ZXWidget *g);
extern int zxwButtonSetCancel (ZXWidget *g);
extern int zxwButtonSetNormal (ZXWidget *g);


////////////////////////////////////////////////////////////////////////////////
// if there is 'click' -- will not change state
extern ZXWidget *zxwNewCheckBox (ZXWindow *w, const char *id, int x, int y, int aw, int ah, const char *aTitle);

extern int zxwCheckBoxSetState (ZXWidget *g, int state);
extern int zxwCheckBoxState (ZXWidget *g); // >=0: state; <0: error


////////////////////////////////////////////////////////////////////////////////
extern ZXWidget *zxwNewRadio (ZXWindow *w, const char *group, const char *id,
  int x, int y, int aw, int ah, const char *aTitle);

extern ZXWidget *zxwinRadioGetActiveWidget (ZXWindow *w, const char *group);
extern int zxwinRadioSet (ZXWindow *w, const char *group, const char *id);
extern const char *zxwinRadioGet (ZXWindow *w, const char *group); // can return NULL

extern const char *zxwRadioGetGroupName (ZXWidget *g); // can return NULL

// will not change other radios
extern int zxwRadioSetActive (ZXWidget *g, int act);
extern int zxwRadioGetActive (ZXWidget *g); // <0: error


////////////////////////////////////////////////////////////////////////////////
extern ZXWidget *zxwNewLineEdit (ZXWindow *w, const char *id,
  int x, int y, int aw, int ah,
  const char *aText);

extern const char *zxwLineEditText (ZXWidget *g); // NULL: error
extern int zxwLineEditSetText (ZXWidget *g, const char *txt); // <0: error


////////////////////////////////////////////////////////////////////////////////
// TODO: pageup/pagedown
extern ZXWidget *zxwNewScrollBar (ZXWindow *w, const char *id, int x, int y, int aSizePx, int isvert);

extern int zxwScrollBarIsVertical (ZXWidget *g);
extern int zxwScrollBarPos (ZXWidget *g);
extern int zxwScrollBarMin (ZXWidget *g);
extern int zxwScrollBarMax (ZXWidget *g);
extern int zxwScrollBarSetPos (ZXWidget *g, int aPos);
extern int zxwScrollBarSetMin (ZXWidget *g, int aVal);
extern int zxwScrollBarSetMax (ZXWidget *g, int aVal);
extern int zxwScrollBarSetMinMax (ZXWidget *g, int aMin, int aMax);
extern ZXWidget *zxwScrollBarLinked (ZXWidget *g);
extern int zxwScrollBarSetLinked (ZXWidget *g, ZXWidget *link);

extern int zxwScrollBarPageSize (ZXWidget *g);
extern int zxwScrollBarSetPageSize (ZXWidget *g, int pgsz);

extern int zxwScrollBarKnobPos (ZXWidget *g);
extern int zxwScrollBarKnobSize (ZXWidget *g);
extern int zxwScrollBarSetKnobPos (ZXWidget *g, int pos);


////////////////////////////////////////////////////////////////////////////////
// TODO: horizontal scrollbar
// default: no scrolls
extern ZXWidget *zxwNewListBox (ZXWindow *w, const char *id,
  int x, int y, int aw, int ah);

extern int zxwListBoxIsHBar (ZXWidget *g);
extern int zxwListBoxIsVBar (ZXWidget *g);
// call zxwListBoxSetXBar() after changing listbox position or size
extern int zxwListBoxSetHBar (ZXWidget *g, int onflag); // <0: error
extern int zxwListBoxSetVBar (ZXWidget *g, int onflag); // <0: error

// multiselect
extern int zxwListBoxIsMulti (ZXWidget *g);
extern int zxwListBoxSetMulti (ZXWidget *g, int multi);

extern int zxwListBoxCount (ZXWidget *g); // <0: error
extern const char *zxwListBoxItem (ZXWidget *g, int idx); // NULL: error
extern int zxwListBoxSetItem (ZXWidget *g, int idx, const char *txt); // <0: error
extern int zxwListBoxDeleteItem (ZXWidget *g, int idx); // <0: error
extern int zxwListBoxInsertItem (ZXWidget *g, int idx, const char *txt); // <0: error; before idx
extern int zxwListBoxAddItem (ZXWidget *g, const char *txt); // <0: error

extern int zxwListBoxIsMarked (ZXWidget *g, int idx); // <0: error
extern int zxwListBoxSetMarked (ZXWidget *g, int idx, int mark); // <0: error

extern int zxwListBoxCurrent (ZXWidget *g); // <0: error
extern int zxwListBoxSetCurrent (ZXWidget *g, int idx); // <0: error


////////////////////////////////////////////////////////////////////////////////
// TODO
extern ZXWidget *zxwNewTextEdit (ZXWindow *w, const char *id, int x, int y, int aw, int ah);

extern const char *zxwTextEditText (ZXWidget *g); // NULL: error
extern int zxwTextEditSetText (ZXWidget *g, const char *txt); // <0: error

extern int zxwTextEditCurPos (ZXWidget *g);
extern int zxwTextEditCurCol (ZXWidget *g);
extern int zxwTextEditCurRow (ZXWidget *g);


#ifdef __cplusplus
}
#endif
#endif
