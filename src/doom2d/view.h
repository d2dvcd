/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// View functions
#ifndef D2D_VIEW_H
#define D2D_VIEW_H

#include <stdio.h>

#include "common.h"
//#include "game.h"
#include "player.h"


#define CELW    (8)
#define CELH    (8)
#define MAXTXW  (16)
#define MAXTXH  (8)

#define Z_MAP2SCR_X(_x)  ((_x)-w_viewofs_x+w_view_wdt/2)
#define Z_MAP2SCR_Y(_y)  ((_y)-w_viewofs_y+w_view_hgt/2+w_viewofs_vert)

#define Z_SCR2MAP_X(_x)  (((_x)+w_viewofs_x)-w_view_wdt/2)
#define Z_SCR2MAP_Y(_y)  (((_y)+w_viewofs_y)-w_view_hgt/2-w_viewofs_vert)


extern ccBool r_lighting;

extern ccBool cheatNoBack, cheatNoFront;

extern uint32_t walf[256];
extern vgaimg *walp[256];
extern vgaimg ltn[2][2];
extern int sky_type;

extern uint8_t walswp[256];
//extern ccBool g_dm, r_drawsky;
extern uint8_t g_st;
extern uint8_t g_map;
extern uint32_t g_time;
//extern pos_t dm_pos[];
//extern int dm_pnum, dm_pl1p, dm_pl2p;

extern int FLDW, FLDH; // 100x100

extern uint8_t *fldb;
extern uint8_t *fldf;
extern uint8_t *fldm;

#define FIELD_MAP(_x_, _y_)  (((uint8_t *)fldm)[(_y_)*FLDW+(_x_)])
#define FIELD_TBG(_x_, _y_)  (((uint8_t *)fldb)[(_y_)*FLDW+(_x_)])
#define FIELD_TFG(_x_, _y_)  (((uint8_t *)fldf)[(_y_)*FLDW+(_x_)])

extern int w_viewofs_vert, w_viewofs_x, w_viewofs_y;
extern int w_view_wdt;
extern int w_view_hgt;


extern void W_set_field_size (int w, int h);

extern void W_draw (player_t *p);
extern void W_act (void);

extern void W_init (void);

extern void G_init (void);
extern void G_start (void);
extern void G_act (void);
extern void G_draw (void);

extern void Z_light_start (void); // shoud be called before 'act'

extern void Z_reset_lights (void);
extern int Z_add_light_sector_mr (int x, int y, int radius, int as, int ae, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha, int minrad);
extern int Z_add_light_mr (int x, int y, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha, int minrad);
extern int Z_add_light_sector (int x, int y, int radius, int as, int ae, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha);
extern int Z_add_light (int x, int y, int radius, Uint8 r, Uint8 g, Uint8 b, Uint8 alpha);
extern void Z_render_lights (void);

//HACK! HACK! HACK!
extern void Z_reset_level_lights (void);
extern void Z_fix_level_lights (void);

extern void lb_reset (int resize); // for vga.c


#endif
