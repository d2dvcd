/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Miscellaneous functions
#ifndef D2D_MISC_H
#define D2D_MISC_H

#include <stdarg.h>

#include "common.h"
#include "sound.h"
#include "vga.h"


enum {
  Z_HITWALL = D2DBIT(0),
  Z_HITCEIL = D2DBIT(1),
  Z_HITLAND = D2DBIT(2),
  Z_FALLOUT = D2DBIT(3),
  Z_INWATER = D2DBIT(4),
  Z_HITWATER = D2DBIT(5),
  Z_HITAIR = D2DBIT(6),
  Z_BLOCK = D2DBIT(7)
};


enum {
  HIT_SOME,
  HIT_ROCKET,
  HIT_BFG,
  HIT_TRAP,
  HIT_WATER,
  HIT_ELECTRO,
  HIT_FLAME
};


typedef enum {
  D2D_LEVEL_EXIT_NONE = 0,
  D2D_LEVEL_EXIT_NORMAL = 1,
  D2D_LEVEL_EXIT_SECRET = 2,
  D2D_LEVEL_EXIT_WINGAME = 3,
  D2D_LEVEL_EXIT_WINGAME_ROBO = 4,
  D2D_LEVEL_EXIT_MAXVALUE
} d2dLevelExit;


typedef enum {
  OBJT_NONE,
  OBJT_DOT,
  OBJT_SMOKE,
  OBJT_ITEM,
  OBJT_PROJECTILE,
  OBJT_MONSTER,
  OBJT_PLAYER
} ObjectType;


// object data structure
typedef struct GCC_PACKED {
  ObjectType type;
  int x, y;   // coordinates
  int xv, yv; // velocity
  int vx, vy;
  int r, h;   // radius, height
} obj_t;


typedef struct GCC_PACKED {
  int x, y;
  uint8_t d;
} pos_t;


static inline int Z_sign (int n) { return (n < 0 ? -1 : (n > 0 ? 1 : 0)); }

static inline int Z_dec (int n, int delta) {
  if (abs(n) > delta) {
    if (n > 0) return n-delta;
    if (n < 0) return n+delta;
  }
  return 0;
}


extern void Z_getspr (vgaimg *img, const char n[4], int s, int d, char *dir);
extern const snd_t *Z_getsnd (const char[6]);
extern int Z_sound (const snd_t *s, int v);

extern void Z_drawspr (int x, int y, vgaimg *p, char d);
extern void Z_drawspr_lit (int x, int y, vgaimg *p, char d);
extern void Z_drawmanspr (int x, int y, vgaimg *p, char d, uint8_t color);
extern void Z_drawstair (int);
extern void Z_drawstprcnt (int y, int n);
extern void Z_drawstnum (int n);
extern void Z_drawstwpn (int n, int a);
extern void Z_drawstkeys (uint8_t k);
extern void Z_drawstlives (int n);

// trace ray
// in, out: map pixel coords
// bmask!0: bit mask for BM_get()
// fldhit can't be NULL
// result: CC_TRUE if trace ends inside the line
//         if we traced out of level, return CC_FALSE too, ex and ey will be set to xend and yend
ccBool Z_traceray (int xstart, int ystart, int xend, int yend, int *ex, int *ey,
  int bmask, int (*fldhit)(int mappixx, int mappixy));

// view.c
extern void Z_drawfld (const uint8_t *fld, int bg);

// monster.c
extern int Z_chktrap (int, int d, int o, int t);
extern int Z_hit (obj_t *o, int d, int own, int t);
extern int Z_gunhit (int x, int y, int objid_shooter, int xv, int yv);
extern int Z_hitobj (int id, int d, int o, int t);
extern void Z_explode (int x, int y, int rad, int own);
extern void Z_bfg9000 (int x, int y, int own);

// switch.c
extern void Z_water_trap (const obj_t *o);
extern void Z_untrap (uint8_t t);

extern int Z_getobjpos (int, obj_t *o);

extern int Z_canstand (ObjectType otype, int x, int y, int r);
extern int Z_cangodown (ObjectType otype, int x, int y, int r);
extern int Z_canfit (ObjectType otype, int x, int y, int r, int h);
extern int Z_hitceil (ObjectType otype, int x, int y, int r, int h);

extern void Z_teleobj (int o, int x, int y);
extern int Z_moveobj (obj_t *);
extern int Z_overlap (const obj_t *, const obj_t *);
extern int Z_look (const obj_t *, const obj_t *, int);
extern void Z_kickobj (obj_t *, int, int, int);
extern int Z_cansee (int, int, int, int);
extern int Z_istrapped (int, int, int, int);
extern int Z_inwater (int x, int y, int r, int h);
extern int Z_canbreathe (int x, int y, int r, int h);
extern int Z_getacid (int x, int y, int r, int h);

extern void Z_splash (const obj_t *p, int n);

extern void Z_set_speed (obj_t *o, int s);

extern void Z_calc_time (uint32_t t, uint16_t *h, uint16_t *m, uint16_t *s);

// print to malloc()ed buffer
// don't forget to free() it!
extern char *Z_vasprintf (const char *fmt, va_list vaorig);
extern char *Z_sprintf (const char *fmt, ...) GCC_PRINTF(1,2);

extern int z_print_x, z_print_y;
extern int z_print_xstart; // for '\r' and '\n'

extern void Z_initst (void);
extern void Z_gotoxy (int x, int y);
//extern void Z_putbfch (int c);
//extern void Z_putsfch (int c);
extern void Z_printbf (const char *fmt, ...) GCC_PRINTF(1,2);
extern void Z_printsf (const char *fmt, ...) GCC_PRINTF(1,2);
extern int Z_strfwidthbf (const char *fmt, ...) GCC_PRINTF(1,2);
extern int Z_strfwidthsf (const char *fmt, ...) GCC_PRINTF(1,2);

extern void Z_clrst (void);


#endif
