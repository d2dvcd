/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
static void normal_game_loop (void) {
  dlogfX("normal_game_loop: enter\n");
  while (n_host == NULL) {
    Uint32 t;
    //
    S_updatemusic();
    widgetsUpdateActiveFlag();
    //
    pl[0].w_prev_x = pl[0].w_new_x;
    pl[0].w_prev_y = pl[0].w_new_y;
    //
    g_frame_advanced = 1;
    g_frame_ticks_start = SDL_GetTicks();
    G_act();
    draw_all();
    g_frame_advanced = 0;
    //
    dump_frame_time();
    //
    if ((t = SDL_GetTicks()) < g_frame_ticks_start+DELAY) {
      if (g_st == GS_GAME && !g_trans) {
        ++frame_count;
        ticks_used += t-g_frame_ticks_start;
        ticks_free += DELAY-(t-g_frame_ticks_start);
      }
      //
      while (t < g_frame_ticks_start+DELAY) {
        sdl_process_events();
        //
        if ((t = SDL_GetTicks()) < g_frame_ticks_start+DELAY) {
          if (g_edit_mode || r_interpolate_panning) {
            G_act();
            draw_all();
            if ((t = SDL_GetTicks()) < g_frame_ticks_start+DELAY) {
              SDL_Delay(1);
            }
          } else {
            Uint32 left = g_frame_ticks_start+DELAY-t;
            //
            if (left > 1) {
              if (left > 10) left = 10;
              SDL_Delay(left-1);
            }
          }
          t = SDL_GetTicks();
        }
      }
    } else {
      sdl_process_events();
      //
      if (g_st == GS_GAME && !g_trans) {
        vo_turtle_alpha = 255;
        ++frame_count;
        ticks_used += t-g_frame_ticks_start;
      }
    }
  }
  dlogfX("normal_game_loop: leave\n");
}
