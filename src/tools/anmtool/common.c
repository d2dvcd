/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "common.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef _WIN32
# include <sys/stat.h>
# include <sys/types.h>
#endif

////////////////////////////////////////////////////////////////////////////////
int xmkdir (const char *name) {
#ifndef _WIN32
  return mkdir(name, S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH);
#else
  return mkdir(name);
#endif
}


int fexists (const char *filename) {
  return (access(filename, R_OK) == 0);
}


////////////////////////////////////////////////////////////////////////////////
const char *get_bin_dir (void) {
  static char myDir[8192];
  static int myDirInited = 0;
  //
  if (!myDirInited) {
#ifndef _WIN32
    memset(myDir, 0, sizeof(myDir));
    if (readlink("/proc/self/exe", myDir, sizeof(myDir)-1) < 0) {
      strcpy(myDir, ".");
    } else {
      char *p = (char *)strrchr(myDir, '/');
      //
      if (p == NULL) strcpy(myDir, "."); else *p = '\0';
    }
#else
    char *p;
    //
    memset(myDir, 0, sizeof(myDir));
    GetModuleFileName(GetModuleHandle(NULL), myDir, sizeof(myDir)-1);
    for (p = myDir; *p; ++p) if (*p == '\\') *p = '/';
    p = strrchr(myDir, '/');
    if (p == NULL) strcpy(myDir, "."); else *p = '\0';
#endif
    myDirInited = 1;
  }
  //
  return myDir;
}


const char *get_home_dir (void) {
#ifndef _WIN32
  return get_bin_dir();
#else
  return getenv("HOME");
#endif
}


////////////////////////////////////////////////////////////////////////////////
void fputqstr (FILE *fo, const char *str) {
  fputc('"', fo);
  if (str != NULL) {
    while (*str) {
      if (*str == '\\' || *str == '"') fputc('\\', fo);
      fputc(*str, fo);
      ++str;
    }
  }
  fputc('"', fo);
}


////////////////////////////////////////////////////////////////////////////////
const char *file_get_ext (const char *fname) {
  if (fname != NULL && fname[0]) {
    const char *e = strrchr(fname, '.');
    //
    if (e != NULL && strrchr(e+1, '/') == NULL) {
      return (e > fname && e[-1] == '/' ? NULL : e);
    }
  }
  //
  return NULL;
}


const char *file_get_name_only (const char *fname, int *len) {
  if (fname != NULL && fname[0]) {
    const char *fn = strrchr(fname, '/'), *e;
    //
    if (fn++ == NULL) fn = fname;
    e = strrchr(fn, '.');
    if (e == NULL || e == fn) e = fn+strlen(fn);
    //
    if (len != NULL) *len = e-fn;
    return fn;
  }
  //
  if (len != NULL) *len = 0;
  return NULL;
}
