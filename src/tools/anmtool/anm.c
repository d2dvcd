/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#include "anm.h"

#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"


////////////////////////////////////////////////////////////////////////////////
uint8_t anm_frame[SCRW*SCRH];


static void putpixel (int x, int y, uint8_t clr) {
  if (x < 0 || y < 0 || x >= SCRW || y >= SCRH) abort();
  anm_frame[y*SCRW+x] = clr;
}


////////////////////////////////////////////////////////////////////////////////
#define SQ      (4)
#define ANIWDT  (320)
#define ANIHGT  (200)


////////////////////////////////////////////////////////////////////////////////
static uint8_t cnum[256];


static void init_cpack (void) {
  for (int f = 0; f < 256; ++f) cnum[f] = f;
}


static inline uint8_t cunpack (uint8_t n) {
  uint8_t c = cnum[n];
  //
  if (n) memmove(cnum+1, cnum, n);
  cnum[0] = c;
  //
  return c;
}


static const uint8_t *unpack (int x, int y, const uint8_t *s, int l) {
  //
  void putpix (uint8_t c) {
    putpixel(x+(SCRW-ANIWDT)/2, y+(SCRH-ANIHGT)/2, c);
    if ((++x) >= ANIWDT) { x = 0; ++y; }
  }
  //
  //dlogf("unpack: x=%d; y=%d; len=%d", x, y, l);
  y += (x/ANIWDT);
  x %= ANIWDT;
  for (; y < ANIHGT && l > 0; --l, ++s) {
    uint8_t c = cunpack(*s);
    //
    if (s[0] == 0) {
      int cnt = *(++s);
      //
      //dlogf("repeat: cnt=%d; c=%u", cnt+1, c);
      l -= cnt;
      while (cnt-- >= 0) putpix(c);
    } else {
      //
      //dlogf("one: c=%u", c);
      putpix(c);
    }
  }
  //
  return s;
}


static const uint8_t *line (int y, const uint8_t *u) {
  for (int x = 0; x < ANIWDT/SQ; ) {
    if (u[0]&0x80) {
      int n = ((*u++)&0x7F)+1;
      //
      for (int sy = 0; sy < SQ; ++sy) u = unpack(x*SQ, y+sy, u, n*SQ);
      x += n;
    } else {
      x += (*u++)+1;
    }
  }
  //
  return u;
}


static const anm_blk_t *anm = NULL;
static const void *rsrc = NULL;


int ANM_start (uint8_t *data, int datalen) {
  memset(anm_frame, 0, sizeof(anm_frame));
  init_cpack();
  anm = rsrc = data;
  return 0;
}


void ANM_close (void) {
  if (rsrc != NULL) {
    anm = NULL;
    rsrc = NULL;
  }
}


int ANM_nextframe (void) {
  const uint8_t *u;
  //
  //if (anm != NULL) dlogf("anm=%p; len=%u; type=%d; st=%d", anm, anm->len, anm->type, anm->st);
  //
  if (anm == NULL || anm->type == AB_END) {
    //if (anm != NULL) dlogf("ANM complete");
    ANM_close();
    return 0;
  }
  //
  u = (const uint8_t *)(&anm->data);
  switch (anm->type) {
    case AB_SCREEN:
      unpack(0, 0, u, 64000);
      break;
    case AB_UPDATE:
      for (int y = 0; y < ANIHGT/SQ; ) {
        if (u[0]&0x80) {
          for (int x = ((*u++)&0x7F)+1; x != 0; --x, ++y) u = line(y*SQ, u);
        } else {
          y += *(u++)+1;
        }
      }
      break;
    default:
      fprintf(stderr, "FATAL: invalid ANM-file!\n");
      abort();
  }
  //
  anm = ((const anm_blk_t *)((const uint8_t *)anm+anm->len))+1;
  //
  return 1;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  uint8_t *data;
  int used;
  int size;
} Buffer;


//FIXME: check for 'out of memory'
static void buffer_init (Buffer *buf) {
  memset(buf, 0, sizeof(*buf));
}


static void buffer_free (Buffer *buf) {
  if (buf->data != NULL) free(buf->data);
  memset(buf, 0, sizeof(*buf));
}


static void buffer_clear (Buffer *buf) {
  buf->used = 0;
}


static void buffer_add (Buffer *buf, const void *data, int datalen) {
  if (datalen > 0) {
    if (buf->used+datalen > buf->size) {
      int newsz = buf->size+((datalen+0x7fff)&~0x7fff)+0x8000;
      //
      buf->data = realloc(buf->data, newsz);
      buf->size = newsz;
    }
    memmove(buf->data+buf->used, data, datalen);
    buf->used += datalen;
  }
}


////////////////////////////////////////////////////////////////////////////////
static anm_blk_t blk;
//static uint32_t bofs;
static uint8_t upd[(SCRW/SQ)*(SCRH/SQ)];
static uint8_t lnu[SCRH/SQ];
static uint8_t prev[SCRW*SCRH];
static Buffer packbufs[2], *packbuf;
static int first_frame;


static uint8_t cpack (uint8_t c) {
  uint8_t n = cnum[c];
  //
  for (int i = 0; i < 256; ++i) if (cnum[i] < n) ++cnum[i];
  cnum[c] = 0;
  //
  return n;
}


static void startblk (int t, int st) {
  //bofs = ftell(oh)+4;
  buffer_clear(packbuf);
  blk.type = t;
  blk.st = st;
  blk.len = 0;
  buffer_add(packbuf, &blk, sizeof(blk));
}


static void endblk (void) {
  /*
  long o = ftell(oh);
  fseek(oh, bofs, SEEK_SET);
  fwrite(&blk.len, 4, 1, oh);
  fseek(oh, o, SEEK_SET);
  */
  packbuf->data[4] = blk.len&0xff;
  packbuf->data[5] = (blk.len>>8)&0xff;
  packbuf->data[6] = (blk.len>>16)&0xff;
  packbuf->data[7] = (blk.len>>24)&0xff;
}


static void wrblk (const void *p, uint32_t l) {
  //fwrite(p, l, 1, oh);
  buffer_add(packbuf, p, l);
  blk.len += l;
}


static void packb (const uint8_t *p, uint32_t l) {
  uint8_t c, r;
  //
  for (r = 0; l; --l, ++p) {
    c = cpack(*p);
    if (c) {
      if (r) {
        wrblk("", 1);
        --r;
        wrblk(&r, 1);
        r = 0;
      }
      wrblk(&c, 1);
    } else if (++r == 0) {
      wrblk("\0\xFF", 2);
    }
  }
  if (r) {
    wrblk("", 1);
    --r;
    wrblk(&r, 1);
    r = 0;
  }
}


static void packln (int y) {
  int s, e, n;
  //
  for (s = 0; s < SCRW/SQ; ) {
    for (e = s+1; e < SCRW/SQ && upd[s+y] == upd[e+y] && e-s < 128; ++e) ;
    n = e-s-1;
    if (upd[s+y]) {
      n |= 0x80;
      wrblk(&n, 1);
      for (n = 0; n < SQ; ++n) packb(anm_frame+y*SQ*SQ+n*SCRW+s*SQ, (e-s)*SQ);
      s = e;
    } else {
      wrblk(&n, 1);
      s = e;
    }
  }
}


static void pack (void) {
  int s, e, n;
  //
  for (s = 0; s < SCRH/SQ; ) {
    for (e = s+1; e < SCRH/SQ && lnu[s] == lnu[e] && e-s < 128; ++e) ;
    n = e-s-1;
    if (lnu[s]) {
      n |= 0x80;
      wrblk(&n, 1);
      for (; s < e; ++s) packln(s*SCRW/SQ);
    } else {
      wrblk(&n, 1);
      s = e;
    }
  }
}


void ANM_pack_init (void) {
  buffer_init(&packbufs[0]);
  buffer_init(&packbufs[1]);
  packbuf = &packbufs[0];
  //
  init_cpack();
  //
  first_frame = 1;
}


void *ANM_pack_next (int *framesize) {
  uint8_t cnum_save[256], cnum_as[256];
  //
  memcpy(cnum_save, cnum, 256);
  packbuf = &packbufs[0];
  startblk(AB_SCREEN, 0);
  packb(anm_frame, SCRW*SCRH);
  endblk();
  memcpy(cnum_as, cnum, 256);
  //
  if (!first_frame) {
    memset(upd, 0, (SCRW/SQ)*(SCRH/SQ));
    memset(lnu, 0, SCRH/SQ);
    //
    memcpy(cnum, cnum_save, 256);
    for (int y = 0; y < SCRH; ++y) {
      for (int x = 0; x < SCRW; ++x) {
        if (anm_frame[y*SCRW+x] != prev[y*SCRW+x]) {
          upd[y/SQ*SCRW/SQ+x/SQ] = 1;
          lnu[y/SQ] = 1;
        }
      }
    }
    //
    packbuf = &packbufs[1];
    startblk(AB_UPDATE, 0);
    pack();
    endblk();
    //
    if (packbufs[0].used < packbufs[1].used) {
      printf(" using 'screen' instead of 'diff'\n");
      packbuf = &packbufs[0];
      memcpy(cnum, cnum_as, 256);
    }
  } else {
    first_frame = 0;
  }
  //
  memcpy(prev, anm_frame, SCRW*SCRH);
  //
  *framesize = packbuf->used;
  return packbuf->data;
}


void *ANM_pack_done (int *framesize) {
  packbuf = &packbufs[0];
  startblk(AB_END, 0);
  endblk();
  //
  *framesize = packbuf->used;
  return packbuf->data;
}


void ANM_pack_close (void) {
  buffer_free(&packbuf[0]);
  buffer_free(&packbuf[1]);
}
