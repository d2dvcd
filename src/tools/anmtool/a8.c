#include <string.h>

#include "a8.h"

#include "common.h"
#include "anm.h"


#define WAIT_SZ  (400000)

//extern uint8_t gamcor[5][64];


#define A8_ID  (0xA8)

enum {
  A8C_ENDFR,
  A8C_PAL,
  A8C_CLRSCR,
  A8C_DRAW,
  A8C_FILL,
  A8C_DRAW2C,
  A8C_DRAW2
};

typedef struct GCC_PACKED {
  uint8_t id;
  uint8_t ver;
  int16_t width;
  int16_t height;
  int16_t frames;
  int32_t maxfsize;
  int32_t f1size;
} a8_hdr_t;

#define SQ  (4)

static int sqw, sqh;

static uint8_t *frp, sqc[2][50][80];
static int frame;
static a8_hdr_t ah;


static const int8_t *unpack (void *dp, const void *pp, int l) {
  char *d = (char *)dp;
  const int8_t *p = (const int8_t *)pp;
  //
  for (; l > 0; ) {
    if (*p > 0) {
      memcpy(d, p+1, *p);
      d += *p;
      l -= *p;
      p += *p+1;
    } else if (*p < 0) {
      memset(d, p[1], -*p);
      d += -*p;
      l -= -*p;
      p += 2;
    } else {
      return p+1;
    }
  }
  return p;
}


static const uint8_t *draw (const int8_t *p) {
  int x, y, sy, yc, n;
  //
  for (y = 0; y < sqh; ) {
    if (*p > 0) {
      for (yc = *p++; yc; --yc, ++y) {
        for (x = 0; x < sqw; ) {
          if (*p > 0) {
            n = (*p++)*SQ;
            for (sy = 0; sy < SQ; ++sy) {
              p = unpack(anm_frame+(y*SQ+sy)*320+x*SQ, p, n);
            }
            x += n/SQ;
          } else {
            x += -*p++;
          }
        }
      }
    } else {
      y += -*p++;
    }
  }
  return (const uint8_t *)p;
}


static void putpixel (int x, int y, uint8_t clr) {
  if (x < 0 || y < 0 || x >= SCRW || y >= SCRH) return;
  anm_frame[y*SCRW+x] = clr;
}


static void V_clr (int x, int w, int y, int h, uint8_t clr) {
  for (int dy = 0; dy < h; ++dy) {
    for (int dx = 0; dx < w; ++dx) {
      putpixel(x+dx, y+dy, clr);
    }
  }
}


static const uint8_t *fill (const int8_t *p) {
  int x, y, yc, n;
  //
  for (y = 0; y < sqh; ) {
    if (*p > 0) {
      for (yc = *p++; yc; --yc, ++y) {
        for (x = 0; x < sqw; ) {
          if (*p > 0) {
            for (n = *p++; n; --n, ++p, ++x) V_clr(x*SQ, SQ, y*SQ, SQ, *p);
          } else {
            x += -*p++;
          }
        }
      }
    } else {
      y += -*p++;
    }
  }
  return (const uint8_t *)p;
}


static const uint8_t *draw2c (const int8_t *p) {
  int x, y, sx, sy, yc, n;
  uint16_t w;
  //
  for (y = 0; y < sqh; ) {
    if (*p > 0) {
      for (yc = *p++; yc; --yc, ++y) {
        for (x = 0; x < sqw; ) {
          if (*p > 0) {
            for (n = *p++; n; --n, ++x) {
              sqc[0][y][x] = *p++;
              sqc[1][y][x] = *p++;
              w = *(uint16_t *)p;
              p += 2;
              for (sy = 0; sy < SQ; ++sy) {
                for (sx = 0; sx < SQ; ++sx, w >>= 1) {
                  anm_frame[(y*SQ+sy)*320+x*SQ+sx] = sqc[w&1][y][x];
                }
              }
            }
          } else {
            x += -*p++;
          }
        }
      }
    } else {
      y += -*p++;
    }
  }
  return (const uint8_t *)p;
}


static const uint8_t *draw2 (const int8_t *p) {
  int x, y, sx, sy, yc, n;
  uint16_t w;
  //
  for (y = 0; y < sqh;) {
    if (*p > 0) {
      for (yc = *p++; yc; --yc, ++y) {
        for (x = 0; x < sqw; ) {
          if (*p > 0) {
            for (n = *p++; n; --n, ++x) {
              w = *(uint16_t *)p;
              p += 2;
              for (sy = 0; sy < SQ; ++sy) {
                for (sx = 0; sx < SQ; ++sx, w >>= 1) {
                  anm_frame[(y*SQ+sy)*320+x*SQ+sx] = sqc[w&1][y][x];
                }
              }
            }
          } else {
            x += -*p++;
          }
        }
      }
    } else {
      y += -*p++;
    }
  }
  return (const uint8_t *)p;
}


////////////////////////////////////////////////////////////////////////////////
static int fdptr;
static uint8_t *fdata;


static void f_close (void) {
  if (fdata) {
    //free(fdata);
    fdata = NULL;
  }
}


void A8_close (void) {
  f_close();
  if (frp) frp = NULL;
}


int A8_nextframe (void) {
  const uint8_t *p;
  int /*i,*/ j;
  int len;
  int res = 1;
  //
  len = *(int *)(fdata+fdptr);
  fdptr += 4;
  len -= 4;
  if (len <= 0) {
    A8_close();
    return 0;
  }
  p = fdata+fdptr;
  fdptr += len;
  for (; *p; ) {
    switch (*p++) {
      case A8C_PAL:
        //fprintf(stderr, "FRAME: A8C_PAL\n");
        /*i = *p++;*/++p;
        j = *p++;
        if (!j) j = 256;
        //fprintf(stderr, " palette: [%d..%d]\n", i, i+j-1);
        //for (k = 0; k < j*3; ++k) p[k] = gamcor[3][p[k]];
        //VP_set(p, i, j);  // j colors starting from i
        p += j*3;
        res = -1;
        break;
      case A8C_CLRSCR:
        //fprintf(stderr, "FRAME: A8C_CLRSCR\n");
        V_clr(0, ah.width, 0, ah.height, *p++);
        break;
      case A8C_DRAW:
        //fprintf(stderr, "FRAME: A8C_DRAW\n");
        p = draw((const void *)p);
        break;
      case A8C_FILL:
        //fprintf(stderr, "FRAME: A8C_FILL\n");
        p = fill((const void *)p);
        break;
      case A8C_DRAW2C:
        //fprintf(stderr, "FRAME: A8C_DRAW2C\n");
        p = draw2c((const void *)p);
        break;
      case A8C_DRAW2:
        //fprintf(stderr, "FRAME: A8C_DRAW2\n");
        p = draw2((const void *)p);
        break;
      default:
        fprintf(stderr, "FATAL: shitfuck!\n");
        abort();
    }
  }
  ++frame;
  return res;
}


int A8_start (uint8_t *data, int datalen) {
  memset(anm_frame, 0, sizeof(anm_frame));
  fdata = data;
  frp = data;
  memcpy(&ah, data, sizeof(ah)-4);
  if (ah.id != A8_ID || ah.ver != 0) {
    fprintf(stderr, "WARNING: invalid A8!\n");
    return -1;
  }
  fdata = data+sizeof(ah)-4;
  sqw = ah.width/SQ;
  sqh = ah.height/SQ;
  frame = -1;
  return 0;
}
