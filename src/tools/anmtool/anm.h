/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
#ifndef D2D_ANM_H
#define D2D_ANM_H

#include "common.h"


////////////////////////////////////////////////////////////////////////////////
extern uint8_t anm_frame[SCRW*SCRH];
extern FILE *oh;


////////////////////////////////////////////////////////////////////////////////
enum {
  AB_END,
  AB_SCREEN,
  AB_UPDATE
};


typedef struct GCC_PACKED {
  uint16_t type;
  int16_t st;
  uint32_t len;
  uint8_t data[0];
} anm_blk_t;


////////////////////////////////////////////////////////////////////////////////
extern int ANM_start (uint8_t *data, int datalen);
extern int ANM_nextframe (void); //0: end of animation
extern void ANM_close (void);


extern void ANM_pack_init (void);
extern void *ANM_pack_next (int *framesize);
extern void *ANM_pack_done (int *framesize);
extern void ANM_pack_close ();


#endif
