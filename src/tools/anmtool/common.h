/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 *
 * Visit the site of the original author:
 * http://ranmantaru.com/
 */
// Globals
#ifndef D2D_COMMON_H
#define D2D_COMMON_H

#define d2d_assert_static(_e)  do { \
  enum { d2d_assert_static__ = 1/(_e) }; \
} while (0)


#ifdef _WIN32
#include <windows.h>
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


#define D2DBIT(bitn)  (1<<(bitn))

#define GCC_PACKED    __attribute__((packed)) __attribute__((gcc_struct))
#define GCC_NORETURN  __attribute__((noreturn))

#define GCC_PRINTF(_f,_a)  __attribute__((format(printf,_f,_a)))


#define K8MIN(__a,__b)  ({typeof(__a)_a = (__a); typeof(__b)_b = (__b); _a < _b ? _a : _b; })
#define K8MAX(__a,__b)  ({typeof(__a)_a = (__a); typeof(__b)_b = (__b); _a > _b ? _a : _b; })


#define SCRW  (320)
#define SCRH  (200)


extern int xmkdir (const char *name);
extern int fexists (const char *filename);

extern const char *get_bin_dir (void);
extern const char *get_home_dir (void);

extern const char *file_get_ext (const char *fname); // can return NULL
extern const char *file_get_name_only (const char *fname, int *len); // can return NULL


#endif
