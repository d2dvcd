#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "a8.h"
#include "anm.h"
#include "cmdline.h"


static void *loadWhileFile (const char *fname, int *fsize) {
  FILE *fl;
  void *res;
  //
  *fsize = 0;
  if ((fl = fopen(fname, "rb")) == NULL) return NULL;
  //
  fseek(fl, 0, SEEK_END);
  *fsize = ftell(fl);
  fseek(fl, 0, SEEK_SET);
  //
  res = malloc(*fsize+1);
  fread(res, *fsize, 1, fl);
  fclose(fl);
  //
  return res;
}


int main (int argc, char *argv[]) {
#ifdef _WIN32
  cmdLineParse();
  argc = k8argc;
  argv = k8argv;
#endif
  static char onamebuf[8192], tplfile[8192];
  //
  if (argc < 3) {
    printf(
      "usage: anmtool <cmd> <args>\n"
      "commands:\n"
      "  u infile          -- unpack ANM/A8\n"
      "  p outfile tplfile -- pack ANM\n"
      "\n"
      "example:\n"
      "  anmtool u DARTS.A8\n"
      "  anmtoll p DARTS.ANM DARTS\n"
    );
    return 1;
  }
  //
  if (strcasecmp(argv[1], "u") == 0) {
    const char *ext;
    int frt;
    int anmsize;
    uint8_t *anm;
    int frameno = 0;
    int isa8;
    //
    void write_frame (void) {
      FILE *fo;
      //
      sprintf(onamebuf, "%s%05d.raw", tplfile, frameno);
      if ((fo = fopen(onamebuf, "wb")) == NULL) {
        printf("FATAL: can't create output file: '%s'\n", onamebuf);
        exit(1);
      }
      if (fwrite(anm_frame, 320*200, 1, fo) != 1) {
        printf("FATAL: can't write to output file: '%s'\n", onamebuf);
        exit(1);
      }
      fclose(fo);
      ++frameno;
    }
    //
    if ((ext = file_get_ext(argv[2])) != NULL) {
      sprintf(tplfile, "%s", argv[2]);
      strrchr(tplfile, '.')[0] = 0;
    } else {
      sprintf(tplfile, "%s", argv[2]);
    }
    //
    anm = loadWhileFile(argv[2], &anmsize);
    //
    if (anm == NULL || anmsize < 16) {
      printf("FATAL: can't open input file: '%s'\n", argv[2]);
      return 1;
    }
    //
    isa8 = (anm[0] == 0xA8 && anm[1] == 0);
    //
    frt = (isa8 ? A8_start : ANM_start)(anm, anmsize);
    //
    if (frt > 0) write_frame();
    while ((frt = (isa8 ? A8_nextframe : ANM_nextframe)()) != 0) {
      if (frt < 0) continue;
      printf("\rframe: %d", frameno); fflush(stdout);
      write_frame();
    }
    printf("\r%d frames unpacked.\n", frameno);
  } else if (strcasecmp(argv[1], "p") == 0) {
    FILE *fo;
    int frameno = 0;
    void *frm;
    int frmsize;
    //
    if (argc < 4) {
      printf("FATAL: WTF?!\n");
      return 1;
    }
    if ((fo = fopen(argv[2], "wb")) == NULL) {
      printf("FATAL: can't create output file: '%s'\n", argv[2]);
      return 1;
    }
    //
    ANM_pack_init();
    //
    for (;;) {
      void *raw_frm;
      int raw_frmsize;
      //
      sprintf(onamebuf, "%s%05d.raw", argv[3], frameno);
      if (!fexists(onamebuf)) break;
      raw_frm = loadWhileFile(onamebuf, &raw_frmsize);
      if (raw_frm == NULL && raw_frmsize != 320*200) {
        printf("FATAL: invalid RAW file!\n");
        return 1;
      }
      memcpy(anm_frame, raw_frm, 320*200);
      free(raw_frm);
      //
      frm = ANM_pack_next(&frmsize);
      if (fwrite(frm, frmsize, 1, fo) != 1) {
        printf("FATAL: file writing error!\n");
        return 1;
      }
      ++frameno;
    }
    frm = ANM_pack_done(&frmsize);
    if (fwrite(frm, frmsize, 1, fo) != 1) {
      printf("FATAL: file writing error!\n");
      return 1;
    }
    fclose(fo);
    printf("\r%d frames packed.\n", frameno);
  } else {
    printf("FATAL: unknown command!\n");
  }
  //
  return 0;
}
