/*
 * TODO:
 * - Split patterns for convenient editing and in case of rows overflow (>1024)
 * - Fix bug with big delay (xm->dmm->xm(buggy)) (???)
 * Also, search for "TODO" strings
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../../libdmmxm/dmmxm.h"


////////////////////////////////////////////////////////////////////////////////
static void *openFileRO (const char *fname, int *fsize) {
  static const char *sps[] = {
    "%s",
    "%s.dmm",
    "%s.DMM",
    "%s.dmi",
    "%s.DMI",
    "_dmm/%s",
    "_dmm/%s.dmm",
    "_dmm/%s.DMM",
    "_dmm/%s.dmi",
    "_dmm/%s.DMI",
    "_dmi/%s",
    "_dmi/%s.dmm",
    "_dmi/%s.DMM",
    "_dmi/%s.dmi",
    "_dmi/%s.DMI",
  };
  static char buf[4096];
  FILE *fl = NULL;
  long sz;
  void *res;
  //
  for (size_t f = 0; f < sizeof(sps)/sizeof(sps[0]); ++f) {
    sprintf(buf, sps[f], fname);
    if ((fl = fopen(buf, "rb")) != NULL) break;
  }
  //
  if (fl == NULL) { if (fsize != NULL) *fsize = 0; return NULL; }
  //
  fseek(fl, 0, SEEK_END);
  sz = ftell(fl);
  if (fsize != NULL) *fsize = sz;
  fseek(fl, 0, SEEK_SET);
  //
  res = malloc(sz+1);
  fread(res, sz, 1, fl);
  fclose(fl);
  //
  return res;
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  FILE *fo;
  DMMCvtInfo cvt;
  static char oname[4096];
  char *p;
  void *indmm = NULL;
  int indmmsize;
  void *outxm = NULL;
  int outxmsize;
  //
  if (argc < 2) {
    fprintf(stderr, "SHIT!\n");
    return 1;
  }
  //
  if ((indmm = openFileRO(argv[1], &indmmsize)) == NULL) {
    fprintf(stderr, "SHIT!\n");
    return 1;
  }
  //
  p = strrchr(argv[1], '/');
  if (p == NULL) p = argv[1]; else ++p;
  strcpy(oname, p);
  if ((p = strrchr(oname, '.')) != NULL && strcasecmp(p, ".DMM") == 0) strcpy(p, ".xm"); else strcat(oname, ".xm");
  //
  printf("[%s] --> [%s]\n", argv[1], oname);
/*
  fo = fopen(oname, "wb");
  if (argc == 2) {
    int ok = 0;
    int minman = 0x7fffffff;
    int qq = 4;
    //
    for (int Quantization = 4; Quantization < 256; Quantization += 2) {
      int res, mcnt;
      char *mem = NULL;
      size_t memsz = 0;
      FILE *xfo;
      //
      xfo = open_memstream(&mem, &memsz); //FIXME
      //
      fseek(fi, 0, SEEK_SET);
      fprintf(stderr, "trying quantization %d...\n", Quantization);
      //
      dmmCvtInfoInit(&cvt, openFileRO);
      cvt.Quantization = Quantization;
      res = dmm2xm(&cvt, argv[1], fi, xfo);
      mcnt = cvt.mancount;
      dmmCvtInfoDeinit(&cvt);
      fclose(xfo);
      if (mem != NULL) free(mem);
      //
      if (res == 0 && mcnt == 0) {
        fprintf(stderr, "QUANTIZATION: %d\n", Quantization);
        fseek(fi, 0, SEEK_SET);
        dmmCvtInfoInit(&cvt, openFileRO);
        cvt.Quantization = Quantization;
        cvt.brxmWriteLog = 1;
        dmm2xm(&cvt, argv[1], fi, fo);
        dmmCvtInfoDeinit(&cvt);
        ok = 1;
        break;
      }
      if (res == 0 && mcnt < minman) { qq = Quantization; minman = mcnt; }
    }
    //
    if (!ok) {
      fprintf(stderr, "QUANTIZATION: %d\n", qq);
      fseek(fi, 0, SEEK_SET);
      dmmCvtInfoInit(&cvt, openFileRO);
      cvt.Quantization = qq;
      cvt.brxmWriteLog = 1;
      dmm2xm(&cvt, argv[1], fi, fo);
      dmmCvtInfoDeinit(&cvt);
    }
  } else
*/
  {
    int qq = atoi(argc > 1 ? argv[2] : "8");
    //
    if (qq < 1 || qq > 255) qq = 4;
    dmmCvtInfoInit(&cvt, openFileRO);
    cvt.Quantization = qq;
    cvt.brxmWriteLog = 1;
    outxm = dmm2xm(&cvt, argv[1], indmm, indmmsize, &outxmsize);
    dmmCvtInfoDeinit(&cvt);
    //
    if (outxm != NULL) {
      if ((fo = fopen(oname, "wb")) != NULL) {
        fwrite(outxm, outxmsize, 1, fo);
        fclose(fo);
      }
    } else {
      printf("CONVERSION ERROR!\n");
    }
  }
  //
  if (outxm != NULL) free(outxm);
  if (indmm != NULL) free(indmm);
  //
  return 0;
}
