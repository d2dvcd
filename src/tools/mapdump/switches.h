/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef D2D_SWITCHES_H
#define D2D_SWITCHES_H

#include "common.h"


enum {
  SW_NONE,
  SW_EXIT,
  SW_EXITS,
  SW_OPENDOOR,
  SW_SHUTDOOR,
  SW_SHUTTRAP,
  SW_DOOR,
  SW_DOOR5,
  SW_PRESS,
  SW_TELE,
  SW_SECRET,
  SW_LIFTUP,
  SW_LIFTDOWN,
  SW_TRAP,
  SW_LIFT
};


extern const char *getSwitchType (int id);



#endif
