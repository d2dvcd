/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef D2D_FILEIO_H
#define D2D_FILEIO_H

#include <setjmp.h>
#include <stdio.h>

#include "common.h"
#include "map.h"


// flXXX() will do longjmp() on error
extern jmp_buf flJmpBuf; // set this before using flXXX()

// error codes for flXXX()
enum {
  FL_NO_ERROR,
  FL_READ_ERROR,
  FL_WRITE_ERROR,
  FL_OTHER_ERROR
};

extern void flError (int errCode);

extern d2dBool flReadBool (FILE *fl);
extern uint8_t flReadUI8 (FILE *fl);
extern uint16_t flReadUI16 (FILE *fl);
extern uint32_t flReadUI32 (FILE *fl);
extern int8_t flReadI8 (FILE *fl);
extern int16_t flReadI16 (FILE *fl);
extern int32_t flReadI32 (FILE *fl);

extern void flWriteBool (FILE *fl, d2dBool v);
extern void flWriteUI8 (FILE *fl, uint8_t v);
extern void flWriteUI16 (FILE *fl, uint16_t v);
extern void flWriteUI32 (FILE *fl, uint32_t v);
extern void flWriteI8 (FILE *fl, int8_t v);
extern void flWriteI16 (FILE *fl, int16_t v);
extern void flWriteI32 (FILE *fl, int32_t v);

extern void flReadBuf (FILE *fl, void *buf, size_t buflen);
extern void flWriteBuf (FILE *fl, const void *buf, size_t buflen);


////////////////////////////////////////////////////////////////////////////////
typedef int (*F_maploadCB) (FILE *fl, const map_block_t *blk); // return 0 for skipped; <0 for error; >0: loaded
extern void F_registerMapLoad (F_maploadCB lcb);


////////////////////////////////////////////////////////////////////////////////
extern void F_loadmap (FILE *fl);


#endif
