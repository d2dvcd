/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Globals
#ifndef D2D_COMMON_H
#define D2D_COMMON_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define D2DBIT(bitn)  (1<<(bitn))

#define GCC_PACKED    __attribute__((packed))
#define GCC_NORETURN  __attribute__((noreturn))

#define GCC_PRINTF(_f,_a)  __attribute__((format(printf,_f,_a)))


typedef enum {
  D2D_FALSE = 0,
  D2D_TRUE = 1
} d2dBool;


//#define min(__a,__b)  ({typeof(__a)_a = (__a); typeof(__b)_b = (__b); _a < _b ? _a : _b; })
//#define max(__a,__b)  ({typeof(__a)_a = (__a); typeof(__b)_b = (__b); _a > _b ? _a : _b; })

#define lambda(_return_type, _body_and_args) ({ \
  _return_type __fn__ _body_and_args \
  __fn__; \
})


#endif
