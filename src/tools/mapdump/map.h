/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef D2D_MAP_H
#define D2D_MAP_H
/**************************************************************************\
*                                                                          *
*  ������� ���� Doom'� 2D                                       ������ 2   *
*                                                                          *
*  Prikol Software                                           10.VII.1996   *
*                                                                          *
*  ����������� �������� �������������� ���� ����, ��� ������� ����������   *
*  ������ �� Prikol Software, ������ � ���� ������� �����.                 *
*                                                                          *
\**************************************************************************/

#include "common.h"


#define FLDW    (100)
#define FLDH    (100)
#define CELW    (8)
#define CELH    (8)
#define MAXTXW  (16)
#define MAXTXH  (8)


// tile type
enum {
  TILE_EMPTY = 0,
  TILE_WALL = 1,
  TILE_DOORC = 2, // closed door
  TILE_DOORO = 3, // opened door
  TILE_STEP = 4,
  TILE_WATER = 5,
  TILE_ACID1 = 6,
  TILE_ACID2 = 7,
  TILE_MBLOCK = 8, // just blocks monsters
  TILE_LIFTU = 9,
  TILE_LIFTD = 10,
  //
  TILE_ACTTRAP = 255
};


/* ������ ������ - ������ 1.04 alpha (� ������)
 *
 *  �������� ������� (old_wall_t)
 *    ��������� ������ ������� (old_wall_t.n[0] == 0)
 *
 *  ���           - ������ 100x100 ���� - ������ �������
 *
 *  ��� ������    - ������ 100x100 ����:
 *    0 - �����
 *    1 - �����
 *    2 - �������� �����
 *    3 - �������� �����
 *    4 - ���������
 *
 *  �������� ���� - ������ 100x100 ���� - ������ �������
 *
 *  ����, ������� � ��. (old_thing_t)
 *    ��������� ������� ����� (old_thing_t.t == 0)
 *
 *  ������������� (old_switch_t)
 *    ��������� ������� ����� (old_switch_t.t == 0)
 */
#if 0
typedef struct GCC_PACKED {
  char n[8]; // �������� ��������
  uint8_t t;    // ���: 0-�������� 1-"��ۣ���"
} old_wall_t;


typedef struct GCC_PACKED {
  uint8_t x, y; // ����������/8
  uint8_t t;    // ���
  uint8_t tm;   // ������ ���� 0
  uint8_t a, b; // ������ - ����������/8 �����
  uint16_t c;    // �� ������������ (����� ��)
} old_switch_t;
#endif


/* ����� ������ - ������� � ������ 1.05 alpha
 *
 *  ��������� ����� (map_header_t)
 *
 *  ����� (map_block_t)
 *    ��������� ������ MB_END (map_block_t.t == MB_END)
 */

typedef struct GCC_PACKED {
  char id[8]; // "�������" - "Doom2D\x1A"
  uint16_t ver;   // ������ �����
} map_header_t;


typedef struct GCC_PACKED {
  uint16_t t;  // ��� �����
  uint16_t st; // ������ (����� ��������, ��������) ���� �� ������������, �� ������ ���� 0 (��� ������� �������������)
  int32_t sz;  // ������ (������� ���� ����� ���� ���������)
} map_block_t;


enum {
  MB_COMMENT = -1,
  MB_END = 0,
  MB_WALLNAMES,
  MB_BACK,
  MB_WTYPE,
  MB_FRONT,
  MB_THING,
  MB_SWITCH,
  MB_MUSIC,
  MB_SKY,
  MB_SWITCH2,
  MB__UNKNOWN
};


/* ������ 0  (Doom2D ������ 1.05 alpha)
 *
 *  MB_COMMENT - �����������
 *
 *  MB_WALLNAMES - �������� ������� (��. ������ ������)
 *    ���������� - �� ������� �����
 *
 *  MB_BACK, MB_WTYPE, MB_FRONT - ���, ���, �������� ���� (��. ������ ������)
 *    ������ 0 - ��� �������� (��� � ������ ������)
 *
 *  MB_THING - ����, ������� � ��. (��. ������ ������)
 *    ���������� - �� ������� �����
 *
 *  MB_SWITCH - ������������� (��. ������ ������)
 *    ���������� - �� ������� �����
 */

/* ������ 1  (Doom2D ������ 1.06 alpha)
 *
 *  MB_WALLNAMES
 *    ��������� ������-�������� _WATER_*
 *      ��� * ��� 0 = ����, 1 = �������, 2 = �����
 *
 *  MB_WTYPE
 *    �������� ����� ��� 5 - ����
 *
 *  MB_MUSIC - ����� ���� - �������� ������ (8 ����)
 *
 *  MB_SKY - ����� ���� - ��� ���� (2 ����� - short)
 *    1 = ������
 *    2 = �����
 *    3 = ��
 */

enum {
  SW_PL_PRESS = D2DBIT(0),
  SW_MN_PRESS = D2DBIT(1),
  SW_PL_NEAR = D2DBIT(2),
  SW_MN_NEAR = D2DBIT(3),
  SW_KEY_R = D2DBIT(4),
  SW_KEY_G = D2DBIT(5),
  SW_KEY_B = D2DBIT(6)
};


typedef struct GCC_PACKED {
  int16_t x, y; // ����������
  uint16_t t;    // ���
  uint16_t f;     // �����
} map_thing_t;


typedef struct GCC_PACKED {
  uint8_t x, y; // ����������/8
  uint8_t t;    // ���
  uint8_t tm;   // ������ ���� 0
  uint8_t a, b; // ������ - ����������/8 �����
  uint16_t c;    // �� ������������ (����� ��)
  uint8_t f;    // �����
} switch2_t;

/* ������ 2  (Doom2D ������ 1.17 alpha)
 *
 *  ���� MB_SWITCH ������� �� MB_SWITCH2 (��. switch2_t)
 */


typedef struct GCC_PACKED {
  char n[8];
  uint8_t t;
} wall_t;


#define LAST_MAP_VER  (2)  // ����� ��������� ������ �����


#endif
