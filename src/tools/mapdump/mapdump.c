/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "common.h"
#include "fileio.h"
#include "map.h"
#include "switches.h"
#include "things.h"
#include "translit.h"


static FILE *dumpfl = NULL;


static uint8_t fldb[FLDH][FLDW];
static uint8_t fldf[FLDH][FLDW];
static uint8_t fld[FLDH][FLDW];
static uint16_t skynum;
static char music[9];
static map_thing_t *things = 0;
static int thingCount = 0;
static switch2_t *switches = 0;
static int switchCount = 0;
static wall_t *wallt = NULL;
static int walltCount = 0;


static void lvlClear (void) {
  memset(fldb, 0, sizeof(fldb));
  memset(fldf, 0, sizeof(fldf));
  memset(fld, 0, sizeof(fld));
  skynum = 0;
  memset(music, 0, sizeof(music));
  if (things != NULL) free(things);
  things = NULL;
  thingCount = 0;
  if (switches != NULL) free(switches);
  switches = NULL;
  switchCount = 0;
  if (wallt != NULL) free(wallt);
  wallt = NULL;
  walltCount = 0;
}



typedef struct {
  uint16_t id;
  const char *name;
  int optional;
  int loaded;
} MapChunkInfo;


static MapChunkInfo mci[] = {
  {MB_WALLNAMES, "wallnames", 0, 0},
  {MB_MUSIC, "music", 1, 0},
  {MB_SKY, "sky", 1, 0},
  {MB_BACK, "back", 0, 0},
  {MB_WTYPE, "wtype", 0, 0},
  {MB_FRONT, "front", 0, 0},
  {MB_SWITCH2, "switches", 1, 0},
  {MB_THING, "things", 1, 0},
};


static void mciClear (void) {
  for (size_t f = 0; f < sizeof(mci)/sizeof(mci[0]); ++f) mci[f].loaded = 0;
}


static void mciHere (uint16_t id) {
  for (size_t f = 0; f < sizeof(mci)/sizeof(mci[0]); ++f) if (mci[f].id == id) ++mci[f].loaded;
}


/*
static int mciIsHere (uint16_t id) {
  for (size_t f = 0; f < sizeof(mci)/sizeof(mci[0]); ++f) if (mci[f].id == id) return mci[f].loaded;
  return 0;
}
*/


static void mciCheck (void) {
  for (size_t f = 0; f < sizeof(mci)/sizeof(mci[0]); ++f) {
    if (!mci[f].optional && !mci[f].loaded) {
      fprintf(stderr, "FATAL: chunk '%s' is not found!\n", mci[f].name);
      abort();
    }
  }
}


static void xcpy (char *dest, const char *n) {
  memset(dest, 0, 9);
  strncpy(dest, n, 8);
}


static void unpackRLE (void *dest, const void *src, int srclen) {
  const uint8_t *p;
  uint8_t *o;
  //
  for (p = (const uint8_t *)src, o = (uint8_t *)dest; srclen > 0; ++p, --srclen) {
    if (*p == 255) {
      if (srclen >= 3) {
        int n = *((const uint16_t *)(++p));
        //
        if (n > 0) memset(o, *(p += 2), n);
        o += n;
        srclen -= 3;
      } else {
        fprintf(stderr, "FATAL: invalid packed level!\n");
        abort();
      }
    } else {
      *(o++) = *p;
    }
  }
}


static int G_mapload (FILE *fl, const map_block_t *blk) {
  if (blk->t == MB_MUSIC) {
    char mus[9], *trm;
    //
    memset(mus, 0, sizeof(mus));
    flReadBuf(fl, mus, 8);
    trm = transstr(mus);
    fputc('\n', dumpfl);
    fprintf(dumpfl, "music: %s (%s)\n", mus, trm);
    free(trm);
    mciHere(blk->t);
    return 1;
  }
  return 0;
}


static int SW_mapload (FILE *fl, const map_block_t *blk) {
  if (blk->t == MB_SWITCH2) {
    static const char *flagnames[8] = {
      "PLAYER_PRESS",
      "MONSTER_PRESS",
      "PLAYER_NEAR",
      "MONSTER_NEAR",
      "RED_KEY",
      "GREEN_KEY",
      "BLUE_KEY",
      "BIT_7"
    };
    int sz = blk->sz;
    int sw_secrets = 0;
    int swnum;
    //
    fputc('\n', dumpfl);
    for (swnum = 0; sz > 0; sz -= sizeof(switch2_t), ++swnum) {
      switch2_t sw;
      //
      flReadBuf(fl, &sw, sizeof(switch2_t));
      if (sw.t == SW_SECRET) ++sw_secrets;
      /*
      sw[swused].tm = 0;
      sw[swused].d = 0;
      sw[swused].f |= 0x80;
      */
      fprintf(dumpfl, "switch #%d: x=%u; y=%u; type=%s; tm=%u; a=%u; b=%u; c=%u; f=0x%02x",
        swnum, sw.x, sw.y, getSwitchType(sw.t), sw.tm, sw.a, sw.b, sw.c, sw.f);
      for (int f = 0; f < 8; ++f) {
        if (sw.f&(1<<f)) fprintf(dumpfl, " %s", flagnames[f]);
      }
      fputc('\n', dumpfl);
    }
    mciHere(blk->t);
    return 1;
  }
  return 0;
}


static int IT_mapload (FILE *fl, const map_block_t *blk) {
  if (blk->t == MB_THING) {
    int sz = blk->sz;
    //
    fputc('\n', dumpfl);
    for (int i = 0; sz > 0; ++i, sz -= 8) {
      char tpb[128];
      const D2DThingInfo *nfo;
      const char *name;
      map_thing_t t;
      //
      flReadBuf(fl, &t, sizeof(t));
      if (t.t == 0) break; // no more things
      //
      if ((nfo = findThingInfo(t.t)) != NULL) {
        name = nfo->name;
      } else {
        sprintf(tpb, "{%u}", t.t);
        name = tpb;
      }
      fprintf(dumpfl, "item #%d: %s; x=%d; y=%d; flags:", i, name, t.x, t.y);
      fprintf(dumpfl, " %s", (t.f&THF_DIR ? "RIGHT" : "LEFT"));
      if (t.f&THF_DM) fprintf(dumpfl, " DM");
      t.f &= ~0x11;
      if (t.f) {
        fprintf(dumpfl, " (0x%04x)", t.f);
      }
      fputc('\n', dumpfl);
    }
    mciHere(blk->t);
    return 1;
  }
  //
  return 0;
}


static int W_mapload (FILE *fl, const map_block_t *blk) {
  int i;
  wall_t w;
  uint8_t *p, *buf;
  uint16_t sky_type;
  int sz;
  char nn[9];
  int istypes = 0;
  //
  switch (blk->t) {
    case MB_WALLNAMES:
      fputc('\n', dumpfl);
      sz = blk->sz;
      for (i = 1; i < 256 && sz > 0; ++i, sz -= sizeof(w)) {
        flReadBuf(fl, &w, sizeof(w));
        xcpy(nn, w.n);
        fprintf(dumpfl, "wall #%02x: t=0x%02x; %s\n", i, w.t, nn);
      }
      mciHere(blk->t);
      return 1;
    case MB_BACK:
      fprintf(dumpfl, "\nback_tiles\n");
      p = (void *)fldb;
      goto unp;
    case MB_WTYPE:
      fprintf(dumpfl, "\nwall_types\n");
      istypes = 1;
      p = (void *)fld;
      goto unp;
    case MB_FRONT:
      fprintf(dumpfl, "\nfront_tiles\n");
      p = (void *)fldf;
unp:  switch (blk->st) {
        case 0:
          flReadBuf(fl, p, FLDW*FLDH);
          break;
        case 1:
          if (!(buf = malloc(blk->sz))) fprintf(stderr, "FATAL: Out of memory");
          flReadBuf(fl, buf, blk->sz);
          unpackRLE(p, buf, blk->sz);
          free(buf);
          break;
        default: abort();
      }
      //
      for (int y = 0; y < FLDH; ++y) {
        for (int x = 0; x < FLDW; ++x) {
          if (istypes) {
            char name[32];
            //
            switch (*p) {
              case TILE_EMPTY: strcpy(name, ". "); break;
              case TILE_WALL: strcpy(name, "W "); break;
              case TILE_DOORC: strcpy(name, "+ "); break;
              case TILE_DOORO: strcpy(name, "/ "); break;
              case TILE_STEP: strcpy(name, "- "); break;
              case TILE_WATER: strcpy(name, "= "); break;
              case TILE_ACID1: strcpy(name, "* "); break;
              case TILE_ACID2: strcpy(name, "% "); break;
              case TILE_MBLOCK: strcpy(name, "# "); break;
              case TILE_LIFTU: strcpy(name, "^ "); break;
              case TILE_LIFTD: strcpy(name, "V "); break;
              case TILE_ACTTRAP: strcpy(name, "! "); break;
              default: sprintf(name, "%02x", *p); break;
            }
            fprintf(dumpfl, " %s", name);
          } else {
            if (*p) fprintf(dumpfl, " %02x", *p); else fprintf(dumpfl, " ..");
          }
          ++p;
        }
        fputc('\n', dumpfl);
      }
      //
      mciHere(blk->t);
      return 1;
    case MB_SKY:
      sky_type = flReadUI16(fl);
      fprintf(dumpfl, "\nsky: %u\n", sky_type);
      mciHere(blk->t);
      return 1;
  }
  //
  return 0;
}


static void dumpMap (FILE *fl) {
  mciClear();
  lvlClear();
  //
  if (setjmp(flJmpBuf) != 0) {
    fprintf(stderr, "FATAL: map reading error");
    exit(1);
  }
  //
  F_loadmap(fl);
  mciCheck();
}


static void registerMapLoaders (void) {
  F_registerMapLoad(G_mapload);
  F_registerMapLoad(W_mapload);
  F_registerMapLoad(IT_mapload);
  F_registerMapLoad(SW_mapload);
}


int main (int argc, char *argv[]) {
  if (argc < 2) {
    printf("usage: %s map+\n", argv[0]);
    return 1;
  }
  //
  registerMapLoaders();
  //
  for (int f = 1; f < argc; ++f) {
    static char dumpfn[4096];
    FILE *fl;
    //
    if ((fl = fopen(argv[f], "rb")) == NULL) {
      fprintf(stderr, "FATAL: can't open map file: '%s'\n", argv[f]);
      return 1;
    }
    sprintf(dumpfn, "%s.txt", argv[f]);
    if ((dumpfl = fopen(dumpfn, "w")) == NULL) {
      fprintf(stderr, "FATAL: can't create dump file: '%s'\n", dumpfn);
      return 1;
    }
    //
    dumpMap(fl);
    //
    fclose(dumpfl);
    fclose(fl);
  }
  //
  return 0;
}
