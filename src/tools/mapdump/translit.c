/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "translit.h"

#include <ctype.h>


static const struct {
  uint8_t ascii;
  uint8_t asciilc;
  const char *ch;
} atrans[] = {
  {0x80, 0xA0, "a"},//�
  {0x81, 0xA1, "b"},//�
  {0x82, 0xA2, "v"},//�
  {0x83, 0xA3, "g"},//�
  {0x84, 0xA4, "d"},//�
  {0x85, 0xA5, "e"},//�
  {0xF0, 0xF1, "yo"},//�
  {0x86, 0xA6, "zh"},//�
  {0x87, 0xA7, "z"},//�
  {0x88, 0xA8, "i"},//�
  {0x89, 0xA9, "j"},//�
  {0x8A, 0xAA, "k"},//�
  {0x8B, 0xAB, "l"},//�
  {0x8C, 0xAC, "m"},//�
  {0x8D, 0xAD, "n"},//�
  {0x8E, 0xAE, "o"},//�
  {0x8F, 0xAF, "p"},//�
  {0x90, 0xE0, "r"},//�
  {0x91, 0xE1, "s"},//�
  {0x92, 0xE2, "t"},//�
  {0x93, 0xE3, "u"},//�
  {0x94, 0xE4, "f"},//�
  {0x95, 0xE5, "h"},//�
  {0x96, 0xE6, "c"},//�
  {0x97, 0xE7, "ch"},//�
  {0x98, 0xE8, "sh"},//�
  {0x99, 0xE9, "sch"},//�
  {0x9A, 0xEA, "x"},//�
  {0x9B, 0xEB, "y"},//�
  {0x9C, 0xEC, "w"},//�
  {0x9D, 0xED, "e"},//�
  {0x9E, 0xEE, "hu"},//�
  {0x9F, 0xEF, "ja"},//�
  {0}
};


static const char *findTRC (char c) {
  uint8_t cc = c&0xff;
  //
  for (int f = 0; atrans[f].ascii; ++f) {
    if (atrans[f].ascii == cc || atrans[f].asciilc == cc) return atrans[f].ch;
  }
  return NULL;
}


char *transstr (const char *src) {
  char *res, *dest;
  //
  if (src == NULL) src = "";
  res = dest = calloc(strlen(src)*3+2, sizeof(char));
  for (int f = 0; f < strlen(src); f++) {
    const char *ch = findTRC(src[f]);
    //
    if (ch != NULL) {
      for (; *ch; ++ch) *dest++ = *ch;
    } else {
      *dest++ = tolower(src[f]);
    }
  }
  *dest = '\0';
  //
  return res;
}
