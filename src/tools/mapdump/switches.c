/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "switches.h"


typedef struct {
  int id;
  const char *name;
} SwitchInfo;


static const SwitchInfo nfo[] = {
  {SW_NONE, "NONE"},
  {SW_EXIT, "EXIT"},
  {SW_EXITS, "EXITS"},
  {SW_OPENDOOR, "OPENDOOR"},
  {SW_SHUTDOOR, "SHUTDOOR"},
  {SW_SHUTTRAP, "SHUTTRAP"},
  {SW_DOOR, "DOOR"},
  {SW_DOOR5, "DOOR5"},
  {SW_PRESS, "PRESS"},
  {SW_TELE, "TELE"},
  {SW_SECRET, "SECRET"},
  {SW_LIFTUP, "LIFTUP"},
  {SW_LIFTDOWN, "LIFTDOWN"},
  {SW_TRAP, "TRAP"},
  {SW_LIFT, "LIFT"},
};


const char *getSwitchType (int id) {
  static char buf[512];
  //
  for (size_t f = 0; f < sizeof(nfo)/sizeof(nfo[0]); ++f) {
    if (nfo[f].id == id) return nfo[f].name;
  }
  sprintf(buf, "{%d}", id);
  return buf;
}
