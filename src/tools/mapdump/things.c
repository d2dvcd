/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Prikol Software 1996-1997
 * Copyright (C) Aleksey Volynskov 1996-1997
 * Copyright (C) <ARembo@gmail.com> 2011
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "things.h"


static const D2DThingInfo nfo[] = {
  {TH_NONE, "NONE"},
  {TH_PLR1, "PLR1"},
  {TH_PLR2, "PLR2"},
  {TH_DMSTART, "DMSTART"},

  {TH_CLIP, "CLIP"},
  {TH_SHEL, "SHEL (4)"},
  {TH_ROCKET, "ROCKET"},
  {TH_CELL, "CELL"},
  {TH_AMMO, "AMMO (box)"},
  {TH_SBOX, "SBOX (25 shells)"},
  {TH_RBOX, "RBOX (5 rockets)"},
  {TH_CELP, "CELP"},
  {TH_STIM, "STIM"},
  {TH_MEDI, "MEDI (big)"},
  {TH_BPACK, "BPACK"},
  {TH_CSAW, "CSAW"},
  {TH_SGUN, "SGUN"},
  {TH_SGUN2, "SGUN2 (double-barreled)"},
  {TH_MGUN, "MGUN"},
  {TH_LAUN, "LAUN"},
  {TH_PLAS, "PLAS"},
  {TH_BFG, "BFG"},
  {TH_ARM1, "ARM1 (green)"},
  {TH_ARM2, "ARM2 (blue)"},
  {TH_MEGA, "MEGA"},
  {TH_INVL, "INVL"},
  {TH_AQUA, "AQUA (aqualung)"},
  {TH_RKEY, "RKEY"},
  {TH_GKEY, "GKEY"},
  {TH_BKEY, "BKEY"},
  {TH_SUIT, "SUIT (envirosuit)"},
  {TH_SUPER, "SUPER (100% health sphere)"},
  {TH_RTORCH, "RTORCH"},
  {TH_GTORCH, "GTORCH"},
  {TH_BTORCH, "BTORCH"},
  {TH_GOR1, "GOR1"},
  {TH_FCAN, "FCAN (flaming barrel)"},
  {TH_GUN2, "GUN2 (supermachinegun)"},

  {TH_DEMON, "DEMON"},
  {TH_IMP, "IMP"},
  {TH_ZOMBY, "ZOMBY"},
  {TH_SERG, "SERG"},
  {TH_CYBER, "CYBER"},
  {TH_CGUN, "CGUN"},
  {TH_BARON, "BARON"},
  {TH_KNIGHT, "KNIGHT"},
  {TH_CACO, "CACO"},
  {TH_SOUL, "SOUL"},
  {TH_PAIN, "PAIN"},
  {TH_SPIDER, "SPIDER"},
  {TH_BSP, "BSP (arachnotron)"},
  {TH_MANCUB, "MANCUB"},
  {TH_SKEL, "SKEL"},
  {TH_VILE, "VILE"},
  {TH_FISH, "FISH"},
  {TH_BARREL, "BARREL"},
  {TH_ROBO, "ROBO"},
  {TH_MAN, "MAN (jocker)"},
};


const D2DThingInfo *findThingInfo (int id) {
  for (size_t f = 0; f < sizeof(nfo)/sizeof(nfo[0]); ++f) {
    if (nfo[f].id == id) return &nfo[f];
  }
  return NULL;
}
