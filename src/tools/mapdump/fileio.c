/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "fileio.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>

#include "common.h"
#include "map.h"


////////////////////////////////////////////////////////////////////////////////
jmp_buf flJmpBuf;


void flError (int errCode) {
  longjmp(flJmpBuf, errCode);
}


void flReadBuf (FILE *fl, void *buf, size_t buflen) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (buflen > 0) {
    if (buf == NULL) flError(FL_OTHER_ERROR);
    if (fread(buf, buflen, 1, fl) != 1) flError(FL_READ_ERROR);
  }
}


void flWriteBuf (FILE *fl, const void *buf, size_t buflen) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (buflen > 0) {
    if (buf == NULL) flError(FL_OTHER_ERROR);
    if (fwrite(buf, buflen, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


d2dBool flReadBool (FILE *fl) {
  uint8_t res;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fread(&res, 1, 1, fl) != 1) flError(FL_READ_ERROR);
  return (res ? D2D_TRUE : D2D_FALSE);
}


uint8_t flReadUI8 (FILE *fl) {
  uint8_t res;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fread(&res, 1, 1, fl) != 1) flError(FL_READ_ERROR);
  return res;
}


uint16_t flReadUI16 (FILE *fl) {
  uint16_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((uint16_t)b)<<(f*8));
  }
  return res;
}


uint32_t flReadUI32 (FILE *fl) {
  uint32_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((uint32_t)b)<<(f*8));
  }
  return res;
}


int8_t flReadI8 (FILE *fl) {
  int8_t res;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fread(&res, 1, 1, fl) != 1) flError(FL_READ_ERROR);
  return res;
}


int16_t flReadI16 (FILE *fl) {
  int16_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((int16_t)b)<<(f*8));
  }
  return res;
}


int32_t flReadI32 (FILE *fl) {
  int32_t res = 0;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) flError(FL_READ_ERROR);
    res |= (((int32_t)b)<<(f*8));
  }
  return res;
}


void flWriteBool (FILE *fl, d2dBool v) {
  uint8_t b;
  //
  if (fl == NULL) flError(FL_OTHER_ERROR);
  b = (v ? 255 : 0);
  if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
}


void flWriteUI8 (FILE *fl, uint8_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fwrite(&v, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
}


void flWriteUI16 (FILE *fl, uint16_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


void flWriteUI32 (FILE *fl, uint32_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


void flWriteI8 (FILE *fl, int8_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  if (fwrite(&v, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
}


void flWriteI16 (FILE *fl, int16_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 2; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


void flWriteI32 (FILE *fl, int32_t v) {
  if (fl == NULL) flError(FL_OTHER_ERROR);
  for (int f = 0; f < 4; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) flError(FL_WRITE_ERROR);
  }
}


////////////////////////////////////////////////////////////////////////////////
#define MAX_MAP_LOADERS (64)

static F_maploadCB maploadCB[MAX_MAP_LOADERS];
static int maploaderCount = 0;


void F_registerMapLoad (F_maploadCB lcb) {
  if (maploaderCount >= MAX_MAP_LOADERS) {
    fprintf(stderr, "FATAL: too many map loaders\n");
    abort();
  }
  maploadCB[maploaderCount++] = lcb;
}


////////////////////////////////////////////////////////////////////////////////
void F_loadmap (FILE *fl) {
  map_header_t hdr;
  //
  flReadBuf(fl, &hdr, sizeof(hdr));
  if (memcmp(hdr.id, "Doom2D\x1A", 8) != 0) {
    fprintf(stderr, "FATAL: not a level\n");
    flError(FL_OTHER_ERROR);
  }
  if (hdr.ver != LAST_MAP_VER) {
    fprintf(stderr, "FATAL: invalid level version: %u\n", hdr.ver);
    flError(FL_OTHER_ERROR);
  }
  //
  for (;;) {
    map_block_t blk;
    uint32_t nbofs;
    int blkok = 0;
    //
    flReadBuf(fl, &blk, sizeof(blk));
    nbofs = ftell(fl)+blk.sz;
    if (blk.t == MB_END) break;
    if (blk.t == MB_COMMENT) {
      fprintf(stderr, "=== COMMENT ===\n");
      for (; blk.sz > 0; --blk.sz) {
        uint8_t ch;
        //
        flReadBuf(fl, &ch, 1);
        if (ch < 32 || ch == 127) fprintf(stderr, "{%u}", ch); else fprintf(stderr, "%c", ch);
      }
      fprintf(stderr, "---\n");
    } else {
      for (int f = 0; f < maploaderCount; ++f) {
        int res = (maploadCB[f])(fl, &blk);
        //
        if (res < 0) {
          fprintf(stderr, "FATAL: error loading map block");
          flError(FL_OTHER_ERROR);
        }
        if (res > 0) {
          blkok = 1;
          break;
        }
      }
      if (!blkok) {
        fprintf(stderr, "WARNING: skipped unknown block (%d:%d)", blk.t, blk.st);
      }
    }
    fseek(fl, nbofs, SEEK_SET);
  }
}
