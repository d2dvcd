/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#ifndef _BSD_SOURCE
# define _BSD_SOURCE
#endif
#include <endian.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "../../libwdx/libwdx.h"


static void reportError (void) {
  fprintf(stderr, "ERROR: %s\n", strerror(errno));
}


static void *loadWholeFile (const char *fname, int *fsize) {
  void *res = NULL;
  off_t fsz;
  int fd = open(fname, O_RDONLY);
  //
  if (fd < 0) goto error;
  fsz = lseek(fd, 0, SEEK_END);
  if (fsz <= (off_t)-1) goto error;
  if (fsz > 0x7fffffff) { fprintf(stderr, "ERROR: file too big\n"); goto error1; }
  if (lseek(fd, 0, SEEK_SET) <= (off_t)-1) goto error;
  res = malloc(fsz+1);
  if (!res) goto error;
  if (fsz > 0) {
    if (read(fd, res, fsz) != fsz) goto error;
  }
  if (close(fd) < 0) { fd = -1; goto error; }
  *fsize = fsz;
  return res;
error:
  reportError();
error1:
  if (fd >= 0) close(fd);
  if (res) free(res);
  return NULL;
}


static int writeUInt (int fd, uint32_t i) {
  uint32_t v = htole32(i);
  //
  if (write(fd, &v, 4) != 4) { reportError(); return -1; }
  return 0;
}


static uint32_t getUInt (const void *buf) {
  uint32_t res;
  //
  memcpy(&res, buf, 4);
  return le32toh(res);
}


static int writeWDX (const char *fname, const void *pbuf, int pSize, int origSize, uint32_t crc) {
  static char *sign = "WDX0";
  int fd;
  //
  if (pSize < 0) return -1;
  fd = open(fname, O_CREAT | O_WRONLY | O_TRUNC, 0644);
  if (fd < 0) goto error;
  if (write(fd, sign, 4) != 4) goto error;
  if (writeUInt(fd, (uint32_t)origSize)) goto error1;
  if (writeUInt(fd, crc)) goto error1;
  if (writeUInt(fd, (uint32_t)pSize)) goto error1;
  if (write(fd, pbuf, pSize) != pSize) goto error1;
  if (close(fd) < 0) { unlink(fname); fd = -1; goto error; }
  return 0;
error:
  reportError();
error1:
  if (fd >= 0) {
    close(fd);
    unlink(fname);
  }
  return -1;
}


static int pack (const char *fin, const char *fout) {
  uint32_t crc;
  void *ibuf, *obuf;
  int isz, osz, psz;
  //
  if (!(ibuf = loadWholeFile(fin, &isz))) {
    fprintf(stderr, "ERROR: can't read input file: %s\n", fin);
    return 1;
  }
  osz = wdxPackBufferSize(isz);
  obuf = malloc(osz);
  if (!obuf) {
    free(ibuf);
    fprintf(stderr, "ERROR: out of memory!\n");
    return 1;
  }
  crc = wdxCRC32(ibuf, isz);
  fprintf(stdout, "packing %d bytes ... ", isz); fflush(stdout);
  psz = wdxPack(obuf, osz, ibuf, isz);
  free(ibuf);
  if (psz < 0) {
    free(obuf);
    fprintf(stdout, "FAILED!\n");
    fprintf(stderr, "ERROR: can't pack!\n");
    return 1;
  }
  fprintf(stdout, "got %d packed bytes\n", psz); fflush(stdout);
  osz = writeWDX(fout, obuf, psz, isz, crc);
  free(obuf);
  if (osz) {
    fprintf(stderr, "ERROR: can't write output file: %s\n", fout);
    return 1;
  }
  return 0;
}


static int unpack (const char *fin, const char *fout) {
  uint32_t crc, ncrc;
  uint8_t *ibuf, *obuf, *origBuf;
  int isz, osz, psz, xsz;
  int fo = -1;
  //
  if (!(ibuf = loadWholeFile(fin, &isz))) {
    fprintf(stderr, "ERROR: can't read input file: %s\n", fin);
    return 1;
  }
  if (isz < 16 || ibuf[0] != 'W' || ibuf[1] != 'D' || ibuf[2] != 'X' || ibuf[3] != '0') {
    fprintf(stderr, "ERROR: invalid input file: %s\n", fin);
    return 1;
  }
  origBuf = ibuf;
  ibuf += 4; /* skip signature */
  osz = getUInt(ibuf); ibuf += 4; /* unpacked size */
  crc = getUInt(ibuf); ibuf += 4; /* crc */
  psz = getUInt(ibuf); ibuf += 4; /* packed size */
  obuf = malloc(osz);
  if (!obuf) {
    free(origBuf);
    fprintf(stderr, "ERROR: out of memory!\n");
    return 1;
  }
  fprintf(stdout, "unpacking %d bytes to %d bytes ... ", psz, osz); fflush(stdout);
  xsz = wdxUnpack(obuf, osz, ibuf, psz);
  free(origBuf);
  if (xsz < 0 || xsz != osz) {
    free(obuf);
    fprintf(stdout, "FAILED!\n");
    fprintf(stderr, "ERROR: can't unpack! (%d)\n", xsz);
    return 1;
  }
  ncrc = wdxCRC32(obuf, xsz);
  if (ncrc != crc) {
    free(obuf);
    fprintf(stdout, "FAILED!\n");
    fprintf(stderr, "ERROR: CRC check failed!\n");
    return 1;
  }
  fprintf(stdout, "OK\n");
  fo = open(fout, O_CREAT | O_WRONLY | O_TRUNC, 0644);
  if (fo < 0) {
    reportError();
    fprintf(stderr, "ERROR: can't write output file: %s\n", fout);
    goto error;
  }
  if (write(fo, obuf, xsz) != xsz) {
    reportError();
    fprintf(stderr, "ERROR: can't write output file (1): %s\n", fout);
    goto error;
  }
  if (close(fo) < 0) {
    reportError();
    fprintf(stderr, "ERROR: can't write output file (2): %s\n", fout);
    unlink(fout);
    fo = -1;
    goto error;
  }
  free(obuf);
  return 0;
error:
  if (fo > 0) {
    close(fo);
    unlink(fout);
  }
  free(obuf);
  return 0;
}


int main (int argc, char *argv[]) {
  const char *inname = NULL, *outname = NULL;
  //
  if (argc == 3) {
    inname = argv[2];
    outname = argv[2];
  } else if (argc > 3) {
    inname = argv[2];
    outname = argv[3];
  } else {
    fprintf(stderr, "usage: %s <c|d> infile [outfile]\n", argv[0]);
    return 1;
  }
  if (!strcmp("c", argv[1]) || !strcmp("-c", argv[1])) return pack(inname, outname);
  return unpack(inname, outname);
}
