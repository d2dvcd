/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>


////////////////////////////////////////////////////////////////////////////////
static int flWriteUI32 (FILE *fl, uint32_t v) {
  if (fl == NULL) return -1;
  for (int f = 0; f < 4; ++f, v >>= 8) {
    uint8_t b = (v&0xff);
    //
    if (fwrite(&b, 1, 1, fl) != 1) return -1;
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct WADLump {
  uint32_t ofs;
  uint32_t size;
  char name[9];
  struct WADLump *next;
} WADLump;

static WADLump *lhead = NULL, *ltail = NULL;


////////////////////////////////////////////////////////////////////////////////
#define UNP_DIR  "_unp"


int main (int argc, char *argv[]) {
  static char dir[4096], listfile[4096], fname[4096];
  int retcode = 1;
  int lineno = 0;
  const char *wadfname = NULL;
  FILE *flist, *wadfo;
  const char *p;
  int isIWAD = 0;
  char sign[4];
  uint32_t fileCount = 0, dirofs = 0;
  //
  if (argc != 3) {
    printf("usage: wadpack srcdir outfile.wad\n");
    return 1;
  }
  //
  sprintf(dir, argv[1]);
  while (dir[0] && dir[strlen(dir)-1] == '/') dir[strlen(dir)-1] = '\0';
  if (!dir[0]) strcpy(dir, ".");
  //
  sprintf(listfile, "%s/00filelist.lst", dir);
  if ((flist = fopen(listfile, "r")) == NULL) {
    fprintf(stderr, "FATAL: can't open LST file: '%s'\n", listfile);
    return 1;
  }
  //
  wadfname = argv[2];
  if ((wadfo = fopen(wadfname, "wb")) == NULL) {
    fclose(flist);
    fprintf(stderr, "FATAL: can't create WAD file: '%s'\n", wadfname);
    return 1;
  }
  //
  if ((p = strrchr(wadfname, '/')) == NULL) p = wadfname; else ++p;
  if (strcasecmp(p, "doom2d.wad") == 0) isIWAD = 1;
  //
  // write WAD header
  sign[0] = (isIWAD ? 'I' : 'P');
  sign[1] = 'W';
  sign[2] = 'A';
  sign[3] = 'D';
  if (fwrite(sign, 4, 1, wadfo) != 1) goto writerror;
  // placeholders
  if (flWriteUI32(wadfo, 0) != 0) goto writerror; // filecount
  if (flWriteUI32(wadfo, 0) != 0) goto writerror; // diroffset
  //
  while (fgets(fname, sizeof(fname), flist) != NULL) {
    WADLump *l;
    char *diskfname;
    int isWADDir = 0;
    FILE *fl;
    //
    ++lineno;
    while (fname[0] && isspace(fname[strlen(fname)-1])) fname[strlen(fname)-1] = '\0';
    //
    if (!fname[0] || fname[0] == '#' || fname[0] == ';') continue;
    if (fname[strlen(fname)-1] == '/') {
      isWADDir = 1;
      fname[strlen(fname)-1] = '\0';
    }
    if ((diskfname = strpbrk(fname, "\t ")) != NULL) {
      while (*diskfname && isspace(*diskfname)) ++diskfname;
    }
    if (diskfname == NULL || !diskfname[0]) {
      fprintf(stderr, "FATAL: invalid command in list file at line %d\n", lineno);
      goto quit;
    }
    diskfname[-1] = '\0';
    //
    printf("[%s] --> [%s] (%d)\n", diskfname, fname, isWADDir);
    if (strlen(fname) > 8) {
      fprintf(stderr, "FATAL: invalid WAD name at line %d\n", lineno);
      goto quit;
    }
    l = calloc(1, sizeof(WADLump));
    l->next = NULL;
    l->ofs = ftell(wadfo);
    strcpy(l->name, fname);
    if (!isWADDir) {
      static char tmpname[4096];
      //
      sprintf(tmpname, "%s/%s", dir, diskfname);
      if ((fl = fopen(tmpname, "rb")) == NULL) {
        fprintf(stderr, "FATAL: can't open file '%s'\n", tmpname);
        goto quit;
      }
      fseek(fl, 0, SEEK_END);
      l->size = ftell(fl);
      fseek(fl, 0, SEEK_SET);
      if (l->size > 0) {
        char *buf = malloc(l->size);
        //
        fread(buf, l->size, 1, fl);
        fwrite(buf, l->size, 1, wadfo);
        free(buf);
      }
      fclose(fl);
    } else {
      l->ofs = 0;
      l->size = 0;
    }
    //
    if (ltail != NULL) ltail->next = l; else lhead = l;
    ltail = l;
    ++fileCount;
  }
  // now write dir
  dirofs = ftell(wadfo);
  for (const WADLump *l = lhead; l != NULL; l = l->next) {
    if (flWriteUI32(wadfo, l->ofs) != 0) goto writerror;
    if (flWriteUI32(wadfo, l->size) != 0) goto writerror;
    if (fwrite(l->name, 8, 1, wadfo) != 1) goto writerror;
  }
  // fix header
  fseek(wadfo, 4, SEEK_SET);
  if (flWriteUI32(wadfo, fileCount) != 0) goto writerror;
  if (flWriteUI32(wadfo, dirofs) != 0) goto writerror;
  retcode = 0;
quit:
  if (flist != NULL) fclose(flist);
  fclose(wadfo);
  if (retcode != 0) unlink(wadfname);
  return retcode;
writerror:
  fprintf(stderr, "FATAL: write error!\n");
  goto quit;
}
