/*
 * Doom2D:Vaya Con Dios
 *
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <ctype.h>
#include <iconv.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>


////////////////////////////////////////////////////////////////////////////////
#if 0
static const struct {
  uint8_t ascii;
  uint8_t asciilc;
  const char *ch;
} atrans[] = {
  {0x80, 0xA0, "a"},//�
  {0x81, 0xA1, "b"},//�
  {0x82, 0xA2, "v"},//�
  {0x83, 0xA3, "g"},//�
  {0x84, 0xA4, "d"},//�
  {0x85, 0xA5, "e"},//�
  {0xF0, 0xF1, "yo"},//�
  {0x86, 0xA6, "zh"},//�
  {0x87, 0xA7, "z"},//�
  {0x88, 0xA8, "i"},//�
  {0x89, 0xA9, "j"},//�
  {0x8A, 0xAA, "k"},//�
  {0x8B, 0xAB, "l"},//�
  {0x8C, 0xAC, "m"},//�
  {0x8D, 0xAD, "n"},//�
  {0x8E, 0xAE, "o"},//�
  {0x8F, 0xAF, "p"},//�
  {0x90, 0xE0, "r"},//�
  {0x91, 0xE1, "s"},//�
  {0x92, 0xE2, "t"},//�
  {0x93, 0xE3, "u"},//�
  {0x94, 0xE4, "f"},//�
  {0x95, 0xE5, "h"},//�
  {0x96, 0xE6, "c"},//�
  {0x97, 0xE7, "ch"},//�
  {0x98, 0xE8, "sh"},//�
  {0x99, 0xE9, "sch"},//�
  {0x9A, 0xEA, "x"},//�
  {0x9B, 0xEB, "y"},//�
  {0x9C, 0xEC, "w"},//�
  {0x9D, 0xED, "e"},//�
  {0x9E, 0xEE, "hu"},//�
  {0x9F, 0xEF, "ja"},//�
  {0}
};


static const char *findTRC (char c) {
  uint8_t cc = c&0xff;
  //
  for (int f = 0; atrans[f].ascii; ++f) {
    if (atrans[f].ascii == cc || atrans[f].asciilc == cc) return atrans[f].ch;
  }
  return NULL;
}


static void transstr (char *dest, const char *src) {
  for (int f = 0; f < strlen(src); f++) {
    const char *ch = findTRC(src[f]);
    //
    if (ch != NULL) {
      for (; *ch; ++ch) *dest++ = *ch;
    } else {
      *dest++ = tolower(src[f]);
    }
  }
  *dest = '\0';
}
#endif


////////////////////////////////////////////////////////////////////////////////
static iconv_t cd;


static void icinit (void) {
  cd = iconv_open("KOI8-U//TRANSLIT", "CP866");
  if (cd == (iconv_t)-1) {
    fprintf(stderr, "FATAL ERROR: iconv failed!\n");
    exit(1);
  }
}


static void doconv (char *dest, const char *src) {
  char *outs, *ibuf, *obuf;
  size_t il, ol, ool;
  int len = strlen(src);
  //
  outs = malloc(len*4+4);
  if (outs == NULL) {
    fprintf(stderr, "FATAL ERROR: iconv failed!\n");
    iconv_close(cd);
    exit(1);
  }
  ibuf = (char *)src;
  obuf = outs;
  il = len;
  ool = ol = il*4;
  il = iconv(cd, &ibuf, &il, &obuf, &ol);
  if (il == (size_t)-1) {
    fprintf(stderr, "FATAL ERROR: iconv failed!\n");
    free(outs);
    iconv_close(cd);
    exit(1);
  }
  if (ool-ol > 0) memcpy(dest, outs, ool-ol);
  dest[ool-ol] = 0;
  free(outs);
}


static void icdone (void) {
  iconv_close(cd);
}


////////////////////////////////////////////////////////////////////////////////
static int flReadUI32 (FILE *fl, uint32_t *r) {
  uint32_t res = 0;
  //
  if (r != NULL) *r = 0;
  if (fl == NULL) return -1;
  for (int f = 0; f < 4; ++f) {
    uint8_t b;
    //
    if (fread(&b, 1, 1, fl) != 1) return -1;
    res |= (((uint32_t)b)<<(f*8));
  }
  //
  if (r != NULL) *r = res;
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  uint32_t ofs;
  uint32_t size;
  char name[9];
} WADLump;


typedef struct {
  int count;
  WADLump *dir;
  FILE *fl;
} WAD;


////////////////////////////////////////////////////////////////////////////////
static int wadOpen (WAD *wad, const char *fname) {
  FILE *fl = fopen(fname, "rb");
  //
  wad->count = 0;
  wad->dir = NULL;
  wad->fl = NULL;
  //
  if (fl != NULL) {
    char sign[4];
    uint32_t dcnt, dofs;
    //
    if (fread(sign, 4, 1, fl) != 1) goto error;
    if (memcmp(sign, "IWAD", 4) != 0 && memcmp(sign, "PWAD", 4) != 0) goto error;
    if (flReadUI32(fl, &dcnt) != 0) goto error;
    if (flReadUI32(fl, &dofs) != 0) goto error;
    if (dofs < 12 || dcnt > 0x10000000U) goto error;
    if ((wad->dir = calloc(dcnt, sizeof(WADLump))) == NULL) goto error;
    // read dir
    if (fseek(fl, dofs, SEEK_SET) != 0) goto error;
    for (uint32_t f = 0; f < dcnt; ++f) {
      WADLump *l = wad->dir+f;
      //
      if (flReadUI32(fl, &l->ofs) != 0) goto error;
      if (flReadUI32(fl, &l->size) != 0) goto error;
      if (fread(l->name, 8, 1, fl) != 1) goto error;
      l->name[8] = 0;
    }
    //
    wad->count = dcnt;
    wad->fl = fl;
    //
    return 0;
  }
  //
error:
  if (fl != NULL) fclose(fl);
  if (wad->dir != NULL) free(wad->dir);
  wad->count = 0;
  wad->dir = NULL;
  //
  return -1;
}


static int wadClose (WAD *wad) {
  int res = 0;
  //
  if (wad != NULL && wad->fl != NULL) {
    res = fclose(wad->fl);
    if (wad->dir != NULL) free(wad->dir);
    wad->count = 0;
    wad->dir = NULL;
    wad->fl = NULL;
  }
  //
  return res;
}


////////////////////////////////////////////////////////////////////////////////
static int renameDups = 0;
static int classicDoom = 0;
static int listOnly = 0;
static int inmap = 0;

#define UNP_DIR  "_unp"


static void usage (void) {
  printf(
    "usage: wadunpack [options] file.wad [unpackdir]\n"
    "options:\n"
    "  -l  list wad contents and exit\n"
    "  -d  DooM/DooM2 classic wad, not D2D\n"
    "  -r  rename duplicate files\n"
  );
  exit(1);
}


static char unpdir[4096];
static char cur_dir[4096];
static char list_file_name[4096];

static FILE *flist;

static char *dirs[64];
static int dirdepth = 0;


static void dir_build_name (void) {
  sprintf(cur_dir, "%s", unpdir);
  for (int f = 0; f < dirdepth; ++f) {
    strcat(cur_dir, "/");
    strcat(cur_dir, dirs[f]);
  }
  //printf("cur_dir: [%s]\n", cur_dir);
}


//dirname: *_START
static void dir_enter (const char *dirname) {
  char emark[32];
  //
  if (dirdepth >= sizeof(dirs)/sizeof(dirs[0])) {
    fprintf(stderr, "FATAL: too many nested dirs!\n");
    exit(1);
  }
  printf(">%s\n", dirname);
  //
  strcpy(emark, dirname);
  dirs[dirdepth++] = strdup(emark);
  dir_build_name();
  mkdir(cur_dir, 0755);
  inmap = 0;
  //
  if (flist != NULL) fprintf(flist, "%s %s/\n", dirname, cur_dir+strlen(unpdir)+1);
}


static void dir_leave (const char *dirname) {
  char emark[32], *p;
  //
  if (dirname != NULL) {
    int ok = 0;
    //
    strcpy(emark, dirname);
    //
    if ((p = strrchr(emark, '_')) != NULL && strcasecmp(p, "_END") == 0) strcpy(p, "_START");
    //
    for (int f = dirdepth-1; f >= 0; --f) {
      if (strcasecmp(dirs[f], emark) == 0) { ok = 1; free(dirs[f]); dirdepth = f; break; }
      printf(" <%s\n", dirs[f]);
      free(dirs[f]);
    }
    if (!ok) {
      fprintf(stderr, "FATAL: invalid dir structure in WAD!\n");
      exit(1);
    }
    printf("<%s\n", dirname);
    if (flist != NULL) fprintf(flist, "%s %s/\n", dirname, cur_dir+strlen(unpdir)+1);
  } else {
    printf("<ENDMAP\n");
    if (dirdepth < 1) {
      fprintf(stderr, "FATAL: invalid dir structure in WAD!\n");
      exit(1);
    }
    free(dirs[--dirdepth]);
  }
  inmap = 0;
  dir_build_name();
}


static int isClassicMapFile (const char *name) {
  static const char *nfl[] = {
    "THINGS",
    "LINEDEFS",
    "SIDEDEFS",
    "VERTEXES",
    "SEGS",
    "SSECTORS",
    "NODES",
    "SECTORS",
    "REJECT",
    "BLOCKMAP",
    "BEHAVIOR"
  };
  //
  for (size_t f = 0; f < sizeof(nfl)/sizeof(nfl[0]); ++f) {
    if (strcasecmp(name, nfl[f]) == 0) return 1;
  }
  return 0;
}


static int isDirName (const char *name) {
  if (classicDoom) {
    if (strlen(name) == 5 && strncmp(name, "MAP", 3) == 0 && isdigit(name[3]) && isdigit(name[4])) return 666; // map
    if (strlen(name) == 4 && toupper(name[0]) == 'E' && toupper(name[2]) == 'M' && isdigit(name[1]) && isdigit(name[3])) return 666; // map
  }
  //
  if ((name = strrchr(name, '_')) != NULL) {
    if (strcasecmp(name, "_START") == 0) return 1;
    if (strcasecmp(name, "_END") == 0) return -1;
  }
  //
  return 0;
}


int main (int argc, char *argv[]) {
  int retcode = 1;
  WAD wad;
  const char *inwadname = NULL, *undirname = NULL;
  char *buf = NULL;
  uint32_t obufsz = 0;
  int nomoreopt = 0;
  //
  for (int f = 1; f < argc; ++f) {
    const char *o = argv[f];
    //
    if (!nomoreopt && o[0] == '-') {
      if (strcmp(o, "--") == 0) { nomoreopt = 1; continue; }
      for (++o; *o; ++o) {
        switch (*o) {
          case 'h': usage(); break;
          case 'l': listOnly = 1; break;
          case 'd': classicDoom = 1; break;
          case 'r': renameDups = 1; break;
          default:
            fprintf(stderr, "FATAL: invalid option: '%c'!\n", *o);
            exit(1);
        }
      }
      continue;
    }
    //
    if (inwadname == NULL) {
      inwadname = o;
    } else if (undirname == NULL) {
      undirname = o;
    } else {
      fprintf(stderr, "FATAL: too many file names!\n");
      exit(1);
    }
  }
  //
  if (inwadname == NULL) usage();
  //
  if (undirname != NULL) {
    sprintf(unpdir, undirname);
    while (unpdir[0] && unpdir[strlen(unpdir)-1] == '/') unpdir[strlen(unpdir)-1] = '\0';
    if (!unpdir[0]) strcpy(unpdir, ".");
  } else {
    strcpy(unpdir, UNP_DIR);
  }
  //
  icinit();
  //
  if (wadOpen(&wad, inwadname) != 0) {
    fprintf(stderr, "FATAL: can't open WAD file: '%s'\n", inwadname);
    icdone();
    return 1;
  }
  //
  if (listOnly) {
    for (int f = 0; f < wad.count; ++f) {
      const WADLump *l = wad.dir+f;
      static char name[64];
      //
      doconv(name, l->name);
      printf("%8s %10u\n", name, l->size);
    }
    wadClose(&wad);
    icdone();
    exit(0);
  }
  //
  dir_build_name();
  mkdir(cur_dir, 0755);
  //
  sprintf(list_file_name, "%s/00filelist.lst", cur_dir);
  unlink(list_file_name);
  if ((flist = fopen(list_file_name, "a")) == NULL) {
    fprintf(stderr, "FATAL: can't create lst file!\n");
    goto quit;
  }
  //
  for (int f = 0; f < wad.count; ++f) {
    const WADLump *l = wad.dir+f;
    static char name[64], fname[1024];
    FILE *fo;
    int dirtype;
    //
    if (strpbrk(l->name, "/\t\n ") != NULL) {
      fprintf(stderr, "FATAL: bad file name: '%s'\n", fname);
      goto quit;
    }
    //
    //transstr(name, l->name);
    doconv(name, l->name);
    //printf("%04d: [%s]\n", f, name);
    if ((dirtype = isDirName(l->name)) != 0) {
      if (dirtype == 666) {
        if (inmap) dir_leave(NULL);
        dir_enter(l->name);
        inmap = 1;
      } else if (dirtype < 0) {
        dir_leave(l->name);
      } else if (dirtype > 0) {
        dir_enter(l->name);
      }
      continue;
    }
    //
    if (inmap && !isClassicMapFile(l->name)) dir_leave(NULL);
    //
    sprintf(fname, "%s/%s", cur_dir, name);
    //
    if (access(fname, F_OK) == 0) {
      fprintf(stderr, "SHIT! duplicate file: '%s'\n", fname);
      if (!renameDups) goto quit;
      for (int f = 0; ; ++f) {
        sprintf(fname, "%s/%s.%03d", cur_dir, name, f);
        if (access(fname, F_OK) != 0) break;
      }
    }
    //
    //printf("[%s]\n", fname);
    if ((fo = fopen(fname, "wb")) != NULL) {
      if (l->size != 0) {
        if (l->size > obufsz) {
          if ((buf = realloc(buf, l->size)) == NULL) {
            fclose(fo);
            fprintf(stderr, "SHIT! out of memory!\n");
            goto quit;
          }
          obufsz = l->size;
        }
        if (fseek(wad.fl, l->ofs, SEEK_SET) != 0) {
          fclose(fo);
          fprintf(stderr, "SHIT! fseek() error!\n");
          goto quit;
        }
        if (fread(buf, l->size, 1, wad.fl) != 1) {
          fclose(fo);
          fprintf(stderr, "SHIT! fread() error!\n");
          goto quit;
        }
        if (fwrite(buf, l->size, 1, fo) != 1) {
          fclose(fo);
          fprintf(stderr, "SHIT! fwrite() error!\n");
          goto quit;
        }
        if (fclose(fo) != 0) {
          fprintf(stderr, "SHIT! fclose() error!\n");
          goto quit;
        }
      }
    } else {
      fprintf(stderr, "SHIT! can't create file: '%s'\n", fname);
      goto quit;
    }
    //
    if (flist != NULL) fprintf(flist, "%s %s\n", l->name, fname+strlen(unpdir)+1);
  }
  //
  retcode = 0;
quit:
  if (buf != NULL) free(buf);
  if (flist != NULL) fclose(flist);
  wadClose(&wad);
  icdone();
  return retcode;
}
