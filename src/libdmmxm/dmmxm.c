/*
 * based on Grom PE dmm2xm converter
 * used some code from BeRoXM, Copyright (C) 2005-2007, Benjamin 'BeRo' Rosseaux
 *
 * Copyright (C) Grom PE
 * Copyright (C) Benjamin 'BeRo' Rosseaux 2005-2007
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "dmmxm.h"

#include <math.h>
#include <string.h>


////////////////////////////////////////////////////////////////////////////////
typedef struct __attribute__((packed)) {
  char Signature[17];
  char Name[20];
  uint8_t End1AValue;
  char Tracker[20];
  uint16_t Version;
  uint32_t Size;
  uint16_t TrackLength;
  uint16_t RestartPosition;
  uint16_t NumberOfChannels;
  uint16_t NumberOfPatterns;
  uint16_t NumberOfInstruments;
  uint16_t Flags;
  uint16_t Speed;
  uint16_t Tempo;
  uint8_t PatternOrder[brxmLastPatternOrder+1]; //brxmPatternOrder
} brxmXMHeader;


typedef struct __attribute__((packed)) {
  uint32_t Size;
  char Name[22];
  uint8_t InstrumentType;
  uint16_t CountOfSamples;
} brxmXMInstrumentHeader;


typedef struct __attribute__((packed)) {
  uint32_t Size;
  uint8_t PackingType;
  uint16_t Rows;
  uint16_t PackedSize;
} brxmXMPatternHeader;


typedef struct __attribute__((packed)) {
  uint32_t Size;
  uint8_t SampleMap[96]; //was [1..96], be careful!
  uint16_t VolumeEnvelope[24];
  uint16_t PanningEnvelope[24];
  uint8_t VolumeEnvelopeNumPoints;
  uint8_t PanningEnvelopeNumPoints;
  uint8_t VolumeEnvelopeSustain;
  uint8_t VolumeEnvelopeLoopStart;
  uint8_t VolumeEnvelopeLoopEnd;
  uint8_t PanningEnvelopeSustain;
  uint8_t PanningEnvelopeLoopStart;
  uint8_t PanningEnvelopeLoopEnd;
  uint8_t VolFlags;
  uint8_t PanFlags;
  uint8_t VibratoType;
  uint8_t VibratoSweep;
  uint8_t VibratoDepth;
  uint8_t VibratoRate;
  uint16_t FadeOut;
  uint16_t Reserved;
} brxmXMInstrumentExtraHeader;


typedef struct __attribute__((packed)) {
  uint32_t SampleLength;
  uint32_t LoopStart;
  uint32_t LoopLength;
  uint8_t Volume;
  int8_t FineTune;
  uint8_t Flags;
  uint8_t Panning;
  int8_t RelativeNote;
  uint8_t Reserved;
  char Name[22];
} brxmXMSampleHeader;


////////////////////////////////////////////////////////////////////////////////
/*
function SwapWordBigEndian(Value:uint16_t):uint16_t; register;
{
  Result = ((Value and 0xff00) shr 8) or ((Value and 0xff) shl 8);
}
*/


////////////////////////////////////////////////////////////////////////////////
brxmSample brxm_sample_Create (void) {
  brxmSample smp;
  //
  smp = calloc(1, sizeof(*smp));
  smp->Data = NULL;
  smp->Name = NULL;
  brxm_sample_Clear(smp);
  //
  return smp;
}

void brxm_sample_Destroy (brxmSample smp) {
  if (smp != NULL) {
    brxm_sample_Clear(smp);
    free(smp);
  }
}


void brxm_sample_Clear (brxmSample smp) {
  if (smp != NULL) {
    if (smp->Data != NULL) free(smp->Data);
    smp->Data = NULL;
    smp->Bits = 8;
    smp->Channels = 1;
    if (smp->Name != NULL) free(smp->Name);
    smp->Name = NULL;
    smp->Loop = BXM_FALSE;
    smp->PingPongLoop = BXM_FALSE;
    smp->LoopStart = 0;
    smp->LoopLength = 0;
    smp->Volume = 64;
    smp->Panning = 128;
    smp->RelativeNote = 0;
    smp->FineTune = 0;
    smp->Samples = 0;
  }
}


void brxm_sample_Resize (brxmSample smp, int32_t Samples) {
  if (Samples == 0) {
    if (smp->Data != NULL) free(smp->Data);
    smp->Data = NULL;
  } else {
    brxmSampleData *nd = realloc(smp->Data, Samples*sizeof(brxmSampleValue));
    //
    if (nd == NULL) abort(); //TODO
    if (Samples > smp->Samples) memset(nd+smp->Samples, 0, (Samples-smp->Samples)*sizeof(brxmSampleValue));
    smp->Data = nd;
  }
  smp->Samples = Samples;
}


////////////////////////////////////////////////////////////////////////////////
brxmEnvelope brxm_envelope_Create (void) {
  brxmEnvelope env;
  //
  env = calloc(1, sizeof(*env));
  brxm_envelope_Clear(env);
  //
  return env;
}


void brxm_envelope_Destroy (brxmEnvelope env) {
  if (env != NULL) {
    brxm_envelope_Clear(env);
    free(env);
  }
}


void brxm_envelope_Clear (brxmEnvelope env) {
  if (env != NULL) {
    memset(env, 0, sizeof(*env));
  }
}


////////////////////////////////////////////////////////////////////////////////
brxmInstrument brxm_instrument_Create (void) {
  brxmInstrument ins;
  //
  ins = calloc(1, sizeof(*ins));
  ins->VolumeEnvelope = brxm_envelope_Create();
  ins->PanningEnvelope = brxm_envelope_Create();
  for (int f = 0; f <= brxmLastSample; ++f) ins->Sample[f] = NULL;
  brxm_instrument_Clear(ins);
  //
  return ins;
}


void brxm_instrument_Destroy (brxmInstrument ins) {
  if (ins != NULL) {
    brxm_instrument_Clear(ins);
    brxm_envelope_Destroy(ins->VolumeEnvelope);
    brxm_envelope_Destroy(ins->PanningEnvelope);
    free(ins);
  }
}


void brxm_instrument_Clear (brxmInstrument ins) {
  for (int f = 0; f <= brxmLastSample; ++f) {
    if (ins->Sample[f] != NULL) brxm_sample_Destroy(ins->Sample[f]);
    ins->Sample[f] = NULL;
  }
  for (int f = 1; f <= 96; ++f) ins->SampleMap[f] = 0;
  brxm_envelope_Clear(ins->VolumeEnvelope);
  brxm_envelope_Clear(ins->PanningEnvelope);
  ins->FadeOut = 0;
  ins->VibratoType = 0;
  ins->VibratoSweep = 0;
  ins->VibratoDepth = 0;
  ins->VibratoRate = 0;
}


////////////////////////////////////////////////////////////////////////////////
brxmPattern brxm_pattern_Create (void) {
  brxmPattern pat;
  //
  pat = calloc(1, sizeof(*pat));
  pat->patcount = 0;
  pat->Pattern = NULL;
  pat->Channels = 0;
  pat->Rows = 0;
  brxm_pattern_Clear(pat);
  //
  return pat;
}


void brxm_pattern_Destroy (brxmPattern pat) {
  if (pat != NULL) {
    brxm_pattern_Clear(pat);
    free(pat);
  }
}


void brxm_pattern_Clear (brxmPattern pat) {
  if (pat != NULL && pat->Pattern != NULL) {
    free(pat->Pattern);
    pat->patcount = 0;
    pat->Pattern = NULL;
  }
}


extern void brxm_pattern_ClearPattern (brxmPattern pat) {
  static const brxmPatternNote EmptyNote = {0};
  //
  for (int row = 0; row < pat->Rows; ++row) {
    for (int chan = 0; chan < pat->Channels; ++chan) {
      pat->Pattern[(row*pat->Channels)+chan] = EmptyNote;
    }
  }
}


void brxm_pattern_GetNote (brxmPattern pat, brxmPatternNote *res, int32_t Row, int32_t Channel) {
  int32_t Cell;
  //
  memset(res, 0, sizeof(*res));
  Cell = (Row*pat->Channels)+Channel;
  if (Cell < pat->patcount) *res = pat->Pattern[Cell];
}


void brxm_pattern_SetNote (brxmPattern pat, int32_t Row, int32_t Channel, const brxmPatternNote *Note) {
  int32_t Cell;
  //
  Cell = (Row*pat->Channels)+Channel;
  if (Cell < pat->patcount) pat->Pattern[Cell] = *Note;
}


void brxm_pattern_Resize (brxmPattern pat, int32_t NewRows, uint8_t NewChannels) {
  static const brxmPatternNote EmptyNote = {0};
  int32_t OldRows, OldChannels, Row, Channel;
  brxmPatternNote *OldPattern;
  //
  OldRows = pat->Rows;
  OldChannels = pat->Channels;
  //
  pat->Rows = NewRows;
  pat->Channels = NewChannels;
  //
  if (pat->Rows > 0 && pat->Channels > 0) {
    OldPattern = pat->Pattern;
    pat->patcount = (pat->Rows+1)*pat->Channels;
    pat->Pattern = calloc(pat->patcount, sizeof(pat->Pattern[0]));
    //
    for (Row = 0; Row < pat->Rows; ++Row) {
      for (Channel = 0; Channel < pat->Channels; ++Channel) {
        if (Channel < OldChannels) {
          if (Row < OldRows) {
            pat->Pattern[(Row*pat->Channels)+Channel] = OldPattern[(Row*OldChannels)+Channel];
          } else {
            pat->Pattern[(Row*pat->Channels)+Channel] = EmptyNote;
          }
        } else {
          pat->Pattern[(Row*pat->Channels)+Channel] = EmptyNote;
        }
      }
    }
    if (OldPattern != NULL) free(OldPattern);
  } else if (pat->Pattern != NULL) {
    free(pat->Pattern);
    pat->patcount = 0;
    pat->Pattern = NULL;
  }
}


bool_t brxm_pattern_IsEmpty (brxmPattern pat) {
  int32_t Row,Channel;
  brxmPatternNote Note;
  //
  for (Row = 0; Row < pat->Rows; ++Row) {
    for (Channel = 0; Channel < pat->Channels; ++Channel) {
      Note = pat->Pattern[(Row*pat->Channels)+Channel];
      if (Note.Note != 0 || Note.Instrument != 0 || Note.Volume != 0 || Note.Effect != 0 || Note.EffectParameter != 0) {
        return BXM_FALSE;
      }
    }
  }
  //
  return BXM_TRUE;
}


////////////////////////////////////////////////////////////////////////////////
brxmModule brxm_module_Create (void) {
  brxmModule mod;
  //
  mod = calloc(1, sizeof(*mod));
  mod->MasterVolume = 128;
  for (int f = 1; f <= brxmLastInstrument; ++f) mod->Instrument[f] = NULL;
  for (int f = 0; f <= brxmLastPattern; ++f) mod->Pattern[f] = brxm_pattern_Create();
  //
  return mod;
}


void brxm_module_Destroy (brxmModule mod) {
  if (mod != NULL) {
    brxm_module_Clear(mod);
    for (int f = 0; f <= brxmLastPattern; ++f) brxm_pattern_Destroy(mod->Pattern[f]);
    free(mod);
  }
}


void brxm_module_Clear (brxmModule mod) {
  if (mod != NULL) {
    mod->GlobalVolume = 64;
    if (mod->Name != NULL) free(mod->Name);
    mod->Name = NULL;
    if (mod->Comment != NULL) free(mod->Comment);
    mod->Comment = NULL;
    for (int f = 1; f <= brxmLastInstrument; ++f) {
      if (mod->Instrument[f] != NULL) {
        brxm_instrument_Destroy(mod->Instrument[f]);
        mod->Instrument[f] = NULL;
      }
    }
    for (int f = 0; f <= brxmLastPattern; ++f) brxm_pattern_Clear(mod->Pattern[f]);
    memset(mod->PatternOrder, 0, sizeof(mod->PatternOrder));
    mod->StartSpeed = 6;
    mod->StartTempo = 125;
    mod->TrackLength = 0;
    mod->RestartPosition = 0;
    mod->LinearSlides = BXM_TRUE;
    mod->ExtendedFilterRange = BXM_FALSE;
  }
}


int32_t brxm_module_NumberOfPatterns (brxmModule mod) {
  int32_t res = 0;
  //
  for (int f = 0; f <= brxmLastPattern; ++f) {
    if (mod->Pattern[f]->Rows > 0) res = f+1;
  }
  //
  return res;
}


int32_t brxm_module_NumberOfInstruments (brxmModule mod) {
  int32_t res = 0;
  //
  for (int f = 1; f <= brxmLastInstrument; ++f) {
    if (mod->Instrument[f] != NULL) res = f;
  }
  //
  return res;
}


////////////////////////////////////////////////////////////////////////////////
typedef struct {
  uint8_t *data;
  int used;
  int size;
} Buffer;


//FIXME: check for 'out of memory'
static void buffer_init (Buffer *buf) {
  memset(buf, 0, sizeof(*buf));
}


static void buffer_free (Buffer *buf) {
  if (buf->data != NULL) free(buf->data);
  memset(buf, 0, sizeof(*buf));
}


static void buffer_add (Buffer *buf, const void *data, int datalen) {
  if (datalen > 0) {
    if (buf->used+datalen > buf->size) {
      int newsz = buf->size+((datalen+0x7fff)&~0x7fff)+0x8000;
      //
      buf->data = realloc(buf->data, newsz);
      buf->size = newsz;
    }
    memmove(buf->data+buf->used, data, datalen);
    buf->used += datalen;
  }
}


////////////////////////////////////////////////////////////////////////////////
#define WRITE_BYTE(_b)  buffer_add(&buf, &(_b), 1)

static void xmwrt_write_pattern (Buffer *destbuf, brxmModule mod, int PatternNummer) {
  int Row, Channel;
  brxmXMPatternHeader PatternHeader;
  brxmPatternNote PatternNote;
  uint8_t Flags;
  Buffer buf;
  //
  buffer_init(&buf);
  memset(&PatternHeader, 0, sizeof(brxmXMPatternHeader));
  PatternHeader.Size = 9;
  PatternHeader.PackingType = 0;
  PatternHeader.Rows = mod->Pattern[PatternNummer]->Rows;
  //
  if (!brxm_pattern_IsEmpty(mod->Pattern[PatternNummer])) {
    for (Row = 0; Row < mod->Pattern[PatternNummer]->Rows; ++Row) {
      for (Channel = 0; Channel < mod->Pattern[PatternNummer]->Channels; ++Channel) {
        brxm_pattern_GetNote(mod->Pattern[PatternNummer], &PatternNote, Row, Channel);
        if (PatternNote.Note != 0 && PatternNote.Instrument != 0 && PatternNote.Volume > 0xF &&
            (PatternNote.Effect != brxmEffectArpeggio || PatternNote.EffectParameter != 0)) {
          WRITE_BYTE(PatternNote.Note);
          WRITE_BYTE(PatternNote.Instrument);
          WRITE_BYTE(PatternNote.Volume);
          WRITE_BYTE(PatternNote.Effect);
          WRITE_BYTE(PatternNote.EffectParameter);
        } else {
          Flags = 0x80;
          if (PatternNote.Note != 0) Flags |= 1;
          if (PatternNote.Instrument != 0) Flags |= 2;
          if (PatternNote.Volume > 0xF) Flags |= 4;
          if (PatternNote.Effect != brxmEffectArpeggio) Flags |= 8;
          if (PatternNote.EffectParameter != 0) Flags |= 16;
          WRITE_BYTE(Flags);
          if (Flags&1) WRITE_BYTE(PatternNote.Note);
          if (Flags&2) WRITE_BYTE(PatternNote.Instrument);
          if (Flags&4) WRITE_BYTE(PatternNote.Volume);
          if (Flags&8) WRITE_BYTE(PatternNote.Effect);
          if (Flags&16) WRITE_BYTE(PatternNote.EffectParameter);
        }
      }
    }
  }
  //
  PatternHeader.PackedSize = buf.used;
  //
  buffer_add(destbuf, &PatternHeader, sizeof(brxmXMPatternHeader));
  buffer_add(destbuf, buf.data, buf.used);
  //
  buffer_free(&buf);
}

#undef WRITE_BYTE


void *brxm_module_Save (brxmModule mod, int *outdatalen) {
  brxmXMHeader Header;
  brxmXMInstrumentHeader InstrumentHeader;
  brxmXMInstrumentExtraHeader InstrumentExtraHeader;
  brxmXMSampleHeader SampleHeader;
  int SubCounter, SubSubCounter, SampleCounter, SourceValue;
  brxmSample Sample;
  char *SampleBuffer = NULL;
  int8_t LastByte, DeltaByte;
  int16_t LastWord, DeltaWord;
  int8_t *DestByte;
  int16_t *DestWord;
  int NumberOfSamples;
  Buffer destbuf;
  //
  if (outdatalen != NULL) *outdatalen = 0;
  buffer_init(&destbuf);
  //
  memset(&Header, 0, sizeof(brxmXMHeader));
  memcpy(Header.Signature, "Extended Module: ", 17);
  strncpy(Header.Name, (mod->Name != NULL ? mod->Name : ""), 19);
  Header.End1AValue = 0x1A;
  strncpy(Header.Tracker, "FastTracker v2.00   ", 20);
  Header.Version = 0x0104;
  Header.Size = sizeof(brxmXMHeader)-60;
  Header.TrackLength = mod->TrackLength;
  Header.RestartPosition = mod->RestartPosition;
  Header.NumberOfChannels = mod->NumberOfChannels;
  Header.NumberOfInstruments = brxm_module_NumberOfInstruments(mod);
  Header.NumberOfPatterns = brxm_module_NumberOfPatterns(mod);
  memcpy(Header.PatternOrder, mod->PatternOrder, sizeof(Header.PatternOrder));

  Header.Flags = 0;
  if (mod->LinearSlides) Header.Flags |= 0x1;
  if (mod->ExtendedFilterRange) Header.Flags |= 0x1000;

  Header.Speed = mod->StartSpeed;
  Header.Tempo = mod->StartTempo;

  buffer_add(&destbuf, &Header, sizeof(brxmXMHeader));

  for (int f = 0; f < Header.NumberOfPatterns; ++f) xmwrt_write_pattern(&destbuf, mod, f);

  for (int f = 1; f <= Header.NumberOfInstruments; ++f) {
    memset(&InstrumentHeader, 0, sizeof(brxmXMInstrumentHeader));
    memset(&InstrumentExtraHeader, 0, sizeof(brxmXMInstrumentExtraHeader));
    NumberOfSamples = 0;
    if (mod->Instrument[f] != NULL) {
      for (SubCounter = 0; SubCounter <= brxmLastSample; ++SubCounter) {
        if (mod->Instrument[f]->Sample[SubCounter] != NULL) NumberOfSamples = SubCounter+1;
      }
    }
    if (NumberOfSamples > 0) {
      InstrumentHeader.Size = sizeof(brxmXMInstrumentHeader)+sizeof(brxmXMInstrumentExtraHeader);
    } else {
      InstrumentHeader.Size = sizeof(brxmXMInstrumentHeader);
    }
    for (SubCounter = 1; SubCounter <= 21; ++SubCounter) InstrumentHeader.Name[SubCounter] = 0;
    if (mod->Instrument[f] != NULL && mod->Instrument[f]->Name != NULL) {
      strncpy(InstrumentHeader.Name, mod->Instrument[f]->Name, 20);
    }
    InstrumentHeader.InstrumentType = 0;
    InstrumentHeader.CountOfSamples = NumberOfSamples;
    if (NumberOfSamples > 0) {
      InstrumentExtraHeader.Size = sizeof(brxmXMSampleHeader);
      InstrumentExtraHeader.FadeOut = mod->Instrument[f]->FadeOut;
      for (SubCounter = 1; SubCounter <= 96; ++SubCounter) {
        InstrumentExtraHeader.SampleMap[SubCounter-1] = mod->Instrument[f]->SampleMap[SubCounter];
      }
      InstrumentExtraHeader.VolumeEnvelopeNumPoints = mod->Instrument[f]->VolumeEnvelope->NumberOfPoints;
      InstrumentExtraHeader.PanningEnvelopeNumPoints = mod->Instrument[f]->PanningEnvelope->NumberOfPoints;
      if (InstrumentExtraHeader.VolumeEnvelopeNumPoints > 12) InstrumentExtraHeader.VolumeEnvelopeNumPoints = 12;
      if (InstrumentExtraHeader.PanningEnvelopeNumPoints > 12) InstrumentExtraHeader.PanningEnvelopeNumPoints = 12;
      for (SubCounter = 0; SubCounter < 12; ++SubCounter) {
        InstrumentExtraHeader.VolumeEnvelope[SubCounter*2] = mod->Instrument[f]->VolumeEnvelope->Points[SubCounter].Tick;
        InstrumentExtraHeader.VolumeEnvelope[(SubCounter*2)+1] = mod->Instrument[f]->VolumeEnvelope->Points[SubCounter].Value;
        InstrumentExtraHeader.PanningEnvelope[SubCounter*2] = mod->Instrument[f]->PanningEnvelope->Points[SubCounter].Tick;
        InstrumentExtraHeader.PanningEnvelope[(SubCounter*2)+1] = mod->Instrument[f]->PanningEnvelope->Points[SubCounter].Value;
      }
      InstrumentExtraHeader.VolumeEnvelopeSustain = mod->Instrument[f]->VolumeEnvelope->SustainPoint;
      InstrumentExtraHeader.VolumeEnvelopeLoopStart = mod->Instrument[f]->VolumeEnvelope->LoopStartPoint;
      InstrumentExtraHeader.VolumeEnvelopeLoopEnd = mod->Instrument[f]->VolumeEnvelope->LoopEndPoint;
      if (mod->Instrument[f]->VolumeEnvelope->Active) InstrumentExtraHeader.VolFlags |= 1;
      if (mod->Instrument[f]->VolumeEnvelope->Sustain) InstrumentExtraHeader.VolFlags |= 2;
      if (mod->Instrument[f]->VolumeEnvelope->Loop) InstrumentExtraHeader.VolFlags |= 4;

      InstrumentExtraHeader.PanningEnvelopeSustain = mod->Instrument[f]->PanningEnvelope->SustainPoint;
      InstrumentExtraHeader.PanningEnvelopeLoopStart = mod->Instrument[f]->PanningEnvelope->LoopStartPoint;
      InstrumentExtraHeader.PanningEnvelopeLoopEnd = mod->Instrument[f]->PanningEnvelope->LoopEndPoint;
      if (mod->Instrument[f]->PanningEnvelope->Active) InstrumentExtraHeader.PanFlags |= 1;
      if (mod->Instrument[f]->PanningEnvelope->Sustain) InstrumentExtraHeader.PanFlags |= 2;
      if (mod->Instrument[f]->PanningEnvelope->Loop) InstrumentExtraHeader.PanFlags |= 4;

      InstrumentExtraHeader.VibratoType = mod->Instrument[f]->VibratoType;
      InstrumentExtraHeader.VibratoSweep = mod->Instrument[f]->VibratoSweep;
      InstrumentExtraHeader.VibratoDepth = mod->Instrument[f]->VibratoDepth;
      InstrumentExtraHeader.VibratoRate = mod->Instrument[f]->VibratoRate;
    }
    //
    buffer_add(&destbuf, &InstrumentHeader, sizeof(brxmXMInstrumentHeader));
    //
    if (NumberOfSamples > 0) {
      buffer_add(&destbuf, &InstrumentExtraHeader, sizeof(brxmXMInstrumentExtraHeader));
      //
      for (SubCounter = 0; SubCounter < NumberOfSamples; ++SubCounter) {
        memset(&SampleHeader, 0, sizeof(brxmXMSampleHeader));
        if (mod->Instrument[f]->Sample[SubCounter] != NULL) {
          Sample = mod->Instrument[f]->Sample[SubCounter];

          for (SubSubCounter = 1; SubSubCounter <= 22; ++SubSubCounter) SampleHeader.Name[SubSubCounter] = 0;
          strncpy(SampleHeader.Name, Sample->Name, 21);

          SampleHeader.SampleLength = Sample->SampleLength;
          SampleHeader.LoopStart = Sample->LoopStart;
          SampleHeader.LoopLength = Sample->LoopLength;
          SampleHeader.Volume = Sample->Volume;
          SampleHeader.FineTune = Sample->FineTune;
          SampleHeader.RelativeNote = Sample->RelativeNote;
          SampleHeader.Flags = 0;
          if (Sample->PingPongLoop) {
            SampleHeader.Flags |= 2;
          } else if (Sample->Loop) {
            SampleHeader.Flags |= 1;
          }
          //
          if (Sample->Bits == 16) {
            SampleHeader.Flags |= 16;
            SampleHeader.SampleLength = SampleHeader.SampleLength*2;
            SampleHeader.LoopStart = SampleHeader.LoopStart*2;
            SampleHeader.LoopLength = SampleHeader.LoopLength*2;
          }
          if (Sample->Channels == 2) {
            SampleHeader.Flags |= 32;
            SampleHeader.SampleLength = SampleHeader.SampleLength*2;
            SampleHeader.LoopStart = SampleHeader.LoopStart*2;
            SampleHeader.LoopLength = SampleHeader.LoopLength*2;
          }
          SampleHeader.Panning = Sample->Panning;
        }
        buffer_add(&destbuf, &SampleHeader, sizeof(brxmXMSampleHeader));
      }
      //
      for (SubCounter = 0;  SubCounter < NumberOfSamples; ++SubCounter) {
        if (mod->Instrument[f]->Sample[SubCounter] != NULL) {
          Sample = mod->Instrument[f]->Sample[SubCounter];
          if (Sample->Channels == 1) {
            if (Sample->Bits == 8) {
              SampleBuffer = malloc(Sample->SampleLength+1);
              DestByte = (int8_t *)SampleBuffer;
              LastByte = 0;
              for (SampleCounter = 0; SampleCounter < Sample->SampleLength; ++SampleCounter) {
                SourceValue = Sample->Data[SampleCounter]/256;
                if (SourceValue < -128) SourceValue = -128;
                if (SourceValue > 127) SourceValue = 127;
                DeltaByte = SourceValue-LastByte;
                *DestByte = DeltaByte;
                LastByte = SourceValue;
                ++DestByte;
              }
              buffer_add(&destbuf, SampleBuffer, Sample->SampleLength);
              free(SampleBuffer);
              SampleBuffer = NULL;
            } else if (Sample->Bits == 16) {
              SampleBuffer = malloc(Sample->SampleLength*2+1);
              DestWord = (int16_t *)SampleBuffer;
              LastWord = 0;
              for (SampleCounter = 0; SampleCounter < Sample->SampleLength; ++SampleCounter) {
                SourceValue = Sample->Data[SampleCounter];
                if (SourceValue < -32768) SourceValue = -32768;
                if (SourceValue > 32767) SourceValue = 32767;
                DeltaWord = SourceValue-LastWord;
                *DestWord = DeltaWord;
                LastWord = SourceValue;
                ++DestWord;
              }
              buffer_add(&destbuf, SampleBuffer, Sample->SampleLength*2);
              free(SampleBuffer);
              SampleBuffer = NULL;
            }
          } else if (Sample->Channels == 2) {
            if (Sample->Bits == 8) {
              SampleBuffer = malloc(Sample->SampleLength+1);
              DestByte = (int8_t *)SampleBuffer;
              LastByte = 0;
              for (SampleCounter = 0; SampleCounter < Sample->SampleLength; ++SampleCounter) {
                SourceValue = Sample->Data[SampleCounter*2]/256;
                if (SourceValue < -128) SourceValue = -128;
                if (SourceValue > 127) SourceValue = 127;
                DeltaByte = SourceValue-LastByte;
                *DestByte = DeltaByte;
                LastByte = SourceValue;
                ++DestByte;
              }
              buffer_add(&destbuf, SampleBuffer, Sample->SampleLength);
              DestByte = (int8_t *)SampleBuffer;
              LastByte = 0;
              for (SampleCounter = 0; SampleCounter < Sample->SampleLength; ++SampleCounter) {
                SourceValue = Sample->Data[(SampleCounter*2)+1]/256;
                if (SourceValue < -128) SourceValue = -128;
                if (SourceValue > 127) SourceValue = 127;
                DeltaByte = SourceValue-LastByte;
                *DestByte = DeltaByte;
                LastByte = SourceValue;
                ++DestByte;
              }
              buffer_add(&destbuf, SampleBuffer, Sample->SampleLength);
              free(SampleBuffer);
              SampleBuffer = NULL;
            } else if (Sample->Bits == 16) {
              SampleBuffer = malloc(Sample->SampleLength*2+1);
              DestWord = (int16_t *)SampleBuffer;
              LastWord = 0;
              for (SampleCounter = 0; SampleCounter < Sample->SampleLength; ++SampleCounter) {
                SourceValue = Sample->Data[SampleCounter*2];
                if (SourceValue < -32768) SourceValue = -32768;
                if (SourceValue > 32767) SourceValue = 32767;
                DeltaWord = SourceValue-LastWord;
                *DestWord = DeltaWord;
                LastWord = SourceValue;
                ++DestWord;
              }
              buffer_add(&destbuf, SampleBuffer, Sample->SampleLength*2);
              DestWord = (int16_t *)SampleBuffer;
              LastWord = 0;
              for (SampleCounter = 0; SampleCounter < Sample->SampleLength; ++SampleCounter) {
                SourceValue = Sample->Data[(SampleCounter*2)+1];
                if (SourceValue < -32768) SourceValue = -32768;
                if (SourceValue > 32767) SourceValue = 32767;
                DeltaWord = SourceValue-LastWord;
                *DestWord = DeltaWord;
                LastWord = SourceValue;
                ++DestWord;
              }
              buffer_add(&destbuf, SampleBuffer, Sample->SampleLength*2);
              free(SampleBuffer);
              SampleBuffer = NULL;
            }
          }
        }
      }
    }
  }
/*
  if (mod->Comment != NULL && mod->Comment[0]) {
    uint32_t len = strlen(mod->Comment);
    static const char *sign = "text";
    //
    buffer_add(&destbuf, sign, 4);
    buffer_add(&destbuf, &len, 4);
    buffer_add(&destbuf, mod->Comment, len);
  }
*/
  if (outdatalen != NULL) *outdatalen = destbuf.used;
  return destbuf.data;
}


////////////////////////////////////////////////////////////////////////////////
#define dlogf(...) do { \
  if (nfo->brxmWriteLog) fprintf(stderr, __VA_ARGS__); \
} while (0)


////////////////////////////////////////////////////////////////////////////////
#define DMM_8_CHANNELS  (8)
//#define DMM2XM_VERSION_TEXT  "Converted with dmm2xm v1.3 [18 Nov 2007] by Grom PE"
#define DMM2XM_VERSION_TEXT  ""
#define MAX_SAMPLES  (11)


////////////////////////////////////////////////////////////////////////////////
static const uint8_t PAN_SETTING[MAX_SAMPLES][MAX_SAMPLES] = {
                  {128,0,0,0,0,0,0,0,0,0,0},
                {120,136,0,0,0,0,0,0,0,0,0},
              {112,128,144,0,0,0,0,0,0,0,0},
            {102,120,136,152,0,0,0,0,0,0,0},
           {96,112,128,144,160,0,0,0,0,0,0},
         {88,102,120,136,152,168,0,0,0,0,0},
        {80,96,112,128,144,160,176,0,0,0,0},
      {72,88,102,120,136,152,168,184,0,0,0},
     {64,80,96,112,128,144,160,176,192,0,0},
   {56,72,88,102,120,136,152,168,184,200,0},
  {48,64,80,96,112,128,144,160,176,192,208}};


////////////////////////////////////////////////////////////////////////////////
typedef struct __attribute__((packed)) {
  uint8_t Note,Instrument,Volume,Delay;
} TDMMNote;


typedef struct __attribute__((packed)) {
  uint8_t UnusedByte;
  char Name[15];
} TDMMInstrument;


////////////////////////////////////////////////////////////////////////////////
static inline long double Log2 (const long double x) {
  return logl(x)/logl(2);
}


static inline int RoundBy (int Value, int By) {
  return (Value+By/2)/By*By;
}


////////////////////////////////////////////////////////////////////////////////
static void SampleRateToXM (int SampleRate, int *RelativeNote, int *FineTune) {
  int ft, rt;
  //
  if (SampleRate != 0) {
    ft = (int)(roundl(Log2((long double)SampleRate/8363.0)*1536.0));
  } else {
    ft = (int)(roundl(Log2(1.0)*1536.0));
  }
  rt = ft/128;
  if (ft < 0) {
    rt = (ft-63)/128;
  } else {
    rt = (ft+64)/128;
  }
  ft -= rt*128;
  if (ft < 0) {
    ft = -((-ft)%128);
  } else {
    ft = ft%128;
  }
  if (rt < -127) rt = -127;
  if (rt > 127) rt = 127;
  //
  *RelativeNote = rt;
  *FineTune = ft;
}


static void MakeNoteEx (brxmPatternNote *res, int Note, int Instrument, int Volume, int Effect, int EffectParameter) {
  res->Note = Note;
  res->Instrument = Instrument;
  res->Volume = Volume;
  res->Effect = Effect;
  res->EffectParameter = EffectParameter;
}


static brxmPatternNote MakeNote (int Note) {
  brxmPatternNote n;
  //
  MakeNoteEx(&n, Note, 0, 0, 0, 0);
  //
  return n;
}


static brxmPatternNote MakeNote2 (int Note, int Instrument, int Volume) {
  brxmPatternNote n;
  //
  MakeNoteEx(&n, Note, Instrument, Volume, 0, 0);
  //
  return n;
}


static inline int IsFinishNote (const TDMMNote *Note) {
  return (Note->Note == 255 && Note->Instrument == 0 && Note->Volume == 128 && Note->Delay == 0);
}


////////////////////////////////////////////////////////////////////////////////
#define GET_BYTES(_buf, _len)  do { \
  if (indatapos+(_len) > indatalen) goto error; \
  memmove((_buf), ((const uint8_t *)indata)+indatapos, (_len)); \
  indatapos += (_len); \
} while (0)


static int LoadInstrumentFromDMI (DMMCvtInfo *nfo, brxmInstrument Instrument, const char *DMIInstrumentName,
  int InstrumentNumber, int InstrumentsTotal, const void *indata, int indatalen)
{
  int indatapos = 0;
  int8_t *SampleBuffer = NULL; // dynamic array
  //FoundDMIFileName:string;
  uint16_t DMILength, DMISampleRate, DMILoopStart, DMILoopLength;
  //
  if (DMIInstrumentName == NULL || !DMIInstrumentName[0]) {
    // skip empty instruments
    return BXM_TRUE;
  }
  //
  Instrument->SampleMap[1] = 0;
  Instrument->Sample[0] = brxm_sample_Create();
  Instrument->Sample[0]->Bits = 8;
  Instrument->Sample[0]->Channels = 1;
  Instrument->Sample[0]->Name = strdup(DMIInstrumentName);
  if (!nfo->NoStereo && InstrumentsTotal <= MAX_SAMPLES) Instrument->Sample[0]->Panning = PAN_SETTING[InstrumentsTotal-1][InstrumentNumber];
  //
  //FIXME: endianness
  if (indatalen < 8) goto error;
  GET_BYTES(&DMILength, 2);
  GET_BYTES(&DMISampleRate, 2);
  GET_BYTES(&DMILoopStart, 2);
  GET_BYTES(&DMILoopLength, 2);
  if (DMILength > indatalen-8) DMILength = indatalen-8;
  //
  Instrument->Sample[0]->SampleLength = DMILength;
  brxm_sample_Resize(Instrument->Sample[0], Instrument->Sample[0]->SampleLength);
  //
  SampleRateToXM(DMISampleRate, &Instrument->Sample[0]->RelativeNote, &Instrument->Sample[0]->FineTune);
  //
  if (DMILoopLength > 0) {
    Instrument->Sample[0]->Loop = BXM_TRUE;
    Instrument->Sample[0]->LoopStart = DMILoopStart;
    Instrument->Sample[0]->LoopLength = DMILoopLength;
  }
  //
  if ((SampleBuffer = malloc(DMILength+1)) == NULL) goto error;
  GET_BYTES(SampleBuffer, DMILength);
  //
  for (int f = 0; f < DMILength; ++f) Instrument->Sample[0]->Data[f] = SampleBuffer[f]*256;
  //
  free(SampleBuffer);
  return BXM_TRUE;
error:
  free(Instrument->Sample[0]->Name);
  Instrument->Sample[0]->Name = NULL;
  return BXM_FALSE;
}


static void RecordPatternFromDMMPattern (DMMCvtInfo *nfo, brxmPattern Pattern, const TDMMNote *DMMPattern, int patlength, int PatternNumber) {
  TDMMNote DMMNote;
  int Delay, Row, Channel, NotesRecorded;
  int MaxNoteRow;
  int ResultVolume;
  brxmPatternNote nn;
  //
  brxm_pattern_Resize(Pattern, 1024, DMM_8_CHANNELS);
  MaxNoteRow = 0;
  nn = MakeNote(brxmNoteStop);
  for (int f = 0; f < DMM_8_CHANNELS; ++f) brxm_pattern_SetNote(Pattern, 0, f, &nn);
  NotesRecorded = 0;
  Delay = 0;
  Row = 0;
  Channel = 0;
  //
  do {
    DMMNote = DMMPattern[NotesRecorded];
    if (Delay%nfo->Quantization != 0) {
      brxmMisalignedNote *n = realloc(nfo->MisalignedNotes, (nfo->mancount+1)*sizeof(nfo->MisalignedNotes[0]));
      //
      if (n == NULL) abort(); //FIXME
      nfo->MisalignedNotes = n;
      nfo->MisalignedNotes[nfo->mancount].Delay = Delay;
      Delay = RoundBy(Delay, nfo->Quantization);
      nfo->MisalignedNotes[nfo->mancount].NewDelay = Delay;
      nfo->MisalignedNotes[nfo->mancount].Row = Row;
      nfo->MisalignedNotes[nfo->mancount].Channel = Channel+1;
      nfo->MisalignedNotes[nfo->mancount].Pattern = PatternNumber;
      ++nfo->mancount;
    }
    if (Delay - nfo->Quantization > 0) Delay -= nfo->Quantization; else Delay = 0;
    if (Delay == 0) {
      ++NotesRecorded;
      //
      if (DMMNote.Volume == 128) {
        ++Channel;
        Row = 0;
        continue;
      }
      if (DMMNote.Note == 254) {
        Delay = (DMMNote.Instrument*256)+DMMNote.Delay+nfo->Quantization;
        continue;
      }
      //
      // just to make smaller patterns
      if (DMMNote.Volume > 126) ResultVolume = 0; else ResultVolume = DMMNote.Volume/2+16;
      //
      if (DMMNote.Volume > 0) {
        nn = MakeNote2(DMMNote.Note+25, DMMNote.Instrument, ResultVolume);
      } else {
        nn = MakeNote(brxmNoteStop);
      }
      brxm_pattern_SetNote(Pattern, Row, Channel, &nn);
      if (nfo->UsedChannels < Channel+1) nfo->UsedChannels = Channel+1;
      //
      if (DMMNote.Delay == 0) Delay = 256; else Delay = DMMNote.Delay;
    }
    ++Row;
    if (MaxNoteRow < Row) MaxNoteRow = Row;
  } while (NotesRecorded != patlength);
  //
  Row += RoundBy(Delay, nfo->Quantization)/nfo->Quantization;
  if (MaxNoteRow < Row) MaxNoteRow = Row;
  if (MaxNoteRow > 1024) {
    dlogf("DMM_WARNING: too many notes in pattern %d, data lost, increase quantization!\n", PatternNumber);
    MaxNoteRow = 1024;
  }
  brxm_pattern_Resize(Pattern, MaxNoteRow, DMM_8_CHANNELS);
}


typedef struct {
  int size;
  TDMMNote *notes;
} DMMPatDA;


static void dmmPatResize (DMMPatDA *pda, int newsz) {
  if (newsz < 0) newsz = 0;
  if (pda->size != newsz) {
    TDMMNote *n = realloc(pda->notes, newsz*sizeof(pda->notes[0]));
    //
    if (n == NULL) abort(); //FIXME
    pda->notes = n;
    if (newsz > pda->size) memset(pda->notes+pda->size, 0, (newsz-pda->size)*sizeof(pda->notes[0]));
    pda->size = newsz;
  }
}


void *dmm2xm (DMMCvtInfo *nfo, const char *name, const void *indata, int indatalen, int *outdatalen) {
  int indatapos = 0;
  void *res = NULL;
  //
  bool_t wok = BXM_TRUE;
  brxmModule XM = NULL;
  char DMMHeader[5];
  //
  uint16_t DMMNoteNumber = 0;
  TDMMNote *DMMNotes = NULL;
  //
  uint8_t DMMPatternOrderSize = 0;
  uint8_t *DMMPatternOrder = NULL;
  //
  uint8_t DMMInstrumentNumber = 0;
  TDMMInstrument *DMMInstruments = NULL;
  //
  // used for unpacking
  int NotesRead, ChannelsFinished, NewPatternLength;
  uint8_t DMMPatternNumber = 0;
  DMMPatDA *DMMPatterns = NULL;
  //
  // set default values
  nfo->UsedChannels = 2; // 2 is minimum number of channels
  //nfo->Quantization = 4; // min: 1; max: 256
  //nfo->NoStereo = 0;
  //
  GET_BYTES(DMMHeader, sizeof(DMMHeader));
  if (memcmp(DMMHeader, "DMM\0\0", 5) != 0) {
    dlogf("DMM2XM: not a DMM file!\n");
    goto error;
  }
  //
  GET_BYTES(&DMMPatternNumber, 1);
  //
  GET_BYTES(&DMMNoteNumber, 2);
  DMMNotes = calloc(DMMNoteNumber+1, sizeof(DMMNotes[0]));
  GET_BYTES(&DMMNotes[0], DMMNoteNumber*sizeof(DMMNotes[0]));
  //
  GET_BYTES(&DMMPatternOrderSize, 1);
  DMMPatternOrder = calloc(DMMPatternOrderSize+1, sizeof(DMMPatternOrder[0]));
  GET_BYTES(&DMMPatternOrder[0], DMMPatternOrderSize*sizeof(DMMPatternOrder[0]));
  //
  GET_BYTES(&DMMInstrumentNumber, 1);
  DMMInstruments = calloc(DMMInstrumentNumber+1, sizeof(DMMInstruments[0]));
  GET_BYTES(&DMMInstruments[0], DMMInstrumentNumber*sizeof(DMMInstruments[0]));
  //
  XM = brxm_module_Create();
  XM->Name = strdup(name != NULL ? name : "");
  XM->LinearSlides = BXM_TRUE;
  XM->StartTempo = 166; //125+33%
  XM->StartSpeed = nfo->Quantization;
  XM->Comment = strdup(DMM2XM_VERSION_TEXT);
  //
  dlogf("DMM2XM: reading instruments...\n");
  for (int f = 0; f < DMMInstrumentNumber; ++f) {
    XM->Instrument[f+1] = brxm_instrument_Create();
    if (DMMInstruments[f].Name[0]) {
      // open instrument file here!
      int fldmisize = 0;
      void *fldmi = nfo->openfile(DMMInstruments[f].Name, &fldmisize);
      //
      if (fldmi == NULL) {
        dlogf("DMM2XM_WARNING: can't find instrument '%s'\n", DMMInstruments[f].Name);
        goto error;
      }
      //
      if (!LoadInstrumentFromDMI(nfo, XM->Instrument[f+1], DMMInstruments[f].Name, f, DMMInstrumentNumber, fldmi, fldmisize)) {
        free(fldmi);
        dlogf("DMM2XM_WARNING: can't load instrument '%s'\n", DMMInstruments[f].Name);
        goto error;
      }
      free(fldmi);
    }
  }
  if (!nfo->NoStereo && DMMInstrumentNumber > MAX_SAMPLES) {
    dlogf("DMM2XM_WARNING: more than 11 instruments used, stereo is not available.\n");
    nfo->NoStereo = 1;
  }
  //
  dlogf("DMM2XM: recording notes; quantization=%d\n", nfo->Quantization);
  DMMPatterns = calloc(DMMPatternNumber, sizeof(DMMPatterns[0]));
  //
  NotesRead = 0;
  for (int f = 0; f < DMMPatternNumber; ++f) {
    ChannelsFinished = 0;
    do {
      if (NotesRead == DMMNoteNumber) {
        dlogf("DMM2XM_WARNING: unexpected end of notes, possibly broken data.\n");
        break;
      }
      NewPatternLength = DMMPatterns[f].size+1;
      dmmPatResize(&DMMPatterns[f], NewPatternLength);
      //
      DMMPatterns[f].notes[NewPatternLength-1] = DMMNotes[NotesRead];
      if (IsFinishNote(&DMMNotes[NotesRead])) ++ChannelsFinished;
      ++NotesRead;
    } while (ChannelsFinished != DMM_8_CHANNELS);
    if (NotesRead == DMMNoteNumber) break;
  }

  for (int f = 0; f < DMMPatternNumber; ++f) {
    RecordPatternFromDMMPattern(nfo, XM->Pattern[f], DMMPatterns[f].notes, DMMPatterns[f].size, f);
  }

  for (int f = 0; f < nfo->mancount; ++f) {
    dlogf("DMM2XM: misaligned note at pattern %d, channel %d, row %d (delay=%d -> %d)\n",
      nfo->MisalignedNotes[f].Pattern,
      nfo->MisalignedNotes[f].Channel,
      nfo->MisalignedNotes[f].Row,
      nfo->MisalignedNotes[f].Delay,
      nfo->MisalignedNotes[f].NewDelay);
  }

  if (nfo->mancount > 0) {
    dlogf("DMM2XM_WARNING: quantization may be wrong or too large, %d notes misaligned.\n", nfo->mancount);
  }

  XM->TrackLength = DMMPatternOrderSize-1;
  for (int f = 0; f < DMMPatternOrderSize; ++f) {
    if (DMMPatternOrder[f] == 255) {
      --(XM->TrackLength);
      if (f == DMMPatternOrderSize-1) {
        dlogf("DMM2XM_WARNING: unexpected end of pattern order, possibly broken data.\n");
        break;
      }
      XM->RestartPosition = DMMPatternOrder[f+1];
      break;
    } else {
      XM->PatternOrder[f] = DMMPatternOrder[f];
    }
  }

  dlogf("DMM2XM: optimizing...\n");
  if (nfo->UsedChannels < DMM_8_CHANNELS) {
    for (int f = 0; f <= brxmLastPattern; ++f) {
      if (XM->Pattern[f]->Rows > 0) {
        brxm_pattern_Resize(XM->Pattern[f], XM->Pattern[f]->Rows, nfo->UsedChannels);
      }
    }
    dlogf("DMM2XM: %d of 8 channels used\n", nfo->UsedChannels);
  }
  XM->NumberOfChannels = nfo->UsedChannels;

  dlogf("DMM2XM: saving...\n");
  if ((res = brxm_module_Save(XM, outdatalen)) == NULL) wok = BXM_FALSE;

  if (DMMNotes != NULL) free(DMMNotes);
  if (DMMPatternOrder != NULL) free(DMMPatternOrder);
  if (DMMInstruments != NULL) free(DMMInstruments);
  if (DMMPatterns != NULL) {
    for (int f = 0; f < DMMPatternNumber; ++f) if (DMMPatterns[f].notes != NULL) free(DMMPatterns[f].notes);
  }
  if (DMMPatterns != NULL) free(DMMPatterns);
  brxm_module_Destroy(XM);
  //
  if (!wok && res != NULL) {
    free(res);
    if (outdatalen != NULL) *outdatalen = 0;
    res = NULL;
  }
  //
  return res;
error:
  if (DMMNotes != NULL) free(DMMNotes);
  if (DMMPatternOrder != NULL) free(DMMPatternOrder);
  if (DMMInstruments != NULL) free(DMMInstruments);
  if (DMMPatterns != NULL) {
    for (int f = 0; f < DMMPatternNumber; ++f) if (DMMPatterns[f].notes != NULL) free(DMMPatterns[f].notes);
  }
  if (DMMPatterns != NULL) free(DMMPatterns);
  if (XM != NULL) brxm_module_Destroy(XM);
  //
  if (res != NULL) free(res);
  if (outdatalen != NULL) *outdatalen = 0;
  //
  return NULL;
}
#undef GET_BYTES


////////////////////////////////////////////////////////////////////////////////
void dmmCvtInfoInit (DMMCvtInfo *nfo, brxm_OpenFileROFn openfile) {
  if (nfo != NULL) {
    memset(nfo, 0, sizeof(*nfo));
    nfo->openfile = openfile;
    nfo->Quantization = 4;
    nfo->UsedChannels = 0;
    nfo->NoStereo = 0;
    nfo->mancount = 0;
    nfo->MisalignedNotes = NULL;
  }
}


void dmmCvtInfoDeinit (DMMCvtInfo *nfo) {
  if (nfo != NULL) {
    if (nfo->MisalignedNotes != NULL) free(nfo->MisalignedNotes);
    nfo->mancount = 0;
    nfo->MisalignedNotes = NULL;
  }
}
