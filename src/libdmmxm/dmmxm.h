/*
 * based on Grom PE dmm2xm converter
 * used some code from BeRoXM, Copyright (C) 2005-2007, Benjamin 'BeRo' Rosseaux
 *
 * Copyright (C) Grom PE
 * Copyright (C) Benjamin 'BeRo' Rosseaux 2005-2007
 * Copyright (C) Ketmar Dark 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LIBDMMXM_H
#define LIBDMMXM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


typedef int bool_t;

#define BXM_FALSE  (0)
#define BXM_TRUE   (1)


enum {
  brxmLastSample = 255,
  brxmLastInstrument = 255,
  brxmLastPattern = 255,
  brxmLastPatternOrder = 255,
  //
  brxmNoteStop = 97,
  //
  brxmEffectArpeggio = 0x00
};


typedef int32_t brxmSampleValue;
typedef brxmSampleValue brxmSampleData;


typedef struct {
  int32_t Note;
  int32_t Instrument;
  int32_t Volume;
  int32_t Effect;
  int32_t EffectParameter;
} brxmPatternNote;


typedef struct {
  brxmSampleData *Data;
  int32_t SampleLength; // in bytes
  int32_t Bits;
  int32_t Channels;
  bool_t ADPCM4;
  char *Name;
  bool_t Loop;
  bool_t PingPongLoop;
  int32_t LoopStart;
  int32_t LoopLength;
  int32_t Volume;
  int32_t Panning;
  int32_t RelativeNote;
  int32_t FineTune;
  int32_t Samples;
} *brxmSample;

extern brxmSample brxm_sample_Create (void);
extern void brxm_sample_Destroy (brxmSample smp);
extern void brxm_sample_Clear (brxmSample smp);
extern void brxm_sample_Resize (brxmSample smp, int32_t Samples);


typedef struct {
  int32_t Tick;
  int32_t Value;
} brxmEnvelopePoint;


typedef struct {
  bool_t Active;
  bool_t Loop;
  bool_t Sustain;
  brxmEnvelopePoint Points[13];
  int32_t NumberOfPoints;
  int32_t LoopStartPoint;
  int32_t LoopEndPoint;
  int32_t SustainPoint;
} *brxmEnvelope;

extern brxmEnvelope brxm_envelope_Create (void);
extern void brxm_envelope_Destroy (brxmEnvelope env);
extern void brxm_envelope_Clear (brxmEnvelope env);


typedef struct {
  char *Name;
  int32_t InstrumentType;
  brxmSample Sample[brxmLastSample+1];
  uint8_t SampleMap[97]; // actually, [1..96]
  brxmEnvelope VolumeEnvelope;
  brxmEnvelope PanningEnvelope;
  int32_t FadeOut;
  int32_t VibratoType;
  int32_t VibratoSweep;
  int32_t VibratoDepth;
  int32_t VibratoRate;
} *brxmInstrument;

extern brxmInstrument brxm_instrument_Create (void);
extern void brxm_instrument_Destroy (brxmInstrument ins);
extern void brxm_instrument_Clear (brxmInstrument ins);


typedef struct {
  int patcount; // for Pattern
  brxmPatternNote *Pattern; // dynamic array
  int32_t Rows;
  uint8_t Channels;
} *brxmPattern;

extern brxmPattern brxm_pattern_Create (void);
extern void brxm_pattern_Destroy (brxmPattern pat);
extern void brxm_pattern_Clear (brxmPattern pat);
extern void brxm_pattern_ClearPattern (brxmPattern pat);

extern void brxm_pattern_GetNote (brxmPattern pat, brxmPatternNote *res, int32_t Row, int32_t Channel);
extern void brxm_pattern_SetNote (brxmPattern pat, int32_t Row, int32_t Channel, const brxmPatternNote *Note);
extern void brxm_pattern_Resize (brxmPattern pat, int32_t NewRows, uint8_t NewChannels);
extern bool_t brxm_pattern_IsEmpty (brxmPattern pat);


//brxmPatterns=array[0..brxmLastPattern] of brxmPattern;
//brxmPatternOrder=array[0..brxmLastPatternOrder] of uint8_t;

typedef struct {
  char *Name;
  char *Comment;
  brxmInstrument Instrument[brxmLastInstrument+1]; // actually, [1..brxmLastInstrument]
  brxmPattern Pattern[brxmLastPattern+1]; //brxmPatterns
  uint8_t PatternOrder[brxmLastPatternOrder+1]; //brxmPatternOrder
  int32_t GlobalVolume;
  int32_t MasterVolume;
  uint8_t StartSpeed; // in frames/row
  uint8_t StartTempo; // in beats per minute
  int32_t TrackLength;
  int32_t RestartPosition;
  bool_t LinearSlides;
  bool_t ExtendedFilterRange;
  int32_t NumberOfChannels;
} *brxmModule;

extern brxmModule brxm_module_Create (void);
extern void brxm_module_Destroy (brxmModule mod);
extern void brxm_module_Clear (brxmModule mod);
extern int32_t brxm_module_NumberOfPatterns (brxmModule mod);
extern int32_t brxm_module_NumberOfInstruments (brxmModule mod);
extern void *brxm_module_Save (brxmModule mod, int *outdatalen);


extern int brxmWriteLog;

// return malloc()'ed buffer
typedef void *(*brxm_OpenFileROFn) (const char *fname, int *datasize);


typedef struct __attribute__((packed)) {
  int32_t Delay,NewDelay,Pattern,Row,Channel;
} brxmMisalignedNote;


typedef struct {
  int brxmWriteLog;
  brxm_OpenFileROFn openfile;
  //
  int Quantization;
  int UsedChannels;
  int NoStereo;
  //
  int mancount;
  brxmMisalignedNote *MisalignedNotes;
} DMMCvtInfo;


extern void dmmCvtInfoInit (DMMCvtInfo *nfo, brxm_OpenFileROFn openfile);
extern void dmmCvtInfoDeinit (DMMCvtInfo *nfo);

extern void *dmm2xm (DMMCvtInfo *nfo, const char *name, const void *indata, int indatalen, int *outdatalen);


#ifdef __cplusplus
}
#endif
#endif
