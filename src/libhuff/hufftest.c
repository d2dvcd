/*
 *  huffcode - Encode/Decode files using Huffman encoding.
 *  http://huffman.sourceforge.net
 *  Copyright (C) 2003  Douglas Ryan Richardson
 */
//#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libhuff.h"


typedef struct {
  FILE *in;
  FILE *out;
} IOData;


static int hread (void *buf, int len, void *udata) {
  size_t rd = fread(buf, 1, len, ((IOData *)udata)->in);
  //
  if (rd == (size_t)-1) return -1;
  return rd;
}


static int hwrite (const void *buf, int len, void *udata) {
  size_t wr = fwrite(buf, 1, len, ((IOData *)udata)->out);
  //
  if (wr != (size_t)len) return -1;
  return wr;
}


static int hrewind (void *udata) {
  rewind(((IOData *)udata)->in);
  return 0;
}


static const HuffmanIO hio = {
  .read = hread,
  .write = hwrite,
  .rewind = hrewind,
};


int main (int argc, char *argv[]) {
  IOData iod;
  int res = 0;
  //
  if (argc != 4 || (argv[1][0] != 'p' && argv[1][0] != 'u')) {
    printf("usage: hufftest <p|u> infile outfile\n");
    return 1;
  }
  //
  if ((iod.in = fopen(argv[2], "rb")) == NULL) {
    fprintf(stderr, "FATAL: can't open input file!\n");
    return 1;
  }
  //
  if ((iod.out = fopen(argv[3], "wb")) == NULL) {
    fprintf(stderr, "FATAL: can't create output file!\n");
    return 1;
  }
  //
  if (argv[1][0] == 'p') {
    static const char sign[4] = "HUF\0";
    //
    if (fwrite(sign, 4, 1, iod.out) != 1) {
      fprintf(stderr, "FATAL: writing error!\n");
      res = -1;
    } else {
      res = huffman_encode(&hio, &iod);
    }
  } else {
    //res = huffman_decode(&hio, &iod);
    static char sign[4];
    HuffmanDState *ds;
    char buf[1024];
    int rd;
    //
    if (fread(sign, 4, 1, iod.in) != 1 || memcmp(sign, "HUF\0", 4) != 0) {
      fprintf(stderr, "FATAL: invalid huffman archive!\n");
      res = -1;
      goto quit;
    }
    //
    res = 0;
    if ((ds = huffman_decode_start(&hio, &iod)) == NULL) {
      fprintf(stderr, "FATAL: invalid huffman data (0)!\n");
      res = -1;
      goto quit;
    }
    //
    while ((rd = huffman_ds_get_bytes(ds, buf, sizeof(buf))) > 0) {
      if (fwrite(buf, rd, 1, iod.out) != 1) {
        fprintf(stderr, "FATAL: writing error!\n");
        huffman_decode_done(ds);
        res = -1;
        goto quit;
      }
    }
    //
    huffman_decode_done(ds);
    if (rd < 0) {
      fprintf(stderr, "FATAL: invalid huffman data (1)!\n");
      res = -1;
      goto quit;
    }
  }
  //
quit:
  fclose(iod.in);
  fclose(iod.out);
  //
  if (res != 0) {
    fprintf(stderr, "FATAL: codec error!\n");
    unlink(argv[3]);
  }
  //
  return 0;
}
