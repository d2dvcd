#!/bin/bash


if [ "z$1" = "z" ]; then
  mkdir _huf 2>/dev/null
  for fn in *; do
    if [ "$fn" = "_huf" ]; then
      continue
    fi
    if [ "$fn" = "_wdx" ]; then
      continue
    fi
    sh "$0" $fn
  done
else
  echo "$1"
  /home/ketmar/k8prj/doom2d-vcd/hufftest p "$1" "_huf/$1.huf"
fi
