/*
 * huffman_coder - Encode/Decode files using Huffman encoding.
 * http://huffman.sourceforge.net
 * Copyright (C) 2003  Douglas Ryan Richardson
 *
 * Copyright (c) 2007, Douglas Ryan Richardson, Gauss Interprise, Inc
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Gauss Interprise, Inc nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "libhuff.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN32
# include <malloc.h>
#endif


typedef uint32_t uintlong_t;
typedef int32_t intlong_t;


typedef struct huffman_node_tag {
  uint8_t isLeaf;
  uintlong_t count;
  struct huffman_node_tag *parent;
  union {
    struct {
      struct huffman_node_tag *zero, *one;
    };
    uint8_t symbol;
  };
} huffman_node;


typedef struct huffman_code_tag {
  /* The length of this code in bits. */
  uintlong_t numbits;
  /* The bits that make up this code. The first
     bit is at position 0 in bits[0]. The second
     bit is at position 1 in bits[0]. The eighth
     bit is at position 7 in bits[0]. The ninth
     bit is at position 0 in bits[1]. */
  uint8_t *bits;
} huffman_code;


static inline uintlong_t numbytes_from_numbits (uintlong_t numbits) {
  return (numbits/8+(numbits%8 ? 1 : 0));
}


/*
 * get_bit returns the ith bit in the bits array
 * in the 0th position of the return value.
 */
static inline uint8_t get_bit (const uint8_t *bits, uintlong_t i) {
  return ((bits[i/8]>>i%8)&1);
}


static void reverse_bits (uint8_t *bits, uintlong_t numbits) {
  uintlong_t numbytes = numbytes_from_numbits(numbits);
  uint8_t *tmp = (uint8_t *)alloca(numbytes);
  intlong_t curbyte = 0;
  //
  memset(tmp, 0, numbytes);

  for (uintlong_t curbit = 0; curbit < numbits; ++curbit) {
    uint32_t bitpos = curbit%8;
    //
    if (curbit > 0 && curbit%8 == 0) ++curbyte;
    tmp[curbyte] |= (get_bit(bits, numbits-curbit-1)<<bitpos);
  }
  memcpy(bits, tmp, numbytes);
}


/*
 * new_code builds a huffman_code from a leaf in
 * a Huffman tree.
 */
static huffman_code *new_code (const huffman_node *leaf) {
  /* Build the huffman code by walking up to
   * the root node and then reversing the bits,
   * since the Huffman code is calculated by
   * walking down the tree. */
  uintlong_t numbits = 0;
  uint8_t *bits = NULL;
  huffman_code *p;
  //
  while (leaf && leaf->parent) {
    huffman_node *parent = leaf->parent;
    uint8_t cur_bit = (uint8_t)(numbits%8);
    uintlong_t cur_byte = numbits/8;

    /* If we need another byte to hold the code, then allocate it. */
    if (cur_bit == 0) {
      size_t newSize = cur_byte+1;
      //
      bits = (uint8_t *)realloc(bits, newSize);
      bits[newSize-1] = 0; /* Initialize the new byte. */
    }
    /* If a one must be added then or it in. If a zero
     * must be added then do nothing, since the byte
     * was initialized to zero. */
    if (leaf == parent->one) bits[cur_byte] |= (1<<cur_bit);
    ++numbits;
    leaf = parent;
  }
  //
  if (bits) reverse_bits(bits, numbits);
  //
  p = (huffman_code *)malloc(sizeof(huffman_code));
  p->numbits = numbits;
  p->bits = bits;
  //
  return p;
}


#define MAX_SYMBOLS  (256)
typedef huffman_node *SymbolFrequencies[MAX_SYMBOLS];
typedef huffman_code *SymbolEncoder[MAX_SYMBOLS];


static huffman_node *new_leaf_node (uint8_t symbol) {
  huffman_node *p = (huffman_node *)malloc(sizeof(huffman_node));
  //
  p->isLeaf = 1;
  p->symbol = symbol;
  p->count = 0;
  p->parent = 0;
  //
  return p;
}


static huffman_node *new_nonleaf_node (uintlong_t count, huffman_node *zero, huffman_node *one) {
  huffman_node *p = (huffman_node *)malloc(sizeof(huffman_node));
  //
  p->isLeaf = 0;
  p->count = count;
  p->zero = zero;
  p->one = one;
  p->parent = 0;
  //
  return p;
}


static void free_huffman_tree (huffman_node *subtree) {
  if (subtree != NULL) {
    if (!subtree->isLeaf) {
      free_huffman_tree(subtree->zero);
      free_huffman_tree(subtree->one);
    }
    free(subtree);
  }
}


static void free_code (huffman_code *p) {
  free(p->bits);
  free(p);
}


static void free_encoder (SymbolEncoder *pSE) {
  for (uintlong_t i = 0; i < MAX_SYMBOLS; ++i) {
    huffman_code *p = (*pSE)[i];
    //
    if (p) free_code(p);
  }
  free(pSE);
}


static void init_frequencies (SymbolFrequencies *pSF) {
  memset(*pSF, 0, sizeof(SymbolFrequencies));
#if 0
  for (uint32_t i = 0; i < MAX_SYMBOLS; ++i) {
    uint8_t uc = (uint8_t)i;
    //
    (*pSF)[i] = new_leaf_node(uc);
  }
#endif
}


static uint32_t get_symbol_frequencies (SymbolFrequencies *pSF, const HuffmanIO *io, void *udata) {
  uint8_t uc;
  uint32_t total_count = 0;
  //
  /* Set all frequencies to 0. */
  init_frequencies(pSF);
  /* Count the frequency of each symbol in the input file. */
  while (io->read(&uc, 1, udata) == 1) {
    if (!(*pSF)[uc]) (*pSF)[uc] = new_leaf_node(uc);
    ++(*pSF)[uc]->count;
    ++total_count;
  }
  //
  return total_count;
}


/*
 * When used by qsort, SFComp sorts the array so that
 * the symbol with the lowest frequency is first. Any
 * NULL entries will be sorted to the end of the list.
 */
static int SFComp (const void *p1, const void *p2) {
  const huffman_node *hn1 = *(const huffman_node **)p1;
  const huffman_node *hn2 = *(const huffman_node **)p2;
  //
  /* Sort all NULLs to the end. */
  if (hn1 == NULL && hn2 == NULL) return 0;
  if (hn1 == NULL) return 1;
  if (hn2 == NULL) return -1;
  if (hn1->count > hn2->count) return 1;
  if (hn1->count < hn2->count) return -1;
  return 0;
}


#if 0
static void
print_freqs(SymbolFrequencies * pSF)
{
  size_t i;
  for(i = 0; i < MAX_SYMBOLS; ++i)
  {
    if((*pSF)[i])
      printf("%d, %ld\n", (*pSF)[i]->symbol, (*pSF)[i]->count);
    else
      printf("NULL\n");
  }
}
#endif


/*
 * build_symbol_encoder builds a SymbolEncoder by walking
 * down to the leaves of the Huffman tree and then,
 * for each leaf, determines its code.
 */
static void build_symbol_encoder (huffman_node *subtree, SymbolEncoder *pSF) {
  if (subtree == NULL) return;
  if (subtree->isLeaf) {
    (*pSF)[subtree->symbol] = new_code(subtree);
  } else {
    build_symbol_encoder(subtree->zero, pSF);
    build_symbol_encoder(subtree->one, pSF);
  }
}


/*
 * calculate_huffman_codes turns pSF into an array
 * with a single entry that is the root of the
 * huffman tree. The return value is a SymbolEncoder,
 * which is an array of huffman codes index by symbol value.
 */
static SymbolEncoder *calculate_huffman_codes (SymbolFrequencies *pSF) {
  uint32_t i = 0, n = 0;
  huffman_node *m1 = NULL, *m2 = NULL;
  SymbolEncoder *pSE = NULL;
  //
#if 0
  printf("BEFORE SORT\n");
  print_freqs(pSF);
#endif
  /* Sort the symbol frequency array by ascending frequency. */
  qsort((*pSF), MAX_SYMBOLS, sizeof((*pSF)[0]), SFComp);
#if 0
  printf("AFTER SORT\n");
  print_freqs(pSF);
#endif
  /* Get the number of symbols. */
  for (n = 0; n < MAX_SYMBOLS && (*pSF)[n]; ++n) ;
  /*
   * Construct a Huffman tree. This code is based
   * on the algorithm given in Managing Gigabytes
   * by Ian Witten et al, 2nd edition, page 34.
   * Note that this implementation uses a simple
   * count instead of probability.
   */
  for (i = 0; i < n-1; ++i) {
    /* Set m1 and m2 to the two subsets of least probability. */
    m1 = (*pSF)[0];
    m2 = (*pSF)[1];
    /* Replace m1 and m2 with a set {m1, m2} whose probability
     * is the sum of that of m1 and m2. */
    (*pSF)[0] = m1->parent = m2->parent = new_nonleaf_node(m1->count+m2->count, m1, m2);
    (*pSF)[1] = NULL;
    /* Put newSet into the correct count position in pSF. */
    qsort((*pSF), n, sizeof((*pSF)[0]), SFComp);
  }
  /* Build the SymbolEncoder array from the tree. */
  pSE = (SymbolEncoder*)malloc(sizeof(SymbolEncoder));
  memset(pSE, 0, sizeof(SymbolEncoder));
  build_symbol_encoder((*pSF)[0], pSE);
  return pSE;
}


/*
 * Write the huffman code table. The format is:
 * 1 byte code count (0 is 256)
 * 4 byte number of bytes encoded
 *   (if you decode the data, you should get this number of bytes)
 * code1
 * ...
 * codeN, where N is the count read at the begginning of the file.
 * Each codeI has the following format:
 * 1 byte symbol, 1 byte code bit length, code bytes.
 * Each entry has numbytes_from_numbits code bytes.
 * The last byte of each code may have extra bits, if the number of
 * bits in the code is not a multiple of 8.
 */
static int write_code_table (SymbolEncoder *se, uint32_t symbol_count, const HuffmanIO *io, void *udata) {
  uintlong_t i, count;
  uint8_t btmp;
  //
  /* Determine the number of entries in se. */
  for (i = count = 0; i < MAX_SYMBOLS; ++i) if ((*se)[i]) ++count;
  /* Write the number of bytes that will be encoded. */
  for (i = 0; i < 4; ++i) {
    btmp = (symbol_count>>(i*8))&0xff;
    //
    if (io->write(&btmp, 1, udata) != 1) return -1;
  }
  btmp = (count&0xff);
  /* Write the number of entries. */
  if (io->write(&btmp, 1, udata) != 1) return -1;
  /* Write the entries. */
  for (i = 0; i < MAX_SYMBOLS; ++i) {
    const huffman_code *p = (*se)[i];
    //
    if (p) {
      uint32_t numbytes;
      //
      /* Write the 1 byte symbol. */
      btmp = i;
      if (io->write(&btmp, 1, udata) != 1) return -1;
      /* Write the 1 byte code bit length. */
      btmp = p->numbits;
      if (io->write(&btmp, 1, udata) != 1) return -1;
      /* Write the code bytes. */
      numbytes = numbytes_from_numbits(p->numbits);
      //FIXME: (int)
      if (io->write(p->bits, (int)numbytes, udata) != (int)numbytes) return -1;
    }
  }
  //
  return 0;
}


/*
 * read_code_table builds a Huffman tree from the code
 * in the in file. This function returns NULL on error.
 * The returned value should be freed with free_huffman_tree.
 */
static huffman_node *read_code_table (uint32_t *pDataBytes, const HuffmanIO *io, void *udata) {
  huffman_node *root = new_nonleaf_node(0, NULL, NULL);
  uint8_t *bytes = NULL;
  uint32_t count, xcount = 0;
  uint8_t btmp;
  //
  /* Read the number of data bytes this encoding represents. */
  for (int f = 0; f < 4; ++f) {
    if (io->read(&btmp, 1, udata) != 1) goto error;
    xcount |= (((uint32_t)btmp)<<(f*8));
  }
  *pDataBytes = xcount;
  /* Read the number of entries. */
  if (io->read(&btmp, 1, udata) != 1) goto error;
  count = (btmp ? btmp : 256);
  /* Read the entries. */
  while (count-- > 0) {
    huffman_node *p = root;
    uint32_t curbit;
    uint8_t symbol;
    uint8_t numbits;
    uint8_t numbytes;
    //
    if (io->read(&btmp, 1, udata) != 1) goto error;
    symbol = btmp;
    if (io->read(&btmp, 1, udata) != 1) goto error;
    numbits = btmp;
    numbytes = (uint8_t)numbytes_from_numbits(numbits);
    if ((bytes = (uint8_t *)malloc(numbytes)) == NULL) goto error;
    if (io->read(bytes, numbytes, udata) != numbytes) goto error;
    /*
     * Add the entry to the Huffman tree. The value
     * of the current bit is used switch between
     * zero and one child nodes in the tree. New nodes
     * are added as needed in the tree.
     */
    for (curbit = 0; curbit < numbits; ++curbit) {
      if (get_bit(bytes, curbit)) {
        if (p->one == NULL) {
          p->one = curbit == (uint8_t)(numbits-1) ? new_leaf_node(symbol) : new_nonleaf_node(0, NULL, NULL);
          p->one->parent = p;
        }
        p = p->one;
      } else {
        if (p->zero == NULL) {
          p->zero = curbit == (uint8_t)(numbits-1) ? new_leaf_node(symbol) : new_nonleaf_node(0, NULL, NULL);
          p->zero->parent = p;
        }
        p = p->zero;
      }
    }
    free(bytes);
    bytes = NULL;
  }
  //
  return root;
error:
  free_huffman_tree(root);
  return NULL;
}


static int do_file_encode (SymbolEncoder *se, const HuffmanIO *io, void *udata) {
  uint8_t curbyte = 0;
  uint8_t curbit = 0;
  uint8_t uc;
  int rd;
  //
  while ((rd = io->read(&uc, 1, udata)) == 1) {
    huffman_code *code = (*se)[uc];
    //
    for (uintlong_t i = 0; i < code->numbits; ++i) {
      /* Add the current bit to curbyte. */
      curbyte |= get_bit(code->bits, i)<<curbit;
      /* If this byte is filled up then write it out and reset the curbit and curbyte. */
      if (++curbit == 8) {
        if (io->write(&curbyte, 1, udata) != 1) return -1;
        curbyte = 0;
        curbit = 0;
      }
    }
  }
  /*
   * If there is data in curbyte that has not been
   * output yet, which means that the last encoded
   * character did not fall on a byte boundary,
   * then output it.
   */
  if (rd >= 0 && curbit > 0) {
    if (io->write(&curbyte, 1, udata) != 1) return -1;
  }
  //
  return (rd >= 0 ? 0 : -1);
}


/*
 * huffman_encode_file huffman encodes in to out.
 */
int huffman_encode (const HuffmanIO *io, void *udata) {
  SymbolFrequencies sf;
  SymbolEncoder *se;
  huffman_node *root = NULL;
  uint32_t symbol_count;
  int rc = -1;
  //
  if (io->rewind(udata) != 0) return -1;
  /* Get the frequency of each symbol in the input file. */
  symbol_count = get_symbol_frequencies(&sf, io, udata);
  /* Build an optimal table from the symbolCount. */
  se = calculate_huffman_codes(&sf);
  root = sf[0];
  /* Scan the file again and, using the table previously built, encode it into the output file. */
  if (io->rewind(udata) != 0) goto quit;
  rc = write_code_table(se, symbol_count, io, udata);
  if (rc == 0) rc = do_file_encode(se, io, udata);
quit:
  /* Free the Huffman tree. */
  free_huffman_tree(root);
  free_encoder(se);
  return rc;
}


int huffman_decode (const HuffmanIO *io, void *udata) {
  huffman_node *root, *p;
  uint32_t data_count;
  //
  /* Read the Huffman code table. */
  root = read_code_table(&data_count, io, udata);
  if (!root) return -1;
  /* Decode the file. */
  p = root;
  while (data_count > 0) {
    uint8_t mask = 1, byte;
    //
    if (io->read(&byte, 1, udata) != 1) {
      free_huffman_tree(root);
      return -1;
    }
    //
    while (data_count > 0 && mask) {
      p = (byte&mask ? p->one : p->zero);
      mask <<= 1;
      if (p->isLeaf) {
        if (io->write(&p->symbol, 1, udata) != 1) {
          free_huffman_tree(root);
          return -1;
        }
        p = root;
        --data_count;
      }
    }
  }
  //
  free_huffman_tree(root);
  //
  return 0;
}


struct HuffmanDState {
  const HuffmanIO *io;
  void *udata;
  //
  huffman_node *root;
  uint32_t data_count;
  uint8_t mask;
  uint8_t byte;
};


int huffman_ds_bytes_left (HuffmanDState *ds) {
  return (ds != NULL ? (ds->root != NULL ? (int)ds->data_count : -1) : 0);
}


HuffmanDState *huffman_decode_start (const HuffmanIO *io, void *udata) {
  HuffmanDState *ds;
  //
  if ((ds = malloc(sizeof(*ds))) == NULL) return NULL;
  ds->io = io;
  ds->udata = udata;
  ds->mask = ds->byte = 0;
  //
  /* Read the Huffman code table. */
  ds->root = read_code_table(&ds->data_count, io, udata);
  if (ds->root == NULL) {
    free(ds);
    return NULL;
  }
  //
  return ds;
}


void huffman_decode_done (HuffmanDState *ds) {
  if (ds != NULL) {
    if (ds->root != NULL) free_huffman_tree(ds->root);
    free(ds);
  }
}


int huffman_ds_get_bytes (HuffmanDState *ds, void *buf, int len) {
  int res = (len > 0 && buf == NULL ? -1 : 0);
  //
  if (ds != NULL && ds->data_count > 0 && len > 0) {
    huffman_node *p = ds->root;
    uint8_t *dest = (uint8_t *)buf;
    //
    if (len > (int)ds->data_count) len = (int)ds->data_count;
    res = len;
    while (len > 0) {
      if (ds->mask == 0) {
        if (ds->io->read(&ds->byte, 1, ds->udata) != 1) {
          free_huffman_tree(ds->root);
          ds->root = NULL;
          return -1;
        }
        ds->mask = 1;
      }
      //
      while (ds->mask) {
        p = (ds->byte&ds->mask ? p->one : p->zero);
        ds->mask = (ds->mask&0x7f)<<1;
        if (p->isLeaf) {
          // got another byte
          --(ds->data_count);
          *dest++ = p->symbol;
          p = ds->root;
          --len;
          break;
        }
      }
    }
  }
  //
  return res;
}
