/*
 * huffman_coder - Encode/Decode files using Huffman encoding.
 * http://huffman.sourceforge.net
 * Copyright (C) 2003  Douglas Ryan Richardson
 *
 * Copyright (c) 2007, Douglas Ryan Richardson, Gauss Interprise, Inc
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Gauss Interprise, Inc nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LIBHUFFMAN_HUFFMAN_H
#define LIBHUFFMAN_HUFFMAN_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>


typedef struct HuffmanDState HuffmanDState;


typedef struct {
  int (*read) (void *buf, int len, void *udata); // return # of bytes read, 0 on EOF, -1 on error
  int (*write) (const void *buf, int len, void *udata); // return # of bytes written, -1 on fatal error; result<len: error too!
  int (*rewind) (void *udata); // <0: error; 0: ok; >0: only idiots doing this
} HuffmanIO;


//WARNING! encoder is 2-pass, hence the need of rewind()!
extern int huffman_encode (const HuffmanIO *io, void *udata);
extern int huffman_decode (const HuffmanIO *io, void *udata);

// no need to set `write` and `rewind` in `io`
extern HuffmanDState *huffman_decode_start (const HuffmanIO *io, void *udata);
extern void huffman_decode_done (HuffmanDState *ds);

extern int huffman_ds_bytes_left (HuffmanDState *ds); // <0: error
extern int huffman_ds_get_bytes (HuffmanDState *ds, void *buf, int len); // can read less than len bytes; <0: error


#ifdef __cplusplus
}
#endif
#endif
