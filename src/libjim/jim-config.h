#ifndef _JIM_CONFIG_H
#define _JIM_CONFIG_H

#include "jimautoconf.h"

#define JIM_UTF8 1
#define JIM_REGEXP 1
#define JIM_REFERENCES 1
//#define JIM_OPTIMIZATION 1
#define JIM_MATH_FUNCTIONS 1

#endif
