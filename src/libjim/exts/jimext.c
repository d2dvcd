#include "../jim.h"

int Jim_InitStaticExtensions (Jim_Interp *interp) {
  extern int Jim_arrayInit(Jim_Interp *);
  extern int Jim_clockInit(Jim_Interp *);
  extern int Jim_execInit(Jim_Interp *);
  extern int Jim_fileInit(Jim_Interp *);
  extern int Jim_loadInit(Jim_Interp *);
  extern int Jim_namespaceInit(Jim_Interp *);
  extern int Jim_packInit(Jim_Interp *);
  extern int Jim_packageInit(Jim_Interp *);
  extern int Jim_posixInit(Jim_Interp *);
  extern int Jim_readdirInit(Jim_Interp *);
  extern int Jim_regexpInit(Jim_Interp *);
  extern int Jim_signalInit(Jim_Interp *);
  extern int Jim_tclprefixInit(Jim_Interp *);
  //
  Jim_arrayInit(interp);
  Jim_clockInit(interp);
  Jim_execInit(interp);
  Jim_fileInit(interp);
  Jim_loadInit(interp);
  Jim_namespaceInit(interp);
  Jim_packInit(interp);
  Jim_packageInit(interp);
  Jim_posixInit(interp);
  Jim_readdirInit(interp);
  Jim_regexpInit(interp);
  Jim_signalInit(interp);
  Jim_tclprefixInit(interp);
  //
  return JIM_OK;
}
